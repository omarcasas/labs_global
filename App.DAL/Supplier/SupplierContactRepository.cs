﻿using App.Entities;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Supplier
{
    public class SupplierContactRepository
    {
        private PRTSAIEntities _context;

        public SupplierContactRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int AddSupplierContact(MA_SUPPLIER_CONTACT supplierContact)
        {
            var check = 0;
            _context.MA_SUPPLIER_CONTACT.Add(supplierContact);
            try
            {
                check = _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }

            return check;
        }
        public List<SupplierContactModel> GetAllContactsBySupplierId(string supplier_id)
        {
            return _context.MA_SUPPLIER_CONTACT.Where(x => x.supplier_id.ToString() == supplier_id)
                .Select(x => new SupplierContactModel
                {
                    supplier_contact_id = x.supplier_contact_id.ToString(),
                    name = x.name,
                    last_name = x.last_name,
                    phone = x.phone,
                    email = x.email,
                    department = x.departament,
                    flagActive = x.flagActive
                })
                .ToList();
        }
        public int UpdateSupplierContact(MA_SUPPLIER_CONTACT supplierContact)
        {
            _context.Entry(supplierContact).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }
        public MA_SUPPLIER_CONTACT GetContactById(int id)
        {
            return _context.MA_SUPPLIER_CONTACT.FirstOrDefault(x => x.supplier_contact_id == id);
        }

        public CreditorPOModel GetContactFirstByDeparment(int id_supplier, string deparment)
        {
            return _context.MA_SUPPLIER
            .Join(_context.MA_SUPPLIER_CONTACT, h => h.supplier_id, d => d.supplier_id, (h, d) => new { h, d })
            .Where(w => w.d.supplier_id == id_supplier && w.d.departament == deparment && w.d.flagActive == true)
            .Select(s => new CreditorPOModel
            {
                CreditorNameBusiness = s.h.business_name,
                CreditorName = s.d.name,
                CreditorPhone = s.d.phone,
                CreditorEmail = s.d.email
            })
            .FirstOrDefault();
        }

        public string GetContactAllByDeparment(int id_supplier, string deparment)
        {
            var supplierEmails = _context.MA_SUPPLIER
            .Join(_context.MA_SUPPLIER_CONTACT, h => h.supplier_id, d => d.supplier_id, (h, d) => new { h, d })
            .Where(w => w.d.supplier_id == id_supplier && w.d.departament == deparment && w.d.flagActive == true)
            .Select(s => new CreditorPOModel
            {
                CreditorEmail = s.d.email
            })
            .ToList();
            if (supplierEmails.Count() > 0)
            {
                string emails = "";
                if (supplierEmails.Count() == 1)
                {
                    emails = supplierEmails[0].CreditorEmail;
                }
                else
                {
                    var lastEmail = supplierEmails.Last();
                    foreach (var item in supplierEmails)
                    {

                        if (lastEmail.Equals(item))
                        {
                            emails += item.CreditorEmail;
                        }
                        else
                        {
                            if (emails == "")
                                emails = item.CreditorEmail + ",";
                            else
                                emails = emails + item.CreditorEmail + ",";
                        }
                    }
                }
                return emails;
            }
            else
                return "";
        }
    }
}
