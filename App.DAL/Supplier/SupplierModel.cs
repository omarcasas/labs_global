﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Supplier
{
    public class SupplierModel
    {
        public int supplier_id { get; set; }
        public string rfc { get; set; }
        public string commercial_name { get; set; }
        public string business_name { get; set; }
        public string supplier_address { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string supplier_state { get; set; }
        public string country { get; set; }
        public string supplier_type { get; set; }
        public string accounting_account { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
        public int? truck_foot { get; set; }
        public int? merchandise_entry { get; set; }
        public int? apply_return { get; set; }
        public int? purchase_applied_return { get; set; }
        public string part_iva_purchase_supplier { get; set; }
        public int? xml_avoidable { get; set; } 
    }
}
