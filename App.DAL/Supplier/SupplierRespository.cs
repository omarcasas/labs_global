﻿using App.DAL.Purchases.PurchasesModels;
using App.DAL.Supplier;
using App.Entities.ViewModels.Creditors;
using App.Entities;
using App.Entities.ViewModels.MaSupplier;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Supplier;

namespace App.DAL.Purchases
{
    public class SupplierRespository
    {
        private PRTSAIEntities _context;

        public SupplierRespository()
        {
            _context = new PRTSAIEntities();
        }

        public int AddSupplier(MA_SUPPLIER supplier)
        {
            
            var check = 0;
            _context.MA_SUPPLIER.Add(supplier);
            try
            {
                check = _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }

            return check;
        }

        public MA_SUPPLIER GetByRfc(string rfc)
        {
            return _context.MA_SUPPLIER.Where(x => x.rfc == rfc).FirstOrDefault();
        }
        public MA_SUPPLIER GetByRfcAndType(string rfc , string type)
        {
            return _context.MA_SUPPLIER.Where(x => x.rfc == rfc && x.supplier_type == type).FirstOrDefault();
        }
        public MA_SUPPLIER GetByRFC2(string rfc)
        {
            return _context.MA_SUPPLIER.FirstOrDefault(x => x.rfc == rfc);
        }
        public MA_SUPPLIER GetById(int id)
        {
            return _context.MA_SUPPLIER.FirstOrDefault(x => x.supplier_id == id);
        }
        public List<MA_SUPPLIER> GetAllSuppliers()
        {
            return _context.MA_SUPPLIER.ToList();
        }
        public List<MA_SUPPLIER> GetAllOnlySuppliers()
        {
            return _context.MA_SUPPLIER.Where(w => w.supplier_type != "Acreedor").ToList();
        }
        public List<SupplierModel> GetAllSuppliersInModel()
        {
            return _context.MA_SUPPLIER
                .Where(x => x.supplier_type != "Acreedor")
                .Select(x => new SupplierModel
                {
                    supplier_id = x.supplier_id,
                    rfc = x.rfc,
                    commercial_name = x.commercial_name,
                    business_name = x.business_name,
                    supplier_address = x.supplier_address,
                    city = x.city,
                    zip_code = x.zip_code,
                    supplier_state = x.supplier_state,
                    country = x.country,
                    supplier_type = x.supplier_type,
                    accounting_account = x.accounting_account,
                    cuser = x.cuser,
                    cdate = x.cdate,
                    uuser = x.uuser,
                    udate = x.udate,
                    program_id = x.program
                })
                .ToList();
        }
        public List<SupplierModel> GetAllSuppliersAndCreditorInModel()
        {
            var consultaGenerico = @" 
            SELECT 
	            S.supplier_id, 
	            S.rfc, 
	            S.commercial_name, 
	            S.business_name, 
	            S.supplier_address, 
	            S.city, zip_code, 
	            S.supplier_state, 
	            S.country, 
	            S.supplier_type, 
	            S.accounting_account, 
	            S.cuser, 
	            S.cdate, 
	            S.uuser, 
	            S.udate, 
	            S.program, 
	            S.merchandise_entry, 
	            S.purchase_applied_return, 
	            S.apply_return, 
	            S.part_iva_purchase_supplier,
	            CASE WHEN M.vkey is null THEN 0 WHEN M.vkey is not null THEN 1 END as xml_avoidable
	            FROM MA_SUPPLIER S
	            LEFT JOIN MA_CODE M ON S.rfc = M.vkey AND code = 'XML_AVOIDABLE_SUPPLIERS' AND M.used = '1' ";

            return _context.Database.SqlQuery<SupplierModel>(consultaGenerico).ToList();

        }
        public List<MA_SUPPLIER> GetAvailableSuppliersByProduct(string partNumber)
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type != "Acreedor" && !_context.ITEM_SUPPLIER.Any(v => v.supplier_id == x.supplier_id && v.part_number == partNumber)).OrderBy(x => x.business_name).ThenByDescending(x => x.business_name).ToList();
        }

        public List<MA_SUPPLIER> GetAvailableSuppliersByProduct2(string partNumber)
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type != "Acreedor" &&(!_context.ITEM_SUPPLIER.Any(ss => ss.supplier_id == x.supplier_id && ss.part_number == partNumber && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == ss.supplier_part_number)) || _context.ITEM_SUPPLIER.Any(v => v.supplier_id == x.supplier_id && v.part_number == partNumber &&  _context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == v.supplier_part_number)))).OrderBy(x => x.business_name).ThenByDescending(x => x.business_name).ToList();
        }
        public List<SupplierNotActiveModel> GetSuppliersByProduct(string partNumber)
        {
            var suppliers = _context.ITEM_SUPPLIER
                .Join(_context.MA_SUPPLIER, its => its.supplier_id, ms => ms.supplier_id, (its, ms) => new { its, ms })
                .Where(x => x.ms.supplier_type != "Acreedor" && x.its.part_number == partNumber && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.its.supplier_part_number)).OrderBy(x => x.ms.business_name).ThenByDescending(x => x.ms.business_name)
                .Select(x => new SupplierNotActiveModel {
                    program = x.ms.program,
                    accounting_account = x.ms.accounting_account,
                    business_name = x.ms.business_name,
                    cdate = x.ms.cdate ?? DateTime.Now,
                    city = x.ms.city,
                    commercial_name = x.ms.commercial_name,
                    country = x.ms.country,
                    cuser = x.ms.cuser,
                    //Enable = !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.its.supplier_part_number), 
                    rfc = x.ms.rfc,
                    SuplierPartNumber = x.its.supplier_part_number,
                    supplier_address = x.ms.supplier_address,
                    supplier_id = x.ms.supplier_id,
                    supplier_state = x.ms.supplier_state,
                    supplier_type = x.ms.supplier_type,
                    udate = x.ms.udate ?? DateTime.Now,
                    uuser = x.ms.uuser,
                    zip_code = x.ms.zip_code
                })
                .ToList();
            return suppliers;
            //return _context.MA_SUPPLIER.Where(x => x.supplier_type != "Acreedor" && _context.ITEM_SUPPLIER.Any(v => v.supplier_id == x.supplier_id && v.part_number == partNumber)).OrderBy(x => x.business_name).ThenByDescending(x => x.business_name).ToList();
        }
        public List<PurchasesPriceModel> GetPURCHASEBASEPRICE(string partNumber)
        {
            try
            {
                var costs = _context.PURCHASE_BASE_COST
                  .Join(_context.ITEM_SUPPLIER, a => a.supplier_part_number, b => b.supplier_part_number, (a, b) => new { a, b })
                  .Join(_context.ITEM, u => u.b.part_number, j => j.part_number, (u, j) => new { u, j })
                  .Join(_context.MA_SUPPLIER, v => v.u.b.supplier_id, g => g.supplier_id, (v, g) => new { v, g })
                  .Where(x => x.v.u.b.part_number == partNumber && x.v.u.a.status != 0 && x.g.supplier_type != "Acreedor" && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.v.u.a.supplier_part_number))
                  .Select(p => new PurchasesPriceModel
                  {
                      base_price_id = p.v.u.a.base_cost_id > -1 ? p.v.u.a.base_cost_id.ToString() : "0",
                      supplier_part_number = p.v.u.a.supplier_part_number.Value > -1 ? p.v.u.a.supplier_part_number.Value.ToString() : "0",
                      part_name = p.v.j.part_description != "" ? p.v.j.part_description : "",
                      business_name = p.g.business_name != "" ? p.g.business_name : "",
                      base_price = p.v.u.a.base_cost.Value.ToString(),
                      currency = p.v.u.a.currency != "" ? p.v.u.a.currency : "",
                      valid_from = p.v.u.a.valid_from.ToString() != "" ? p.v.u.a.valid_from.ToString() : "",
                      valid_to = p.v.u.a.valid_to.ToString() != "" ? p.v.u.a.valid_to.ToString() : ""
                  }).ToList();
                return costs;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }

        }
        public int UpdateSupplier(MA_SUPPLIER supplier)
        {
            _context.Entry(supplier).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public List<MaSupplierDropDownModel> GetAllSuppliersToDropDown()
        {
            return _context.MA_SUPPLIER
                .Where(x => x.supplier_type == "Acreedor")
                .Select(x => new MaSupplierDropDownModel
                {
                    supplier_id = x.supplier_id,
                    business_name = x.business_name,
                }).ToList();
        }

        public List<CreditorPOModel> GetCreditorInformation(int id_creditor)
        {
            try
            {
                return _context.MA_SUPPLIER
                .Join(_context.MA_SUPPLIER_CONTACT, h => h.supplier_id, d => d.supplier_id, (h, d) => new { h, d })
                .Where(w => w.d.supplier_id == id_creditor)
                .Select(s => new CreditorPOModel
                {
                    CreditorNameBusiness = s.h.business_name,
                    CreditorName = s.d.name,
                    CreditorPhone = s.d.phone,
                    CreditorEmail = s.d.email
                })
                .ToList();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<MaSupplierDropDownModel> GetAllByType(string type)
        {
            return _context.MA_SUPPLIER
                .Where(x => x.supplier_type == type)
                .Select(x => new MaSupplierDropDownModel
                {
                    supplier_id = x.supplier_id,
                    business_name = x.business_name,
                }).ToList();
        }

        public string GetNameSupplier(int Supplier)
        {
            var SupplierName = _context.MA_SUPPLIER.Where(x => x.supplier_id == Supplier).SingleOrDefault();
            return SupplierName != null ? SupplierName.commercial_name : "";
        }

        public Tuple<string , int> EditSupplier(string RFC , string type , MA_SUPPLIER supplier )
        {
            var a = _context.MA_SUPPLIER.Any(x => x.rfc == RFC && x.supplier_type == type);
            if (a == true)
                return new Tuple<string, int> ("Ya existe el proveedor y como tipo " + type,0);
            else
            {
                try
                {
                    var sup = _context.MA_SUPPLIER.Add(supplier);
                    _context.SaveChanges();                  
                    return new Tuple<string,int> ("Guardado Correctamente",sup.supplier_id);
                }
                catch (Exception e)
                {
                    return new Tuple<string, int>("Error",0);
                    throw;
                }
            }
        }

        public bool EditSupplierPartIva(int supplier, string part_iva, string uuser)
        {
            try
            {
                var ma_supplier = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier).FirstOrDefault();

                ma_supplier.part_iva_purchase_supplier = part_iva;
                ma_supplier.uuser = uuser;
                ma_supplier.udate = DateTime.Now;

                _context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public decimal GetValueSupplierPartIva(int supplier)
        {
            try
            {
                string part_iva_supplier = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier).FirstOrDefault().part_iva_purchase_supplier;
                decimal tax = _context.MA_TAX.Where(w => w.tax_code == part_iva_supplier).FirstOrDefault().tax_value ?? 0;
                return tax;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }
        public MaSupplierModel GetSupplierInfoById(int supplier_id)
        {
            var model = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier_id)
                .Select(s => new MaSupplierModel
                {
                    SupplierId = s.supplier_id,
                    Rfc = s.rfc,
                    CommercialName = s.commercial_name,
                    BusinessName = s.business_name,
                    SupplierAddress = s.supplier_address,
                    City = s.city,
                    ZipCode = s.zip_code,
                    SupplierType = s.supplier_type,
                    AccountingAccount = s.accounting_account
                }).FirstOrDefault();

            var contact = _context.MA_SUPPLIER_CONTACT.Where(w => w.flagActive == true && w.supplier_id == supplier_id).FirstOrDefault();

            if (contact != null)
            {
                model.supplier_contact_id = contact.supplier_contact_id;
                model.name = contact.name;
                model.last_name = contact.last_name;
                model.phone = contact.phone;
                model.email = contact.email;
                model.departament = contact.departament;
            }
            else
                model.supplier_contact_id = 0;

            return model;
        }

        public List<MaSuppliersModel> GetAll()
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor").Select(x => new MaSuppliersModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }

        public List<MaSupplierModel> SearchSupplier(string supplier)
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor" && (x.business_name.Contains(supplier) || x.supplier_id.ToString().Contains(supplier))).Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }
    }
}
