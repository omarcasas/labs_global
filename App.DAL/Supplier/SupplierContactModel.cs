﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Supplier
{
    public class SupplierContactModel
    {
        public string supplier_contact_id { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string department { get; set; }
        public bool flagActive { get; set; }
    }
}
