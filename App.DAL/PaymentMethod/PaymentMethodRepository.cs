﻿using App.Entities;
using App.Entities.ViewModels.PaymentMethod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.PaymentMethod
{
    public class PaymentMethodRepository
    {
        private PRTSAIEntities _context;

        public PaymentMethodRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool AddPaymentMethod(PAYMENT_METHOD Item)
        {
            try
            {
                _context.PAYMENT_METHOD.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<PAYMENT_METHOD> GetPaymentMethods()
        {
            return _context.PAYMENT_METHOD.OrderByDescending(c => c.pm_name).ToList();
        }

        public bool DisablePaymentMethod(int PaymentMethodId)
        {
            var PaymentMethod = _context.PAYMENT_METHOD.Find(PaymentMethodId);

            if (PaymentMethod != null)
            {
                PaymentMethod.pm_menu = false;
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public PaymentMethodModel GetPaymentMethodById(int PaymentMethodId)
        {
            var PaymentMethod = new PaymentMethodModel();
            PaymentMethod = (from pm in _context.PAYMENT_METHOD
                             where pm.pm_id == PaymentMethodId
                             select new PaymentMethodModel
                             {
                                 Id = pm.pm_id,
                                 Name = pm.pm_name,
                                 Menu = pm.pm_menu,
                                 MenuOrder = pm.pm_order,
                                 SatCode = pm.sat_pm_code,
                                 Key = pm.pm_key,
                                 MaxAllowedCashBack = pm.max_allowed_cash_back,
                                 MaxAllowedPerSale = pm.max_allowed_per_sale,
                                 MaxChangedAllowedPerSale = pm.max_changed_allowed_per_sale,
                                 AllowExcededBalance = pm.allow_exceeded_balance,
                                 CanReturnChange = pm.can_return_change,
                                 AcceptWithdraw = pm.accept_withdraw,
                                 AcceptDonation = pm.accept_donation,
                                 CancellationOrder = pm.cancellation_order,
                                 Currency = pm.currency,
                                 PM = pm.payment_method1,
                                 TypeV = pm.typev
                             }).FirstOrDefault();
            return PaymentMethod;
        }

        public bool UpdatePaymentMethod(PAYMENT_METHOD Item)
        {
            var ItemEdit = _context.PAYMENT_METHOD.Find(Item.pm_id);
            if(ItemEdit != null)
            {
                _context.Entry(ItemEdit).State = System.Data.Entity.EntityState.Detached;
                _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return true;
            }

            return false;
        }
    }    
}
