﻿using App.Entities;
using App.Entities.ViewModels.SalesPriceChange;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SalesPriceChange
{
    public class SalesPriceChangesRepository
    {
        private readonly PRTSAIEntities _context;

        public SalesPriceChangesRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<SalesPriceChangeViewModel> GetSalesPriceChanges(DateTime BeginDate, DateTime EndDate, string Department, string Family)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@BeginDate", BeginDate),
                    new SqlParameter("@EndDate", EndDate)
                };

                var query = @"SELECT SALES_PRICE_CHANGE.sales_price_id, SALES_PRICE_CHANGE.part_number, ITEM.part_description, 
                    CASE WHEN ITEM.level_header > 0 AND ITEM.level_header IS NOT NULL THEN (SELECT class_name FROM MA_CLASS WHERE class_id = ITEM.level_header) ELSE 'N/A' END family,
                    CASE WHEN ITEM.level1_code > 0 AND ITEM.level1_code IS NOT NULL THEN (select class_name FROM MA_CLASS WHERE class_id = ITEM.level1_code) ELSE 'N/A' END category,
                    SALES_PRICE_CHANGE.applied_date, SALES_PRICE_CHANGE.margin, SALES_PRICE_CHANGE.sale_price, SALES_PRICE_CHANGE.sale_price_iva, 
                    ISNULL((SELECT TOP 1 LP.sale_price FROM SALES_PRICE_CHANGE LP WHERE LP.status = 0 AND LP.part_number = SALES_PRICE_CHANGE.part_number ORDER BY LP.cdate DESC), 0) 'last_price',
                    CASE WHEN SALES_PRICE_CHANGE.[status] = 1 THEN 'Activo' ELSE 'Desactivado' END 'status' , SALES_PRICE_CHANGE.cdate, USER_MASTER.first_name + ' ' + USER_MASTER.last_name cuser
                    FROM SALES_PRICE_CHANGE 
                    INNER JOIN ITEM ON SALES_PRICE_CHANGE.part_number = ITEM.part_number 
                    LEFT JOIN USER_MASTER ON SALES_PRICE_CHANGE.cuser = USER_MASTER.user_name 
                    WHERE SALES_PRICE_CHANGE.cdate BETWEEN @BeginDate AND @EndDate AND SALES_PRICE_CHANGE.status = 1 ";

                if (Department != "0")
                {
                    query += " AND ITEM.level_header = @Department ";
                    parameters.Add(new SqlParameter("@Department", Department));
                }

                if (Family != "0")
                {
                    query += " AND ITEM.level1_code = @Family ";
                    parameters.Add(new SqlParameter("@Family", Family));
                }

                query += @" ORDER BY SALES_PRICE_CHANGE.part_number, SALES_PRICE_CHANGE.cdate DESC ";

                var item = _context.Database.SqlQuery<SalesPriceChangeViewModel>(query, parameters.ToArray()).ToList();

                return item;
            }
            catch (Exception ex)
            {
                //_ = ex.Message;
                return null;
            }
        }

        public List<SalesPriceChangePresentationViewModel> GetSalesPriceChangesPresentations(DateTime BeginDate, DateTime EndDate, string Department, string Family)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@BeginDate", BeginDate),
                    new SqlParameter("@EndDate", EndDate)
                };

                var query = @"SELECT SALES_PRICE_CHANGE_PRESENTATION.sales_price_presentation_id, SALES_PRICE_CHANGE_PRESENTATION.presentation_id, ITEM_PRESENTATION.presentation_name, 
                    CASE WHEN ITEM.level_header > 0 AND ITEM.level_header IS NOT NULL THEN (SELECT class_name FROM MA_CLASS WHERE class_id = ITEM.level_header) ELSE 'N/A' END family,
                    CASE WHEN ITEM.level1_code > 0 AND ITEM.level1_code IS NOT NULL THEN (select class_name FROM MA_CLASS WHERE class_id = ITEM.level1_code) ELSE 'N/A' END category,
                    SALES_PRICE_CHANGE_PRESENTATION.applied_date, SALES_PRICE_CHANGE_PRESENTATION.margin, SALES_PRICE_CHANGE_PRESENTATION.sale_price, SALES_PRICE_CHANGE_PRESENTATION.sale_price_iva, 
                    ISNULL((SELECT TOP 1 LP.sale_price FROM SALES_PRICE_CHANGE_PRESENTATION LP WHERE LP.status = 0 AND LP.presentation_id = SALES_PRICE_CHANGE_PRESENTATION.presentation_id ORDER BY LP.cdate DESC), 0) 'last_price',
                    CASE WHEN SALES_PRICE_CHANGE_PRESENTATION.[status] = 1 THEN 'Activo' ELSE 'Desactivado' END 'status' , SALES_PRICE_CHANGE_PRESENTATION.cdate, USER_MASTER.first_name + ' ' + USER_MASTER.last_name cuser
                    FROM SALES_PRICE_CHANGE_PRESENTATION 
                    INNER JOIN ITEM_PRESENTATION ON SALES_PRICE_CHANGE_PRESENTATION.presentation_id = ITEM_PRESENTATION.presentation_id 
                    INNER JOIN ITEM ON ITEM_PRESENTATION.part_number = ITEM.part_number
                    LEFT JOIN USER_MASTER ON SALES_PRICE_CHANGE_PRESENTATION.cuser = USER_MASTER.user_name 
                    WHERE SALES_PRICE_CHANGE_PRESENTATION.cdate BETWEEN @BeginDate AND @EndDate AND SALES_PRICE_CHANGE_PRESENTATION.status = 1 ";

                if (Department != "0")
                {
                    query += " AND ITEM.level_header = @Department ";
                    parameters.Add(new SqlParameter("@Department", Department));
                }

                if (Family != "0")
                {
                    query += " AND ITEM.level1_code = @Family ";
                    parameters.Add(new SqlParameter("@Family", Family));
                }

                query += @" ORDER BY SALES_PRICE_CHANGE_PRESENTATION.presentation_id, SALES_PRICE_CHANGE_PRESENTATION.cdate DESC ";

                var item = _context.Database.SqlQuery<SalesPriceChangePresentationViewModel>(query, parameters.ToArray()).ToList();

                return item;
            }
            catch (Exception ex)
            {
                //_ = ex.Message;
                return null;
            }
        }

        public List<SalesPriceChangeViewModel> GetSalesPriceChangesDetail(DateTime BeginDate, DateTime EndDate, string PartNumber)
        {
            try
            {
                var item = _context.Database.SqlQuery<SalesPriceChangeViewModel>(@"SELECT SALES_PRICE_CHANGE.sales_price_id, SALES_PRICE_CHANGE.part_number, ITEM.part_description, 
                    CASE WHEN ITEM.level_header > 0 AND ITEM.level_header IS NOT NULL THEN (SELECT class_name FROM MA_CLASS WHERE class_id = ITEM.level_header) ELSE 'N/A' END family,
                    CASE WHEN ITEM.level1_code > 0 AND ITEM.level1_code IS NOT NULL THEN (select class_name FROM MA_CLASS WHERE class_id = ITEM.level1_code) ELSE 'N/A' END category,
                    SALES_PRICE_CHANGE.applied_date, SALES_PRICE_CHANGE.margin, SALES_PRICE_CHANGE.sale_price, SALES_PRICE_CHANGE.sale_price_iva, 
                    ISNULL((SELECT TOP 1 LP.sale_price FROM SALES_PRICE_CHANGE LP WHERE LP.status = 0 AND LP.part_number = SALES_PRICE_CHANGE.part_number ORDER BY LP.cdate DESC), 0) 'last_price',
                    CASE WHEN SALES_PRICE_CHANGE.[status] = 1 THEN 'Activo' ELSE 'Desactivado' END 'status' , SALES_PRICE_CHANGE.cdate, USER_MASTER.first_name + ' ' + USER_MASTER.last_name cuser
                    FROM SALES_PRICE_CHANGE 
                    INNER JOIN ITEM ON SALES_PRICE_CHANGE.part_number = ITEM.part_number 
                    LEFT JOIN USER_MASTER ON SALES_PRICE_CHANGE.cuser = USER_MASTER.user_name 
                    WHERE SALES_PRICE_CHANGE.cdate BETWEEN @BeginDate AND @EndDate AND SALES_PRICE_CHANGE.part_number = @PartNumber AND SALES_PRICE_CHANGE.status = 0
                    ORDER BY SALES_PRICE_CHANGE.part_number, SALES_PRICE_CHANGE.cdate DESC ",
                new SqlParameter("BeginDate", BeginDate), new SqlParameter("EndDate", EndDate), new SqlParameter("PartNumber", PartNumber)).ToList();

                return item;
            }
            catch (Exception ex)
            {
                //_ = ex.Message;
                return null;
            }
        }

        public List<SalesPriceChangePresentationViewModel> GetSalesPriceChangesPresentationsDetail(DateTime BeginDate, DateTime EndDate, int PresentationId)
        {
            try
            {
                var item = _context.Database.SqlQuery<SalesPriceChangePresentationViewModel>(@"SELECT SALES_PRICE_CHANGE_PRESENTATION.sales_price_presentation_id, SALES_PRICE_CHANGE_PRESENTATION.presentation_id, ITEM_PRESENTATION.presentation_name, 
                    CASE WHEN ITEM.level_header > 0 AND ITEM.level_header IS NOT NULL THEN (SELECT class_name FROM MA_CLASS WHERE class_id = ITEM.level_header) ELSE 'N/A' END family,
                    CASE WHEN ITEM.level1_code > 0 AND ITEM.level1_code IS NOT NULL THEN (select class_name FROM MA_CLASS WHERE class_id = ITEM.level1_code) ELSE 'N/A' END category,
                    SALES_PRICE_CHANGE_PRESENTATION.applied_date, SALES_PRICE_CHANGE_PRESENTATION.margin, SALES_PRICE_CHANGE_PRESENTATION.sale_price, SALES_PRICE_CHANGE_PRESENTATION.sale_price_iva, 
                    ISNULL((SELECT TOP 1 LP.sale_price FROM SALES_PRICE_CHANGE_PRESENTATION LP WHERE LP.status = 0 AND LP.presentation_id = SALES_PRICE_CHANGE_PRESENTATION.presentation_id ORDER BY LP.cdate DESC), 0) 'last_price',
                    CASE WHEN SALES_PRICE_CHANGE_PRESENTATION.[status] = 1 THEN 'Activo' ELSE 'Desactivado' END 'status' , SALES_PRICE_CHANGE_PRESENTATION.cdate, USER_MASTER.first_name + ' ' + USER_MASTER.last_name cuser
                    FROM SALES_PRICE_CHANGE_PRESENTATION 
                    INNER JOIN ITEM_PRESENTATION ON SALES_PRICE_CHANGE_PRESENTATION.presentation_id = ITEM_PRESENTATION.presentation_id 
                    INNER JOIN ITEM ON ITEM_PRESENTATION.part_number = ITEM.part_number
                    LEFT JOIN USER_MASTER ON SALES_PRICE_CHANGE_PRESENTATION.cuser = USER_MASTER.user_name 
                    WHERE SALES_PRICE_CHANGE_PRESENTATION.cdate BETWEEN @BeginDate AND @EndDate AND SALES_PRICE_CHANGE_PRESENTATION.presentation_id = @PresentationId AND SALES_PRICE_CHANGE_PRESENTATION.status = 0
                    ORDER BY SALES_PRICE_CHANGE_PRESENTATION.cdate DESC ",
                new SqlParameter("BeginDate", BeginDate), new SqlParameter("EndDate", EndDate), new SqlParameter("PresentationId", PresentationId)).ToList();

                return item;
            }
            catch (Exception ex)
            {
                //_ = ex.Message;
                return null;
            }
        }
    }
}