﻿using App.Entities;
using System.Collections.Generic;
namespace App.DAL.Configuration
{
    public class SiteViewModel
    {
        public string site_code { get; set; }
        public string site_name { get; set; }
        public string ip_address { get; set; }
        public string address { get; set; }
    }
}
