﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Item
{
    public class ItemOldErpRepository
    {
        private CDSERPEntities _contextOldERP;
        private PRTSAIEntities _context;

        public ItemOldErpRepository()
        {
            _contextOldERP = new CDSERPEntities();
            _context = new PRTSAIEntities();
        }

        //Supplier ERP viejo
        public List<ItemOldErpModel> getOLDERPSupplier(string Filter)
        {
            try
            {
                var item = _contextOldERP.Database.SqlQuery<ItemOldErpModel>(@"select Supplier_id,Supplier_name 
                from Florido_ERP.dbo.Suppliers where supplier_status =1 and Supplier_name like @Filter",
                new SqlParameter("filter", string.Format("%{0}%", Filter))).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        //Supplier ERP nuevo
        public List<ItemOldErpModel> getERPSupplier(string Filter)
        {
            try
            {
                var item = _context.Database.SqlQuery<ItemOldErpModel>(@"select supplier_id , commercial_name 
                from dbo.MA_SUPPLIER where  commercial_name like @Filter",
                new SqlParameter("filter", string.Format("%{0}%", Filter))).ToList();

                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }



        public List<ItemOldErpModel> getDistributionType()
        {
            try
            {
                var item = _contextOldERP.Database.SqlQuery<ItemOldErpModel>(@"SELECT vkey FROM MA_CODE WHERE (code = 'DISTRIBUTION') ORDER BY vkey_seq").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<ItemOldErpModel> getItemGrup()
        {
            try
            {
                var item = _contextOldERP.Database.SqlQuery<ItemOldErpModel>(@"SELECT grupo_id, grupo_desc FROM Florido_Erp.dbo.Grupos WHERE(grupo_id <> 0)").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<ItemOldErpModel> getItemFamili(string Filter)
        {
            try
            {
                var item = _contextOldERP.Database.SqlQuery<ItemOldErpModel>(@"SELECT level1_code,level1_desc FROM Florido_Erp.dbo.level1 WHERE (grupo_id = @filter)",
                new SqlParameter("filter", Filter)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<ItemOldErpModel> getItemUm()
        {
            try
            {
                var item = _contextOldERP.Database.SqlQuery<ItemOldErpModel>(@"SELECT um_desc FROM Florido_Erp.dbo.UM").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<ItemOldErpModel> getBuyerid(string buyer)
        {
            try
            {
                var item = _contextOldERP.Database.SqlQuery<ItemOldErpModel>(@"SELECT buyer_id,description FROM Florido_ERP.dbo.buyers FB
                INNER JOIN CDSERP.dbo.MA_CODE CM ON FB.buyer_id = CM.vkey WHERE CODE = 'DIVISION_BUYERERP'
                and description = @buyer", new SqlParameter("buyer", buyer)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }



        public int AddItemWMSTemp(ITEM_TEMP_ERP ItemWMS, ITEM Item)
        {
            try
            {
                var item = _contextOldERP.Database.ExecuteSqlCommand(@"INSERT INTO [dbo].[ITEM_TEMP_ERP]
                    ([product_id],[product_name],[product_description],[product_nick],[um],[iva],[ieps],[distribution_type]
                    ,[buyer_division],[product_type],[ERP_part_cost],[ERP_part_cost_cur],[ERP_part_price],[ERP_part_price3]
                    ,[ERP_part_price_cur],[ERP_supplier_id],[ERP_level1_code],[ERP_part_status],[ERP_part_um],[ERP_grupo_id]
                    ,[ERP_part_margen],[ERP_part_margen3],[ERP_part_pesado],[ERP_buyer_id],[active_flag],[mrp_type],[cuser]
                    ,[cdate],[sat_code],[NEW_erp_level_header],[NEW_erp_level1_code],[NEW_erp_level2_code],[NEW_erp_level3_code]
                    ,[NEW_erp_unit_sat],[NEW_erp_part_iva_sale],[NEW_erp_combo_flag],[NEW_erp_extra_items_flag], [expiration_days])
                    VALUES
                    (@product_id,
                    @product_name,
                    @product_description,
                    @product_nick,
                    @um,
                    @iva,
                    @ieps,
                    @distribution_type,
                    @buyer_division, 
                    @product_type,
                    @ERP_part_cost,
                    @ERP_part_cost_cur, 
                    @ERP_part_price,
                    @ERP_part_price3,
                    @ERP_part_price_cur,
                    @ERP_supplier_id,
                    @ERP_level1_code,
                    1,
                    @ERP_part_um, 
                    @ERP_grupo_id, 
                    @ERP_part_margen,
                    @ERP_part_margen3, 
                    @ERP_part_pesado,
                    @ERP_buyer_id, 
                    0,
                    @mrp_type, 
                    @cuser,
                    GETDATE(),
                    @sat_code,
                    @NEW_erp_level_header,
                    @NEW_erp_level1_code,
                    @NEW_erp_level2_code,
                    @NEW_erp_level3_code,
                    @NEW_erp_unit_sat,
                    @NEW_erp_part_iva_sale,
                    @NEW_erp_combo_flag,
                    @NEW_erp_extra_items_flag,
                    @expiration_days)",
                    new SqlParameter("product_id", ItemWMS.product_id),
                    new SqlParameter("product_name", ItemWMS.product_description),
                    new SqlParameter("product_description", ItemWMS.product_description),
                    new SqlParameter("product_nick", ItemWMS.product_description),
                    new SqlParameter("um", ItemWMS.um),
                    new SqlParameter("iva", ItemWMS.iva),
                    new SqlParameter("ieps", ItemWMS.ieps),
                    new SqlParameter("distribution_type", ItemWMS.distribution_type),
                    new SqlParameter("buyer_division", ItemWMS.buyer_division),
                    new SqlParameter("product_type", ItemWMS.product_type),
                    new SqlParameter("ERP_part_cost", ItemWMS.ERP_part_cost),
                    new SqlParameter("ERP_part_cost_cur", ItemWMS.ERP_part_cost_cur),
                    new SqlParameter("ERP_part_price", ItemWMS.ERP_part_price),
                    new SqlParameter("ERP_part_price3", ItemWMS.ERP_part_price3),
                    new SqlParameter("ERP_part_price_cur", ItemWMS.ERP_part_price_cur),
                    new SqlParameter("ERP_supplier_id", ItemWMS.ERP_supplier_id),
                    new SqlParameter("ERP_level1_code", ItemWMS.ERP_level1_code),
                    new SqlParameter("ERP_part_um", ItemWMS.ERP_part_um),
                    new SqlParameter("ERP_grupo_id", ItemWMS.ERP_grupo_id),
                    new SqlParameter("ERP_part_margen", ItemWMS.ERP_part_margen),
                    new SqlParameter("ERP_part_margen3", ItemWMS.ERP_part_margen3),
                    new SqlParameter("ERP_part_pesado", ItemWMS.ERP_part_pesado),
                    new SqlParameter("ERP_buyer_id", ItemWMS.ERP_buyer_id),
                    new SqlParameter("mrp_type", ItemWMS.mrp_type),
                    new SqlParameter("cuser", ItemWMS.cuser),
                    new SqlParameter("sat_code", ItemWMS.sat_code),
                    new SqlParameter("NEW_erp_level_header", Item.level_header),
                    new SqlParameter("NEW_erp_level1_code", Item.level1_code),
                    new SqlParameter("NEW_erp_level2_code", Item.level2_code),
                    new SqlParameter("NEW_erp_level3_code", Item.level3_code),
                    new SqlParameter("NEW_erp_unit_sat", Item.unit_sat),
                    new SqlParameter("NEW_erp_part_iva_sale", Item.part_iva_sale),
                    new SqlParameter("NEW_erp_extra_items_flag", Item.extra_items_flag),
                    new SqlParameter("NEW_erp_combo_flag", Item.combo_flag),
                    new SqlParameter("expiration_days", Item.expiration_days));

                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int AddItemTemp(ITEM_TEMP_ERP ItemWMS, ITEM Item)
        {
            try
            {
                var item = _context.Database.ExecuteSqlCommand(@"INSERT INTO [dbo].[ITEM_TEMP_ERP_PRT]
                    ([product_id],[product_name],[product_description],[product_nick],[um],[iva],[ieps]
                    ,[ERP_part_cost],[ERP_part_cost_cur],[ERP_part_price],[ERP_part_price3]
                    ,[ERP_supplier_id]
                    ,[ERP_part_pesado],[active_flag],[mrp_type],[cuser]
                    ,[cdate],[sat_code],[NEW_erp_level_header],[NEW_erp_level1_code],[NEW_erp_level2_code],[NEW_erp_level3_code]
                    ,[NEW_erp_unit_sat],[NEW_erp_part_iva_sale],[NEW_erp_combo_flag],[NEW_erp_extra_items_flag], [expiration_days])
                    VALUES
                    (@product_id,
                    @product_name,
                    @product_description,
                    @product_nick,
                    @um,
                    @iva,
                    @ieps,
                   
                    @ERP_part_cost,
                    @ERP_part_cost_cur, 
                    @ERP_part_price,
                    @ERP_part_price3,
                    @ERP_supplier_id,
                    @ERP_part_pesado,
                    1,
                    @mrp_type, 
                    @cuser,
                    GETDATE(),
                    @sat_code,
                    @NEW_erp_level_header,
                    @NEW_erp_level1_code,
                    @NEW_erp_level2_code,
                    @NEW_erp_level3_code,
                    @NEW_erp_unit_sat,
                    @NEW_erp_part_iva_sale,
                    @NEW_erp_combo_flag,
                    @NEW_erp_extra_items_flag,
                    @expiration_days)",
                    new SqlParameter("product_id", ItemWMS.product_id),
                    new SqlParameter("product_name", ItemWMS.product_description),
                    new SqlParameter("product_description", ItemWMS.product_description),
                    new SqlParameter("product_nick", ItemWMS.product_description),
                    new SqlParameter("um", ItemWMS.um),
                    new SqlParameter("iva", ItemWMS.iva),
                    new SqlParameter("ieps", ItemWMS.ieps),
                    
                    new SqlParameter("ERP_part_cost", ItemWMS.ERP_part_cost),
                    new SqlParameter("ERP_part_cost_cur", ItemWMS.ERP_part_cost_cur),
                    new SqlParameter("ERP_part_price", ItemWMS.ERP_part_price),
                    new SqlParameter("ERP_part_price3", ItemWMS.ERP_part_price3),
                    new SqlParameter("ERP_supplier_id", ItemWMS.ERP_supplier_id),
                    new SqlParameter("ERP_part_pesado", ItemWMS.ERP_part_pesado),
                    new SqlParameter("mrp_type", ItemWMS.mrp_type),
                    new SqlParameter("cuser", ItemWMS.cuser),
                    new SqlParameter("sat_code", ItemWMS.sat_code),
                    new SqlParameter("NEW_erp_level_header", Item.level_header),
                    new SqlParameter("NEW_erp_level1_code", Item.level1_code),
                    new SqlParameter("NEW_erp_level2_code", Item.level2_code),
                    new SqlParameter("NEW_erp_level3_code", Item.level3_code),
                    new SqlParameter("NEW_erp_unit_sat", Item.unit_sat),
                    new SqlParameter("NEW_erp_part_iva_sale", Item.part_iva_sale),
                    new SqlParameter("NEW_erp_extra_items_flag", Item.extra_items_flag),
                    new SqlParameter("NEW_erp_combo_flag", Item.combo_flag),
                    new SqlParameter("expiration_days", Item.expiration_days));

                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int Sp_Insert_New_Produ(string part_number, string user)
        {
            var DateReturn = _context
                .Database
                .SqlQuery<int>(

                @"DECLARE @return_value int

                EXEC  @return_value = [dbo].[sp_Insert_New_Produ]
                @product_id = @p, 
                @uuser = @u

                SELECT	'Return Value' = @return_value",

                new SqlParameter("@p", part_number),
                new SqlParameter("@u", user)).SingleOrDefault();

            return DateReturn;
        }



    }
}
