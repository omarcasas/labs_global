﻿using App.DAL.Supplier;
using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Purchases
{

    public class ItemSupplierRepository
    {
        private PRTSAIEntities _context;
        private CDSERPEntities _contextOldERP;

        public ItemSupplierRepository()
        {
            _context = new PRTSAIEntities();
            _contextOldERP = new CDSERPEntities();
        }

        public List<ItemSupplierModel> GetItemsBySupplierCodeList(List<int> supplier_code)
        {
            return _context.ITEM_SUPPLIER.Join(_context.ITEM, itemSupplier => itemSupplier.part_number, item => item.part_number, (itemSupplier, item) => new { itemSupplier, item })
                .Join(_context.PURCHASE_BASE_COST, i => i.itemSupplier.supplier_part_number, pbc => pbc.supplier_part_number, (i, pbc) => new { i, pbc })
                .Join(_context.MA_SUPPLIER, i => i.i.itemSupplier.supplier_id, sub => sub.supplier_id, (e, sub) => new { e.i, e.pbc, sub })
                .Where(x => x.i.itemSupplier.part_number == x.i.item.part_number && supplier_code.Contains(x.i.itemSupplier.supplier_id.Value) && x.pbc.supplier_part_number == x.i.itemSupplier.supplier_part_number && x.pbc.status == 1 && x.i.item.active_flag == true
                            && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.i.itemSupplier.supplier_part_number))
                .OrderByDescending(k => k.pbc.udate)
                .ToList()
                .Select(q => new ItemSupplierModel
                {
                    item_number = q.i.item.part_number,
                    item_name = q.i.item.part_description,
                    item_cost = q.pbc.base_cost.ToString(),
                    item_currency = q.pbc.currency,
                    item_base_cost_id = q.pbc.base_cost_id.ToString(),
                    item_supplier_id = q.pbc.supplier_part_number.ToString(),
                    supplier_name = q.sub.commercial_name,

                    uDate = q.pbc.udate != null ? q.pbc.udate.Value.ToString("MM/dd/yyyy") : ""
                }).ToList();
        }
        public bool DisableItemSupplier(int supplierPartNumber, string cuser)
        {
            try
            {
                _context.ITEM_SUPPLIER_INACTIVE.Add(new ITEM_SUPPLIER_INACTIVE() { supplier_part_number = supplierPartNumber, cuser = cuser, cdate = DateTime.Now });
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<ItemSupplierModel> GetItemsBySupplierCodeInSalePrice(int supplier_code)
        {
            try
            {
                //var costs = _context.ITEM_SUPPLIER
                //    .Join(_context.ITEM, itemSupplier => itemSupplier.part_number, item => item.part_number, (itemSupplier, item) => new { itemSupplier, item })
                //    .Join(_context.PURCHASE_BASE_COST, i => i.itemSupplier.supplier_part_number, pbc => pbc.supplier_part_number, (i, pbc) => new { i, pbc })
                //    .Join(_context.SALES_PRICE_CHANGE, items => items.i.itemSupplier.part_number, spc => spc.part_number, (items, spc) => new { items, spc })
                //    .Join(_context.MA_TAX, s => s.items.i.item.part_iva_purchase, iva => iva.tax_code, (s, iva) => new { s, iva })
                //    .Join(_context.MA_TAX, ss => ss.s.items.i.item.part_ieps, ieps => ieps.tax_code, (ss, ieps) => new { ss, ieps })
                //    .Where(x => x.ss.s.items.i.itemSupplier.supplier_id == supplier_code && x.ss.s.items.pbc.status == 1 && x.ss.s.spc.status == 1 && x.ss.s.items.i.item.combo_flag != true && x.ss.s.items.i.item.active_flag == true && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.ss.s.items.i.itemSupplier.supplier_part_number))
                //    .OrderByDescending(k => k.ss.s.spc.cdate)
                //    .ToList()
                //    .Select(q => new ItemSupplierModel
                //    {
                //        item_number = q.ss.s.items.i.item.part_number,
                //        item_name = q.ss.s.items.i.item.part_description,
                //        currency = q.ss.s.items.pbc.currency,
                //        item_cost = q.ss.s.items.pbc.base_cost.Value.ToString("0.00"),
                //        margen = q.ss.s.spc.margin.ToString(),
                //        iva = q.ss.iva.tax_value.ToString(),
                //        ieps = q.ieps.tax_value.ToString(),
                //        price = q.ss.s.spc.sale_price_iva.ToString(),
                //        uDate = q.ss.s.spc.applied_date != null ? q.ss.s.spc.applied_date.ToString("MM/dd/yyyy") : "",
                //        packing_size = "0"
                //    }).ToList();

                //            var costs = _context.ITEM_SUPPLIER
                //.Join(_context.ITEM, itemSupplier => itemSupplier.part_number, item => item.part_number, (itemSupplier, item) => new { itemSupplier, item })
                //.Join(_context.PURCHASE_BASE_COST, i => i.itemSupplier.supplier_part_number, pbc => pbc.supplier_part_number, (i, pbc) => new { i, pbc })
                //.Join(_context.SALES_PRICE_CHANGE, items => items.i.itemSupplier.part_number, spc => spc.part_number, (items, spc) => new { items, spc })
                //.Join(_context.MA_TAX, s => s.items.i.item.part_iva_purchase, iva => iva.tax_code, (s, iva) => new { s, iva })
                //.Join(_context.MA_TAX, ss => ss.s.items.i.item.part_ieps, ieps => ieps.tax_code, (ss, ieps) => new { ss, ieps })
                //.Join(_context.ITEM_SPECS, iss => iss.ss.s.spc.part_number, iss2 => iss2.part_number, (iss, iss2) => new { iss, iss2 })
                //.Where(x => x.iss.ss.s.items.i.itemSupplier.supplier_id == supplier_code && x.iss.ss.s.items.pbc.status == 1 && x.iss.ss.s.spc.status == 1 && x.iss.ss.s.items.i.item.combo_flag != true && x.iss.ss.s.items.i.item.active_flag == true && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.iss.ss.s.items.i.itemSupplier.supplier_part_number))
                //.OrderByDescending(k => k.iss.ss.s.spc.cdate)
                //.ToList()
                //.Select(q => new ItemSupplierModel
                //{
                //    item_number = q.iss.ss.s.items.i.item.part_number,
                //    item_name = q.iss.ss.s.items.i.item.part_description,
                //    currency = q.iss.ss.s.items.pbc.currency,
                //    item_cost = q.iss.ss.s.items.pbc.base_cost.Value.ToString("0.00"),
                //    margen = q.iss.ss.s.spc.margin.ToString(),
                //    iva = q.iss.ss.iva.tax_value.ToString(),
                //    ieps = q.iss.ieps.tax_value.ToString(),
                //    price = q.iss.ss.s.spc.sale_price_iva.ToString(),
                //    uDate = q.iss.ss.s.spc.applied_date != null ? q.iss.ss.s.spc.applied_date.ToString("MM/dd/yyyy") : "",
                //    packing_size = q.iss2.packing_of_size != null ? q.iss2.packing_of_size.ToString() : "1"
                //}).ToList();

                var costs = _context.ITEM_SUPPLIER
    .Join(_context.ITEM, itemSupplier => itemSupplier.part_number, item => item.part_number, (itemSupplier, item) => new { itemSupplier, item })
    .Join(_context.PURCHASE_BASE_COST, i => i.itemSupplier.supplier_part_number, pbc => pbc.supplier_part_number, (i, pbc) => new { i, pbc })
    .Join(_context.SALES_PRICE_CHANGE, items => items.i.itemSupplier.part_number, spc => spc.part_number, (items, spc) => new { items, spc })
    .Join(_context.MA_TAX, s => s.items.i.item.part_iva_sale, iva => iva.tax_code, (s, iva) => new { s, iva })
    .Join(_context.MA_TAX, ss => ss.s.items.i.item.part_ieps, ieps => ieps.tax_code, (ss, ieps) => new { ss, ieps })
    .Where(x => x.ss.s.items.i.itemSupplier.supplier_id == supplier_code && x.ss.s.items.pbc.status == 1 && x.ss.s.spc.status == 1 && x.ss.s.items.i.item.combo_flag != true && x.ss.s.items.i.item.active_flag == true && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.ss.s.items.i.itemSupplier.supplier_part_number))
    .OrderByDescending(k => k.ss.s.spc.cdate)
    .ToList()
    .Select(q => new ItemSupplierModel
    {
        item_number = q.ss.s.items.i.item.part_number,
        item_name = q.ss.s.items.i.item.part_description,
        currency = q.ss.s.items.pbc.currency,
        item_cost = q.ss.s.items.pbc.base_cost.Value.ToString("0.00"),
        margen = q.ss.s.spc.margin.ToString(),
        iva = q.ss.iva.tax_value.ToString(),
        ieps = q.ieps.tax_value.ToString(),
        price = q.ss.s.spc.sale_price_iva.ToString(),
        uDate = q.ss.s.spc.applied_date != null ? q.ss.s.spc.applied_date.ToString("MM/dd/yyyy") : "",
        packing_size = "0"
    }).ToList();


                foreach (var cost in costs)
                {
                    var packing = _context.ITEM_SPECS.Where(w => w.part_number == cost.item_number).FirstOrDefault();
                    cost.packing_size = packing.packing_of_size == null ? "1" : packing.packing_of_size.ToString();
                }
                return costs;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }

        }

        public int AssignSupplierToProduct(ITEM_SUPPLIER SupplierId)
        {
            try
            {
                var supplierExist = _context.ITEM_SUPPLIER.Where(x => x.supplier_id == (SupplierId.supplier_id ?? 0) && x.part_number == SupplierId.part_number).SingleOrDefault();
                if (supplierExist != null)
                {
                    var supplier = _context.ITEM_SUPPLIER_INACTIVE.Where(x => x.supplier_part_number == supplierExist.supplier_part_number).SingleOrDefault();
                    if (supplier != null)
                    {
                        var model = _context.ITEM_SUPPLIER_INACTIVE.Remove(supplier);
                        _context.SaveChanges();
                        return model.supplier_part_number;
                    }
                    return 0;
                }
                else
                {
                    var model = _context.ITEM_SUPPLIER.Add(SupplierId);
                    _context.SaveChanges();
                    return model.supplier_part_number;
                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }

        public List<ItemMasterModel> getSupplierItem(int Supplier_id)
        {
            var Supplier = _context.ITEM_SUPPLIER.Where(x => x.supplier_id == Supplier_id).ToList();
            List<ItemMasterModel> Items = new List<ItemMasterModel>();
            foreach (var item in Supplier)
            {
                var Product = _context.ITEM.Where(x => x.part_number == item.part_number).Select(x => new ItemMasterModel
                {
                    part_number = x.part_number,
                    part_description = x.part_description
                }).SingleOrDefault();
                if (Product != null)
                {
                    Items.Add(Product);
                }
            }
            return Items;
        }
        public List<SupplierModel> getItemSupplier(string PartNumber)
        {
            var Items = _context.ITEM_SUPPLIER.Where(x => x.part_number == PartNumber).ToList();
            List<SupplierModel> Supplier = new List<SupplierModel>();
            foreach (var item in Items)
            {
                var Product = _context.MA_SUPPLIER.Where(x => x.supplier_id == item.supplier_id && x.supplier_type != "Acreedor").Select(x => new SupplierModel
                {
                    supplier_id = x.supplier_id,
                    business_name = x.business_name
                }).SingleOrDefault();
                if (Product != null)
                {
                    Supplier.Add(Product);
                }
            }
            return Supplier;
        }
        public List<itemsSuppier> GetItemsBySupplierIdToPromotions(int supplier_id)
        {

            var _supplierId = new SqlParameter("@supplier_id", supplier_id);

            var result = _context.Database
                .SqlQuery<itemsSuppier>("sp_GetItemsBySupplierId @supplier_id", _supplierId)
                .ToList();
            return result;
        }
    }
}
