﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace App.DAL.Item
{
    public class ItemComboRepository
    {
        private PRTSAIEntities _context;

        //constructor
        public ItemComboRepository()
        {
            _context = new PRTSAIEntities();
        }
        public int AddComboHeader(COMBO_HEADER combo)
        {
            try
            {
                _context.COMBO_HEADER.Add(combo);
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int AddComboDetail(COMBO_DETAIL comboDetail)
        {
            try
            {
                _context.COMBO_DETAIL.Add(comboDetail);
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int UpdateComboHeader(COMBO_HEADER comboHeader)
        {
            try
            {
                _context.Entry(comboHeader).State = System.Data.Entity.EntityState.Modified;
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }

        public int UpdateComboDetail(COMBO_DETAIL comboDetail)
        {
            try
            {
                _context.Entry(comboDetail).State = System.Data.Entity.EntityState.Modified;
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }

        public List<ItemComboModel> getCombosByStatus(string status)
        {
            return _context.COMBO_HEADER.Join(_context.ITEM, combo => combo.part_number_combo, item => item.part_number, (combo, item) => new { combo, item })
                .Where(x => x.combo.combo_status == status&& x.item.active_flag == true)
                .Select(q => new ItemComboModel
                {
                    part_number_combo = q.combo.part_number_combo,
                    part_description_combo = q.item.part_description,
                    combo_status = q.item.part_description
                }).ToList();
        }
        public List<ItemComboModel> getCombosDetailByPartNumberCombo(string partNumberCombo)
        {
            return _context.COMBO_DETAIL.Join(_context.ITEM, combo => combo.part_number, item => item.part_number, (combo, item) => new { combo, item })
                .Where(x => x.combo.part_number_combo == partNumberCombo)
                .Select(q => new ItemComboModel
                {
                    part_number_combo = q.combo.part_number_combo,
                    part_number = q.combo.part_number,
                    part_description_combo = q.item.part_description,
                    active_status = q.combo.active_status.Value,
                    quantity = q.combo.quantity.Value,
                    iva = q.combo.iva.Value,
                    ivaDescription = q.item.part_iva_sale,
                    total_price_sale = q.combo.total_price_sale.Value,
                    weightValue = q.item.weight_flag != null ? q.item.weight_flag.Value : false
                }).ToList();
        }
        public COMBO_HEADER getComboHeaderItem(string partNumberCombo)
        {
            return _context.COMBO_HEADER.FirstOrDefault(x => x.part_number_combo == partNumberCombo);
        }
        public COMBO_DETAIL getComboDetailItem(string partNumberCombo, string partNumber)
        {
            return _context.COMBO_DETAIL.FirstOrDefault(x => x.part_number_combo == partNumberCombo && x.part_number == partNumber);
        }
        public ItemComboModel getCombosBaseCostByPartNumber(string partNumberCombo)
        {
            try
            {
                var baseCost = _context.COMBO_HEADER.Where(x => x.part_number_combo == partNumberCombo).Select(p => new ItemComboModel
                {
                    base_cost = p.combo_cost.Value > -1 ? p.combo_cost.Value : 0,
                }).SingleOrDefault();
                if (baseCost == null)
                {
                    baseCost = new ItemComboModel();
                    baseCost.base_cost = 0;
                }
                return baseCost;
            }
            catch (Exception es)
            {
                var msg = es.Message;
                throw;
            }
        }
    }
}
