﻿using App.DAL.ChangePrice;
using App.Entities;
using App.Entities.ViewModels.CodesPackege;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.ItemPresentation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

public class ItemMasterRepository
{
    private readonly PRTSAIEntities _context;

    public ItemMasterRepository()
    {
        _context = new PRTSAIEntities();
    }

    public ITEM GetItemByPartNumber(string partNumber)
    {
        var value = _context.BARCODES.FirstOrDefault(f => f.part_barcode == partNumber || f.part_number == partNumber);

        if (value != null)
            return _context.ITEM.FirstOrDefault(x => x.part_number == value.part_number);
        else
            return null;
    }

    public List<ITEM> SearchItem(string part_number)
    {
        return _context.ITEM.Where(x => x.part_number.Contains(part_number) || x.part_description.Contains(part_number)).ToList();
    }

    public List<ItemModel> SearchItemBarcode(string part_number)
    {
        return _context.ITEM
            .Join(_context.BARCODES, document => document.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
            .Where(x => (x.document.part_number.Contains(part_number) || x.document.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
            && x.document.extra_items_flag != true && x.document.combo_flag != true && x.document.active_flag == true)
            .Select(x => new ItemModel
            {
                Description = x.document.part_description,
                PathNumber = x.document.part_number
            }).Distinct().ToList();
    }

    public List<ItemBarcodeAndPartNumber> SearchBarcode(string part_number)
    {
        return _context.BARCODES.Where(x => x.part_number == part_number || x.part_barcode == part_number)
            .Select(x => new ItemBarcodeAndPartNumber
            {
                Barcode = x.part_barcode,
                Cuser = x.cuser,
                PartNumber = x.part_number
            }).ToList();
    }

    public int SaveBarcode(string part_number, string barcode, string user)
    {
        try
        {
            var count = _context.BARCODES.Where(x => x.part_number == barcode || x.part_barcode == barcode).Count();
            if (count == 0)
            {
                _context.BARCODES.Add(new BARCODES() { part_number = part_number, part_barcode = barcode.ToUpper(), cdate = DateTime.Now, cuser = user });
                _context.SaveChanges();
                return 1;
            }
            else
            {
                return 2;
            }
        }
        catch (Exception)
        {
            return 3;
        }

    }

    public string GeneraTableByEmail(List<SalesPriceChangeModel> products)
    {
        //Obtener descripcion
        foreach (var productDescription in products)
        {
            var itemInfo = GetItemByPartNum(productDescription.partNumber);
            productDescription.part_description = itemInfo.part_description;
        }
        //Generar tabla
        string table = "<table align='center'><tr><th style='width: 220pxpx; color: white; background-color: #224191;'>Código</th><th style='width: 220pxpx; color: white; background-color: #224191;'>Descripción</th><th style='width: 220pxpx; color: white; background-color: #224191;'>Precio de Venta</th></tr>";
        foreach (var product in products)
        {
            table += "<tr><td style='width: 220pxpx;'>" + product.partNumber + "</td><td style='width: 220pxpx;'>" + product.part_description + "</td><td style='width: 220pxpx;'> $" + product.priceUser + "</td><tr>";
        }
        table += "</table>";
        return table;
    }

    public List<ItemModel> SearchItemBarcodeSupplier(string part_number, int supplier)
    {
        return _context.ITEM_SUPPLIER
            .Join(_context.ITEM, item => item.part_number, itemSupplier => itemSupplier.part_number, (itemSupplier, item) => new { itemSupplier, item })
            .Join(_context.BARCODES, document => document.item.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
            .Where(x => (x.document.item.part_number.Contains(part_number) || x.document.item.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
            && x.document.item.extra_items_flag != true && x.document.item.combo_flag != true && x.document.item.active_flag == true && x.document.itemSupplier.supplier_id == supplier
            && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.document.itemSupplier.supplier_part_number))
            .Select(x => new ItemModel
            {
                Description = x.document.item.part_description,
                PathNumber = x.document.item.part_number
            }).Distinct().ToList();
    }

    public ItemMasterModel GetItemByPartNum(string partNumber)
    {
        var value = _context.BARCODES.FirstOrDefault(f => f.part_barcode == partNumber || f.part_number == partNumber);

        if (value != null)
        {
            var item = _context.ITEM
                .Where(x => x.part_number == value.part_number && x.active_flag == true)
                .Select(x => new ItemMasterModel
                {
                    part_number = x.part_number,
                    part_description = x.part_description,
                    unit_size = x.unit_size,
                    part_type = x.part_type,
                    buyer_division = x.buyer_division,
                    part_iva = x.part_iva_sale ?? "",
                    part_ieps = x.part_ieps ?? "",
                    part_number_sat = x.part_number_sat,
                    unit_sat = x.unit_sat,
                    weight_flag = x.weight_flag != null ? x.weight_flag.Value : false,
                    active_flag = x.active_flag != null ? x.active_flag.Value : false
                }).FirstOrDefault();

            return item;
        }
        else
            return null;
    }

    public int AddItemMaster(ITEM item)
    {
        try
        {
            _context.ITEM.Add(item);
            return _context.SaveChanges();
        }
        catch (DbEntityValidationException dbEx)
        {
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    Trace.TraceInformation("Property: {0} Error: {1}",
                                            validationError.PropertyName,
                                            validationError.ErrorMessage);
                }
            }
            return 0;
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return 0;
        }
    }

    public ITEM GetItemByPartNumberSAT(string partNumberSAT)
    {
        return _context.ITEM.FirstOrDefault(x => x.part_number_sat == partNumberSAT);
    }

    public int DeleteItemMaster(ITEM item)
    {
        _context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
        return _context.SaveChanges();
    }

    public int UpdateItemMaster(ITEM item)
    {
        _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
        return _context.SaveChanges();
    }

    public List<ITEM> GetAllItems()
    {
        return _context.ITEM.Where(x => x.combo_flag != true).ToList();
    }

    public List<ITEM> GetAllItemsAll()
    {
        return _context.ITEM.ToList();
    }

    public List<ItemDropDownModel> GetAllItemsToDropDownList()
    {
        return _context.ITEM.Where(x => x.active_flag == true && x.combo_flag != true)
            .Select(x => new ItemDropDownModel
            {
                part_number = x.part_number,
                part_description = x.part_description,
            }).ToList();
    }

    public List<ItemMasterModel> GetAllItems2()
    {
        return _context.ITEM.Select(x => new ItemMasterModel
        {
            part_number = x.part_number,
            part_description = x.part_description,
            unit_size = x.unit_size,
            part_type = x.part_type,
            buyer_division = x.buyer_division,
            part_iva = x.part_iva_purchase ?? "",
            part_ieps = x.part_ieps ?? "",
            part_number_sat = x.part_number_sat,
            unit_sat = x.unit_sat,
            weight_flag = x.weight_flag != null ? x.weight_flag.Value : false,
            active_flag = x.active_flag != null ? x.active_flag.Value : false
        }).ToList();
    }

    public List<ItemMasterModel> GetAllItemsActives()
    {
        return _context.ITEM.Where(x => x.active_flag == true).Select(x => new ItemMasterModel
        {
            part_number = x.part_number,
            part_description = x.part_description,
            unit_size = x.unit_size,
            part_type = x.part_type,
            buyer_division = x.buyer_division,
            part_iva = x.part_iva_purchase ?? "",
            part_ieps = x.part_ieps ?? "",
            part_number_sat = x.part_number_sat,
            unit_sat = x.unit_sat,
            weight_flag = x.weight_flag != null ? x.weight_flag.Value : false,
            active_flag = x.active_flag != null ? x.active_flag.Value : false
        }).ToList();
    }

    public List<ItemMasterModel> GetAllItemsActivesN()
    {
        return _context.ITEM.Where(x => x.active_flag == true && x.combo_flag != true).Select(x => new ItemMasterModel
        {
            part_number = x.part_number,
            part_description = x.part_description,
            unit_size = x.unit_size,
            part_type = x.part_type,
            buyer_division = x.buyer_division,
            part_iva = x.part_iva_purchase ?? "",
            part_ieps = x.part_ieps ?? "",
            part_number_sat = x.part_number_sat,
            unit_sat = x.unit_sat,
            weight_flag = x.weight_flag != null ? x.weight_flag.Value : false,
            active_flag = x.active_flag != null ? x.active_flag.Value : false
        }).ToList();
    }

    public ItemMasterModel GetItemPriceByPartNumber(string partNumber)
    {
        return _context.SALES_PRICE_CHANGE.Join(_context.ITEM, salePrice => salePrice.part_number, item => item.part_number, (salePrice, item) => new { salePrice, item })
                .Where(x => x.salePrice.part_number == partNumber && x.salePrice.status == 1)
                .Select(q => new ItemMasterModel
                {
                    part_number = q.item.part_number,
                    price = q.salePrice.sale_price_iva.ToString(),
                    part_description = q.item.part_description,
                    weight_flag = q.item.weight_flag ?? false
                }).FirstOrDefault();
    }

    public List<ItemPresentationModel> GetPresentations()
    {
        var model = _context.ITEM_PRESENTATION.Where(x => x.active_flag == true).Select(x => new ItemPresentationModel
        {
            PartNumber = x.part_number,
            Name = x.presentation_name
        }).ToList();

        return model;
    }

    public int ItemPresentation(string part_number)
    {
        var item = _context.ITEM.Where(w => w.part_number == part_number).FirstOrDefault();
        var presentation = _context.BARCODES.Where(w => w.part_barcode.Contains(part_number)).FirstOrDefault();
        if (presentation == null)
            return 0;
        return presentation.presentation_id ?? 0;
    }

    public string ItemPresentationPartnumber(string part_number)
    {
        var presentation = _context.BARCODES.Where(w => w.part_barcode.Contains(part_number)).FirstOrDefault();
        if (presentation == null)
            return part_number;
        return presentation.part_number;
    }

    public List<ItemMasterModel> GetItemsFlag(bool flag)
    {
        //var items = _context.ITEM.Where(x => x.active_flag == true && (x.combo_flag == false || x.combo_flag == null) && (flag ? x.extra_items_flag == flag : (x.extra_items_flag == flag || x.extra_items_flag == null)) && x.part_type == "HAWA").Select(x => new ItemMasterModel

        var items = _context.Database.SqlQuery<ItemMasterModel>(@"SELECT part_number, part_description FROM ITEM WHERE active_flag = 1 AND combo_flag = 0 AND extra_items_flag = 0").ToList();

        return items;
    }

    public List<ItemMasterModel> GetItemsFlagComboRaw()
    {
        return _context.ITEM.Where(x => x.active_flag == true && x.combo_flag != true && x.extra_items_flag != true).Select(x => new ItemMasterModel
        {
            part_number = x.part_number,
            part_description = x.part_description
        }).ToList();
    }

    public List<ItemDropDownModel> getItemsByLike(string Filter, int status)
    {
        try
        {
            var item = _context.ITEM
                .Join(_context.BARCODES, items => items.part_number, barcode => barcode.part_number, (items, barcode) => new { items, barcode })
                .Where(x => (x.items.part_number.Contains(Filter) || x.items.part_description.Contains(Filter) || x.barcode.part_barcode.Contains(Filter))
                && x.items.active_flag == true && x.items.combo_flag != true)
                .Select(x => new ItemDropDownModel
                {
                    part_number = x.items.part_number,
                    part_description = x.items.part_number + " ~ " + x.items.part_description,
                    presentation_id = 0,
                    presentation_flag = false
                }).Distinct().ToList();

            if (status == 1)
            {
                var item_presentation = _context.ITEM_PRESENTATION.Where(x => x.active_flag == true && (x.part_number.Contains(Filter) || x.presentation_name.Contains(Filter) || x.presentation_code.Contains(Filter)))
                   .Select(x => new ItemDropDownModel
                   {
                       part_number = x.presentation_code,
                       part_description = x.part_number + " PRESENTACION (" + x.presentation_code + ") ~ " + x.presentation_name,
                       presentation_id = x.presentation_id,
                       presentation_flag = true
                   }).ToList();

                item.AddRange(item_presentation);
            }
            return item;
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return null;
        }
    }

    public int UpdateItemIVABySupplier(int supplier, string uuser)
    {
        try
        {
            //Busca información del proveedor
            string part_iva_supplier = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier).FirstOrDefault().part_iva_purchase_supplier;
            string supplier_acreedor = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier && w.supplier_type == "Acreedor").FirstOrDefault().supplier_type;

            if (supplier_acreedor != null || supplier_acreedor != "")
                return 4; //Retorna exitoso directamente porque acreedores no tiene productos relacionados (no deberia)

            if (part_iva_supplier == null || part_iva_supplier == "" || part_iva_supplier == "N/A")
                return 1; //Asignar un iva Correcto

            var items = _context.ITEM_SUPPLIER.Where(w => w.supplier_id == supplier && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == w.supplier_part_number)).ToList();
            if (items.Count() == 0)
                return 2; //El proveedor no tiene productos relacionados

            var itemIVA = _context.ITEM_SUPPLIER.Join(_context.ITEM, IS => IS.part_number, I => I.part_number, (IS, I) => new { IS, I })
                .Join(_context.MA_TAX, II => II.I.part_iva_purchase, M => M.tax_code, (II, M) => new { II, M })
                .Where(w => w.II.IS.supplier_id == supplier && w.M.tax_type == "1" && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == w.II.IS.supplier_part_number)).ToList();
            if (itemIVA.Count() == 0)
                return 3; //El proveedor no tiene productos con IVA

            //Cambiar el IVA a todos los productos con IVA
            itemIVA.ForEach(f => { f.II.I.part_iva_purchase = part_iva_supplier; f.II.I.udate = DateTime.Now; f.II.I.uuser = uuser; });
            _context.SaveChanges();

            return 4;// Exitoso

        }
        catch (Exception e)
        {
            var msg = e.Message;
            return 0; //Error
        }
    }

    public ItemMasterModel GetInformationProduct(string part_number)
    {
        var product = _context.ITEM.Where(w => w.part_number == part_number)
            .Select(s => new ItemMasterModel
            {
                part_number = s.part_number,
                part_description = s.part_description,
                part_type = s.part_type,
                unit_size = s.unit_size
            }).FirstOrDefault();
        var sale = _context.SALES_PRICE_CHANGE.Where(w => w.part_number == part_number && w.status == 1).FirstOrDefault();
        if (sale != null)
            product.price = sale.sale_price_iva.ToString() ?? "0";
        else
            product.price = "0";

        var cost = _context.ITEM_SUPPLIER.OrderByDescending(o => o.cdate).Where(w => w.part_number == part_number).FirstOrDefault();
        if (cost != null)
        {
            var costReal = _context.PURCHASE_BASE_COST.OrderByDescending(o => o.cdate).Where(w => w.supplier_part_number == cost.supplier_part_number && w.status == 1).FirstOrDefault();
            if (costReal != null)
                product.cost = costReal.base_cost.ToString() ?? "0";
            else
                product.cost = "0";
        }
        product.CodesPackages = _context.CODES_PACKAGES.Join(_context.ITEM, its => its.part_number, it => it.part_number, (its, it) => new { its, it })
            .Where(w => w.its.part_number_origin == part_number && w.its.active_flag == true)
            .Select(s => new CodesPackagesModel
            {
                PartNumberOrigin = s.its.part_number_origin,
                PartNumber = s.its.part_number,
                PartDescription = s.it.part_description,
                Quantity = s.its.quantity,
                Cost = s.its.cost
            }).ToList();

        return product;
    }

    public List<ItemModel> SearchItemLike(string part_number)
    {
        return _context.ITEM
            .Join(_context.BARCODES, document => document.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
            .Where(x => (x.document.part_number.Contains(part_number) || x.document.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
            && x.document.active_flag == true)
            .Select(x => new ItemModel
            {
                Description = x.document.part_description,
                PathNumber = x.document.part_number
            }).Distinct().ToList();
    }

    public List<ItemStockHistory> GetStockHistory(string IP, string Date)
    {
        var model = _context.Database.SqlQuery<ItemStockHistory>(@"
            SELECT * FROM OPENQUERY([" + IP + "],'SELECT * FROM [DFL_SAI_SITE].[dbo].[fnc_Stock_History](''" + Date + "'')' ) AS OQ ").ToList();

        return model;
    }
    public List<ItemBlockedModel> GetAllStoresItemBlocked(string part_number)
    {
        return _context.ITEM_BLOCKED.Where(x => x.part_number == part_number && x.active_flag == true)
             .Select(s => new ItemBlockedModel
             {
                 part_number = s.part_number,
                 site_code = s.site_code,
                 active_flag = s.active_flag
             }).ToList();
    }
    public int AddItemBlocked(string PartNumber, string Sites, string user)
    {
        try
        {
            var pagesArray = Sites.Split(',');
            var ExistList = _context.ITEM_BLOCKED.Where(x => x.part_number == PartNumber).ToList();
            if (ExistList.Count() != 0)
            {

                foreach (var item in ExistList)
                {
                    item.active_flag = false;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }

            }

            foreach (var item in pagesArray)
            {
                var Exist = _context.ITEM_BLOCKED.Where(x => x.part_number == PartNumber && x.site_code == item).ToList();
                if (Exist.Count == 0)
                {
                    _context.ITEM_BLOCKED.Add(new ITEM_BLOCKED() { part_number = PartNumber, site_code = item, active_flag = true, cdate = DateTime.Now, cuser = user,program_id= "PURCH005" });
                    _context.SaveChanges();
                }
                else
                {
                    foreach (var items in Exist)
                    {
                        items.active_flag = true;
                        items.uuser = user;
                        items.udate = DateTime.Now;
                        _context.Entry(items).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }
                }
            }
            return 1;
            
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return 0;
        }

    }
}