﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Item
{
    public class ItemSpecsRepository
    {
        private PRTSAIEntities _context;
        public ItemSpecsRepository()
        {
            _context = new PRTSAIEntities();
        }
        public ItemSpecs getSpecsByPartNumber(string PartNumber)
        {
            return _context.ITEM_SPECS.Where(x => x.part_number == PartNumber).Select(x => new ItemSpecs
            {
                part_number = x.part_number,
                packing_of_size = x.packing_of_size,
                pallet_of_size = x.pallet_of_size,
                weight_of_packing = x.weight_of_packing,
                dun14 = x.dun14,
                gr_storage_location = x.gr_storage_location
            }).SingleOrDefault();
        }
        public int AddItemSpecs(ITEM_SPECS itemSpecs)
        {
            try
            {
                _context.ITEM_SPECS.Add(itemSpecs);
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int UpdateItemSpecs(ITEM_SPECS item)
        {
            _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }
    }
}
