﻿using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Projects
{
    public class ProjectHeaderRepository
    {
        private PRTSAIEntities _context;

        public ProjectHeaderRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ProjectHeaderModel> GetAllProjectBasic()
        {
            return _context.PROJECT_HEADER
                .Select(s => new ProjectHeaderModel
                {
                    id_project = s.id_project,
                    title = s.title
                }).ToList();
        }


        public List<ProjectHeaderModel> GetActiveProjectBasic()
        {
            return _context.PROJECT_HEADER.Where(w => w.status == 0)
                .Select(s => new ProjectHeaderModel
                {
                    id_project = s.id_project,
                    title = s.title
                }).ToList();
        }
        public int AddProjectHeader(PROJECT_HEADER model)
        {         
            try
            {
                var x = _context.PROJECT_HEADER.Add(model);
                _context.SaveChanges();
                return model.id_project;
            }
            catch (Exception)
            {
                return 0;
            }
        }
       
        public bool CancelHeaderByError(int id)
        {
            try
            {
                var header = _context.PROJECT_HEADER.Where(x => x.id_project == id).FirstOrDefault();
                header.status = 8;
                _context.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        public List<ProjectHeaderModel> GetHeaderByUser(string user)
        {
            return _context.PROJECT_HEADER.Where(x => (x.cuser == user && x.status == 0) || (x.cuser == user && x.status == 1)).Select(s =>
             new ProjectHeaderModel()
             {
                 compromise_date = s.compromise_date,
                 title = s.title,
                 description = s.description,
                 estimate = s.estimate,
                 id_category = s.id_category,
                 id_project = s.id_project,
                 estimate_usd = s.estimate_usd ?? 0,
                 
             }).ToList();      
        }
        public ProjectHeaderModel GetHeaderById(int id)
        {
            return _context.PROJECT_HEADER.Where(x => x.id_project == id).Select(s =>
                new ProjectHeaderModel
                {
                    compromise_date = s.compromise_date,
                    title = s.title,
                    description = s.description,
                    estimate = s.estimate,
                    id_category = s.id_category,
                    id_project = s.id_project,
                    estimate_usd = s.estimate_usd ?? 0,
                 
                }).FirstOrDefault();      
        }
        public ProjectHeaderModel GetDetailsByHeader(int id)
        {
            return _context.PROJECT_HEADER.Where(x => x.id_project == id).Select(s =>
             new ProjectHeaderModel()
             {
                 compromise_date = s.compromise_date,
                 title = s.title,
                 description = s.description,
                 estimate = s.estimate,
                 id_category = s.id_category,
                 id_project = s.id_project,
                 estimate_usd = s.estimate_usd ?? 0,
             }).FirstOrDefault();
        }
        public bool UpdateStatusHeaderClose(int id , int status, decimal cost_mxn, decimal cost_usd  , string user)
        {
            try
            {
                var header = _context.PROJECT_HEADER.Where(x => x.id_project == id).FirstOrDefault();
                    header.status = status;
                    header.total_cost = cost_mxn;
                    header.total_cost_usd = cost_usd;
                    header.uuser = user;
                    header.end_date = DateTime.Now;
                    header.udate = DateTime.Now;
                    header.program_id = "PROJ001.cshtml";
                    _context.SaveChanges();              
                return true;           
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool UpdateStatusHeaderCancel(int id, int status, string user)
        {
            try
            {
                var header = _context.PROJECT_HEADER.Where(x => x.id_project == id).FirstOrDefault();
                header.status = status;
                header.uuser = user;
                header.end_date = DateTime.Now;
                header.udate = DateTime.Now;
                header.program_id = "PROJ001.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateHeader(ProjectHeaderModel model)
        {
            try
            {
                var modelHeader = _context.PROJECT_HEADER.FirstOrDefault(x => x.id_project == model.id_project);
                if (modelHeader != null)
                {
                    modelHeader.compromise_date = model.compromise_date;
                    modelHeader.estimate = model.estimate;
                    modelHeader.title = model.title;
                    modelHeader.description = model.description;
                    modelHeader.estimate_usd = model.estimate_usd;
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            { 
            }
            return false;

        }
    }
}
