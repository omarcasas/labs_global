﻿using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Projects
{
    public class ProjectItemHistoryRepository
    {
        private PRTSAIEntities _context;

        public ProjectItemHistoryRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ProjectItemHistoryHeaderModel> GetItemHistoryById(int id_project)
        {
            return _context.PROJECT_HEADER
                .Join(_context.PROJECT_LABEL, ph => ph.id_project, pl => pl.id_project, (ph, pl) => new { ph, pl })
                .Join(_context.PROJECT_CATEGORY, pl2 => pl2.ph.id_category, pc => pc.id_category, (pl2, pc) => new { pl2, pc })
                .Join(_context.USER_MASTER, pl3 => pl3.pl2.pl.emp_no, us => us.emp_no, (pl3, us) => new { pl3, us })
                .Where(w => w.pl3.pl2.ph.id_project == id_project && w.pl3.pl2.pl.id_project == id_project && w.pl3.pl2.pl.active_label == true)
                .Select(s => new ProjectItemHistoryHeaderModel
                {
                    id_proyect = s.pl3.pl2.ph.id_project,
                    description = s.pl3.pl2.ph.description,
                    title = s.pl3.pl2.ph.title,
                    category = s.pl3.pc.description,
                    estimate = s.pl3.pl2.ph.estimate,
                    id_category = s.pl3.pl2.ph.id_category,
                    id_label = s.pl3.pl2.pl.id_label_project,
                    label_name = s.pl3.pl2.pl.id_label_project_default,
                    label_emp_no = s.pl3.pl2.pl.emp_no,
                    compromise_date = s.pl3.pl2.ph.compromise_date,
                    start_date = s.pl3.pl2.ph.start_date,
                    end_date = s.pl3.pl2.ph.end_date,
                    project_status = s.pl3.pl2.ph.status,
                    label_emp_no_user_name = s.us.user_name,
                    estimate_usd = s.pl3.pl2.ph.estimate_usd ?? 0,
                }).ToList();
        }

        public List<ProjectItemHistoryModel> GetLastComentByLabel(int id_label)
        {
            var items = _context.PROJECT_ITEM
                .Where(w => w.id_label_project == id_label)
                .Select(s => new ProjectItemHistoryModel
                {
                    id_item = s.item_id,
                    item_description = s.description,
                    item_status = s.status,
                    id_label = id_label,
                }
            ).ToList();

            foreach (var i in items)
            {
                var history = _context.PROJECT_ITEM_HISTORY
                    .Join(_context.USER_MASTER, pih => pih.cuser, us => us.user_name, (pih, us) => new { pih, us })
                    .Where(w => w.pih.id_item == i.id_item)
                    .OrderByDescending(o => o.pih.id_history)
                    .Select(s => new ProjectItemHistoryModel
                    {
                        user_photo = s.us.photo,
                        user_name = s.us.user_name,
                        user_comment = s.pih.commentary,
                        post_date = s.pih.cdate,
                        user_full_name = s.us.first_name + " " + s.us.last_name
                    }).FirstOrDefault();

                if (history != null)
                {
                    i.user_photo = history.user_photo;
                    i.user_full_name = history.user_full_name;
                    i.user_comment = history.user_comment;
                    i.post_date = history.post_date;
                    i.user_full_name = history.user_full_name;
                    i.user_name = history.user_name;
                    i.post_numbers = GetPostNumberByItem(i.id_item);
                }
            }
            return items;
        }

        public int GetPostNumberByItem(int id_item)
        {
            return _context.PROJECT_ITEM_HISTORY.Count(c => c.id_item == id_item);
        }

        public string GetProjectHeaderUser(int id_proyect)
        {
            return _context.PROJECT_HEADER.Where(w => w.id_project == id_proyect).Select(s => s.cuser).FirstOrDefault();
        }

        public string GetCommentsPdfByHistory(int id_history_detail)
        {
            return _context.PROJECT_ITEM_HISTORY_DETAIL.Where(w => w.id_history_detail == id_history_detail).Select(s => s.document_pdf).FirstOrDefault();
        }

        public List<ProjectItemHistoryModel> GetLastComentBasicByLabel(int id_label)
        {
            return _context.PROJECT_ITEM
                .Where(w => w.id_label_project == id_label)
                .Select(s => new ProjectItemHistoryModel
                {
                    id_item = s.item_id,
                    item_description = s.description,
                    item_status = s.status,
                    id_label = id_label
                }).ToList();
        }

        public List<ProjectItemHistoryModel> GetAllCommentsByItem(int id_item)
        {
            {
                return _context.PROJECT_ITEM
                    .Join(_context.PROJECT_ITEM_HISTORY, pi => pi.item_id, pih => pih.id_item, (pi, pih) => new { pi, pih })
                    .Join(_context.USER_MASTER, pih2 => pih2.pih.cuser, us => us.user_name, (pih2, us) => new { pih2, us })
                    .Where(w => w.pih2.pi.item_id == id_item)
                    .OrderByDescending(o => o.pih2.pih.id_history)
                    .Select(s => new ProjectItemHistoryModel
                    {
                        id_item = s.pih2.pi.item_id,
                        item_description = s.pih2.pi.description,
                        start_date = s.pih2.pi.start_date,
                        end_date = s.pih2.pi.end_date,
                        real_end_date = s.pih2.pi.real_end_date,
                        real_start_date = s.pih2.pi.real_start_date,
                        estimate = s.pih2.pi.estimate,
                        total_cost = s.pih2.pi.total_cost,
                        user_photo = s.us.photo,
                        user_name = s.us.user_name,
                        user_comment = s.pih2.pih.commentary,
                        post_date = s.pih2.pih.cdate,
                        id_label = s.pih2.pi.id_label_project,
                        item_status = s.pih2.pi.status,
                        id_history = s.pih2.pih.id_history,
                        user_full_name = s.us.first_name + " " + s.us.last_name,
                        estimate_usd = s.pih2.pi.estimate_usd ?? 0,
                        total_cost_usd = s.pih2.pi.total_cost_usd ?? 0,
                                         
                    }).ToList();
            }
        }

        public List<ProjectItemHistoryModel> GetAllCommentsBasicByItem(int id_item)
        {
            {
                return _context.PROJECT_ITEM
                    .Where(w => w.item_id == id_item)
                    .Select(s => new ProjectItemHistoryModel
                    {
                        id_item = s.item_id,
                        item_description = s.description,
                        start_date = s.start_date,
                        end_date = s.end_date,
                        real_end_date = s.real_end_date,
                        real_start_date = s.real_start_date,
                        estimate = s.estimate,
                        total_cost = s.total_cost,
                        id_label = s.id_label_project,
                        item_status = s.status,
                        estimate_usd = s.estimate_usd ?? 0,
                        total_cost_usd = s.total_cost_usd ?? 0,
                    }).ToList();
            }
        }

        public List<ProjectItemHistoryDetail> GetDocumentsByHistory(int id_history)
        {
            return _context.PROJECT_ITEM_HISTORY_DETAIL.Where(w => w.id_history == id_history)
                .Select(s => new ProjectItemHistoryDetail
                {
                    id_history_detail = s.id_history_detail,
                    document_image = s.document_image,
                    document_pdf = s.document_pdf != null ? "PDF" : null,
                    id_history = s.id_history
                }).ToList();
        }

        public int InsertComment(PROJECT_ITEM_HISTORY model)
        {

            _context.PROJECT_ITEM_HISTORY.Add(model);
            try
            {
                _context.SaveChanges();
                return model.id_history;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public bool InsertAttached(PROJECT_ITEM_HISTORY_DETAIL model)
        {
            _context.PROJECT_ITEM_HISTORY_DETAIL.Add(model);
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }
    }
}
