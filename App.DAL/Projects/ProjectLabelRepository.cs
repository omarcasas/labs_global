﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Projects;
using System.Data.SqlClient;

namespace App.DAL.Projects
{
    public class ProjectLabelRepository
    {
        private PRTSAIEntities _context = new PRTSAIEntities();
        public List<ProjectDescriptionModel> GetAllLabelsDefaultByCategory(int id)
        {
            return _context.PROJECT_LABEL_DEFAULT
                .Where(i => i.id_category == id && i.active_flag == true).ToList().Select(s => new ProjectDescriptionModel
                {
                    description = s.description
                }).ToList();
        }
        public bool AddLabelDefault(PROJECT_LABEL_DEFAULT model)
        {
            try
            {
                _context.PROJECT_LABEL_DEFAULT.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public int AddLabel(PROJECT_LABEL model)
        {
            try
            {
                _context.PROJECT_LABEL.Add(model);
                _context.SaveChanges();
                return model.id_label_project;
            }
            catch (Exception e)
            {

                return 0;
            }
        }
        public bool AddLabelUser(PROJECT_LABEL_USER model)
        {
            try
            {
                _context.PROJECT_LABEL_USER.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public List<RTPProjectLabelModel> ReadyToPayLabels(int projectId, string userEmpNo, string userName)
        {
            var q = _context.PROJECT_LABEL
                .Where(label => label.active_label && label.id_project == projectId && (label.emp_no == userEmpNo ||
                                                                                        label.PROJECT_LABEL_USER.Any(lu=> lu.emp_no==userEmpNo)
                                                                                        || label.PROJECT_HEADER.cuser == userName))
                .Select(label => new RTPProjectLabelModel
                {
                    IdLabelProject = label.id_label_project,
                    IdProject = label.id_project,
                    IdLabelProjectDefault = label.id_label_project_default,
                    Estimate = label.estimate,
                    TotalCost = label.total_cost,
                    EmpNo = label.emp_no,
                    EmpName = _context.USER_MASTER.Where(e => e.emp_no == label.emp_no)
                        .Select(e=> e.first_name + " " + e.last_name).FirstOrDefault(),
                    
                    ActiveLabel = label.active_label,
                    ActualTotalCost = _context.XML_HEADER
                        .Where(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.label_id == label.id_label_project))
                        .Where(x => x.currency=="MXN")
                        .Sum(z => z.invoice_type=="E" ? -z.total_amount : z.total_amount) ?? 0m,
                    ActualTotalCostUsd = _context.XML_HEADER
                                          .Where(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.label_id == label.id_label_project))
                                          .Where(x => x.currency=="USD")
                                          .Sum(z =>  z.invoice_type=="E" ? -z.total_amount : z.total_amount) ?? 0m,
                    ActualInvoiceCount = _context.XML_HEADER.Count(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.label_id == label.id_label_project)),
                    
                    Items = _context.PROJECT_ITEM
                        .Where(e => e.id_label_project == label.id_label_project && (e.status == 1 || e.status==2) )
                        .Select(e =>
                            new ProjectItemModel
                            {
                                IdItem = e.item_id,
                                IdLabelProject = e.id_label_project,
                                StartDate = e.start_date,
                                EndDate = e.end_date,
                                RealStartDate = e.real_start_date,
                                RealEndDate = e.real_end_date,
                                Description = e.description,
                                Status = e.status,
                                Estimate = e.estimate,
                                TotalCost = e.total_cost,
                                ConsecutiveItem = e.consecutive_item,
                                ActualInvoiceCount =_context.XML_HEADER.Count(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.label_id == label.id_label_project)),
                                estimate_usd = e.estimate_usd??0,
                                total_cost_usd = e.total_cost_usd??0,
                            }),
                    estimate_usd = label.estimate_usd??0,
                    total_cost_usd = label.total_cost_usd??0
                });
            var r = q.ToList();
            return r;
        }

        public List<ProjectUserNotification> GetAllUserEmail(int IdProject, int IdLabel, bool allUsers)
        {
            string select = "";
            if (!allUsers) { select = "select DISTINCT pl.id_label_project,pl.id_label_project_default,um.emp_no, user_name ,first_name,last_name,email"; }
            else { select = "select DISTINCT um.emp_no, user_name ,first_name,last_name,email"; }

            string query = $@"{select} from PROJECT_LABEL pl
                right join PROJECT_LABEL_USER plu on plu.id_label_project = pl.id_label_project
                right join PROJECT_CATEGORY_USER pcu on pcu.id_project = pl.id_project
                inner join USER_MASTER um on um.emp_no = plu.emp_no or um.emp_no = pl.emp_no or um.user_name = pl.cuser
                where pl.id_project = @project";
            if (!allUsers)
            {
                query += " AND pl.id_label_project = @label";
                var info = _context.Database.SqlQuery<ProjectUserNotification>(query,
              new SqlParameter("@project", IdProject),
              new SqlParameter("@label", IdLabel)).ToList();
                if (info != null && info.Count() > 0)
                {
                    var user = info.Select(s => s.email).Distinct().ToList();
                    string emails = "";
                    if (user.Count == 1)
                    {
                        emails = user[0];
                    }
                    else
                    {
                        var lastEmail = user.Last();
                        foreach (var item in user)
                        {

                            if (lastEmail.Equals(item))
                            {
                                emails += item;
                            }
                            else
                            {
                                if (emails == "")
                                    emails = item + ",";
                                else
                                    emails = emails + item + ",";
                            }
                        }
                    }
                    info[0].all_emails = emails;
                }
                return info;
            }
            else
            {
                var info = _context.Database.SqlQuery<ProjectUserNotification>(query,
                new SqlParameter("@project", IdProject)).ToList();

                if (info != null && info.Count() > 0)
                {
                    var user = info.Select(s => s.email).Distinct().ToList();
                    string emails = "";
                    if (user.Count == 1)
                    {
                        emails = user[0];
                    }
                    else
                    {
                        var lastEmail = user.Last();
                        foreach (var item in user)
                        {

                            if (lastEmail.Equals(item))
                            {
                                emails += item;
                            }
                            else
                            {
                                if (emails == "")
                                    emails = item + ",";
                                else
                                    emails = emails + item + ",";
                            }
                        }
                    }
                    info[0].all_emails = emails;
                }
                return info;
            }

        }

        public ProjectItemHistoryHeaderModel GetLabelInfo(int id_label_label)
        {
            return _context.PROJECT_LABEL
                .Join(_context.USER_MASTER, pl => pl.emp_no, us => us.emp_no, (pl, us) => new { pl, us })
                .Where(w => w.pl.id_label_project == id_label_label)
                .Select(s => new ProjectItemHistoryHeaderModel
                {
                    estimate = s.pl.estimate,
                    total_cost = s.pl.total_cost,
                    label_name = s.pl.id_label_project_default,
                    label_emp_no_user_name = s.us.first_name + " " + s.us.last_name,
                    description = s.us.user_name
                }).FirstOrDefault();
        }

        public bool existLabelDefault(int id_category, string description)
        {
            return _context.PROJECT_LABEL_DEFAULT.Any(x => x.id_category == id_category && x.description == description);
        }
        public bool updateLabelDefault(int id_category, string description, string user)
        {
            try
            {
                var label = _context.PROJECT_LABEL_DEFAULT.Where(x => x.id_category == id_category && x.description == description).FirstOrDefault();
                label.active_flag = true;
                label.uuser = user;
                label.udate = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<ProjectUserNotification> GetAllUserLabel(int id_proyect_label)
        {
            var x = _context.PROJECT_LABEL
                .Join(_context.PROJECT_LABEL_USER, pl => pl.id_label_project, plu => plu.id_label_project, (pl, plu) => new { pl, plu })
                .Join(_context.USER_MASTER, plu2 => plu2.plu.emp_no, us => us.emp_no, (plu2, us) => new { plu2, us })
                .Where(w => w.plu2.pl.id_label_project == id_proyect_label && w.plu2.plu.active == true)
                .Select(s => new ProjectUserNotification
                {
                    user_name = s.us.user_name,
                    id_label_project_default = s.plu2.pl.id_label_project_default
                }).ToList();
            return x;
        }

        public List<ProjectUserNotification> GetAllUserLeaderLabel(int id_proyect)
        {
            var x = _context.PROJECT_LABEL
                .Join(_context.USER_MASTER, pl => pl.emp_no, us => us.emp_no, (pl, us) => new { pl, us })
                .Where(w => w.pl.id_project == id_proyect)
                .Select(s => new ProjectUserNotification
                {
                    id_label_project = s.pl.id_label_project,
                    user_name = s.us.user_name,
                    id_label_project_default = s.pl.id_label_project_default
                }).ToList();
            return x;
        }
        public List<ProjectLabelModel> GetLabelById(int id)
        {
            return _context.PROJECT_LABEL
                .Join(_context.USER_MASTER, pl => pl.emp_no, us => us.emp_no, (pl, um) => new { pl, um })
                .Where(x => x.pl.id_project == id && x.pl.active_label == true).Select(s => new ProjectLabelModel
                {
                    EmpNo = s.pl.emp_no,
                    Description = s.pl.id_label_project_default,
                    userLeaderName = s.um.first_name + " " + s.um.last_name,
                    IdLabelProject = s.pl.id_label_project
                }).ToList();
        }
        public List<string> GetUsersLabelById(int id)
        {
            return _context.PROJECT_LABEL_USER
                .Join(_context.USER_MASTER, pl => pl.emp_no, us => us.emp_no, (pl, us) => new { pl, us })
                .Where(x => x.pl.id_label_project == id && x.pl.active == true)
                .Select(s => s.pl.emp_no).ToList();
        }
        public List<string> GetNameUsersLabelById(int id)
        {
            return _context.PROJECT_LABEL_USER
                .Join(_context.USER_MASTER, pl => pl.emp_no, us => us.emp_no, (pl, us) => new { pl, us })
                .Where(x => x.pl.id_label_project == id && x.pl.active == true)
                .Select(s => s.us.first_name + " " + s.us.last_name).ToList();
        }
        public bool AddLabelHeader(PROJECT_LABEL model)
        {
            try
            {
                _context.PROJECT_LABEL.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool DeleteLabelByHeader(string desc, int idProj, string user)
        {
            try
            {
                var label = _context.PROJECT_LABEL.Where(x => x.id_label_project_default == desc && x.id_project == idProj).FirstOrDefault();
                label.active_label = false;
                label.udate = DateTime.Now;
                label.uuser = user;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<ProjectUserNotification> GetUserAllEmail(int IdProject)
        {
            string query = $@"select DISTINCT um.emp_no, um.user_name, um.first_name, um.last_name, um.email from PROJECT_LABEL pl
				right join PROJECT_CATEGORY_USER pcu on pcu.id_project = pl.id_project
                right join PROJECT_LABEL_USER plu on plu.id_label_project = pl.id_label_project
                inner join USER_MASTER um on um.emp_no = plu.emp_no or um.emp_no = pl.emp_no or um.user_name = pl.cuser
                where plu.active = 1 and pl.active_label = 1 and pcu.id_project = @project and pl.id_project = @project;";
            var info = _context.Database.SqlQuery<ProjectUserNotification>(query,
          new SqlParameter("@project", IdProject)).ToList();
            if (info != null && info.Count() > 0)
            {
                var user = info.Select(s => s.email).Distinct().ToList();
                string emails = "";
                if (user.Count == 1)
                {
                    emails = user[0];
                }
                else
                {
                    var lastEmail = user.Last();
                    foreach (var item in user)
                    {

                        if (lastEmail.Equals(item))
                        {
                            emails += item;
                        }
                        else
                        {
                            if (emails == "")
                                emails = item + ",";
                            else
                                emails = emails + item + ",";
                        }
                    }
                }
                info[0].all_emails = emails;
            }
            return info;
        }
        public bool ExistUserInLabel(int idLabel , string empNo)
        {
            return _context.PROJECT_LABEL_USER.Any(x => x.id_label_project == idLabel && x.emp_no == empNo);
        }
        public bool UpdateLabelUserFlag(int idLabel , string empNo , string cuser)
        {
            try
            {
                var user = _context.PROJECT_LABEL_USER.Where(x => x.id_label_project == idLabel && x.emp_no == empNo).FirstOrDefault();
                user.active = true;
                user.uuser = cuser;
                user.udate = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool UpdateAllUsersLabel(int idLabel)
        {
            try
            {
                _context.Database.ExecuteSqlCommand(@"update PROJECT_LABEL_USER set active = 0 where id_label_project = " + idLabel);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}