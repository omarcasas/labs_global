﻿using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace App.DAL.Projects
{
    public class ProjectReportRepository
    {
        private readonly PRTSAIEntities _context;

        public ProjectReportRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ProjectModel> GetProjectsReport(string user)
        {
            try
            {
                var model = _context.PROJECT_HEADER
                    .Where(x => x.cuser == user)
                    .Select(x => new ProjectModel
                    {
                        ProjectId = x.id_project,
                        Title = x.title,
                        Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                        Category = x.PROJECT_CATEGORY.description,
                        StartDate = x.start_date,
                        EndDate = x.end_date,
                        CompromiseDate = x.compromise_date,
                        EstimateCost = x.estimate,
                        TotalCost = x.total_cost,
                        estimate_usd = x.estimate_usd??0,
                        total_cost_usd = x.total_cost_usd??0
                    }).ToList();

                return model;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<ProjectModel> GetProjectsReport(int CategoryId, string LabelId, DateTime? StartDate, DateTime? EndDate, string User)
        {
            try
            {
                if (CategoryId == 0 && LabelId == "" && StartDate.ToString() == "" && EndDate.ToString() == "")
                {
                    var model = _context.PROJECT_HEADER
                        .Where(x => x.cuser == User)
                        .Select(x => new ProjectModel
                        {
                            ProjectId = x.id_project,
                            Title = x.title,
                            Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                            Category = x.PROJECT_CATEGORY.description,
                            StartDate = x.start_date,
                            EndDate = x.end_date,
                            CompromiseDate = x.compromise_date,
                            EstimateCost = x.estimate,
                            TotalCost = x.total_cost,
                            estimate_usd = x.estimate_usd??0,
                            total_cost_usd = x.total_cost_usd??0
                        }).ToList();

                    return model;
                }

                else if (CategoryId == 0 & LabelId == "" & StartDate.ToString() != "" & EndDate.ToString() != "")
                {
                    var model = _context.PROJECT_HEADER
                        .Where(x => x.cuser == User && DbFunctions.TruncateTime(x.start_date) >= DbFunctions.TruncateTime(StartDate) && DbFunctions.TruncateTime(x.start_date) <= DbFunctions.TruncateTime(EndDate))
                        .Select(x => new ProjectModel
                        {
                            ProjectId = x.id_project,
                            Title = x.title,
                            Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                            Category = x.PROJECT_CATEGORY.description,
                            StartDate = x.start_date,
                            EndDate = x.end_date,
                            CompromiseDate = x.compromise_date,
                            EstimateCost = x.estimate,
                            TotalCost = x.total_cost,
                            estimate_usd = x.estimate_usd??0,
                            total_cost_usd = x.total_cost_usd??0
                        }).ToList();

                    return model;
                }

                else if (CategoryId != 0 & LabelId == "" & StartDate.ToString() == "" & EndDate.ToString() == "")
                {
                    var model = _context.PROJECT_HEADER
                        .Where(x => x.id_category == CategoryId && x.cuser == User)
                        .Select(x => new ProjectModel
                        {
                            ProjectId = x.id_project,
                            Title = x.title,
                            Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                            Category = x.PROJECT_CATEGORY.description,
                            StartDate = x.start_date.HasValue ? x.start_date : null,
                            EndDate = x.end_date.HasValue ? x.end_date : null,
                            CompromiseDate = x.compromise_date,
                            EstimateCost = x.estimate,
                            TotalCost = x.total_cost,
                            estimate_usd = x.estimate_usd??0,
                            total_cost_usd = x.total_cost_usd??0
                        }).ToList();

                    return model;
                }

                else if (CategoryId != 0 & LabelId == "" & StartDate.ToString() != "" & EndDate.ToString() != "")
                {
                    var model = _context.PROJECT_HEADER
                        .Where(x => x.id_category == CategoryId && DbFunctions.TruncateTime(x.start_date) >= DbFunctions.TruncateTime(StartDate) && DbFunctions.TruncateTime(x.start_date) <= DbFunctions.TruncateTime(EndDate) && x.cuser == User)
                        .Select(x => new ProjectModel
                        {
                            ProjectId = x.id_project,
                            Title = x.title,
                            Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                            Category = x.PROJECT_CATEGORY.description,
                            StartDate = x.start_date.HasValue ? x.start_date : null,
                            EndDate = x.end_date.HasValue ? x.end_date : null,
                            CompromiseDate = x.compromise_date,
                            EstimateCost = x.estimate,
                            TotalCost = x.total_cost,
                            estimate_usd = x.estimate_usd??0,
                            total_cost_usd = x.total_cost_usd??0,
                        }).ToList();

                    return model;
                }

                else if (CategoryId != 0 & LabelId != "" & StartDate.ToString() == "" & EndDate.ToString() == "")
                {
                    var model = _context.PROJECT_HEADER
                        .Where(x => x.id_category == CategoryId && x.PROJECT_LABEL.Any(y => y.id_label_project_default == LabelId) && x.cuser == User)
                        .Select(x => new ProjectModel
                        {
                            ProjectId = x.id_project,
                            Title = x.title,
                            Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                            Category = x.PROJECT_CATEGORY.description,
                            StartDate = x.start_date.HasValue ? x.start_date : null,
                            EndDate = x.end_date.HasValue ? x.end_date : null,
                            CompromiseDate = x.compromise_date,
                            EstimateCost = x.estimate,
                            TotalCost = x.total_cost,
                            estimate_usd = x.estimate_usd??0,
                            total_cost_usd = x.total_cost_usd??0,
                        }).ToList();

                    return model;
                }

                else if (CategoryId != 0 & LabelId != "" & StartDate.ToString() != "" & EndDate.ToString() != "")
                {
                    var model = _context.PROJECT_HEADER
                        .Where(x => x.id_category == CategoryId && x.PROJECT_LABEL.Any(y => y.id_label_project_default == LabelId) && DbFunctions.TruncateTime(x.start_date) >= DbFunctions.TruncateTime(StartDate) && DbFunctions.TruncateTime(x.start_date) <= DbFunctions.TruncateTime(EndDate) && x.cuser == User)
                        .Select(x => new ProjectModel
                        {
                            ProjectId = x.id_project,
                            Title = x.title,
                            Status = x.status == 0 ? "Iniciado" : x.status == 1 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                            Category = x.PROJECT_CATEGORY.description,
                            StartDate = x.start_date.HasValue ? x.start_date : null,
                            EndDate = x.end_date.HasValue ? x.end_date : null,
                            CompromiseDate = x.compromise_date,
                            EstimateCost = x.estimate,
                            TotalCost = x.total_cost,
                            estimate_usd = x.estimate_usd??0,
                            total_cost_usd = x.total_cost_usd??0,
                        }).ToList();

                    return model;
                }

                else
                    return null;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<ItemModel> GetProjectDetailReport(int ProjectId, string LabelId, string User)
        {
            if (!string.IsNullOrEmpty(LabelId))
            {
                var model = _context.PROJECT_ITEM
                    .Where(x => x.PROJECT_LABEL.id_project == ProjectId && x.PROJECT_LABEL.PROJECT_HEADER.cuser == User && x.PROJECT_LABEL.id_label_project_default == LabelId && x.PROJECT_LABEL.active_label == true)
                    .Select(x => new ItemModel
                    {
                        ItemId = x.item_id,
                        ItemDescription = x.description,
                        LabelDescription = x.PROJECT_LABEL.id_label_project_default,
                        Status = x.status == 1 ? "Iniciado" : x.status == 2 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                        StartDate = x.start_date,
                        EndDate = x.end_date,
                        EstimateCost = x.estimate,
                        estimate_usd = x.estimate_usd??0,
                        total_cost_usd = x.total_cost_usd??0,
                    }).ToList();

                return model;
            }
            else
            {
                var model = _context.PROJECT_ITEM
                    .Where(x => x.PROJECT_LABEL.id_project == ProjectId && x.PROJECT_LABEL.PROJECT_HEADER.cuser == User && x.PROJECT_LABEL.active_label == true)
                    .Select(x => new ItemModel
                    {
                        ItemId = x.item_id,
                        ItemDescription = x.description,
                        LabelDescription = x.PROJECT_LABEL.id_label_project_default,
                        Status = x.status == 1 ? "Iniciado" : x.status == 2 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                        StartDate = x.start_date,
                        EndDate = x.end_date,
                        EstimateCost = x.estimate,
                        
                        estimate_usd = x.estimate_usd??0,
                    }).ToList();

                return model;
            }
        }

        public ProjectModel GetProjectDetails(int ProjectId, string User)
        {
            var model = _context.PROJECT_HEADER
                .Where(x => x.id_project == ProjectId && x.cuser == User)
                .Select(x => new ProjectModel
                {
                    ProjectId = x.id_project,
                    Title = x.title,
                    Description = x.description,
                    Category = x.PROJECT_CATEGORY.description,
                    EstimateCost = x.estimate,
                    TotalCost = x.total_cost,
                    CompromiseDate = x.compromise_date,
                    Status = x.status == 0 ? "INICIADO" : x.status == 1 ? "EN PROCESO" : x.status == 8 ? "CANCELADO" : x.status == 9 ? "TERMINADO" : "",
                    StartDate = x.start_date.HasValue ? x.start_date : null,
                    EndDate = x.end_date.HasValue ? x.end_date : null,
                    CreatedBy = x.cuser,
                    estimate_usd = x.estimate_usd??0,
                    total_cost_usd = x.total_cost_usd??0,
                }).FirstOrDefault();

            return model;
        }

        public List<ItemModel> GetProjectItems(int ProjectId)
        {
            var model = _context.PROJECT_ITEM
                .Where(x => x.PROJECT_LABEL.id_project == ProjectId & x.PROJECT_LABEL.active_label == true)
                .Select(x => new ItemModel
                {
                    ItemId = x.item_id,
                    LabelId = x.id_label_project,
                    ItemDescription = x.description,
                    Status = x.status == 1 ? "Iniciado" : x.status == 2 ? "En Proceso" : x.status == 8 ? "Cancelado" : x.status == 9 ? "Terminado" : "",
                    StartDate = x.start_date,
                    EndDate = x.end_date,
                    RealEndDate = x.real_end_date.HasValue ? x.real_end_date : null,
                    EstimateCost = x.estimate,
                    LabelDescription = x.PROJECT_LABEL.id_label_project_default,
                    CreatedBy = x.cuser,
                    estimate_usd = x.estimate_usd??0,
                }).OrderBy(x => x.StartDate).ToList();

            if (model != null)
            {
                foreach (var item in model)
                {
                    if (item.Status != "Terminado" & item.Status != "Cancelado")
                    {
                        if ((item.EndDate.Date - DateTime.Now.Date).TotalDays <= 7)
                            item.Days = Convert.ToInt32((item.EndDate.Date - DateTime.Now.Date).TotalDays);
                        else
                            item.Days = null;
                    }
                    else
                        item.Days = null;
                }
            }

            return model;
        }

        public decimal GetTotalCostByProject(int ProjectId, string currency)
        {
            var TotalCost = _context.XML_HEADER
                .Where(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.project_id == ProjectId))
                .Where(x => x.currency==currency)
                .Sum(z => z.total_amount);

            return TotalCost ?? 0m;
        }

        public decimal GetCostByLabel(int LabelId, string currency)
        {
            var TotalCost = _context.XML_HEADER
                .Where(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.label_id == LabelId))
                .Where(x => x.currency==currency)
                .Sum(z => z.total_amount);

            return TotalCost ?? 0m;
        }

        public decimal GetTotalCostByDay(int ProjectId,string currency)
        {
            var TotalCost = _context.XML_HEADER
                .Where(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.project_id == ProjectId) & DbFunctions.TruncateTime(x.cdate.Value) == DbFunctions.TruncateTime(DateTime.Now))
                .Where(x => x.currency==currency)
                .Sum(z => z.total_amount);

            return TotalCost ?? 0m;
        }

        public decimal GetTotalCostByWeek(int ProjectId,string currency)
        {
            var date = DateTime.Now.AddDays(-7);
            var TotalCost = _context.XML_HEADER
                .Where(x => x.EXPENSE.PROJECT_EXPENSE.Any(z => z.project_id == ProjectId) & DbFunctions.TruncateTime(x.cdate.Value) >= DbFunctions.TruncateTime(date))
                .Where(x => x.currency==currency)
                .Sum(z => z.total_amount);

            return TotalCost ?? 0m;
        }

        public List<CategoryUserModel> GetCategoryUsers(int ProjectId)
        {
            var model = _context.PROJECT_CATEGORY_USER
                .Where(x => x.id_project == ProjectId)
                .Select(x => new CategoryUserModel { EmployeeNumber = x.emp_no }).ToList();

            return model;
        }
    }
}