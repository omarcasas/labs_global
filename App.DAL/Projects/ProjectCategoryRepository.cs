﻿using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Projects
{
    public class ProjectCategoryRepository
    {
        private PRTSAIEntities _context = new PRTSAIEntities();
        public List<ProjectCategoryModel> GetAllCategory()
        {
            return _context.PROJECT_CATEGORY.Where(x => x.status == true).Select(s => new ProjectCategoryModel
            {
                description = s.description,
                id_category = s.id_category
            }).ToList();
        }
        public PROJECT_CATEGORY AddCategory(PROJECT_CATEGORY model)
        {
            try
            {
                _context.PROJECT_CATEGORY.Add(model);
                _context.SaveChanges();
                return model;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool AddCategoryUsers(PROJECT_CATEGORY_USER model)
        {
            try
            {
                _context.PROJECT_CATEGORY_USER.Add(model);
                _context.SaveChanges();
                return true; 
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateAllDefault(int id_category)
        {
            try
            {
                _context.Database.ExecuteSqlCommand(@"update PROJECT_LABEL_DEFAULT set active_flag = 0 where id_category = " + id_category);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<ProjectUserNotification> GetAllUserCategory(int id_category, int id_proyect)
        {
            return _context.PROJECT_CATEGORY
                .Join(_context.PROJECT_CATEGORY_USER, pc => pc.id_category, pcu => pcu.id_category, (pc, pcu) => new { pc, pcu })
                .Join(_context.USER_MASTER, pcu2 => pcu2.pcu.emp_no, us => us.emp_no, (pcu2, us) => new { pcu2, us})
                .Where(w => w.pcu2.pc.id_category == id_category && w.pcu2.pcu.id_project == id_proyect)
                .Select(s => new ProjectUserNotification
                {
                        user_name = s.us.user_name,
                        id_label_project_default = s.pcu2.pc.description
                }).ToList();
        }
        public List<string> GetUsersCategoryById(int id)
        {
            return _context.PROJECT_CATEGORY_USER.Where(x => x.id_project == id).Select(s =>
                s.emp_no
            ).ToList();
        }

        public bool UpdateCategoryUsers(int id_project, IEnumerable<PROJECT_CATEGORY_USER> models)
        {
            try
            {
                _context.PROJECT_CATEGORY_USER.RemoveRange(
                    _context.PROJECT_CATEGORY_USER.Where(e => e.id_project == id_project)
                    );
                _context.PROJECT_CATEGORY_USER.AddRange(models);
                
                _context.SaveChanges();
                return true; 
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
