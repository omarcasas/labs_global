﻿using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;

namespace App.DAL.Projects
{
    public class ProjectItemRepository
    {
        private PRTSAIEntities _context;
        public ProjectItemRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ProjectHeaderModel> GetProjectsHeader(string User, string number)
        {
            return _context.PROJECT_HEADER.Where(x => x.status != 8 && x.status != 9 && (x.cuser == User || x.PROJECT_LABEL.Any(y => y.emp_no == number && y.active_label))).Select(x => new ProjectHeaderModel
            {
                status = x.status,
                compromise_date = x.compromise_date,
                start_date = x.start_date ?? DateTime.Now,
                estimate = x.estimate,
                id_project = x.id_project,
                total_cost = x.total_cost,
                title = x.title,
                cuser = x.cuser,
                currentuser = User,
                estimate_usd = x.estimate_usd ?? 0,
                total_cost_usd = x.total_cost_usd ?? 0,
            }).ToList();
        }

        public List<ProjectHeaderModel> GetProjectsHeaderAll(string user, string number)
        {
            return _context.Database.SqlQuery<ProjectHeaderModel>(@"SELECT DISTINCT ph.id_project, ph.compromise_date, ISNULL(ph.start_date, GETDATE()) as start_date, ph.estimate, ph.total_cost, ph.title, ph.cuser, ph.estimate_usd, ISNULL(ph.total_cost_usd,0) as total_cost_usd from PROJECT_LABEL pl
                left join PROJECT_LABEL_USER plu on plu.id_label_project = pl.id_label_project
                left join PROJECT_CATEGORY_USER pcu on pcu.id_project = pl.id_project
                left join PROJECT_HEADER ph on ph.id_project = pl.id_project
                inner join USER_MASTER um on um.emp_no = plu.emp_no or um.emp_no = pl.emp_no or um.user_name = pl.cuser or pcu.emp_no = um.emp_no or ph.cuser = um.user_name
                where  ( plu.active = 1 AND plu.emp_no = @number or pl.emp_no = @number or pcu.emp_no = @number or ph.cuser = @user  AND plu.active = 1)",
          new SqlParameter("@number", number),
          new SqlParameter("@user", user)).ToList();
        }
        public List<ProjectHeaderModel> GetProjectsHeaderAllExpenses(string user, string number)
        {
            return _context.Database.SqlQuery<ProjectHeaderModel>(@"SELECT DISTINCT ph.id_project, ph.compromise_date, ISNULL(ph.start_date, GETDATE()) as start_date, ph.estimate, ph.total_cost, ph.title, ph.cuser, ph.estimate_usd, ISNULL(ph.total_cost_usd,0) as total_cost_usd from PROJECT_LABEL pl
                left join PROJECT_LABEL_USER plu on plu.id_label_project = pl.id_label_project
                left join PROJECT_CATEGORY_USER pcu on pcu.id_project = pl.id_project
                left join PROJECT_HEADER ph on ph.id_project = pl.id_project
                inner join USER_MASTER um on um.emp_no = plu.emp_no or um.emp_no = pl.emp_no or um.user_name = pl.cuser or pcu.emp_no = um.emp_no or ph.cuser = um.user_name
                where  ( plu.active = 1 AND plu.emp_no = @number or pl.emp_no = @number or pcu.emp_no = @number or ph.cuser = @user  AND plu.active = 1);",
                new SqlParameter("@number", number),
                new SqlParameter("@user", user)).ToList();
        }
        public List<ProjectLabelModel> GetProjectLabel(int IdProject, bool ActiveLabel, string User)
        {//d?
            try
            {
                //traer los usuarios y las etiquetas
                return _context.Database.SqlQuery<ProjectLabelModel>(@"  select um.emp_no as EmpNo , (um.first_name+ ' '+um.last_name) as FullName,
                      pl.active_label as ActiveLabel,case when um.photo is null then 'N/A' else um.photo end as Image,
                                         isnull((select  sum(isnull(total_amount,0))from XML_HEADER xh inner join PROJECT_EXPENSE pe on xh.expense_id = pe.expense_id where pe.label_id = pl.id_label_project and xh.currency='MXN'  ),0) as TotalCost, 
                                         isnull((select  sum(isnull(total_amount,0)) from XML_HEADER xh inner join PROJECT_EXPENSE pe on xh.expense_id = pe.expense_id where pe.label_id = pl.id_label_project and xh.currency='USD'  ),0) as total_cost_usd, 
                      pl.Estimate,ISNULL(pl.estimate_usd,0) as estimate_usd, pl.id_label_project_default as IdLabelProjectDefault,
                      pl.id_project as IdProject , pl.id_label_project as IdLabelProject, 
                        (select count(*) from PROJECT_ITEM pis where pis.id_label_project = pl.id_label_project and pis.status != 8) as ItemsCount,
						(select SUM(estimate) from PROJECT_LABEL where  active_label = @ActiveLabel and id_project = @IdProject) as EstimateCard,
						(select SUM(ISNULL(estimate_usd,0)) from PROJECT_LABEL where  active_label = @ActiveLabel and id_project = @IdProject) as EstimateCardUsd
                      from PROJECT_LABEL pl 
                      inner join USER_MASTER um on um.emp_no = pl.emp_no where 
                        (pl.emp_no in (select emp_no from USER_MASTER where user_name = @user) or pl.cuser = @user) and
                        active_label = @ActiveLabel and pl.id_project = @IdProject",
                    new SqlParameter("@IdProject", IdProject),
                    new SqlParameter("@ActiveLabel", ActiveLabel),
                    new SqlParameter("@user", User)).ToList();
                //return _context.PROJECT_LABEL.Where(x => x.id_project == IdProject).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        public List<ProjectItemModel> GetProjectItem(int IdLabelProject)
        {
            //Traer usuario y tambien los items
            var Items = _context.PROJECT_ITEM.Where(x => x.id_label_project == IdLabelProject && x.status != 8).Select(x => new ProjectItemModel
            {
                ConsecutiveItem = x.consecutive_item,
                Description = x.description,
                EndDate = x.end_date,
                StartDate = x.start_date,
                Estimate = x.estimate,
                IdItem = x.item_id,
                IdLabelProject = x.id_label_project,
                RealEndDate = x.real_end_date,
                RealStartDate = x.real_start_date,
                Status = x.status,
                TotalCost = x.total_cost,
                estimate_usd = x.estimate_usd??0,
                total_cost_usd = x.total_cost_usd??0,
            }).ToList();
            return Items;
        }

        public PROJECT_ITEM PostProjectItem(PROJECT_ITEM ProjectItem)
        {
            _context.PROJECT_ITEM.Add(ProjectItem);
            _context.SaveChanges();
            return ProjectItem;
        }

        public int UpdateProjectItem(ProjectItemModel ProjectItem, string User)
        {
            try
            {
                var Project = _context.PROJECT_ITEM.Where(x => x.item_id == ProjectItem.IdItem).SingleOrDefault();
                if (Project.status == 1 || Project.status == 2)
                {
                    Project.estimate = ProjectItem.Estimate;
                    Project.uuser = User;
                    Project.udate = DateTime.Now;
                    Project.status = ProjectItem.Status;
                    Project.start_date = ProjectItem.StartDate;
                    Project.end_date = ProjectItem.EndDate;
                    Project.estimate_usd = ProjectItem.estimate_usd;
                    Project.consecutive_item = ProjectItem.ConsecutiveItem;
                    if (ProjectItem.Status == 2)
                    {
                        Project.real_start_date = DateTime.Now;
                    }
                    if (ProjectItem.Status == 9)
                    {
                        if (Project.real_start_date == null)
                        {
                            Project.real_start_date = DateTime.Now;
                        }
                        Project.real_end_date = ProjectItem.RealEndDate ?? null;
                    }
                    Project.program_id = "PROJ002.cshtml";
                    _context.SaveChanges();
                    return 1;
                }
                return 2;
            }
            catch (Exception)
            {
                return 3;
            }

        }
        public int UpdateProjectLabel(int IdProjectLabel, decimal Estimate, string User, decimal estimate_usd)
        {
            try
            {
                if (_context.PROJECT_ITEM.Where(x => x.id_label_project == IdProjectLabel && x.status != 8).ToList().Any())
                    return 3;
                else
                {
                    var Label = _context.PROJECT_LABEL.SingleOrDefault(x => x.id_label_project == IdProjectLabel);
                    Label.estimate = Estimate;
                    Label.estimate_usd = estimate_usd;
                    Label.uuser = User;
                    Label.udate = DateTime.Now;
                    Label.program_id = "PROJ002.cshtml";
                    _context.SaveChanges();
                    return 1;
                }
            }
            catch (Exception e)
            {
                return 2;
            }
        }

        public int UpdateStatusProjectHeader(int IdProjectHeader, int Status, string User)
        {
            try
            {
                var ProjectHeaderUpdate = _context.PROJECT_HEADER.Where(x => x.id_project == IdProjectHeader).SingleOrDefault();
                ProjectHeaderUpdate.status = Status;
                ProjectHeaderUpdate.uuser = User;
                ProjectHeaderUpdate.udate = DateTime.Now;
                ProjectHeaderUpdate.program_id = "PROJ002.cshtml";
                if (ProjectHeaderUpdate.start_date == null)
                {
                    ProjectHeaderUpdate.start_date = DateTime.Now;
                }
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 2;
            }
        }


    }
}
