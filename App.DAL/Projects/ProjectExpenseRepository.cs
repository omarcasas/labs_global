using System;
using System.Collections.Generic;
using System.Linq;
using App.Entities;
using App.Entities.ViewModels.Projects;

namespace App.DAL.Projects
{
    public class ProjectExpenseRepository
    {
        private PRTSAIEntities _context = new PRTSAIEntities();

        public bool AddXmlsAndTotalCost(string userName, UploadProjectXmls data, EXPENSE exp)
        {
            using (var trans = _context.Database.BeginTransaction())
            {

                try
                {
                    var items = _context.PROJECT_ITEM.Where(e => e.id_label_project == data.label.IdLabelProject)
                        .ToList();
                    foreach (var i in data.items)
                    {
                        var  found = items.Find(e => e.item_id == i.IdItem);
                        if (found == null)
                        {
                            return false;
                        }
                        
                        found.total_cost += i.NTotalCost;
                        found.total_cost_usd += i.ntotal_cost_usd;
                    }
                    var m = _context.PROJECT_EXPENSE.Add(new PROJECT_EXPENSE
                    {

                        project_id = data.label.IdProject,
                        expense_id = exp.expense_id,
                        label_id = data.label.IdLabelProject,
                        cdate = DateTime.Now,
                        cuser = userName,
                        program_id = "",
                        PROJECT_EXPENSE_ITEMS = data.items.Select(e => new PROJECT_EXPENSE_ITEMS
                        {
                            project_item_id = e.IdItem,
                        }).ToList(),
                    });
                    _context.SaveChanges();
                    var filter = data.items.Select(e => e.IdItem);
                    var to_edit = _context.PROJECT_ITEM.Where(e => filter.Contains(e.item_id) && e.status == 1);
                    foreach (var e in to_edit)
                    {
                        e.status = 2;
                    }
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return false;
                }
            }


        }
        public bool ExistExpenseByProject(int id)
        {
            return _context.PROJECT_EXPENSE.Any(x => x.project_id == id);
        }
        public bool ExistExpenseByLabel(int id)
        {
            return _context.PROJECT_EXPENSE.Any(x => x.label_id == id);
        }
        
    }
}