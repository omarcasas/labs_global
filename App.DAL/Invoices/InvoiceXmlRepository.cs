﻿using App.Entities;
using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace App.DAL.Invoices
{
    public class InvoiceXmlRepository
    {
        private readonly PRTSAIEntities _context;

        public InvoiceXmlRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<Customer> GetCustomersBySiteIP(string SiteIP)
        {
            var query = @" EXEC [" + SiteIP + "].[DFL_SAI_SITE].[dbo].[sp_Get_Customers] 1 ";
            var model = _context.Database.SqlQuery<Customer>(query).ToList();

            return model;
        }

        public List<InvoiceXml> GetXML()
        {
            var query = @" 
                SELECT [XML].id 'Id', [XML].uuid 'Uuid', [XML].invoice_no 'InvoiceNo', [XML].serie 'Serie', [XML].xml_name 'FileName', 
	            [XML].invoice_type 'CDFI', [XML].supplier_id 'SupplierId', [XML].customer_code 'CustomerCode', 
	            [XML].site_code 'SiteCode', [XML].xml_status 'Status', [XML].active 'Active', UM.first_name + ' ' + UM.last_name 'UserCreated', 
	            [XML].cdate 'CreateDate'
                FROM INVOICE_XML [XML]
                INNER JOIN USER_MASTER UM ON [XML].cuser = UM.user_name
                WHERE CONVERT(DATE, [XML].cdate) = CONVERT(DATE, GETDATE()) ";

            var model = _context.Database.SqlQuery<InvoiceXml>(query).ToList();

            return model;
        }

        public bool SaveXML(List<InvoiceXml> Xml, string User)
        {
            try
            {
                foreach (var file in Xml)
                {
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.Xml));
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;

                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var Uuid = root?
                       .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                       .Attribute("UUID");

                    var UsoCFDI = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Receptor").Attribute("UsoCFDI");
                    string CFDI = "";
                    if (UsoCFDI != null)
                    {
                        switch (UsoCFDI.Value)
                        {
                            case "G01":
                                CFDI = "Adquisicion de Mercancias";
                                break;
                            case "G02":
                                CFDI = "Devoluciones, Descuentos o Bonificaciones";
                                break;
                            case "G03":
                                CFDI = "Gastos en General";
                                break;
                            case "I01":
                                CFDI = "Construcciones";
                                break;
                            case "I02":
                                CFDI = "Mobiliario y Equipo de Oficina por Inversiones";
                                break;
                            case "I03":
                                CFDI = "Equipo de Transporte";
                                break;
                            case "I04":
                                CFDI = "Equipo de Computo y Accesorios";
                                break;
                            case "I05":
                                CFDI = "Dados, Troqueles, Moldes, Matrices y Herramiental";
                                break;
                            case "I06":
                                CFDI = "Comunicaciones Telefonicas";
                                break;
                            case "I07":
                                CFDI = "Comunicaciones Satelitales";
                                break;
                            case "I08":
                                CFDI = "Otra Maquinaria y Equipo";
                                break;
                            case "D01":
                                CFDI = "Honorarios Medicos, Dentales y Gastos Hospitalarios";
                                break;
                            case "D02":
                                CFDI = "Gastos Medicos por Incapacidad o Discapacidad";
                                break;
                            case "D03":
                                CFDI = "Gastos Funerales";
                                break;
                            case "D04":
                                CFDI = "Donativos";
                                break;
                            case "D05":
                                CFDI = "Intereses Reales Efectivamente Pagados por Creditos Hipotecarios (Casa Habitacion)";
                                break;
                            case "D06":
                                CFDI = "Aportaciones Voluntarias al SAR";
                                break;
                            case "D07":
                                CFDI = "Primas por Seguros de Gastos Medicos";
                                break;
                            case "D08":
                                CFDI = "Gastos de Transportacion Escolar Obligatoria";
                                break;
                            case "D09":
                                CFDI = "Depositos en Cuentas para el Ahorro, Primas que tengan como Base Planes de Pensiones";
                                break;
                            case "D10":
                                CFDI = "Pagos por Servicios Educativos (Colegiaturas)";
                                break;
                            case "P01":
                                CFDI = "Por Definir";
                                break;
                            default:
                                CFDI = "";
                                break;
                        };
                    }

                    INVOICE_XML model = new INVOICE_XML
                    {
                        uuid = (string)Uuid,
                        xml_name = file.FileName,
                        xml = file.Xml,
                        invoice_type = CFDI,
                        xml_status = 1,
                        active = true,
                        cuser = User,
                        cdate = DateTime.Now
                    };

                    var Serie = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("Serie");
                    if (Serie != null)
                        model.serie = Serie.Value;

                    var Folio = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("Folio");
                    if (Folio != null)
                        model.invoice_no = Folio.Value;

                    _context.INVOICE_XML.Add(model);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool SaveCustomerXML(List<InvoiceXml> Xml, string InvoiceType, int SupplierId, string User)
        {
            try
            {
                foreach (var file in Xml)
                {
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.Xml));
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;

                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var Uuid = root?
                       .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                       .Attribute("UUID");

                    INVOICE_XML model = new INVOICE_XML
                    {
                        uuid = (string)Uuid,
                        xml_name = file.FileName,
                        xml = file.Xml,
                        invoice_type = InvoiceType,
                        supplier_id = SupplierId,
                        xml_status = 1,
                        active = true,
                        cuser = User,
                        cdate = DateTime.Now
                    };

                    _context.INVOICE_XML.Add(model);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public INVOICE_XML GetPDF(int Id)
        {
            var model = _context.INVOICE_XML.Where(x => x.id == Id).FirstOrDefault();

            return model;
        }

        public List<INVOICE_XML> GetAllPDF()
        {
            var model = _context.INVOICE_XML.ToList();

            return model;
        }

        public Customer GetCustomerInfoById(string SiteIP, string CustomerCode)
        {
            var query = @" SELECT customer_rfc AS RFC, customer_code AS Code, customer_name AS Name, customer_status AS Status
                        FROM [" + SiteIP + "].[DFL_SAI_SITE].[dbo].[CUSTOMERS] where customer_code = " + CustomerCode;
            var model = _context.Database.SqlQuery<Customer>(query).FirstOrDefault();

            return model;
        }
    }
}