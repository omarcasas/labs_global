﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Invoices
{
    public class InvoiceCedulasRepository
    {
        private PRTSAIEntities _context;

        public InvoiceCedulasRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<DUPLICADOS_CEDULA> PorTipoyEstado(string tipo, int estado)
        {
            return _context.DUPLICADOS_CEDULA.Where(w => w.tipo == tipo && w.estado > estado).ToList();
        }

        public List<DUPLICADOS_CEDULA> PorFolioyEstado(string folio, int estado)
        {
            return _context.DUPLICADOS_CEDULA.Where(w => w.folio == folio && w.estado > estado).ToList();
        }


        public string NombreTiendaPorSerie(string serie)
        {
            return _context.SITES.Where(w => w.site_serie == serie).FirstOrDefault().site_name;
        }

        public string CanceladoCedulaPorFolioyTipo(string folio, string tipo)
        {
            return _context.CANCELADOS_CEDULA.Where(w => w.nombre == folio && w.tipo == tipo).Select(s => s.nombre).FirstOrDefault();
        }

        public RELACION_CEDULAS RelacionCedulasPorIdEgreso(string id)
        {
            return _context.RELACION_CEDULAS.Where(w => w.uuid_nota_credito == id).FirstOrDefault();
        }

        public List<XML_TRANS_DATE> XMLPorFechayTipo(int year, int month, string tipo)
        {
            return _context.XML_TRANS_DATE.Where(w => w.trans_date.Value.Year == year && w.trans_date.Value.Month == month + 1 && w.tipo == tipo).ToList();
        }


    }
}
