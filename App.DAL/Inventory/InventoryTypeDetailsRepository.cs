﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Inventory
{
    public class InventoryTypeDetailsRepository
    {
        private PRTSAIEntities _context;
        public InventoryTypeDetailsRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<InventoryTypeDetailsModel> GetInventoryTypeDatails(int IdInventoryType)
        {
            return _context.INVENTORY_TYPE_DETAILS
                .Join(_context.MA_CLASS, InventoryTypeDetails => InventoryTypeDetails.level_ma_class, MaClass => MaClass.class_id, (InventoryTypeDetails, MaClass) => new { InventoryTypeDetails, MaClass })
                .Join(_context.INVENTORY_TYPE, it => it.InventoryTypeDetails.id_inventory_type, itd => itd.id_inventory_type, (it, itd) => new { it, itd })
                .Where(x => x.it.InventoryTypeDetails.id_inventory_type == IdInventoryType).Select(x => new InventoryTypeDetailsModel
                {
                    ActiveDetails = x.it.InventoryTypeDetails.active_details,
                    IdInventoryType = x.it.InventoryTypeDetails.id_inventory_type,
                    LevelMaClass = x.it.InventoryTypeDetails.level_ma_class,
                    LevelName = x.it.MaClass.class_name,
                    IsCreate = true,
                    CloseTime = x.itd.close_time.ToString()
                }).ToList();
        }

        public InventoryTypeDetailsModel EditInventoryDetails(InventoryTypeDetailsModel InventoryTypeDetails, string User)
        {
            var InventoryType = _context.INVENTORY_TYPE_DETAILS
                .Where(x => x.id_inventory_type == InventoryTypeDetails.IdInventoryType && x.level_ma_class == InventoryTypeDetails.LevelMaClass)
                .SingleOrDefault();
            if (InventoryType != null)
            {
                InventoryType.active_details = InventoryTypeDetails.ActiveDetails;
                InventoryType.uuser = User;
                InventoryType.udate = DateTime.Now;
                InventoryType.program_id = "INVE002.cshtml";
                _context.SaveChanges();
                return InventoryTypeDetails;
            }
            return new InventoryTypeDetailsModel();
        }

        public InventoryTypeDetailsModel CreateInventoryTypeDatails(InventoryTypeDetailsModel InventoryTypeDetails, string User)
        {
            var NewInventoryTypeDetails = new INVENTORY_TYPE_DETAILS()
            {
                active_details = InventoryTypeDetails.ActiveDetails,
                id_inventory_type = InventoryTypeDetails.IdInventoryType,
                level_ma_class = InventoryTypeDetails.LevelMaClass,
                cdate = DateTime.Now,
                cuser = User,
                program_id = "INVE002.cshtml"
            };
            _context.INVENTORY_TYPE_DETAILS.Add(NewInventoryTypeDetails);
            _context.SaveChanges();
            return InventoryTypeDetails;
        }
    }
}
