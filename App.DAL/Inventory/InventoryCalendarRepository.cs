﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryCalendarRepository
    {
        private PRTSAIEntities _context;

        //constructor Inventory Calendar
        public InventoryCalendarRepository()
        {
            _context = new PRTSAIEntities();
        }
        public List<InventoryCalendarModel> getInventoryCalendarDatesBetween(DateTime date1, DateTime date2)
        {
            try
            {
                var item = _context.Database.SqlQuery<InventoryCalendarModel>(@"select id_calendar as id,inventory_name as title,inventory_description as description,B.site_code,site_name,calendar_date as start,calendar_status,type as typeInv
                from INVENTORY_CALENDAR AS A INNER JOIN SITES AS B ON A.site_code = B.site_code 
                where calendar_date between @date1 and  @date2 and calendar_status != 8",
                new SqlParameter("date1", date1),
                new SqlParameter("date2", date2)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public int UpdateInventoryCalendar(InventoryCalendarModel InventoryObj)
        {
            try
            {
                var DateReturn = _context.Database.ExecuteSqlCommand(@"update INVENTORY_CALENDAR 
                set inventory_name=@inventory_name,inventory_description=@inventory_description,site_code=@site_code,calendar_date=@calendar_date,calendar_status=@calendar_status,type=@type,udate=GETDATE(),uuser=@uuser,program_id=@program_id
                where id_calendar=@id_calendar ",
                new SqlParameter("id_calendar", InventoryObj.id),
                new SqlParameter("inventory_name",InventoryObj.title),
                new SqlParameter("inventory_description", InventoryObj.description),
                new SqlParameter("site_code", InventoryObj.site_code),
                new SqlParameter("calendar_date", InventoryObj.start),
                new SqlParameter("calendar_status", InventoryObj.calendar_status),
                new SqlParameter("type", InventoryObj.typeInv),
                new SqlParameter("uuser", InventoryObj.uuser),
                new SqlParameter("program_id", InventoryObj.program_id));
                return DateReturn;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }
        public int InsertInventoryCalendar(InventoryCalendarModel InventoryObj)
        {
            try
            {
                var DateReturn = _context.Database.ExecuteSqlCommand(@"insert into INVENTORY_CALENDAR (inventory_name,inventory_description,site_code,calendar_date,calendar_status,type,cdate,cuser,program_id)
                values (@inventory_name,@inventory_description,@site_code,@calendar_date,@calendar_status,@type,GETDATE(),@cuser,@program_id)",
                new SqlParameter("inventory_name", InventoryObj.title),
                new SqlParameter("inventory_description", InventoryObj.description),
                new SqlParameter("site_code", InventoryObj.site_code),
                new SqlParameter("calendar_date", InventoryObj.start),
                new SqlParameter("calendar_status", InventoryObj.calendar_status),
                new SqlParameter("type", InventoryObj.typeInv),
                new SqlParameter("cuser", InventoryObj.cuser),
                new SqlParameter("program_id", InventoryObj.program_id));
                return DateReturn;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }
        public int UpdateInventoryCalendarOnlyDate(InventoryCalendarModel InventoryObj, string user, string program)
        {
            try
            {
                var DateReturn = _context.Database.ExecuteSqlCommand(@"update INVENTORY_CALENDAR 
                set calendar_date=@calendar_date,udate=GETDATE(),uuser=@uuser,program_id=@program_id
                where id_calendar=@id_calendar and calendar_status=0",
                new SqlParameter("id_calendar", InventoryObj.id),
                new SqlParameter("calendar_date", InventoryObj.start),
                new SqlParameter("uuser", user),
                new SqlParameter("program_id", program));
                return DateReturn;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }
        public int AddInventoryCyclic(INVENTORY_CALENDAR model)
        {
            try
            {
                _context.INVENTORY_CALENDAR.Add(model);
                _context.SaveChanges();
                int id = model.id_calendar;
                return id;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }
        public bool AddInventoryCyclicPartNumber(INVENTORY_CYCLIC model)
        {
            try
            {
                _context.INVENTORY_CYCLIC.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }
        public INVENTORY_CALENDAR CheckInventoryStatus0(int idCalendar)
        {
            var ret = _context.INVENTORY_CALENDAR.Where(x => x.id_calendar == idCalendar && x.calendar_status == 0).FirstOrDefault();
            return ret;
        }
        public bool CancelInventory(INVENTORY_CALENDAR InvCalendar)
        {
            try
            {
                var InventoryCalendar = _context.INVENTORY_CALENDAR.Where(a => a.id_calendar == InvCalendar.id_calendar).FirstOrDefault();
                if (InventoryCalendar != null)
                {
                    InventoryCalendar.calendar_status = InvCalendar.calendar_status;
                    InventoryCalendar.udate = InvCalendar.udate;
                    InventoryCalendar.uuser = InvCalendar.uuser;
                    InventoryCalendar.program_id = InvCalendar.program_id;
                    _context.SaveChanges();
                    return true;
                }else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
          
        }

    }
}

