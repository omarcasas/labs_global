﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Inventory
{
    public class InventoryTypeRepository
    {
        private PRTSAIEntities _context;
        public InventoryTypeRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<InventoryTypeModel> GetAllInventoryType()
        {
            return _context.INVENTORY_TYPE
                //.Where(x => x.active_inventory)
                .Select(x => new InventoryTypeModel
            {
                ActiveInventory = x.active_inventory,
                Day = x.day,
                IdInventoryType = x.id_inventory_type,
                InventoryName = x.inventory_name,
                TypeReview = x.type_review,
                CloseTime = x.close_time ?? new TimeSpan()          
            }).ToList();
        }
        public List<InventoryTypeModel> GetAllInventoryTypeActives()
        {
            return _context.INVENTORY_TYPE
                .Where(x => x.active_inventory==true)
                .Select(x => new InventoryTypeModel
                {
                    ActiveInventory = x.active_inventory,
                    Day = x.day,
                    IdInventoryType = x.id_inventory_type,
                    InventoryName = x.inventory_name,
                    TypeReview = x.type_review
                }).ToList();
        }

        public InventoryTypeModel SaveInventory(InventoryTypeModel Inventory, string User)
        {
            var InventoryType = new INVENTORY_TYPE()
            {
                active_inventory = Inventory.ActiveInventory,
                day = Inventory.Day,
                id_inventory_type = Inventory.IdInventoryType,
                inventory_name = Inventory.InventoryName,
                type_review = Inventory.TypeReview,
                cuser = User,
                cdate = DateTime.Now ,
                program_id = "INVE002.cshtml",
                close_time = Inventory.CloseTime
            };
            _context.INVENTORY_TYPE.Add(InventoryType);
            _context.SaveChanges();
            Inventory.IdInventoryType = InventoryType.id_inventory_type;
            return Inventory;
        }

        public InventoryTypeModel EditInventory(InventoryTypeModel Inventory,string User)
        {
            var InventoryType = _context.INVENTORY_TYPE.Where(x => x.id_inventory_type == Inventory.IdInventoryType).SingleOrDefault();
            InventoryType.inventory_name = Inventory.InventoryName;
            InventoryType.type_review = Inventory.TypeReview;
            InventoryType.day = Inventory.Day;
            InventoryType.active_inventory = Inventory.ActiveInventory;
            InventoryType.uuser = User;
            InventoryType.udate = DateTime.Now;
            InventoryType.program_id = "INVE002.cshtml";
            InventoryType.close_time = Inventory.CloseTime;
            _context.SaveChanges();
            return Inventory;
        }

        public bool IsActiveInventory(int Inventory, bool IsActive, string User)
        {
            var InventoryType = _context.INVENTORY_TYPE.Where(x => x.id_inventory_type == Inventory).SingleOrDefault();
            InventoryType.active_inventory = IsActive;
            InventoryType.uuser = User;
            InventoryType.udate = DateTime.Now;
            InventoryType.program_id = "INVE002.cshtml";
            _context.SaveChanges();
            return true;
        }

    }
}
