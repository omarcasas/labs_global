﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Purchases
{
    public class NegotiatonRepository
    {
        private PRTSAIEntities _context;

        public NegotiatonRepository()
        {
            _context = new PRTSAIEntities();
        }
        public NEGOTIATION_DETAIL GetNegotiatonDetailById(int NegotiatonDetailId , int MasterId)
        {
            UpdateNegotiatonMasterCount(MasterId);
            return _context.NEGOTIATION_DETAIL.FirstOrDefault(x => x.nd_id == NegotiatonDetailId & x.nm_id== MasterId);
        }
        public NEGOTIATION_MASTER ValidateNegotiatonMasterIfExist(int supplierId)
        {
            return _context.NEGOTIATION_MASTER.FirstOrDefault(x => x.supplier_id == supplierId);
        }
        public List<NEGOTIATION_DETAIL> GetSupplierNegociationBySupplierId(int negotiatonId)
        {
            return _context.NEGOTIATION_DETAIL.Where(x => _context.NEGOTIATION_MASTER.Any(v => v.nm_id == x.nm_id && v.supplier_id == negotiatonId)).OrderBy(x => x.nm_id).ThenByDescending(x => x.nm_id).ToList();
        }
        public int AddNegotiaton(NEGOTIATION_MASTER Negotiaton)
        {
            try
            {
                _context.NEGOTIATION_MASTER.Add(Negotiaton);
                return _context.SaveChanges();

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int AddNegotiatonDetail(NEGOTIATION_DETAIL Negotiaton)
        {
            try
            {
                _context.NEGOTIATION_DETAIL.Add(Negotiaton);
                return _context.SaveChanges();

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int UpdateNegotiatonDetail(NEGOTIATION_DETAIL NegotiatonDetail)
        {
            _context.Entry(NegotiatonDetail).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }
        public int UpdateNegotiatonMasterCount(int MasterId)
        {
            try
            {
                var DateReturn = _context.Database.ExecuteSqlCommand(@"update NEGOTIATION_MASTER SET total_negotiations =  (select COUNT(*) from NEGOTIATION_DETAIL where nm_id = @id_nego),
                total_active_negotiations = (select COUNT(*) from NEGOTIATION_DETAIL where nm_id = @id_nego and valid_until >= CONVERT(date, getdate())),
                total_inactive_negotiations = (select COUNT(*) from NEGOTIATION_DETAIL where nm_id = @id_nego and valid_until < CONVERT(date, getdate()))
                where nm_id = @id_nego",
                new SqlParameter("id_nego", MasterId));
                return DateReturn;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }
        public int AddNegotiatonMaCodeCatalogue(MA_CODE MaCode)
        {
            try
            {
                _context.MA_CODE.Add(MaCode);
                return _context.SaveChanges();

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        
    }
}
