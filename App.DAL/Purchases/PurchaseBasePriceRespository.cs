﻿using App.DAL.Purchases.PurchasesModels;
using App.Entities;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data;

namespace App.DAL.Purchases
{
    public class PurchaseBasePriceRespository
    {

        private PRTSAIEntities _context;

        public PurchaseBasePriceRespository()
        {
            _context = new PRTSAIEntities();
        }
        public List<PurchaseBasePriceModel> GetPricesOfSuppliersByProduct(string PartNumber)
        {
            try
            {
                return _context.PURCHASE_BASE_COST.Join(_context.ITEM_SUPPLIER, a => a.supplier_part_number, b => b.supplier_part_number, (a, b) => new { a, b })
               .Join(_context.ITEM, u => u.b.part_number, j => j.part_number, (u, j) => new { u, j })
               .Join(_context.MA_SUPPLIER, v => v.u.b.supplier_id, g => g.supplier_id, (v, g) => new { v, g })
               .Where(x => x.v.u.b.part_number == PartNumber && x.v.u.a.status != 0 && x.g.supplier_type != "Acreedor" && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.v.u.a.supplier_part_number))
               .Select(p => new PurchaseBasePriceModel
               {
                   base_price_id = p.v.u.a.base_cost_id > -1 ? p.v.u.a.base_cost_id : 0,
                   supplier_part_number = p.v.u.a.supplier_part_number.Value > -1 ? p.v.u.a.supplier_part_number.Value : 0,
                   part_name = p.v.j.part_description != "" ? p.v.j.part_description : "",
                   business_name = p.g.business_name != "" ? p.g.business_name : "",
                   base_price = p.v.u.a.base_cost.Value > -1 ? p.v.u.a.base_cost.Value : 0,
                   currency = p.v.u.a.currency != "" ? p.v.u.a.currency : "",
                   valid_from = p.v.u.a.valid_from.ToString() != "" ? p.v.u.a.valid_from.ToString() : "",
                   valid_to = p.v.u.a.valid_to.ToString() != "" ? p.v.u.a.valid_to.ToString() : ""
               }).ToList();
            }
            catch (Exception es)
            {
                var msg = es.Message;
                throw;
            }

        }
        public PurchaseBasePriceModel GetCostProductDesc(string PartNumber)
        {
            try
            {
                var baseCost = _context.PURCHASE_BASE_COST.Join(_context.ITEM_SUPPLIER, a => a.supplier_part_number, b => b.supplier_part_number, (a, b) => new { a, b })
               .Where(x => x.b.part_number == PartNumber && x.a.status==1).OrderByDescending(f => f.a.base_cost).Take(1)
               .Select(p => new PurchaseBasePriceModel
               {
                   base_price_id = p.a.base_cost_id,
                   supplier_part_number = p.a.supplier_part_number.Value,
                   base_price = p.a.base_cost.Value > -1 ? p.a.base_cost.Value : 0,
                   currency=p.a.currency,
                   unit_of_price=p.a.unit_of_price.Value
               }).OrderByDescending(f => f.base_price).SingleOrDefault();
                if (baseCost == null)
                {
                    baseCost = new PurchaseBasePriceModel();
                    baseCost.base_price = 0;
                }
                return baseCost;
            }
            catch (Exception es)
            {
                var msg = es.Message;
                throw;
            }
        }
        public int sp_Purchases_UpdateBaseCost(int BasePriceId, int SupplierPartNumberId, decimal BasePrice, string Currency, DateTime ValidFrom, string User, string program_id)
        {

            try
            {
                if (BasePriceId == 0 && SupplierPartNumberId != 0)
                {
                    BasePriceId = _context.PURCHASE_BASE_COST.FirstOrDefault(e =>
                        e.supplier_part_number == SupplierPartNumberId && (e.status == 1 || e.status == 3))?.base_cost_id ?? 0;
                }
                var returnCode = new SqlParameter();
                returnCode.ParameterName = "@return_value";
                returnCode.SqlDbType = SqlDbType.Int;
                returnCode.Direction = ParameterDirection.Output;
                var result = _context.sp_Purchases_UpdateBaseCost(BasePriceId, SupplierPartNumberId, BasePrice, Currency, ValidFrom, User, program_id);
                return result;
            }
            catch (Exception es)
            {
                var msg = es.Message;
                throw;
            }
        }
        public List<PurchaseBasePriceViewModel> GetPurchaseBasePrice(DateTime LaterDate, DateTime NextDate)
        {
            try
            {
                var Purchase = _context.Database.SqlQuery<PurchaseBasePriceViewModel>(@"
                    select pbc.base_cost_id as base_price_id ,
                    pbc.supplier_part_number as supplier_part_number,
                    case when it.part_number is null then 'N/A'  else it.part_description end as part_name,
                    case when ms.business_name  is null then 'N/A' else business_name end as business_name,
                    case when pbc.base_cost is null or pbc.base_cost < 0 then 0 else pbc.base_cost end as base_price,
                    case when pbc.currency is null then 'N/A' else pbc.currency end as currency,
                    CONVERT(VARCHAR(10), DATEADD(dd, 0, DATEDIFF(dd, 0, case when pbc.valid_from is null then GETDATE() else pbc.valid_from end)), 103) as valid_from,
                    CONVERT(VARCHAR(10), DATEADD(dd, 0, DATEDIFF(dd, 0, case when pbc.valid_to is null then GETDATE() else pbc.valid_to end)), 103) as valid_to,
                    it.part_number as part_number,
                    case when it.level_header > 0 and it.level_header is not null then (select class_name from MA_CLASS where class_id = it.level_header) else 'N/A' end as NameLevel1,
                    case when it.level1_code > 0 and it.level1_code is not null then (select class_name from MA_CLASS where class_id = it.level1_code) else 'N/A' end as  NameLevel2,
                    case when it.level2_code > 0 and it.level2_code is not null then (select class_name from MA_CLASS where class_id = it.level2_code) else 'N/A' end as NameLevel3
                    from PURCHASE_BASE_COST pbc 
                    inner join ITEM_SUPPLIER its on its.supplier_part_number = pbc.supplier_part_number
                    inner join ITEM it on it.part_number = its.part_number
                    inner join MA_SUPPLIER ms on ms.supplier_id = its.supplier_id
                    where ms.supplier_type != 'Acreedor' and DATEADD(dd, 0, DATEDIFF(dd, 0, case when pbc.valid_from is null then GETDATE() else pbc.valid_from end)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @fecha)) and  DATEADD(dd, 0, DATEDIFF(dd, 0, case when pbc.valid_from is null then GETDATE() else pbc.valid_from end)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @fecha2))
                    ", new SqlParameter("@fecha", LaterDate), new SqlParameter("@fecha2", NextDate)).ToList();
                return Purchase;
            }
            catch (Exception es)
            {
                var msg = es.Message;
                throw;
            }

        }

        public string NameLevel(int level)
        {
            var MaClass = _context.MA_CLASS.Where(x => x.class_id == level).SingleOrDefault();
            return MaClass != null ? MaClass.class_name : "";
        }
    }
}
