﻿using System;

namespace App.DAL.Purchases.PurchasesModels
{
   public class PurchaseBasePriceModel
    {
        public int base_price_id { get; set; }
        public int supplier_part_number { get; set; }
        public string part_name { get; set; }
        public string business_name { get; set; }
        public Nullable<decimal> base_price { get; set; }
        public string currency { get; set; }
        public Nullable<decimal> profit_margin { get; set; }
        public Nullable<decimal> sales_price { get; set; }
        public string valid_from { get; set; }
        public string valid_to { get; set; }
        public Nullable<decimal> unit_of_price { get; set; }
    }
}
