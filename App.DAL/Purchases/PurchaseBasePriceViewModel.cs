﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Purchases
{
    public class PurchaseBasePriceViewModel
    {
        public string currency { get; set; }
        public decimal unit_of_price { get; set; }
        public string valid_from { get; set; }
        public string valid_to { get; set; }
        public int base_price_id { get; set; }
        public string part_name { get; set; }
        public string part_number { get; set; }
        public string business_name { get; set; }
        public Nullable<decimal> base_price { get; set; }
        public Nullable<decimal> profit_margin { get; set; }
        public Nullable<decimal> sales_price { get; set; }
        public int supplier_part_number { get; set; }//era decimal no se porque?
        public string NameLevel1 { get; set; }
        public string NameLevel2 { get; set; }
        public string NameLevel3 { get; set; }
    }
}
