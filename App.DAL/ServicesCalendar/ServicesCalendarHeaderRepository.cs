﻿using App.Entities;
using App.Entities.ViewModels.ServicesCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SalesCalendar
{
    public class ServicesCalendarHeaderRepository
    {
        private readonly PRTSAIEntities _context;

        public ServicesCalendarHeaderRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int AddServicesCalendarHeader(SERVICES_CALENDAR serviceCalendar)
        {
            try
            {
                _context.SERVICES_CALENDAR.Add(serviceCalendar);
                _context.SaveChanges();
                return serviceCalendar.service_id;
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return 0;
            }
        }

        public int GetServiceCalendarHeaderId(DateTime ServiceDate, string SiteCode)
        {
            var id = _context.SERVICES_CALENDAR.Where(x => x.service_date == ServiceDate && x.site_code == SiteCode && x.service_status == 1).FirstOrDefault();
            if (id != null)
                return id.service_id;
            else
                return 0;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDate(string site, DateTime start, DateTime finish)
        {
            var y = _context.SERVICES_CALENDAR
                .Join(_context.SITES, st => st.site_code, sc => sc.site_code, (sc, st) => new { sc, st })
                .Where(x => x.sc.site_code == site && x.sc.service_date >= start.Date && x.sc.service_date <= finish.Date)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.sc.service_id,
                    service_date = x.sc.service_date.ToString(),
                    service_status_ = x.sc.service_status == 1 ? "Programado" : x.sc.service_status == 8 ? "Cancelado" : x.sc.service_status == 9 ? "Terminado" : null,
                    site_code = x.sc.site_code,
                    site_name = x.st.site_name
                }).ToList();

            return y;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateAndStatus(string site, DateTime start, DateTime finish, int status)
        {
            var y = _context.SERVICES_CALENDAR
                .Join(_context.SITES, st => st.site_code, sc => sc.site_code, (sc, st) => new { sc, st })
                .Where(x => x.sc.site_code == site && x.sc.service_date >= start.Date && x.sc.service_date <= finish.Date && x.sc.service_status == status)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.sc.service_id,
                    service_date = x.sc.service_date.ToString(),
                    service_status_ = x.sc.service_status == 1 ? "Programado" : x.sc.service_status == 8 ? "Cancelado" : x.sc.service_status == 9 ? "Terminado" : null,
                    site_code = x.sc.site_code,
                    site_name = x.st.site_name
                }).ToList();

            return y;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateAndStatusNotSite(DateTime start, DateTime finish, int status)
        {
            var y = _context.SERVICES_CALENDAR
                .Join(_context.SITES, st => st.site_code, sc => sc.site_code, (sc, st) => new { sc, st })
                .Where(x => x.sc.service_date >= start.Date && x.sc.service_date <= finish.Date && x.sc.service_status == status)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.sc.service_id,
                    service_date = x.sc.service_date.ToString(),
                    service_status_ = x.sc.service_status == 1 ? "Programado" : x.sc.service_status == 8 ? "Cancelado" : x.sc.service_status == 9 ? "Terminado" : null,
                    site_code = x.sc.site_code,
                    site_name = x.st.site_name
                }).ToList();

            return y;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateNotSite(DateTime start, DateTime finish)
        {
            var y = _context.SERVICES_CALENDAR
                .Join(_context.SITES, st => st.site_code, sc => sc.site_code, (sc, st) => new { sc, st })
                .Where(x => x.sc.service_date >= start.Date && x.sc.service_date <= finish.Date)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.sc.service_id,
                    service_date = x.sc.service_date.ToString(),
                    service_status_ = x.sc.service_status == 1 ? "Programado" : x.sc.service_status == 8 ? "Cancelado" : x.sc.service_status == 9 ? "Terminado" : null,
                    site_code = x.sc.site_code,
                    site_name = x.st.site_name
                }).ToList();

            return y;
        }

        public List<ServicesCalendarDetailModel> GetServicesDetail(int id)
        {
            try
            {
                var detail = _context.SERVICES_CALENDAR_DETAIL
                    .Join(_context.MAQUINERY_EQUIPMENT, scd => scd.equipment_id, me => me.equipment_id, (scd, me) => new { scd, me })
                    .Join(_context.MA_SUPPLIER, scdMa => scdMa.scd.supplier_id, ma => ma.supplier_id, (scdMa, ma) => new { scdMa, ma })
                    .Where(x => x.scdMa.scd.service_id == id)
                    .Select(x => new ServicesCalendarDetailModel
                    {
                        equipment_id = x.scdMa.scd.equipment_id,
                        service_id = x.scdMa.scd.service_id,
                        supplier_id = x.scdMa.scd.supplier_id,
                        equipment_description = x.scdMa.me.equipment_description,
                        supplier_name = x.ma.commercial_name,
                        service_cost = x.scdMa.scd.service_cost,
                        service_department = x.scdMa.scd.service_department,
                        quotation_document = x.scdMa.scd.quotation_document,
                        service_date_finished2 = x.scdMa.scd.service_date_finished.ToString(),
                        currency = x.scdMa.scd.currency ?? "N/A"
                    }).ToList();

                return detail;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ServicesCalendarModel GetServicesId(int service_id)
        {
            var service = _context.SERVICES_CALENDAR
                .Join(_context.SITES, st => st.site_code, sc => sc.site_code, (sc, st) => new { sc, st })
                .Where(x => x.sc.service_id == service_id)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.sc.service_id,
                    service_date = x.sc.service_date.ToString(),
                    service_status_ = x.sc.service_status == 1 ? "Programado" : x.sc.service_status == 8 ? "Cancelado" : x.sc.service_status == 9 ? "Terminado" : null,
                    site_code = x.sc.site_code,
                    site_name = x.st.site_name
                }).FirstOrDefault();
            return service;
        }

        public bool ModifyServicesHeader(int service_id, int status, string user)
        {
            try
            {
                var service = _context.SERVICES_CALENDAR.Where(elements => elements.service_id == service_id).FirstOrDefault();
                service.service_status = status;
                service.udate = DateTime.Now;
                service.uuser = user;
                service.program_id = "MRO003.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public ServicesCalendarModel ActionGetServicesCalendarDetail(int id)
        {
            try
            {
                var model = _context.SERVICES_CALENDAR
                .Join(_context.SITES, s => s.site_code, h => h.site_code, (h, s) => new { h, s })
                .Where(x => x.h.service_id == id)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.h.service_id,
                    service_date = x.h.service_date.ToString(),
                    service_status_ = x.h.service_status == 1 ? "Programado" : x.h.service_status == 8 ? "Cancelado" : x.h.service_status == 9 ? "Terminado" : null,
                    site_code = x.h.site_code,
                    site_name = x.s.site_name,
                    ServicesCalendarDetails = _context.SERVICES_CALENDAR_DETAIL
                    .Where(y => y.service_id == x.h.service_id)
                    .Select(y => new ServicesCalendarDetailModel
                    {
                        service_id = y.service_id,
                        equipment_id = y.equipment_id,
                        equipment_description = y.MAQUINERY_EQUIPMENT.equipment_description,
                        supplier_id = y.supplier_id,
                        supplier_name = y.MA_SUPPLIER.commercial_name,
                        currency = y.currency,
                        service_cost = y.service_cost,
                        service_department = y.service_department,
                        quotation_document = y.quotation_document,
                        service_date_finished2 = y.service_date_finished.ToString()
                    }).ToList()
                }).FirstOrDefault();

                return model;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateServicesCalendar(int ServiceId, DateTime ServiceDate, string ServiceSite, string User, List<ServicesCalendarDetailModel> Services)
        {
            try
            {
                var service = _context.SERVICES_CALENDAR.Where(x => x.service_id == ServiceId).FirstOrDefault();
                service.service_date = ServiceDate;
                service.site_code = ServiceSite;
                service.uuser = User;
                service.udate = DateTime.Now;
                service.program_id = "MRO001.cshtml";
                _context.SaveChanges();

                var list = _context.SERVICES_CALENDAR_DETAIL.Where(x => x.service_id == ServiceId).ToList();

                foreach (var eq in list)
                {
                    var equipment = Services.Where(x => x.equipment_id == eq.equipment_id).FirstOrDefault();
                    if (equipment == null)
                        _context.SERVICES_CALENDAR_DETAIL.Remove(eq);
                }

                foreach (var detail in Services)
                {
                    var model = _context.SERVICES_CALENDAR_DETAIL.Where(x => x.service_id == ServiceId & x.equipment_id == detail.equipment_id).FirstOrDefault();
                    if (model != null)
                    {
                        model.equipment_id = detail.equipment_id;
                        model.supplier_id = detail.supplier_id;
                        model.currency = detail.currency;
                        model.service_cost = detail.service_cost;
                        model.service_department = detail.service_department;

                        if (detail.quotation_document == null || detail.quotation_document == "")
                            model.quotation_document = null;
                        else
                            model.quotation_document = detail.quotation_document.Split(',')[1];

                        model.uuser = User;
                        model.udate = DateTime.Now;
                        model.program_id = "MRO001.cshtml";
                        _context.SaveChanges();
                    }
                    else
                    {
                        SERVICES_CALENDAR_DETAIL serviceDetail = new SERVICES_CALENDAR_DETAIL
                        {
                            service_id = ServiceId,
                            equipment_id = detail.equipment_id,
                            supplier_id = detail.supplier_id,
                            currency = detail.currency,
                            service_cost = detail.service_cost,
                            service_department = detail.service_department,
                            cuser = User,
                            cdate = DateTime.Now,
                            program_id = "MRO001.cshtml"
                        };

                        if (detail.quotation_document == null || detail.quotation_document == "")
                            serviceDetail.quotation_document = null;
                        else
                            serviceDetail.quotation_document = detail.quotation_document.Split(',')[1];

                        _context.SERVICES_CALENDAR_DETAIL.Add(serviceDetail);
                        _context.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }
    }
}