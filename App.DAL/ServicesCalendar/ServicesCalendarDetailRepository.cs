﻿using App.Entities;
using App.Entities.ViewModels.ServicesCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SalesCalendar
{
    public class ServicesCalendarDetailRepository
    {
        private readonly PRTSAIEntities _context;

        public ServicesCalendarDetailRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int AddServicesCalendarDetail(SERVICES_CALENDAR_DETAIL serviceCalendarDetail)
        {
            try
            {
                _context.SERVICES_CALENDAR_DETAIL.Add(serviceCalendarDetail);
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return 0;
            }
        }

        public List<ServicesCalendarDetailModel> GetServicesDetailByServiceId(int service_id)
        {
            var details = _context.SERVICES_CALENDAR_DETAIL
                .Join(_context.MAQUINERY_EQUIPMENT, service => service.equipment_id, maquinery => maquinery.equipment_id, (service, maquinery) => new { service, maquinery })
                .Join(_context.MA_SUPPLIER, sermaq => sermaq.service.supplier_id, supplier => supplier.supplier_id, (sermaq, supplier) => new { sermaq, supplier })
                .Where(x => x.sermaq.service.service_id == service_id)
                .Select(selectData => new ServicesCalendarDetailModel
                {
                    equipment_serial = selectData.sermaq.maquinery.serial_number,
                    equipment_description = selectData.sermaq.maquinery.equipment_description,
                    supplier_name = selectData.supplier.business_name,
                    supplier_id = selectData.supplier.supplier_id,
                    service_cost = selectData.sermaq.service.service_cost,
                    service_department = selectData.sermaq.service.service_department
                }).ToList();
            return details;
        }

        public int[] GetServicesDetailDepartmentByServiceId(int service_id)
        {
            var details = _context.SERVICES_CALENDAR_DETAIL.Where(x => x.service_id == service_id).Select(x => x.equipment_id).ToArray();

            return details;
        }
    }
}