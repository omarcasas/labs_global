﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class SatPaymentMethodCodeRepository
    {
        private PRTSAIEntities _context;

        public SatPaymentMethodCodeRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<SAT_PAYMENT_METHOD_CODE> SatPaymentMethodCodeList()
        {
            return _context.SAT_PAYMENT_METHOD_CODE.ToList();
        }
    }
}
