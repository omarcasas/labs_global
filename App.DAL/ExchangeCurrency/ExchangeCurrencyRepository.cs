﻿using App.Entities;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.ExchangeCurrency
{
    public class ExchangeCurrencyRepository
    {
        private PRTSAIEntities _context;
        public ExchangeCurrencyRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int postExchangeCurrency(EXCHANGE_CURRENCY_COMMERCIAL ExchangeCurrency)
        {
            try
            {
                _context.EXCHANGE_CURRENCY_COMMERCIAL.Add(ExchangeCurrency);
                _context.SaveChanges();
                return ExchangeCurrency.id_exchange_currency_commercial;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public int putExchangeCurrency(string User)
        {
            var Exchange = _context.EXCHANGE_CURRENCY.Where(x => x.status).ToList();
            foreach (var item in Exchange)
            {
                item.status = false;
                item.uuser = User;
                item.udate = DateTime.Now;
            }
            _context.SaveChanges();
            return 1;
        }
        public List<ExchangeCurrencyViewModel> getAllCurrency()
        {
            var ExchangeSistem = _context.EXCHANGE_CURRENCY.OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id,
                Price = x.price,
                User = x.cuser,
                currency_type = "Banco"
            }).Take(90).ToList();
            var ExchangeUser = _context.EXCHANGE_CURRENCY_COMMERCIAL.OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id_exchange_currency_commercial,
                Price = x.price,
                User = x.cuser,
                currency_type = "Comercial"
            }).Take(90).ToList();
            ExchangeSistem.AddRange(ExchangeUser);
            ExchangeSistem = ExchangeSistem.OrderByDescending(x => x.CurrencyDate).ToList();
            return ExchangeSistem;

        }

        public List<ExchangeCurrencyViewModel> getAllCurrencyBySite(DateTime DateInit, DateTime DateFin, string siteCode)
        {
            var ExchangeSistem = _context.EXCHANGE_CURRENCY.Where(w => w.currency_date >= DateInit.Date && w.currency_date <= DateFin.Date).OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id,
                Price = x.price,
                User = x.cuser,
                currency_type = "Banco"
            }).ToList();
            var ExchangeUser = _context.EXCHANGE_CURRENCY_COMMERCIAL_SITES
                .Join(_context.EXCHANGE_CURRENCY_COMMERCIAL, ECCS => ECCS.id_exchange_currency_commercial, ECC => ECC.id_exchange_currency_commercial, (ECCS, ECC) => new { ECCS, ECC })
                .Where(w => w.ECCS.site_code == siteCode && w.ECC.currency_date >= DateInit.Date && w.ECC.currency_date <= DateFin.Date).OrderByDescending(x => x.ECC.currency_date).ToList()
                .Select(s => new ExchangeCurrencyViewModel
                {
                    Currency = "MX",
                    CurrencyDate = s.ECC.currency_date.ToShortDateString(),
                    Id = s.ECC.id_exchange_currency_commercial,
                    Price = s.ECC.price,
                    User = s.ECC.cuser,
                    currency_type = "Comercial"
                }).ToList();
            ExchangeSistem.AddRange(ExchangeUser);
            ExchangeSistem = ExchangeSistem.OrderByDescending(x => x.CurrencyDate).ToList();
            return ExchangeSistem;

        }

        public List<ExchangeCurrencyViewModel> getAllCurrencyByType(DateTime DateInit, DateTime DateFin)
        {
            return _context.EXCHANGE_CURRENCY.Where(w => w.currency_date >= DateInit.Date && w.currency_date <= DateFin.Date).OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id,
                Price = x.price,
                User = x.cuser,
                currency_type = "Banco"
            }).ToList();

        }

        public List<ExchangeCurrencyViewModel> getAllCurrencyByTypeByCommercial(DateTime DateInit, DateTime DateFin)
        {
            return _context.EXCHANGE_CURRENCY.Where(w => w.currency_date >= DateInit.Date && w.currency_date <= DateFin.Date).OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id,
                Price = x.price,
                User = x.cuser,
                currency_type = "Comercial"
            }).ToList();

        }
        public List<ExchangeCurrencyViewModel> getAllCurrencyCommercialByType(DateTime DateInit, DateTime DateFin)
        {
            return _context.EXCHANGE_CURRENCY_COMMERCIAL.Where(w => w.currency_date >= DateInit.Date && w.currency_date <= DateFin.Date).OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id_exchange_currency_commercial,
                Price = x.price,
                User = x.cuser,
                currency_type = "Comercial"
            }).ToList();
        }
        
        public List<ExchangeCurrencyViewModel> getAllCurrencyCommercialBySite(DateTime DateInit, DateTime DateFin, string siteCode)
        {
            return _context.EXCHANGE_CURRENCY_COMMERCIAL_SITES
                .Join(_context.EXCHANGE_CURRENCY_COMMERCIAL, ECCS => ECCS.id_exchange_currency_commercial, ECC => ECC.id_exchange_currency_commercial, (ECCS, ECC) => new { ECCS, ECC })
                .Where(w => w.ECCS.site_code == siteCode && w.ECC.currency_date >= DateInit.Date && w.ECC.currency_date <= DateFin.Date).OrderByDescending(x => x.ECC.currency_date).ToList()
                .Select(s => new ExchangeCurrencyViewModel
                {
                    Currency = "MX",
                    CurrencyDate = s.ECC.currency_date.ToShortDateString(),
                    Id = s.ECC.id_exchange_currency_commercial,
                    Price = s.ECC.price,
                    User = s.ECC.cuser,
                    currency_type = "Comercial"
                }).ToList();
        }

        public List<ExchangeCurrencyViewModel> getDateCurrency(DateTime DateInit, DateTime DateFin)
        {
            var ExchangeSistem = _context.EXCHANGE_CURRENCY.ToList().Where(x => x.currency_date.Date >= DateInit.Date && x.currency_date.Date <= DateFin.Date).OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id,
                Price = x.price,
                User = x.cuser,
                currency_type = "Banco"
            }).ToList();
            var ExchangeUser = _context.EXCHANGE_CURRENCY_COMMERCIAL.ToList().Where(x => x.currency_date.Date >= DateInit.Date && x.currency_date.Date <= DateFin.Date).OrderByDescending(x => x.currency_date).ToList().Select(x => new ExchangeCurrencyViewModel
            {
                Currency = "MX",
                CurrencyDate = x.currency_date.ToShortDateString(),
                Id = x.id_exchange_currency_commercial,
                Price = x.price,
                User = x.cuser,
                currency_type = "Comercial"
            }).ToList();
            ExchangeSistem.AddRange(ExchangeUser);
            ExchangeSistem = ExchangeSistem.OrderByDescending(x => x.CurrencyDate).ToList();
            return ExchangeSistem;
        }
        public int AddExchangeSite(EXCHANGE_CURRENCY_COMMERCIAL_SITES exchange_model)
        {
            try
            {
                _context.EXCHANGE_CURRENCY_COMMERCIAL_SITES.Add(exchange_model);
                _context.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public List<SiteModel> GetExchanceCurrencySites(int exchange_id)
        {
            return _context.EXCHANGE_CURRENCY_COMMERCIAL_SITES.Join(_context.SITES, s => s.site_code, ss => ss.site_code, (s, ss) => new { s, ss })
            .Where(w => w.s.id_exchange_currency_commercial == exchange_id)
            .Select(e => new SiteModel
            {
                SiteCode = e.ss.site_code,
                SiteName = e.ss.site_name
            }).ToList();
        }

        public decimal GetLastExchangeCurrent()
        {
            var valueEC = _context.EXCHANGE_CURRENCY.Where(w => w.status == true).FirstOrDefault();
            if (valueEC != null)
                return valueEC.price;
            else
            {
                var EC = _context.EXCHANGE_CURRENCY.Take(1).OrderByDescending(o => o.cdate).FirstOrDefault();
                if (EC != null)
                    return EC.price;
            }
            return 0;
        }

    }
}
