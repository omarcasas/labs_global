﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetTransferHeaderModel
    {
        public string document_no { get; set; }
        public string transfer_site_code { get; set; }
        public string transfer_site_code_name { get; set; }
        public string destination_site_code { get; set; }
        public string destination_site_code_name { get; set; }
        public DateTime? transfer_date { get; set; }
        public int? status { get; set; }
        public string status_str { get; set; }
    }
}

