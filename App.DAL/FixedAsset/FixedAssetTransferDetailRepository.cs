﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetTransferDetailRepository
    {
        private PRTSAIEntities _context;

        public FixedAssetTransferDetailRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool AddFixedAseet(FIXED_ASSET_TRANSFER_DETAIL transfer_detail)
        {
            try
            {
                _context.FIXED_ASSET_TRANSFER_DETAIL.Add(transfer_detail);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public int EditFixedAssetTransferDetail(FIXED_ASSET_TRANSFER_DETAIL transfer_detail)
        {
            _context.Entry(transfer_detail).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }


        public List<FIXED_ASSET_TRANSFER_DETAIL> GetDetailByDocument(string document_no)
        {
            return _context.FIXED_ASSET_TRANSFER_DETAIL.Where(w => w.document_no == document_no).ToList();
        }


    }
}
