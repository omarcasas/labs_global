﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetPurchaseOrderRepository
    {

        private PRTSAIEntities _context;

        public FixedAssetPurchaseOrderRepository()
        {
            _context = new PRTSAIEntities();
        }


        public int AddFixedAseetPurchase(FIXED_PURCHASE_ORDER fixed_purchase)
        {
            try
            {
                _context.FIXED_PURCHASE_ORDER.Add(fixed_purchase);
                _context.SaveChanges();
                return fixed_purchase.po_no;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public bool AddFixedPurchaseItem(FIXED_PURCHASE_ORDER_ITEM fixed_purchase_item)
        {
            try
            {
                _context.FIXED_PURCHASE_ORDER_ITEM.Add(fixed_purchase_item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

    }
}
