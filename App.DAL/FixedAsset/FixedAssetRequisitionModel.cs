﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetRequisitionModel
    {
        public int requisition_code { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
        public int? requisition_status { get; set; }
        public string requisition_status_str { get; set; }
        public DateTime? requisition_date { get; set; }
        public string requisition_date_string { get; set; }
        public DateTime? delivery_date { get; set; }
        public string delivery_date_string { get; set; }
        public string requisition_remark { get; set; }
    }
}
