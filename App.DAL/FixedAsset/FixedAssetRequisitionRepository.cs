﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetRequisitionRepository
    {
        private PRTSAIEntities _context;


        public FixedAssetRequisitionRepository()
        {
            _context = new PRTSAIEntities();
        }

        public FIXED_ASSET_REQUISITION GetFixedAssetRequisitionById(int id) => _context.FIXED_ASSET_REQUISITION.Where(w => w.requisition_code == id).FirstOrDefault();

        public List<FixedAssetRequisitionModel> GetFixedAssetRequisitionBySite(string site)
        {

            return _context.FIXED_ASSET_REQUISITION.Where(w => w.site_code == site)
                .Where(w => w.requisition_status == 1)
               .Select(s => new FixedAssetRequisitionModel
               {
                   requisition_code  = s.requisition_code,
                   requisition_date = s.requisition_date,
                   requisition_date_string = SqlFunctions.DatePart("day", s.requisition_date) + "/" + SqlFunctions.DatePart("m", s.requisition_date) + "/" + SqlFunctions.DatePart("year", s.requisition_date),
               })
               .ToList();
        }

        public int EditFixedAssetRequisition(FIXED_ASSET_REQUISITION fixed_asset_requisition)
        {
            _context.Entry(fixed_asset_requisition).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }


        public List<FixedAssetRequisitionModel> GetPendingRequisitionAsset()
        {
            var query = _context.FIXED_ASSET_REQUISITION.Where(w => w.requisition_status == 1).Select(s => new FixedAssetRequisitionModel
            {
                requisition_code = s.requisition_code,
                site_code = s.site_code,
                requisition_status = s.requisition_status,
                requisition_date = s.requisition_date,
                delivery_date = s.delivery_date,
                requisition_remark = s.requisition_remark
            }).ToList();

            foreach (var item in query)
            {
                switch (item.requisition_status)
                {
                    case 1:
                        item.requisition_status_str = "En espera";
                        break;
                    case 2:
                        item.requisition_status_str = "Aprobado";
                        break;
                    case 8:
                        item.requisition_status_str = "Cancelado";
                        break;
                    case 9:
                        item.requisition_status_str = "Terminado";
                        break;
                }
            }

            return query;
        }

    }
}
