﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetTransferDetailModel
    {
        public int? fixed_asset_id { get; set; }
        public string fixed_asset_folio { get; set; }
        public int? fixed_asset_code { get; set; }
        public string description { get; set; }
        public string model { get; set; }
        public string brand { get; set; }
    }
}
