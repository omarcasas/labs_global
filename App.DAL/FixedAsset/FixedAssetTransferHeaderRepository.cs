﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetTransferHeaderRepository
    {
        private PRTSAIEntities _context;

        public FixedAssetTransferHeaderRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool AddFixedAseet(FIXED_ASSET_TRANSFER_HEADER transfer_header)
        {
            try
            {
                _context.FIXED_ASSET_TRANSFER_HEADER.Add(transfer_header);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public int EditFixedAssetTransferHeader(FIXED_ASSET_TRANSFER_HEADER transfer_header)
        {
            _context.Entry(transfer_header).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetOwnFixedAssetTransferHeader()
        {
            return _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.transfer_site_code == "0011").ToList();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetOwnSiteFixedAssetTransferHeader(string site_code)
        {
            return _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.transfer_site_code == site_code).ToList();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetDestinationFixedAssetTransferHeader(string site_code)
        {
            return _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.transfer_site_code == site_code).ToList();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetFixedAssetTransferHeaderByStatusAndSite(string site_code, int status)
        {
            return _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.transfer_site_code == site_code && w.status == status).ToList();
        }

        public FIXED_ASSET_TRANSFER_HEADER GetHeaderByDocument(string document_no)
        {
            return _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.document_no == document_no).FirstOrDefault();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetPendingTransferHeader()
        {
            var site = "0011";
            var query = _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.destination_site_code == site && w.status == 4).ToList();

            return query;
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetReceivedTransferHeader()
        {
            var site = "0011";
            var query = _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.destination_site_code == site && w.status == 9).ToList();

            return query;
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetSendTransferHeader()
        {
            var site = "0011";
            var query = _context.FIXED_ASSET_TRANSFER_HEADER.Where(w => w.transfer_site_code == site).ToList();

            return query;
        }

    }
}
