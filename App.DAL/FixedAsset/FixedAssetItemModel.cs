﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetItemModel
    {
        public int fixed_asset_id { get; set; }
        public string fixed_asset_folio { get; set; }
        public Nullable<int> fixed_asset_code { get; set; }
        public string fixed_asset_name { get; set; }
        public string site_code { get; set; }
        public string description { get; set; }
        public string brand { get; set; }
        public string serie { get; set; }
        public string model { get; set; }
        public decimal cost { get; set; }
        public decimal iva { get; set; }
        public Nullable<System.DateTime> acquisition_date { get; set; }
        public Nullable<int> status_asset { get; set; }
        public Nullable<bool> requisition_flag { get; set; }
        public Nullable<int> requisiton_code { get; set; }
        public string cuser { get; set; }


    }
}
