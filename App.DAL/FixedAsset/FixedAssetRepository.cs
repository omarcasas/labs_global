﻿using App.Entities;
using App.Entities.ViewModels.FixedAsset;
using App.Entities.ViewModels.SysInfo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.FixedAsset
{
    public class FixedAssetRepository
    {
        private PRTSAIEntities _context;

        public FixedAssetRepository()
        {
            _context = new PRTSAIEntities();
        }


        public bool AddFixedAseet(FIXED_ASSET fixed_asset)
        {
            try
            {
                _context.FIXED_ASSET.Add(fixed_asset);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public int EditFixedAsset(FIXED_ASSET fixed_asset)
        {
            _context.Entry(fixed_asset).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public List<FIXED_ASSET> GetAllActiveFixedAsset()
        {
            return _context.FIXED_ASSET.Where(w => w.status_flag == true).ToList();
        }

        public List<FIXED_ASSET> GetAllFixedAsset()
        {
            return _context.FIXED_ASSET.ToList();
        }

        public FIXED_ASSET GetFixedAssetById(int id)
        {
            return _context.FIXED_ASSET.Where(w => w.fixed_asset_code == id).FirstOrDefault();
        }


        public StoreProcedureResult Proc_Next_Document(string document)
        {

            var DateReturn = _context
                .Database
                .SqlQuery<StoreProcedureResult>("" +

                "DECLARE @return_value int," +
                "@Document nvarchar(10) " +

                "EXEC  @return_value = [dbo].[sp_Get_Next_Document]" +
                "@Process_Name = N'" + document + "', " +
                "@Document = @Document OUTPUT SELECT 'Document'= @Document,'ReturnValue' = @return_value").ToList();

            return DateReturn[0];
        }

        public List<FixedAsserReportModel> GetFixedAssetReport(string site_code)
        {
            _context.Database.CommandTimeout = 1000000;
            var r = _context.Database.SqlQuery<FixedAsserReportModel>(@"DECLARE	@return_value int
            EXEC	@return_value = [dbo].[sp_Report_Fixed_Asset]
            @site_code = '" + site_code + @"'").ToList();
            return r;
        }


    }
}
