﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetItemRepository
    {
        private PRTSAIEntities _context;

        public FixedAssetItemRepository()
        {
            _context = new PRTSAIEntities();
        }


        public List<FIXED_ASSET_ITEM> GetItemsByRequisition(int id)
        {
            return _context.FIXED_ASSET_ITEM.Where(w => w.requisiton_code == id).ToList(); 
        }

        public FIXED_ASSET_ITEM AddItem(FIXED_ASSET_ITEM item)
        {
            try
            {
                _context.FIXED_ASSET_ITEM.Add(item);
                _context.SaveChanges();
                return item;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }
        }

        public FIXED_ASSET_ITEM GetItemById(int id)
        {
            return _context.FIXED_ASSET_ITEM.Where(w => w.fixed_asset_id == id).FirstOrDefault();
        }

        public int EditFixedAssetItem(FIXED_ASSET_ITEM fixed_asset)
        {
            _context.Entry(fixed_asset).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }


    }
}
