﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

public class UserPwdRepository
{
    private PRTSAIEntities _context;

    public UserPwdRepository()
    {
        _context = new PRTSAIEntities();
    }

    public USER_PWD GetUserPwdByEmployeeNumber(string employeeNumber)
    {
        return _context.USER_PWD.FirstOrDefault(x => x.emp_no == employeeNumber);
    }

    public USER_PWD GetUserPwdByEmployeeNumberFromStore(string employeeNumber ,string SiteCode)
    {
        try
        {
            var name = _context.SITES.Where(x => x.site_code == SiteCode).SingleOrDefault();
            string query = @"SELECT * FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_PWD] WHERE emp_no=@emp";
            var info = _context.Database.SqlQuery<USER_PWD>(query,
            new SqlParameter("emp", employeeNumber)).FirstOrDefault();
            return info;
        }
        catch (Exception)
        {
            return new USER_PWD();
        }
    }
    public int UpdateUserPwd(USER_PWD user)
    {
        _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
        return _context.SaveChanges();
    }
    public string UpdateStatusUser(string userNumber, string statusUser, string Sitecode)
    {
        var itemReturn = (dynamic)null;
        try
        {
            if (Sitecode != "")
            {
                string[] SiteS = Sitecode.Split(',');
                foreach (var item in SiteS)
                {
                    if (item != "")
                    {
                        itemReturn = item;
                        var name = _context.SITES.Where(x => x.site_code == item).SingleOrDefault();
                        string query = @"UPDATE [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_PWD] SET failed_attempts=0 ,status = @status WHERE emp_no=@emp";
                        var info = _context.Database.SqlQuery<List<string>>(query,
                        new SqlParameter("status", statusUser),
                        new SqlParameter("emp", userNumber)).ToList();

                    }
                }
                return "1";
            }
            else
            {
                var user = _context.USER_PWD.FirstOrDefault(x => x.emp_no == userNumber);
                user.status = statusUser;
                user.failed_attempts = 0;
                _context.SaveChanges();
                return "1";
            }

        }
        catch (Exception e)
        {
            return e.Message + "En tienda " + itemReturn;
        }

    }

}
