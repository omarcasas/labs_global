﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL
{
    public class SysRoleMasterRepository
    {
        private PRTSAIEntities _context;

        public SysRoleMasterRepository()
        {
            _context = new PRTSAIEntities();
        }
        public ICollection<SYS_ROLE_MASTER> GetAllRolesAdmin()
        {
            return _context.SYS_ROLE_MASTER.Select(x => x).ToList();
        }
        public ICollection<SYS_ROLE_MASTER_STORE> GetAllRolesAdminStore()
        {
            return _context.SYS_ROLE_MASTER_STORE.Select(x => x).ToList();
        }
        public SYS_ROLE_MASTER getRoleByID(string rolId)
        {
            return _context.SYS_ROLE_MASTER.FirstOrDefault(x => x.role_id == rolId);
        }
        public SYS_ROLE_MASTER_STORE getRoleByIDStore(string rolId)
        {
            return _context.SYS_ROLE_MASTER_STORE.FirstOrDefault(x => x.role_id == rolId);
        }
        public int AddRolesMaster(SYS_ROLE_MASTER rol)
        {
            try
            {
                _context.SYS_ROLE_MASTER.Add(rol);
                return _context.SaveChanges();

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int AddRolesMasterStore(SYS_ROLE_MASTER_STORE rol)
        {
            try
            {
                _context.SYS_ROLE_MASTER_STORE.Add(rol);
                return _context.SaveChanges();

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int updateRoleMaster(SYS_ROLE_MASTER rol)
        {
            _context.Entry(rol).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }
        public int updateRoleMasterStore(SYS_ROLE_MASTER_STORE rol)
        {
            _context.Entry(rol).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public SYS_ROLE_MASTER SearchRole(string id_role)
        {
            var role = _context.SYS_ROLE_MASTER.Where(w => w.role_id == id_role).FirstOrDefault();
            if (role != null)
                return role;
            else
                return null;
        }
        public SYS_ROLE_MASTER_STORE SearchRoleStore(string id_role)
        {
            var role = _context.SYS_ROLE_MASTER_STORE.Where(w => w.role_id == id_role).FirstOrDefault();
            if (role != null)
                return role;
            else
                return null;
        }

        public string GetCutEmails(List<string> StoreEmails)
        {
            string emails = "";
            if (StoreEmails.Count == 1)
            {
                emails = StoreEmails[0];
            }
            else
            {
                var lastEmail = StoreEmails.Last();
                foreach (var item in StoreEmails)
                {

                    if (lastEmail.Equals(item))
                    {
                        emails += item;
                    }
                    else
                    {
                        if (emails == "")
                            emails = item + ",";
                        else
                            emails = emails + item + ",";
                    }
                }
            }
            return emails;
        }

    }
}
