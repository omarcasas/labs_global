﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Linq;

public class UserMasterRepository
{
    private PRTSAIEntities _context;

    //constructor
    public UserMasterRepository()
    {
        _context = new PRTSAIEntities();
    }

    //functions
    public USER_MASTER GetUserMasterByEmployeeNumber(string employeeNumber)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.emp_no == employeeNumber);
    }

    public USER_MASTER GetUserMasterByUserName(string username)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.user_name == username);
    }

    public int AddUserMaster(USER_MASTER user)
    {
        try
        {
            _context.USER_MASTER.Add(user);
            return _context.SaveChanges();

        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return 0;
        }

    }

    public USER_MASTER GetUserMasterByEmail(string email)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.email == email);
    }

    public USER_MASTER GetUserMasterByUsername(string username)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.user_name == username);
    }

    public USER_MASTER GetUserMasterByUsernameOrEmail(string usernameOrEmail)
    {
        try
        {
            return _context.USER_MASTER.FirstOrDefault(x => x.user_name == usernameOrEmail || x.email == usernameOrEmail);
        }
        catch (EntityException ex)
        {
            var msg = ex.Message;
            return new USER_MASTER();
        }

    }

    public int DeleteUserMaster(USER_MASTER user)
    {
        _context.Entry(user).State = System.Data.Entity.EntityState.Deleted;
        return _context.SaveChanges();
    }

    public int UpdateUserMaster(USER_MASTER user)
    {
        _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
        return _context.SaveChanges();
    }

    public USER_MASTER GetUserMasterByRecoveryPasswordCode(string code)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.USER_PWD.pass_code.Equals(code));
    }
    public List<USER_MASTER> GetAllUsers()
    {
        return _context.USER_MASTER.Select(x => x).ToList();
    }
    public List<USER_MASTER> GetAllUsersFromStore(string SiteCode)
    {
        try
        {
            var name = _context.SITES.Where(x => x.site_code == SiteCode).SingleOrDefault();
            string query = $@"select * from ["+ name.ip_address+ "].[DFL_SAI_SITE].[dbo].user_master ";
            var info = _context.Database.SqlQuery<USER_MASTER>(query).ToList();
            return info;
        }
        catch (Exception ex)
        {
            return new List<USER_MASTER>();
        }
    }
    public string getFirstLastName(string user_name)
    {
        var name = _context.USER_MASTER.Where(x => x.user_name == user_name).SingleOrDefault();
        if (name != null)
            return name.last_name + " " + name.first_name;
        else
            return "Error al buscar el nombre.";
    }

    public bool DeletePicture(USER_MASTER model)
    {
        try
        {
            model.photo = null;
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
            return false;
        }
    }

    public int ReloadCountUser(USER_MASTER user)
    {
        try
        {
            var User = _context.USER_PWD.Find(user.emp_no);
            User.failed_attempts = 0;
            return _context.SaveChanges();
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return 0;
        }
    }

    public string GetEmailByUser(string user_name)
    {
        if (user_name != "")
        {
            var e = _context.USER_MASTER.Where(x => x.user_name == user_name).SingleOrDefault();
            return e.email;
        }
        else
            return "";
    }

    public string getCompleteName(string user_name)
    {
        var user = _context.USER_MASTER.Where(x => x.user_name == user_name).FirstOrDefault();
        if (user != null)
            return user.last_name + " " + user.first_name;
        else
            return user_name;
    }


    public int UpdateDepartmentUser(string userNumber, int deparment, string uuser)
    {
        try
        {
            var user = _context.USER_MASTER.FirstOrDefault(x => x.emp_no == userNumber);
            user.uuser = uuser;
            user.department_id = deparment;
            user.program_id = "ADMS0004";
            user.udate = DateTime.Now;
            _context.SaveChanges();
            return 1;
        }
        catch (Exception error)
        {
            var message = error.Message;
            return 0;
        }
    }
    public string MigrateUser(string userNumber,string SiteFrom, string SitesToMigrate)
    {
        var itemReturn = (dynamic)null;
        try
        {
            if (SitesToMigrate != "")
            {
                string[] SiteS = SitesToMigrate.Split(',');
                var From = _context.SITES.Where(x => x.site_code == SiteFrom).SingleOrDefault();
                foreach (var item in SiteS)
                {
                    if (item != "")
                    {
                        itemReturn = item;
                        var name = _context.SITES.Where(x => x.site_code == item).SingleOrDefault();
                        string query = @"INSERT INTO [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_MASTER] select * from [" + From.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_MASTER] where emp_no = @emp and emp_no not in (select emp_no from [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_MASTER] where emp_no = @emp)  INSERT INTO  [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_PWD] select * from [" + From.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_PWD] where emp_no = @emp and emp_no not in (select emp_no from [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[USER_PWD] where emp_no = @emp)  DELETE [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].SYS_ROLE_USER where emp_no  =@emp and role_id in(SELECT role_id FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].SYS_ROLE_USER AS A WHERE not exists (SELECT 1 FROM  [" + From.ip_address + "].[DFL_SAI_SITE].[dbo].SYS_ROLE_USER AS B WHERE A.role_id = B.role_id and A.emp_no = B.emp_no )and a.emp_no =@emp) insert into [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].SYS_ROLE_USER SELECT * FROM [" + From.ip_address + "].[DFL_SAI_SITE].[dbo].SYS_ROLE_USER AS A WHERE not exists (SELECT 1 FROM  [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].SYS_ROLE_USER AS B WHERE A.role_id = B.role_id and A.emp_no = B.emp_no )and a.emp_no =@emp";
                        var info = _context.Database.SqlQuery<List<string>>(query,
                        new SqlParameter("emp", userNumber)).ToList();
                    }
                }
                
            }
            return "1";

        }
        catch (Exception e)
        {
            return e.Message + "En tienda " + itemReturn;
        }

    }

}

