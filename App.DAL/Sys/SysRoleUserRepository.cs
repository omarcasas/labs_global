﻿using App.Entities;
using App.Entities.ViewModels.SysInfo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL
{
    public class SysRoleUserRepository
    {
        private PRTSAIEntities _context;

        public SysRoleUserRepository()
        {
            _context = new PRTSAIEntities();
        }
        public ICollection<SYS_ROLE_MASTER> GetAllRolesOfUser(string EmployeeNumber)
        {
            return _context.SYS_ROLE_MASTER.Where(x => _context.SYS_ROLE_USER.Any(v => v.role_id == x.role_id && v.emp_no == EmployeeNumber)).OrderBy(x => x.role_id).ThenByDescending(x => x.role_id).ToList();
        }
        public List<SYS_ROLE_MASTER> GetAllRolesOfUserFromStore(string EmployeeNumber, string SiteCode)
        {
            try
            {
                var name = _context.SITES.Where(x => x.site_code == SiteCode).SingleOrDefault();
                string query = @"SELECT * FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_USER] syru INNER JOIN [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_MASTER] syrm ON syru.role_id = syrm.role_id WHERE syru.emp_no=@emp order by role_name asc";
                var info = _context.Database.SqlQuery<SYS_ROLE_MASTER>(query,
                new SqlParameter("emp", EmployeeNumber)).ToList();
                return info;
            }
            catch (Exception ex)
            {
                return new List<SYS_ROLE_MASTER>();
            }
        }
        public ICollection<SYS_ROLE_MASTER> GetAllRolesAvailable(string EmployeeNumber)
        {
            return _context.SYS_ROLE_MASTER.Where(x => !_context.SYS_ROLE_USER.Any(v => v.role_id == x.role_id && v.emp_no == EmployeeNumber)).OrderBy(x => x.role_id).ThenByDescending(x => x.role_id).ToList();
        }
        public List<SYS_ROLE_MASTER> GetAllRolesAvailableFromStore(string EmployeeNumber, string SiteCode)
        {
            try
            {
                var name = _context.SITES.Where(x => x.site_code == SiteCode).SingleOrDefault();
                string query = @"SELECT * FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_MASTER] WHERE role_id not in(SELECT syru.role_id FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_USER] syru INNER JOIN [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_MASTER] syrm ON syru.role_id = syrm.role_id WHERE SYRU.emp_no=@emp )order by role_name asc";
                var info = _context.Database.SqlQuery<SYS_ROLE_MASTER>(query,
                new SqlParameter("emp", EmployeeNumber)).ToList();
                return info;
            }
            catch (Exception ex)
            {
                return new List<SYS_ROLE_MASTER>();
            }
        }
        public int AddRolesToUser(string EmployeeNumber, string Roles , string user)
        {
            try
            {
                var pagesArray = Roles.Split(',');
                var list = _context.SYS_ROLE_MASTER.Where(x => pagesArray.Contains(x.role_id) && !x.SYS_ROLE_USER.Any(y => y.emp_no == EmployeeNumber && pagesArray.Contains(y.role_id))).ToList();

                foreach (var id in list)
                {
                    SYS_ROLE_USER role = new SYS_ROLE_USER();
                    role.role_id = id.role_id;
                    role.emp_no = EmployeeNumber;
                    role.cdate = DateTime.Now;
                    role.cuser = user;
                    role.program_id = "ADMS004.cshtml";

                    _context.SYS_ROLE_USER.Add(role);
                    _context.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int AddRolesToUserOnStore(string EmployeeNumber, string Roles, string user ,string SiteCode)
        {
            try
            {

                Roles = Roles.Replace(",", "','").Replace("','", "','");
                Roles = "'" + Roles + "'";
                var name = _context.SITES.Where(x => x.site_code == SiteCode).SingleOrDefault();
                string query = @"INSERT INTO  [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_USER] SELECT role_id, @emp as emp_no,GETDATE(),@user  as cuser,null as udate,null as uuser,'ADMS004.cshtml' as program_id  FROM  [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_MASTER] WHERE role_id IN (" + Roles + ") AND role_id NOT IN (SELECT role_id FROM   [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_USER] WHERE emp_no = @emp  AND role_id IN ("+ Roles +"))";
                var info = _context.Database.SqlQuery <List<string>>(query,
                new SqlParameter("emp", EmployeeNumber),
                new SqlParameter("user", user)).ToList();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int RemoveRolesFromUser(string EmployeeNumber, string roles)
        {
            try
            {
                var pagesArray = roles.Split(',');
                _context.SYS_ROLE_USER.RemoveRange(_context.SYS_ROLE_USER.Where(x => pagesArray.Contains(x.role_id) && x.emp_no == EmployeeNumber));
                _context.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public int RemoveRolesFromUserOnStore(string EmployeeNumber, string Roles, string SiteCode)
        {
            try
            {

                Roles = Roles.Replace(",", "','").Replace("','", "','");
                Roles = "'" + Roles + "'";
                var name = _context.SITES.Where(x => x.site_code == SiteCode).SingleOrDefault();
                string query = @"DELETE FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_USER] WHERE emp_no = @emp AND role_id IN (SELECT role_id FROM [" + name.ip_address + "].[DFL_SAI_SITE].[dbo].[SYS_ROLE_USER]WHERE emp_no = @emp AND role_id IN (" + Roles + "))";
                var info = _context.Database.SqlQuery<List<string>>(query,
                new SqlParameter("emp", EmployeeNumber)).ToList();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public List<SysPageMaster> GetPagesByUser(string emp_no)
        {
            var x = _context.SYS_ROLE_USER.Join(_context.SYS_ROLE_PAGE, SRU => SRU.role_id, SRP => SRP.role_id, (SRU, SRP) => new { SRU, SRP })
                .Join(_context.SYS_PAGE_MASTER, SPM2 => SPM2.SRP.page_id, SPM => SPM.page_id, (SPM2, SPM) => new { SPM2, SPM })
                .Where(w => w.SPM2.SRU.emp_no == emp_no && w.SPM.active_flag == true)
                .Select(s => new SysPageMaster
                {
                    page_name = s.SPM.page_name,
                    url = s.SPM.url,
                    description = s.SPM.description
                }).Distinct().ToList();
            return x;
        }

        public string GetBuyerDivision(string user_name)
        {
            var obj = _context.USER_MASTER.Where(w => w.user_name == user_name).FirstOrDefault();
            return obj.buyer_division;
        }
    }
}
