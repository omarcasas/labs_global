﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
namespace App.DAL.CommonAccess
{
    public class MaClassRepository
    {
        private PRTSAIEntities _context;
        public MaClassRepository()
        {
            _context = new PRTSAIEntities();
        }
        public List<MA_CLASS> GetMaClassDivision(int parent,int level)
        {
            return _context.MA_CLASS.Where(x => x.level_category == level && x.parent_class == parent).ToList();
        }
        public List<MA_CLASS> GetMaClassDivisionForLevel(int level)
        {
            return _context.MA_CLASS.Where(x => x.level_category == level).ToList();
        }
        public int AddMaClass(MA_CLASS Category)
        {
            try
            {
                _context.MA_CLASS.Add(Category);
                return _context.SaveChanges();

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }

        }
        public List<MA_CLASS> GetAllMaClass()
        {
            return _context.MA_CLASS.ToList();
        }
        /*busqueda en caso de producto*/
        public List<MaClassViewModel> GetItemMaClass(string PartNumber)
        {
            var Item =_context.ITEM.Where(x => x.part_number == PartNumber).SingleOrDefault();
            var Maclass = new List<MaClassViewModel>();
            if (Item != null)
            {
                Maclass = _context.MA_CLASS.Where(x => x.level_category == Item.level_header && x.parent_class == 0).Select(x => new MaClassViewModel
                {
                    class_id = x.class_id,
                    class_name = x.class_name
                }).ToList();
            }
            return Maclass;
        }

        public List<MaClassViewModel> GetItemMaClass2(string PartNumber)
        {
            var Item = _context.ITEM.Where(x => x.part_number == PartNumber).SingleOrDefault();
            var Maclass = new List<MaClassViewModel>();
            if (Item != null)
            {
                Maclass = _context.MA_CLASS.Where(x => x.level_category == Item.level1_code && x.parent_class == Item.level_header ).Select(x => new MaClassViewModel
                {
                    class_id = x.class_id,
                    class_name = x.class_name
                }).ToList();
            }
            return Maclass;
        }
        /**/
        /*cuando seleccionas una categoria*/
        public List<MaClassViewModel> GetMaClassPrimary(int MaClassId)
        {
            var MaClass = _context.MA_CLASS.Where(x => x.class_id == MaClassId).SingleOrDefault();
            var ListMaclass = new List<MaClassViewModel>();
            if (MaClass != null)
            {
                ListMaclass = _context.MA_CLASS.Where(x => x.parent_class == MaClass.level_category).Select(x => new MaClassViewModel {
                    class_id = x.class_id,
                    class_name = x.class_name
                }).ToList();
            }
            return ListMaclass;
        }
        public List<MaClassViewModel> GetMaClassSecundary(int MaClassId)
        {
            var MaClass = _context.MA_CLASS.Where(x => x.class_id == MaClassId).SingleOrDefault();
            var ListMaclass = new List<MaClassViewModel>();
            if (MaClass != null)
            {
                ListMaclass = _context.MA_CLASS.Where(x => x.level_category == MaClass.parent_class).Select(x => new MaClassViewModel
                {
                    class_id = x.class_id,
                    class_name = x.class_name
                }).ToList();
            }
            return ListMaclass;
        }
        /**/
        /*filtro de productos mediante categoria*/
        public List<ItemMasterModel> GetProductMaClass(int MaClassId)
        {
            var MaClass = _context.MA_CLASS.Where(x => x.class_id == MaClassId).SingleOrDefault();
            var Items = new List<ItemMasterModel>();
            if (MaClass != null)
            {
                Items = _context.ITEM.Where(x => x.level_header == MaClass.level_category).Select(x => new ItemMasterModel
                {
                    part_description = x.part_description,
                    part_number = x.part_number
                }).ToList();
            }
            return Items;
        }
        public List<ItemMasterModel> GetProductMaClass2(int MaClassId)
        {
            var MaClass = _context.MA_CLASS.Where(x => x.class_id == MaClassId).SingleOrDefault();
            var Items = new List<ItemMasterModel>();
            if (MaClass != null)
            {
                Items = _context.ITEM.Where(x => x.level1_code == MaClass.level_category).Select(x => new ItemMasterModel
                {
                    part_description = x.part_description,
                    part_number = x.part_number
                }).ToList();
            }
            return Items;
        }
        /**/
        public List<MA_CLASS> GetAllDepartments(int level1)
        {
            return _context.MA_CLASS.Where(x => x.level_category == level1).ToList();
        }
        public List<MA_CLASS> GetAllFamilyByParentClass(int level2)
        {
            return _context.MA_CLASS.Where(x => x.parent_class == level2).ToList();
        }
    }
}
