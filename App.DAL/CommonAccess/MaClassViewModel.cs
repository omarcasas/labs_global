﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CommonAccess
{
    public class MaClassViewModel
    {
        public int class_id { get; set; }
        public string class_name { get; set; }
    }
}
