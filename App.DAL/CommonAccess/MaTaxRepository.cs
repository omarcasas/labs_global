﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CommonAccess
{
    public class MaTaxRepository
    {
        private PRTSAIEntities _context;

        public MaTaxRepository()
        {
            _context = new PRTSAIEntities();
        }
        public List<MA_TAX> GetAllIva()
        {
            return _context.MA_TAX.Where(x => x.tax_type == "1" || x.tax_type == "0").OrderBy(x => x.name).ThenByDescending(x => x.name).ToList();
        }
        public List<MA_TAX> GetAllIeps()
        {
            return _context.MA_TAX.Where(x => x.tax_type == "2" || x.tax_type == "0").OrderBy(x => x.name).ThenByDescending(x => x.name).ToList();
        }
        public List<MA_TAX> GetAllIVANormal()
        {
            return _context.MA_TAX.Where(x => (x.tax_type == "1" || x.tax_type == "0") && (x.type_stc == "001" || x.type_stc == "002") && x.tax_code != "EXE_0" && x.tax_code != "IRS").OrderBy(x => x.name).ThenByDescending(x => x.name).ToList();
        }
        public MA_TAX GetMaTaxByCode(string Code)
        {
            return _context.MA_TAX.Where(x => x.tax_code == Code).SingleOrDefault();
        }
        public StoreProcedureIvaIeps StoreProcedureIvaIeps(string PartNumber)
        {
            var item = _context.Database.SqlQuery<StoreProcedureIvaIeps>("DECLARE	@return_value int, @iva_value decimal(18, 4), @ieps_value decimal(18, 4) EXEC    @return_value = [dbo].[sp_GetIvaIepsPartNumber] @part_number = @partNumber, @iva_value = @iva_value OUTPUT, @ieps_value = @ieps_value OUTPUT SELECT  (case when @iva_value is null then 0 else @iva_value end) as  'Iva', (case when @ieps_value is null then 0 else @ieps_value end) as 'Ieps'", new SqlParameter("partNumber", PartNumber)).SingleOrDefault();
            return item;
        }
    }
}
