﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities;
using App.Common;
using App.Entities.ViewModels.Expenses;
using System.Data.SqlClient;
using System.Data;

namespace App.DAL.Expenses
{
    public class ExpensesRepository
    {
        private PRTSAIEntities _context;

        public ExpensesRepository()
        {
            _context = new PRTSAIEntities();
        }

        public Result<Empty> ExpenseStart(INTERNAL_REQUIREMENT_HEADER model ,IEnumerable<INTERNAL_REQUIREMENT_DETAIL> details, string processName)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.INTERNAL_REQUIREMENT_HEADER.Add(model);
                    _context.SaveChanges();
                    details = details.Select(e =>
                    {
                         e.folio = model.folio;
                         return e;
                    });
                    _context.INTERNAL_REQUIREMENT_DETAIL.AddRange(details);
                    _context.SaveChanges();

                    var user = _context.USER_MASTER.Single(e => e.user_name == model.cuser);
                    
                    var history = new APPROVAL_HISTORY
                    {
                        process_name = processName,
                        emp_no = user.emp_no,
                        step_level = 1,
                        approval_type = "req",
                        approval_status = "aprobado",
                        remark = "",
                        approval_datetime = DateTime.Now,
                        program_id = model.program_id
                    };

                    _context.APPROVAL_HISTORY.Add(history);
                    _context.SaveChanges();
                    //var oldCurrent = _context.INTERNAL_REQUIREMENT_APPROVALS
                    //    .FirstOrDefault(e => e.folio_requirement == model.folio
                    //                         && e.approval_id == history.approval_id);
                    //if (oldCurrent != null)
                    //{
                    //    oldCurrent.current_status_flag = false;
                    //}

                    //var reqHistory = new INTERNAL_REQUIREMENT_APPROVALS
                    //{
                    //    folio_requirement = model.folio,
                    //    approval_id = history.approval_id,
                    //    current_status_flag = true
                    //};
                    //_context.SaveChanges();
                    trans.Commit();
                    return Result.OK();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return Result.Error("Fallo en transacción");
                }
            }
            
        }
        public List<ExpensesReportModel> ExpensesReport(string Site_code, int SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate)
        {
            var site_code = new SqlParameter { ParameterName = "SITEID", Value = Site_code };
            var supplierid = new SqlParameter { ParameterName = "SUPPLIERID", Value = SupplierId };
            var category = new SqlParameter { ParameterName = "CATEGORYID", Value = Category };
            var begindate = new SqlParameter { ParameterName = "BEGINDATE", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = BeginDate };
            var enddate = new SqlParameter { ParameterName = "ENDDATE", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = EndDate };

            if (BeginDate.HasValue)
                begindate.Value = begindate.Value;
            else
                begindate.Value = DBNull.Value;

            if (EndDate.HasValue)
                enddate.Value = enddate.Value;
            else
                enddate.Value = DBNull.Value;

            var model = _context.Database.SqlQuery<ExpensesReportModel>("sp_Get_ExpenseseReport @SITEID, @SUPPLIERID, @CATEGORYID, @BEGINDATE, @ENDDATE", site_code, supplierid, category, begindate, enddate).ToList();

            return model;
        }

        public List<ExpensesReportModel> ExpensesReportNew(string Site_code, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status)
        {
            var consultaGenerico = @" SELECT XH.xml_id 'ReferenceFolio', S.site_name 'SiteName',  E.expense_category 'Category', XH.invoice_no 'Invoice', 
		        CASE WHEN E.reference_type = 'GASTO FIJO' THEN   E.remark
		        WHEN E.reference_type != 'GASTO FIJO' THEN  E.reference_type END AS 'ReferenceType',  MS.business_name 'Supplier', E.cdate 'InvoiceDate', XH.subtotal 'SubTotal', XH.iva 'TotalIVA', XH.ieps 'TotalIEPS', XH.currency 'Currency', XH.discount 'Discount', XH.iva_retained 'IvaRet', XH.isr_retained 'IsrRet', XH.total_amount'TotalAmount' 
		        FROM EXPENSE E
		        JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
		        JOIN SITES S on S.site_code = E.site_code
		        JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor' 
                WHERE convert(varchar, E.cdate, 23)  BETWEEN convert(varchar,@BEGINDATE, 23) AND convert(varchar,  @ENDDATE, 23) ";
            var consultaGroupBy = " GROUP BY XH.xml_id, S.site_name, E.expense_category, XH.invoice_no, E.remark, E.reference_type, XH.invoice_no , XH.subtotal, MS.business_name, E.cdate , XH.total_amount, XH.iva, XH.ieps, XH.currency, XH.discount, XH.iva_retained, XH.isr_retained ";

            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            if (BeginDate.HasValue)
            {
                firstDayOfMonth = BeginDate.Value;
                lastDayOfMonth = EndDate.Value;
            }

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BEGINDATE", firstDayOfMonth.Date));
            parameters.Add(new SqlParameter("@ENDDATE", lastDayOfMonth.Date));
            if (!string.IsNullOrWhiteSpace(Site_code))
            {
                consultaGenerico += " AND E.site_code = @SITEID ";
                parameters.Add(new SqlParameter("@SITEID", Site_code));//Destino
            }
            if (!string.IsNullOrWhiteSpace(SupplierId))
            {
                consultaGenerico += " AND XH.supplier_id = @SUPPLIERID ";
                parameters.Add(new SqlParameter("@SUPPLIERID", Convert.ToInt32(SupplierId)));
            }
            if (!string.IsNullOrWhiteSpace(Category))
            {
                consultaGenerico += " AND E.reference_type = @CATEGORYID ";
                parameters.Add(new SqlParameter("@CATEGORYID", Category));
            }
            if (!string.IsNullOrWhiteSpace(Currency))
            {
                consultaGenerico += " AND XH.currency = @CURRENCY ";
                parameters.Add(new SqlParameter("@CURRENCY", Currency));
            }
            if (!string.IsNullOrWhiteSpace(typeExpense))
            {
                consultaGenerico += " AND E.expense_category = @TYPEEXPENSE ";
                parameters.Add(new SqlParameter("@TYPEEXPENSE", typeExpense));
            }
            return _context.Database.SqlQuery<ExpensesReportModel>(consultaGenerico + consultaGroupBy, parameters.ToArray()).ToList();
        }


        public List<ExpensesReportModel> ExpensesReportDetailNew(string Site_code, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status, bool detail)
        {
            var consultaGenerico = @" SELECT XH.xml_id 'ReferenceFolio', S.site_name 'SiteName',  E.expense_category 'Category', XH.invoice_no 'Invoice', 
                CASE WHEN E.reference_type = 'GASTO FIJO' THEN   E.remark
                WHEN E.reference_type != 'GASTO FIJO' THEN  E.reference_type END AS 'ReferenceType',  MS.business_name 'Supplier', E.cdate 'InvoiceDate', XD.item_no 'PartNumber', XD.item_description 'Description', XD.quantity 'Quantity', XD.unit_cost 'UnitCost', XD.item_amount 'SubTotal', XD.discount 'Discount', XD.iva_amount 'TotalIVA', XD.ieps_amount 'TotalIEPS', XH.currency 'Currency', XD.iva_retained 'IvaRet', XD.isr_retained 'IsrRet', (XD.item_amount + XD.iva_amount + XD.ieps_amount - XD.discount - ( XD.iva_retained + XD.isr_retained ) ) 'TotalAmount' 
                FROM EXPENSE E
                JOIN XML_HEADER XH ON XH.expense_id = e.expense_id ";
                
            var consultaGenerico2 = @"
                JOIN SITES S on S.site_code = E.site_code
                JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
                WHERE convert(varchar, E.cdate, 23)  BETWEEN convert(varchar,@BEGINDATE, 23) AND convert(varchar,  @ENDDATE, 23)";

                consultaGenerico += "JOIN XML_DETAIL XD ON XD.xml_id = XH.xml_id";

            consultaGenerico += consultaGenerico2;

            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            if (BeginDate.HasValue)
            {
                firstDayOfMonth = BeginDate.Value;
                lastDayOfMonth = EndDate.Value;
            }

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BEGINDATE", firstDayOfMonth.Date));
            parameters.Add(new SqlParameter("@ENDDATE", lastDayOfMonth.Date));
            if (!string.IsNullOrWhiteSpace(Site_code))
            {
                consultaGenerico += " AND E.site_code = @SITEID ";
                parameters.Add(new SqlParameter("@SITEID", Site_code));//Destino
            }
            if (!string.IsNullOrWhiteSpace(SupplierId))
            {
                consultaGenerico += " AND XH.supplier_id = @SUPPLIERID ";
                parameters.Add(new SqlParameter("@SUPPLIERID", Convert.ToInt32(SupplierId)));
            }
            if (!string.IsNullOrWhiteSpace(Category))
            {
                consultaGenerico += " AND E.reference_type = @CATEGORYID ";
                parameters.Add(new SqlParameter("@CATEGORYID", Category));
            }
            if (!string.IsNullOrWhiteSpace(Currency))
            {
                consultaGenerico += " AND XH.currency = @CURRENCY ";
                parameters.Add(new SqlParameter("@CURRENCY", Currency));
            }
            if (!string.IsNullOrWhiteSpace(typeExpense))
            {
                consultaGenerico += " AND E.expense_category = @TYPEEXPENSE ";
                parameters.Add(new SqlParameter("@TYPEEXPENSE", typeExpense));
            }
            var xx = _context.Database.SqlQuery<ExpensesReportModel>(consultaGenerico, parameters.ToArray()).ToList();
            return xx;
        }


        public List<ExpensesChart> CreditorsExpensive(string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT TOP 5 MS.business_name AS ColumnString, SUM(XH.total_amount) AS Value FROM EXPENSE E
	                JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	                JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
	                WHERE E.site_code = @siteCode
	                GROUP BY MS.business_name
	                ORDER BY Value DESC",
                new SqlParameter("siteCode", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            //return _context.EXPENSE
            //    .Join(_context.XML_HEADER, xh => xh.expense_id, xhh => xhh.expense_id, (xh, xhh) => new { xh, xhh })
            //    .Join(_context.MA_SUPPLIER, m => m.xhh.supplier_id, mm => mm.supplier_id, (m, mm) => new { m, mm })
            //    .Where(w => w.m.xh.site_code == site_code && w.mm.supplier_type == "Acreedor")
            //    .GroupBy(g => new { g.mm.business_name, g.m.xhh.total_amount })
            //    .OrderByDescending(o => o.Key.total_amount)
            //    .Select(s => new ExpensesChart
            //    {
            //        Column = s.Key.business_name,
            //        Value = s.Sum(ss => ss.m.xhh.total_amount) ?? 0
            //    }).Take(5).ToList();
        }

        public List<ExpensesChart> CreditorsExpensiveQuantity(string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT TOP 5 MS.business_name AS ColumnString, SUM(XD.quantity) AS Value FROM EXPENSE E
	            JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	            JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
	            JOIN XML_DETAIL XD ON XD.xml_id = XH.xml_id 
	                WHERE E.site_code = @siteCode
	                GROUP BY MS.business_name
	                ORDER BY Value DESC",
                new SqlParameter("siteCode", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            //return _context.EXPENSE
            //    .Join(_context.XML_HEADER, xh => xh.expense_id, xhh => xhh.expense_id, (xh, xhh) => new { xh, xhh })
            //    .Join(_context.MA_SUPPLIER, m => m.xhh.supplier_id, mm => mm.supplier_id, (m, mm) => new { m, mm })
            //    .Join(_context.XML_DETAIL, xd => xd.m.xhh.xml_id, xdd => xdd.xml_id, (xd, xdd) => new { xd, xdd })
            //    .Where(w => w.xd.m.xh.site_code == site_code && w.xd.mm.supplier_type == "Acreedor")
            //    .GroupBy(g => new { g.xd.mm.business_name, g.xdd.quantity })
            //    .OrderByDescending(o => o.Key.quantity)
            //    .Select(s => new ExpensesChart { Column = s.Key.business_name, Value = s.Sum(ss => ss.xdd.quantity) ?? 0 })
            //    .Take(5).ToList();
        }

        public List<ExpensesChart> CreditorsExpensiveReferenceType(string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT DISTINCT E.reference_type AS ColumnString, SUM(XH.total_amount) AS Value FROM EXPENSE E
	            JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	                WHERE E.site_code = @siteCode
	                	GROUP BY E.reference_type
	                    ORDER BY Value DESC",
                new SqlParameter("siteCode", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            //return _context.EXPENSE
            //    .Join(_context.XML_HEADER, xh => xh.expense_id, xhh => xhh.expense_id, (xh, xhh) => new { xh, xhh })
            //    .Where(w => w.xh.site_code == site_code)
            //    .GroupBy(g => new { g.xh.reference_type, g.xhh.total_amount })
            //    .OrderByDescending(o => o.Key.total_amount)
            //    .Select(s => new ExpensesChart { Column = s.Key.reference_type, Value = s.Sum(ss => ss.xhh.total_amount) ?? 0 })
            //    .ToList();
        }

        public List<ExpensesChart> CreditorsExpensiveAll()
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT TOP 5MS.business_name AS ColumnString, SUM(XH.total_amount) AS Value FROM EXPENSE E
	            JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	            JOIN SITES S on S.site_code = E.site_code
	            JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
	            GROUP BY MS.business_name
	            ORDER BY Value DESC").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            //return _context.EXPENSE
            //    .Join(_context.XML_HEADER, xh => xh.expense_id, xhh => xhh.expense_id, (xh, xhh) => new { xh, xhh })
            //    .Join(_context.MA_SUPPLIER, m => m.xhh.supplier_id, mm => mm.supplier_id, (m, mm) => new { m, mm })
            //    .Where(w => w.mm.supplier_type == "Acreedor")
            //    .GroupBy(g => new { g.mm.business_name, g.m.xhh.total_amount })
            //    .OrderByDescending(o => o.Key.total_amount)
            //    .Select(s => new ExpensesChart
            //    {
            //        Column = s.Key.business_name,
            //        Value = s.Sum(ss => ss.m.xhh.total_amount) ?? 0
            //    }).Take(5).ToList();
        }

        public List<ExpensesChart> CreditorsExpensiveQuantityAll()
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT TOP 5 MS.business_name AS ColumnString, SUM(XD.quantity) AS Value FROM EXPENSE E
	                JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	                JOIN SITES S on S.site_code = E.site_code
	                JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
	                JOIN XML_DETAIL XD ON XD.xml_id = XH.xml_id
	                GROUP BY MS.business_name
	                ORDER BY Value DESC").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            //return _context.EXPENSE
            //    .Join(_context.XML_HEADER, xh => xh.expense_id, xhh => xhh.expense_id, (xh, xhh) => new { xh, xhh })
            //    .Join(_context.MA_SUPPLIER, m => m.xhh.supplier_id, mm => mm.supplier_id, (m, mm) => new { m, mm })
            //    .Join(_context.XML_DETAIL, xd => xd.m.xhh.xml_id, xdd => xdd.xml_id, (xd, xdd) => new { xd, xdd })
            //    .Where(w => w.xd.mm.supplier_type == "Acreedor")
            //    .GroupBy(g => new { g.xd.mm.business_name, g.xdd.quantity })
            //    .OrderByDescending(o => o.Key.quantity)
            //    .Select(s => new ExpensesChart { Column = s.Key.business_name, Value = s.Sum(ss => ss.xdd.quantity) ?? 0 })
            //    .Take(5).ToList();
        }

        public List<ExpensesChart> CreditorsExpensiveReferenceTypeAll()
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT DISTINCT E.reference_type AS ColumnString, SUM(XH.total_amount) AS Value FROM EXPENSE E
	                JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	                GROUP BY E.reference_type
	                ORDER BY Value DESC").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
            //return _context.EXPENSE
            //    .Join(_context.XML_HEADER, xh => xh.expense_id, xhh => xhh.expense_id, (xh, xhh) => new { xh, xhh })
            //    //.Where(w => w.xh.site_code == site_code)
            //    .GroupBy(g => new { g.xh.reference_type, g.xhh.total_amount })
            //    .OrderByDescending(o => o.Key.total_amount)
            //    .Select(s => new ExpensesChart { Column = s.Key.reference_type, Value = s.Sum(ss => ss.xhh.total_amount) ?? 0 })
            //    .Distinct().Take(5)
            //    .ToList();
        }

        public List<ItemXMLDetail> getAllItemsById(int xml_id)
        {
            return _context.XML_DETAIL.Where(x => x.xml_id == xml_id)
                .Select(s => new ItemXMLDetail
                {
                    id = s.id, 
                    xml_id = s.xml_id ?? 0,
                    item_no = s.item_no,
                    item_description = s.item_description,
                    quantity = s.quantity ?? 0,
                    unit_cost = s.unit_cost ?? 0,
                    item_amount = s.item_amount ?? 0,
                    discount = s.discount ?? 0
                }).ToList();
        }
        public ItemXMLDetail getAllItemHeaderById(int xml_id)
        {
            var ItemHeaderXml = _context.XML_HEADER
                .Join(_context.EXPENSE, x => x.expense_id, e => e.expense_id, (x, e) => new { x, e })
                .Where(w => w.x.xml_id == xml_id)
                .Select(s => new ItemXMLDetail
                {
                    id = s.x.xml_id,
                    invoice_date = s.x.invoice_date,
                    invoice = s.x.invoice_no,
                    supplier_id = s.x.supplier_id ?? 0,
                    subtotal = s.x.subtotal ?? 0,
                    iva = s.x.iva ?? 0,
                    ieps = s.x.ieps ?? 0,
                    iva_retained = s.x.iva_retained ?? 0,
                    isr_retained = s.x.isr_retained ?? 0,
                    discount = s.x.discount ?? 0,
                    total_amount = s.x.total_amount ?? 0,
                    currency = s.x.currency,
                    cdate = s.x.cdate,
                    expense_type = s.e.reference_type,
                    expense_type_exactly = s.e.remark,
                    cuser = s.e.cuser
                }).FirstOrDefault();
            return ItemHeaderXml;
        }
        public EXPENSE CreateExpense(EXPENSE expense, List<XML_HEADER> xml_header)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.EXPENSE.Add(expense);
                    _context.SaveChanges();
                    foreach (var d in xml_header)
                    {
                        d.expense_id = expense.expense_id;
                    }
                    _context.XML_HEADER.AddRange(xml_header);
                    _context.SaveChanges();


                    trans.Commit();
                    return expense;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return null;
                }
            }
        }
    }

    public enum ExpenseType
    {
        Normal,Distrital,NewProjects
    }
}
