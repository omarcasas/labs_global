﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Department
{
    public class MaClassRepository
    {
        private PRTSAIEntities _context;
        public MaClassRepository()
        {
            _context = new PRTSAIEntities();
        }
        public List<MaClassModel> departament(int Supplier)
        {
            var Departament = _context.ITEM_SUPPLIER.Join(_context.ITEM, its => its.part_number,
                it => it.part_number, (its, it) => new { its, it })
                .Join(_context.MA_CLASS, x => x.it.level_header,
                mc => mc.level_category, (x, mc) => new { x, mc })
                .Where(s => s.x.its.supplier_id == Supplier)
                .GroupBy(u => new {u.mc.level_category, u.mc.class_name })
                .OrderBy(u => u.Key.class_name)
                .Select(mcm => new MaClassModel
                {
                    ClassName = mcm.Key.class_name,
                    LevelCategory = mcm.Key.level_category.Value

                }).ToList();

            return Departament;
        }
    }

}
