﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities;

namespace App.DAL.Barcodes
{
    public class BarcodesRepository
    {
        private readonly PRTSAIEntities _context;

        public BarcodesRepository()
        {
            _context = new PRTSAIEntities();
        }
        public bool BarcodeExist(string barcode)
        {
            return _context.BARCODES.Count(b => b.part_barcode == barcode) > 0;
        }

        public int AddBarcode(BARCODES barcode)
        {
            try
            {
                _context.BARCODES.Add(barcode);
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

    }
}
