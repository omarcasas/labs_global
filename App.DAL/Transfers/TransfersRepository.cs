﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Transfers;
using System.Data.SqlClient;
using System.Data.Entity;

namespace App.DAL.Transfers
{
    public class TransfersRepository
    {
        private PRTSAIEntities _contextDFLSAI;
        public TransfersRepository()
        {
            _contextDFLSAI = new PRTSAIEntities();
        }

        public List<TransferHeaderModel> GetTransfersByDateAndTransferSite(string site_code, DateTime dateInitial, DateTime dateFinish)
        {
            var list = _contextDFLSAI.Database.SqlQuery<TransferHeaderModel>(@"
            SELECT TH.transfer_document , ST.site_name as 'transfer_site' , SD.site_name as 'destination_site' ,
            convert(varchar,TH.transfer_date, 103) AS 'transfer_date' ,
            TH.transfer_status , ISNULL(convert(varchar,TH.transfer_date_in, 103),'') as 'transfer_date_in'  ,
			sUM((TD.quantity * TD.cost) + (TD.quantity * TD.iva) + (TD.quantity * TD.ieps)) AS 'total',
			SUM(TD.quantity * TD.cost) as 'importe',
			SUM(TD.quantity * TD.iva) AS 'IVA',
			SUM(TD.quantity * TD.ieps) AS 'IEPS'
            FROM TRANSFER_HEADER TH
			JOIN TRANSFER_DETAIL TD ON TD.transfer_document = TH.transfer_document
            LEFT JOIN SITES ST ON ST.site_code = TH.transfer_site_code
            LEFT JOIN SITES SD ON SD.site_code = TH.destination_site_code
            WHERE TH.transfer_date BETWEEN @dateInitial AND @dateFinish
            AND TH.transfer_site_code = @site_code
            AND TH.transfer_status IN (4,8,9)
			GROUP BY TH.transfer_document , ST.site_name , SD.site_name , TH.transfer_date , TH.transfer_status , TH.transfer_date_in
"
            , new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish), new SqlParameter("site_code", site_code)).ToList();
            return list;
        }
        public List<TransferDetailReportModel> GetAllTransferDetail(DateTime date1, DateTime date2, string TransferType, string status, string originSite, string partNumber, string department, string family, string mainSite)
        {
            var consultaGenerico = @"
                        SELECT 'SALIDA: TRANSFERENCIA' TransationType, CASE
                        WHEN TD.transfer_status = 9 THEN 'Terminado'
                        WHEN TD.transfer_status = 8 THEN 'Cancelado'
                        WHEN TD.transfer_status = 4  AND ISNULL(TD.gr_quantity,0) = TD.quantity AND TD.scan_status = 1 THEN 'En Transito'
                        WHEN TD.transfer_status = 4 THEN 'En Transito'
                        WHEN TD.transfer_status = 3 THEN 'Camino transito'
                        WHEN TD.transfer_status = 2 THEN 'Cancelado'
                        WHEN TD.transfer_status = 1 THEN 'Aprobado'
                        WHEN TD.transfer_status = 0 THEN 'Creado'
                        END as Item_Status, MC1.class_name Department, MC2.class_name Family,
                        TD.part_number PartNumber, I.part_description DescriptionC,
                        I.unit_size UnitSize,
                        TD.quantity Quantity, 'MXN' Currency,
                        TD.cost CostUnit,
                        CAST(TD.quantity*TD.cost AS decimal(18,4)) SubTotal,
                        M1.name TASA_IVA,
                        CAST(TD.iva* TD.quantity AS decimal(18,4)) IVA,
                        M2.name TASA_IEPS,
                        CAST(TD.ieps*TD.quantity AS decimal(18,4)) IEPS,
                        CAST(TD.ieps*TD.quantity AS decimal(18,4)) + CAST(TD.iva*TD.quantity AS decimal(18,4))+CAST(TD.cost*TD.quantity AS decimal(18,4))  Amount,
                        S.site_name Supplier, TH.transfer_document Folio, CONVERT(VARCHAR,TH.udate,101) MerchandiseEntry, CONVERT(VARCHAR,TH.cdate,101) MerchandiseEntryEta
                        FROM TRANSFER_DETAIL TD
                        JOIN TRANSFER_HEADER TH ON TH.transfer_document = TD.transfer_document
                        JOIN SITES S ON S.site_code = TH.destination_site_code
                        JOIN ITEM I ON I.part_number = TD.part_number
                        JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
                        JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
                        JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
                        JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
                        WHERE CONVERT(date, ISNULL(TH.udate, TH.cdate),101) >= @date1 and CONVERT(date, ISNULL(TH.udate, TH.cdate),101) <= @date2 ";
            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", date1.Date));
            parameters.Add(new SqlParameter("@date2", date2.Date));

            if (TransferType == "salida")
            {
                consultaGenerico += " AND TH.transfer_site_code = @thisSite ";
                parameters.Add(new SqlParameter("@thisSite", mainSite)); //La Tienda donde salio
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND TH.destination_site_code = @originSite ";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La tienda Destino
                }
            }
            else
            {
                consultaGenerico += " AND TH.destination_site_code = @thisSite ";
                parameters.Add(new SqlParameter("@thisSite", mainSite)); //La tienda Destino
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND TH.transfer_site_code = @originSite ";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La Tienda donde salio
                }
            }

            if (!string.IsNullOrWhiteSpace(status))
            {
                consultaGenerico += " and TH.transfer_status = @status AND TD.transfer_status = @status ";
                parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            }
            if (!string.IsNullOrWhiteSpace(department))
            {
                consultaGenerico += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }
            if (!string.IsNullOrWhiteSpace(family))
            {
                consultaGenerico += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }
            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and TD.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }
            return _contextDFLSAI.Database.SqlQuery<TransferDetailReportModel>(consultaGenerico, parameters.ToArray()).ToList();

        }
        public List<TransferHeaderModel> GetTransfersHeaderIn(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family, string mainSite)
        {
            var consultaGenerico = @"SELECT HEADER.transfer_document AS 'TransferDocument', ORIGINSITE.site_code AS 'TransferSiteCode', ORIGINSITE.site_name AS 'TransferSiteName', DESTINATIONSITE.site_code AS 'DestinationSiteCode', DESTINATIONSITE.site_name AS 'DestinationSiteName',             
                    HEADER.comment  AS 'Comment',
                    CONVERT(VARCHAR, HEADER.transfer_date, 101) AS 'TransferDate', 'MXN' Currency,

                    CASE WHEN HEADER.transfer_status = 0 THEN 'Creado' 
						WHEN HEADER.transfer_status = 1 THEN 'Aprobado' 
						WHEN HEADER.transfer_status = 2 THEN 'Rechazado' 
						WHEN HEADER.transfer_status = 4 THEN 
							CASE WHEN not exists(select 1 from TRANSFER_DETAIL d where d.transfer_document = header.transfer_document and d.transfer_site_code = header.transfer_site_code and d.destination_site_code = header.destination_site_code 
								and d.quantity <> d.gr_quantity ) 
							THEN 'Recibido' ELSE 'Transito' END
                        WHEN HEADER.transfer_status = 7 THEN 'Solicitud Cancelacion' 
						WHEN HEADER.transfer_status = 8 THEN 'Cancelado' 
						WHEN HEADER.transfer_status = 9 THEN 'Terminado' 
					END AS 'Status', 
                    CASE WHEN DETAIL.amount IS NULL THEN 0 ELSE DETAIL.amount  END AS 'TotalAmount', CASE WHEN DETAIL.iva IS NULL THEN 0 ELSE DETAIL.iva END AS 'TotalIva', CASE WHEN DETAIL.ieps IS NULL THEN 0 ELSE DETAIL.ieps END AS 'TotalIEPS' 
                    FROM TRANSFER_HEADER HEADER 
                    INNER JOIN SITES ORIGINSITE ON HEADER.transfer_site_code = ORIGINSITE.site_code 
                    INNER JOIN SITES DESTINATIONSITE ON HEADER.destination_site_code = DESTINATIONSITE.site_code 
                    INNER JOIN (SELECT transfer_document, transfer_site_code, destination_site_code, SUM(quantity * cost) AS amount, SUM(quantity * iva) AS iva, SUM(quantity * ieps)  AS ieps, SUM(quantity) as Quantity_sum, SUM(gr_quantity) as GR_quantity_sum FROM TRANSFER_DETAIL GROUP BY transfer_document, transfer_site_code, destination_site_code ) DETAIL ON DETAIL.transfer_document = HEADER.transfer_document 
                    
					
					WHERE CONVERT(date, ISNULL(HEADER.udate, HEADER.cdate), 101) BETWEEN @BEGINDATE AND @ENDDATE";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BEGINDATE", BeginDate.Date));
            parameters.Add(new SqlParameter("@ENDDATE", EndDate.Date));

            if (TransferType == "salida")
            {
                consultaGenerico += " AND HEADER.transfer_site_code = @thisSite AND DETAIL.transfer_site_code = @thisSite";
                parameters.Add(new SqlParameter("@thisSite", mainSite)); //La Tienda donde salio
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND HEADER.destination_site_code = @originSite AND DETAIL.destination_site_code = @originSite";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La tienda Destino
                }
            }
            else
            {
                consultaGenerico += " AND HEADER.destination_site_code = @thisSite AND DETAIL.destination_site_code = @thisSite";
                parameters.Add(new SqlParameter("@thisSite", mainSite)); //La tienda Destino
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND HEADER.transfer_site_code = @originSite AND DETAIL.transfer_site_code = @originSite";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La Tienda donde salio
                }
            }

            if (!string.IsNullOrWhiteSpace(status))
            {
                consultaGenerico += " and HEADER.transfer_status = @status ";
                parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            }
            //if (!string.IsNullOrWhiteSpace(department))
            //{
            //    consultaGenerico += " and I.level_header = @depart";
            //    parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            //}
            //if (!string.IsNullOrWhiteSpace(family))
            //{
            //    consultaGenerico += " and I.level1_code = @fam";
            //    parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            //}
            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and DETAIL.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }
            return _contextDFLSAI.Database.SqlQuery<TransferHeaderModel>(consultaGenerico, parameters.ToArray()).ToList();
        }
        public List<TransferDetailModel> GetTransferDetailsInReport(string document, string transferSiteCode)
        {
            var global = _contextDFLSAI.TRANSFER_HEADER
                .Where(e => e.transfer_document == document && e.transfer_site_code == transferSiteCode)
                .Select(e => e.TRANSFER_DETAIL)
                .FirstOrDefault()?
                .Select(e => new TransferDetailModel
                {
                    PartNumber = e.part_number,
                    Description = e.ITEM.part_description,
                    Quantity = e.quantity,
                    UnitCost = e.cost ?? 0,
                    Iva = e.iva ?? 0,
                    IEPS = e.ieps ?? 0,
                    GrQuantity = e.gr_quantity,
                    ScanStatus = e.scan_status ? 1 : 0,
                    TransferStatus = e.TRANSFER_HEADER.transfer_status,
                    DestinationSiteCode = e.TRANSFER_HEADER.transfer_site_code
                }).ToList();

            return global;
        }
        public List<TransferDetailModel> GetAllTransferDeatil(string Document, string mainSite)
        {
            var list = _contextDFLSAI.TRANSFER_HEADER.Select(e => new
            {
                header = e,
                details = e.TRANSFER_DETAIL.Select(de => new TransferDetailModel
                {
                    TransferDate = de.TRANSFER_HEADER.transfer_date,
                    Description = de.ITEM.part_description,
                    DestinationSiteCode = e.destination_site_code,
                    DestinationSiteAddress = e.SITES1.address,
                    DestinationSiteName = e.SITES1.site_name,
                    Quantity = de.quantity,
                    GrQuantity = de.gr_quantity,
                    ScanStatus = de.scan_status ? 1 : 0,
                    TransferStatus = de.TRANSFER_HEADER.transfer_status,
                    PartNumber = de.part_number,
                    TransferDocument = de.transfer_document,
                    UnitCost = de.cost ?? 0,
                    IEPS = de.ieps ?? 0,
                    Iva = de.iva ?? 0,
                    transfer_status = de.transfer_status
                })
            }).FirstOrDefault(e => e.header.transfer_document == Document && e.header.transfer_site_code == mainSite);

            return list?.details?.ToList() ?? new List<TransferDetailModel>();
        }
        public List<TransferStoresSPModel> GetTransfersTotalsSP(DateTime dateInit, DateTime dateFinish, int statusTransfers, string part_type)
        {
            var r = _contextDFLSAI.Database.SqlQuery<TransferStoresSPModel>(@"DECLARE	@return_value int
            EXEC	@return_value = sp_Transfers_Report
		    @date_initial = '" + dateInit.Date.ToString() + @"',
		    @date_finish ='" + dateFinish.Date.ToString() + @"',
            @status_transfer = " + statusTransfers + @",
            @part_type = '" + part_type + "'").ToList();
            return r;
        }
        public List<TransferStoresSPModel> GetTransfersReportSiteSP(DateTime dateInit, DateTime dateFinish, string site_code, int status_transfers, string part_type)
        {
            var r = _contextDFLSAI.Database.SqlQuery<TransferStoresSPModel>(@"DECLARE	@return_value int
            EXEC	@return_value = sp_Transfers_Report_Site
		    @date_initial = '" + dateInit.Date.ToString() + @"',
		    @date_finish ='" + dateFinish.Date.ToString() + @"',
            @site_code_detail = N'" + site_code + @"',
            @status_transfer = " + status_transfers + @",
            @part_type = '" + part_type + "'").ToList();
            return r;
        }
        public List<TransferHeaderReportSPModel> GetAllTransfersSites(DateTime dateInit, DateTime dateFinish, string site_code_des, string site_code_tra, int statusTransfers, string part_type)
        {
            var r = new List<TransferHeaderReportSPModel>();

            if (part_type == "ALL")
            {
                if (statusTransfers == 9)
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferHeaderReportSPModel>(@"
                (
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total ,
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' ,  
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code ,  TH.transfer_document , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' ,TH.transfer_status , 'Entrada' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.transfer_site_code
				WHERE TH.destination_site_code = '" + site_code_tra + "' AND TD.destination_site_code = '" + site_code_tra + @"'
                AND  TH.transfer_site_code = '" + site_code_des + "' AND TD.transfer_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND (TH.transfer_status = 4 OR TH.transfer_status = 9)
				AND (TD.transfer_status = 4 OR TD.transfer_status = 9)
				GROUP BY TD.destination_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code , TH.transfer_date)

				union all

				(
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total ,
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' ,  
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code , TH.transfer_document , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out'  , TH.transfer_status , 'Salida' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.destination_site_code
				WHERE TH.transfer_site_code = '" + site_code_tra + "' AND TD.transfer_site_code = '" + site_code_tra + @"'
                AND TH.destination_site_code = '" + site_code_des + "' AND TD.destination_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND (TH.transfer_status = 4 OR TH.transfer_status = 9)
				AND (TD.transfer_status = 4 OR TD.transfer_status = 9)
				GROUP BY TD.transfer_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code , TH.transfer_date)
            ").ToList();
                }
                else
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferHeaderReportSPModel>(@"
                (
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total , 
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' , 
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code ,  TH.transfer_document , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' , TH.transfer_status , 'Entrada' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.transfer_site_code
				WHERE TH.destination_site_code = '" + site_code_tra + "' AND TD.destination_site_code = '" + site_code_tra + @"'
                AND  TH.transfer_site_code = '" + site_code_des + "' AND TD.transfer_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND TH.transfer_status = " + statusTransfers + @" 
				AND TD.transfer_status = " + statusTransfers + @" 
				GROUP BY TD.destination_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code , TH.transfer_date)

				union all

				(
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total , 
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' , 
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code , TH.transfer_document  , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' , TH.transfer_status , 'Salida' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.destination_site_code
				WHERE TH.transfer_site_code = '" + site_code_tra + "' AND TD.transfer_site_code = '" + site_code_tra + @"'
                AND TH.destination_site_code = '" + site_code_des + "' AND TD.destination_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND TH.transfer_status =" + statusTransfers + @" 
				AND TD.transfer_status = " + statusTransfers + @" 
				GROUP BY TD.transfer_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code , TH.transfer_date)
            ").ToList();
                }
            }
            else
            {
                if (statusTransfers == 9)
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferHeaderReportSPModel>(@"
                (
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total , 
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' , 
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code ,  TH.transfer_document  , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' , TH.transfer_status , 'Entrada' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.transfer_site_code
                join ITEM i on i.part_number = TD.part_number
				WHERE TH.destination_site_code = '" + site_code_tra + "' AND TD.destination_site_code = '" + site_code_tra + @"'
                AND  TH.transfer_site_code = '" + site_code_des + "' AND TD.transfer_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND (TH.transfer_status = 4 OR TH.transfer_status = 9)
				AND (TD.transfer_status = 4 OR TD.transfer_status = 9)
				AND I.part_type = '" + part_type + @"'
				GROUP BY TD.destination_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code, TH.transfer_date)

				union all

				(
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total , 
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' , 
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code , TH.transfer_document  , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' , TH.transfer_status , 'Salida' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.destination_site_code
                join ITEM i on i.part_number = TD.part_number
				WHERE TH.transfer_site_code = '" + site_code_tra + "' AND TD.transfer_site_code = '" + site_code_tra + @"'
                AND TH.destination_site_code = '" + site_code_des + "' AND TD.destination_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND (TH.transfer_status = 4 OR TH.transfer_status = 9)
				AND (TD.transfer_status = 4 OR TD.transfer_status = 9)
				AND I.part_type = '" + part_type + @"'
				GROUP BY TD.transfer_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code, TH.transfer_date)
            ").ToList();
                }
                else
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferHeaderReportSPModel>(@"
                (
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total , 
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' , 
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code ,  TH.transfer_document  , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' , TH.transfer_status , 'Entrada' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.transfer_site_code
                join ITEM i on i.part_number = TD.part_number
				WHERE TH.destination_site_code = '" + site_code_tra + "' AND TD.destination_site_code = '" + site_code_tra + @"'
                AND  TH.transfer_site_code = '" + site_code_des + "' AND TD.transfer_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND TH.transfer_status = " + statusTransfers + @" 
				AND TD.transfer_status = " + statusTransfers + @" 
				AND I.part_type = '" + part_type + @"'
				GROUP BY TD.destination_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code, TH.transfer_date)

				union all

				(
                SELECT SUM((TD.quantity) * (TD.cost + TD.iva + TD.ieps)) as total , 
                (select site_name from SITES where site_code = '" + site_code_tra + @"') as 'transfer_site_name' , 
				SUM(TD.quantity * cost) as subtotal, SUM(TD.quantity * TD.iva) AS iva , SUM(TD.quantity * TD.ieps) AS ieps , 
				S.site_name as site_name , s.site_code , TH.transfer_document  , ISNULL(convert(varchar, TH.transfer_date_in, 103),'N/A') as 'transfer_date_in' ,
                isnull(convert(varchar, TH.transfer_date, 103),'N/A') as 'transfer_date_out' , TH.transfer_status , 'Salida' as transfer_type
				FROM TRANSFER_HEADER TH
				JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
				and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
				FULL OUTER JOIN SITES S ON S.site_code = TH.destination_site_code
                join ITEM i on i.part_number = TD.part_number
				WHERE TH.transfer_site_code = '" + site_code_tra + "' AND TD.transfer_site_code = '" + site_code_tra + @"'
                AND TH.destination_site_code = '" + site_code_des + "' AND TD.destination_site_code = '" + site_code_des + @"'
				AND convert(date,TH.transfer_date) BETWEEN '" + dateInit.Date.ToString() + @"' AND '" + dateFinish.Date.ToString() + @"'
				AND TH.transfer_status =" + statusTransfers + @" 
				AND TD.transfer_status = " + statusTransfers + @"
				AND I.part_type = '" + part_type + @"'
				GROUP BY TD.transfer_site_code , TH.transfer_document , TH.transfer_date_in , S.site_name , TH.transfer_status , s.site_code, TH.transfer_date)
            ").ToList();
                }
            }

            return r;
        }

        public List<TransferDetailModel> GetAllTransferDetailType(string Document, string mainSite, string status_transfer, string product_type, string transfer_type)
        {
            var transferType = "";
            if (transfer_type == "Salida")
                transferType = "destination_site_code";
            else
                transferType = "transfer_site_code";
            var r = new List<TransferDetailModel>();
            if (product_type == "ALL")
            {
                if (status_transfer == "9")
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferDetailModel>(@" select TD.part_number as 'PartNumber' , i.part_description as 'Description' , TD.quantity as 'Quantity',
                    TD.cost as 'UnitCost' , TD.iva * TD.quantity as 'Iva' , TD.ieps * TD.cost as 'IEPS' ,
                    TD.quantity * (TD.cost + TD.iva + TD.ieps) as 'Total'  , TD.quantity * TD.cost as 'SubTotal'
                    from TRANSFER_HEADER th
                    JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
                    and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
                    join ITEM i on i.part_number = TD.part_number
                    where th.transfer_document = '" + Document + @"'
                    and TD.transfer_status in (4,9)
                    and TD." + transferType + " = '" + mainSite + "'").ToList();

                }
                else
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferDetailModel>(@" select TD.part_number as 'PartNumber' , i.part_description as 'Description' , TD.quantity as 'Quantity',
                    TD.cost as 'UnitCost' , TD.iva * TD.quantity as 'Iva' , TD.ieps * TD.cost as 'IEPS' ,
                    TD.quantity * (TD.cost + TD.iva + TD.ieps) as 'Total'  , TD.quantity * TD.cost as 'SubTotal'
                    from TRANSFER_HEADER th
                    JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
                    and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
                    join ITEM i on i.part_number = TD.part_number
                    where th.transfer_document = '" + Document + @"'
                    and TD.transfer_status = '" + status_transfer + @"'
                    and TD." + transferType + " = '" + mainSite + "'").ToList();
                }

            }
            else
            {
                if (status_transfer == "9")
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferDetailModel>(@" select TD.part_number as 'PartNumber' , i.part_description as 'Description' , TD.quantity as 'Quantity',
                    TD.cost as 'UnitCost' , TD.iva * TD.quantity as 'Iva' , TD.ieps * TD.cost as 'IEPS' ,
                    TD.quantity * (TD.cost + TD.iva + TD.ieps) as 'Total'  , TD.quantity * TD.cost as 'SubTotal'
                    from TRANSFER_HEADER th
                    JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
                    and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
                    join ITEM i on i.part_number = TD.part_number
                    where th.transfer_document = '" + Document + @"'
                    and TD.transfer_status in (4,9)
                    and i.part_type = '" + product_type + @"'
                    and TD." + transferType + " = '" + mainSite + "'").ToList();

                }
                else
                {
                    r = _contextDFLSAI.Database.SqlQuery<TransferDetailModel>(@" select TD.part_number as 'PartNumber' , i.part_description as 'Description' , TD.quantity as 'Quantity',
                    TD.cost as 'UnitCost' , TD.iva * TD.quantity as 'Iva' , TD.ieps * TD.cost as 'IEPS' ,
                    TD.quantity * (TD.cost + TD.iva + TD.ieps) as 'Total'  , TD.quantity * TD.cost as 'SubTotal'
                    from TRANSFER_HEADER th
                    JOIN TRANSFER_DETAIL TD ON (TH.transfer_document = TD.transfer_document
                    and TD.destination_site_code = TH.destination_site_code and TH.transfer_site_code = TD.transfer_site_code)
                    join ITEM i on i.part_number = TD.part_number
                    where th.transfer_document = '" + Document + @"'
                    and TD.transfer_status = '" + status_transfer + @"'
                    and i.part_type = '" + product_type + @"'
                    and TD." + transferType + " = '" + mainSite + "'").ToList();
                }
            }
            return r;
        }

    }

}