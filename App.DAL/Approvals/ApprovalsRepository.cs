﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.Entities;

namespace App.DAL.Approvals
{
    public class ApprovalsRepository
    {
        private PRTSAIEntities _context;

        public ApprovalsRepository()
        {
            _context = new PRTSAIEntities();
        }
        public Result<List<APPROVAL_MASTER_ROUTE>> ApprovalProcessList()
        {
            return Result.OK(_context.APPROVAL_MASTER_ROUTE.Select(e => e).ToList());
        }

        public Result<List<APPROVAL_HISTORY>> ApprovalHistory()
        {
            return Result.OK(_context.APPROVAL_HISTORY.Select(e => e).ToList());  
        }


    }
}
