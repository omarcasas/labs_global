﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.MaEmail
{
    public class MaEmailRepository
    {
        private PRTSAIEntities _context;

        public MaEmailRepository()
        {
            _context = new PRTSAIEntities();
        }

        public string GetMaEmail(string site_code, string cargo)
        {
            if (cargo.ToLower().Trim() == "gerencia")
            {
                return _context.MA_EMAIL.Where(w => w.site_code == site_code)
                    .Select(s => s.gerencia_email).FirstOrDefault();
            }
            else if(cargo.ToLower().Trim() == "mesa")
            {
                return _context.MA_EMAIL.Where(w => w.site_code == site_code)
                    .Select(s => s.mesa_control_email).FirstOrDefault();
            }
            else if (cargo.ToLower().Trim() == "recibo")
            {
                return _context.MA_EMAIL.Where(w => w.site_code == site_code)
                    .Select(s => s.recibo_email).FirstOrDefault();
            }
            else if (cargo.ToLower().Trim() == "caja")
            {
                return _context.MA_EMAIL.Where(w => w.site_code == site_code)
                    .Select(s => s.caja_gral_email).FirstOrDefault();
            }
            else
            {
                return "";
            }
        }

        public string GetEmailGenericList(string cargo)
        {
            var listEmails = _context.MA_EMAIL.ToList();
            if (cargo.ToLower().Trim() == "gerencia")
                return string.Join(",", listEmails.Select(s => s.gerencia_email).ToArray());
            else if (cargo.ToLower().Trim() == "mesa")
                return string.Join(",", listEmails.Select(s => s.mesa_control_email).ToArray());
            else if (cargo.ToLower().Trim() == "recibo")
                return string.Join(",", listEmails.Select(s => s.recibo_email).ToArray());
            else if (cargo.ToLower().Trim() == "caja")
                return string.Join(",", listEmails.Select(s => s.caja_gral_email).ToArray());
            else
                return "";
        }
    }
}
