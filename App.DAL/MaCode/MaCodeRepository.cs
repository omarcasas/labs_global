﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.MaCode
{
    public class MaCodeRepository
    {
        private PRTSAIEntities _context;

        public MaCodeRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<MA_CODE> GetListByCode(string code)
        {
            var model = _context.MA_CODE.Where(x => x.code == code).ToList();

            return model;
        }


        public string GetBuyerDivisionSistemas()
        {
            var p999 = _context.MA_CODE.Where(w => w.code == "BUYER_DIVISION" && w.description == "Sistemas").SingleOrDefault();
            return p999.vkey;
        }

        public bool ValidateItemForce90(string part_number)
        {
            //Productos exlusivos que no deben terminar en .90
            var validateItems = _context.MA_CODE.Where(w => w.vkey == part_number && w.code == "NOT_FORCED_90_ITEM" && w.description == "ITEM" && w.used == "1").FirstOrDefault();
            if(validateItems == null)
                 validateItems = _context.MA_CODE.Where(w => w.vkey == "TODOS" && w.code == "NOT_FORCED_90_ITEM_ALL" && w.description == "LABORATORIO" && w.used == "1").FirstOrDefault();

            if (validateItems != null)
                return false; //Este producto no debe terminar en .90

            //Información del producto
            var infoItem = _context.ITEM.Where(w => w.part_number == part_number).FirstOrDefault();
            if (infoItem != null)
            {
                string level_header = Convert.ToString(infoItem.level_header ?? 0);
                string level1_code = Convert.ToString(infoItem.level1_code ?? 0);
                //Buscar en sus departamentos y familias
                var validateItemClassHeader = _context.MA_CODE.Where(w => w.vkey == level_header && w.code == "NOT_FORCED_90_CLASS" && w.description == "CLASS" && w.used == "1").FirstOrDefault();
                if (validateItemClassHeader != null)
                    return false;//Este producto no debe terminar en .90
                var validateItemClasslevel1 = _context.MA_CODE.Where(w => w.vkey == level1_code && w.code == "NOT_FORCED_90_CLASS" && w.description == "CLASS" && w.used == "1").FirstOrDefault();
                if (validateItemClasslevel1 != null)
                    return false;//Este producto no debe terminar en .90
            }
            return true; //ESTE PRODUCTO DEBE TERMINAR EN .90
        }
        
        public string GetEmailsPriceChange()
        {
            var listEmails = _context.MA_CODE.Where(w => w.code == "CHANGE_PRICE_EMAIL" && w.used == "1").ToList();
            if(listEmails != null)
                return string.Join(",", listEmails.Select(s => s.vkey).ToArray());
            return "";
        }

        public bool GetAndSetSupplierXML(string rfc, string business_name, int status, string uuser)
        {
            try
            {
                var statusOriginal = _context.MA_CODE.Where(w => w.code == "XML_AVOIDABLE_SUPPLIERS" && w.vkey == rfc).FirstOrDefault();
                if (statusOriginal == null)
                {
                    if (status == 0)
                        return true; //No hay nada que actualizar
                    MA_CODE new_supplier = new MA_CODE();
                    new_supplier.code = "XML_AVOIDABLE_SUPPLIERS";
                    new_supplier.vkey = rfc;
                    new_supplier.vkey_seq = 1;
                    new_supplier.description = business_name;
                    new_supplier.used = "1";
                    new_supplier.program_id = "PURCH003";
                    new_supplier.cuser = uuser;
                    new_supplier.cdate = DateTime.Now;
                    _context.MA_CODE.Add(new_supplier);
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    if (statusOriginal.used == "0" && status == 0 || statusOriginal.used == "1" && status == 1)
                        return true; //No hay nada que actualizar
                    statusOriginal.used = status.ToString();
                    statusOriginal.program_id = "PURCH003";
                    statusOriginal.udate = DateTime.Now;
                    statusOriginal.uuser = uuser;
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
            
        }

        public List<MA_CODE> GetListXMLSites()
        {
            return _context.MA_CODE.Where(w => w.code == "XML_REQUIRED" && w.used == "1").OrderBy(o => o.vkey).ToList();
        }
    }
}
