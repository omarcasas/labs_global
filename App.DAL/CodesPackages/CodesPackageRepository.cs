﻿using App.Entities;
using App.Entities.ViewModels.CodesPackege;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodesPackages
{
    public class CodesPackageRepository
    {
        private readonly PRTSAIEntities _context;

        public CodesPackageRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool CreateCodePackage(string PartNumberOrigin, List<CodesPackagesModel> Codes, string User)
        {
            try
            {
                foreach (var item in Codes)
                {
                    CODES_PACKAGES CodePackage = new CODES_PACKAGES { part_number_origin = PartNumberOrigin, part_number = item.PartNumber, quantity = item.Quantity, cost = item.Cost, active_flag = true, cuser = User, cdate = DateTime.Now, program_id = "PURCH017.cshtml" };
                    _context.CODES_PACKAGES.Add(CodePackage);
                }
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateCodePackage(string PartNumberOrigin, List<CodesPackagesModel> Codes, string User)
        {
            try
            {
                var items = _context.CODES_PACKAGES.Where(x => x.part_number_origin == PartNumberOrigin).ToList();
                foreach (var item in items)
                    _context.Entry(item).State = EntityState.Deleted;

                _context.SaveChanges();

                foreach (var item in Codes)
                {
                    CODES_PACKAGES CodePackage = new CODES_PACKAGES { part_number_origin = PartNumberOrigin, part_number = item.PartNumber, quantity = item.Quantity, cost = item.Cost, active_flag = true, cuser = User, cdate = DateTime.Now, uuser = User, udate = DateTime.Now, program_id = "PURCH017.cshtml" };
                    _context.CODES_PACKAGES.Add(CodePackage);
                }
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<CodesPackagesModel> GetCodePackage(string PartNumberOrigin)
        {
            var Codes = _context.CODES_PACKAGES.Join(_context.ITEM, its => its.part_number,
            it => it.part_number, (its, it) => new { its, it }).Where(element => element.its.part_number_origin == PartNumberOrigin & element.its.active_flag == true).Select(x => new CodesPackagesModel
            {
                PartNumberOrigin = x.its.part_number_origin,
                PartNumber = x.its.part_number,
                PartDescription = x.it.part_description,
                Quantity = x.its.quantity,
                Cost = x.its.cost
            }).ToList();

            return Codes;
        }

        public bool ValidateCodePackage(string PartNumberOrigin)
        {
            if (_context.CODES_PACKAGES.Where(x => x.part_number_origin == PartNumberOrigin).Count() > 0)
                return true;
            else
                return false;
        }

        public List<CodesPackagesModel> GetCodesPackages()
        {
            var model = _context.Database.SqlQuery<CodesPackagesModel>(@"SELECT CP.part_number_origin 'PartNumberOrigin', IT.part_description 'PartDescription', IT.unit_size 'UnitSize', PBC.base_cost 'Cost' 
                FROM ( 
                	SELECT *, ROW_NUMBER() OVER (PARTITION BY part_number_origin ORDER BY part_number_origin) AS RN 
	                FROM CODES_PACKAGES) CP 
                INNER JOIN ITEM IT ON CP.part_number_origin = IT.part_number 
                INNER JOIN ( 
                    SELECT base_cost, part_number FROM PURCHASE_BASE_COST 
                    INNER JOIN ITEM_SUPPLIER ON PURCHASE_BASE_COST.supplier_part_number = ITEM_SUPPLIER.supplier_part_number AND PURCHASE_BASE_COST.[status] = 1 
                    GROUP BY base_cost, part_number 
                ) AS PBC ON CP.part_number_origin = PBC.part_number 
                WHERE RN = 1 ").ToList();

            return model;
        }
    }
}