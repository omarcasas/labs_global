﻿using App.DAL.CommonAccess;
using App.Entities;
using App.Entities.ViewModels.CodesPackege;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.ItemPresentation;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Linq.Enumerable;

namespace App.DAL.ItemPresentation
{
    public class ItemPresentationRepository
    {
        private readonly PRTSAIEntities _context;

        public ItemPresentationRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ItemDropDownModel> GetAllBasicInformation()
        {
            var presentations = from p in _context.ITEM_PRESENTATION
                                where p.active_flag
                                select new ItemDropDownModel
                                {
                                    part_description = p.presentation_name,
                                    part_number = p.presentation_code,
                                };
            var r = presentations.ToList();
            return r;
        }

        public ItemPresentationModel GetInfoPresentationByIdInt(int presentation_id)
        {
            return _context.ITEM_PRESENTATION.Where(W => W.presentation_id == presentation_id)
                .Select(s => new ItemPresentationModel
                {
                    PresentationId = s.presentation_id,
                    Name = s.presentation_name,
                    PartNumber = s.part_number,
                    PartDescription = s.presentation_name,
                    Quantity = s.quantity,
                    Price = s.sale_price ?? 0,
                    FactorUnitSize = s.factor_unit_size ?? "",
                    ActiveFlag = s.active_flag,
                    PresentationCode = s.presentation_code ?? "",
                    IEPS = s.ieps ?? 0,
                    IVA = s.iva ?? 0,
                    TotalPrice = s.sale_price_iva,
                }).FirstOrDefault();
        }
        public ItemPresentationModel GetInformationBasic(string presentation_code)
        {
            return _context.ITEM_PRESENTATION.Where(w => w.presentation_code == presentation_code)
                .Select(s => new ItemPresentationModel
                {
                    Name = s.presentation_name,
                    PartNumber = s.part_number,
                    PresentationId = s.presentation_id,
                    PresentationCode = s.presentation_code,
                    TotalPrice = s.sale_price_iva,
                    PartDescription = s.presentation_name

                }).FirstOrDefault();
        }

        public List<ItemPresentationModel> GetBasicInformation()
        {
            var presentations = from p in _context.ITEM_PRESENTATION
                                join i in _context.ITEM on p.part_number equals i.part_number
                                select new ItemPresentationModel
                                {
                                    PresentationId = p.presentation_id,
                                    Name = p.presentation_name,
                                    PartNumber = p.part_number,
                                    PartDescription = i.part_description,
                                    Quantity = p.quantity,
                                    Price = p.sale_price ?? 0,
                                    FactorUnitSize = p.factor_unit_size ?? "",
                                    ActiveFlag = p.active_flag,
                                    PresentationCode = p.presentation_code ?? "",
                                    IEPS = p.ieps ?? 0,
                                    IVA = p.iva ?? 0,
                                    TotalPrice = p.sale_price_iva,
                                };
            var r = presentations.ToList();
            return r;
        }

        public List<ItemPresentationModel> GetPresentationByPartNumber(string partNumber)
        {
            var presentations = from p in _context.ITEM_PRESENTATION
                                join i in _context.ITEM on p.part_number equals i.part_number
                                where i.part_number == partNumber

                                //where p.active_flag == true
                                select new ItemPresentationModel
                                {
                                    PresentationId = p.presentation_id,
                                    Name = p.presentation_name,
                                    PartNumber = p.part_number,
                                    PartDescription = i.part_description,
                                    Quantity = p.quantity,
                                    PackingType = p.packing_type,
                                    Price = p.sale_price ?? 0,
                                    FactorUnitSize = p.factor_unit_size ?? "",
                                    ActiveFlag = p.active_flag,
                                    PresentationCode = p.presentation_code ?? "",
                                    IEPS = p.ieps ?? 0,
                                    IVA = p.iva ?? 0,
                                    TotalPrice = p.sale_price_iva,
                                    ShowInLabel = p.show_in_label ?? false,
                                };
            var r = presentations.ToList();
            return r;
        }

        public bool PresentationExist(int id)
        {
            return _context.ITEM_PRESENTATION.Count(p => p.presentation_id == id) > 0;
        }

        public bool PresentationCodeExist(string presentationCode)
        {
            return _context.ITEM_PRESENTATION.Count(p => p.presentation_code == presentationCode) > 0;
        }

        public bool PresentationCodeExistForEdit(string presentationCode, int presentationId)
        {
            return _context.ITEM_PRESENTATION.Count(p => p.presentation_code == presentationCode && p.presentation_id != presentationId) > 0;
        }

        public bool Add(ItemPresentationModel model, string user, string page)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    if (model.ShowInLabel)
                    {
                        var list = _context.ITEM_PRESENTATION.Where(e => e.part_number == model.PartNumber).ToList();
                        if (list.Any())
                        {
                            list.ForEach(e => e.show_in_label = false);
                            _context.SaveChanges();
                        }
                        else
                        {
                            model.ShowInLabel = true;
                        }

                    }
                    var pre = new ITEM_PRESENTATION
                    {
                        presentation_id = model.PresentationId,
                        presentation_name = model.Name,
                        part_number = model.PartNumber,
                        quantity = model.Quantity,
                        sale_price_iva = model.TotalPrice,
                        packing_type = model.PackingType,
                        presentation_code = model.PresentationCode,
                        factor_unit_size = model.FactorUnitSize,
                        active_flag = model.ActiveFlag,
                        show_in_label = model.ShowInLabel,
                        iva = model.IVA,
                        ieps = model.IEPS,
                        sale_price = model.Price,
                        cuser = user,
                        cdate = DateTime.Now,
                        program_id = page,

                    };

                    _context.ITEM_PRESENTATION.Add(pre);
                    _context.SaveChanges();
                    var pri_change = new SALES_PRICE_CHANGE_PRESENTATION
                    {
                        presentation_id = pre.presentation_id,
                        margin = 0,
                        applied_date = model.appliedDate,
                        sale_price_iva = model.TotalPrice,
                        sale_price = pre.sale_price ?? 0,
                        cdate = DateTime.Now,
                        status = 1,
                        cuser = user,
                        program_id = page
                    };
                    _context.SALES_PRICE_CHANGE_PRESENTATION.Add(pri_change);

                    _context.BARCODES.Add(new BARCODES
                    {
                        part_number = model.PartNumber,
                        part_barcode = model.PresentationCode,
                        presentation_id = pre.presentation_id,
                        cuser = user,
                        cdate = DateTime.Now,
                    });
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    trans.Rollback();
                    return false;
                }
            }
        }

        public bool Edit(ItemPresentationModel model, string user, string page)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    //ist.Add(model);
                    var p = _context.ITEM_PRESENTATION
                        .SingleOrDefault(m => m.presentation_id == model.PresentationId);
                    if (p == null)
                    {
                        return false;
                    }

                    if (model.ShowInLabel)
                    {
                        var list = _context.ITEM_PRESENTATION.Where(e => e.part_number == model.PartNumber).ToList();
                        if (list.Any())
                        {
                            list.ForEach(e => e.show_in_label = false);
                            _context.SaveChanges();
                        }
                        else
                        {
                            model.ShowInLabel = true;
                        }

                    }

                    //p.name
                    p.presentation_name = model.Name;
                    p.quantity = model.Quantity;
                    p.sale_price_iva = model.TotalPrice;
                    //p.presentation_code = model.PresentationCode;
                    p.packing_type = model.PackingType;
                    p.factor_unit_size = model.FactorUnitSize;
                    p.active_flag = model.ActiveFlag;
                    p.udate = DateTime.Now;
                    p.uuser = user;
                    p.program_id = page;
                    p.show_in_label = model.ShowInLabel;
                    p.iva = model.IVA;
                    p.ieps = model.IEPS;
                    p.sale_price = model.Price;
                    _context.SaveChanges();

                    //Modificar y cambiar el ultimo valor
                    var Product = _context.SALES_PRICE_CHANGE_PRESENTATION
                        .Where(x => x.presentation_id == p.presentation_id && x.status == 1).ToList();
                    foreach (var product in Product)
                    {
                        product.status = 0;
                        product.uuser = user;
                        product.udate = DateTime.Now;
                        product.program_id = page;
                    }

                    var pri_change = new SALES_PRICE_CHANGE_PRESENTATION
                    {
                        presentation_id = p.presentation_id,
                        margin = 0,
                        applied_date = model.appliedDate,
                        sale_price = model.Price,
                        sale_price_iva = model.TotalPrice,
                        cdate = DateTime.Now,
                        status = 1,
                        cuser = user,
                        program_id = page
                    };
                    _context.SALES_PRICE_CHANGE_PRESENTATION.Add(pri_change);
                    _context.SaveChanges();
                    trans.Commit();

                    return true;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    trans.Rollback();
                    return false;
                }
            }
        }

        public bool Remove(int id)
        {
            try
            {
                //ist.Add(model);
                //var m = new ITEM_PRESENTATION {presentation_id = id};
                var m = _context.ITEM_PRESENTATION.Where(w => w.presentation_id == id).FirstOrDefault();
                //_context.ITEM_PRESENTATION.Attach(m);
                m.active_flag = false;
                //_context.ITEM_PRESENTATION.Remove(m);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return false;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public ItemPresentationModel GetPresentationById(string presentation_id)
        {
            var model = (from p in _context.ITEM_PRESENTATION
                         join i in _context.ITEM on p.part_number equals i.part_number
                         where i.part_number == presentation_id
                         select new ItemPresentationModel
                         {
                             PresentationId = p.presentation_id,
                             Name = p.presentation_name,
                             PartNumber = p.part_number,
                             PartDescription = i.part_description,
                             Quantity = p.quantity,
                             PackingType = p.packing_type,
                             Price = p.sale_price ?? 0,
                             FactorUnitSize = p.factor_unit_size ?? "",
                             ActiveFlag = p.active_flag,
                             PresentationCode = p.presentation_code ?? "",
                             IEPS = p.ieps ?? 0,
                             IVA = p.iva ?? 0,
                             TotalPrice = p.sale_price_iva,
                             ShowInLabel = p.show_in_label ?? false
                         }).FirstOrDefault();

            //model.CodesPackages = _context.CODES_PACKAGES.Join(_context.ITEM, its => its.part_number,
            //it => it.part_number, (its, it) => new { its, it }).Where(element => element.its.presentation_id == model.PresentationId & element.its.active_flag == true).Select(x => new CodesPackagesModel
            //{
            //    PresentationId = x.its.presentation_id,
            //    PartNumber = x.its.part_number,
            //    PartDescription = x.it.part_description,
            //    Quantity = x.its.quantity,
            //    Cost = x.its.cost
            //}).ToList();

            return model;
        }
    }
}