﻿using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.StorageLocation
{
    public class StorageLocationRepository
    {
        private PRTSAIEntities _context;

        public StorageLocationRepository()
        {
            _context = new PRTSAIEntities();
        }
        public List<StorageLocationModel> GetAllLocationsInStatusActive()
        {
            var Locations = _context.STORAGE_LOCATION.Where(x => x.active_flag == true)
                .Select(x => new StorageLocationModel
                {
                    site_code = x.site_code,
                    storage_location = x.storage_location_name,
                    storage_type = x.storage_type,
                    active_flag = x.active_flag.Value
                }).ToList();
            return Locations;
        }
    }
}
