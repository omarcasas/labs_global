﻿using App.Entities;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.PurchaseOrder
{
    public class PurchaseOrderRepository
    {
        private readonly PRTSAIEntities _context;

        public PurchaseOrderRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int CreatePuchaseOrder(PURCHASE_ORDER model)
        {
            try
            {
                _context.PURCHASE_ORDER.Add(model);
                _context.SaveChanges();
                int id = model.purchase_no;
                return id;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public int CreatePurchaseOrderItems(PURCHASE_ORDER_ITEM model)
        {
            try
            {
                _context.PURCHASE_ORDER_ITEM.Add(model);
                _context.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public List<PurchaseOrderXMLModel> GetAllGenericPurchasesOrdersXML(DateTime BeginDate, DateTime EndDate, string SiteIP, int SupplierId, string PartNumber, string Department, string Family, string Currency)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BeginDate", BeginDate),
                new SqlParameter("@EndDate", EndDate)
            };

            var query = @" 
                SELECT PURCHASE_ORDER.purchase_no 'PurchaseNo', SITES.site_code 'SiteCode', SITES.site_name 'SiteName', MA_SUPPLIER.supplier_id, 
                MA_SUPPLIER.business_name 'supplier_name', PURCHASE_ORDER.purchase_date 'PurchaseDate', ISNULL(PURCHASE_ORDER.eta, PURCHASE_ORDER.ata) 'Eta', 
                ISNULL(PURCHASE_ORDER.invoice_no, '-') 'invoice', PURCHASE_ORDER.currency 'Currency', SUM(PURCHASE_ORDER_ITEM.iva) 'Iva', 
                SUM(PURCHASE_ORDER_ITEM.ieps) 'Ieps', SUM(PURCHASE_ORDER_ITEM.item_amount) 'ItemAmount', SUM(PURCHASE_ORDER_ITEM.item_total_amount) 'ItemTotalAmount', 
                PURCHASE_ORDER.purchase_status 'purchase_status_value' ";

            query += @" FROM [" + SiteIP + "].[DFL_SAI_SITE].[dbo].PURCHASE_ORDER ";
            query += @" INNER JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].SITES ON PURCHASE_ORDER.site_code = SITES.site_code ";
            query += @" INNER JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].MA_SUPPLIER ON PURCHASE_ORDER.supplier_id = MA_SUPPLIER.supplier_id ";
            query += @" INNER JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].PURCHASE_ORDER_ITEM ON PURCHASE_ORDER.purchase_no = PURCHASE_ORDER_ITEM.purchase_no ";
            query += @" INNER JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].ITEM ON PURCHASE_ORDER_ITEM.part_number = ITEM.part_number ";

            query += @" 
                WHERE ISNULL(PURCHASE_ORDER.ata, PURCHASE_ORDER.purchase_date) BETWEEN CONVERT(DATE, @BeginDate, 101) AND CONVERT(DATE, @EndDate, 101) 
                AND PURCHASE_ORDER.purchase_status = 9 AND (PURCHASE_ORDER_ITEM.item_status = 1 OR PURCHASE_ORDER_ITEM.item_status = 8) 
                AND (PURCHASE_ORDER.uuid IS NULL OR PURCHASE_ORDER.uuid <> '') ";

            if (SupplierId > 0)
            {
                query += " AND MA_SUPPLIER.supplier_id = @SupplierId ";
                parameters.Add(new SqlParameter("@SupplierId", Convert.ToInt32(SupplierId)));
            }

            if (!string.IsNullOrWhiteSpace(PartNumber))
            {
                query += " AND PURCHASE_ORDER_ITEM.part_number = @PartNumber ";
                parameters.Add(new SqlParameter("@PartNumber", PartNumber));
            }

            if (!string.IsNullOrWhiteSpace(Department))
            {
                query += " AND ITEM.level_header = @Department ";
                parameters.Add(new SqlParameter("@Department", Department));
            }

            if (!string.IsNullOrWhiteSpace(Family))
            {
                query += " AND ITEM.level1_code = @Family ";
                parameters.Add(new SqlParameter("@Family", Family));
            }

            if (!string.IsNullOrWhiteSpace(Currency))
            {
                query += " AND PURCHASE_ORDER.currency = @Currency ";
                parameters.Add(new SqlParameter("@Currency", Currency));
            }

            query += @" 
                GROUP BY PURCHASE_ORDER.purchase_no, SITES.site_code, SITES.site_name, MA_SUPPLIER.supplier_id, MA_SUPPLIER.business_name, 
                PURCHASE_ORDER.purchase_date, PURCHASE_ORDER.currency, PURCHASE_ORDER.purchase_status, PURCHASE_ORDER.cdate, 
                PURCHASE_ORDER.invoice_no, PURCHASE_ORDER.eta, PURCHASE_ORDER.ata 
                ORDER BY PURCHASE_ORDER.purchase_no ";

            var model = _context.Database.SqlQuery<PurchaseOrderXMLModel>(query, parameters.ToArray()).ToList();

            return model;
        }

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetailStatus(string PoNumber, string SiteIP)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@PoNumber", PoNumber)
            };

            var query = @" 
                SELECT PURCHASE_ORDER_ITEM.part_number 'PartNumber', ISNULL(PURCHASE_ORDER_ITEM.quantity, 0) 'Quantity', 
                ISNULL(PURCHASE_ORDER_ITEM.gr_quantity, 0) 'grQuantity',
                ISNULL(ITEM.unit_size, 'N/A') 'UnitSize',
                ISNULL(PURCHASE_ORDER_ITEM.purchase_price, 0) 'PurchasePrice',
                ISNULL(PURCHASE_ORDER_ITEM.packing_of_size, 1) 'Parcking',
                ISNULL(PURCHASE_ORDER_ITEM.item_amount, 0) 'ItemAmount',
                ISNULL(PURCHASE_ORDER_ITEM.iva, 0) 'Iva',
                ISNULL(PURCHASE_ORDER_ITEM.ieps, 0) 'Ieps',
                ISNULL(PURCHASE_ORDER_ITEM.item_total_amount, 0) 'ItemTotalAmount',
                ISNULL(PURCHASE_ORDER_ITEM.item_status, 0) 'itemStatus',
                ITEM.part_description 'Description' ";

            query += @" FROM [" + SiteIP + "].[DFL_SAI_SITE].[dbo].PURCHASE_ORDER_ITEM ";
            query += @" INNER JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].ITEM ON PURCHASE_ORDER_ITEM.part_number = ITEM.part_number ";

            query += @" 
                WHERE PURCHASE_ORDER_ITEM.purchase_no = @PoNumber
                ORDER BY PURCHASE_ORDER_ITEM.item_position ";

            var model = _context.Database.SqlQuery<PurchaseOrderItemListModel>(query, parameters.ToArray()).ToList();

            return model;
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllGenericPurchasesOrdersDetailXML(DateTime date1, DateTime date2, string SiteIP, int supp, string partNumber, string department, string family, string currency)
        {
            // reporte detalle

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BeginDate", date1),
                new SqlParameter("@EndDate", date2)
            };

            var query = @"SELECT MC1.class_name Department, MC2.class_name Family,
                TD.part_number PartNumber, I.part_description DescriptionC, 'ENTRADA: ORDEN DE COMPRA' TransationType,
                ISNULL(TD.gr_quantity, TD.original_quantity) Quantity,
                TD.purchase_price CostUnit,
                CAST(ISNULL(TD.gr_quantity, TD.original_quantity)*TD.purchase_price AS decimal(18,4)) SubTotal,
                M1.name TASA_IVA,
                CAST(TD.iva AS decimal(18,4)) IVA,
                M2.name TASA_IEPS,
                CAST(TD.ieps AS decimal(18,4)) IEPS,
                CAST(TD.item_total_amount AS decimal(18,4)) Amount,
                CASE
	                WHEN TD.item_status = 1 and TH.purchase_status = 1 THEN 'Iniciado'
	                WHEN TD.item_status = 1 and (TH.purchase_status = 1 OR TH.purchase_status = 3 OR TH.purchase_status = 4 OR TH.purchase_status = 5) THEN 'Escaneando'
	                WHEN TD.item_status = 7 THEN 'Producto Cancelado'
	                WHEN TD.item_status = 8 THEN 'Cancelado'
	                WHEN TD.item_status = 1 and TH.purchase_status = 9 THEN 'Terminando'
                END as Item_Status, 
                M.business_name Supplier, TH.purchase_no Folio, CONVERT(VARCHAR,TH.ata,101) MerchandiseEntry, CONVERT(VARCHAR,TH.eta,101) MerchandiseEntryEta, TH.currency Currency";

            query += @" FROM [" + SiteIP + "].[DFL_SAI_SITE].[dbo].PURCHASE_ORDER_ITEM TD ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].PURCHASE_ORDER TH ON TH.purchase_no = TD.purchase_no ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].ITEM I ON I.part_number = TD.part_number ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].MA_SUPPLIER M ON M.supplier_id = TH.supplier_id ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].MA_TAX M1 ON M1.tax_code = I.part_iva_sale ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].MA_TAX M2 ON M2.tax_code = I.part_ieps ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].MA_CLASS MC1 ON MC1.class_id = I.level_header ";
            query += @" JOIN [" + SiteIP + "].[DFL_SAI_SITE].[dbo].MA_CLASS MC2 ON MC2.class_id = I.level1_code ";
            query += @" WHERE CONVERT(date, ISNULL(TH.ata, TH.purchase_date),101) >= @BeginDate and  CONVERT(date, ISNULL(TH.ata, TH.purchase_date),101)<= @EndDate AND (TH.uuid IS NULL OR TH.uuid = '') ";

            query += " and TH.purchase_status = 9 AND TD.item_status = 1";

            if (!string.IsNullOrWhiteSpace(department))
            {
                query += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }

            if (!string.IsNullOrWhiteSpace(family))
            {
                query += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }

            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                query += " and TD.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }

            if (!string.IsNullOrWhiteSpace(currency))
            {
                query += " and TH.currency = @currency";
                parameters.Add(new SqlParameter("@currency", currency));
            }

            if (supp != 0)
            {
                query += " AND TH.supplier_id = @supplier";
                parameters.Add(new SqlParameter("@supplier", supp));
            }

            var model = _context.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(query, parameters.ToArray()).ToList();

            return model;
        }
    }
}