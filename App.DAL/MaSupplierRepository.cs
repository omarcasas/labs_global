using System;
using System.Collections.Generic;
using System.Linq;
using App.Entities;

namespace App.DAL
{
    public class MaSupplierRepository
    {
        private PRTSAIEntities _context;
        public MaSupplierRepository()
        {
            _context = new PRTSAIEntities();
        }
        public List<MA_SUPPLIER> GetSupplierByRFC(string rfc)
        {
            var x = _context.MA_SUPPLIER.Where(e => e.rfc == rfc).ToList();
            return x; 
        }

        public string GetNameSupplier(int Supplier)
        {
            var SupplierName = _context.MA_SUPPLIER.Where(x => x.supplier_id == Supplier).SingleOrDefault();
            return SupplierName != null ? SupplierName.commercial_name : "";
        }
    }
}