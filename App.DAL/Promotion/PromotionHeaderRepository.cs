﻿using App.Entities;
using App.Entities.ViewModels.Promotion;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Promotion
{
    public class PromotionHeaderRepository
    {
        private PRTSAIEntities _context;

        public PromotionHeaderRepository()
        {
            _context = new PRTSAIEntities();
        }
        public int CountHeader()
        {
            var date = DateTime.Now;
            DayOfWeek fdow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            int offset = fdow - date.DayOfWeek;
            DateTime fdowDate = date.AddDays(offset);
            DateTime ldowDate = fdowDate.AddDays(6);
            var StartDate = fdowDate.Date;
            var EndDate = ldowDate.Date;

            return _context.PROMOTION_HEADER.Where(w => DbFunctions.TruncateTime( w.cdate) >= DbFunctions.TruncateTime( StartDate) 
            && DbFunctions.TruncateTime( w.cdate) <= DbFunctions.TruncateTime( EndDate)).Select(x => x).Count();
        }

        public string GetNumberPromotion(string promotion_code)
        {
            var promo = _context.PROMOTION_HEADER.Where(w => w.promotion_code == promotion_code).FirstOrDefault();
            string newPromotionCode = promotion_code.Substring(promotion_code.Length - 4);
            int numberPlus = Int32.Parse(newPromotionCode);
            numberPlus = numberPlus + 1;
            return numberPlus.ToString();

        }
        public string AddPromotionHeader(PromotionModel modelHeader, string buyer_division, string cuser)
        {
            try
            {
                PROMOTION_HEADER promotion = new PROMOTION_HEADER()
                {
                    promotion_code = modelHeader.promotion_code,
                    promotion_type = modelHeader.promotion_type,
                    promotion_start_date = modelHeader.promotion_start_date,
                    promotion_end_date = modelHeader.promotion_end_date,
                    buyer_division = buyer_division,
                    delete_flag = false,
                    cuser = cuser,
                    cdate = DateTime.Now,
                    program_id = "PURCH013.cshtml"
                };
                _context.PROMOTION_HEADER.Add(promotion);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "";
            }

            return modelHeader.promotion_code;
        }

        public List<PromotionItem> GetPromotionAdvancedByCode(string promotion_code)
        {
            var promotions = _context.PROMOTION_ADVANCED_DETAIL
               .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
               .Join(_context.MA_TAX, t => t.ii.part_iva_sale, tt => tt.tax_code, (t, tt) => new { t, tt})
               .Where(w => w.t.i.promotion_code == promotion_code && w.t.i.delete_flag == false)
               .Select(s => new PromotionItem
               {
                   promotion_code = s.t.i.promotion_code,
                   part_number = s.t.i.part_number,
                   description = s.t.ii.part_description,
                   quantity = s.t.i.promotion_quantity,
                   price = s.t.i.promotion_price_iva == null ?  ((1 * s.tt.tax_value ?? 0) * s.t.i.promotion_price / 100) + s.t.i.promotion_price : s.t.i.promotion_price_iva ?? 0
               }).ToList();
            //Busca si hay comunes
            foreach (var promotion in promotions)
            {
                var commons = _context.PROMOTION_ADVANCED_COMMON.Where(w => w.promotion_advanced_code == promotion.promotion_code && w.common == promotion.part_number && w.delete_flag == false).ToList();

                if (commons.Count() > 0)
                    promotion.common = 1;
                else
                    promotion.common = 0;
            }
            return promotions;
        }

        public List<PromotionItem> GetPromotionByCode(string promotion_code)
        {
            var prom = _context.PROMOTION_DETAIL
               .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
               .Join(_context.MA_TAX, t => t.ii.part_iva_sale, tt => tt.tax_code, (t, tt) => new { t, tt })
               .Where(w => w.t.i.promotion_code == promotion_code && w.t.i.delete_flag == false)
               .Select(s => new PromotionItem
               {
                   part_number = s.t.i.part_number,
                   description = s.t.ii.part_description,
                   price = s.t.i.promotion_price_iva == null ? ((1 * s.tt.tax_value ?? 0) * s.t.i.promotion_price / 100) + s.t.i.promotion_price : s.t.i.promotion_price_iva ?? 0
               }).ToList();
            var promoPre = _context.PROMOTION_DETAIL
                .Join(_context.ITEM_PRESENTATION, p => p.presentation_id, pres => pres.presentation_id, (p, pp) => new { p, pp })
                .Join(_context.ITEM, i => i.pp.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Join(_context.MA_TAX, t => t.ii.part_iva_sale, tt => tt.tax_code, (t, tt) => new { t, tt })
                .Where(w => w.t.i.p.promotion_code == promotion_code && w.t.i.p.delete_flag == false)
                .Select(s => new PromotionItem
                {
                    part_number = s.t.i.pp.presentation_code,
                    description = s.t.i.pp.presentation_name,
                    price = s.t.i.p.promotion_price_iva == null ? ((1 * s.tt.tax_value ?? 0) * s.t.i.p.promotion_price / 100) + s.t.i.p.promotion_price : s.t.i.p.promotion_price_iva ?? 0
                }).ToList();
            prom.AddRange(promoPre);
            return prom;
        }


        public PROMOTION_HEADER GetPromotion(string promotion_code)
        {
            return _context.PROMOTION_HEADER.Where(w => w.promotion_code == promotion_code).FirstOrDefault();
        }

        public List<SiteModel> GetPromotionSites(string promotion_code)
        {
            return _context.PROMOTION_SITES.Join(_context.SITES, s => s.site_code, ss => ss.site_code, (s, ss) => new { s, ss })
                .Where(w => w.s.promotion_code == promotion_code)
                .Select(e => new SiteModel
                {
                    SiteCode = e.ss.site_code,
                    SiteName = e.ss.site_name,
                    Status = e.s.replication_status
                }).ToList();
        }

        public bool PromotionHeaderEdit(PROMOTION_HEADER model, ModelPromotionEdit ModelEdit, string User)
        {
            try
            {
                model.udate = DateTime.Now;
                model.uuser = User;
                model.promotion_end_date = ModelEdit.promotion_end_date;
                model.promotion_start_date = ModelEdit.promotion_start_date;
                model.program_id = "PURCH023.CSHTML";
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool PromotionHeaderActivate(string promotion_code, string User)
        {
            var model = _context.PROMOTION_HEADER.Where(w => w.promotion_code == promotion_code).SingleOrDefault();
            if (model != null)
            {
                try
                {
                    model.delete_flag = false;
                    model.uuser = User;
                    model.udate = DateTime.Now;
                    model.program_id = "PURCH023.CSHTML";
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            } else
            {
                return false;
            }
        }

        public bool PromotionHeaderDelete(PROMOTION_HEADER model, string User)
        {
            try
            {
                model.delete_flag = true;
                model.uuser = User;
                model.udate = DateTime.Now;
                model.program_id = "PURCH023.CSHTML";
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        //patricio cazarez
        public int AddPromotionDFLPOS(DFLPOS_SALES_PROMOTIONS promotionDFLPOS)
        {
            var check = 0;


            _context.DFLPOS_SALES_PROMOTIONS.Add(promotionDFLPOS);
            try
            {
                check = _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }

            return check;
        }
    }
}
