﻿using App.Entities;
using App.Entities.ViewModels.Promotion;
using App.Entities.ViewModels.SysInfo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Promotion
{
    public class PromotionSiteRepository
    {
        private PRTSAIEntities _context;

        public PromotionSiteRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<PromotionModel> GetPromotions(string Site_code, string Type_Promotion, string Part_Number, DateTime? BeginDate, DateTime? EndDate, bool allstores, int onlyStatus)
        {
            if (Type_Promotion == "VOLANTE")
                Type_Promotion = "NORMAL";
            var begindate = new SqlParameter { ParameterName = "BeginDate", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = BeginDate };
            var enddate = new SqlParameter { ParameterName = "EndDate", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = EndDate };
            var site_code = new SqlParameter { ParameterName = "SiteCode", Value = Site_code };
            var typepromotion = new SqlParameter { ParameterName = "TypePromotion", Value = Type_Promotion };
            var partnumber = new SqlParameter { ParameterName = "PartNumber", Value = Part_Number };
            var AllStores = new SqlParameter { ParameterName = "AllStores", Value = allstores };
            var OnlyStatus = new SqlParameter { ParameterName = "OnlyStatus", Value = onlyStatus };
            if (BeginDate.HasValue)
                begindate.Value = begindate.Value;
            else
                begindate.Value = DBNull.Value;

            if (EndDate.HasValue)
                enddate.Value = enddate.Value;
            else
                enddate.Value = DBNull.Value;
            var model = _context.Database.SqlQuery<PromotionModel>("sp_Get_Promotions @BeginDate, @EndDate, @SiteCode, @TypePromotion, @PartNumber, @AllStores, @OnlyStatus", begindate, enddate, site_code, typepromotion, partnumber, AllStores, OnlyStatus).ToList();
            return model;
        }

        public List<PromotionModel> GetPromotionsDetails(string Site_code, string Type_Promotion, string Part_Number, DateTime? BeginDate, DateTime? EndDate, bool allstores, int onlyStatus)
        {
            if (Type_Promotion == "VOLANTE")
                Type_Promotion = "NORMAL";
            var begindate = new SqlParameter { ParameterName = "BeginDate", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = BeginDate };
            var enddate = new SqlParameter { ParameterName = "EndDate", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = EndDate };
            var site_code = new SqlParameter { ParameterName = "SiteCode", Value = Site_code };
            var typepromotion = new SqlParameter { ParameterName = "TypePromotion", Value = Type_Promotion };
            var partnumber = new SqlParameter { ParameterName = "PartNumber", Value = Part_Number };
            var AllStores = new SqlParameter { ParameterName = "AllStores", Value = allstores };
            var OnlyStatus = new SqlParameter { ParameterName = "OnlyStatus", Value = onlyStatus };
            if (BeginDate.HasValue)
                begindate.Value = begindate.Value;
            else
                begindate.Value = DBNull.Value;

            if (EndDate.HasValue)
                enddate.Value = enddate.Value;
            else
                enddate.Value = DBNull.Value;
            var model = _context.Database.SqlQuery<PromotionModel>("sp_Get_Promotions_Details @BeginDate, @EndDate, @SiteCode, @TypePromotion, @PartNumber, @AllStores, @OnlyStatus", begindate, enddate, site_code, typepromotion, partnumber, AllStores, OnlyStatus).ToList();
            return model;
        }
        public string AddPromotionSite(string promotion_code, string site_code, string promotion_type, string cuser)
        {
            if (site_code == "")
                return "";
            try
            {
                PROMOTION_SITES site = new PROMOTION_SITES();
                site.promotion_code = promotion_code;
                site.type = promotion_type;
                site.site_code = site_code;
                site.replication_status = 0;
                site.delete_flag = false;
                site.cuser = cuser;
                site.cdate = DateTime.Now;
                site.program_id = "PURCH013.cshtml";
                _context.PROMOTION_SITES.Add(site);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "Error al insertar la promoción en la tienda (" + site_code + ").";
            }
            return "";
        }


        public bool UpdateSiteByCode(string site_code, string promotion_code, string promotion_type, string User)
        {
            if (site_code == "" || site_code == "All")
                return true;
            var Update = _context.PROMOTION_SITES.Where(w => w.promotion_code == promotion_code && w.site_code == site_code).SingleOrDefault();
            if (Update != null)
            {
                try
                {
                    Update.uuser = User;
                    Update.udate = DateTime.Now;
                    Update.program_id = "PURCH023.CSHTML";
                    Update.type = promotion_type;
                    Update.replication_status = 4;
                    Update.delete_flag = false;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                PROMOTION_SITES model_new = new PROMOTION_SITES
                {
                    promotion_code = promotion_code,
                    site_code = site_code,
                    type = promotion_type,
                    replication_status = 4,
                    delete_flag = false,
                    cuser = User,
                    cdate = DateTime.Now,
                    program_id = "PURCH023.cshtml"
                };
                try
                {
                    _context.PROMOTION_SITES.Add(model_new);
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
        }

        public bool UpdateSiteDeleteByCode(string site_code, string promotion_code, string promotion_type, string user)
        {
            if (site_code == "" || site_code == "All")
                return true;
            var updateSite = _context.PROMOTION_SITES.Where(w => w.site_code == site_code && w.promotion_code == promotion_code).SingleOrDefault();

            if (updateSite != null)
            {
                try
                {
                    updateSite.uuser = user;
                    updateSite.udate = DateTime.Now;
                    updateSite.program_id = "PURCH023.CHSTML";
                    updateSite.delete_flag = true;
                    updateSite.replication_status = 4;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
                return true;
        }

        public bool UpdateSiteDeleteAll(string promotion_code, string promotion_type, string User)
        {
            var updateSites = _context.PROMOTION_SITES.Where(w => w.promotion_code == promotion_code).ToList();

            try
            {
                foreach (var site in updateSites)
                {
                    site.replication_status = 4;
                    site.program_id = "PURCH023.CHSTML";
                    site.uuser = User;
                    site.udate = DateTime.Now;
                }
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool DeleteSiteByCode(string promotion_code, string promotion_type, string User)
        {
            var deletes = _context.PROMOTION_SITES.Where(w => w.promotion_code == promotion_code).ToList();

            if (deletes.Count() > 0)
            {
                try
                {
                    foreach (var item in deletes)
                    {
                        item.replication_status = 4;
                        item.delete_flag = true;
                    }
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool UpdateAdvancedSiteByCode(string site_code, string promotion_code, string User)
        {
            if (site_code == "" || site_code == "All")
                return true;
            var Update = _context.PROMOTION_SITES.Where(w => w.promotion_code == promotion_code && w.site_code == site_code && w.type == "AVANZADA").SingleOrDefault();
            if (Update != null)
            {
                try
                {
                    Update.uuser = User;
                    Update.udate = DateTime.Now;
                    Update.program_id = "PURCH023.CSHTML";
                    Update.replication_status = 4;
                    Update.delete_flag = false;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                PROMOTION_SITES model_new = new PROMOTION_SITES
                {
                    promotion_code = promotion_code,
                    site_code = site_code,
                    type = "AVANZADA",
                    replication_status = 4,
                    delete_flag = false,
                    cuser = User,
                    cdate = DateTime.Now,
                    program_id = "PURCH023.cshtml"
                };
                try
                {
                    _context.PROMOTION_SITES.Add(model_new);
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
        }

        public bool UpdateAdvancedSiteDeleteByCode(string site_code, string promotion_code, string user)
        {
            if (site_code == "" || site_code == "All")
                return true;
            var updateSite = _context.PROMOTION_SITES.Where(w => w.site_code == site_code && w.promotion_code == promotion_code && w.type == "AVANZADA").SingleOrDefault();
            if (updateSite != null)
            {
                try
                {
                    updateSite.uuser = user;
                    updateSite.udate = DateTime.Now;
                    updateSite.program_id = "PURCH023.CHSTML";
                    updateSite.delete_flag = true;
                    updateSite.replication_status = 4;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
                return true;
        }

        public StoreProcedureResult ReplicatePromotions(string promotion_code, string promotion_type, string site_code)
        {
            _context.Database.CommandTimeout = 15000;
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int  EXEC    @return_value = [dbo].[sp_Replicate_Promotion_Direct] @Id_promotion = N'" + promotion_code + "', @TypeReplicate = N'" + promotion_type + "', @SiteReplicate = N'" + site_code + "' SELECT  'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public bool ActivatePromotionSites(string promotion_code, string promotion_type, string User)
        {

            var model = _context.PROMOTION_SITES.Where(w => w.promotion_code == promotion_code && w.type == promotion_type).ToList();
            if (model.Count() > 0)
            {
                try
                {
                    foreach (var item in model)
                    {
                        item.program_id = "PURCH023.cshtml";
                        item.delete_flag = false;
                        item.cuser = User;
                        item.cdate = DateTime.Now;
                    }
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    var msg = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        //patricio cazarez
        public int AddPromotionSiteDFLPOS(DFLPOS_SALES_PROMOTIONS_SITES promotionSite)
        {
            var check = 0;
            _context.DFLPOS_SALES_PROMOTIONS_SITES.Add(promotionSite);
            try
            {
                check = _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }
            return check;
        }
        public int AddPromotionSiteDFLPOSOnSites(DFLPOS_SALES_PROMOTIONS _pro, List<DFLPOS_SALES_PROMOTIONS_DETAIL> _listsDetails, string site)
        {
            var check = 0;
            var _promId = 0;
            Conecta _conection = new Conecta();
            SqlConnection cnn = _conection.conexion(site);
            var _status = _pro.status ? 1 : 0;
            string _insert = " INSERT INTO DFLPOS_SALES_PROMOTIONS (promotion_name,start_date,end_date,status,item_quantity,item_amount,promotions_by_ticket,ticket_header ,ticket_footer,sale_min,promotion_value ,cuser,cdate,uuser,udate,program_id)" +
                        " VALUES " +
           "( '" + _pro.promotion_name + "', " +
           "'" + _pro.start_date + "', " +
           "'" + _pro.end_date + "', " +
           "" + _status + ", " +
           "" + _pro.item_quantity + ", " +
           "" + _pro.item_amount + ", " +
           "" + _pro.promotions_by_ticket + ", " +
           "'" + _pro.ticket_header + "',  " +
           "'" + _pro.ticket_footer + "',  " +
           "" + _pro.sale_min + ",  " +
           "" + _pro.promotion_value + ", " +
           "'" + _pro.cuser + "',  " +
           "'" + _pro.cdate + "', " +
           "'" + _pro.uuser + "',  " +
           "'" + _pro.udate + "', " +
           "'" + _pro.program_id + "' )";


            cnn.Open();
            SqlCommand cmd = new SqlCommand(_insert, cnn);
            check = cmd.ExecuteNonQuery();
            cnn.Close();
            try
            {
                System.Data.DataSet ds = new System.Data.DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                string strGASSQL = "SELECT  TOP 1 promotion_id FROM DFLPOS_SALES_PROMOTIONS ORDER BY promotion_id DESC";

                SqlDataAdapter sqladaptGAS = new System.Data.SqlClient.SqlDataAdapter(strGASSQL, cnn);

                sqladaptGAS.Fill(ds, "DFLPOS_SALES_PROMOTIONS");
                dt = ds.Tables["DFLPOS_SALES_PROMOTIONS"];

                foreach (System.Data.DataRow filas in dt.Rows)
                {
                    //se obiene el id de la promocion
                    _promId = Int32.Parse(filas["promotion_id"].ToString());//obtiene el ultimo folio



                }

            }
            catch (Exception)
            {
                // MessageBox.Show("sin datos en movtan");
            }

            foreach (DFLPOS_SALES_PROMOTIONS_DETAIL _detail in _listsDetails)
            {
                string _insertDetails = "INSERT INTO DFLPOS_SALES_PROMOTIONS_DETAIL  (promotion_id,part_number,item_amount,item_quantity)" +
                                        "VALUES " +
                                       "(" + _promId + "" +
                                       ",'" + _detail.part_number + "' " +
                                       "," + _detail.item_amount + " " +
                                       "," + _detail.item_quantity + ")";

                cnn.Open();
                SqlCommand cmdD = new SqlCommand(_insertDetails, cnn);
                check = cmdD.ExecuteNonQuery();
                cnn.Close();
            }
            return check;
        }
    }
}
