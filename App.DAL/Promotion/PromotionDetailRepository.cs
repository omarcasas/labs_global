﻿using App.Entities;
using App.Entities.ViewModels.Promotion;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace App.DAL.Promotion
{
    public class PromotionDetailRepository
    {
        private PRTSAIEntities _context;


        public PromotionDetailRepository()
        {
            _context = new PRTSAIEntities();
        }

        public string AddPromotionDetail(ModelPromotionItemEdit promotionDetail, string promotion_code, StoreProcedureIvaIeps Tax, string cuser)
        {

            try
            {
                PROMOTION_DETAIL promotion = new PROMOTION_DETAIL();
                promotion.promotion_code = promotion_code;
                if (promotionDetail.presentation_flag)
                {
                    promotion.presentation_id = promotionDetail.presentation_id;
                    promotion.part_number = "PRE-" + promotionDetail.presentation_id.ToString();
                }
                else
                {
                    promotion.part_number = promotionDetail.part_number;
                    promotion.presentation_id = null;
                }
                promotion.promotion_price = (promotionDetail.promotion_price / (1 + (Tax.Iva) / 100)); //Remover IVA si tiene el producto
                promotion.promotion_price_iva = promotionDetail.promotion_price;
                promotion.delete_flag = false;
                promotion.cuser = cuser;
                promotion.cdate = DateTime.Now;
                promotion.program_id = "PURCH013.cshtml";

                _context.PROMOTION_DETAIL.Add(promotion);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "Error al momento de agregar el producto: " + promotionDetail.part_number + ".";
            }
            return "";
        }

        public List<PROMOTION_DETAIL> GetPromotionDetailById(string promotion_code)
        {
            return _context.PROMOTION_DETAIL.Where(w => w.promotion_code == promotion_code && w.delete_flag == false).ToList();
        }

        public List<SiteModel> GetPromotionsSite(string promotion_code, string type_promotion)
        {
            List<SiteModel> sitesAll = new List<SiteModel>();
            sitesAll.Add(new SiteModel { SiteName = "Todas las Tiendas", SiteCode = "All" });
            return _context.PROMOTION_SITES
                .Join(_context.SITES, ps => ps.site_code, s => s.site_code, (ps, s) => new { ps, s })
                .Where(w => w.ps.promotion_code == promotion_code && w.ps.delete_flag == false)
                .Select(s => new SiteModel
                {
                    SiteName = s.s.site_name,
                    SiteCode = s.s.site_code,
                    Status = s.ps.replication_status
                }).ToList();
        }


        public bool UpdateItemByCode(ModelPromotionItemEdit model, decimal tax, string promotion_code, string user)
        {
            PROMOTION_DETAIL obj = new PROMOTION_DETAIL();
            if (model.presentation_flag)
                obj = _context.PROMOTION_DETAIL.Where(w => w.promotion_code == promotion_code && w.presentation_id == model.presentation_id).FirstOrDefault();
            else
                obj = _context.PROMOTION_DETAIL.Where(w => w.promotion_code == promotion_code && w.part_number == model.part_number).FirstOrDefault();

            if (obj != null)
            {
                try
                {
                    obj.program_id = "PURCH023.CSHTML";
                    obj.udate = DateTime.Now;
                    obj.uuser = user;
                    obj.promotion_price = (model.promotion_price / (1 + tax / 100));
                    obj.promotion_price_iva = model.promotion_price;
                    obj.delete_flag = model.delete_flag;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                PROMOTION_DETAIL model_new = new PROMOTION_DETAIL();
                model_new.promotion_code = promotion_code;
                model_new.promotion_price = (model.promotion_price / (1 + tax / 100));
                model_new.promotion_price_iva = model.promotion_price;
                model_new.delete_flag = false;
                model_new.cuser = user;
                model_new.cdate = DateTime.Now;
                model_new.program_id = "PURCH023.cshtml";
                if (model.presentation_flag)
                {
                    model_new.presentation_id = model.presentation_id;
                    model.part_number = "PRE-" + model.presentation_id.ToString();
                }
                else
                {
                    model_new.part_number = model.part_number;
                    model_new.presentation_id = null;
                }

                try
                {
                    _context.PROMOTION_DETAIL.Add(model_new);
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
        }

        public bool UpdateItemDeleteByCode(ModelPromotionItemEdit model, string promotion_code, string user)
        {
            PROMOTION_DETAIL obj = new PROMOTION_DETAIL();
            if (model.presentation_flag)
                obj = _context.PROMOTION_DETAIL.Where(w => w.presentation_id == model.presentation_id && w.promotion_code == promotion_code).SingleOrDefault();
            else
                obj = _context.PROMOTION_DETAIL.Where(w => w.part_number == model.part_number && w.promotion_code == promotion_code).SingleOrDefault();

            if (obj != null)
            {
                try
                {
                    obj.uuser = user;
                    obj.udate = DateTime.Now;
                    obj.program_id = "PURCH023.cshtml";
                    obj.delete_flag = true;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
                return true;
        }
        //patricio cazarez
        public int AddPromotionDetailDFLPOS(DFLPOS_SALES_PROMOTIONS_DETAIL promotionDetail)
        {
            var check = 0;
            _context.DFLPOS_SALES_PROMOTIONS_DETAIL.Add(promotionDetail);
            try
            {
                check = _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }
            return check;
        }
    }
}
