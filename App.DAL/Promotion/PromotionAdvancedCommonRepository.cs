﻿using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace App.DAL.Promotion
{
    public class PromotionAdvancedCommonRepository
    {
        private PRTSAIEntities _context;

        public PromotionAdvancedCommonRepository()
        {
            _context = new PRTSAIEntities();
        }

        public string AddPromotionCommon(PromotionCommonEdit modelCommmon, string promotion_code, string cuser)
        {
            try
            {
                PROMOTION_ADVANCED_COMMON promotion = new PROMOTION_ADVANCED_COMMON()
                {
                    promotion_advanced_code = promotion_code,
                    common = modelCommmon.part_number_origin,
                    item = modelCommmon.part_number,
                    delete_flag = false,
                    cuser = cuser,
                    cdate = DateTime.Now,
                    program_id = "PURCH013.cshtml"
                };
                _context.PROMOTION_ADVANCED_COMMON.Add(promotion);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "Error al momento de agregar el producto: " + modelCommmon.part_number + ".";
            }
            return "";
        }
        public bool UpdateCommonByPromotionCode(string common, string item, string promotion_code, string user)
        {
            var itemUpdate = _context.PROMOTION_ADVANCED_COMMON.Where(w => w.promotion_advanced_code == promotion_code && w.common == common && w.item == item).SingleOrDefault();
            if (itemUpdate != null)
            {
                try
                {
                    itemUpdate.program_id = "PURCH023.CSHTML";
                    itemUpdate.udate = DateTime.Now;
                    itemUpdate.uuser = user;
                    itemUpdate.delete_flag = false;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                try
                {
                    PROMOTION_ADVANCED_COMMON model_new = new PROMOTION_ADVANCED_COMMON
                    {
                        promotion_advanced_code = promotion_code,
                        common = common,
                        item = item,
                        delete_flag = false,
                        cuser = user,
                        cdate = DateTime.Now,
                        program_id = "PURCH023.cshtml"
                    };
                    _context.PROMOTION_ADVANCED_COMMON.Add(model_new);
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
        }

        public bool UpdateCommonDeleteByPromotionCode(string common, string item, string promotion_code, string user)
        {
            var updateItem = _context.PROMOTION_ADVANCED_COMMON.Where(w => w.promotion_advanced_code == promotion_code && w.common == common && w.item == item).SingleOrDefault();
            if (updateItem != null)
            {
                try
                {
                    updateItem.uuser = user;
                    updateItem.udate = DateTime.Now;
                    updateItem.program_id = "PURCH023.cshtml";
                    updateItem.delete_flag = true;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
                return true;
        }

        public List<PromotionItem> GetPromotionAdvancedCommonByCode(string promotion_code, string common)
        {
            var xx = _context.PROMOTION_ADVANCED_COMMON
                .Join(_context.ITEM, PAC => PAC.item, I => I.part_number, (PAC, I) => new { PAC, I })
                .Where(w => w.PAC.promotion_advanced_code == promotion_code && w.PAC.common == common && w.PAC.delete_flag == false)
                .Select(s => new PromotionItem
                {
                    part_number = s.PAC.item,
                    description = s.I.part_description
                }).ToList();
            return xx;
        }
    }
}
