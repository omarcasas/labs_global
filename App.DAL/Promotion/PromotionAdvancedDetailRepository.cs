﻿using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Promotion
{
    public class PromotionAdvancedDetailRepository
    {
        private PRTSAIEntities _context;
        public PromotionAdvancedDetailRepository()
        {
            _context = new PRTSAIEntities();
        }
        public string AddPromotionDetail(ModelPromotionItemEdit model, string promotion_code, StoreProcedureIvaIeps Tax, string cuser)
        {
            try
            {
                PROMOTION_ADVANCED_DETAIL promotion = new PROMOTION_ADVANCED_DETAIL()
                {
                    promotion_code = promotion_code,
                    part_number = model.part_number,
                    promotion_quantity = model.quantity_total,
                    promotion_price = (model.promotion_price / (1 + (Tax.Iva) / 100)), //Remover IVA si tiene el producto
                    promotion_price_iva = model.promotion_price,
                    delete_flag = false,
                    cuser = cuser,
                    cdate = DateTime.Now,
                    program_id = "PURCH013.cshtml"
                };
                _context.PROMOTION_ADVANCED_DETAIL.Add(promotion);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "Error al momento de agregar el producto: " + model.part_number + ".";
            }
            return "";
        }
        public List<PROMOTION_ADVANCED_DETAIL> GetPromotionDetailById(string promotion_code)
        {
            return _context.PROMOTION_ADVANCED_DETAIL.Where(w => w.promotion_code == promotion_code && w.delete_flag == false).ToList();
        }

        public List<PROMOTION_ADVANCED_COMMON> GetPromotionCommonByIdAndPartNumber(string promotion_code, string part_number)
        {
            return _context.PROMOTION_ADVANCED_COMMON.Where(w => w.promotion_advanced_code == promotion_code && w.common == part_number && w.delete_flag == false).ToList();
        }
        public List<PROMOTION_ADVANCED_COMMON> GetPromotionCommonByIdOnly(string promotion_code)
        {
            return _context.PROMOTION_ADVANCED_COMMON.Where(w => w.promotion_advanced_code == promotion_code && w.delete_flag == false).ToList();
        }

        public bool UpdateItemByPromotionCode(ModelPromotionItemEdit model, decimal tax, string promotion_code, string user)
        {
            var itemUpdate = _context.PROMOTION_ADVANCED_DETAIL.Where(w => w.promotion_code == promotion_code && w.part_number == model.part_number).FirstOrDefault();
            if (itemUpdate != null)
            {
                try
                {
                    itemUpdate.program_id = "PURCH023.CSHTML";
                    itemUpdate.udate = DateTime.Now;
                    itemUpdate.uuser = user;
                    itemUpdate.promotion_price = (model.promotion_price / (1 + tax / 100));
                    itemUpdate.promotion_price_iva = model.promotion_price;
                    itemUpdate.delete_flag = false;
                    itemUpdate.promotion_quantity = model.quantity_total;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                try
                {
                    PROMOTION_ADVANCED_DETAIL model_new = new PROMOTION_ADVANCED_DETAIL
                    {
                        promotion_code = promotion_code,
                        part_number = model.part_number,
                        promotion_price = (model.promotion_price / (1 + tax / 100)),
                        promotion_price_iva = model.promotion_price,
                        promotion_quantity = model.quantity_total,
                        delete_flag = false,
                        cuser = user,
                        cdate = DateTime.Now,
                        program_id = "PURCH023.cshtml"
                    };
                    _context.PROMOTION_ADVANCED_DETAIL.Add(model_new);
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
        }

        public bool UpdateItemDeleteByPromotionCode(string part_number, string promotion_code, string user)
        {
            var updateItem = _context.PROMOTION_ADVANCED_DETAIL.Where(w => w.part_number == part_number && w.promotion_code == promotion_code).SingleOrDefault();

            if (updateItem != null)
            {
                try
                {
                    updateItem.uuser = user;
                    updateItem.udate = DateTime.Now;
                    updateItem.program_id = "PURCH023.cshtml";
                    updateItem.delete_flag = true;
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
                return true;
        }
    }
}
