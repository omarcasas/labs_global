﻿using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Promotion
{
    public class PromotionAdvancedHeaderRepository
    {
        private PRTSAIEntities _context;
        public PromotionAdvancedHeaderRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int CountHeader()
        {
            var date = DateTime.Now;
            DayOfWeek fdow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            int offset = fdow - date.DayOfWeek;
            DateTime fdowDate = date.AddDays(offset);
            DateTime ldowDate = fdowDate.AddDays(6);
            var StartDate = fdowDate.Date;
            var EndDate = ldowDate.Date;
            return _context.PROMOTION_ADVANCED_HEADER.Where(w => w.cdate >= StartDate && w.cdate <= EndDate).Select(x => x).Count();
        }

        public string AddPromotionAdvanced(PromotionModel modelHeader, string buyer_division, string cuser)
        {
            try
            {
                PROMOTION_ADVANCED_HEADER promotion = new PROMOTION_ADVANCED_HEADER()
                {
                    promotion_advanced_code = modelHeader.promotion_code,
                    promotion_description = modelHeader.promotion_descrip,
                    promotion_start_date = modelHeader.promotion_start_date,
                    promotion_end_date = modelHeader.promotion_end_date,
                    buyer_division = buyer_division,
                    delete_flag = false,
                    cuser = cuser,
                    cdate = DateTime.Now,
                    program_id = "PURCH013.cshtml"
                };
                _context.PROMOTION_ADVANCED_HEADER.Add(promotion);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "";
            }

            return modelHeader.promotion_code;

        }

        public PROMOTION_ADVANCED_HEADER GetPromotion(string promotion_code)
        {
            return _context.PROMOTION_ADVANCED_HEADER.Where(w => w.promotion_advanced_code == promotion_code).FirstOrDefault();
        }

        public bool PromotionHeaderEdit(PROMOTION_ADVANCED_HEADER model, ModelPromotionEdit ModelEdit, string User)
        {
            try
            {
                model.udate = DateTime.Now;
                model.uuser = User;
                model.promotion_description = ModelEdit.description;
                model.promotion_end_date = ModelEdit.promotion_end_date;
                model.promotion_start_date = ModelEdit.promotion_start_date;
                model.program_id = "PURCH023.CSHTML";
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }


        public bool PromotionHeaderDelete(PROMOTION_ADVANCED_HEADER model, string User)
        {
            try
            {
                model.delete_flag = true;
                model.uuser = User;
                model.udate = DateTime.Now;
                model.program_id = "PURCH023.CSHTML";
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }


        public bool PromotionHeaderActivate(string promotion_code, string User)
        {
            var model = _context.PROMOTION_ADVANCED_HEADER.Where(w => w.promotion_advanced_code == promotion_code).SingleOrDefault();

            if (model != null)
            {
                try
                {
                    model.delete_flag = false;
                    model.uuser = User;
                    model.udate = DateTime.Now;
                    model.program_id = "PURCH023.CSHTML";
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

    }
}
