﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL
{
    public class StoreProcedureIvaIeps
    {
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
    }
}
