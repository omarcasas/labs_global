﻿using App.Entities;
using App.Entities.ViewModels.MaquineryEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.MaquineryEquipment
{
    public class MaquineryEquipmentRepository
    {
        private readonly PRTSAIEntities _context;

        public MaquineryEquipmentRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<MaquineryEquipmentModel> getAll()
        {
            var model = _context.MAQUINERY_EQUIPMENT
               .Join(_context.SITES, equipment => equipment.site_code, site => site.site_code, (equipment, site) => new { equipment, site })
               .Join(_context.MA_CODE, e => e.equipment.department_id, department => department.vkey, (e, department) => new { e, department })
               .Select(element => new MaquineryEquipmentModel
               {
                   EquipmentId = element.e.equipment.equipment_id,
                   SerialNumber = element.e.equipment.serial_number,
                   EquipmentDescription = element.e.equipment.equipment_description,
                   SiteCode = element.e.equipment.site_code,
                   Site = element.e.site.site_name,
                   DepartmentId = element.e.equipment.department_id,
                   Department = element.department.description,
                   SerialNumberSupplier = element.e.equipment.supplier_serial_number ?? "N/A",
                   FlagActive = element.e.equipment.active
               }).ToList();
            return model;
        }

        public MaquineryEquipmentModel getEquipmentById(int id)
        {
            var model = _context.MAQUINERY_EQUIPMENT
               .Join(_context.SITES, equipment => equipment.site_code, site => site.site_code, (equipment, site) => new { equipment, site })
               .Join(_context.MA_CODE, e => e.equipment.department_id, department => department.vkey, (e, department) => new { e, department })
               .Where(x => x.e.equipment.equipment_id == id)
               .Select(element => new MaquineryEquipmentModel
               {
                   EquipmentId = element.e.equipment.equipment_id,
                   SerialNumber = element.e.equipment.serial_number,
                   EquipmentDescription = element.e.equipment.equipment_description,
                   SiteCode = element.e.equipment.site_code,
                   Site = element.e.site.site_name,
                   DepartmentId = element.e.equipment.department_id,
                   Department = element.department.description,
                   SerialNumberSupplier = element.e.equipment.supplier_serial_number ?? "N/A"
               }).FirstOrDefault();
            return model;
        }

        public MaquineryEquipmentModel getEquipmentBySerialNumber(string serial_number)
        {
            var model = _context.MAQUINERY_EQUIPMENT
               .Join(_context.SITES, equipment => equipment.site_code, site => site.site_code, (equipment, site) => new { equipment, site })
               .Join(_context.MA_CODE, e => e.equipment.department_id, department => department.vkey, (e, department) => new { e, department })
               .Where(x => x.e.equipment.serial_number == serial_number)
               .Select(element => new MaquineryEquipmentModel
               {
                   EquipmentId = element.e.equipment.equipment_id,
                   SerialNumber = element.e.equipment.serial_number,
                   EquipmentDescription = element.e.equipment.equipment_description,
                   SiteCode = element.e.equipment.site_code,
                   Site = element.e.site.site_name,
                   DepartmentId = element.e.equipment.department_id,
                   Department = element.department.description,
                   SerialNumberSupplier = element.e.equipment.supplier_serial_number ?? "N/A"
               }).FirstOrDefault();
            return model;
        }

        public List<MaquineryEquipmentModel> getAllBySite(string site_code)
        {
            var model = _context.MAQUINERY_EQUIPMENT
               .Join(_context.SITES, equipment => equipment.site_code, site => site.site_code, (equipment, site) => new { equipment, site })
               .Join(_context.MA_CODE, e => e.equipment.department_id, department => department.vkey, (e, department) => new { e, department })
               .Where(x => x.e.equipment.site_code == site_code && x.e.equipment.active == true)
               .Select(element => new MaquineryEquipmentModel
               {
                   EquipmentId = element.e.equipment.equipment_id,
                   SerialNumber = element.e.equipment.serial_number,
                   EquipmentDescription = element.e.equipment.equipment_description,
                   SiteCode = element.e.equipment.site_code,
                   Site = element.e.site.site_name,
                   DepartmentId = element.e.equipment.department_id,
                   Department = element.department.description,
                   SerialNumberSupplier = element.e.equipment.supplier_serial_number ?? "N/A"
               }).ToList();
            return model;
        }

        public List<MaquineryEquipmentDropDownModel> GetAllBySiteAndDepartmentToDropDown(string departmentt, string site_code, int[] ids)
        {
            var model = _context.MAQUINERY_EQUIPMENT
               .Where(x => x.site_code == site_code && x.department_id == departmentt && x.active == true && !ids.Contains(x.equipment_id))
               .Select(element => new MaquineryEquipmentDropDownModel
               {
                   equipment_id = element.equipment_id,
                   equipment_description = element.equipment_description
               }).ToList();
            return model;
        }
        public List<MaquineryEquipmentDropDownModel> GetAllBySiteAndDepartmentToDropDown(string departmentt, string site_code)
        {
            var model = _context.MAQUINERY_EQUIPMENT
               .Where(x => x.site_code == site_code && x.department_id == departmentt && x.active == true)
               .Select(element => new MaquineryEquipmentDropDownModel
               {
                   equipment_id = element.equipment_id,
                   equipment_description = element.equipment_description
               }).ToList();
            return model;
        }

        public bool validateSerialNumber(string SerialNumber)
        {
            try
            {
                if (_context.MAQUINERY_EQUIPMENT.Where(x => x.serial_number == SerialNumber && x.active == true).Count() > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool validateExistingSerialNumber(string SerialNumber, int EquipmentId)
        {
            try
            {
                if (_context.MAQUINERY_EQUIPMENT.Where(x => x.serial_number == SerialNumber && x.equipment_id != EquipmentId && x.active == true).Count() > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool addMaquineryEquipment(MaquineryEquipmentModel model, string user)
        {
            try
            {
                MAQUINERY_EQUIPMENT MAQUINERY_EQUIPMENT = new MAQUINERY_EQUIPMENT
                {
                    serial_number = model.SerialNumber,
                    supplier_serial_number = model.SerialNumberSupplier,
                    equipment_description = model.EquipmentDescription,
                    site_code = model.SiteCode,
                    department_id = model.DepartmentId,
                    cdate = DateTime.Now,
                    cuser = user,
                    program_id = "MRO002.cshtml",
                    active = true
                };
                _context.MAQUINERY_EQUIPMENT.Add(MAQUINERY_EQUIPMENT);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return false;
            }
        }

        public bool setMaquineryEquipment(MaquineryEquipmentModel model, string user)
        {
            try
            {
                MAQUINERY_EQUIPMENT MAQUINERY_EQUIPMENT = _context.MAQUINERY_EQUIPMENT.Where(x => x.equipment_id == model.EquipmentId).FirstOrDefault();
                if (MAQUINERY_EQUIPMENT != null)
                {
                    MAQUINERY_EQUIPMENT.serial_number = model.SerialNumber;
                    MAQUINERY_EQUIPMENT.supplier_serial_number = model.SerialNumberSupplier;
                    MAQUINERY_EQUIPMENT.equipment_description = model.EquipmentDescription;
                    MAQUINERY_EQUIPMENT.site_code = model.SiteCode;
                    MAQUINERY_EQUIPMENT.department_id = model.DepartmentId;
                    MAQUINERY_EQUIPMENT.udate = DateTime.Now;
                    MAQUINERY_EQUIPMENT.uuser = user;
                    MAQUINERY_EQUIPMENT.program_id = "MRO002.cshtml";
                    _context.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool removeMaquineryEquipment(int EquipmentId, bool FlagActive, string user)
        {
            try
            {
                MAQUINERY_EQUIPMENT MAQUINERY_EQUIPMENT = _context.MAQUINERY_EQUIPMENT.Where(x => x.equipment_id == EquipmentId).FirstOrDefault();
                if (MAQUINERY_EQUIPMENT != null)
                {
                    MAQUINERY_EQUIPMENT.udate = DateTime.Now;
                    MAQUINERY_EQUIPMENT.uuser = user;
                    MAQUINERY_EQUIPMENT.program_id = "MRO002.cshtml";
                    MAQUINERY_EQUIPMENT.active = !FlagActive;
                    _context.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}