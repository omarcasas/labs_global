﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SAT
{
    public class productCatalogueSatRepository
    {
        private CFDI33Entities _context;

        public productCatalogueSatRepository()
        {
            _context = new CFDI33Entities();
        }
        public List<SATDivision> GetAllSATDivision()
        {
            return _context.SATDivision.OrderBy(x => x.division).ToList();
        }
        public List<SATGrupo> GetAllSATGrupo(string cve_division)
        {
            return _context.SATGrupo.Where(x => x.cve_division == cve_division).OrderBy(x => x.grupo).ToList();
        }
        public List<SATClase> GetAllSATClase(string cve_grupo)
        {
            return _context.SATClase.Where(x => x.cve_grupo == cve_grupo).OrderBy(x => x.clase).ToList();
        }
        public List<SATCatalogoProdServ> GetAllSATCatalogoProdServ(string Filter)
        {
            try
            {
                var items = _context.Database.SqlQuery<SATCatalogoProdServ>(@"select c_ClaveProdServ,null AS tipo,'('+c_ClaveProdServ+') ' +Descripcion_ProdServ AS Descripcion_ProdServ ,FechaInicioVigencia,FechaFinVigencia,null as Incluir_IVA_trasladado,null as Incluir_IEPS_trasladado
                ,null as Complemento,null as cve_clase,null as cve_grupo,null as cve_division from SATCatalogoProdServ
                where Descripcion_ProdServ like @filter OR c_ClaveProdServ like @filter",
                new SqlParameter("filter", string.Format("%{0}%", Filter))).ToList();
                return items;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<SATum> GetAllSATum(string Filter)
        {
            try
            {
                var items = _context.Database.SqlQuery<SATum>(@"select c_ClaveUnidad,nombre,null as descripcion,null as fecha_iniciovigencia,null as fecha_finvigencia,null as simbolo,activo,orden from SATum
                where nombre like @filter",
                new SqlParameter("filter", string.Format("%{0}%", Filter))).ToList();
                return items;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<SATum> GetAllSATumById(string Filter)
        {
            try
            {
                var items = _context.Database.SqlQuery<SATum>(@"select c_ClaveUnidad,nombre,null as descripcion,null as fecha_iniciovigencia,null as fecha_finvigencia,null as simbolo,activo,orden from SATum
                where c_ClaveUnidad like @filter",
                new SqlParameter("filter", string.Format("%{0}%", Filter))).ToList();
                return items;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
    }
}
