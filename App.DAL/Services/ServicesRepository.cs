﻿using App.Entities;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Services
{
    public class ServicesRepository
    {
        private PRTSAIEntities _context;

        public ServicesRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<DFLPOS_SERVICES> Services()
        {
            return _context.DFLPOS_SERVICES.Where(c => c.enabled).OrderBy(d => d.order).ToList();
        }

        public List<Service> GetServices()
        {
            var List = new List<Service>();
            List = (from s in _context.DFLPOS_SERVICES
                    select new Service
                    {
                        ServiceId = s.id,
                        Name = s.description,
                        Order = s.order,
                        IsActive = s.enabled
                    }).ToList();

            return List;
        }

        public bool AddService(DFLPOS_SERVICES Item)
        {
            try
            {
                if (UpdateOrderService(Item))
                {
                    _context.DFLPOS_SERVICES.Add(Item);
                    _context.SaveChanges();

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        bool UpdateOrderService(DFLPOS_SERVICES Item)
        {
            try
            {
                var PreviousItemOrder = _context.DFLPOS_SERVICES.Where(c => c.order == Item.order).ToList();
                var Services = _context.DFLPOS_SERVICES.ToList();
                int cont = Services.Count();

                if (PreviousItemOrder.Any())
                {
                    foreach (var Service in PreviousItemOrder)
                    {
                        cont++;
                        var serviceEdit = _context.DFLPOS_SERVICES.Find(Service.id);
                        Service.order = cont;
                        _context.Entry(serviceEdit).State = System.Data.Entity.EntityState.Detached;
                        _context.Entry(Service).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public Service GetServiceById(int ServiceId)
        {
            var Service = new Service();
            Service = (from s in _context.DFLPOS_SERVICES
                       where s.id == ServiceId
                       select new Service
                       {
                           Name = s.description,
                           ServiceId = s.id,
                           IsActive = s.enabled,
                           Order = s.order
                       }).FirstOrDefault();

            return Service;
        }

        public bool UpdateService(DFLPOS_SERVICES Item)
        {
            var ItemEdit = _context.DFLPOS_SERVICES.Find(Item.id);
            if(ItemEdit != null)
            {
                if (UpdateOrderService(Item))
                {
                    _context.Entry(ItemEdit).State = System.Data.Entity.EntityState.Detached;
                    _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    return true;
                }

                return false;
            }

            return false;
        }

        public bool DisableService(int ServiceId)
        {
            var Service = _context.DFLPOS_SERVICES.Find(ServiceId);

            if(Service != null)
            {
                Service.enabled = false;
                _context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
