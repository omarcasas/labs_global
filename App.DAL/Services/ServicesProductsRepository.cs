﻿using App.Entities;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Services
{
    public class ServicesProductsRepository
    {
        private PRTSAIEntities _context;

        public ServicesProductsRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool AddServicesProduct(DFLPOS_SERVICES_PRODUCTS Item)
        {
            try
            {
                _context.DFLPOS_SERVICES_PRODUCTS.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<ServiceProduct> GetProductsList()
        {
            var List = new List<ServiceProduct>();

            List = (from sp in _context.DFLPOS_SERVICES_PRODUCTS
                    select new ServiceProduct
                    {
                        ProductId = sp.id,
                        ServiceName = sp.DFLPOS_SERVICES.description,
                        CompanyName = sp.DFLPOS_SERVICES_COMPANIES.company,
                        Description = sp.description,
                        PartNumber = sp.part_number,
                        Price = sp.price
                    }).ToList();

            return List;
        }

        public ServiceProduct GetProductById(int ProductId)
        {
            var Item = new ServiceProduct();

            Item = (from sp in _context.DFLPOS_SERVICES_PRODUCTS
                    where sp.id == ProductId
                    select new ServiceProduct
                    {
                        ProductId = sp.id,
                        Description = sp.description,
                        CompanyId = sp.company_id,
                        CompanyName = sp.DFLPOS_SERVICES_COMPANIES.company,
                        PartNumber = sp.part_number,
                        Sku = sp.sku,
                        Price = sp.price,
                        Commission = sp.commission,
                        Type = sp.type == 1 ? TypeEnum.One : TypeEnum.Three,
                        Enabled = sp.enabled,
                        Order = sp.order,
                        VerifyDigit = sp.verify_digit.HasValue ? sp.verify_digit.Value.ToString() : string.Empty,
                        CommissionPartNumber = sp.commission_part_number,
                        Provider = sp.provider,
                        ServiceId = sp.service_id,
                        ServiceName = sp.DFLPOS_SERVICES.description
                    }).FirstOrDefault();

            return Item;
        }

        public bool UpdateServiceProduct(DFLPOS_SERVICES_PRODUCTS Item)
        {
            var ItemEdit = _context.DFLPOS_SERVICES_PRODUCTS.Find(Item.id);
            if(ItemEdit != null)
            {
                if (UpdateOrderProducts(Item))
                {
                    _context.Entry(ItemEdit).State = System.Data.Entity.EntityState.Detached;
                    _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public bool DisableServiceProduct(int ProductId)
        {
            var Product = _context.DFLPOS_SERVICES_PRODUCTS.Find(ProductId);

            if(Product != null)
            {
                Product.enabled = false;
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        bool UpdateOrderProducts(DFLPOS_SERVICES_PRODUCTS Item)
        {
            try
            {
                var PreviousItemOrder = _context.DFLPOS_SERVICES_PRODUCTS.Where(c => c.order == Item.order && c.company_id == Item.company_id && c.service_id == Item.service_id).ToList();
                var Products = _context.DFLPOS_SERVICES_PRODUCTS.Where(c => c.company_id == Item.company_id && c.service_id == Item.service_id).ToList();
                int cont = Products.Count();

                if (PreviousItemOrder.Any())
                {
                    foreach (var Product in PreviousItemOrder)
                    {
                        cont++;
                        var serviceEdit = _context.DFLPOS_SERVICES_PRODUCTS.Find(Product.id);
                        Product.order = cont;
                        _context.Entry(serviceEdit).State = System.Data.Entity.EntityState.Detached;
                        _context.Entry(Product).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}
