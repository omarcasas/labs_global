﻿using App.Entities;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Services
{
    public class ServicesCompaniesRepository
    {
        private PRTSAIEntities _context;

        public ServicesCompaniesRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<DFLPOS_SERVICES_COMPANIES> ServicesCompanies()
        {
            return _context.DFLPOS_SERVICES_COMPANIES.OrderBy(c => c.order).ToList();
        }

        public List<ServiceCompany> GetCompaniesList()
        {
            var List = new List<ServiceCompany>();
            List = (from sc in _context.DFLPOS_SERVICES_COMPANIES
                    select new ServiceCompany
                    {
                        CompanyId = sc.id,
                        Name = sc.company,
                        Order = sc.order,
                        IsActive = sc.enabled
                    }).ToList();
            return List;
        }

        public bool AddCompany(DFLPOS_SERVICES_COMPANIES Item)
        {
            try
            {
                _context.DFLPOS_SERVICES_COMPANIES.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        bool UpdateOrderServiceCompany(DFLPOS_SERVICES_COMPANIES Item)
        {
            try
            {
                var PreviousItemOrder = _context.DFLPOS_SERVICES_COMPANIES.Where(c => c.order == Item.order).ToList();
                var ServicesCompanies = _context.DFLPOS_SERVICES_COMPANIES.ToList();
                int cont = ServicesCompanies.Count();

                if (PreviousItemOrder.Any())
                {
                    foreach (var ServiceCompany in PreviousItemOrder)
                    {
                        cont++;
                        var serviceEdit = _context.DFLPOS_SERVICES_COMPANIES.Find(ServiceCompany.id);
                        ServiceCompany.order = cont;
                        _context.Entry(serviceEdit).State = System.Data.Entity.EntityState.Detached;
                        _context.Entry(ServiceCompany).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public ServiceCompany GetServiceCompanyById(int CompanyId)
        {
            var Service = new ServiceCompany();
            Service = (from s in _context.DFLPOS_SERVICES_COMPANIES
                       where s.id == CompanyId
                       select new ServiceCompany
                       {
                           Name = s.company,
                           CompanyId = s.id,
                           IsActive = s.enabled,
                           Order = s.order
                       }).FirstOrDefault();

            return Service;
        }

        public bool UpdateServiceCompany(DFLPOS_SERVICES_COMPANIES Item)
        {
            var ItemEdit = _context.DFLPOS_SERVICES_COMPANIES.Find(Item.id);
            if (ItemEdit != null)
            {
                if (UpdateOrderServiceCompany(Item))
                {
                    _context.Entry(ItemEdit).State = System.Data.Entity.EntityState.Detached;
                    _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    return true;
                }

                return false;
            }

            return false;
        }

        public bool DisableServiceCompany(int CompanyId)
        {
            var ServiceCompany = _context.DFLPOS_SERVICES_COMPANIES.Find(CompanyId);

            if (ServiceCompany != null)
            {
                ServiceCompany.enabled = false;
                _context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
