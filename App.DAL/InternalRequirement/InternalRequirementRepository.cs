﻿using App.Entities;
using App.Entities.ViewModels.InternalRequirement;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.InternalRequirement
{
    public class InternalRequirementRepository
    {
        private PRTSAIEntities _context;

        public InternalRequirementRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<InternalRequirementModel> GetAllItemsCreditors()
        {
            try
            {

                var item = _context.Database.SqlQuery<InternalRequirementModel>(@"select ITEM_CREDITORS.part_number as part_number,ITEM_CREDITORS.description as description,ITEM_CREDITORS.type as type , ITEM_CREDITORS_CATEGORY.description as category , ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors as id_item_supplier , MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price from ITEM_CREDITORS
join ITEM_CREDITORS_CATEGORY on ITEM_CREDITORS.category = ITEM_CREDITORS_CATEGORY.category_id
join ITEM_SUPPLIER_CREDITORS on ITEM_SUPPLIER_CREDITORS.part_number_creditors = ITEM_CREDITORS.part_number
join MA_SUPPLIER on ITEM_SUPPLIER_CREDITORS.supplier_id = MA_SUPPLIER.supplier_id
LEFT JOIN ITEM_CREDITORS_PRICE on ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
where ITEM_CREDITORS.active_flag = 1 and ITEM_CREDITORS.requisition_flag = 1 and ITEM_SUPPLIER_CREDITORS.flag_active = 1;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public List<InternalRequirementModel> SearchItemsCreditors(string search)
        {
            var itemCreditors = from i in _context.ITEM_CREDITORS
                                join cat in _context.ITEM_CREDITORS_CATEGORY on i.category equals cat.category_id
                                join isup in _context.ITEM_SUPPLIER_CREDITORS on i.part_number equals isup.part_number_creditors
                                join sup in _context.MA_SUPPLIER on isup.supplier_id equals sup.supplier_id
                                join price in _context.ITEM_CREDITORS_PRICE on isup.supplier_part_number_creditors equals price.supplier_part_number_creditors
                                into tt
                                from price2 in tt.DefaultIfEmpty()
                                where i.active_flag == true && i.requisition_flag == true && isup.flag_active == true && !true != !!false && price2.flag_active
                                where i.description.Contains(search) || i.part_number.Contains(search) || cat.description.Contains(search) || sup.commercial_name.Contains(search)
                                select new InternalRequirementModel
                                {
                                    part_number = i.part_number,
                                    description = i.description,
                                    category = cat.name,
                                    id_item_supplier = isup.supplier_part_number_creditors,
                                    supplier_name = sup.commercial_name,
                                    price = price2 != null ? price2.price : 0,
                                    estimate_price = price2 != null ? price2.price : 0,
                                };
            var r = itemCreditors.ToList();
            return r;

        }
        public List<InternalRequirementDetailPOModel> GetRequirementsAll()
        {
            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
	                INTERNAL_REQUIREMENT_DETAIL.supplier_id,
	                MA_SUPPLIER.commercial_name AS name_creditor,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity_total,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.total) AS total_total
	                FROM INTERNAL_REQUIREMENT_HEADER 
		                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
		                JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = INTERNAL_REQUIREMENT_DETAIL.supplier_id
	                WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0
	                GROUP BY INTERNAL_REQUIREMENT_DETAIL.supplier_id, MA_SUPPLIER.commercial_name;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GerRequirementsAllBySupplier()
        {
            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
	                INTERNAL_REQUIREMENT_DETAIL.supplier_id,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity_total,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.total) AS total_total
	                FROM INTERNAL_REQUIREMENT_HEADER 
		                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0
	                GROUP BY INTERNAL_REQUIREMENT_DETAIL.supplier_id").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplier(int supplier_id)
        {
            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
	                ITEM.part_description,
	                INTERNAL_REQUIREMENT_DETAIL.part_number,
	                SITES.site_name,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) as quantity
	                FROM INTERNAL_REQUIREMENT_HEADER 
		                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
		                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
		                JOIN ITEM ON ITEM.part_number = INTERNAL_REQUIREMENT_DETAIL.part_number
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_DETAIL.supplier_id =  @supplier_id
	                GROUP BY ITEM.part_description, SITES.site_name, INTERNAL_REQUIREMENT_DETAIL.part_number;",
                new SqlParameter("supplier_id", supplier_id))
                .ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
        public int AddInternalRequirementHeader(INTERNAL_REQUIREMENT_HEADER model)
        {
            _context.INTERNAL_REQUIREMENT_HEADER.Add(model);
            _context.SaveChanges();
            int id = model.folio;
            return id;
        }
        public bool AddInternalRequirementDetail(INTERNAL_REQUIREMENT_DETAIL model)
        {
            try
            {
                _context.INTERNAL_REQUIREMENT_DETAIL.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public int GetCountRequirementFolios()
        {
            return _context.INTERNAL_REQUIREMENT_HEADER.Where(x => x.requirement_status == 0).Count();
        }

        public decimal GetCountRequirementsQuantity()
        {
            return _context.INTERNAL_REQUIREMENT_DETAIL
                .Join(_context.INTERNAL_REQUIREMENT_HEADER, d => d.folio, h => h.folio, (d, h) => new { d, h })
                .Where(w => w.h.requirement_status == 0)
                .Sum(x => x.d.quantity);
        }
        public List<InternalRequirementPOModel> GetAllRequirementsHeaderBySite(string site)
        {
            var list = _context.INTERNAL_REQUIREMENT_HEADER
                .Where(x => x.site_code == site && x.requirement_type == "INTERNO")
                .Select(x => new InternalRequirementPOModel()
                {
                    folio = x.folio,
                    cdate2 = x.cdate.ToString(),
                    requirement_status = x.requirement_status
                }).ToList();
            return list;
        }

        public List<InternalRequirementDetailPOModel> GetAllRequirementsDetails(string folio)
        {
            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"select  ITEM_CREDITORS.part_number as part_number,ITEM_CREDITORS.description as part_description , ITEM_CREDITORS_CATEGORY.description as category_name , MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price, INTERNAL_REQUIREMENT_DETAIL.quantity as quantity from ITEM_CREDITORS
join ITEM_CREDITORS_CATEGORY on ITEM_CREDITORS.category = ITEM_CREDITORS_CATEGORY.category_id
join ITEM_SUPPLIER_CREDITORS on ITEM_SUPPLIER_CREDITORS.part_number_creditors = ITEM_CREDITORS.part_number
join MA_SUPPLIER on ITEM_SUPPLIER_CREDITORS.supplier_id = MA_SUPPLIER.supplier_id
JOIN INTERNAL_REQUIREMENT_DETAIL on ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
LEFT JOIN ITEM_CREDITORS_PRICE on ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
WHERE INTERNAL_REQUIREMENT_DETAIL.folio = @folio ;",
                new SqlParameter("folio", folio))
                .ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsInSiteAll()
        {
            return _context.INTERNAL_REQUIREMENT_HEADER.Where(w => w.requirement_status == 0)
                .Select(s => new InternalRequirementDetailPOModel
                {
                    site_name = s.site_code
                })
                .Distinct()
                .ToList();
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsInSupplierAll()
        {
            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"select distinct INTERNAL_REQUIREMENT_DETAIL.supplier_id from INTERNAL_REQUIREMENT_DETAIL
					JOIN INTERNAL_REQUIREMENT_HEADER ON INTERNAL_REQUIREMENT_HEADER.folio = INTERNAL_REQUIREMENT_DETAIL.folio
					where INTERNAL_REQUIREMENT_HEADER.requirement_status = 0;")
                .ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplierBySite(int supplier_id, string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT INTERNAL_REQUIREMENT_DETAIL.folio,
                        INTERNAL_REQUIREMENT_DETAIL.part_number,
                        ITEM.part_description,
                        INTERNAL_REQUIREMENT_DETAIL.quantity,
                        INTERNAL_REQUIREMENT_DETAIL.total
                        FROM INTERNAL_REQUIREMENT_DETAIL
                        JOIN INTERNAL_REQUIREMENT_HEADER ON INTERNAL_REQUIREMENT_HEADER.folio = INTERNAL_REQUIREMENT_DETAIL.folio
                        JOIN ITEM ON ITEM.part_number = INTERNAL_REQUIREMENT_DETAIL.part_number
                        WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_DETAIL.supplier_id = @supplier_id AND INTERNAL_REQUIREMENT_HEADER.site_code = @site_code;",
                new SqlParameter("supplier_id", supplier_id),
                new SqlParameter("site_code", site_code))
                .ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public InternalRequirementPOModel GetInfoRequirement(int folio)
        {
            return _context.INTERNAL_REQUIREMENT_HEADER.Where(w => w.folio == folio)
                .Select(s => new InternalRequirementPOModel
                {
                    cuser = s.cuser,
                    cdate = s.cdate,
                    uuser = s.uuser,
                    udate = s.udate
                }).SingleOrDefault();
        }


        public List<InternalRequirementPOModel> GetAllRequirementsHeaderAll()
        {
            var list = _context.INTERNAL_REQUIREMENT_HEADER
                .Where(x => x.requirement_status == 0)
                .Select(x => new InternalRequirementPOModel()
                {
                    folio = x.folio,
                    cdate2 = x.cdate.ToString(),
                    requirement_status = x.requirement_status
                }).ToList();
            return list;
        }








































        //Pasillas

        public List<InternalRequirementDetailPOModel> GetRequirementsAllSites(string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    if (BuyerDivision == "P999")
                    {
                        var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
		                ITEM_SUPPLIER_CREDITORS.supplier_id AS id_supplier,
		                MA_SUPPLIER.commercial_name AS supplier_name,
		                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity
	                    FROM INTERNAL_REQUIREMENT_HEADER 
	                    JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                    LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                    JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                    JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                    LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                    WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                    GROUP BY ITEM_SUPPLIER_CREDITORS.supplier_id, MA_SUPPLIER.commercial_name;").ToList();
                            return item;
                    }
                    else
                    {
                        var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
	                    ITEM_SUPPLIER_CREDITORS.supplier_id AS id_supplier,
	                    MA_SUPPLIER.commercial_name AS supplier_name,
	                    SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity
                        FROM INTERNAL_REQUIREMENT_HEADER 
                        JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
                        LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
                        JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
                        JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
                        JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
                        LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
                        WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
                        AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision
                        GROUP BY ITEM_SUPPLIER_CREDITORS.supplier_id, MA_SUPPLIER.commercial_name;",
                            new SqlParameter("BuyerDivision", BuyerDivision)).ToList();
                            return item;
                    }
                }
                else
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
		                ITEM_SUPPLIER_CREDITORS.supplier_id AS id_supplier,
		                MA_SUPPLIER.commercial_name AS supplier_name,
		                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity
	                    FROM INTERNAL_REQUIREMENT_HEADER 
	                    JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                    LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                    JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                    JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                    LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                    WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                    GROUP BY ITEM_SUPPLIER_CREDITORS.supplier_id, MA_SUPPLIER.commercial_name;").ToList();
                        return item;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public decimal GetSumTotalByStore(int supplier_id)
        {
            try
            {
                decimal total = 0;
                var items = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT INTERNAL_REQUIREMENT_DETAIL.folio, INTERNAL_REQUIREMENT_DETAIL.quantity, ITEM_CREDITORS_PRICE.price*INTERNAL_REQUIREMENT_DETAIL.quantity as total_total, INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors AS id_supplier FROM INTERNAL_REQUIREMENT_HEADER 
	            JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	            LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	            JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	            LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
				WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
				AND ITEM_SUPPLIER_CREDITORS.supplier_id =  @supplier_id;",
                new SqlParameter("supplier_id", supplier_id)).ToList();
                foreach (var item in items)
                {
                    total += item.total_total ?? 0;
                }
                return total;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public decimal GetSumTotalByFolio(int folio, string BuyerDivision)
        {
            try
            {
                decimal total = 0;
                if (BuyerDivision != null)
                {
                    var items = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT 
                    ISNULL(ITEM_CREDITORS_PRICE.price,0)*INTERNAL_REQUIREMENT_DETAIL.quantity AS total_total 
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code 
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio 
	                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors 
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors 
                    JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
	                JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id 
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors 
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1 
	                AND INTERNAL_REQUIREMENT_DETAIL.folio = @folio AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision;",
                    new SqlParameter("folio", folio),
                    new SqlParameter("BuyerDivision", BuyerDivision)).ToList();

                    foreach (var item in items)
                        total += item.total_total ?? 0;
                }
                else
                {
                    var items = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT 
                    ISNULL(ITEM_CREDITORS_PRICE.price,0)*INTERNAL_REQUIREMENT_DETAIL.quantity AS total_total 
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code 
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio 
	                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors 
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors 
	                JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id 
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors 
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1 
	                AND INTERNAL_REQUIREMENT_DETAIL.folio = @folio;",
                    new SqlParameter("folio", folio)).ToList();

                    foreach (var item in items)
                        total += item.total_total ?? 0;
                }

                return total;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementDetailsBySupplier(int supplier_id, string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"	SELECT 
		                SITES.site_name,
		                ITEM_CREDITORS.part_number AS part_number,
		                ITEM_CREDITORS.description AS part_description,
		                ITEM_CREDITORS.type AS category_name,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0) AS price,
		                INTERNAL_REQUIREMENT_DETAIL.quantity,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0)*INTERNAL_REQUIREMENT_DETAIL.quantity AS total, 
		                ITEM_CREDITORS_PRICE.flag_active
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
				    JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1
	                AND ITEM_CREDITORS_PRICE.flag_active = 1 AND ITEM_SUPPLIER_CREDITORS.supplier_id = @supplier_id AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision;",
                    new SqlParameter("supplier_id", supplier_id),
                    new SqlParameter("BuyerDivision", BuyerDivision))
                    .ToList();
                    return item;
                }
                else
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"	SELECT 
		                SITES.site_name,
		                ITEM_CREDITORS.part_number AS part_number,
		                ITEM_CREDITORS.description AS part_description,
		                ITEM_CREDITORS.type AS category_name,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0) AS price,
		                INTERNAL_REQUIREMENT_DETAIL.quantity,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0)*INTERNAL_REQUIREMENT_DETAIL.quantity AS total, 
		                ITEM_CREDITORS_PRICE.flag_active
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1
	                AND ITEM_CREDITORS_PRICE.flag_active = 1 AND ITEM_SUPPLIER_CREDITORS.supplier_id = @supplier_id;",
                    new SqlParameter("supplier_id", supplier_id))
                    .ToList();
                    return item;

                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public int GetRequirementcountFolios()
        {
            try
            {
                int item = _context.Database.SqlQuery<int>(@"
				SELECT COUNT(INTERNAL_REQUIREMENT_HEADER.folio) FROM INTERNAL_REQUIREMENT_HEADER WHERE requirement_status = 0 AND requirement_type = 'INTERNO';").SingleOrDefault();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }


        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsAll(string folio, string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT 
		                ITEM_CREDITORS.part_number AS part_number,
		                ITEM_CREDITORS.description AS part_description,
		                ITEM_CREDITORS_CATEGORY.name AS category_name,
		                MA_SUPPLIER.business_name AS supplier_name,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0) AS price,
		                INTERNAL_REQUIREMENT_DETAIL.quantity,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0)*INTERNAL_REQUIREMENT_DETAIL.quantity AS total
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
				    JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
	                JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision
				    AND INTERNAL_REQUIREMENT_DETAIL.folio = @folio;",
                    new SqlParameter("folio", folio),
                    new SqlParameter("BuyerDivision", BuyerDivision))
                    .ToList();
                    return item;
                }
                else
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT 
		                ITEM_CREDITORS.part_number AS part_number,
		                ITEM_CREDITORS.description AS part_description,
		                ITEM_CREDITORS_CATEGORY.name AS category_name,
		                MA_SUPPLIER.business_name AS supplier_name,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0) AS price,
		                INTERNAL_REQUIREMENT_DETAIL.quantity,
		                ISNULL(ITEM_CREDITORS_PRICE.price,0)*INTERNAL_REQUIREMENT_DETAIL.quantity AS total
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
				    JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
	                JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                AND INTERNAL_REQUIREMENT_DETAIL.folio = @folio;",
                    new SqlParameter("folio", folio))
                    .ToList();
                    return item;
                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsHeaderAll(string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"	SELECT DISTINCT
		                INTERNAL_REQUIREMENT_HEADER.folio,
		                SITES.site_name,
		                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
		            LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
                    LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision
                    GROUP BY INTERNAL_REQUIREMENT_HEADER.folio, SITES.site_name;",
                    new SqlParameter("BuyerDivision", BuyerDivision)).ToList();
                    return item;
                }
                else
                {
                    var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"	SELECT DISTINCT
		                INTERNAL_REQUIREMENT_HEADER.folio,
		                SITES.site_name,
		                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
		            LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                GROUP BY INTERNAL_REQUIREMENT_HEADER.folio, SITES.site_name;").ToList();
                    return item;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetSentRequirements(string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    if(BuyerDivision == "P999")
                    {
                        var orders = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT 
		                INTERNAL_REQUIREMENT_HEADER.folio, 
		                SITES.site_name, 
		                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity 
	                    FROM INTERNAL_REQUIREMENT_HEADER 
	                    JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code 
	                    JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio 
		                LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors 
	                    JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors 
	                    LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors 
	                    WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 1 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1 
                        AND CONVERT(date, INTERNAL_REQUIREMENT_HEADER.cdate) BETWEEN DATEADD(DD, -7, CAST(GETDATE() AS DATE)) AND CONVERT(DATE, GETDATE()) 
	                    GROUP BY INTERNAL_REQUIREMENT_HEADER.folio, SITES.site_name;").ToList();
                            return orders;
                    }
                    else
                    {
                        var orders = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
	                    INTERNAL_REQUIREMENT_HEADER.folio,
	                    SITES.site_name,
	                    SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity
                        FROM INTERNAL_REQUIREMENT_HEADER 
                        JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
                        JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
                        LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
                        JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
                        JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id = ITEM_CREDITORS.category
                        LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
                        WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
                        AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision
                        GROUP BY INTERNAL_REQUIREMENT_HEADER.folio, SITES.site_name;",
                            new SqlParameter("BuyerDivision", BuyerDivision)).ToList();
                            return orders;
                    }
                }
                else
                {
                    var orders = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT 
		            INTERNAL_REQUIREMENT_HEADER.folio, 
		            SITES.site_name, 
		            SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity 
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code 
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio 
		            LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors 
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors 
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors 
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 1 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1 
                    AND CONVERT(date, INTERNAL_REQUIREMENT_HEADER.cdate) BETWEEN DATEADD(DD, -7, CAST(GETDATE() AS DATE)) AND CONVERT(DATE, GETDATE()) 
	                GROUP BY INTERNAL_REQUIREMENT_HEADER.folio, SITES.site_name;").ToList();
                    return orders;
                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementModel> GetItemsCreditorsAll(string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    var item = _context.Database.SqlQuery<InternalRequirementModel>(@"select 
	                ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors as id_item_supplier,
	                ITEM_CREDITORS.part_number as part_number,
					ITEM_CREDITORS.type,
	                ITEM_CREDITORS.description as description,
	                ITEM_CREDITORS_CATEGORY.name as category,
	                MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price 
	                from ITEM_CREDITORS
                    join ITEM_CREDITORS_CATEGORY on ITEM_CREDITORS.category = ITEM_CREDITORS_CATEGORY.category_id
                    join ITEM_SUPPLIER_CREDITORS on ITEM_SUPPLIER_CREDITORS.part_number_creditors = ITEM_CREDITORS.part_number
                    join MA_SUPPLIER on ITEM_SUPPLIER_CREDITORS.supplier_id = MA_SUPPLIER.supplier_id
                    LEFT JOIN ITEM_CREDITORS_PRICE on ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
                    WHERE ITEM_CREDITORS.type = 'Producto' AND ITEM_CREDITORS.active_flag = 1 and ITEM_CREDITORS.requisition_flag = 1 and ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
                    AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision;",
                    new SqlParameter("BuyerDivision", BuyerDivision)).ToList();
                    return item;
                }
                else
                {
                    var item = _context.Database.SqlQuery<InternalRequirementModel>(@"select 
	                ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors as id_item_supplier,
	                ITEM_CREDITORS.part_number as part_number,
					ITEM_CREDITORS.type,
	                ITEM_CREDITORS.description as description,
	                ITEM_CREDITORS_CATEGORY.name as category,
	                MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price 
	                from ITEM_CREDITORS
                    join ITEM_CREDITORS_CATEGORY on ITEM_CREDITORS.category = ITEM_CREDITORS_CATEGORY.category_id
                    join ITEM_SUPPLIER_CREDITORS on ITEM_SUPPLIER_CREDITORS.part_number_creditors = ITEM_CREDITORS.part_number
                    join MA_SUPPLIER on ITEM_SUPPLIER_CREDITORS.supplier_id = MA_SUPPLIER.supplier_id
                    LEFT JOIN ITEM_CREDITORS_PRICE on ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
                    WHERE ITEM_CREDITORS.type = 'Producto' AND ITEM_CREDITORS.active_flag = 1 and ITEM_CREDITORS.requisition_flag = 1 and ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1;").ToList();
                    return item;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }


        public List<InternalRequirementModel> getItemsCreditorsByFolio(int folio, string BuyerDivision)
        {
            try
            {
                if (BuyerDivision != null)
                {
                    var item = _context.Database.SqlQuery<InternalRequirementModel>(@"	SELECT 
	                    ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors as id_item_supplier,
	                    ITEM_CREDITORS.part_number as part_number,
	                    ITEM_CREDITORS.description as description,
	                    ITEM_CREDITORS.type as type,
	                    ITEM_CREDITORS_CATEGORY.name as category,
	                    MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price,
	                    INTERNAL_REQUIREMENT_DETAIL.quantity
	                    FROM INTERNAL_REQUIREMENT_HEADER 
	                    JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                    JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                    LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                    JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                    JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id =  ITEM_CREDITORS.category
	                    JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                    LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                    WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                    AND ITEM_CREDITORS_CATEGORY.buyer_division = @BuyerDivision
	                    AND INTERNAL_REQUIREMENT_DETAIL.folio = @folio;",
                        new SqlParameter("folio", folio),
                        new SqlParameter("BuyerDivision", BuyerDivision)).ToList();
                    return item;
                }
                else
                {
                    var item = _context.Database.SqlQuery<InternalRequirementModel>(@"	SELECT 
	                    ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors as id_item_supplier,
	                    ITEM_CREDITORS.part_number as part_number,
	                    ITEM_CREDITORS.description as description,
	                    ITEM_CREDITORS.type as type,
	                    ITEM_CREDITORS_CATEGORY.name as category,
	                    MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price,
	                    INTERNAL_REQUIREMENT_DETAIL.quantity
	                    FROM INTERNAL_REQUIREMENT_HEADER 
	                    JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                    JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                    LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                    JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                    JOIN ITEM_CREDITORS_CATEGORY ON ITEM_CREDITORS_CATEGORY.category_id =  ITEM_CREDITORS.category
	                    JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                    LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                    WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1
	                    AND INTERNAL_REQUIREMENT_DETAIL.folio = @folio;",
                        new SqlParameter("folio", folio)).ToList();
                    return item;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public int UpdateItemsRequirement(InternalRequirementModel model, string uuser, int folio)
        {
            try
            {
                var item = _context.INTERNAL_REQUIREMENT_DETAIL.SingleOrDefault(i => i.folio == folio && i.supplier_part_number_creditors == model.id_item_supplier);
                if (item != null)
                {
                    item.quantity = model.quantity;
                    item.uuser = uuser;
                    item.udate = DateTime.Now;
                    item.program_id = "PURCH019.cshtml";
                    _context.SaveChanges();
                }
                else
                {
                    var newItem = new INTERNAL_REQUIREMENT_DETAIL()
                    {
                        folio = folio,
                        supplier_part_number_creditors = model.id_item_supplier,
                        quantity = model.quantity,
                        cuser = uuser,
                        cdate = DateTime.Now,
                        program_id = "PURCH19.cshtml"
                    };
                    _context.INTERNAL_REQUIREMENT_DETAIL.Add(newItem);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
            return 1;
        }

        public int DeleteInternalHeaderFolio(int folio, string uuser)
        {
            try
            {
                var item = _context.INTERNAL_REQUIREMENT_HEADER.SingleOrDefault(w => w.folio == folio);
                item.requirement_status = 8;
                item.uuser = uuser;
                item.udate = DateTime.Now;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsAllStatus0()
        {
            return _context.INTERNAL_REQUIREMENT_HEADER
                .Join(_context.SITES, s => s.site_code, ss => ss.site_code, (s, ss) => new { s, ss })
                .Where(w => w.s.requirement_status == 0)
                .Select(s => new InternalRequirementDetailPOModel
                {
                    site_code = s.s.site_code,
                    site_name = s.ss.site_name

                })
                .Distinct()
                .ToList();
        }



        public List<InternalRequirementDetailPOModel> GetRequirementsAllCreditors()
        {

            try
            {
                var item = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
                ITEM_SUPPLIER_CREDITORS.supplier_id AS id_supplier
            FROM INTERNAL_REQUIREMENT_HEADER
            JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
            LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
            JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
            WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO'
            GROUP BY ITEM_SUPPLIER_CREDITORS.supplier_id;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }

            //return _context.INTERNAL_REQUIREMENT_HEADER
            //    .Join(_context.INTERNAL_REQUIREMENT_DETAIL, d => d.folio, dd => dd.folio, (d, dd) => new { d, dd })
            //    .Join(_context.ITEM_SUPPLIER_CREDITORS, isc => isc.dd.supplier_part_number_creditors, iscc => iscc.supplier_part_number_creditors, (isc, iscc) => new { isc, iscc })
            //    .Join(_context.ITEM_CREDITORS, ic => ic.iscc.part_number_creditors, icc => icc.part_number, (ic, icc) => new { ic, icc })
            //    .Where(w => w.ic.isc.d.requirement_status == 0 && w.ic.isc.d.requirement_type == "INTERNO")
            //    .GroupBy(g => new { g.ic.iscc.supplier_id })
            //    .Distinct()
            //    .Select(s => new InternalRequirementDetailPOModel
            //    {
            //        id_supplier = s.Key.supplier_id ?? 0
            //    })
            //    .ToList();
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplierSite(int supplier_id, string site_code, string buyer_division)
        {
            if (buyer_division != null)
            {
                var x = _context.INTERNAL_REQUIREMENT_HEADER
                .Join(_context.INTERNAL_REQUIREMENT_DETAIL, d => d.folio, dd => dd.folio, (d, dd) => new { d, dd })
                .Join(_context.ITEM_SUPPLIER_CREDITORS, isc => isc.dd.supplier_part_number_creditors, iscc => iscc.supplier_part_number_creditors, (isc, iscc) => new { isc, iscc })
                .Join(_context.ITEM_CREDITORS, ic => ic.iscc.part_number_creditors, icc => icc.part_number, (ic, icc) => new { ic, icc })
                .Join(_context.ITEM_CREDITORS_PRICE, icp => icp.ic.iscc.supplier_part_number_creditors, icpp => icpp.supplier_part_number_creditors, (icp, icpp) => new { icp, icpp })
                .Where(w => w.icp.ic.isc.d.requirement_status == 0 && w.icp.ic.isc.d.requirement_type == "INTERNO" && w.icp.ic.iscc.supplier_id == supplier_id && w.icp.ic.isc.d.site_code == site_code && w.icp.ic.isc.d.buyer_division == buyer_division)
                .OrderBy(o => o.icp.ic.iscc.supplier_id)
                .ThenBy(o => o.icp.ic.isc.d.site_code)
                .Select(s => new InternalRequirementDetailPOModel
                {
                    folio = s.icp.ic.isc.dd.folio,
                    part_number = s.icp.icc.part_number,
                    part_description = s.icp.icc.description,
                    quantity = s.icp.ic.isc.dd.quantity,
                    price = s.icpp.price,
                    total = s.icpp.price * s.icp.ic.isc.dd.quantity
                })
                .ToList();
                return x;
            }
            else
            {
                var x = _context.INTERNAL_REQUIREMENT_HEADER
                .Join(_context.INTERNAL_REQUIREMENT_DETAIL, d => d.folio, dd => dd.folio, (d, dd) => new { d, dd })
                .Join(_context.ITEM_SUPPLIER_CREDITORS, isc => isc.dd.supplier_part_number_creditors, iscc => iscc.supplier_part_number_creditors, (isc, iscc) => new { isc, iscc })
                .Join(_context.ITEM_CREDITORS, ic => ic.iscc.part_number_creditors, icc => icc.part_number, (ic, icc) => new { ic, icc })
                .Join(_context.ITEM_CREDITORS_PRICE, icp => icp.ic.iscc.supplier_part_number_creditors, icpp => icpp.supplier_part_number_creditors, (icp, icpp) => new { icp, icpp })
                .Where(w => w.icp.ic.isc.d.requirement_status == 0 && w.icp.ic.isc.d.requirement_type == "INTERNO" && w.icp.ic.iscc.supplier_id == supplier_id && w.icp.ic.isc.d.site_code == site_code)
                .OrderBy(o => o.icp.ic.iscc.supplier_id)
                .ThenBy(o => o.icp.ic.isc.d.site_code)
                .Select(s => new InternalRequirementDetailPOModel
                {
                    folio = s.icp.ic.isc.dd.folio,
                    part_number = s.icp.icc.part_number,
                    part_description = s.icp.icc.description,
                    quantity = s.icp.ic.isc.dd.quantity,
                    price = s.icpp.price,
                    total = s.icpp.price * s.icp.ic.isc.dd.quantity
                })
                .ToList();
                return x;
            }

            //      SELECT INTERNAL_REQUIREMENT_DETAIL.folio,
            //                  ITEM_CREDITORS.part_number,
            //                  ITEM_CREDITORS.description,
            //                  INTERNAL_REQUIREMENT_DETAIL.quantity,
            //ITEM_CREDITORS_PRICE.price,
            //ITEM_CREDITORS_PRICE.price* INTERNAL_REQUIREMENT_DETAIL.quantity as total
            //          FROM INTERNAL_REQUIREMENT_HEADER
            //          JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
            //          JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
            //          JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
            //          JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
            //          WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND INTERNAL_REQUIREMENT_HEADER.site_code = '0009' /*AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1*/
            //          AND ITEM_SUPPLIER_CREDITORS.supplier_id = 5541
        }


        public int SendInternalHeaderFolio(int folio, string uuser)
        {
            try
            {
                var item = _context.INTERNAL_REQUIREMENT_HEADER.SingleOrDefault(w => w.folio == folio);
                item.requirement_status = 1;
                item.uuser = uuser;
                item.udate = DateTime.Now;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }


        public List<AllItemSitesSupplier> GetRequirementsRelationItems()
        {

            try
            {
                var item = _context.Database.SqlQuery<AllItemSitesSupplier>(@"SELECT INTERNAL_REQUIREMENT_DETAIL.folio,
	                    ITEM_CREDITORS.part_number,
	                    ITEM_CREDITORS.description,
	                    INTERNAL_REQUIREMENT_DETAIL.quantity,
	                    ITEM_CREDITORS_PRICE.price,
	                    (ITEM_CREDITORS_PRICE.price * INTERNAL_REQUIREMENT_DETAIL.quantity) * (MA_TAX.tax_value/100) as iva,
	                    (ITEM_CREDITORS_PRICE.price * INTERNAL_REQUIREMENT_DETAIL.quantity) as import,
	                    (ITEM_CREDITORS_PRICE.price * INTERNAL_REQUIREMENT_DETAIL.quantity) + (ITEM_CREDITORS_PRICE.price * INTERNAL_REQUIREMENT_DETAIL.quantity) * (MA_TAX.tax_value/100) as total,
	                    ITEM_SUPPLIER_CREDITORS.supplier_id,
	                    SITES.site_code,
	                    SITES.site_name,
	                    SITES.address,
	                    SITES.city,
	                    SITES.state,
	                    SITES.email_manager,
	                    SITES.phone,
	                    SITES.postal_code as potal_code
	                    FROM INTERNAL_REQUIREMENT_HEADER
	                    JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
	                    JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
	                    JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors
	                    JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
	                    JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code
	                    JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = ITEM_SUPPLIER_CREDITORS.supplier_id
	                    JOIN MA_TAX ON MA_TAX.tax_code = MA_SUPPLIER.part_iva_purchase_supplier
	                    WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS_PRICE.flag_active = 1 /*AND INTERNAL_REQUIREMENT_HEADER.site_code = '0009'*/ /*AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1*/
	                    ORDER BY ITEM_SUPPLIER_CREDITORS.supplier_id, SITES.site_code;
                    ").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetSentRequirementsByDates(DateTime BeginDate, DateTime EndDate)
        {
            try
            {
                var orders = _context.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT 
		            INTERNAL_REQUIREMENT_HEADER.folio, 
		            SITES.site_name, 
		            SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity 
	                FROM INTERNAL_REQUIREMENT_HEADER 
	                JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code 
	                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio 
		            LEFT JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors 
	                JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors 
	                LEFT JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors 
	                WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 1 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1 
                    AND CONVERT(date, INTERNAL_REQUIREMENT_HEADER.cdate) BETWEEN @BeginDate AND @EndDate
	                GROUP BY INTERNAL_REQUIREMENT_HEADER.folio, SITES.site_name;", new SqlParameter("BeginDate", BeginDate), new SqlParameter("EndDate", EndDate)).ToList();

                return orders;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        //SELECT INTERNAL_REQUIREMENT_DETAIL.folio,
        //   ITEM_CREDITORS.part_number,
        //   ITEM_CREDITORS.description,
        //   INTERNAL_REQUIREMENT_DETAIL.quantity,
        //   ITEM_CREDITORS_PRICE.price,
        //   ITEM_CREDITORS_PRICE.price* INTERNAL_REQUIREMENT_DETAIL.quantity as total,
        //   ITEM_SUPPLIER_CREDITORS.supplier_id,
        //   SITES.site_code,
        //   SITES.site_name,
        //   SITES.address,
        //   SITES.city,
        //   SITES.state,
        //   SITES.email_manager,
        //   SITES.phone,
        //   SITES.postal_code
        //   FROM INTERNAL_REQUIREMENT_HEADER
        //   JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio

        //   JOIN ITEM_SUPPLIER_CREDITORS ON ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors

        //   JOIN ITEM_CREDITORS ON ITEM_CREDITORS.part_number = ITEM_SUPPLIER_CREDITORS.part_number_creditors

        //   JOIN ITEM_CREDITORS_PRICE ON ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors

        //   JOIN SITES ON SITES.site_code = INTERNAL_REQUIREMENT_HEADER.site_code

        //   WHERE INTERNAL_REQUIREMENT_DETAIL.quantity != 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_HEADER.requirement_type = 'INTERNO' /*AND INTERNAL_REQUIREMENT_HEADER.site_code = '0009'*/ /*AND ITEM_CREDITORS.active_flag = 1 AND ITEM_CREDITORS.requisition_flag = 1 AND ITEM_SUPPLIER_CREDITORS.flag_active = 1 AND ITEM_CREDITORS_PRICE.flag_active = 1*/

        //   ORDER BY ITEM_SUPPLIER_CREDITORS.supplier_id, SITES.site_code;

    }
}
