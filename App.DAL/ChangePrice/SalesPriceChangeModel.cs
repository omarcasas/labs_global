﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.ChangePrice
{
    public class SalesPriceChangeModel
    {
        public string partNumber { get; set; }
        public string part_description { get; set; }

        public DateTime appliedDate { get; set; }

        public decimal margin { get; set; }

        public decimal priceUser { get; set; }

        public bool priceChangeNow { get; set; }
    }
}
