﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.ChangePrice
{
    public class SalesPriceChangeRepository
    {
        private PRTSAIEntities _context;
        

        public SalesPriceChangeRepository()
        {
            _context = new PRTSAIEntities();
        }

        public int addItemSalesPriceChange(string partNumber, DateTime appliedDate, decimal margin, decimal salePrice, string Sesion, string ProgramId, StoreProcedureIvaIeps Tax)
        {
            try
            {
                var Product = _context.SALES_PRICE_CHANGE.Where(x => x.part_number == partNumber && x.status == 1).ToList();
                decimal WithoutIva = salePrice;
                foreach (var product in Product)
                {
                    product.status = 0;
                    product.uuser = Sesion;
                    product.udate = DateTime.Now;
                    product.program_id = ProgramId;
                }

                if (Tax.Iva != 0)
                    WithoutIva = salePrice / (1 + (Tax.Iva / 100));

                var ItemChangePrice = new SALES_PRICE_CHANGE()
                {
                    part_number = partNumber,
                    applied_date = appliedDate,
                    margin = margin,
                    sale_price = WithoutIva,
                    sale_price_iva = salePrice,
                    status = 1,
                    cuser = Sesion,
                    cdate = DateTime.Now,
                    program_id = ProgramId
                };
                _context.SALES_PRICE_CHANGE.Add(ItemChangePrice);
                var save = _context.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }
        public SalesPriceChangeModel GetSalePriceByPartNumber(string PartNumber)
        {
            var price = _context.SALES_PRICE_CHANGE.Where(x => x.part_number == PartNumber && x.status == 1).Select(x => new SalesPriceChangeModel
            {
                appliedDate = x.applied_date,
                margin  = x.margin,
                priceUser = x.sale_price_iva ?? 0
            }).FirstOrDefault();

            if (price == null)
                return price;

            if (price.appliedDate <= DateTime.Now)
                price.appliedDate = DateTime.Now;
            return price;


        }
    }
}
