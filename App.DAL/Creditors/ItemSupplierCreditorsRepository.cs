﻿using App.DAL.Supplier;
using App.Entities;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Creditors
{
    public class ItemSupplierCreditorsRepository
    {
        private PRTSAIEntities _context;
        public ItemSupplierCreditorsRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ItemSupplierCreditorsModel> GetAllSupplierCreditors(string BuyerDivision)
        {
            if (BuyerDivision == null)
                return new List<ItemSupplierCreditorsModel>();

            if (BuyerDivision != "P999")
            {
                return _context.Database.SqlQuery<ItemSupplierCreditorsModel>(@"    select i.active_flag as ActiveFlag, ic.name as CategoryName ,isnull(icp.currency,'0') as Currency
                            ,ic.category_id as CategoryId, i.description as Description, 
                            ist.supplier_part_number_creditors as Id, 
                            ist.part_number_creditors as PartNumber,ISNULL(icp.price,0) as Price, 
                            i.requisition_flag as RequisitionFlag,ist.supplier_id as SupplierId, ms.commercial_name as SupplierName, i.type as Type from ITEM_SUPPLIER_CREDITORS ist 
                            inner join ITEM_CREDITORS i on i.part_number = ist.part_number_creditors 
                            inner join ITEM_CREDITORS_CATEGORY ic on ic.category_id = i.category
                            inner join MA_SUPPLIER ms  on ms.supplier_id = ist.supplier_id
                            left join (select * from ITEM_CREDITORS_PRICE where flag_active = 1 )icp on icp.supplier_part_number_creditors = ist.supplier_part_number_creditors
                            where ist.flag_active = 1 and ic.buyer_division = @BuyerDivision",
                new SqlParameter("BuyerDivision", BuyerDivision)).ToList();
            }
            else
            {
                return _context.Database.SqlQuery<ItemSupplierCreditorsModel>(@"    select i.active_flag as ActiveFlag, ic.name as CategoryName ,isnull(icp.currency,'0') as Currency
                            ,ic.category_id as CategoryId, i.description as Description, 
                            ist.supplier_part_number_creditors as Id, 
                            ist.part_number_creditors as PartNumber,ISNULL(icp.price,0) as Price, 
                            i.requisition_flag as RequisitionFlag,ist.supplier_id as SupplierId, ms.commercial_name as SupplierName, i.type as Type from ITEM_SUPPLIER_CREDITORS ist 
                            inner join ITEM_CREDITORS i on i.part_number = ist.part_number_creditors 
                            inner join ITEM_CREDITORS_CATEGORY ic on ic.category_id = i.category
                            inner join MA_SUPPLIER ms  on ms.supplier_id = ist.supplier_id
                            left join (select * from ITEM_CREDITORS_PRICE where flag_active = 1 )icp on icp.supplier_part_number_creditors = ist.supplier_part_number_creditors
                            where ist.flag_active = 1").ToList();
            }
        }

        public bool RemoveCategory(int Supplier,List<int> Category, string User)
        {
            try
            {
                var Creditor = _context.ITEM_SUPPLIER_CREDITORS
                        .Join(_context.ITEM_CREDITORS, category => category.part_number_creditors, Item => Item.part_number, (Item, category) => new { Item, category })
                        .Where(x => !Category.Contains(x.category.category) && x.Item.supplier_id == Supplier).ToList();
                Creditor.ForEach(x =>
                {
                    x.Item.flag_active = false;
                    x.Item.uuser = User;
                    x.Item.udate = DateTime.Now;
                    x.Item.program_id = "PURCH018.cshtml";
                });
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool DisabaleItemCreditorActive(ItemCreditorPriceModel ItemPrice, string User)
        {
            try
            {
                var ItemCreditor = _context.ITEM_SUPPLIER_CREDITORS.Where(w => w.supplier_part_number_creditors == ItemPrice.SupplierPartNumberCreditors).FirstOrDefault();
                ItemCreditor.flag_active = ItemPrice.ActiveFlag;
                ItemCreditor.udate = DateTime.Now;
                ItemCreditor.uuser = User;
                ItemCreditor.program_id = "PURCH018.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }
        public bool SaveCategoryCreditors(int Supplier, List<int> Category,string User)
        {
            try
            {
                var products = _context.ITEM_CREDITORS.Where(x => Category.Contains(x.category)).ToList();
                products.ForEach(x =>
                {
                    var ItemSupplier = new ITEM_SUPPLIER_CREDITORS();
                    ItemSupplier.supplier_id = Supplier;
                    ItemSupplier.part_number_creditors = x.part_number;
                    ItemSupplier.cuser = User;
                    ItemSupplier.flag_active = true;
                    ItemSupplier.cdate = DateTime.Now;
                    ItemSupplier.program_id = "PURCH018.cshtml";
                    _context.ITEM_SUPPLIER_CREDITORS.Add(ItemSupplier);
                    _context.SaveChanges();
                    _context.ITEM_CREDITORS_PRICE.Add(new ITEM_CREDITORS_PRICE() { flag_active = true, cuser = User, cdate = DateTime.Now, price = 0, supplier_part_number_creditors = ItemSupplier.supplier_part_number_creditors, program_id = "PURCH018.cshtml" });
                    _context.SaveChanges();
                });
                //_context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<int> CategorysCreditors(int Supplier)
        {
            return _context.ITEM_SUPPLIER_CREDITORS
                         .Join(_context.ITEM_CREDITORS, category => category.part_number_creditors, Item => Item.part_number, (Item, category) => new { Item, category })
                         .Where(x => x.Item.supplier_id == Supplier && x.Item.flag_active == true).Select(x => x.category.category).Distinct().ToList();
        }

        public bool ExisCreditors(int Supplier, int Category,string User)
        {
            try
            {
                var Creditors = _context.ITEM_SUPPLIER_CREDITORS
                        .Join(_context.ITEM_CREDITORS, category => category.part_number_creditors, Item => Item.part_number, 
                        (Item, category) => new { Item, category })
                        .Where(x => Category == x.category.category && x.Item.supplier_id == Supplier).ToList();
                if (Creditors.Count() > 0)
                {
                    var items = Creditors.Select(x => x.Item.part_number_creditors).Distinct().ToList();
                    var newItems = _context.ITEM_CREDITORS.Where(x => !items.Contains(x.part_number) && x.category == Category);
                    foreach (var item in newItems)
                    {
                        var ItemSupplierCreditors = new ITEM_SUPPLIER_CREDITORS();
                        ItemSupplierCreditors.program_id = "PURCH018.cshtml";
                        ItemSupplierCreditors.cuser = User;
                        ItemSupplierCreditors.cdate = DateTime.Now;
                        ItemSupplierCreditors.part_number_creditors = item.part_number;
                        ItemSupplierCreditors.supplier_id = Supplier;
                        ItemSupplierCreditors.flag_active = true;
                        _context.ITEM_SUPPLIER_CREDITORS.Add(ItemSupplierCreditors);

                    }
                    Creditors.ForEach(x => { x.Item.flag_active = true; });
                    _context.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {

                return true;
            }
        }

        public ItemSupplierCreditorsModel SaveSupplierCreditor(ItemSupplierCreditorsModel ItemSupplier, string User)
        {
            try
            {
                var ItemSupplierCreditors = new ITEM_SUPPLIER_CREDITORS();
                ItemSupplierCreditors.program_id = "PURCH018.cshtml";
                ItemSupplierCreditors.cuser = User;
                ItemSupplierCreditors.cdate = DateTime.Now;
                ItemSupplierCreditors.part_number_creditors = ItemSupplier.PartNumber;
                ItemSupplierCreditors.supplier_id = ItemSupplier.SupplierId;
                ItemSupplierCreditors.flag_active = true;
                _context.ITEM_SUPPLIER_CREDITORS.Add(ItemSupplierCreditors);
                _context.SaveChanges();
                ItemSupplier.Id = ItemSupplierCreditors.supplier_part_number_creditors;
                return ItemSupplier;
            }
            catch (Exception)
            {

                return ItemSupplier;
            }

        }

        public bool EditSupplierCreditor(ItemSupplierCreditorsModel ItemSupplier, string User)
        {
            try
            {
                var ItemSupplierCreditor = _context.ITEM_SUPPLIER_CREDITORS.Where(x => x.supplier_id == ItemSupplier.Id).SingleOrDefault();
                //ItemSupplierCreditor.supplier_id = ItemSupplier.SupplierId;
                //ItemSupplierCreditor.part_number_creditors = ItemSupplierCreditor.part_number_creditors;
                ItemSupplierCreditor.program_id = "PURCH018.cshtml";
                ItemSupplierCreditor.uuser = User;
                ItemSupplierCreditor.flag_active = false;
                ItemSupplierCreditor.udate = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<MaSupplierModel> GetAllSuppliersInModel()
        {
            return _context.MA_SUPPLIER
                .Where(x => x.supplier_type == "Acreedor")
                .Select(x => new MaSupplierModel
                {
                    SupplierId = x.supplier_id,
                    CommercialName = x.commercial_name,
                    BusinessName = x.business_name,
                }).ToList();
        }

    }
}
