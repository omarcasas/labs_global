﻿using App.Entities;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Creditors
{
    public class ItemCreditosRepository
    {
        private PRTSAIEntities _context;
        public ItemCreditosRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ItemCreditorsModel> GetAllCreditorsItems(string BuyerDivision)
        {
            if (BuyerDivision == null)
                return new List<ItemCreditorsModel>();

            if (BuyerDivision != "P999")
            {
                return _context.ITEM_CREDITORS
                .Join(_context.ITEM_CREDITORS_CATEGORY, category => category.category, Item => Item.category_id, (Item, category) => new { Item, category })
                .Where(w => w.category.buyer_division == BuyerDivision)
                .Select(x => new ItemCreditorsModel
                {
                    PartNumber = x.Item.part_number,
                    Description = x.Item.description,
                    CategoryDiscription = x.category.description,
                    CategoryId = x.category.category_id,
                    CategoryName = x.category.name,
                    ActiveFlag = x.Item.active_flag != null ? x.Item.active_flag.Value : false,
                    RequisitionFlag = x.Item.requisition_flag != null ? x.Item.requisition_flag.Value : false,
                    Type = x.Item.type
                }).ToList();
            }
            else
            {
                return _context.ITEM_CREDITORS
                .Join(_context.ITEM_CREDITORS_CATEGORY, category => category.category, Item => Item.category_id, (Item, category) => new { Item, category })
                .Select(x => new ItemCreditorsModel
                {
                    PartNumber = x.Item.part_number,
                    Description = x.Item.description,
                    CategoryDiscription = x.category.description,
                    CategoryId = x.category.category_id,
                    CategoryName = x.category.name,
                    ActiveFlag = x.Item.active_flag != null ? x.Item.active_flag.Value : false,
                    RequisitionFlag = x.Item.requisition_flag != null ? x.Item.requisition_flag.Value : false,
                    Type = x.Item.type
                }).ToList();
            }
        }

        public ItemCreditorsModel SaveItem(ItemCreditorsModel Item, string User)
        {
            try
            {
                var CreditorsItem = new ITEM_CREDITORS();
                CreditorsItem.cuser = User;
                CreditorsItem.cdate = DateTime.Now;
                CreditorsItem.description = Item.Description;
                CreditorsItem.part_number = Item.PartNumber;
                CreditorsItem.category = Item.CategoryId;
                CreditorsItem.active_flag = Item.ActiveFlag;
                CreditorsItem.requisition_flag = Item.RequisitionFlag;
                CreditorsItem.type = Item.Type;
                CreditorsItem.program_id = "PURCH018.cshtml";
                _context.ITEM_CREDITORS.Add(CreditorsItem);
                _context.SaveChanges();
                return Item;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return Item;
            }
        }

        public bool SaveItemNewSuppliersCategories(ItemCreditorsModel Item, string User)
        {
            try
            {
                var ItemByCategory = _context.ITEM_CREDITORS.Where(w => w.active_flag == true && w.category == Item.CategoryId && w.part_number != Item.PartNumber).FirstOrDefault();

                if (ItemByCategory != null)
                {
                    var Suppliers = _context.ITEM_SUPPLIER_CREDITORS.Where(w => w.flag_active == true && w.part_number_creditors == ItemByCategory.part_number).ToList();

                    if (Suppliers.Count() > 0)
                    {
                        List<ITEM_SUPPLIER_CREDITORS> ItemCreditorsList = new List<ITEM_SUPPLIER_CREDITORS>();
                        foreach (var supplier in Suppliers)
                        {
                            try
                            {
                                var ItemSupplierCreditors = new ITEM_SUPPLIER_CREDITORS();
                                ItemSupplierCreditors.program_id = "PURCH018.cshtml";
                                ItemSupplierCreditors.cuser = User;
                                ItemSupplierCreditors.cdate = DateTime.Now;
                                ItemSupplierCreditors.part_number_creditors = Item.PartNumber;
                                ItemSupplierCreditors.supplier_id = supplier.supplier_id;
                                ItemSupplierCreditors.flag_active = true;
                                _context.ITEM_SUPPLIER_CREDITORS.Add(ItemSupplierCreditors);
                                _context.SaveChanges();
                                ItemCreditorsList.Add(ItemSupplierCreditors);
                            }
                            catch (Exception e)
                            {
                                var msg = e.Message;
                                return false;
                            }
                        }

                        foreach (var itemPrice in ItemCreditorsList)
                        {
                            try
                            {
                                var ItemCreditors = new ITEM_CREDITORS_PRICE();
                                ItemCreditors.cuser = User;
                                ItemCreditors.cdate = DateTime.Now;
                                ItemCreditors.flag_active = true;
                                ItemCreditors.supplier_part_number_creditors = itemPrice.supplier_part_number_creditors;
                                ItemCreditors.price = 0;
                                _context.ITEM_CREDITORS_PRICE.Add(ItemCreditors);
                                _context.SaveChanges();
                            } catch (Exception e)
                            {
                                var msg = e.Message;
                                return false;
                            }
                        }
                        return true; //Agregados correctamente
                    }
                    else
                        return true; //Que el producto no este relacionado con ningun proveedor aqun
                }
                else
                    return true; //Podria pasar que no existe ningun producto en la categoria

            } catch(Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public bool EditItem(ItemCreditorsModel Item, string User)
        {
            bool changeItemCategory = false;
            try
            {
                var CreditorsItem = _context.ITEM_CREDITORS.Where(x => x.part_number == Item.PartNumber).SingleOrDefault();
                if (Item.CategoryId != CreditorsItem.category)
                    changeItemCategory = true;
                CreditorsItem.uuser = User;
                CreditorsItem.udate = DateTime.Now;
                CreditorsItem.description = Item.Description;
                CreditorsItem.part_number = Item.PartNumber;
                CreditorsItem.category = Item.CategoryId;
                CreditorsItem.active_flag = Item.ActiveFlag;
                CreditorsItem.requisition_flag = Item.RequisitionFlag;
                CreditorsItem.type = Item.Type;
                CreditorsItem.program_id = "PURCH018.cshtml";
                _context.SaveChanges();
                //Si pertenece a una nueva categoria
                if (changeItemCategory)
                {
                    //Desactivar
                    var itemCreditorSuppliers = _context.ITEM_SUPPLIER_CREDITORS.Where(w => w.part_number_creditors == Item.PartNumber && w.flag_active == true).ToList();
                    if (itemCreditorSuppliers.Count() > 0)
                    {
                        foreach (var itemCreditors in itemCreditorSuppliers)
                        {
                            itemCreditors.flag_active = false;
                            itemCreditors.uuser = User;
                            itemCreditors.udate = DateTime.Now;
                            itemCreditors.program_id = "PURCH018.cshtml";
                            _context.SaveChanges();
                        }
                    }

                    //Reactivar/Activar
                    //Código aleatorio
                    //Busca los productos de esa categoria: conocer los proveedores
                    var ItemByCategory = _context.ITEM_CREDITORS.Where(w => w.active_flag == true && w.category == Item.CategoryId && w.part_number != Item.PartNumber).FirstOrDefault();
                    if (ItemByCategory != null)
                    {
                        //Los proveedores
                        var Suppliers = _context.ITEM_SUPPLIER_CREDITORS.Where(w => w.flag_active == true && w.part_number_creditors == ItemByCategory.part_number).ToList();
                        if (Suppliers.Count() > 0)
                        {
                            List<ITEM_SUPPLIER_CREDITORS> ItemCreditorsList = new List<ITEM_SUPPLIER_CREDITORS>();
                            foreach (var supplier in Suppliers)
                            {
                                //Busca primero si existe relación
                                var relationExits = _context.ITEM_SUPPLIER_CREDITORS.Where(w => w.supplier_id == supplier.supplier_id && w.part_number_creditors == Item.PartNumber).FirstOrDefault();
                                //Si no existe crea registro y si existe actualiza
                                if (relationExits == null)
                                {
                                    try
                                    {
                                        var ItemSupplierCreditors = new ITEM_SUPPLIER_CREDITORS();
                                        ItemSupplierCreditors.program_id = "PURCH018.cshtml";
                                        ItemSupplierCreditors.cuser = User;
                                        ItemSupplierCreditors.cdate = DateTime.Now;
                                        ItemSupplierCreditors.part_number_creditors = Item.PartNumber;
                                        ItemSupplierCreditors.supplier_id = supplier.supplier_id;
                                        ItemSupplierCreditors.flag_active = true;
                                        _context.ITEM_SUPPLIER_CREDITORS.Add(ItemSupplierCreditors);
                                        _context.SaveChanges();
                                        ItemCreditorsList.Add(ItemSupplierCreditors);
                                    }
                                    catch (Exception e)
                                    {
                                        var msg = e.Message;
                                        return false;
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        relationExits.flag_active = true;
                                        relationExits.program_id = "PURCH018.cshtml";
                                        relationExits.uuser = User;
                                        relationExits.udate = DateTime.Now;
                                        _context.SaveChanges();
                                    }
                                    catch (Exception e)
                                    {
                                        var msg = e.Message;
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                        return false;
                    }
                    else
                        return true;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<TypeCreditorsModel> GetAllTypeCreditorsList()
        {
            return _context.MA_CODE.Where(x => x.code == "TYPE_CREDITOR").Select(x => new TypeCreditorsModel
            {
                Id = x.vkey,
                Name = x.description
            }).ToList();
        }

        public bool AddTypeCreditorsList(TypeCreditorsModel Type)
        {
            try
            {
                var Macode = new MA_CODE();
                Macode.code = "TYPE_CREDITOR";
                Macode.vkey = Type.Id;
                Macode.description = Type.Name;
                _context.MA_CODE.Add(Macode);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
