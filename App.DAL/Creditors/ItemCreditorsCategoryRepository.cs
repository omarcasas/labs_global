﻿using App.Entities;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Creditors
{
    public class ItemCreditorsCategoryRepository
    {
        private PRTSAIEntities _context;
        public ItemCreditorsCategoryRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<ItemCreditorsCategoryModel> GetAllCreditorsCategorys(string BuyerDivision)
        {
            if (BuyerDivision == null)
                return new List<ItemCreditorsCategoryModel>();

            if (BuyerDivision != "P999")
            {
                return _context.ITEM_CREDITORS_CATEGORY
                .Join(_context.MA_CODE, icc => icc.buyer_division, mc => mc.vkey, (icc, mc) => new { icc, mc })
                .Where(w => w.icc.buyer_division == BuyerDivision)
                .Select(x => new ItemCreditorsCategoryModel
                {
                    Id = x.icc.category_id,
                    Description = x.icc.description,
                    Name = x.icc.name,
                    BuyerDivision = x.mc.description,
                    BuyerDivisionCode = x.icc.buyer_division
                }).ToList();
            }
            else
            {
                return _context.ITEM_CREDITORS_CATEGORY
                .Join(_context.MA_CODE, icc => icc.buyer_division, mc => mc.vkey, (icc, mc) => new { icc, mc })
                .Select(x => new ItemCreditorsCategoryModel
                {
                    Id = x.icc.category_id,
                    Description = x.icc.description,
                    Name = x.icc.name,
                    BuyerDivision = x.mc.description,
                    BuyerDivisionCode = x.icc.buyer_division
                }).ToList();
            }
        }

        public ItemCreditorsCategoryModel SaveCategory(ItemCreditorsCategoryModel Category,string User)
        {
            try
            {
                var CreditorsCategory = new ITEM_CREDITORS_CATEGORY();
                CreditorsCategory.cuser = User;
                CreditorsCategory.cdate = DateTime.Now;
                CreditorsCategory.description = Category.Description;
                CreditorsCategory.buyer_division = Category.BuyerDivisionCode;
                CreditorsCategory.name = Category.Name;
                CreditorsCategory.program_id = "PURH018.cshtml";
                _context.ITEM_CREDITORS_CATEGORY.Add(CreditorsCategory);
                _context.SaveChanges();
                Category.Id = CreditorsCategory.category_id;
                return Category;
            }
            catch (Exception)
            {

                return Category;
            }
        }

        public bool EditCategory(ItemCreditorsCategoryModel Category, string User)
        {
            try
            {
                var CreditorsCategory = _context.ITEM_CREDITORS_CATEGORY.Where(x => x.category_id == Category.Id).SingleOrDefault();
                CreditorsCategory.uuser = User;
                CreditorsCategory.udate = DateTime.Now;
                CreditorsCategory.description = Category.Description;
                CreditorsCategory.buyer_division = Category.BuyerDivisionCode;
                CreditorsCategory.name = Category.Name;
                CreditorsCategory.program_id = "PURH018.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
