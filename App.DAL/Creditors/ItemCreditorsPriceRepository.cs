﻿using App.Entities;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Creditors
{
    public class ItemCreditorsPriceRepository
    {
        private PRTSAIEntities _context;
        public ItemCreditorsPriceRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool DisableItemCreditors(int SupplierPartNumer, string User)
        {
            try
            {
                var ItemCreditorsPrice = _context.ITEM_CREDITORS_PRICE.Where(x => x.supplier_part_number_creditors == SupplierPartNumer && x.flag_active).ToList();
                ItemCreditorsPrice.ForEach(x => { x.flag_active = false;x.uuser = User;x.udate = DateTime.Now;x.program_id = "Purch018.cshtml";  });
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SaveItemCreditors(ItemCreditorPriceModel ItemPrice, string User)
        {
            try
            {
                var ItemCreditors = new ITEM_CREDITORS_PRICE();
                ItemCreditors.cuser = User;
                ItemCreditors.cdate = DateTime.Now;
                ItemCreditors.flag_active = true;
                ItemCreditors.supplier_part_number_creditors = ItemPrice.SupplierPartNumberCreditors;
                ItemCreditors.price = ItemPrice.Price;
                ItemCreditors.currency = ItemPrice.Currency;
                _context.ITEM_CREDITORS_PRICE.Add(ItemCreditors);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
