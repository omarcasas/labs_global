﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities;
using App.Entities.ViewModels.PRT;
using App.Entities.ViewModels.Site;
using System.Data.SqlClient;

namespace App.DAL.PRT
{
    public class DeliveryOrderRepository
    {
        private MES_PRTEntities _contextPRT;
        private CDSERPEntities _contextCDSERP;
        private PRTSAIEntities _contextDFLSAI;
        public DeliveryOrderRepository()
        {
            _contextPRT = new MES_PRTEntities();
            _contextCDSERP = new CDSERPEntities();
            _contextDFLSAI = new PRTSAIEntities();
        }
        public List<ReportHeader> GetTotalsTransfersPRTByDate(DateTime dateInitial, DateTime dateFinish)
        {
            var list = _contextPRT.Database.SqlQuery<ReportHeader>(@"select s.site_code site_code , s.site_name site_name , s.ip_address,
            SUM(di.amount) totalPRT from DELIVERY_ORDER do 
            join DELIVERY_ORDER_ITEM di on di.do_no = do.do_no
            join SITES s on s.site_code = di.destination_site 
            where do.gi_date between @dateInitial and @dateFinish
            and site_type = 'TIENDA'
            and do.storage_id = '1004'
            and do.do_status IN (4,9,11)
            group by s.site_name , s.site_code , s.ip_address
            order by s.site_code", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).ToList();
            return list;
        }
        public decimal GetTotalsTransfersStoreByDate(DateTime dateInitial, DateTime dateFinish, string ip)
        {
            _contextCDSERP.Database.CommandTimeout = 1000;
            var result = _contextCDSERP.Database.SqlQuery<decimal>(@"SELECT isnull(Sum(purchase_balance),0) FROM [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] 
            where supplier_id = '1535' and purchase_status is null and purchase_date between @dateInitial and @dateFinish",
        new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).FirstOrDefault();
            return result;
        }
        public List<ReportDetail> GetTransferDetailsERPBOX(DateTime dateInitial, DateTime dateFinish, string ip)
        {
            try
            {
                _contextCDSERP.Database.CommandTimeout = 1000;
                var result = _contextCDSERP.Database.SqlQuery<ReportDetail>(@"SELECT cast(purchase_no as int) purchase_no , convert(varchar,purchase_date, 103) date_erp ,
                   purchase_total purchase_total_erp , purchase_status status_erp , purchase_comments FROM [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] 
                   where supplier_id = '1535' and purchase_date between @dateInitial and @dateFinish",
                new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).ToList();
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<ReportDetail> GetTransferDetailsPRT(DateTime dateInitial, DateTime dateFinish, string site)
        {
            var result = _contextPRT.Database.SqlQuery<ReportDetail>(@"select sum(isnull(di.amount,0)) purchase_total_prt , do.do_no  , 
            di.po_erpbox purchase_no , convert(varchar, do.gi_date, 103) date_prt , cast(do.do_status as nvarchar(10)) status_prt  from DELIVERY_ORDER do 
            join DELIVERY_ORDER_ITEM di on di.do_no = do.do_no 
            where do.gi_date between @dateInitial and @dateFinish
            and di.destination_site = @site
            and do.storage_id = '1004'
            and do.do_status IN (4,9,11)
            group by do.do_no,di.po_erpbox , do.gi_date , do.do_status",
            new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish), new SqlParameter("site", site)).ToList();
            return result;
        }
        public List<ReportDetail> GetAllPurchasesStoreByNumberandSite(DateTime dateInitial, DateTime dateFinish, string ip)
        {
            _contextCDSERP.Database.CommandTimeout = 1000;
            var result = _contextCDSERP.Database.SqlQuery<ReportDetail>(@"SELECT cast(purchase_no as int) purchase_no , purchase_total purchase_total_erp FROM [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] 
            where supplier_id = '1535'  and purchase_date between @dateInitial and @dateFinish", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).ToList();
            return result;
        }
        public ReportDetail CheckExistPoInDeliveryOrder(int po_erpbox, string site)
        {
            var result = _contextPRT.Database.SqlQuery<ReportDetail>(@"select SUM(amount) purchase_total_prt , convert(varchar, do.gi_date, 103) date_prt , do.do_no
            from DELIVERY_ORDER_ITEM di
            join DELIVERY_ORDER do on do.do_no = di.do_no  WHERE po_erpbox = '" + po_erpbox + @"' and destination_site = '" + site + @"' group by po_erpbox , do.gi_date , do.do_no").FirstOrDefault();
            return result;
        }
        public ReportDetail CheckExistPoInPurchasesERP(int po_erpbox, string ip)
        {
            _contextCDSERP.Database.CommandTimeout = 1000;
            var result = _contextCDSERP.Database.SqlQuery<ReportDetail>(@"SELECT CONVERT(nvarchar(40),purchase_date,103) date_erp , purchase_status status_erp,
            purchase_comments ,purchase_total purchase_total_erp FROM [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] 
            where purchase_no = '" + po_erpbox + "'").FirstOrDefault();
            return result;
        }
        public Tuple<bool, decimal> GetTotalOfTransfersByPO(string po_erpbox, string ip)
        {
            try
            {
                _contextPRT.Database.CommandTimeout = 1000;
                var result = _contextPRT.Database.SqlQuery<decimal>(@"SELECT ISNULL(SUM(purchase_total),0) purchase_total_erp from
                [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] where purchase_no in (" + po_erpbox + ") and purchase_status is null").FirstOrDefault();
                return new Tuple<bool, decimal>(true, result);
            }
            catch (Exception)
            {
                return new Tuple<bool, decimal>(false, 0);
            }
        }
        public List<ReportDetail> GetAllDeliveryOrder(DateTime dateInitial, DateTime dateFinish)
        {
            var result = _contextPRT.Database.SqlQuery<ReportDetail>(@"select distinct di.destination_site destination_site , isnull(di.po_erpbox,0) purchase_no from DELIVERY_ORDER do 
            JOIN DELIVERY_ORDER_ITEM di on do.do_no = di.do_no where do.gi_date
             between @dateInitial and @dateFinish
             and do.storage_id = '1004'
             and do.do_status in (4,9,11) ", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).ToList();
            return result;
        }
        public List<ReportHeader> GetTotalsTransfersPRTByDateAndSite(DateTime dateInitial, DateTime dateFinish, string site)
        {
            var list = _contextPRT.Database.SqlQuery<ReportHeader>(@"select s.site_code site_code , s.site_name site_name , s.ip_address,
            SUM(di.amount) totalPRT from DELIVERY_ORDER do 
            join DELIVERY_ORDER_ITEM di on di.do_no = do.do_no
            join SITES s on s.site_code = di.destination_site 
            where do.gi_date between @dateInitial and @dateFinish
            and site_type = 'TIENDA'
            and do.storage_id = '1004'
            and do.do_status IN (4,9,11)
            and s.site_code = @site
            group by s.site_name , s.site_code , s.ip_address
            order by s.site_code", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish), new SqlParameter("site", site)).ToList();
            return list;
        }
        public List<ReportDetail> GetAllDeliveryOrderBySite(DateTime dateInitial, DateTime dateFinish, string site, bool newErp)
        {
            var result = new List<ReportDetail>();
            if (newErp)
            {
                result = _contextPRT.Database.SqlQuery<ReportDetail>(@"select distinct di.destination_site destination_site , cast(isnull(di.do_no,0) as int) purchase_no from DELIVERY_ORDER do 
            JOIN DELIVERY_ORDER_ITEM di on do.do_no = di.do_no where do.gi_date
             between @dateInitial and @dateFinish
             and do.storage_id = '1004'
             and di.destination_site = @site
             and do.do_status in (4,9,11) ", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish), new SqlParameter("site", site)).ToList();
            }
            else
            {
                result = _contextPRT.Database.SqlQuery<ReportDetail>(@"select distinct di.destination_site destination_site , isnull(di.po_erpbox,0) purchase_no from DELIVERY_ORDER do 
            JOIN DELIVERY_ORDER_ITEM di on do.do_no = di.do_no where do.gi_date
             between @dateInitial and @dateFinish
             and do.storage_id = '1004'
             and di.destination_site = @site
             and do.do_status in (4,9,11) ", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish), new SqlParameter("site", site)).ToList();
            }
            return result;
        }
        public Tuple<bool, decimal> GetTotalOfTransfersNewERP(DateTime dateInitial, string id, string ip, DateTime newErpDate, string siteCode)
        {
            try
            {
                _contextPRT.Database.CommandTimeout = 1000;
                //var result = _contextPRT.Database.SqlQuery<decimal>(@"select isnull(SUM(isnull(tdi.quantity,0) * isnull(tdi.cost,0)),0) from
                //["+ip+ @"].[DFL_SAI_SITE].[dbo].[TRANSFER_DETAIL_IN] TDI
                //JOIN [" + ip + @"].[DFL_SAI_SITE].[dbo].[TRANSFER_HEADER_IN] THI ON TDI.transfer_id = THI.transfer_id
                //where THI.transfer_document in( " + id + ") and thi.transfer_status = 9").FirstOrDefault();

                var result = _contextDFLSAI.Database.SqlQuery<decimal>(@"select isnull(SUM(isnull(td.quantity,0) * isnull(td.cost,0)),0) from TRANSFER_HEADER th
			join TRANSFER_DETAIL td on th.transfer_document = td.transfer_document
			where th.transfer_document in( " + id + @") and th.transfer_status = 9 and th.destination_site_code = '" + siteCode + "' and td.destination_site_code = '" + siteCode + "'").FirstOrDefault();

                //Para transferencias en ERPBOX

                if(Convert.ToInt32(siteCode) < 31)
                {
                    result += _contextPRT.Database.SqlQuery<decimal>(@"SELECT ISNULL(SUM(purchase_total),0) purchase_total_erp from
                [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] where supplier_id = '1535'  and purchase_status is null and purchase_date between @dateInitial and @dateNewErp",
                new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateNewErp", newErpDate)).FirstOrDefault();
                }
                return new Tuple<bool, decimal>(true, result);

            }
            catch (Exception e)
            {
                return new Tuple<bool, decimal>(false, 0);
            }
        }
        public List<ReportDetail> GetTransfersDetailsNewERPByPO(string po, string ip , string siteCode)
        {
            try
            {
                _contextCDSERP.Database.CommandTimeout = 1000;
                var result = _contextDFLSAI.Database.SqlQuery<ReportDetail>(@"select sum(td.quantity * td.cost) as purchase_total_erp , 
            cast(th.transfer_document as int) as purchase_no , cast(th.transfer_status as nvarchar(10)) as status_erp, th.comment as 'purchase_comments' ,
            convert(varchar,th.cdate, 103) as date_erp from TRANSFER_HEADER th
			join TRANSFER_DETAIL td on th.transfer_document = td.transfer_document
			where th.transfer_document in  (" + po + @")
        and th.destination_site_code = '" + siteCode + "' and td.destination_site_code = '" + siteCode + "'" + @"
            group by th.transfer_document , th.transfer_status , th.cdate , th.comment").ToList();
                return result;
            }
            catch (Exception aa)
            {
                return null;
            }
        }
        //temporal santa fe erpbox
        public List<ReportDetail> GetTransferDetailsOldERPBOX(DateTime dateInitial, DateTime dateFinish, string ip, DateTime dateNewErp)
        {
            try
            {
                _contextCDSERP.Database.CommandTimeout = 1000;
                var result = _contextCDSERP.Database.SqlQuery<ReportDetail>(@"SELECT cast(purchase_no as int) purchase_no , convert(varchar,purchase_date, 103) date_erp ,
                   purchase_total purchase_total_erp , purchase_status status_erp , purchase_comments FROM [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] 
                   where supplier_id = '1535' and purchase_date >= @dateInitial and purchase_date <= @dateNewErp",
                new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateNewErp", dateNewErp)).ToList();
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public decimal GetTotalOfTransfersSantaFeByPO(DateTime dateInitial, string ip)
        {
            try
            {
                _contextPRT.Database.CommandTimeout = 1000;
                var result = _contextPRT.Database.SqlQuery<decimal>(@"SELECT ISNULL(SUM(purchase_total),0) purchase_total_erp from
                [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases] where supplier_id = '1535'  and purchase_status is null and purchase_date between @dateInitial and '19/09/2019'",
                new SqlParameter("dateInitial", dateInitial)).FirstOrDefault();
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public Tuple<bool, decimal> GetTotalOfTransfersByDate(DateTime dateInitial, DateTime dateFinish, string ip)
        {
            try
            {
                _contextPRT.Database.CommandTimeout = 1000;
                var result = _contextPRT.Database.SqlQuery<decimal>(@"SELECT ISNULL(SUM(purchase_total),0) purchase_total_erp from
                [" + ip + @"].[FLORIDO_ERP].[dbo].[Purchases]where purchase_date between @dateInitial and @dateFinish and supplier_id = '1535' and purchase_status is null "
                , new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).FirstOrDefault();
                return new Tuple<bool, decimal>(true, result);
            }
            catch (Exception)
            {
                return new Tuple<bool, decimal>(false, 0);
            }
        }
        public List<TransferItems> GetTransferItemsPRT(string do_no, string site_code)
        {
            var result = _contextPRT.Database.SqlQuery<TransferItems>(@"select doi.part_number , pm.part_description , doi.quantity , isnull(doi.amount,0) as 'amount' from DELIVERY_ORDER_ITEM doi
join PART_MASTER pm on doi.part_number = pm.part_number
 where doi.do_no = @do_no and doi.destination_site = @site"
, new SqlParameter("do_no", do_no), new SqlParameter("site", site_code)).ToList();
            return result;
        }
        public List<TransferItems> GetTransferItemsERP(string purchase_no, string ip)
        {
            var result = _contextPRT.Database.SqlQuery<TransferItems>(@"select pd.part_number , pm.part_description , pd.detail_qty as 'quantity' , 
pd.detail_price as 'amount' from [" + ip + @"].[Florido_ERP].[dbo].[PurchaseDetail] pd
join [" + ip + @"].[Florido_ERP].[dbo].[PartMaster] pm on pd.part_number = pm.part_number
 where pd.purchase_no = @purchase_no"
, new SqlParameter("purchase_no", purchase_no)).ToList();
            return result;
        }
        public List<SiteModel> GetSitesNewERP()
        {
            var result = _contextDFLSAI.Database.SqlQuery<SiteModel>(@"SELECT site_code as 'SiteCode' , new_erp_date as 'NewErpDate' FROM SITES WHERE new_erp = 1").ToList();
            return result;
        }
    }
}
