﻿using App.Entities;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Donations
{
    public class DonationsItemsRepository
    {
        private PRTSAIEntities _context;

        public DonationsItemsRepository()
        {
            _context = new PRTSAIEntities();
        }

        public List<DonationItem> GetItemsByDonationNumber(int DonationId)
        {
            var ItemsList = new List<DonationItem>();

            ItemsList = (from i in _context.DFLPOS_DONATION_ITEMS
                         where i.donation_id == DonationId                               
                         select new DonationItem
                         {
                             Id = i.item_id,
                             IsActive = i.active_flag,
                             Article = i.item_description,
                             Price = i.item_price,
                             DonationId = i.donation_id
                         }).ToList();

            return ItemsList;
        }

        public bool AddArticle(DFLPOS_DONATION_ITEMS Item)
        {
            try
            {
                _context.DFLPOS_DONATION_ITEMS.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public DonationItem GetArticleById(int ArticleId)
        {
            var Item = new DonationItem();
            Item = (from i in _context.DFLPOS_DONATION_ITEMS
                    where i.item_id == ArticleId
                    select new DonationItem
                    {
                        Article = i.item_description,
                        Price = i.item_price,
                        DonationId = i.donation_id,
                        IsActive = i.active_flag,
                        Id = i.item_id
                    }).FirstOrDefault();

            return Item;
        } 

        public bool UpdateArticle(DFLPOS_DONATION_ITEMS Item)
        {
            var ItemEdit = _context.DFLPOS_DONATION_ITEMS.Find(Item.item_id);
            if (ItemEdit != null)
            {
                _context.Entry(ItemEdit).State = System.Data.Entity.EntityState.Detached;
                _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;            
        }
    }
}
