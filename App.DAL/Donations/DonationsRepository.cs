﻿using App.Entities;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Donations
{
    public class DonationsRepository
    {
        private PRTSAIEntities _context;

        public DonationsRepository()
        {
            _context = new PRTSAIEntities();
        }

        public bool AddDonationCampaign(DFLPOS_DONATIONS Item)
        {
            try
            {
                _context.DFLPOS_DONATIONS.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }            
        }

        public List<Donation> GetDonationsCampaigns()
        {
            var DonationsList = new List<Donation>();
            DonationsList = (from d in _context.DFLPOS_DONATIONS
                             where d.active_flag
                             select d).AsEnumerable().Select(c => new Donation
                             {
                                 Id = c.donation_id,
                                 EndDate = c.final_date.ToString("dd/MM/yyyy"),
                                 StartDate = c.initial_date.ToString("dd/MM/yyyy"),
                                 Organization = c.organization,
                                 Name = c.name
                             }).ToList();

            return DonationsList;
        }

        public Donation GetDonationById(int DonationId)
        {
            var Donation = new Donation();
            Donation = (from d in _context.DFLPOS_DONATIONS
                        where d.donation_id == DonationId
                        select d).AsEnumerable().Select(c => new Donation
                        {
                            Id = c.donation_id,
                            EndDate = c.final_date.ToString("dd/MM/yyyy"),
                            StartDate = c.initial_date.ToString("dd/MM/yyyy"),
                            Organization = c.organization,
                            Name = c.name
                        }).FirstOrDefault();

            return Donation;
        }

        public bool UpdateDonationCampaigns(DFLPOS_DONATIONS Item)
        {
            var ItemEdit = _context.DFLPOS_DONATIONS.Find(Item.donation_id);
            if (ItemEdit != null)
            {
                _context.Entry(ItemEdit).State = System.Data.Entity.EntityState.Detached;
                _context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteCampaign(int DonationdId)
        {
            var Campaign = _context.DFLPOS_DONATIONS.Find(DonationdId);
            if (Campaign != null)
            {
                Campaign.active_flag = false;
                _context.SaveChanges();
                return true;
            }

            return false;
        
        }
    }
}
