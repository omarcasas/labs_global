
using App.Entities;
using App.Entities.ViewModels.InternalRequirement;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace App.Common
{
    public class Common
    {
        public static string SetPassword(string password)
        {
            return string.Join("", (new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(password))).Select(x => x.ToString("X2")).ToArray());
        }
        public static string MailMessageHtml(string title, string body, string buttonLink,string buttonText, string subject, string from, USER_MASTER to)
        {
            string Return = string.Empty;
            try
            {   var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_template.html");
                var text = System.IO.File.ReadAllText(urlFile);
                text = text.Replace("{Title}", title);
                text = text.Replace("{Body}", body);
                if (buttonText !="" && buttonLink!="")
                    text = text.Replace("<a href=>{Buton}</a>", "<a href="+ buttonLink + ">"+buttonText+"</a>");
                else
                    text = text.Replace("<a href=>{Buton}</a>", "");

                body = HttpUtility.HtmlDecode(text);

                message.To.Add(new MailAddress(to.email));
                message.From = new MailAddress(user,"Florido ERP");
                message.Subject = subject;
                message.IsBodyHtml = true;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                    body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }
        public string MailMessageServicesCalendar(SendMail information, Stream stream)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                string urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_services_calendar.html");
                var text = System.IO.File.ReadAllText(urlFile);
                text = text.Replace("{date}", information.date);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(new MailAddress(information.email));
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(information.body, Encoding.UTF8, "text/html");
                message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));
                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType();
                ct.MediaType = MediaTypeNames.Application.Pdf;
                ct.Name = "servicio_programado.pdf";
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageServicesCalendartoSupplier(SendMail information, Stream stream)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                string urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_services_calendarToSupplier.html");
                var text = System.IO.File.ReadAllText(urlFile);
                text = text.Replace("{proveedor}", information.supplier);
                text = text.Replace("{date}", information.date);
                text = text.Replace("{store}", information.store);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(new MailAddress(information.email));
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(information.body, Encoding.UTF8, "text/html");
                message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));
                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType();
                ct.MediaType = MediaTypeNames.Application.Pdf;
                ct.Name = "servicio_programado.pdf";
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessagePOCreditor(SendMail information, List<EmailPORequirements> stream)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                string urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_purchase_order.html");
                var text = System.IO.File.ReadAllText(urlFile);
                text = text.Replace("{proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);


                int count = 0;
                List<Attachment> Attachments = new List<Attachment>();
                foreach (var item in stream)
                {
                    var ct = new ContentType();
                    ct.MediaType = MediaTypeNames.Application.Pdf;
                    ct.Name = "Orden_Compra_" + item.site_code + ".pdf";
                    item.Reporte.Position = 0;
                    Attachment pdfb64 = new Attachment(item.Reporte, ct);
                    pdfb64.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                    Attachments.Add(pdfb64);
                    count++;
                }
                foreach (Attachment a in Attachments)
                    message.Attachments.Add(a);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageChangePrice(SendMail information, string tablePrice)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                string urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_change_price.html");
                var text = System.IO.File.ReadAllText(urlFile);
                text = text.Replace("{tablePrice}", tablePrice);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }


        public string MailMessagePOStore(SendMail information, List<EmailPORequirements> stream)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                string urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_purchase_order_site.html");
                var text = System.IO.File.ReadAllText(urlFile);
                text = text.Replace("{proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(new MailAddress(information.email));
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);


                int count = 0;
                List<Attachment> Attachments = new List<Attachment>();
                foreach (var item in stream)
                {
                    var ct = new ContentType();
                    ct.MediaType = MediaTypeNames.Application.Pdf;
                    ct.Name = "Orden_Compra_" + item.site_code + ".pdf";
                    item.Reporte.Position = 0;
                    Attachment pdfb64 = new Attachment(item.Reporte, ct);
                    pdfb64.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                    Attachments.Add(pdfb64);
                    count++;
                }
                foreach (Attachment a in Attachments)
                    message.Attachments.Add(a);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }


        public string MailLabelAndItems(SendMail information,bool history , bool allUsers)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                string urlFile = "";
                var text = "";
                if (history)
                {
                    urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_item_history.html");
                    text = System.IO.File.ReadAllText(urlFile);
                    text = text.Replace("{item}", information.item);
                    text = text.Replace("{project}", information.project);
                    text = text.Replace("{etiqueta}", information.label);
                }else if (allUsers)
                {
                    urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_new_project.html");
                    text = System.IO.File.ReadAllText(urlFile);
                }
                else
                {
                    urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_items.html");
                    text = System.IO.File.ReadAllText(urlFile);
                    text = text.Replace("{etiqueta}", information.label);
                }

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new System.Net.Mime.ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }
    }
}
