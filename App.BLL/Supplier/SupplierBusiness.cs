﻿using App.DAL.Purchases;
using App.DAL.Purchases.PurchasesModels;
using App.DAL.Supplier;
using App.Entities;
using App.Entities.ViewModels.MaSupplier; 
using System;
using System.Collections.Generic;
using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Supplier;
using App.BLL.Supplier;
using System.Linq;

namespace App.BLL.Purchases
{
    public class SupplierBusiness
    {
        private SupplierRespository _repository;
        private PurchaseBasePriceRespository _repositoryModel;
        private SupplierContactBusiness _supplierContactBusiness;
        private ItemMasterBusiness _itemMasterBusiness;


        public SupplierBusiness()
        {
            _repository = new SupplierRespository();
            _repositoryModel = new PurchaseBasePriceRespository();
            _supplierContactBusiness = new SupplierContactBusiness();
            _itemMasterBusiness = new ItemMasterBusiness();
        }

        public bool AddSupplier(MA_SUPPLIER supplier)
        {
            bool check = false;
            if (_repository.AddSupplier(supplier) > 0) { check = true; }
            return check;
        }

        public MA_SUPPLIER RfcExists(string rfc , string type)
        {
            var x = _repository.GetByRfcAndType(rfc , type);
            return x;
            //if (x == null)
            //{
            //    return;
            //}
            //else
            //{
            //    return true;
            //}
        }

        public MA_SUPPLIER GetByRFC2(string rfc)
        {
            return _repository.GetByRFC2(rfc);
        }
        public MA_SUPPLIER GetById(int id)
        {
            return _repository.GetById(id);
        }

        public List<MA_SUPPLIER> GetAllSuppliers()
        {
            return _repository.GetAllSuppliers();
        }

        public List<MA_SUPPLIER> GetAllOnlySuppliers()
        {
            return _repository.GetAllOnlySuppliers();
        }
        public List<SupplierModel> GetAllSuppliersInModel()
        {
            return _repository.GetAllSuppliersInModel();
        }
        public List<SupplierModel> GetAllSuppliersAndCreditorInModel()
        {
            return _repository.GetAllSuppliersAndCreditorInModel();
        }
        public List<MaSupplierDropDownModel> GetAllSuppliersToDropDown()
        {
            return _repository.GetAllSuppliersToDropDown();
        }
        public List<MA_SUPPLIER> GetAvailableSuppliersByProduct(string partNumber)
        {
            return _repository.GetAvailableSuppliersByProduct(partNumber);
        }
        public List<MA_SUPPLIER> GetAvailableSuppliersByProduct2(string partNumber)
        {
            return _repository.GetAvailableSuppliersByProduct2(partNumber);
        }
        public List<SupplierNotActiveModel> GetSuppliersByProduct(string partNumber)
        {
            var SupplierList = _repository.GetSuppliersByProduct(partNumber);
            //List<MA_SUPPLIER> items = new List<MA_SUPPLIER>();
            //foreach (var i in SupplierList)
            //{
            //    items.Add(new MA_SUPPLIER { supplier_id = i.supplier_id, rfc = i.rfc, commercial_name = i.commercial_name, business_name = i.business_name,supplier_address = i.supplier_address, city=i.city,zip_code = i.zip_code, supplier_state = i.supplier_state, country = i.country, supplier_type = i.supplier_type, accounting_account=i.accounting_account, cuser=i.cuser,cdate=i.cdate, uuser=i.uuser, udate=i.udate, program=i.program  });
            //}
            //return items;
            return SupplierList;
        }
        public List<PurchaseBasePriceModel> GetPricesOfSuppliersByProduct(string partNumber)
        {
            return _repositoryModel.GetPricesOfSuppliersByProduct(partNumber);
        }

        public int sp_Purchases_UpdateBasePrice(int BasePriceId, int SupplierPartNumberId, decimal BasePrice, string Currency, DateTime ValidFrom, string User, string program_id)
        {
            return _repositoryModel.sp_Purchases_UpdateBaseCost(BasePriceId, SupplierPartNumberId, BasePrice, Currency, ValidFrom, User, program_id);
        }

        public List<PurchasesPriceModel> GetPURCHASEBASEPRICE(string partNumber)
        {

            return _repository.GetPURCHASEBASEPRICE(partNumber);
        }
        public bool UpdateSupplier(MA_SUPPLIER supplier)
        {
            var check = false;
            if (_repository.UpdateSupplier(supplier) > 0) check = true;
            return check;
        }
        public List<PurchaseBasePriceViewModel> GetPricesOfSuppliersByProducts(DateTime LaterDate, DateTime NextDate)
        {
            //return null;
            var Purchase = _repositoryModel.GetPurchaseBasePrice(LaterDate,NextDate);
            return Purchase;
        }

        public List<CreditorPOModel> GetCreditorInformation(int id_creditor)
        {
            return _repository.GetCreditorInformation(id_creditor);
        }

        public List<MaSupplierDropDownModel> GetAllByType(string type)
        {
            return _repository.GetAllByType(type);
        }

        public string GetNameSupplier(int supplier_id)
        {
            return _repository.GetNameSupplier(supplier_id);
        }
        public string EditSupplier(string RFC , string type , string user , List<SupplierContactsEditModel> contacts)
        {
            var supplier = _repository.GetByRfc(RFC);
            if (type == "Acreedor")
            {
                supplier.truck_foot = 0;
                supplier.merchandise_entry = 0;
                supplier.apply_return = 0;
            }
            supplier.cdate = DateTime.Now;
            supplier.cuser = user;
            supplier.supplier_type = type;
            supplier.program = "PURCH002.cshtml";
            var x = _repository.EditSupplier(RFC, type, supplier);
            if (x.Item2 != 0)
            {
                if (contacts != null)
                {
                    foreach (var item in contacts)
                    {
                        MA_SUPPLIER_CONTACT newContact = new MA_SUPPLIER_CONTACT();
                        newContact.supplier_id = x.Item2;
                        newContact.name = item.name;
                        newContact.last_name = item.last_name;
                        newContact.phone = item.phone;
                        newContact.email = item.email;
                        newContact.departament = item.department;
                        newContact.cuser = user;
                        newContact.flagActive = true;
                        newContact.cdate = DateTime.Now;
                        newContact.program_id = "PURCH002.cshtml";
                        _supplierContactBusiness.AddSupplierContact(newContact);
                    }
                }                
            }            
            return x.Item1;
        }
        public List<MA_SUPPLIER> GetAllOnlySuppliersForPromotions()
        {
            return _repository.GetAllOnlySuppliers();
        }


        public bool EditSupplierPartIva(int supplier, string part_iva, string uuser)
        {
            return _repository.EditSupplierPartIva(supplier, part_iva, uuser);
        }

        public int EditSupplierPartIvaMetohd(int supplier, string part_iva, string uuser)
        {
            if (EditSupplierPartIva(supplier, part_iva, uuser))
                return _itemMasterBusiness.UpdateItemIVABySupplier(supplier, uuser);
            return 0;
        }

        public decimal GetValueSupplierPartIva(int supplier)
        {
            return _repository.GetValueSupplierPartIva(supplier);
        }

        public MaSupplierModel GetSupplierInfoById(int supplier_id)
        {
            return _repository.GetSupplierInfoById(supplier_id);
        }

        public List<MaSuppliersModel> GetAll() => _repository.GetAll();

        public List<MaSupplierModel> SearchSupplier(string supplier) => _repository.SearchSupplier(supplier);
    }
}
