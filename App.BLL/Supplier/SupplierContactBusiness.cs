﻿using App.DAL.Supplier;
using App.Entities;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Supplier
{
    public class SupplierContactBusiness
    {
        private SupplierContactRepository _supplierContactRepository;

        public SupplierContactBusiness()
        {
            _supplierContactRepository = new SupplierContactRepository();
        }

        public bool AddSupplierContact(MA_SUPPLIER_CONTACT supplierContact)
        {
            bool check = false;
            if (_supplierContactRepository.AddSupplierContact(supplierContact) > 0) { check = true; }
            return check;
        }
        public List<SupplierContactModel> GetAllContactsBySupplierId(string supplier_id)
        {
            return _supplierContactRepository.GetAllContactsBySupplierId(supplier_id);
        }
        public bool UpdateSupplierContact(MA_SUPPLIER_CONTACT supplierContact)
        {
            var check = false;
            if (_supplierContactRepository.UpdateSupplierContact(supplierContact) > 0) check = true;
            return check;
        }
        public MA_SUPPLIER_CONTACT GetContactById(int id)
        {
            return _supplierContactRepository.GetContactById(id);
        }

        public CreditorPOModel GetContactFirstByDeparment(int id_supplier, string deparment)
        {
            return _supplierContactRepository.GetContactFirstByDeparment(id_supplier, deparment);
        }

        public string GetContactAllByDeparment(int id_supplier, string deparment)
        {
            return _supplierContactRepository.GetContactAllByDeparment(id_supplier, deparment);
        }
    }
}
