﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.DAL.Approvals;
using App.Entities;

namespace App.BLL.Approval
{
    public class ApprovalBusiness
    {
        private readonly ApprovalsRepository approvalsRepo = new ApprovalsRepository();

        public Result<List<APPROVAL_MASTER_ROUTE>> ApprovalProcessList()
        {
            return approvalsRepo.ApprovalProcessList();
        }
        public Result<List<APPROVAL_HISTORY>> ApprovalHistoryList()
        {
            return approvalsRepo.ApprovalHistory();
        }
        public Result<List<APPROVAL_HISTORY>> StartProcess()
        {
            throw new NotImplementedException();
        }

        public Result<Empty> UpdateProcess()
        {
            throw new NotImplementedException();
        }

        private Result<Empty> NotifyToNextStep()
        {
            throw new NotImplementedException();
        }

        private Result<Empty> NotifyToRequesters()
        {
            throw new NotImplementedException();
        }



    }


}
