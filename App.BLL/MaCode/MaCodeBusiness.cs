﻿using App.DAL.MaCode;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.MaCode
{
    public class MaCodeBusiness
    {
        private MaCodeRepository _MaCodeRepository;

        public MaCodeBusiness()
        {
            _MaCodeRepository = new MaCodeRepository();
        }

        public List<MA_CODE> GetListByCode(string code)
        {
            return _MaCodeRepository.GetListByCode(code);
        }

        public string GetBuyerDivisionSistemas()
        {
            return _MaCodeRepository.GetBuyerDivisionSistemas();
        }

        public bool ValidateItemForce90(string part_number)
        {
            return _MaCodeRepository.ValidateItemForce90(part_number);
        }

        public string GetEmailsPriceChange()
        {
            return _MaCodeRepository.GetEmailsPriceChange();
        }
        public bool GetAndSetSupplierXML(string rfc, string business_name, int status, string uuser) => _MaCodeRepository.GetAndSetSupplierXML(rfc, business_name, status, uuser);

        public List<MA_CODE> GetListXMLSites() => _MaCodeRepository.GetListXMLSites();
    }
}
