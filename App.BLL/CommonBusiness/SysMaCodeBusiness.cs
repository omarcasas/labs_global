﻿using App.DAL.CommonAccess;
using App.Entities;
using App.Entities.ViewModels.MaCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.CommonBusiness
{
    public class SysMaCodeBusiness
    {
        private MaCodeRepository _MaCodeRepo;

        //constructor
        public SysMaCodeBusiness()
        {
            _MaCodeRepo = new MaCodeRepository();
        }

        public List<MA_CODE> GetAllFixedAssetDepreciation()
        {
            return _MaCodeRepo.GetAllFixedAssetDepreciation();
        }
        public List<MA_CODE> GetAllFixedAsset()
        {
            return _MaCodeRepo.GetAllFixedAsset();
        }
        public List<MA_CODE> GetAllUM()
        {
            return _MaCodeRepo.GetAllUM();
        }
        public List<MA_CODE> GetAllCurrency()
        {
            return _MaCodeRepo.GetAllCurrency();
        }
        public List<MA_CODE> GetAllNegotiationDiscount()
        {
            return _MaCodeRepo.GetAllNegotiationDiscount();
        }

        public List<MA_CODE> GetAllNegotiationPrice()
        {
            return _MaCodeRepo.GetAllNegotiationPrice();
        }
        public List<MA_CODE> GetAllNegotiationPoint()
        {
            return _MaCodeRepo.GetAllNegotiationPoint();
        }

        public List<MA_CODE> GetAllNegotiationPenalties()
        {
            return _MaCodeRepo.GetAllNegotiationPenalties();
        }
        public List<MA_CODE> GetAllBuyerDivision()
        {
            return _MaCodeRepo.GetAllBuyerDivision();
        }
        public List<MA_CODE> GetAllItemType()
        {
            return _MaCodeRepo.GetAllItemType();
        }
        public List<MA_CODE> GetAllCodeByCode(string code)
        {
            return _MaCodeRepo.GetAllCodeByCode(code);
        }

        public List<MaCodeModel> GetCurrency()
        {
            var Currency = _MaCodeRepo.GetListCurrency()
                .Select(x => new MaCodeModel
                {
                    VKey = x.vkey

                }).ToList();
            return Currency;
        }
    }
}
