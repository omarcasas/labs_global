﻿using App.DAL.CommonAccess;
using App.Entities;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
namespace App.BLL.CommonBusiness
{
    public class SysMaClassBusiness
    {
        private MaClassRepository _MaClassRepo;
        //constructor
        public SysMaClassBusiness()
        {
            _MaClassRepo = new MaClassRepository();
        }
        public List<MA_CLASS> GetMaClassDivision(int parent, int level)
        {
            List<MA_CLASS> items = new List<MA_CLASS>();
            var ClassItems = _MaClassRepo.GetMaClassDivision(parent, level);
            foreach (var i in ClassItems)
            {
                items.Add(new MA_CLASS { class_id = i.class_id, class_name = i.class_name });
            }
            return items;
        }
        public List<MA_CLASS> GetMaClassDivisionForLevel(int level)
        {
            List<MA_CLASS> items = new List<MA_CLASS>();
            var ClassItems = _MaClassRepo.GetMaClassDivisionForLevel(level);
            foreach (var i in ClassItems)
            {
                items.Add(new MA_CLASS { class_id = i.class_id, class_name = i.class_name });
            }
            return items;
        }
        public bool AddMaClass(MA_CLASS Category)
        {
            var check = false;
            var flag = _MaClassRepo.AddMaClass(Category);

            if (flag > 0) check = true;

            return check;
        }
        public List<MA_CLASS> GetAllMaClass()
        {
            return _MaClassRepo.GetAllMaClass();
        }
        /*busqueda por producto*/
        public List<MaClassViewModel> GetItemMaClass(string PartNumber)
        {
            return _MaClassRepo.GetItemMaClass(PartNumber);
        }
        public List<MaClassViewModel> GetItemMaClass2(string PartNumber)
        {
            return _MaClassRepo.GetItemMaClass2(PartNumber);
        }
        /**/
        /*busqueda por categoria*/
        public List<MaClassViewModel> GetMaClassPrimary(int MaClassId)
        {
            return _MaClassRepo.GetMaClassPrimary(MaClassId);
        }
        public List<MaClassViewModel> GetMaClassSecundary(int MaClassId)
        {
            return _MaClassRepo.GetMaClassSecundary(MaClassId);
        }
        /**/
        /*filtro de productos mediante categoria*/
        public List<ItemMasterModel> GetProductMaClass(int MaClassId)
        {
            return _MaClassRepo.GetProductMaClass(MaClassId);
        }
        public List<ItemMasterModel> GetProductMaClass2(int MaClassId)
        {
            return _MaClassRepo.GetProductMaClass2(MaClassId);
        }
        /**/
        public List<MA_CLASS> GetAllDepartments(int level)
        {
            return _MaClassRepo.GetAllDepartments(level);
        }
        public List<MA_CLASS> GetAllFamilyByParentClass(int level2)
        {
            return _MaClassRepo.GetAllFamilyByParentClass(level2);
        }
    }
}
