﻿using App.DAL;
using App.DAL.CommonAccess;
using App.Entities;
using System.Collections.Generic;
namespace App.BLL.CommonBusiness
{
    public class SysMaTaxBusiness
    {
        private MaTaxRepository _MaTaxRepo;

        //constructor
        public SysMaTaxBusiness()
        {
            _MaTaxRepo = new MaTaxRepository();
        }

        public List<MA_TAX> GetAllIva()
        {
            return _MaTaxRepo.GetAllIva();
        }
        public List<MA_TAX> GetAllIeps()
        {
            return _MaTaxRepo.GetAllIeps();
        }
        public List<MA_TAX> GetAllIVANormal()
        {
            return _MaTaxRepo.GetAllIVANormal();
        }
            public MA_TAX GetMaTaxByCode(string code)
        {
            return _MaTaxRepo.GetMaTaxByCode(code) ?? new MA_TAX();
        }
        public StoreProcedureIvaIeps GetIvaIepsByIdValue(string part_number)
        {
            StoreProcedureIvaIeps itemC = new StoreProcedureIvaIeps();
            return _MaTaxRepo.StoreProcedureIvaIeps(part_number);
        }

    }
}
