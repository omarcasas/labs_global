using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using App.Common;
using App.DAL;
using App.Entities;
using App.Entities.ViewModels.XML;

namespace App.BLL.Expense
{
    public class Xml
    {
        
        public static Result<List<XML_HEADER>> GenXMLList(List<XMLFile> files,string user_name,string program_id)
        {
            MaSupplierRepository _maSupplierRepository = new MaSupplierRepository();
            var xml_header_list = new List<XML_HEADER>();
            foreach (var file in files)
            {
                
                try
                {
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.xml));
                
                
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;
                    var space = root?.Name.NamespaceName;


                    var rfc = (string) root?.Element(XName.Get("Emisor",space))?.Attribute("Rfc");
                    var sup = _maSupplierRepository.GetSupplierByRFC(rfc);
                    int flag = 0;
                    var index = 0;                
                    for (var i = 0; i < sup.Count; i++)
                    {
                        index = i;
                        if (sup[i].supplier_id == 0) return Result<List<XML_HEADER>>.Error("No esta dado de alta el provedor del XML");
                        if (sup[i].supplier_type != "Acreedor")                         
                            flag++;                          
                          
                    }
                    if (flag == sup.Count) { return Result<List<XML_HEADER>>.Error("El proveedor " + (sup.Any() ? sup[index].commercial_name : "") + " no está como acreedor"); }
                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var currency = (string)root?.Attribute("Moneda") ?? "";
                    var iepsTotal = 0M;
                    var ivaTotal = 0M;
                    var isr_retainedTotal = 0M;
                    var iva_retainedTotal = 0M;
                    try
                    {
                        iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception e)
                    {
                        iepsTotal = 0;
                    }
                    try
                    {
                        isr_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception e)
                    {
                        isr_retainedTotal = 0;
                    }
                    try
                    {
                        iva_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception e)
                    {
                        iva_retainedTotal = 0;
                    }
                    try
                    {
                        ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception e)
                    {
                        ivaTotal = 0;
                    }

                    var xml_header = new XML_HEADER
                    {
                        uuid_invoice = (string)uuid,
                        invoice_no = (string)root?.Attribute("Folio"),
                        invoice_date = (DateTime?)root?.Attribute("Fecha"),
                        supplier_id = sup[index].supplier_id,
                        currency = currency,
                        iva = ivaTotal,
                        ieps = iepsTotal,
                        isr_retained = isr_retainedTotal,
                        iva_retained = iva_retainedTotal,
                        subtotal = (decimal?)root?.Attribute("SubTotal") ?? 0,
                        discount = (decimal?)root?.Attribute("Descuento") ?? 0,
                        total_amount = (decimal?)root?.Attribute("Total") ?? 0,
                        xml = file.xml,
                        xml_name = file.xml_name,
                        cdate = DateTime.Now,
                        cuser = user_name,
                        program_id = program_id,
                        invoice_type= (string)root?.Attribute("TipoDeComprobante"),
                        exchange_currency = (decimal?) root?.Attribute("TipoCambio"),
                        payment_method = (string) root?.Attribute("MetodoPago"),
                        payment_way = (string) root?.Attribute("FormaPago"),
                        version =  (string) root?.Attribute("Version")
                    };
                    xml_header.XML_DETAIL = root?.Descendants(XName.Get("Concepto", space)).Select(e => {
                        var iva_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var ieps_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var isr_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var iva_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        return new XML_DETAIL
                        {
                            iva_amount = iva_amount,
                            ieps_amount = ieps_amount,
                            isr_retained = isr_retained,
                            iva_retained = iva_retained,
                            item_no = (string)e.Attribute("NoIdentificacion") ?? "",
                            item_description = (string)e.Attribute("Descripcion") ?? "",
                            quantity = (decimal?)e.Attribute("Cantidad") ?? 0,
                            unit_cost = (decimal?)e.Attribute("ValorUnitario") ?? 0,
                            item_amount = (decimal?)e.Attribute("Importe") ?? 0,
                            discount = (decimal?)e.Attribute("Descuento") ?? 0,
                        };
                    }).ToList();
                    xml_header_list.Add(xml_header);
                }
                catch (Exception)
                {
                    return Result.Error($"Error leer datos de xml: \n{file.xml_name}");
                }
            }

            return Result.OK(xml_header_list);
        }
        
    }
}