﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using App.Common;
using App.DAL.Expenses;
using App.DAL.InternalRequirement;
using App.Entities;
using App.Entities.ViewModels.InternalRequirement;
using App.Entities.ViewModels.Expenses;

namespace App.BLL.Expense
{
    public class ExpenseStartReq
    {
        
        public string Username { get; set; }
        public string ProgramId { get; set; }
        public string SiteCode { get; set; }
        public List<InternalRequirementModel> Products { get; set; }
        public bool ToNewProjects { get; set; }

    }

    public class ExpenseUpdateReq
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public bool Approved { get; set; }
        public string Comment { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
    }

    public class ExpenseBusiness
    {
        private  readonly  InternalRequirementRepository _internalRequirementRepository = new InternalRequirementRepository();
        private readonly ExpensesRepository _expensesRepository = new ExpensesRepository();
        private readonly UserMasterBusiness _userMasterBusiness = new UserMasterBusiness();


        public Result<Empty> ExpenseStart(ExpenseStartReq data)
        {
            var model = new INTERNAL_REQUIREMENT_HEADER
            {
                requirement_type = "gasto",
                cuser = data.Username,
                cdate = DateTime.Now,
                requirement_status = 0,
                program_id = data.ProgramId,
                site_code = data.SiteCode ?? "0025",

            };
            
            if(data.Products== null || data.Products.Count ==0)
                return Result<Empty>.Error("Se necesita agregar por lo menos un producto");


            var details = data.Products.Select(e => new INTERNAL_REQUIREMENT_DETAIL
            {
                //part_number = e.part_number,
                //supplier_id = e.supplier_id,
                supplier_part_number_creditors = e.id_item_supplier,
                quantity = e.quantity,
                estimated_price = e.estimate_price,
                cuser = data.Username,
                cdate = DateTime.Now,
                program_id = data.ProgramId
            });
            var estimate_price = data.Products.Select(e => e.estimate_price).Sum();

            var type = ExpenseType.Normal;

            
            if (estimate_price >= 10000 || data.ToNewProjects)
            {
                return _expensesRepository.ExpenseStart(model,details, "gastos_proyectos");
            }else if (estimate_price >= 5000)
            {
                return _expensesRepository.ExpenseStart(model, details, "gastos_distrital");
            }
            else
            {
                return _expensesRepository.ExpenseStart(model, details, "gastos_simple");
            }

        }


        public List<ExpensesReportModel> ExpensesReport(string siteCode, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status)
        {
            return _expensesRepository.ExpensesReportNew(siteCode, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, Status);
        }
        public List<ExpensesReportModel> ExpensesReportDetail(string siteCode, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status, bool detailFlag)
        {
            return _expensesRepository.ExpensesReportDetailNew(siteCode, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, Status, detailFlag);
        }
        public ItemXMLDetail getAllItemHeaderById(int xml_id)
        {
            var header = _expensesRepository.getAllItemHeaderById(xml_id);
            var nameUser = _userMasterBusiness.getCompleteName(header.cuser);
            if (nameUser != "")
                header.cuser = nameUser;
            return header;
        }

        public List<ExpensesChart> CreditorsExpensive(string site_code)
        {
            return _expensesRepository.CreditorsExpensive(site_code);
        }

        public List<ExpensesChart> CreditorsExpensiveQuantity(string site_code)
        {
            return _expensesRepository.CreditorsExpensiveQuantity(site_code);
        }

        public List<ExpensesChart> CreditorsExpensiveReferenceType(string site_code)
        {
            return _expensesRepository.CreditorsExpensiveReferenceType(site_code);
        }

        public List<ExpensesChart> CreditorsExpensiveAll()
        {
            return _expensesRepository.CreditorsExpensiveAll();
        }

        public List<ExpensesChart> CreditorsExpensiveQuantityAll()
        {
            return _expensesRepository.CreditorsExpensiveQuantityAll();
        }

        public List<ExpensesChart> CreditorsExpensiveReferenceTypeAll()
        {
            return _expensesRepository.CreditorsExpensiveReferenceTypeAll();
        }

        public List<ItemXMLDetail> getAllItemsById(int xml_id)
        {
            return _expensesRepository.getAllItemsById(xml_id);
        }

    }
}
