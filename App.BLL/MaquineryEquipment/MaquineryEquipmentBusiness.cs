﻿using App.DAL.MaquineryEquipment;
using App.DAL.SalesCalendar;
using App.Entities.ViewModels.MaquineryEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.MaquineryEquipment
{
    public class MaquineryEquipmentBusiness
    {
        private readonly MaquineryEquipmentRepository _MaquineryEquipmentRepository;
        private readonly ServicesCalendarHeaderRepository _servicesCalendarHeaderRepository;
        private readonly ServicesCalendarDetailRepository _servicesCalendarDetailRepository;

        public MaquineryEquipmentBusiness()
        {
            _MaquineryEquipmentRepository = new MaquineryEquipmentRepository();
            _servicesCalendarHeaderRepository = new ServicesCalendarHeaderRepository();
            _servicesCalendarDetailRepository = new ServicesCalendarDetailRepository();
        }

        public List<MaquineryEquipmentModel> getAll()
        {
            return _MaquineryEquipmentRepository.getAll();
        }

        public List<MaquineryEquipmentModel> getAllOnlyActive()
        {
            return _MaquineryEquipmentRepository.getAll().Where(w => w.FlagActive = true).ToList();
        }

        public MaquineryEquipmentModel getEquipmentById(int id)
        {
            return _MaquineryEquipmentRepository.getEquipmentById(id);
        }

        public MaquineryEquipmentModel getEquipmentBySerialNumber(string serial_number)
        {
            return _MaquineryEquipmentRepository.getEquipmentBySerialNumber(serial_number);
        }

        public List<MaquineryEquipmentModel> getAllBySite(string site_code)
        {
            return _MaquineryEquipmentRepository.getAllBySite(site_code);
        }

        public Tuple<List<MaquineryEquipmentDropDownModel>, int> GetAllBySiteAndDepartmentToDropDown(string departmentt, string site_code, string ServiceDate)
        {
            var x = DateTime.Parse(ServiceDate).Date;
            var serviceId = _servicesCalendarHeaderRepository.GetServiceCalendarHeaderId(x, site_code);
            if (serviceId != 0)
            {
                var listDetails = _servicesCalendarDetailRepository.GetServicesDetailDepartmentByServiceId(serviceId);
                var listMaquinery = _MaquineryEquipmentRepository.GetAllBySiteAndDepartmentToDropDown(departmentt, site_code, listDetails);
                if (listMaquinery.Count > 0)
                    return Tuple.Create(listMaquinery, 1);
                else
                    return Tuple.Create<List<MaquineryEquipmentDropDownModel>, int>(null, 0);
            }
            else
            {
                var listMaquinery = _MaquineryEquipmentRepository.GetAllBySiteAndDepartmentToDropDown(departmentt, site_code);
                if (listMaquinery.Count > 0)
                    return Tuple.Create(listMaquinery, 1);
                else
                    return Tuple.Create<List<MaquineryEquipmentDropDownModel>, int>(null, 2);
            }
        }

        public bool addMaquineryEquipment(MaquineryEquipmentModel model, string user)
        {
            return _MaquineryEquipmentRepository.addMaquineryEquipment(model, user);
        }

        public bool validateMaquineryEquipment(string SerialNumber)
        {
            return _MaquineryEquipmentRepository.validateSerialNumber(SerialNumber);
        }

        public bool validateExistingSerialNumber(string SerialNumber, int EquipmentId)
        {
            return _MaquineryEquipmentRepository.validateExistingSerialNumber(SerialNumber, EquipmentId);
        }

        public bool setMaquineryEquipment(MaquineryEquipmentModel model, string user)
        {
            return _MaquineryEquipmentRepository.setMaquineryEquipment(model, user);
        }

        public bool removeMaquineryEquipment(int EquipmentId, bool FlagActive, string user)
        {
            return _MaquineryEquipmentRepository.removeMaquineryEquipment(EquipmentId, FlagActive, user);
        }
    }
}