﻿using App.DAL.SAT;
using App.Entities;
using System.Collections.Generic;

namespace App.BLL.SAT
{
    public class productCatalogueSatBusiness
    {

        private productCatalogueSatRepository _context;

        //constructor
        public productCatalogueSatBusiness()
        {
            _context = new productCatalogueSatRepository();
        }

        public List<SATDivision> GetAllSATDivision()
        {
            return _context.GetAllSATDivision();
        }
        public List<SATGrupo> GetAllSATGrupo(string cve_division)
        {
            return _context.GetAllSATGrupo(cve_division);
        }
        public List<SATClase> GetAllSATClase(string cve_grupo)
        {
            return _context.GetAllSATClase(cve_grupo);
        }
        public List<SATCatalogoProdServ> GetAllSATCatalogoProdServ(string Filter)
        {
            return _context.GetAllSATCatalogoProdServ(Filter);
        }
        public List<SATum> GetAllSATum(string Filter)
        {
            return _context.GetAllSATum(Filter);
        }
        public List<SATum> GetAllSATumById(string Filter)
        {
            return _context.GetAllSATumById(Filter);
        }
    }
}
