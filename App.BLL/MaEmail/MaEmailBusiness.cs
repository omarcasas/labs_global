﻿using App.DAL.MaEmail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.MaEmail
{
    public class MaEmailBusiness
    {
        private MaEmailRepository _maEmailRepository;
        public MaEmailBusiness()
        {
            _maEmailRepository = new MaEmailRepository();
        }

        public string GetMaEmail(string site_code, string cargo)
        {
            return _maEmailRepository.GetMaEmail(site_code, cargo);
        }
        public string GetEmailGenericList(string cargo)
        {
            return _maEmailRepository.GetEmailGenericList(cargo);
        }
    }
}
