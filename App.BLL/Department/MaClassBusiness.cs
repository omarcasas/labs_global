﻿using App.DAL.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Department
{
    public class MaClassBusiness
    {
        private MaClassRepository _MaClassBusiness;

        public MaClassBusiness()
        {
            _MaClassBusiness = new MaClassRepository();
        }
        public List<MaClassModel> getListDepartament(int Supplier)
        {
            var description = _MaClassBusiness.departament(Supplier);

            return description;
        }

    }
}
