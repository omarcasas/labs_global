﻿using App.DAL.CodesPackages;
using App.Entities.ViewModels.CodesPackege;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.CodesPackages
{
    public class CodesPackagesBusiness
    {
        private readonly CodesPackageRepository _CodesPackageRepository;

        public CodesPackagesBusiness()
        {
            _CodesPackageRepository = new CodesPackageRepository();
        }

        public bool SaveCodePackage(string PartNumberOrigin, List<CodesPackagesModel> Codes, string User)
        {
            if (_CodesPackageRepository.ValidateCodePackage(PartNumberOrigin))
                return _CodesPackageRepository.UpdateCodePackage(PartNumberOrigin, Codes, User);
            else
                return _CodesPackageRepository.CreateCodePackage(PartNumberOrigin, Codes, User);
        }

        public List<CodesPackagesModel> GetCodePackage(string PartNumberOrigin) => _CodesPackageRepository.GetCodePackage(PartNumberOrigin);

        public bool ValidateCodePackage(string PartNumberOrigin) => _CodesPackageRepository.ValidateCodePackage(PartNumberOrigin);

        public List<CodesPackagesModel> GetCodesPackages() => _CodesPackageRepository.GetCodesPackages();

        public List<CodesPackagesModel> GetCodePackagesByOriginPartNumber(string OriginPartNumber) => _CodesPackageRepository.GetCodePackage(OriginPartNumber);
    }
}