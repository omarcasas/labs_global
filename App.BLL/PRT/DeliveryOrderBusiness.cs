﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.DAL.PRT;
using App.Entities.ViewModels.PRT;

namespace App.BLL.PRT
{
    public class DeliveryOrderBusiness
    {
        private DeliveryOrderRepository _deliveryOrderRepository;
        public DeliveryOrderBusiness()
        {
            _deliveryOrderRepository = new DeliveryOrderRepository();
        }
        public List<ReportHeader> GetTotalsTransfersByDate(DateTime dateInitial, DateTime dateFinish, string site)
        {
            //List<ReportHeader> list = new List<ReportHeader>();
            //list =_deliveryOrderRepository.GetTotalsTransfersPRTByDate(dateInitial,dateFinish);
            //list.ForEach(a =>
            //{
            //    var erp = _deliveryOrderRepository.GetAllPurchasesStoreByNumberandSite(dateInitial, dateFinish, a.ip_address);
            //    erp.ForEach(b =>
            //    {                   
            //        var ex = _deliveryOrderRepository.CheckExistPoInDeliveryOrder(b.purchase_no);
            //        if (ex != 0)
            //            a.totalPRT += b.purchase_total_erp;

            //        a.totalStore += b.purchase_total_erp;
            //    });
            //});
            //return list;

            var listSites = _deliveryOrderRepository.GetSitesNewERP();

            if (site == "0")
            {
                List<ReportHeader> list = new List<ReportHeader>();
                //Obtiene todas las tiendas del rango de fechas con su total de procesadora
                list = _deliveryOrderRepository.GetTotalsTransfersPRTByDate(dateInitial, dateFinish);
                //Obtiene todas las ordenes de compra de todas las tiendas involucradas
                var listAllPRT = _deliveryOrderRepository.GetAllDeliveryOrder(dateInitial, dateFinish);
                list.ForEach(a =>
                {
                    //Filtra de por site
                    var temp = listAllPRT.Where(b => b.destination_site == a.site_code).ToList();
                    var result = new Tuple<bool, decimal>(false, 0);
                    //Agrega todos las ordenes de compra de procesadora
                    var pos = "";
                    if (temp.Count > 1)
                    {
                        temp.ForEach(d => pos += "'" + d.purchase_no + "',");
                    }
                    else
                    {
                        pos = "'" + temp[0].purchase_no.ToString() + "',";
                    }
                    pos = pos.Remove(pos.Length - 1);

                    //validar nuevo erp
                    if (listSites.Exists(x => x.SiteCode == a.site_code))
                    {
                        //Obtiene el total de la transferencias de las ordenes de compras de la tienda
                        result = _deliveryOrderRepository.GetTotalOfTransfersNewERP(dateInitial, pos, a.ip_address, listSites.Find(x => x.SiteCode == a.site_code).NewErpDate, a.site_code);
                        //temporal santa fe
                        //result.Item2 += _deliveryOrderRepository.GetTotalOfTransfersSantaFeByPO(pos, a.ip_address).Item2;

                    }
                    else
                    {
                        //Obtiene el total de la transferencias de las ordenes de compras de la tienda
                        //result = _deliveryOrderRepository.GetTotalOfTransfersByPO(pos, a.ip_address);
                        result = _deliveryOrderRepository.GetTotalOfTransfersByDate(dateInitial, dateFinish, a.ip_address);
                    }
                    if (result.Item1)
                    {
                        a.checkERP = true;
                        a.totalStore = result.Item2;
                    }
                    else
                    {
                        a.checkERP = false;
                    }

                });
                return list;
            }
            else
            {
                //Obtiene el total de procesadora con la ip
                var list = _deliveryOrderRepository.GetTotalsTransfersPRTByDateAndSite(dateInitial, dateFinish, site);
                list.ForEach(a =>
                {
                    //Obtiene todas las ordenes de compras de procesadora
                    var listAllPRT = _deliveryOrderRepository.GetAllDeliveryOrderBySite(dateInitial, dateFinish, site, listSites.Exists(bb => bb.SiteCode == site));
                    //Filtra por todas la tienda
                    var temp = listAllPRT.Where(b => b.destination_site == a.site_code).ToList();
                    var result = new Tuple<bool, decimal>(false, 0);

                    //Obtencion de todas las ordenes de compras
                    var pos = "";
                    if (temp.Count > 1)
                    {
                        temp.ForEach(d => pos += "'" + d.purchase_no + "',");
                    }
                    else
                    {
                        pos = "'" + temp[0].purchase_no.ToString() + "',";
                    }
                    pos = pos.Remove(pos.Length - 1);

                    //validar nuevos erp obtiene el monto total de la tienda
                    if (listSites.Exists(x => x.SiteCode == a.site_code))
                    {
                        //Obtiene el monto total que recibio la tienda con los numeros de ordenes
                        result = _deliveryOrderRepository.GetTotalOfTransfersNewERP(dateInitial, pos, a.ip_address, listSites.Find(x => x.SiteCode == a.site_code).NewErpDate, a.site_code);
                    }
                    else
                    {
                        //Obtiene el monto total que recibio la tienda con los numeros de ordenes
                        //result = _deliveryOrderRepository.GetTotalOfTransfersByPO(pos, a.ip_address);
                        result = _deliveryOrderRepository.GetTotalOfTransfersByDate(dateInitial, dateFinish, a.ip_address);
                    }
                    if (result.Item1)
                    {
                        a.checkERP = true;
                        a.totalStore = result.Item2;
                    }
                    else
                    {
                        a.checkERP = false;
                    }
                });
                return list;
            }
        }
        public List<ReportDetail> GetTransferDetails(DateTime dateInitial, DateTime dateFinish, string ip, string site)
        {
            var erp = new List<ReportDetail>();
            //Obtiene los movimientos de procesadora con montos
            var prt = _deliveryOrderRepository.GetTransferDetailsPRT(dateInitial, dateFinish, site);
            var pos = "";
            prt.ForEach(d => pos += "'" + d.purchase_no + "',");
            pos = pos.Remove(pos.Length - 1);

            var listSites = _deliveryOrderRepository.GetSitesNewERP();

            //valiudacion nuevos erp
            if (listSites.Exists(x => x.SiteCode == site))
            {
                //Obtiene los montos de la transferencias
                erp = _deliveryOrderRepository.GetTransfersDetailsNewERPByPO(pos, ip , site);
                var t = _deliveryOrderRepository.GetTransferDetailsOldERPBOX(dateInitial, dateFinish, ip, listSites.Find(x => x.SiteCode == site).NewErpDate);
                if (t != null && erp != null)
                    erp = erp.Concat(t).ToList();
            }
            else
            {
                ///*/*/*/* IMPORTANTE    Obtiene el los de detalles de las compras de erpbox , en el rango de fechas seleccionado
                erp = _deliveryOrderRepository.GetTransferDetailsERPBOX(dateInitial, dateFinish, ip);
            }

            //if (erp == null)
            //    return null;
            List<ReportDetail> list = new List<ReportDetail>();
            foreach (var p in prt)
            {
                var no = p.purchase_no;
                //Valida si existe en tienda la orden de compra de procesadora
                var exist = erp.Where(q => q.purchase_no == no).FirstOrDefault();
                if (exist != null)
                {
                    if (exist.status_erp == null || exist.status_erp == "9")
                        exist.status = "Recibido";
                    else if (exist.status_erp == "P" || exist.status_erp == "1" || exist.status_erp == "2")
                        exist.status = "Pendiente";
                    else if (exist.status_erp == "C")
                        exist.status = "Cancelado";
                    else
                        exist.status = exist.status_erp;
                    exist.do_no = p.do_no;
                    exist.purchase_no = no;
                    exist.date_prt = p.date_prt;
                    exist.purchase_total_prt = p.purchase_total_prt;
                    exist.status_prt = p.status_prt;
                    if (exist.purchase_comments == null)
                        exist.purchase_comments = "";
                    list.Add(exist);
                }
                else
                {
                    //Si no existe en tienda , solo esta en procesadora
                    var erpExist = _deliveryOrderRepository.CheckExistPoInPurchasesERP(no ?? 0, ip);
                    if (erpExist != null)
                    {
                        p.date_erp = erpExist.date_erp;
                        p.purchase_comments = erpExist.purchase_comments;
                        p.purchase_total_erp = erpExist.purchase_total_erp;
                        if (erpExist.status_erp == null)
                            p.status = "Recibido";
                        else if (erpExist.status_erp == "P")
                            p.status = "Pendiente";
                        else if (erpExist.status_erp == "C")
                            p.status = "Cancelado";
                        else
                            p.status = exist.status_erp;
                    }
                    else
                    {
                        p.date_erp = "";
                        p.purchase_comments = "";
                        p.status = "Solo en Procesadora";
                    }
                    list.Add(p);
                }
            }
            foreach (var e in erp)
            {
                var no = e.purchase_no;
                //Valida si existe la orden de compra de erpbox en procesadora
                var exist = prt.Where(q => q.purchase_no == no).FirstOrDefault();
                if (exist == null)
                {
                    var delivery = _deliveryOrderRepository.CheckExistPoInDeliveryOrder(no ?? 0, site);
                    if (delivery != null)
                    {
                        e.purchase_total_prt = delivery.purchase_total_prt;
                        e.do_no = delivery.do_no;
                        e.date_prt = delivery.date_prt;
                        if (e.status_erp == null)
                            e.status = "Recibido";
                        else if (e.status_erp == "P")
                            e.status = "Pendiente";
                        else if (e.status_erp == "C")
                            e.status = "Cancelado";
                        else
                            e.status = e.status_erp;
                    }
                    else
                    {
                        //Si no existe , solo esta en erpbox
                        if (e.purchase_comments == null)
                            e.purchase_comments = "";
                        e.do_no = "";
                        e.date_prt = "";
                        e.status = "Solo en ERP_BOX";
                    }
                    list.Add(e);
                }
            }
            return list;
        }
        public List<TransferItems> GetTransferProductsPRT(string do_no, string purchase_no, string ip_address, string site_code)
        {
            var result = new List<TransferItems>();
            if (do_no != "")
            {
                result = _deliveryOrderRepository.GetTransferItemsPRT(do_no, site_code);
            }
            else
            {
                result = _deliveryOrderRepository.GetTransferItemsERP(purchase_no, ip_address);
            }
            return result;
        }
    }
}
