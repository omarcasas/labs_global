﻿using App.DAL.Projects;
using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.BLL.Expense;
using App.BLL.Site;
using App.Common;
using App.DAL.Expenses;

namespace App.BLL.Projects
{
    public class ProjectItemBusiness
    {
        private ProjectItemRepository _projectItemRepository;
        private UserMasterRepository _userMasterRepository;
        private readonly ExpensesRepository _expensesRepository ;
        private SiteBusiness _siteConfigBusiness;
        private ProjectExpenseRepository _projectExpenseRepository;
        private ProjectReportRepository _ProjectReportRepository;
        public ProjectItemBusiness()
        {
            _userMasterRepository = new UserMasterRepository();
            _projectItemRepository = new ProjectItemRepository();
            _expensesRepository = new ExpensesRepository();
            _projectExpenseRepository = new ProjectExpenseRepository();
            _ProjectReportRepository = new ProjectReportRepository();
        }

        public List<ProjectHeaderModel> GetProjectsHeader(string User)
        {
            var number = _userMasterRepository.GetUserMasterByUsername(User);
            return _projectItemRepository.GetProjectsHeader(User, number.emp_no);
        }

        public List<ProjectHeaderModel> GetProjectsHeaderAll(string User)
        {
            var number = _userMasterRepository.GetUserMasterByUsername(User);
            return _projectItemRepository.GetProjectsHeaderAll(User, number.emp_no);
        }

        public List<ProjectLabelModel> GetProjectLabel(int IdProject, bool ActiveLabel,string User)
        {
            //var nuncaMasLoBuelboAhacer = _projectItemRepository.GetProjectLabel(IdProject, ActiveLabel, User).Select(x => new ProjectLabelModel
            //{
            //    ItemsCount = x.ItemsCount,
            //    Image = x.Image,
            //    TotalCost = _ProjectReportRepository.GetCostByLabel(x.IdLabelProject),
            //    IdLabelProject = x.IdLabelProject,
            //    IdProject = x.IdProject,
            //    IdLabelProjectDefault = x.IdLabelProjectDefault,
            //    FullName = x.FullName,
            //    EstimateCard = x.EstimateCard,
            //    Estimate = x.Estimate,
            //    EmpNo = x.EmpNo,
            //    Description = x.Description,
            //    ActiveLabel = x.ActiveLabel,
            //    userLeaderName = x.userLeaderName,
            //    usersLabel = x.usersLabel,
            //    usersNameLabels = x.usersNameLabels,
            //}).ToList();
            //return nuncaMasLoBuelboAhacer;
            return _projectItemRepository.GetProjectLabel(IdProject, ActiveLabel, User);
        }
        public List<ProjectItemModel> GetProjectItem(int IdLabelProject)
        {
            return _projectItemRepository.GetProjectItem(IdLabelProject);
        }

        public ProjectItemModel PostProjectItem(ProjectItemModel ProjectItem, string User)
        {

            if (ProjectItem.IdLabelProject > 0 && ProjectItem.Status > 0
                && ProjectItem.StartDate != null && ProjectItem.EndDate != null
                && ProjectItem.Estimate >= 0 && ProjectItem.estimate_usd>=0 && !string.IsNullOrWhiteSpace(ProjectItem.Description)
                && ProjectItem.ConsecutiveItem > 0)
            {
                //quite expense id porque cesar lo elimino de la tabla
                var Project = new PROJECT_ITEM()
                {
                    cuser = User,
                    cdate = DateTime.Now,
                    description = ProjectItem.Description,
                    consecutive_item = ProjectItem.ConsecutiveItem,
                    end_date = Convert.ToDateTime(ProjectItem.EndDate),
                    start_date = Convert.ToDateTime(ProjectItem.StartDate),
                    estimate = ProjectItem.Estimate,
                    id_label_project = ProjectItem.IdLabelProject,
                    program_id = "PROJ002.cshtml",
                    status = 1,
                    total_cost = 0,
                    estimate_usd = ProjectItem.estimate_usd,
                    total_cost_usd = 0,

                };
                ProjectItem.IdItem = _projectItemRepository.PostProjectItem(Project).item_id;
                return ProjectItem;
            }
            return new ProjectItemModel();
        }

        public int UpdateProjectItem(ProjectItemModel ProjectItem, string User)
        {
            return _projectItemRepository.UpdateProjectItem(ProjectItem, User);
        }

        public int UpdateProjectLabel(ProjectLabelModel Label, ProjectHeaderModel ProjectHeader, string User)
        {
            var LabelModel = _projectItemRepository.UpdateProjectLabel(Label.IdLabelProject, Label.Estimate, User,Label.estimate_usd);
            var Datetime = Convert.ToDateTime("1/1/0001 12:00:00AM");
            if (ProjectHeader.start_date.Date == Datetime.Date && LabelModel == 1)
            {
                ProjectHeader.status = 1;
                if (UpdateStatusProjectHeader(ProjectHeader, User) == 1)
                {
                    return 11;
                }
                return 108;
            }
            return LabelModel;
        }

        public int UpdateStatusProjectHeader(ProjectHeaderModel ProjectHeader, string User)
        {
            return _projectItemRepository.UpdateStatusProjectHeader(ProjectHeader.id_project, ProjectHeader.status, User);
        }


        public Result<Empty> ActionUploadXmls(string userName,UploadProjectXmls data)
        {
            var sitecode = "0094";
            var expense = new EXPENSE
            {
                
                site_code = sitecode,
                remark = "",
                reference_type = "nuevos_proyectos",
                
                cdate = DateTime.Now,
                cuser = userName,
                program_id = data.program_id
            };
            var r = Xml.GenXMLList(data.xmls, userName, data.program_id);
            if (!r.Ok) return Result.Error(r.Msg);
            var b = _expensesRepository.CreateExpense(expense, r.Data);
            if (b == null) return Result.Error("Error al guardar datos");
            var b2 = _projectExpenseRepository.AddXmlsAndTotalCost(userName,data,b);
            if (!b2) return Result.Error("Error al guardar datos");
            
            return Result.OK();

        }
    }
}
