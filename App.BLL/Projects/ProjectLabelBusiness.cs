﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.DAL.Projects;
using App.Entities.ViewModels.Projects;
using App.Entities;

namespace App.BLL.Projects
{
    public class ProjectLabelBusiness
    {
        private readonly ProjectLabelRepository _projectLabelRepo = new ProjectLabelRepository();
        private readonly ProjectExpenseRepository _projectExpenseRepo = new ProjectExpenseRepository();
        private readonly ProjectReportRepository _projectRepo = new ProjectReportRepository();
        private readonly UserMasterRepository _userMasterRepository = new UserMasterRepository();

        public List<ProjectDescriptionModel> GetAllLabelsDefaultByCategory(int id)
        {
            var x = _projectLabelRepo.GetAllLabelsDefaultByCategory(id);
            return x;
        }


        public Result<List<RTPProjectLabelModel>> ReadyToPayLabels(string userName, int projectId)
        {
            var user = _userMasterRepository.GetUserMasterByUsername(userName);
            if (user == null) return Result.SF();
            var r = _projectLabelRepo.ReadyToPayLabels(projectId,user.emp_no,userName);
            return Result.OK(r);
        }

        public List<ProjectUserNotification> GetAllUser(int IdProject, int IdLabel, bool allUsers)
        {
            return _projectLabelRepo.GetAllUserEmail(IdProject, IdLabel, allUsers);
        }

        public ProjectItemHistoryHeaderModel GetLabelInfo(int id_label_label)
        {
            var info = _projectLabelRepo.GetLabelInfo(id_label_label);
            info.total_cost = _projectRepo.GetCostByLabel(id_label_label,"MXN");
            info.total_cost_usd = _projectRepo.GetCostByLabel(id_label_label,"USD");
            return info;
        }

        public List<ProjectUserNotification> GetAllUserLabel(int id_proyect)
        {
            return _projectLabelRepo.GetAllUserLabel(id_proyect);
        }

        public List<ProjectUserNotification> GetAllUserLeaderLabel(int id_proyect)
        {
            return _projectLabelRepo.GetAllUserLeaderLabel(id_proyect);
        }
        public string DeleteLabelById(string desc, int idProj , int idLabel, string user)
        {
            var exist = _projectExpenseRepo.ExistExpenseByLabel(idLabel);
            if (exist)
            {
                return "No se puede eliminar la etiqueta ya que tiene un gasto";
            }
            else
            {
                var update = _projectLabelRepo.DeleteLabelByHeader(desc, idProj, user);
                if (update)
                {
                    return "OK";
                }else
                {
                    return "Error desconocido , contacta a sistemas";
                }
            }
        }
        public string AddLabelById(ProjectLabelModel model , int idHeader , int idCategory , string user)
        {
            var exist = _projectLabelRepo.existLabelDefault(idCategory, model.Description);
            if (exist)
            {
                _projectLabelRepo.updateLabelDefault(idCategory,model.Description, user);
            }
            else
            {
                //CREATE LABEL DEFAULT
                PROJECT_LABEL_DEFAULT modelLabelDefault = new PROJECT_LABEL_DEFAULT
                {
                    id_category = idCategory,
                    description = model.Description,
                    active_flag = true,
                    cuser = user,
                    program_id = "PROJ001.cshtml",
                    cdate = DateTime.Now
                };
                var labelDefault = _projectLabelRepo.AddLabelDefault(modelLabelDefault);
                if (!labelDefault) { return "Error Inesperado , Contacte a Sistemas"; }
            }
            PROJECT_LABEL modelLabel = new PROJECT_LABEL
            {
                id_project = idHeader,
                emp_no = model.EmpNo,
                id_label_project_default = model.Description,
                cuser = user,
                active_label = true,
                program_id = "PROJ001.cshtml",
                cdate = DateTime.Now
            };
            var labelH = _projectLabelRepo.AddLabel(modelLabel);
            if (labelH == 0) { return "Error Inesperado , Contacte a Sistemas"; }
            if (model.usersLabel != null)
            {
                foreach (var userL in model.usersLabel)
                {
                    PROJECT_LABEL_USER label_user = new PROJECT_LABEL_USER
                    {
                        emp_no = userL.ToString(),
                        id_label_project = labelH,
                        cuser = user,
                        program_id = "PROJ001.cshtml",
                        cdate = DateTime.Now
                    };
                    var label_user_add = _projectLabelRepo.AddLabelUser(label_user);
                }
            }
            return "OK";
        }
        public bool UpdateLabelUsers(List<string> users, int idLabel, string user)
        {
            //cambiar todos los status a 0
            _projectLabelRepo.UpdateAllUsersLabel(idLabel);
            if (users != null)
            {
                foreach (var u in users)
                {
                    //checar si existe
                    var exist = _projectLabelRepo.ExistUserInLabel(idLabel, u);
                    if (exist)
                    {
                        _projectLabelRepo.UpdateLabelUserFlag(idLabel, u, user);
                    }
                    else
                    {
                        PROJECT_LABEL_USER label_user = new PROJECT_LABEL_USER
                        {
                            emp_no = u,
                            id_label_project = idLabel,
                            cuser = user,
                            active = true,
                            program_id = "PROJ001.cshtml",
                            cdate = DateTime.Now
                        };
                        _projectLabelRepo.AddLabelUser(label_user);
                    }
                }
            }
            return true;
        }
        public List<ProjectUserNotification> GetUserAllEmail(int IdProject)
        {
            return _projectLabelRepo.GetUserAllEmail(IdProject);
        }
    }
}
