﻿using App.DAL.Projects;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Projects
{
    public class ProjectReportBusiness
    {
        private readonly ProjectReportRepository _projectReportRepository;
        private readonly UserMasterBusiness _userMasterBusiness;

        public ProjectReportBusiness()
        {
            _projectReportRepository = new ProjectReportRepository();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public List<ProjectModel> GetProjectsReport(string user)
        {
            var model = _projectReportRepository.GetProjectsReport(user);

            if (model != null)
            {
                foreach (var project in model)
                {
                    project.TotalCost = GetTotalCostByProject(project.ProjectId,"MXN");
                    project.total_cost_usd = GetTotalCostByProject(project.ProjectId,"USD");
                    
                }
            }

            return model;
        }

        public List<ItemModel> GetProjectDetailReport(int ProjectId, string LabelId, string User)
        {
            return _projectReportRepository.GetProjectDetailReport(ProjectId, LabelId, User);
        }

        public List<ProjectModel> GetProjectsReport(int CategoryId, string LabelId, DateTime? StartDate, DateTime? EndDate, string User)
        {
            var model = _projectReportRepository.GetProjectsReport(CategoryId, LabelId, StartDate, EndDate, User);

            if (model != null)
            {
                foreach (var project in model)
                {
                    project.TotalCost = GetTotalCostByProject(project.ProjectId,"MXN");
                    project.total_cost_usd = GetTotalCostByProject(project.ProjectId,"USD");
                }
            }

            return model;
        }

        public ProjectModel GetProjectDetails(int ProjectId, string User)
        {
            var model = _projectReportRepository.GetProjectDetails(ProjectId, User);

            if (model != null)
            {
                var userCreated = _userMasterBusiness.GetUserMasterByUsername(model.CreatedBy);
                model.CreatedBy = userCreated.first_name + " " + userCreated.last_name;

                model.TotalCost = _projectReportRepository.GetTotalCostByProject(ProjectId,"MXN");
                model.TodayCost = _projectReportRepository.GetTotalCostByDay(ProjectId,"MXN");
                model.CostByWeek = _projectReportRepository.GetTotalCostByWeek(ProjectId,"MXN");
                
                model.total_cost_usd = _projectReportRepository.GetTotalCostByProject(ProjectId,"USD");
                model.today_cost_usd = _projectReportRepository.GetTotalCostByDay(ProjectId,"USD");
                model.cost_by_week_usd = _projectReportRepository.GetTotalCostByWeek(ProjectId,"USD");
                
                model.Items = _projectReportRepository.GetProjectItems(ProjectId);
                model.CategoryUsers = _projectReportRepository.GetCategoryUsers(ProjectId);
            }

            if (model.Items != null)
            {
                foreach (var item in model.Items)
                {
                    var emp = _userMasterBusiness.GetUserMasterByUsername(item.CreatedBy);
                    if (emp != null)
                    {
                        item.CreatedBy = emp.first_name + " " + emp.last_name;
                    }
                }
            }

            if (model.CategoryUsers != null)
            {
                foreach (var user in model.CategoryUsers)
                {
                    var emp = _userMasterBusiness.GetUserMasterByEmployeeNumber(user.EmployeeNumber);
                    if (emp != null)
                    {
                        user.Name = emp.first_name + " " + emp.last_name;
                        user.Photo = emp.photo;
                    }
                }
            }

            return model;
        }

        public decimal GetTotalCostByProject(int id_proyect,string currency)
        {
            return _projectReportRepository.GetTotalCostByProject(id_proyect,currency);
        }
    }
}
