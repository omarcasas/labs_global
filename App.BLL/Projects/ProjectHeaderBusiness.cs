﻿using App.DAL.Projects;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.Entities;

namespace App.BLL.Projects
{
    public class ProjectHeaderBusiness
    {
        private ProjectHeaderRepository _repository;
        private UserMasterRepository _userMasterRepository = new UserMasterRepository();
        private ProjectCategoryRepository _projectCategoryRepository = new ProjectCategoryRepository();
        private ProjectLabelRepository _projectLabelRepository = new ProjectLabelRepository();
        private ProjectExpenseRepository _projectExpenseRepository = new ProjectExpenseRepository();
        private ProjectReportRepository _projectReportRepository = new ProjectReportRepository();
        private ProjectItemRepository _projectItemRepository = new ProjectItemRepository();
        public ProjectHeaderBusiness()
        {
            _repository = new ProjectHeaderRepository();
        }

        public List<ProjectHeaderModel> GetActiveProjectBasic()
        {
            return _repository.GetActiveProjectBasic();
        }

        public List<ProjectHeaderModel> GetAllProjectBasic()
        {
            return _repository.GetAllProjectBasic();
        }

        public Result<List<ProjectHeaderModel>> ActiveProjects(string userName)
        {
            var u = _userMasterRepository.GetUserMasterByUsername(userName);
            if(u==null) return Result.SF();
            return Result.OK(_projectItemRepository.GetProjectsHeaderAllExpenses(userName,u.emp_no));
        }
        public Tuple<bool,string> AddProjectHeader(ProjectHeaderModel model , string user)
        {
            var page = "PROJ001.cshtml";
            var labelDefault = true;
            
            PROJECT_HEADER modelHeader = new PROJECT_HEADER
            {
                compromise_date = model.compromise_date,
                estimate = model.estimate,
                title = model.title,
                description = model.description,
                status = 0,
                id_category = model.id_category,
                cuser = user,
                program_id = page,
                cdate = DateTime.Now,
                estimate_usd = model.estimate_usd

            };
            //ADD PROJECT_HEADER
            var id_header = _repository.AddProjectHeader(modelHeader);
            if(id_header != 0)
            {
                var categoryUser = true;
                //ADD LOOP OF CATEGORY USERS
                foreach(var userCat in model.users_category)
                {
                    PROJECT_CATEGORY_USER modelCategoryUser = new PROJECT_CATEGORY_USER
                    {
                        id_category = model.id_category,
                        id_project = id_header,
                        emp_no = userCat.ToString(),
                        cuser = user,
                        program_id = page,
                        cdate = DateTime.Now

                    };
                categoryUser = _projectCategoryRepository.AddCategoryUsers(modelCategoryUser);
                }
                if (categoryUser)
                {

                    //ALL FLAGS DISABLED BY ID_CATEGORY
                    _projectCategoryRepository.UpdateAllDefault(model.id_category);
                    //ADD LOOP LABEL DEFAULT Y LABEL
                    foreach(var label in model.label)
                    {
                        //IF ID CATEGORY AND DESCRIPTION EXIST SET ACTIVE FLAG TRUE
                        var exist = _projectLabelRepository.existLabelDefault(model.id_category, label.Description);
                        if (exist)
                        {
                            _projectLabelRepository.updateLabelDefault(model.id_category, label.Description , user);
                        }
                        else
                        {
                            //CREATE LABEL DEFAULT
                            PROJECT_LABEL_DEFAULT modelLabelDefault = new PROJECT_LABEL_DEFAULT
                            {
                                id_category = model.id_category,
                                description = label.Description,
                                active_flag = true,
                                cuser = user,
                                program_id = page,
                                cdate = DateTime.Now
                            };
                            labelDefault = _projectLabelRepository.AddLabelDefault(modelLabelDefault);
                            if(!labelDefault) { _repository.CancelHeaderByError(id_header); return new Tuple<bool, string>(false, "Error Inesperado , Contacte a Sistemas") ; }
                        }
                        PROJECT_LABEL modelLabel = new PROJECT_LABEL
                        {
                            id_project = id_header,
                            emp_no = label.EmpNo,
                            id_label_project_default = label.Description,
                            cuser = user,
                            active_label = true,
                            program_id = page,
                            cdate = DateTime.Now
                        };
                        var labelH = _projectLabelRepository.AddLabel(modelLabel);
                        if(labelH == 0) { _repository.CancelHeaderByError(id_header); return new Tuple<bool, string>(false, "Error Inesperado , Contacte a Sistemas"); }
                        if(label.usersLabel != null)
                        {
                            foreach (var userL in label.usersLabel)
                            {
                                PROJECT_LABEL_USER label_user = new PROJECT_LABEL_USER
                                {
                                    emp_no = userL.ToString(),
                                    id_label_project = labelH,
                                    cuser = user,
                                    active = true,
                                    program_id = page,
                                    cdate = DateTime.Now
                                };
                                var label_user_add = _projectLabelRepository.AddLabelUser(label_user);
                            }
                        }              
                    }                   
                }
            }else
            {               
                 return new Tuple<bool, string>(false, "Error Inesperado , Contacte a Sistemas"); ;
            }
             return new Tuple<bool, string>(true, id_header.ToString());
        }
        
        public Tuple<bool,string> UpdateProjectHeader(ProjectHeaderModel model , string user)
        {
            var page = "PROJ001.cshtml";
            model.compromise_date = DateTime.ParseExact(model.compromise_date_str, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var labelDefault = true;
            var modelHeader = _repository.GetHeaderById(model.id_project);
            if(modelHeader == null) return new Tuple<bool, string>(false,"No se encontro proyecto");
            modelHeader.compromise_date = model.compromise_date;
            modelHeader.estimate = model.estimate;
            modelHeader.title = model.title;
            modelHeader.description = model.description;
            modelHeader.estimate_usd = model.estimate_usd;
            var b = _repository.UpdateHeader(modelHeader);
            if(!b) return new Tuple<bool, string>(false,"No error al actualizar proyecto");
            var users = model.users_category.Select(e => new PROJECT_CATEGORY_USER
            {
                id_category = model.id_category,
                id_project = model.id_project,
                emp_no = e,
                cuser = user,
                program_id = page,
                cdate = DateTime.Now

            });
            _projectCategoryRepository.UpdateCategoryUsers(model.id_project, users);
            
             return new Tuple<bool, string>(true, modelHeader.id_project.ToString()); 
        }
        public List<ProjectHeaderModel> GetHeaderByUser(string user)
        {
            return _repository.GetHeaderByUser(user);
        }
        public ProjectHeaderModel GetDetailsByHeader(int id)
        {
            var model =  _repository.GetDetailsByHeader(id);
            model.users_category = _projectCategoryRepository.GetUsersCategoryById(id);
            model.label = _projectLabelRepository.GetLabelById(id);
            foreach (var lab in model.label)
            {
                lab.usersLabel = _projectLabelRepository.GetUsersLabelById(lab.IdLabelProject);
                lab.usersNameLabels = _projectLabelRepository.GetNameUsersLabelById(lab.IdLabelProject);
                lab.Image = new byte[0];
            }
            return model;
        }
        public string UpdateStatusHeader(int id , int status , string user)
        {
            if(status == 8)
            {
                var exist = _projectExpenseRepository.ExistExpenseByProject(id);
                if (exist)
                {
                    return "Ya existe un gasto en el proyecto no se puede cancelar";
                }
                else
                {
                    var update = _repository.UpdateStatusHeaderCancel(id, status, user);
                    if (update)
                    {
                        return "OK";
                    }
                    else
                    {
                        return "Error desconocido , contacta a sistemas";
                    }
                }
            }else
            {
                var cost_mxn =  _projectReportRepository.GetTotalCostByProject(id,"MXN");
                var cost_usd =  _projectReportRepository.GetTotalCostByProject(id,"USD");
                var update = _repository.UpdateStatusHeaderClose(id, status, cost_mxn,cost_usd, user);
                if (update)
                {
                    return "OK";
                }
                else
                {
                    return "Error desconocido , contacta a sistemas";
                }
            }
           
           
        }
    }
}
