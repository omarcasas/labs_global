﻿using App.DAL.Projects;
using App.Entities;
using App.Entities.ViewModels.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Projects
{
    public class ProjectCategoryBusiness
    {
        private ProjectCategoryRepository _repository = new ProjectCategoryRepository();

        public List<ProjectCategoryModel> GetGetAllCategory()
        {
            return _repository.GetAllCategory();
        }
        public PROJECT_CATEGORY AddCategory(string desc, string user)
        {
            PROJECT_CATEGORY cat = new PROJECT_CATEGORY
            {
                description = desc,
                cuser = user,
                cdate = DateTime.Now,
                program_id = "PROJ001.cshtml",
                status = true
            };
            return _repository.AddCategory(cat);
        }

        public List<ProjectUserNotification> GetAllUserCategory(int id_category, int id_proyect)
        {
            return _repository.GetAllUserCategory(id_category, id_proyect);
        }
    }
}
