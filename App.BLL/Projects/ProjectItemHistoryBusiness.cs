﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.DAL.Projects;
using System.Threading.Tasks;
using App.Entities.ViewModels.Projects;
using App.Entities;

namespace App.BLL.Projects
{
    public class ProjectItemHistoryBusiness
    {
        private ProjectItemHistoryRepository _repository;
        private ProjectReportBusiness _repositoryReport;
        public ProjectItemHistoryBusiness()
        {
            _repository = new ProjectItemHistoryRepository();
            _repositoryReport = new ProjectReportBusiness();
        }

        public List<ProjectItemHistoryHeaderModel> GetItemHistoryById(int id_project)
        {
            var labels = _repository.GetItemHistoryById(id_project);
            if (labels.Count() > 0)
            {
                labels[0].label_emp_no_user_name = _repository.GetProjectHeaderUser(labels[0].id_proyect);
                labels[0].total_cost = _repositoryReport.GetTotalCostByProject(labels[0].id_proyect,"MXN");
                labels[0].total_cost_usd = _repositoryReport.GetTotalCostByProject(labels[0].id_proyect,"USD");
            }
            foreach (var label in labels)
            {
                
                List<ProjectItemHistoryModel> list = new List<ProjectItemHistoryModel>();
                list = GetLastComentByLabel(label.id_label);
                if (list.Count() == 0)
                    list = GetLastComentBasicByLabel(label.id_label);
                label.table_item_history = list;
            }
            return labels;
        }

        public List<ProjectItemHistoryModel> GetLastComentByLabel(int id_label)
        {
            return _repository.GetLastComentByLabel(id_label);
        }
        public List<ProjectItemHistoryModel> GetLastComentBasicByLabel(int id_label)
        {
            return _repository.GetLastComentBasicByLabel(id_label);
        }

        public string GetCommentsPdfByHistory(int id_history_detail)
        {
            return _repository.GetCommentsPdfByHistory(id_history_detail);
        }


        public List<ProjectItemHistoryModel> GetAllCommentsByItem(int id_item)
        {
            var history = _repository.GetAllCommentsByItem(id_item);
            if ( history != null && history.Count() > 0)
            {
                foreach (var item in history)
                {
                    item.table_attached = GetDocumentsByHistory(item.id_history);
                }
            } else
                history = _repository.GetAllCommentsBasicByItem(id_item);
            return history;
        }

        public List<ProjectItemHistoryDetail> GetDocumentsByHistory(int id_history)
        {
            return _repository.GetDocumentsByHistory(id_history);
        }

        public bool InsertCommentsWithAttached(int id_item, string comment, string user, List<ProjectItemHistoryDetailAction> attached)
        {
            PROJECT_ITEM_HISTORY model_history = new PROJECT_ITEM_HISTORY
            {
                id_item = id_item,
                commentary = comment,
                cuser = user,
                cdate = DateTime.Now,
                program_id = "PROJ003.cshtml"
            };
            int id_history = _repository.InsertComment(model_history);
            if (id_history != 0)
            {
                if(attached != null)
                {
                    foreach (var item in attached)
                    {
                        PROJECT_ITEM_HISTORY_DETAIL model_detail = new PROJECT_ITEM_HISTORY_DETAIL
                        {
                            id_history = id_history,
                            document_image = null,
                            document_pdf = null,
                            cuser = user,
                            cdate = DateTime.Now,
                            program_id = "PROJ003.cshtml"
                        };
                        if (item.file_type == "PDF")
                            model_detail.document_pdf = item.file_base64;
                        else
                            model_detail.document_image = item.file_base64;
                        bool result = _repository.InsertAttached(model_detail);
                        if (!result) { return false; }
                    }
                }
                return true;
            }
            else
                return false;
        }
    }
}
