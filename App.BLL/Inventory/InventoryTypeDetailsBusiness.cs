﻿using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Inventory
{
    public class InventoryTypeDetailsBusiness
    {
        private InventoryTypeDetailsRepository _InventoryTypeDetailsRepo;

        public InventoryTypeDetailsBusiness()
        {
            _InventoryTypeDetailsRepo = new InventoryTypeDetailsRepository();
        }

        public List<InventoryTypeDetailsModel> GetInventoryTypeDatails(int IdInventoryType)
        {
            if (IdInventoryType > 0)
            {
                return _InventoryTypeDetailsRepo.GetInventoryTypeDatails(IdInventoryType);
            }
            return new List<InventoryTypeDetailsModel>();
        }

        public InventoryTypeDetailsModel EditInventoryDetails(InventoryTypeDetailsModel InventoryTypeDetails,string User)
        {
            if (InventoryTypeDetails.IdInventoryType > 0 && InventoryTypeDetails.LevelMaClass > 0)
            {
                return _InventoryTypeDetailsRepo.EditInventoryDetails(InventoryTypeDetails, User);
            }
            return new InventoryTypeDetailsModel();
        }

        public InventoryTypeDetailsModel CreateInventoryTypeDatails(InventoryTypeDetailsModel InventoryTypeDetails, string User)
        {
            if (InventoryTypeDetails.IdInventoryType > 0 && InventoryTypeDetails.LevelMaClass > 0)
            {
                return _InventoryTypeDetailsRepo.CreateInventoryTypeDatails(InventoryTypeDetails, User);
            }
            return new InventoryTypeDetailsModel();
        }
    }
}
