﻿using App.BLL.Site;
using App.DAL.PRT;
using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using App.Entities;

namespace App.BLL.Inventory
{
    public class InventoryCalendarBusiness
    {
        private InventoryCalendarRepository _InventoryCalendarRepo;
        private DeliveryOrderRepository _deliveryRepo;

        //constructor
        public InventoryCalendarBusiness()
        {
            _InventoryCalendarRepo = new InventoryCalendarRepository();
            _deliveryRepo = new DeliveryOrderRepository();

        }
        public List<InventoryCalendarModel> getInventoryCalendarDatesBetween(DateTime Date1 , DateTime Date2)
        {
           
            return _InventoryCalendarRepo.getInventoryCalendarDatesBetween(Date1, Date2);

        }
        public bool UpdateInventoryCalendar(InventoryCalendarModel model)
        {
            var check = false;
            var flag = _InventoryCalendarRepo.UpdateInventoryCalendar(model);
            if (flag > 0) check = true;
            return check;
        }
        public bool UpdateInventoryCalendarOnlyDate(InventoryCalendarModel model, string user, string program)
        {
            var check = false;
            model.uuser = user;
            var flag = _InventoryCalendarRepo.UpdateInventoryCalendarOnlyDate(model, user, program);
            if (flag > 0) check = true;
            return check;
        }
        public bool InsertInventoryCalendar(InventoryCalendarModel model)
        {
            var check = false;
            var flag = 0;
            //INSERTA EN TODAS LAS TIENDAS
            if (model.site_code == "0000")
            {
                var sitesNewErp = _deliveryRepo.GetSitesNewERP();
                foreach(var site in sitesNewErp)
                {
                    var insertCalendar = model;
                    insertCalendar.site_code = site.SiteCode;
                    flag = _InventoryCalendarRepo.InsertInventoryCalendar(insertCalendar);
                }
            }else //INSERTA SOLO UNA TIENDA
            {
                flag = _InventoryCalendarRepo.InsertInventoryCalendar(model);
            }
            if (flag > 0) check = true;
            return check;
        }
        public bool AddInventoryCyclic(InventoryCalendarModel Inventory, List<string> part_numbers, string user)
        {
            INVENTORY_CALENDAR inv_cal = new INVENTORY_CALENDAR();
            inv_cal.inventory_name = Inventory.title;
            inv_cal.inventory_description = Inventory.description;
            inv_cal.site_code = Inventory.site_code;
            inv_cal.calendar_date = Inventory.start;
            inv_cal.calendar_status = 0;
            inv_cal.cdate = DateTime.Now;
            inv_cal.cuser = user;
            inv_cal.type = 7;
            inv_cal.flag_cyclic = true;
            inv_cal.program_id = "INVE003";
            //var id_calendar = 1;
            var id_calendar = _InventoryCalendarRepo.AddInventoryCyclic(inv_cal);
            if (id_calendar == 0)
                return false;
            foreach(var i in part_numbers)
            {
                INVENTORY_CYCLIC inv_cyc = new INVENTORY_CYCLIC();
                inv_cyc.id_calendar = id_calendar;
                inv_cyc.part_number = i;
                inv_cyc.site = Inventory.site_code;
                inv_cyc.flag_active = true;
                inv_cyc.cuser = user;
                inv_cyc.cdate = DateTime.Now;
                inv_cyc.program_id = "INVE003";
                _InventoryCalendarRepo.AddInventoryCyclicPartNumber(inv_cyc);
            }

            return true;
        }
        public bool CancelInventory(int idCalendar , string user)
        {
            var InvCalendar = _InventoryCalendarRepo.CheckInventoryStatus0(idCalendar);
            var r = false;
            if(InvCalendar != null)
            {
                InvCalendar.calendar_status = 8;
                InvCalendar.udate = DateTime.Now;
                InvCalendar.uuser = user;
                InvCalendar.program_id = "INVE001";
                r =_InventoryCalendarRepo.CancelInventory(InvCalendar);
            }
            return r;
        }
    }
}
