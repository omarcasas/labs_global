﻿using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Inventory
{
    public class InventoryTypeBusiness
    {
        private InventoryTypeRepository _inventoryTypeRepository;
        private InventoryTypeDetailsBusiness _InventoryTypeDetailsBusiness;
        public InventoryTypeBusiness()
        {
            _inventoryTypeRepository = new InventoryTypeRepository();
            _InventoryTypeDetailsBusiness = new InventoryTypeDetailsBusiness();
        }

        public List<InventoryTypeModel> GetAllInventoryType()
        {
            return _inventoryTypeRepository.GetAllInventoryType();
        }
        public List<InventoryTypeModel> GetAllInventoryTypeActives()
        {
            return _inventoryTypeRepository.GetAllInventoryTypeActives();
        }
        public InventoryTypeModel SaveInventory(InventoryTypeModel Inventory, List<InventoryTypeDetailsModel> InventoryTypeDetails,string User)
        {
            if (Inventory.Day.Trim() != ""  && Inventory.InventoryName.Trim() != "" && Inventory.TypeReview.Trim() != "" && Inventory.CloseTime != null)
            {
                var InventoryType = _inventoryTypeRepository.SaveInventory(Inventory, User);
                foreach (var item in InventoryTypeDetails)
                {
                    if (item.ActiveDetails)
                    {
                        item.IdInventoryType = InventoryType.IdInventoryType;
                        var InventoryDetails = _InventoryTypeDetailsBusiness.CreateInventoryTypeDatails(item, User);
                        if (InventoryDetails.IdInventoryType > 0)
                            continue;
                        else
                            return new InventoryTypeModel();
                    }
                }
                return InventoryType;
            }
            else
            {
                return new InventoryTypeModel();
            }
        }

        public InventoryTypeModel EditInventory(InventoryTypeModel Inventory, List<InventoryTypeDetailsModel> InventoryTypeDetails, string User)
        {
            if (Inventory.Day.Trim() != "" && Inventory.InventoryName.Trim() != "" && Inventory.TypeReview.Trim() != "" && Inventory.IdInventoryType > 0 && Inventory.CloseTime != null)
            {
                var InventoryType = _inventoryTypeRepository.EditInventory(Inventory, User);
                foreach (var item in InventoryTypeDetails)
                {
                    if (item.IsCreate)
                    {
                        _InventoryTypeDetailsBusiness.EditInventoryDetails(item, User);
                    }
                    else
                    {
                        item.IdInventoryType = Inventory.IdInventoryType;
                        _InventoryTypeDetailsBusiness.CreateInventoryTypeDatails(item, User);
                    }
                }
                return InventoryType;
            }
            else
            {
                return new InventoryTypeModel();
            }
        }

        public List<InventoryTypeDetailsModel> GetInventoryTypeDatails(int IdInventoryType)
        {
            return _InventoryTypeDetailsBusiness.GetInventoryTypeDatails(IdInventoryType);
        }


        public bool IsActiveInventory(int Inventory,bool Active, string User)
        {
            if (Inventory > 0)
            {
                return _inventoryTypeRepository.IsActiveInventory(Inventory, Active, User);
            }
            return false;
        }
    }
}
