﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.InternalRequirement;
using App.DAL.InternalRequirement;
using App.Entities;

namespace App.BLL.InternalRequirement
{
    public class InternalRequirementBusiness
    {
        private InternalRequirementRepository _internalRequirementRepository;

        public InternalRequirementBusiness()
        {
            _internalRequirementRepository = new InternalRequirementRepository();
        }
        public List<InternalRequirementModel> getAllItemsCreditors()
        {
            var items = _internalRequirementRepository.GetAllItemsCreditors();
            return items;
        }
        public List<InternalRequirementModel> SearchItemsCreditors(string search)
        {
            var items = _internalRequirementRepository.SearchItemsCreditors(search);
            return items;
        }
        public List<InternalRequirementDetailPOModel> GetRequirementsAll()
        {
            var items = _internalRequirementRepository.GetRequirementsAll();
            decimal? total = 0;
            decimal quantity = 0;
            foreach (var item in items)
            {
                quantity = quantity + item.quantity_total;
                if (item.total_total != null)
                    total = total + item.total_total;
                else
                    total = total + 0;
            }
            items[0].quantity = quantity;
            items[0].total = total;
            items[0].folio = _internalRequirementRepository.GetCountRequirementFolios();

            return items;

        }

        public int GetCountRequirementFolios()
        {
            return _internalRequirementRepository.GetCountRequirementFolios();
        }


        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplier(int supplier_id)
        {
            var items = _internalRequirementRepository.GetRequirementsDetailsBySupplier(supplier_id);
            int i = 0;
            foreach (var item in items)
            {
                item.price = 5;
                item.total = item.quantity * item.price;
                item.folio = 2;
            }

            return items;
        }
        public bool AddInternalRequierement(List<InternalRequirementModel> list, string user, string site)
        {
            try
            {
                INTERNAL_REQUIREMENT_HEADER header = new INTERNAL_REQUIREMENT_HEADER();
                header.site_code = site;
                header.cdate = DateTime.Now;
                header.requirement_status = 0;
                header.cuser = user;
                header.program_id = "PURCH020.cshtml";
                header.requirement_type = "INTERNO";
                var id = _internalRequirementRepository.AddInternalRequirementHeader(header);
                if (id != 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        INTERNAL_REQUIREMENT_DETAIL detail = new INTERNAL_REQUIREMENT_DETAIL();
                        detail.cdate = DateTime.Now;
                        detail.cuser = user;
                        detail.folio = id;
                        detail.supplier_part_number_creditors = list[i].id_item_supplier;
                        detail.quantity = list[i].quantity;
                        detail.program_id = "PURCH020.cshtml";
                        detail.price_base = list[i].price;
                        _internalRequirementRepository.AddInternalRequirementDetail(detail);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public List<InternalRequirementPOModel> GetAllRequirementsHeaderBySite(string site)
        {
            var list = _internalRequirementRepository.GetAllRequirementsHeaderBySite(site);
            return list;
        }
        public List<InternalRequirementDetailPOModel> GetAllRequirementsDetails(string folio)
        {
            var list = _internalRequirementRepository.GetAllRequirementsDetails(folio);
            return list;
        }

        public List<InternalRequirementDetailPOModel> GetSentRequirementsByDates(DateTime BeginDate, DateTime EndDate, string BuyerDivision)
        {
            var items = _internalRequirementRepository.GetSentRequirementsByDates(BeginDate, EndDate);

            foreach (var item in items)
                item.total = _internalRequirementRepository.GetSumTotalByFolio(item.folio, BuyerDivision);

            return items;
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplierBySite(int supplier_id, string site_code)
        {
            return _internalRequirementRepository.GetRequirementsDetailsBySupplierBySite(supplier_id, site_code);
        }
        public List<InternalRequirementDetailPOModel> GetRequirementsInSupplierAll()
        {
            return _internalRequirementRepository.GetRequirementsInSupplierAll();
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsInSiteAll()
        {
            return _internalRequirementRepository.GetRequirementsInSiteAll();
        }

        public InternalRequirementPOModel GetInfoRequirement(int folio)
        {
            return _internalRequirementRepository.GetInfoRequirement(folio);
        }
        //public List<InternalRequirementDetailPOModel> GetRequirementsHeaderAll()
        //{
        //    return _internalRequirementRepository.GetRequirementsHeaderAll();
        //}










































        //Pasillas
        public List<InternalRequirementDetailPOModel> GetRequirementsAllSites(string BuyerDivision)
        {
            var items = _internalRequirementRepository.GetRequirementsAllSites(BuyerDivision);
            decimal quantity = 0;
            if (items.Count() > 0)
            {
                foreach (var item in items)
                {
                    quantity = quantity + item.quantity;
                    item.total = _internalRequirementRepository.GetSumTotalByStore(item.id_supplier);
                }
                items[0].folio = _internalRequirementRepository.GetRequirementcountFolios();
            }

            return items;
        }

        public List<InternalRequirementDetailPOModel> GetRequirementDetailsBySupplier(int supplier_id, string BuyerDivision)
        {
            var items = _internalRequirementRepository.GetRequirementDetailsBySupplier(supplier_id, BuyerDivision);
            foreach (var item in items)
            {
                item.total = item.quantity * item.price;
            }

            return items;
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsHeaderAll(string BuyerDivision)
        {
            var items = _internalRequirementRepository.GetRequirementsHeaderAll(BuyerDivision);
            foreach (var item in items)
            {
                item.total = _internalRequirementRepository.GetSumTotalByFolio(item.folio, BuyerDivision);
            }
            return items;
        }

        public List<InternalRequirementDetailPOModel> GetSentRequirements(string BuyerDisision)
        {
            var items = _internalRequirementRepository.GetSentRequirements(BuyerDisision);
            foreach (var item in items)
            {
                item.total = _internalRequirementRepository.GetSumTotalByFolio(item.folio, BuyerDisision);
            }
            return items;
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsAll(string folio, string BuyerDivision)
        {
            return _internalRequirementRepository.GetRequirementsDetailsAll(folio, BuyerDivision);
        }

        public List<InternalRequirementModel> getItemsCreditorsAll(string BuyerDivision)
        {
            return _internalRequirementRepository.GetItemsCreditorsAll(BuyerDivision);
        }

        public List<InternalRequirementModel> getItemsCreditorsByFolio(int folio, string BuyerDivision)
        {
            return _internalRequirementRepository.getItemsCreditorsByFolio(folio, BuyerDivision);
        }

        public int UpdateItemsRequirement(List<InternalRequirementModel> items, string uuser, int folio)
        {
            foreach (var item in items)
            {
                var resutl = _internalRequirementRepository.UpdateItemsRequirement(item, uuser, folio);
                if (resutl == 0)
                    return 0;
            }
            return 1;
        }

        public int DeleteInternalHeaderFolio(int folio, string uuser)
        {
            return _internalRequirementRepository.DeleteInternalHeaderFolio(folio, uuser);
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsAllStatus0()
        {
            return _internalRequirementRepository.GetRequirementsAllStatus0();
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsAllCreditors()
        {
            return _internalRequirementRepository.GetRequirementsAllCreditors();
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplierSite(int id_supplier, string site_code, string buyer_division)
        {
            return _internalRequirementRepository.GetRequirementsDetailsBySupplierSite(id_supplier, site_code, buyer_division);
        }

        public int SendInternalHeaderFolio(int folio, string uuser)
        {
            return _internalRequirementRepository.SendInternalHeaderFolio(folio, uuser);
        }

        public List<AllItemSitesSupplier> GetRequirementsRelationItems()
        {
            return _internalRequirementRepository.GetRequirementsRelationItems();
        }
    }
}
