﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Transfers;
using App.DAL.Transfers;

namespace App.BLL.Transfers
{
    public class TransfersBusiness
    {
        private TransfersRepository _transferRepository;
        public TransfersBusiness()
        {
            _transferRepository = new TransfersRepository();
        }
        public List<TransferHeaderModel> GetTransfersByDateAndTransferSite(string site_code, DateTime dateInitial, DateTime dateFinish)
        {
            return _transferRepository.GetTransfersByDateAndTransferSite(site_code, dateInitial, dateFinish);
        }
        public List<TransferDetailReportModel> GetAllTransferDetail(DateTime date1, DateTime date2, string TransferType, string status, string originSite, string partNumber, string department, string family ,string mainSite)
        {
            return _transferRepository.GetAllTransferDetail(date1, date2, TransferType, status, originSite, partNumber, department, family , mainSite);
        }
        public List<TransferHeaderModel> GetTransfersHeaderIn(DateTime initial, DateTime finish, string TransferType, string status, string originSite, string partNumber, string department, string family , string mainSite)
        {
            var model = _transferRepository.GetTransfersHeaderIn(initial, finish, TransferType, status, originSite, partNumber, department, family , mainSite);
            return model;
        }
        public List<TransferDetailModel> GetTransferDetailsInReport(string document, string transferSiteCode)
        {
            if (transferSiteCode == null) throw new Exception("Falta valor de SiteCode");
            return _transferRepository.GetTransferDetailsInReport(document, transferSiteCode);
        }
        public List<TransferDetailModel> GetAllTransferDetailWithoutStatus(string Document , string mainSite)
        {
            return _transferRepository.GetAllTransferDeatil(Document , mainSite);
        }
        public List<TransferStoresSPModel> GetTransfersTotalsSP(DateTime dateInit , DateTime dateFinish , int statusTransfers , string part_type)
        {
            return _transferRepository.GetTransfersTotalsSP(dateInit,dateFinish , statusTransfers , part_type);
        }
        public List<TransferStoresSPModel> GetTransfersReportSiteSP(DateTime dateInit, DateTime dateFinish, string site_code , int status_transfers , string part_type)
        {
            return _transferRepository.GetTransfersReportSiteSP(dateInit, dateFinish, site_code , status_transfers , part_type);
        }
        public List<TransferHeaderReportSPModel> GetAllTransfersSites(DateTime dateInit, DateTime dateFinish, string site_code_des, string site_code_tra , int statusTransfers , string part_type)
        {
            return _transferRepository.GetAllTransfersSites(dateInit, dateFinish, site_code_des , site_code_tra , statusTransfers , part_type);
        }
        public List<TransferDetailModel> GetAllTransferDetailType(string Document, string mainSite, string status_transfer, string product_type , string transfer_type)
        {
            return _transferRepository.GetAllTransferDetailType(Document, mainSite, status_transfer, product_type , transfer_type);
        }

    }
}
