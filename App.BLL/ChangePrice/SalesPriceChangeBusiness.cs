﻿using App.DAL.ChangePrice;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace App.BLL.ChangePrice
{
    public class SalesPriceChangeBusiness

    {
        private SalesPriceChangeRepository _SalesPriceChangeRepository;
        private ItemMasterBusiness _ItemMasterBusines;
        public SalesPriceChangeBusiness()
        {
            _SalesPriceChangeRepository = new SalesPriceChangeRepository();
            _ItemMasterBusines = new ItemMasterBusiness();
        }

        public int addItemSalesPriceChange(string partNumber, DateTime appliedDate, decimal margin, decimal priceUser, string Sesion, string ProgramId)
        {
            var Tax = _ItemMasterBusines.GetItemIvaIepsValue(partNumber);
            var save = _SalesPriceChangeRepository.addItemSalesPriceChange(partNumber, appliedDate, margin, priceUser, Sesion, ProgramId, Tax);

            return save;
        }
        public SalesPriceChangeModel GetSalePriceByPartNumber(string partNumber)
        {
            return _SalesPriceChangeRepository.GetSalePriceByPartNumber(partNumber);
        }
    }
}
