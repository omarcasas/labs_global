﻿using App.DAL.Barcodes;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Barcodes
{
    public class BarcodesBusiness
    {
        private BarcodesRepository _barcodesRepository;
        public BarcodesBusiness()
        {
            _barcodesRepository = new BarcodesRepository();
        }

        public bool BarcodeExist(string barcode)
        {
            return _barcodesRepository.BarcodeExist(barcode);
        }

        public int AddBarcode(BARCODES barcode)
        {
            return _barcodesRepository.AddBarcode(barcode);
        }
    }
}