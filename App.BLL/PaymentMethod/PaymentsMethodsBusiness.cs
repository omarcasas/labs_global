﻿using App.DAL.PaymentMethod;
using App.Entities;
using App.Entities.ViewModels.PaymentMethod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.PaymentMethod
{
    public class PaymentsMethodsBusiness
    {
        private PaymentMethodRepository _PaymentMethodRepository;

        public PaymentsMethodsBusiness()
        {
            _PaymentMethodRepository = new PaymentMethodRepository();
        }

        public bool AddPaymentMethod(PaymentMethodModel Model)
        {
            var MethodsCount = _PaymentMethodRepository.GetPaymentMethods();
            Model.Id = MethodsCount.Count + 1;
            return _PaymentMethodRepository.AddPaymentMethod(Model.ITEM);
        }

        public List<PAYMENT_METHOD> GetPaymentMethods()
        {
            return _PaymentMethodRepository.GetPaymentMethods();
        }

        public bool DisablePaymentMethod(int PaymentMethodId)
        {
            return _PaymentMethodRepository.DisablePaymentMethod(PaymentMethodId);
        }

        public PaymentMethodModel GetPaymentMethodById(int PaymentMethodId)
        {
            return _PaymentMethodRepository.GetPaymentMethodById(PaymentMethodId);
        }

        public bool UpdatePaymentMethod(PaymentMethodModel Model)
        {
            var Item = new PAYMENT_METHOD();
            Item = Model.ITEM;

            return _PaymentMethodRepository.UpdatePaymentMethod(Item);
        }
    }
}
