﻿using App.DAL.FixedAsset;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetItemBusiness
    {
        private FixedAssetBusiness _fixedAsset;
        private FixedAssetItemRepository _fixedAssetItem;
        private FixedAssetRequisitionBusiness _fixedAssetRequisition;


        public FixedAssetItemBusiness()
        {
            _fixedAsset = new FixedAssetBusiness();
            _fixedAssetItem = new FixedAssetItemRepository();
            _fixedAssetRequisition = new FixedAssetRequisitionBusiness();
        }

        public FIXED_ASSET_ITEM GetItemById(int id)
        {
            return _fixedAssetItem.GetItemById(id);
        }

        public bool UpdateFixedAsset(FIXED_ASSET_ITEM item)
        {

            var check = false;
            if (_fixedAssetItem.EditFixedAssetItem(item) > 0) check = true;
            return check;

        }

        public List<FixedAssetItemModel> GetFixedAssetItemByRequisition(int requisition_code)
        {
            var query = _fixedAssetItem.GetItemsByRequisition(requisition_code)
                .Select(s => new FixedAssetItemModel {

                   fixed_asset_code = s.fixed_asset_code,
                   site_code = s.site_code,
                   description = s.description,
                   brand = s.brand,
                   serie = s.serie,
                   model = s.model,
                   requisiton_code = s.requisiton_code,
                   cuser = s.cuser

                }).ToList();

            foreach (var item in query)
            {
                item.fixed_asset_name = _fixedAsset.GetFixedAssetById(item.fixed_asset_code.Value).fixed_asset_name;
            }

            return query;
        }

        public FIXED_ASSET_ITEM AddItem(DateTime acquisition, string folio,string user, string site, int asset, string description, string brand, string serie, string model, int? requisition)
        {
            FIXED_ASSET_ITEM item = new FIXED_ASSET_ITEM();
            item.fixed_asset_folio = folio;
            item.acquisition_date = acquisition;
            item.fixed_asset_code = asset;
            item.site_code = site;
            item.description = description;
            item.brand = brand;
            item.serie = serie; 
            item.model = model;
            item.status_asset = 1;
            item.requisition_flag = false;
            item.requisiton_code = (requisition == 0) ? null : requisition;
            item.cuser = user;
            item.cdate = DateTime.Now;
            item.program_id = "RFX001.cshtml";
            item.location = "RB";

            return _fixedAssetItem.AddItem(item);
        }



    }
}
