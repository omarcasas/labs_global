﻿using App.DAL.FixedAsset;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetPurchaseBusiness
    {
        private FixedAssetPurchaseOrderRepository _fixedAssetPurchase;

        public FixedAssetPurchaseBusiness()
        {
            _fixedAssetPurchase = new FixedAssetPurchaseOrderRepository();
        }

        public int AddFixedAseetPurchase(FIXED_PURCHASE_ORDER item)
        {
            return _fixedAssetPurchase.AddFixedAseetPurchase(item);
        }

        public bool AddFixedAseetPurchaseItem(FIXED_PURCHASE_ORDER_ITEM purchase_item)
        {
            return _fixedAssetPurchase.AddFixedPurchaseItem(purchase_item);
        }

    }
}
