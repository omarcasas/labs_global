﻿using App.DAL.FixedAsset;
using App.DAL.Site;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetTransferHeaderBusiness
    {
        private FixedAssetTransferHeaderRepository _fixedTransferHeader;
        private SiteRepository _site;

        public FixedAssetTransferHeaderBusiness()
        {
            _fixedTransferHeader = new FixedAssetTransferHeaderRepository();
            _site = new SiteRepository();
        }

        public bool AddFixedAseet(FIXED_ASSET_TRANSFER_HEADER fixed_transfer_header)
        {
            return _fixedTransferHeader.AddFixedAseet(fixed_transfer_header);
        }

        public bool UpdateFixedAsset(FIXED_ASSET_TRANSFER_HEADER fixed_transfer_header)
        {
            var check = false;
            if (_fixedTransferHeader.EditFixedAssetTransferHeader(fixed_transfer_header) > 0) check = true;
            return check;
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetOwnFixedAssetTransferHeader()
        {
            return _fixedTransferHeader.GetOwnFixedAssetTransferHeader();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetOwnSiteFixedAssetTransferHeader(string site_code)
        {
            return _fixedTransferHeader.GetOwnSiteFixedAssetTransferHeader(site_code);
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetDestinationFixedAssetTransferHeader(string site_code)
        {
            return _fixedTransferHeader.GetDestinationFixedAssetTransferHeader(site_code);
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetFixedAssetTransferHeaderByStatusAndSite(string site_code, int status)
        {
            return _fixedTransferHeader.GetFixedAssetTransferHeaderByStatusAndSite(site_code, status);
        }

        public FIXED_ASSET_TRANSFER_HEADER GetHeaderByDocument(string document_no)
        {
            return _fixedTransferHeader.GetHeaderByDocument(document_no);
        }

        public List<FixedAssetTransferHeaderModel> GetTransferByType(string process)
        {
            List<FIXED_ASSET_TRANSFER_HEADER> query = new List<FIXED_ASSET_TRANSFER_HEADER>();
            List<FixedAssetTransferHeaderModel> result = new List<FixedAssetTransferHeaderModel>();

            switch (process)
            {
                case "Enviado":
                    query = GetSendTransferHeader();
                    break;
                case "Pendiente":
                    query = GetPendingTransferHeader();
                    break;
                case "Recibido":
                    query = GetReceivedTransferHeader();
                    break;
                default:
                    return null;
            }

            //generar viewmodel
            foreach (var item in query)
            {
                result.Add(new FixedAssetTransferHeaderModel
                {
                    document_no = item.document_no,
                    transfer_site_code_name = _site.GetSiteNameByCode(item.transfer_site_code),
                    destination_site_code_name = _site.GetSiteNameByCode(item.destination_site_code),
                    transfer_date = item.transfer_date,
                    status_str = GetStrStatus(item.status)

                });
            }

            return result;

        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetPendingTransferHeader()
        {
            return _fixedTransferHeader.GetPendingTransferHeader();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetReceivedTransferHeader()
        {
            return _fixedTransferHeader.GetReceivedTransferHeader();
        }

        public List<FIXED_ASSET_TRANSFER_HEADER> GetSendTransferHeader()
        {
            return _fixedTransferHeader.GetSendTransferHeader();
        }



        public string GetStrStatus(int? status)
        {
            switch (status)
            {
                case 1:
                    return "Enviado";
                case 4:
                    return "Pendiente";
                case 8:
                    return "Cancelado";
                case 9:
                    return "Recibido";
                default:
                    return null;
            }
        }
    }
}
