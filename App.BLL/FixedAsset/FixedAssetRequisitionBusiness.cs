﻿using App.BLL.Site;
using App.DAL.FixedAsset;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetRequisitionBusiness
    {
        private FixedAssetRequisitionRepository _fixedAssetRequisition;
        private SiteBusiness _site;

        public FixedAssetRequisitionBusiness()
        {
            _fixedAssetRequisition = new FixedAssetRequisitionRepository();
            _site= new SiteBusiness();
        }

        public FIXED_ASSET_REQUISITION GetFixedAssetById(int id) => _fixedAssetRequisition.GetFixedAssetRequisitionById(id);

        public List<FixedAssetRequisitionModel> GetFixedAssetBySite(string site) => _fixedAssetRequisition.GetFixedAssetRequisitionBySite(site);
   

        public List<FixedAssetRequisitionModel> GetPendingRequisitionAsset()
        {
            var query = _fixedAssetRequisition.GetPendingRequisitionAsset();

            foreach (var item in query)
            {
                item.site_name = _site.GetSiteNameByCode(item.site_code);
            }

            return query;
        }

        public bool UpdateFixedAssetRequisition(int id, int status, string user)
        {
            var assetRequisition = _fixedAssetRequisition.GetFixedAssetRequisitionById(id);
            assetRequisition.requisition_status = status;
            assetRequisition.user_approve = user;
            assetRequisition.udate = DateTime.Now;
            assetRequisition.uuser = user;

            var check = false;
            if (_fixedAssetRequisition.EditFixedAssetRequisition(assetRequisition)  > 0) check = true;
            return check;

        }

    }
}
