﻿using App.DAL.FixedAsset;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetTransferDetailBusiness
    {
        private FixedAssetTransferDetailRepository _fixedTransferDetail;

        public FixedAssetTransferDetailBusiness()
        {
            _fixedTransferDetail = new FixedAssetTransferDetailRepository();
        }


        public bool AddFixedAseet(FIXED_ASSET_TRANSFER_DETAIL fixed_transfer_detail)
        {
            return _fixedTransferDetail.AddFixedAseet(fixed_transfer_detail);
        }

        public bool UpdateFixedAsset(FIXED_ASSET_TRANSFER_DETAIL fixed_transfer_detail)
        {
            var check = false;
            if (_fixedTransferDetail.EditFixedAssetTransferDetail(fixed_transfer_detail) > 0) check = true;
            return check;
        }

        public List<FIXED_ASSET_TRANSFER_DETAIL> GetDetailByDocument(string document_no)
        {
            return _fixedTransferDetail.GetDetailByDocument(document_no);
        }

        public List<FixedAssetTransferDetailModel> GetDetailModelByDocument(string folio)
        {
            List<FixedAssetTransferDetailModel> result = new List<FixedAssetTransferDetailModel>();
            var query = _fixedTransferDetail.GetDetailByDocument(folio);

            foreach (var item in query)
            {
                result.Add(new FixedAssetTransferDetailModel
                {
                    fixed_asset_id = item.fixed_asset_id,
                    fixed_asset_folio = item.FIXED_ASSET_ITEM.fixed_asset_folio,
                    fixed_asset_code = item.FIXED_ASSET_ITEM.fixed_asset_code,
                    brand = item.FIXED_ASSET_ITEM.brand,
                    model = item.FIXED_ASSET_ITEM.model,
                    description = item.FIXED_ASSET_ITEM.description
                });
            }

            return result;

        }

    }
}
