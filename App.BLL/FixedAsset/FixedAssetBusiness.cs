﻿using App.DAL.FixedAsset;
using App.Entities;
using App.Entities.ViewModels.FixedAsset;
using App.Entities.ViewModels.SysInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetBusiness
    {
        private FixedAssetRepository _fixedAseet;

        public FixedAssetBusiness()
        {
            _fixedAseet = new FixedAssetRepository();
        }

        public bool AddFixedAseet(string name, string category, string cuser, decimal depreciation)
        {
            FIXED_ASSET fixed_asset = new FIXED_ASSET();
            fixed_asset.fixed_asset_name = name;
            fixed_asset.category = category;
            fixed_asset.depreciation = depreciation;
            fixed_asset.status_flag = true;
            fixed_asset.program_id = "FXD001.cshtml";
            fixed_asset.cuser = cuser;
            fixed_asset.cdate = DateTime.Now;
            return _fixedAseet.AddFixedAseet(fixed_asset);
        }

        public List<FIXED_ASSET> GetAllActiveFixedAsset()
        {
            return _fixedAseet.GetAllActiveFixedAsset();
        }

        public List<FIXED_ASSET> GetAllFixedAsset()
        {
            return _fixedAseet.GetAllFixedAsset();
        }

        public FIXED_ASSET GetFixedAssetById(int id)
        {
            return _fixedAseet.GetFixedAssetById(id);
        }

        public bool UpdateFixedAsset(int id, string name, string category, string user, bool status, decimal depreciation)
        {
            var asset = _fixedAseet.GetFixedAssetById(id);
            asset.fixed_asset_name = name;
            asset.category = category;
            asset.depreciation = depreciation;
            asset.status_flag = status;
            asset.udate = DateTime.Now;
            asset.uuser = user;

            var check = false;
            if (_fixedAseet.EditFixedAsset(asset) > 0) check = true;
            return check;

        }

        public StoreProcedureResult FixedAsset_Next_Document()
        {
            return _fixedAseet.Proc_Next_Document("FIXED_ASSET_CODE");
        }

        public StoreProcedureResult Transfer_Next_Document()
        {
            return _fixedAseet.Proc_Next_Document("FIXED_ASSET_TRANSFER");
        }

        public List<FixedAsserReportModel> GetFixedAssetReport(string site_code)
        {
            return _fixedAseet.GetFixedAssetReport(site_code);
        }

    }
}
