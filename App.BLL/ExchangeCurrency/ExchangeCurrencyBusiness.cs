﻿using App.BLL.Site;
using App.DAL.ExchangeCurrency;
using App.Entities;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.ExchangeCurrency
{
    public class ExchangeCurrencyBusiness
    {
        private ExchangeCurrencyRepository _ExchangeCurrencyRepository;
        private UserMasterBusiness _userMasterBusiness;
        private SiteBusiness _siteBusiness;
        public ExchangeCurrencyBusiness()
        {
            _ExchangeCurrencyRepository = new ExchangeCurrencyRepository();
            _userMasterBusiness = new UserMasterBusiness();
            _siteBusiness = new SiteBusiness();
        }

        public int postExchangeCurrency(EXCHANGE_CURRENCY_COMMERCIAL ExchangeCurrency)
        {
            //_ExchangeCurrencyRepository.putExchangeCurrency(ExchangeCurrency.cuser);
            return _ExchangeCurrencyRepository.postExchangeCurrency(ExchangeCurrency);
        }

        public List<ExchangeCurrencyViewModel> getAllCurrency()
        {
            var exchange = _ExchangeCurrencyRepository.getAllCurrency();
            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;
        }
        public List<ExchangeCurrencyViewModel> getDateCurrency(DateTime DateInit, DateTime DateFin, int type)
        {
            List<ExchangeCurrencyViewModel> exchange = new List<ExchangeCurrencyViewModel>();
            if (type == 0)
                exchange = _ExchangeCurrencyRepository.getDateCurrency(DateInit, DateFin);
            else if (type == 1)
                exchange = getAllCurrencyByType(DateInit, DateFin);
            else
                exchange = getAllCurrencyCommercialByType(DateInit, DateFin);

            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;
        }
        public bool AddExchangeSiteAll(int id_exchange, string cuser)
        {
            bool check = false;
            int con = _siteBusiness.GetAllSitesOferts().Count();
            var sitess = _siteBusiness.GetAllSitesOferts();
            for (int i = 0; i < con; i++)
            {
                EXCHANGE_CURRENCY_COMMERCIAL_SITES site = new EXCHANGE_CURRENCY_COMMERCIAL_SITES();
                site.id_exchange_currency_commercial = id_exchange;
                site.cuser = cuser;
                site.cdate = DateTime.Now;
                site.replication_status = 0;
                site.site_code = sitess[i].site_code;
                site.program_id = "PURCH015.cshtml";
                if (_ExchangeCurrencyRepository.AddExchangeSite(site) > 0) { check = true; }
                else { break; }
            }
            return check;
        }

        public int AddExchangeSite(EXCHANGE_CURRENCY_COMMERCIAL_SITES site)
        {
            return _ExchangeCurrencyRepository.AddExchangeSite(site);
        }

        public List<SiteModel> GetExchanceCurrencySites(int exchange_id, int type)
        {
            if (type == 1)
            {
                return _ExchangeCurrencyRepository.GetExchanceCurrencySites(exchange_id);
            }
            else
            {
                return _siteBusiness.GetAllSitesName();
            }
            
        }

        public decimal GetLastExchangeCurrent()
        {
            return _ExchangeCurrencyRepository.GetLastExchangeCurrent();
        }

        public List<ExchangeCurrencyViewModel> getAllCurrencyByType(DateTime DateInit, DateTime DateFin)
        {
            var exchange = _ExchangeCurrencyRepository.getAllCurrencyByType(DateInit, DateFin);
            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;


        }

        public List<ExchangeCurrencyViewModel> getDateCurrency(DateTime DateInit, DateTime DateFin)
        {
            var exchange = _ExchangeCurrencyRepository.getDateCurrency(DateInit, DateFin);
            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;

            
        }

        public List<ExchangeCurrencyViewModel> getAllCurrencyCommercialByType(DateTime DateInit, DateTime DateFin)
        {
            var exchange = _ExchangeCurrencyRepository.getAllCurrencyCommercialByType(DateInit, DateFin);
            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;
        }


        public List<ExchangeCurrencyViewModel> getAllCurrencyBySite(DateTime DateInit, DateTime DateFin, string siteCode)
        {
            var exchange = _ExchangeCurrencyRepository.getAllCurrencyBySite(DateInit, DateFin, siteCode);
            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;
        }

        

        public List<ExchangeCurrencyViewModel> getAllCurrencyCommercialBySite(DateTime DateInit, DateTime DateFin, string siteCode)
        {
            var exchange = _ExchangeCurrencyRepository.getAllCurrencyCommercialBySite(DateInit, DateFin, siteCode);
            foreach (var item in exchange)
            {
                if (item.User != "System")
                    item.User = _userMasterBusiness.getFirstLastName(item.User);
                else
                    item.User = "Sistema";
            }
            return exchange;
        }

        public List<ExchangeCurrencyViewModel> GetExchangeByDateAndType(DateTime? BeginDate, DateTime? EndDate, int Type, string SiteCode)
        {
            DateTime date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            DateTime BeginDate2 = BeginDate ?? firstDayOfMonth;
            DateTime EndDate2 = EndDate ?? lastDayOfMonth;
            if (Type == 0)
                if (SiteCode == "All")
                    return getDateCurrency(BeginDate2, EndDate2);
                else
                    return getAllCurrencyBySite(BeginDate2, EndDate2, SiteCode);
            else if (Type == 1)
                return getAllCurrencyByType(BeginDate2, EndDate2);
            else
            {
                if (SiteCode == "All")
                    return getAllCurrencyCommercialByType(BeginDate2, EndDate2);
                else
                    return getAllCurrencyCommercialBySite(BeginDate2, EndDate2, SiteCode);
            }
        }
    }
}
