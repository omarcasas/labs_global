﻿using App.DAL.Creditors;
using App.DAL.Supplier;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Creditors
{
    public class ItemSupplierCreditorsBusiness
    {
        ItemSupplierCreditorsRepository _ItemSupplierCreditorsRepository;
        ItemCreditorsPriceRepository _ItemCreditorsPriceRepository;

        public ItemSupplierCreditorsBusiness()
        {
            _ItemSupplierCreditorsRepository = new ItemSupplierCreditorsRepository();
            _ItemCreditorsPriceRepository = new ItemCreditorsPriceRepository();
        }

        public List<ItemSupplierCreditorsModel> GetAllSupplierCreditors(string BuyerDivision)
        {
            return _ItemSupplierCreditorsRepository.GetAllSupplierCreditors(BuyerDivision);
        }

        public ItemSupplierCreditorsModel SaveSupplierCreditor(ItemSupplierCreditorsModel ItemSupplier, string User)
        {
            if (ItemSupplier.SupplierId > 0 && ItemSupplier.PartNumber != "")
            {
                return _ItemSupplierCreditorsRepository.SaveSupplierCreditor(ItemSupplier, User);
            }
            return ItemSupplier;
        }

        public bool EditSupplierCreditor(ItemSupplierCreditorsModel ItemSupplier, string User)
        {
            if (ItemSupplier.SupplierId > 0 && ItemSupplier.PartNumber != "")
            {
                return _ItemSupplierCreditorsRepository.EditSupplierCreditor(ItemSupplier, User);
            }
            return false;
        }

        public List<MaSupplierModel> GetAllSuppliersInModel()
        {
            return _ItemSupplierCreditorsRepository.GetAllSuppliersInModel();
        }
        public bool DisabaleItemCreditorActive(ItemCreditorPriceModel ItemPrice, string User)
        {
            if (ItemPrice.SupplierPartNumberCreditors > 0)
            {
                return _ItemSupplierCreditorsRepository.DisabaleItemCreditorActive(ItemPrice, User);
            }
            return false;
        }

        public bool DisableItemCreditors(int SupplierPartNumer, string User)
        {
            if (SupplierPartNumer > 0)
            {
                return _ItemCreditorsPriceRepository.DisableItemCreditors(SupplierPartNumer, User);
            }
            return false;
        }
        public bool SaveItemCreditors(ItemCreditorPriceModel ItemPrice, string User)
        {
            if (ItemPrice.Price > 0 && ItemPrice.SupplierPartNumberCreditors > 0)
            {
                return _ItemCreditorsPriceRepository.SaveItemCreditors(ItemPrice, User);
            }
            return false;
        }

        public bool EditCategory(int Supplier, List<int> Category,string User)
        {
            List<int> CategoryList = new List<int>();
            if (_ItemSupplierCreditorsRepository.RemoveCategory(Supplier, Category,User))
            {
                foreach (var item in Category)
                {
                    if (_ItemSupplierCreditorsRepository.ExisCreditors(Supplier,item,User))
                    {
                       
                    }
                    else
                    {
                        CategoryList.Add(item);
                    }
                }
                return _ItemSupplierCreditorsRepository.SaveCategoryCreditors(Supplier,CategoryList,User);
            }
            return false;
        }

        public List<int> CategorysCreditors(int Supplier)
        {
            return _ItemSupplierCreditorsRepository.CategorysCreditors(Supplier);
        }
    }
}
