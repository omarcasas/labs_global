﻿using App.DAL.Creditors;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Creditors
{
    public class ItemCreditorsCategoryBusiness
    {
        ItemCreditorsCategoryRepository _CodesPackageRepository;
        public ItemCreditorsCategoryBusiness()
        {
            _CodesPackageRepository = new ItemCreditorsCategoryRepository();
        }

        public ItemCreditorsCategoryModel SaveCategory(ItemCreditorsCategoryModel Category, string User)
        {
            if (Category.Description.Trim() != "" && Category.Name != "")
            {
                return _CodesPackageRepository.SaveCategory(Category, User);
            }
            return new ItemCreditorsCategoryModel();
        }
        public bool EditCategory(ItemCreditorsCategoryModel Category, string User)
        {
            if (Category.Description.Trim() != "" && Category.Name != "")
            {
                return _CodesPackageRepository.EditCategory(Category, User);
            }
            return false;
        }
        public List<ItemCreditorsCategoryModel> GetAllCreditorsCategorys(string BuyerDivision)
        {
            return _CodesPackageRepository.GetAllCreditorsCategorys(BuyerDivision);
        }
    }
}
