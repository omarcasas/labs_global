﻿using App.DAL.Creditors;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Creditors
{
    public class ItemCreditorsBusiness
    {
        ItemCreditosRepository _ItemCreditosRepository;
        public ItemCreditorsBusiness()
        {
            _ItemCreditosRepository = new ItemCreditosRepository();
        }


        public ItemCreditorsModel SaveItem(ItemCreditorsModel Item, string User)
        {
            if (Item.Description.Trim() != "" && Item.PartNumber != "" && Item.CategoryId > 0)
            {
                return _ItemCreditosRepository.SaveItem(Item, User);
            }
            return new ItemCreditorsModel();
        }

        public bool SaveItemNewsuppliers(ItemCreditorsModel Item, string User)
        {
            return _ItemCreditosRepository.SaveItemNewSuppliersCategories(Item, User);
        }

        public bool EditItem(ItemCreditorsModel Item, string User)
        {
            if (Item.Description.Trim() != "" && Item.PartNumber != "" && Item.CategoryId > 0)
            {
                return _ItemCreditosRepository.EditItem(Item, User);

            }
            return false;
        }

        public List<ItemCreditorsModel> GetAllCreditorsItems(string BuyerDivision)
        {
            return _ItemCreditosRepository.GetAllCreditorsItems(BuyerDivision);
        }

        public List<TypeCreditorsModel> TypeCreditorsList()
        {
            return _ItemCreditosRepository.GetAllTypeCreditorsList();
        }

        public bool AddTypeCreditorsList(TypeCreditorsModel Type)
        {
            if (Type.Id.Trim() != "" && Type.Name != "")
            {
                return _ItemCreditosRepository.AddTypeCreditorsList(Type);
            }
            return false;
        }
    }
}
