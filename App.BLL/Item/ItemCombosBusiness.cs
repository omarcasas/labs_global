﻿using App.BLL.Purchases;
using App.DAL;
using App.DAL.ChangePrice;
using App.DAL.CommonAccess;
using App.DAL.Item;
using App.DAL.Purchases;
using App.DAL.Purchases.PurchasesModels;
using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Item
{
    public class ItemCombosBusiness
    {

        private ItemComboRepository _ItemComboRepo;
        private MaTaxRepository _MaTaxRepo;
        private PurchaseBasePriceRespository _PurchaseBasePriceRespository;
        private SalesPriceChangeRepository _SalesPriceChangeRepository;
        private ItemMasterBusiness _ItemMasterBusines;
        //constructor
        public ItemCombosBusiness()
        {
            _ItemComboRepo = new ItemComboRepository();
            _MaTaxRepo = new MaTaxRepository();
            _PurchaseBasePriceRespository = new PurchaseBasePriceRespository();
            _SalesPriceChangeRepository = new SalesPriceChangeRepository();
            _ItemMasterBusines = new ItemMasterBusiness();
        }
        public bool AddComboHeader(COMBO_HEADER combo)
        {
            var check = false;
            var flag = _ItemComboRepo.AddComboHeader(combo);

            if (flag > 0) check = true;

            return check;
        }
        public List<ItemComboModel> getCombosByStatus(string comboStatus)
        {
            if (comboStatus != "")
            {
                return _ItemComboRepo.getCombosByStatus(comboStatus);
            }
            return new List<ItemComboModel>();
        }
        public List<ItemComboModel> getCombosDetailByPartNumberCombo(string partNumberCombo)
        {
            if (partNumberCombo != "")
            {
                StoreProcedureIvaIeps itemC = new StoreProcedureIvaIeps();
                var detail = _ItemComboRepo.getCombosDetailByPartNumberCombo(partNumberCombo);
                detail.Where(w => w.part_number == w.part_number).ToList().ForEach(s =>
                {
                    s.ivaDescription = _MaTaxRepo.StoreProcedureIvaIeps(s.part_number).Iva.ToString();
                    s.base_cost = _PurchaseBasePriceRespository.GetCostProductDesc(s.part_number).base_price;
                    decimal x = Convert.ToDecimal(s.base_cost / (s.total_price_sale));
                    s.margen_part_number = Math.Round(((1 - x) * 100), 4).ToString();
                });
                return detail;
            }
            return new List<ItemComboModel>();
        }
        public bool AddComboDetail(List<ItemComboModel> model, string User, decimal newComboCost, decimal newComboPrice)
        {
            var flag = 0;
            var check = false;
            string comboPart = model.First().part_number_combo;
            var ComboHeader = _ItemComboRepo.getComboHeaderItem(comboPart);
            if (ComboHeader != null)
            {
                ComboHeader.combo_cost = newComboCost;
                ComboHeader.combo_price = newComboPrice;
                ComboHeader.udate = DateTime.Now;
                ComboHeader.uuser = User;
                if (_ItemComboRepo.UpdateComboHeader(ComboHeader) > 0)
                {
                    check = true;
                }
                else
                {
                    check = false;
                    return check;
                }
            }
            var itemPrice = _SalesPriceChangeRepository.GetSalePriceByPartNumber(comboPart);
            if (itemPrice == null)
            {
                itemPrice = new SalesPriceChangeModel();
                itemPrice.priceUser = 0;
            }
            if (itemPrice.priceUser != newComboPrice)
            {
                decimal x = Convert.ToDecimal(newComboCost / (newComboPrice));
                decimal margen_part_number = Math.Round(((1 - x) * 100), 4);
                StoreProcedureIvaIeps Tax = new StoreProcedureIvaIeps() { Ieps = 0, Iva = 0 };//Los combos no llevan IVA (ya estan incluido dentro de los productos)
                var result = _SalesPriceChangeRepository.addItemSalesPriceChange(comboPart, DateTime.Now, margen_part_number, newComboPrice, User, "PURCH016.cshtml", Tax);
                if (result == 0)
                {
                    check = false;
                    return check;
                }
                else
                {
                    check = true;
                }
            }
            foreach (var item in model)
            {
                if (item.statusInsert)
                {
                    var COMBODETAIL = new COMBO_DETAIL
                    {
                        part_number_combo = item.part_number_combo,
                        part_number = item.part_number,
                        quantity = item.quantity,
                        iva = item.iva,
                        active_status = item.active_status,
                        total_price_sale = item.total_price_sale,
                        cuser = User,
                        cdate = DateTime.Now,
                        program_id = "PURCH016.cshtml",
                    };
                    flag = _ItemComboRepo.AddComboDetail(COMBODETAIL);
                }
                else
                {
                    var COMBODETAIL = _ItemComboRepo.getComboDetailItem(item.part_number_combo, item.part_number);
                    COMBODETAIL.quantity = item.quantity;
                    COMBODETAIL.iva = item.iva;
                    COMBODETAIL.total_price_sale = item.total_price_sale;
                    COMBODETAIL.active_status = item.active_status;
                    COMBODETAIL.uuser = User;
                    COMBODETAIL.udate = DateTime.Now;
                    flag = _ItemComboRepo.UpdateComboDetail(COMBODETAIL);
                }
                if (flag == 0)
                {
                    check = false;
                    break;
                }
            }
            return check;
        }
        public ItemComboModel getCombosBaseCostByPartNumber(string partNumberCombo)
        {
           return _ItemComboRepo.getCombosBaseCostByPartNumber(partNumberCombo);
        }
    }
}
