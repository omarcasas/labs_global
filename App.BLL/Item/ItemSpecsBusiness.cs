﻿using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Item
{
    public class ItemSpecsBusiness
    {
        private ItemSpecsRepository _itemSpecsReporitory;
        public ItemSpecsBusiness()
        {
            _itemSpecsReporitory = new ItemSpecsRepository();
        }
        public ItemSpecs getSpecsByPartNumber(string partNumber)
        {
            if (partNumber != "")
            {
                return _itemSpecsReporitory.getSpecsByPartNumber(partNumber);
            }
            return new ItemSpecs();
        }
        public bool AddItemSpecs(ITEM_SPECS itemSpecs)
        {
            var check = false;
            var flag = _itemSpecsReporitory.AddItemSpecs(itemSpecs);

            if (flag > 0) check = true;

            return check;
        }
        public bool UpdateItemSpecs(ITEM_SPECS itemSpecs)
        {
            var check = false;
            var flag = _itemSpecsReporitory.UpdateItemSpecs(itemSpecs);

            if (flag > 0) check = true;

            return check;
        }
    }
}
