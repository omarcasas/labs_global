﻿using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;

namespace App.BLL.Item
{
    public class ItemOldErpBusiness
    {
        private ItemOldErpRepository _ItemOldErpRepository;

        //constructor
        public ItemOldErpBusiness()
        {
            _ItemOldErpRepository = new ItemOldErpRepository();
        }
        public List<ItemOldErpModel> getOLDERPSupplier(string Filter)
        {
            var OldErpSuppliers = _ItemOldErpRepository.getOLDERPSupplier(Filter);
            if (OldErpSuppliers != null)
            {
                if (OldErpSuppliers.Count == 0)
                {
                    return null;
                }
                else
                {
                    return OldErpSuppliers;
                }
            }
            else
            {
                return null;
            }
        }

        public List<ItemOldErpModel> getERPSupplier(string Filter)
        {
            var OldErpSuppliers = _ItemOldErpRepository.getERPSupplier(Filter);
            if (OldErpSuppliers != null)
            {
                if (OldErpSuppliers.Count == 0)
                {
                    return null;
                }
                else
                {
                    return OldErpSuppliers;
                }
            }
            else
            {
                return null;
            }
        }

        public List<ItemOldErpModel> getOLDERPFamili(string Filter)
        {
            var OldErpSuppliers = _ItemOldErpRepository.getItemFamili(Filter);
            if (OldErpSuppliers != null)
            {
                if (OldErpSuppliers.Count == 0)
                {
                    return null;
                }
                else
                {
                    return OldErpSuppliers;
                }
            }
            else
            {
                return null;
            }
        }
        public bool AddItemTemp(ITEM_TEMP_ERP ItemWMS, ITEM Item)
        {
            ItemWMS.ERP_part_pesado = Item.weight_flag != true ? "0" : "1";
            ItemWMS.product_type = Item.part_type != "ROH" ? "HAWA" : "OPER";
            ItemWMS.mrp_type = "Y";
            ItemWMS.um = Item.unit_size == "KGS" ? "KG" : Item.unit_size;

            //var j = _ItemOldErpRepository.getBuyerid(Item.buyer_division);
            //foreach (var itm in j)
            //{
            //    ItemWMS.ERP_buyer_id = itm.buyer_id;
            //    if (itm.description == "P007" || itm.description == "P008")
            //    {
            //        ItemWMS.mrp_type = "N";
            //        ItemWMS.product_type = "OPER";
            //    }
            //}

            if (_ItemOldErpRepository.AddItemTemp(ItemWMS, Item) > 0)
            {
                return true;
            }
            else
                return false;
        }


        public int Sp_Insert_New_Produ(string part_number, string user)
        {
            return _ItemOldErpRepository.Sp_Insert_New_Produ(part_number, user); 
        }

    }
}
