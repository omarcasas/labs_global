﻿using App.BLL.Site;
using App.DAL;
using App.DAL.CommonAccess;
using App.DAL.Item;
using App.DAL.ItemPresentation;
using App.Entities;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.ItemPresentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL
{
    public class ItemMasterBusiness
    {
        private readonly ItemMasterRepository _itemMasterRepo;
        private readonly MaTaxRepository _MaTaxRepository;
        private readonly ItemComboRepository _ComboRepository;
        private readonly ItemPresentationRepository _itemPresentationRepository;
        private readonly SiteBusiness _SiteBusiness;

        public ItemMasterBusiness()
        {
            _itemMasterRepo = new ItemMasterRepository();
            _MaTaxRepository = new MaTaxRepository();
            _ComboRepository = new ItemComboRepository();
            _itemPresentationRepository = new ItemPresentationRepository();
            _SiteBusiness = new SiteBusiness();
        }

        public ITEM GetItemByPartNumber(string partnumber)
        {
            return _itemMasterRepo.GetItemByPartNumber(partnumber);
        }

        public List<ItemModel> SerchItem(string part_number)
        {
            return _itemMasterRepo.SearchItem(part_number).Where(w => w.extra_items_flag != true && w.combo_flag != true && w.active_flag == true).Select(x => new ItemModel
            {
                Description = x.part_description,
                PathNumber = x.part_number
            }).ToList();
        }

        public List<ItemModel> SerchItemBarcode(string part_number)
        {
            return _itemMasterRepo.SearchItemBarcode(part_number);
        }

        public List<ItemModel> SearchItemBarcodeSupplier(string part_number, int supplier)
        {
            return _itemMasterRepo.SearchItemBarcodeSupplier(part_number, supplier);
        }

        public List<ItemBarcodeAndPartNumber> SearchBarcode(string part_number)
        {
            return _itemMasterRepo.SearchBarcode(part_number);
        }

        public int SaveBarcode(string part_number, string barcode, string user)
        {
            return _itemMasterRepo.SaveBarcode(part_number, barcode, user);
        }
        public ITEM GetItemByPartNumberSAT(string partNumberSAT)
        {
            return _itemMasterRepo.GetItemByPartNumberSAT(partNumberSAT);
        }

        public bool AddItemMaster(ITEM user)
        {
            var check = false;
            var flag = _itemMasterRepo.AddItemMaster(user);

            if (flag > 0) check = true;

            return check;
        }

        public bool UpdateItemMaster(ITEM item, string user)
        {
            bool check = false;
            var Item = _itemMasterRepo.GetItemByPartNumber(item.part_number);
            Item.part_description = item.part_description;
            Item.unit_size = item.unit_size;
            Item.part_type = item.part_type;
            Item.buyer_division = item.buyer_division;
            Item.part_iva_purchase = item.part_iva_purchase;
            Item.part_iva_sale = item.part_iva_sale;
            Item.part_number_sat = item.part_number_sat;
            Item.unit_sat = item.unit_sat;
            Item.active_flag = item.active_flag;
            Item.combo_flag = item.combo_flag;
            Item.weight_flag = item.weight_flag;
            Item.extra_items_flag = item.extra_items_flag;
            Item.level_header = item.level_header;
            Item.level1_code = item.level1_code;
            Item.level2_code = item.level2_code;
            Item.level3_code = item.level3_code;
            Item.part_ieps = item.part_ieps;
            Item.udate = DateTime.Now;
            Item.uuser = user;
            Item.expiration_days = item.expiration_days;

            var ComboHeader = _ComboRepository.getComboHeaderItem(item.part_number);
            if (ComboHeader != null)
            {
                ComboHeader.combo_status = Convert.ToInt32(item.combo_flag).ToString();
                ComboHeader.udate = DateTime.Now;
                ComboHeader.uuser = user;
                if (_ComboRepository.UpdateComboHeader(ComboHeader) > 0)
                {
                    check = true;
                }
                else
                {
                    Item.combo_flag = false;
                }
            }
            else if (item.combo_flag == true)
            {
                ComboHeader = new COMBO_HEADER
                {
                    part_number_combo = item.part_number,
                    combo_status = Convert.ToInt32(item.combo_flag).ToString(),
                    cuser = user,
                    cdate = DateTime.Now,
                    program_id = "PURCH016.cshtml",
                    combo_price = 0,
                    combo_cost = 0
                };
                if (_ComboRepository.AddComboHeader(ComboHeader) > 0)
                {
                    check = true;
                }
                else
                {
                    Item.combo_flag = false;
                }
            }
            if (_itemMasterRepo.UpdateItemMaster(Item) > 0)
            {
                check = true;
            }

            return check;
        }

        public bool DeleteItemMaster(ITEM user)
        {
            var check = false;
            if (_itemMasterRepo.DeleteItemMaster(user) > 0) check = true;
            return check;
        }

        public List<ITEM> GetAllItems()
        {
            return _itemMasterRepo.GetAllItems();

        }

        public List<ITEM> GetAllItemsAll()
        {
            return _itemMasterRepo.GetAllItemsAll();

        }

        public List<ItemMasterModel> GetAllItems2()
        {
            return _itemMasterRepo.GetAllItems2();

        }

        public List<ItemMasterModel> GetAllItemsActives()
        {
            return _itemMasterRepo.GetAllItemsActives();

        }
        public List<ItemMasterModel> GetAllItemsActivesN()
        {
            return _itemMasterRepo.GetAllItemsActivesN();

        }

        public List<ItemDropDownModel> GetAllItemsToDropDownList()
        {
            return _itemMasterRepo.GetAllItemsToDropDownList();
        }

        public ItemMasterModel GetItemByPartNum(string partNumber)
        {
            var item = _itemMasterRepo.GetItemByPartNum(partNumber);
            return item;
        }

        public StoreProcedureIvaIeps GetItemIvaIepsValue(string partNumber)
        {
            return _MaTaxRepository.StoreProcedureIvaIeps(partNumber);
        }

        public ItemMasterModel GetItemIvaForIeps(string PartNumber)
        {
            if (PartNumber.Trim() != "")
            {
                var Item = _itemMasterRepo.GetItemByPartNum(PartNumber);
                var IvaIeps = _MaTaxRepository.StoreProcedureIvaIeps(PartNumber);
                Item.part_ieps = IvaIeps.Ieps.ToString();
                Item.part_iva = IvaIeps.Iva.ToString();
                return Item;
            }
            return new ItemMasterModel();
        }

        public List<ItemPresentationModel> GetPresentations()
        {
            return _itemMasterRepo.GetPresentations();
        }

        public int ItemPresentation(string part_number)
        {
            return _itemMasterRepo.ItemPresentation(part_number);
        }

        public string ItemPresentationPartnumber(string part_number)
        {
            return _itemMasterRepo.ItemPresentationPartnumber(part_number);
        }

        public List<ItemMasterModel> GetItemsTrueFlag()
        {

            var Item = _itemMasterRepo.GetItemsFlag(true);
            //foreach (var item in Item)
            //{
            //    var IvaIeps = _MaTaxRepository.StoreProcedureIvaIeps(item.part_number);
            //    item.part_ieps = IvaIeps.Ieps.ToString();
            //    item.part_iva = IvaIeps.Iva.ToString();
            //}
            return Item;
        }

        public List<ItemMasterModel> GetItemsFalseFlag()
        {
            var Item = _itemMasterRepo.GetItemsFlag(false);
            //foreach (var item in Item)
            //{
            //    var IvaIeps = _MaTaxRepository.StoreProcedureIvaIeps(item.part_number);
            //    item.part_ieps = IvaIeps.Ieps.ToString();
            //    item.part_iva = IvaIeps.Iva.ToString();
            //}
            return Item;
        }

        public List<ItemMasterModel> GetItemsFlagComboRaw()
        {

            var Item = _itemMasterRepo.GetItemsFlagComboRaw();
            return Item;
        }

        public List<ItemDropDownModel> getItemsByLike(string Filter, int status)
        {
            return _itemMasterRepo.getItemsByLike(Filter, status);
        }

        public int UpdateItemIVABySupplier(int supplier, string uuser)
        {
            return _itemMasterRepo.UpdateItemIVABySupplier(supplier, uuser);
        }

        public ItemMasterModel GetInformationProduct(string part_number)
        {
            return _itemMasterRepo.GetInformationProduct(part_number);
        }

        public List<ItemModel> SearchItemLike(string part_number)
        {
            return _itemMasterRepo.SearchItemLike(part_number);
        }

        public ItemPresentationModel GetInfoPresentationByIdInt(int presentation_id)
        {
            return _itemPresentationRepository.GetInfoPresentationByIdInt(presentation_id);
        }

        public List<ItemStockHistory> GetStockHistory(string SiteCode, string Date) => _itemMasterRepo.GetStockHistory(_SiteBusiness.GetSiteIP(SiteCode), Date);

        public List<ItemBlockedModel> GetAllStoresItemBlocked(string part_number)
        {
            return _itemMasterRepo.GetAllStoresItemBlocked(part_number);
        }
        public bool AddItemBlocked(string PartNumber, string Sites, string user)
        {
            var check = false;
            var flag = _itemMasterRepo.AddItemBlocked(PartNumber, Sites, user);

            if (flag > 0) check = true;

            return check;
        }
    }
}