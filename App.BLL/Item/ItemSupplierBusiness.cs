﻿using App.DAL.Purchases;
using App.DAL.Supplier;
using App.Entities;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;

namespace App.BLL.Purchases
{
    public class ItemSupplierBusiness
    {

        private ItemSupplierRepository _ItemSupplierRepo;

        //constructor
        public ItemSupplierBusiness()
        {
            _ItemSupplierRepo = new ItemSupplierRepository();
        }

        public List<ItemSupplierModel> GetItemsBySupplierCodeList(List<int> list)
        {
            var ItemsSuppliers = _ItemSupplierRepo.GetItemsBySupplierCodeList(list);
            if (ItemsSuppliers != null)
            {
                if (ItemsSuppliers.Count == 0)
                {
                    return null;
                }
                else
                {
                    return ItemsSuppliers;
                }
            }
            else
            {
                return null;
            }
        }
        public bool DisableItemSupplier(int supplierPartNumber, string cuser)
        {
            return _ItemSupplierRepo.DisableItemSupplier(supplierPartNumber,cuser);
        }

        public List<ItemSupplierModel> GetItemsBySupplierCodeInSalePrice(int sCode)
        {
            var ItemsSuppliers = _ItemSupplierRepo.GetItemsBySupplierCodeInSalePrice(sCode);
            if (ItemsSuppliers != null)
            {
                if (ItemsSuppliers.Count == 0)
                {
                    return null;
                }
                else
                {
                    return ItemsSuppliers;
                }
            }
            else
            {
                return null;
            }
        }

        public int AssignSupplierToProduct(ITEM_SUPPLIER SupplierId)
        {
            
            return _ItemSupplierRepo.AssignSupplierToProduct(SupplierId); ;
        }
        public List<ItemMasterModel> getSupplierItem(int Supplier_id)
        {
            if (Supplier_id > 0)
            {
                return _ItemSupplierRepo.getSupplierItem(Supplier_id);
            }
            return new List<ItemMasterModel>();
        }

        public List<SupplierModel> getItemSupplier(string ParthNumber)
        {
            if (!string.IsNullOrWhiteSpace(ParthNumber.Trim()))
            {
                return _ItemSupplierRepo.getItemSupplier(ParthNumber);
            }
            return new List<SupplierModel>();
        }
        public List<itemsSuppier> GetItemsBySupplierIdToPromotions(int supplier_id)
        {
            return _ItemSupplierRepo.GetItemsBySupplierIdToPromotions(supplier_id);
        }
    }
}
