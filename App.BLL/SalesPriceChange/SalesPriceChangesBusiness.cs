﻿using App.DAL.SalesPriceChange;
using App.Entities.ViewModels.SalesPriceChange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.SalesPriceChange
{
    public class SalesPriceChangesBusiness
    {
        private readonly SalesPriceChangesRepository _SalesPriceChangeRepository;

        public SalesPriceChangesBusiness()
        {
            _SalesPriceChangeRepository = new SalesPriceChangesRepository();
        }

        public List<SalesPriceChangeViewModel> GetSalesPriceChanges(DateTime BeginDate, DateTime EndDate, string Department, string Family) => _SalesPriceChangeRepository.GetSalesPriceChanges(BeginDate, EndDate, Department, Family);

        public List<SalesPriceChangeViewModel> GetSalesPriceChangesDetail(DateTime BeginDate, DateTime EndDate, string PartNumber) => _SalesPriceChangeRepository.GetSalesPriceChangesDetail(BeginDate, EndDate, PartNumber);

        public List<SalesPriceChangePresentationViewModel> GetSalesPriceChangesPresentations(DateTime BeginDate, DateTime EndDate, string Department, string Family) => _SalesPriceChangeRepository.GetSalesPriceChangesPresentations(BeginDate, EndDate, Department, Family);

        public List<SalesPriceChangePresentationViewModel> GetSalesPriceChangesPresentationsDetail(DateTime BeginDate, DateTime EndDate, int PresentationId) => _SalesPriceChangeRepository.GetSalesPriceChangesPresentationsDetail(BeginDate, EndDate, PresentationId);
    }
}