﻿using App.DAL.Services;
using App.Entities;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Services
{
    public class ServicesProductsBusiness
    {
        private ServicesProductsRepository _ServicesProductsRepository;

        public ServicesProductsBusiness()
        {
            _ServicesProductsRepository = new ServicesProductsRepository();
        }

        public bool AddServicesProduct(ServiceProduct Model)
        {
            var Item = Model.ITEM;
            Item.type = Model.Type == TypeEnum.One ? 1 : 3;
            if (!string.IsNullOrEmpty(Model.VerifyDigit))
                Item.verify_digit = int.Parse(Model.VerifyDigit);

            return _ServicesProductsRepository.AddServicesProduct(Item);
        }

        public List<ServiceProduct> GetProductsList()
        {
            return _ServicesProductsRepository.GetProductsList();
        }

        public ServiceProduct GetProductById(int ProductId)
        {
            return _ServicesProductsRepository.GetProductById(ProductId);
        }

        public bool UpdateServiceProduct(ServiceProduct Model)
        {
            var Item = new DFLPOS_SERVICES_PRODUCTS();

            Item = Model.ITEM;
            Item.id = Model.ProductId;
            Item.type = Model.Type == TypeEnum.One ? 1 : 3;
            if (!string.IsNullOrEmpty(Model.VerifyDigit))
                Item.verify_digit = int.Parse(Model.VerifyDigit);

            return _ServicesProductsRepository.UpdateServiceProduct(Item);
        }

        public bool DisableServiceProduct(int ProductId)
        {
            return _ServicesProductsRepository.DisableServiceProduct(ProductId);
        }
    }
}
