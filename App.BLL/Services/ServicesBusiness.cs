﻿using App.DAL.Services;
using App.Entities;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Services
{
    public class ServicesBusiness
    {
        ServicesRepository _ServicesRepository;

        public ServicesBusiness()
        {
            _ServicesRepository = new ServicesRepository();
        }

        public List<DFLPOS_SERVICES> Services()
        {
            return _ServicesRepository.Services();
        }

        public List<Service> GetServices()
        {
            return _ServicesRepository.GetServices();
        }

        public bool AddService(Service Model)
        {
            var Item = new DFLPOS_SERVICES();
            Item.description = Model.Name;
            Item.enabled = Model.IsActive;
            Item.order = Model.Order;

            return _ServicesRepository.AddService(Item);
        }

        public Service GetServiceById(int ServiceId)
        {
            return _ServicesRepository.GetServiceById(ServiceId);
        }

        public bool UpdateService(Service Model)
        {
            var Item = new DFLPOS_SERVICES();

            Item.id = Model.ServiceId;
            Item.description = Model.Name;
            Item.enabled = Model.IsActive;
            Item.order = Model.Order;

            return _ServicesRepository.UpdateService(Item);
        }
        
        public bool DisableService(int ServiceId)
        {
            return _ServicesRepository.DisableService(ServiceId);
        }
    }
}
