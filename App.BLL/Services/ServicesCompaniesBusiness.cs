﻿using App.DAL.Services;
using App.Entities;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Services
{
    public class ServicesCompaniesBusiness
    {
        private ServicesCompaniesRepository _ServicesCompaniesRepository;

        public ServicesCompaniesBusiness()
        {
            _ServicesCompaniesRepository = new ServicesCompaniesRepository();
        }

        public List<DFLPOS_SERVICES_COMPANIES> ServicesCompanies()
        {
            return _ServicesCompaniesRepository.ServicesCompanies();
        }

        public List<ServiceCompany> GetCompaniesList()
        {
            return _ServicesCompaniesRepository.GetCompaniesList();
        }

        public bool AddCompany(ServiceCompany Model)
        {
            var Item = new DFLPOS_SERVICES_COMPANIES();
            Item.company = Model.Name;
            Item.enabled = Model.IsActive;
            Item.order = Model.Order;

            return _ServicesCompaniesRepository.AddCompany(Item);
        }

        public ServiceCompany GetServiceCompanyById(int CompanyId)
        {
            return _ServicesCompaniesRepository.GetServiceCompanyById(CompanyId);
        }

        public bool UpdateServiceCompany(ServiceCompany Model)
        {
            var Item = new DFLPOS_SERVICES_COMPANIES();

            Item.id = Model.CompanyId;
            Item.company = Model.Name;
            Item.enabled = Model.IsActive;
            Item.order = Model.Order;

            return _ServicesCompaniesRepository.UpdateServiceCompany(Item);
        }

        public bool DisableServiceCompany(int CompanyId)
        {
            return _ServicesCompaniesRepository.DisableServiceCompany(CompanyId);
        }
    }
}
