﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using App.Common;
using App.DAL.Barcodes;
using App.DAL.CommonAccess;
using App.Entities.ViewModels.ItemPresentation;
using App.DAL.ItemPresentation;
using App.Entities.ViewModels.Item;

namespace App.BLL.ItemPresentationBusiness
{
    public class ItemPresentationBusiness
    {
        private static Regex decimalRegex = new Regex(@"^\d+.90{0,3}$");
        readonly ItemPresentationRepository _presentations = new ItemPresentationRepository();
        readonly MaTaxRepository _maTaxRepository = new MaTaxRepository();
        readonly ItemMasterRepository _itemRepository = new ItemMasterRepository();
        readonly BarcodesRepository _barcodesRepository = new BarcodesRepository();

        public Result<List<ItemPresentationModel>> GetPresentationByProductId(string part_number)
        {
            return Result.OK(_presentations.GetPresentationByPartNumber(part_number));
        }

        public Result Add(ItemPresentationModel model, string user, string page)
        {
            var item = _itemRepository.GetItemByPartNumber(model.PartNumber);
            if (item == null)
                return Result.Error("Código de producto no existe", 10);
            if (_presentations.PresentationCodeExist(model.PresentationCode))
                return Result.Error("Ya existe una presentación con este código", 11);
            if (model.PresentationCode == item.part_number)
                return Result.Error("El código de presentación no puede ser el mismo que el código del producto", 11);
            if (!decimalRegex.IsMatch(model.TotalPrice.ToString()))
                return Result.Error("El precio total debe terminar en .90", 14);
            if (_itemRepository.GetItemByPartNumber(model.PresentationCode) != null)
                return Result.Error("El código de presentación no puede ser el mismo el código de un producto", 11);
            if (_barcodesRepository.BarcodeExist(model.PresentationCode))
                return Result.Error("El codigo de presentacion no esta disponible", 11);
            var ivaieps = _maTaxRepository.StoreProcedureIvaIeps(item.part_number);
            var priceIva = model.TotalPrice / (1 + ivaieps.Iva / 100);
            var ivaVal = model.TotalPrice - priceIva;
            var price = priceIva / (1 + ivaieps.Ieps / 100);
            var iepsVal = priceIva - price;
            model.IVA = ivaVal;
            model.IEPS = iepsVal;
            model.Price = price;
            return _presentations.Add(model, user, page) ? Result.OK() : Result.ServerError();
        }

        public Result Edit(ItemPresentationModel model, string user, string page)
        {
            var item = _itemRepository.GetItemByPartNumber(model.PartNumber);
            if (item == null)
                return Result.Error("Código de producto no exite", 10);
            if (!_presentations.PresentationExist(model.PresentationId))
                return Result.Error("No existe una presentación con este id", 13);
            var ivaieps = _maTaxRepository.StoreProcedureIvaIeps(item.part_number);
            var priceIva = model.TotalPrice / (1 + ivaieps.Iva / 100);
            var ivaVal = model.TotalPrice - priceIva;
            var price = priceIva / (1 + ivaieps.Ieps / 100);
            var iepsVal = priceIva - price;
            model.IVA = ivaVal;
            model.IEPS = iepsVal;
            model.Price = price;
            return _presentations.Edit(model, user, page) ? Result.OK() : Result.ServerError();
        }

        public Result Remove(int id)
        {
            return _presentations.Remove(id) ? Result.OK() : Result.ServerError();
        }

        public List<ItemDropDownModel> GetAllBasicInformation()
        {
            return _presentations.GetAllBasicInformation();
        }

        public ItemPresentationModel GetInformationBasic(string presentation_code)
        {
            return _presentations.GetInformationBasic(presentation_code);
        }

        public ItemPresentationModel GetPresentationById(string presentation_id)
        {
            return _presentations.GetPresentationById(presentation_id);
        }

        public string GetTablePresentation(ItemPresentationModel model)
        {
            string table = "<table align='center'><tr><th style='width: 220pxpx; color: white; background-color: #224191;'>Código</th><th style='width: 220pxpx; color: white; background-color: #224191;'>Descripción (Presentación)</th><th style='width: 220pxpx; color: white; background-color: #224191;'>Precio de Venta</th></tr>";
            table += "<tr><td style='width: 220pxpx;'> "+model.PartNumber+" (" + model.PresentationCode + ")</td><td style='width: 220pxpx;'>" + model.Name + "</td><td style='width: 220pxpx;'> $" + model.TotalPrice + "</td><tr>";
            table += "</table>";
            return table;
        }
    }
}