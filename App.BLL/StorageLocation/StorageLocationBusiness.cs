﻿using App.DAL.Site;
using App.DAL.StorageLocation;
using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.StorageLocation
{
    public class StorageLocationBusiness
    {
        private StorageLocationRepository _storageLocationRepository;

        public StorageLocationBusiness()
        {
            _storageLocationRepository = new StorageLocationRepository();
        }
        public List<StorageLocationModel> GetAllLocationsInStatusActive()
        {
            return _storageLocationRepository.GetAllLocationsInStatusActive();
        }   
    }
}