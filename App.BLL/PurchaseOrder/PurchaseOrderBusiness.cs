﻿using App.Entities;
using App.DAL.PurchaseOrder;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.InternalRequirement;
using App.BLL.Purchases;
using App.BLL.Site;

namespace App.BLL.PurchaseOrder
{
    public class PurchaseOrderBusiness
    {
        private readonly PurchaseOrderRepository _purchaseOrderRepository;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SiteBusiness _SiteBusiness;

        public PurchaseOrderBusiness()
        {
            _purchaseOrderRepository = new PurchaseOrderRepository();
            _supplierBusiness = new SupplierBusiness();
            _SiteBusiness = new SiteBusiness();
        }

        public int CreatePuchaseOrder(int folio, int supplier_id, string site_code, string cuser)
        {
            var model = new PURCHASE_ORDER()
            {
                folio_requirement = folio,
                supplier_id = supplier_id,
                site_code = site_code,
                purchase_date = DateTime.Now,
                purchase_status = 0,
                currency = "MX",
                cuser = cuser,
                cdate = DateTime.Now,
                program_id = "PURCH019.cshtml"
            };
            return _purchaseOrderRepository.CreatePuchaseOrder(model);
        }

        public int CreatePurchaseOrderItems(int purchase_no, List<AllItemSitesSupplier> items, int supplier_id, string cuser)
        {
            var tax = _supplierBusiness.GetValueSupplierPartIva(supplier_id);
            var itemsSum = items.GroupBy(g => g.part_number).Select(s => new { quantity = s.Sum(ss => ss.quantity), part_number = s.Key }).ToList();
            foreach (var item in itemsSum)
            {
                var price = items.Where(w => w.part_number == item.part_number).Select(s => s.price).FirstOrDefault();
                var iva = (Convert.ToInt32(item.quantity) * price) * tax;
                var model = new PURCHASE_ORDER_ITEM()
                {
                    purchase_no = purchase_no,
                    part_number = item.part_number,
                    quantity = Convert.ToInt32(item.quantity),
                    purchase_price = price,
                    iva_amount = iva,
                    item_total_amount = (Convert.ToInt32(item.quantity) * price) + iva,
                    cuser = cuser,
                    cdate = DateTime.Now,
                    program_id = "PURCH019.cshtml"
                };

                int result = _purchaseOrderRepository.CreatePurchaseOrderItems(model);
                if (result == 0)
                    return 0;
            }
            return 1;
        }

        public List<PurchaseOrderXMLModel> GetAllPurchasesOrdersXML(DateTime date1, DateTime date2, string siteCode, int supplier, string partNumber, string department, string family, string currency)
        {
            string SiteIP = _SiteBusiness.GetSiteIP(siteCode);
            var InformationPurchase = _purchaseOrderRepository.GetAllGenericPurchasesOrdersXML(date1, date2, SiteIP, supplier, partNumber, department, family, currency);
            InformationPurchase.ForEach(s =>
            {
                switch (s.purchase_status_value)
                {
                    case 1:
                        s.purchase_status_description = "Confirmada";
                        break;
                    case 8:
                        s.purchase_status_description = "Cancelada";
                        break;
                    case 9:
                        s.purchase_status_description = "Terminada";
                        break;
                    default:
                        s.purchase_status_description = "Escaneada";
                        break;
                }
            });
            return InformationPurchase;
        }

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetailStatusFormatView(string PoNumber, string SiteCode)
        {
            string SiteIP = _SiteBusiness.GetSiteIP(SiteCode);
            return _purchaseOrderRepository.NewGetAllPurchasesDetailStatus(PoNumber, SiteIP);
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllPurchasesOrdersStatusSupplierDetailXML(DateTime date1, DateTime date2, string siteCode, int supplier, string partNumber, string department, string family, string currency)
        {
            string SiteIP = _SiteBusiness.GetSiteIP(siteCode);
            return _purchaseOrderRepository.GetAllGenericPurchasesOrdersDetailXML(date1, date2, SiteIP, supplier, partNumber, department, family, currency);
        }
    }
}
