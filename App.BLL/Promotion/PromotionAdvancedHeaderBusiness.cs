﻿using App.DAL.Promotion;
using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Promotion
{
    public class PromotionAdvancedHeaderBusiness
    {
        private PromotionAdvancedHeaderRepository _promotionAdvRepository;

        public PromotionAdvancedHeaderBusiness()
        {
            _promotionAdvRepository = new PromotionAdvancedHeaderRepository();
        }

        public string AddPromotionAdvanced(PromotionModel modelDeteail, string buyer_division, string cuser)
        {
            var currentCulture = CultureInfo.CurrentCulture;
            var week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, currentCulture.DateTimeFormat.CalendarWeekRule, currentCulture.DateTimeFormat.FirstDayOfWeek);
            int countHeaders = _promotionAdvRepository.CountHeader();
            var nextNumber = "";
            if (countHeaders > 0)
            {
                var countHeadersString = countHeaders.ToString();
                int letras = countHeadersString.Length;
                var numero = 0;
                if (letras == 1 && countHeadersString != "9")
                {
                    numero = countHeaders + 1;
                    nextNumber = "000" + numero.ToString();
                }
                else if (letras == 1 && countHeadersString == "9")
                {
                    numero = countHeaders + 1;
                    nextNumber = "00" + numero.ToString();
                }
                else if (letras == 2)
                {
                    numero = countHeaders + 1;
                    nextNumber = "00" + numero.ToString();
                }
                else if (letras == 3)
                {
                    numero = countHeaders + 1;
                    nextNumber = "0" + numero.ToString();
                }
                else if (letras == 4)
                {
                    numero = countHeaders + 1;
                    nextNumber = numero.ToString();
                }
                else
                {
                    numero = countHeaders + 1;
                    nextNumber = numero.ToString();
                }
            }
            else
            {
                nextNumber = "0001";
            }
            modelDeteail.promotion_code = week.ToString() + DateTime.Now.Year.ToString() + nextNumber + "9";
            return _promotionAdvRepository.AddPromotionAdvanced(modelDeteail, buyer_division, cuser);
        }

        public PROMOTION_ADVANCED_HEADER GetPromotion(string promotion_code)
        {
            return _promotionAdvRepository.GetPromotion(promotion_code);
        }

        public bool PromotionHeaderEdit(PROMOTION_ADVANCED_HEADER model, ModelPromotionEdit ModelEdit, string User)
        {
            return _promotionAdvRepository.PromotionHeaderEdit(model, ModelEdit, User);
        }

        public bool PromotionHeaderDelete(PROMOTION_ADVANCED_HEADER model, string User)
        {
            return _promotionAdvRepository.PromotionHeaderDelete(model, User);
        }

        public bool PromotionHeaderActivate(string promotion_code, string User)
        {
            return _promotionAdvRepository.PromotionHeaderActivate(promotion_code, User);
        }
    }
}
