﻿using App.DAL.Promotion;
using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Promotion
{
    public class PromotionAdvancedDetailBusiness
    {
        private PromotionAdvancedDetailRepository _promotionAdvDetailRepository;
        private PromotionDetailBusiness _promotionDetailBusiness;
        private readonly ItemMasterBusiness _itemMasterBusiness;
        public PromotionAdvancedDetailBusiness()
        {
            _promotionAdvDetailRepository = new PromotionAdvancedDetailRepository();
            _promotionDetailBusiness = new PromotionDetailBusiness();
            _itemMasterBusiness = new ItemMasterBusiness();
        }

        public string AddPromotionAdvanced(List<ModelPromotionItemEdit> modelDetail, string promotion_code, string cuser)
        {
            string r = "";
            foreach (var item in modelDetail)
            {
                var Tax = _itemMasterBusiness.GetItemIvaIepsValue(item.part_number);
                r = _promotionAdvDetailRepository.AddPromotionDetail(item, promotion_code, Tax, cuser);
                if (r != "")
                    return r;
            }
            return r;

        }

        public List<PromotionEdit> GetItemsByPromotionCode(string promotion_code)
        {
            List<PromotionEdit> promoEdit = new List<PromotionEdit>();
            var items = _promotionAdvDetailRepository.GetPromotionDetailById(promotion_code);
            foreach (var item in items)
            {
                var infoItem = _promotionDetailBusiness.GetItemPriceIvaIeps(item.part_number);
                var Tax = _itemMasterBusiness.GetItemIvaIepsValue(item.part_number);
                var purchasePrice = new PromotionEdit()
                {
                    part_number = item.part_number,
                    part_description = infoItem.part_description,
                    price_iva_current = Decimal.Parse(infoItem.Price),
                    quantity_promotion = item.promotion_quantity,
                    total_price_current = Decimal.Parse(infoItem.Price) * 1,
                    total_price_promotion = ((1 * Tax.Iva) * item.promotion_price / 100) + item.promotion_price,
                    comun_flag = false,
                    delete_flag = false,
                    update_flag = false,
                    presentation_flag = false,
                    weight_flag = infoItem.weight_flag,
                    forced90 = infoItem.forced_90,
                    presentation_id = 0
                };
                promoEdit.Add(purchasePrice);
            }
            return promoEdit;
        }

        public List<PromotionCommonEdit> GetItemsByPromotionComunCode(string promotion_code)
        {
            List<PromotionCommonEdit> promoEdit = new List<PromotionCommonEdit>();
            var items = _promotionAdvDetailRepository.GetPromotionCommonByIdOnly(promotion_code);
            foreach (var item in items)
            {
                var infoItem = _promotionDetailBusiness.GetItemPriceIvaIeps(item.common);
                var purchasePrice = new PromotionCommonEdit()
                {
                    part_number_origin = item.common,
                    part_number = item.item,
                    part_description = infoItem.part_description,
                    promotion_past_price = infoItem.Price.ToString(),
                    delete_flag = false,
                    update_flag = false
                };
                promoEdit.Add(purchasePrice);
            }
            return promoEdit;
        }

        public bool UpdateItemByPromotionCode(List<ModelPromotionItemEdit> items, string promotion_code, string User)
        {
            if (items == null)
                return true;
            foreach (var item in items)
            {
                var tax = _itemMasterBusiness.GetItemIvaIepsValue(item.part_number);
                var promotionItem = _promotionAdvDetailRepository.UpdateItemByPromotionCode(item, tax.Iva, promotion_code, User);
                if (!promotionItem)
                    return false;
            }
            return true;
        }

        public bool UpdateItemDeleteByPromotionCode(List<ModelPromotionItemEdit> items, string promotion_code, string User)
        {
            if (items == null)
                return true;
            var newItems = items.Where(w => w.delete_flag == true).ToList();
            if (newItems != null)
            {
                foreach (var item in newItems)
                {
                    var updateCode = _promotionAdvDetailRepository.UpdateItemDeleteByPromotionCode(item.part_number, promotion_code, User);
                    if (!updateCode)
                        return false;
                }
            }
            return true;
        }
    }
}
