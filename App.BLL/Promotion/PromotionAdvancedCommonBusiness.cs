﻿using App.DAL.Promotion;
using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Promotion
{
    public class PromotionAdvancedCommonBusiness
    {
        private PromotionAdvancedCommonRepository _promotionAdvCommonRepo;

        public PromotionAdvancedCommonBusiness()
        {
            _promotionAdvCommonRepo = new PromotionAdvancedCommonRepository();
        }
        public string AddPromotionCommon(List<PromotionCommonEdit> itemsCommons, string promotion_code, string cuser)
        {
            string r = "";
            if (itemsCommons == null)
                return r;
            if (itemsCommons.Count() <= 0)
                return r;
            foreach (var item in itemsCommons)
            {
                r = _promotionAdvCommonRepo.AddPromotionCommon(item, promotion_code, cuser);
                if (r != "")
                    return r;
            }
            return r;
        }

        public bool UpdateCommonByPromotionCode(List<PromotionCommonEdit> items, string promotion_code, string user)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    var result = _promotionAdvCommonRepo.UpdateCommonByPromotionCode(item.part_number_origin, item.part_number, promotion_code, user);
                    if (!result)
                        return false;
                }
            }
            return true;

        }

        public bool UpdateCommonDeleteByPromotionCode(List<PromotionCommonEdit> items, string promotion_code, string user)
        {
            if (items == null)
                return true;
            var newItems = items.Where(w => w.delete_flag == true).ToList();
            if (newItems != null)
            {
                foreach (var item in newItems)
                {
                    var result = _promotionAdvCommonRepo.UpdateCommonDeleteByPromotionCode(item.part_number_origin, item.part_number, promotion_code, user);
                    if (!result)
                        return false;
                }
            }
            return true;
        }

        public List<PromotionItem> GetPromotionAdvancedCommonByCode(string promotion_code, string common)
        {
            return _promotionAdvCommonRepo.GetPromotionAdvancedCommonByCode(promotion_code, common);
        }

    }
}
