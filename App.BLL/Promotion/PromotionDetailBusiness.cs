﻿using App.BLL.CommonBusiness;
using App.DAL.Promotion;
using App.Entities;
using App.Entities.ViewModels.Item;
using App.BLL.ItemPresentationBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Promotion;
using App.Entities.ViewModels.Site;
using App.BLL.Site;
using App.BLL.MaCode;

namespace App.BLL.Promotion
{
    public class PromotionDetailBusiness
    {
        private PromotionDetailRepository _promotionDetailRepository;
        private readonly ItemMasterRepository _itemMasterRepo;
        private readonly SysMaTaxBusiness _MaTaxBusiness;
        private readonly ItemPresentationBusiness.ItemPresentationBusiness _itemPresentationBusines;
        private readonly SiteBusiness siteBusiness;
        private readonly ItemMasterBusiness _itemMasterBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;

        //constructor
        public PromotionDetailBusiness()
        {
            _promotionDetailRepository = new PromotionDetailRepository();
            _itemMasterRepo = new ItemMasterRepository();
            _MaTaxBusiness = new SysMaTaxBusiness();
            _itemPresentationBusines = new ItemPresentationBusiness.ItemPresentationBusiness();
            siteBusiness = new SiteBusiness();
            _itemMasterBusiness = new ItemMasterBusiness();
            _maCodeBusiness = new MaCodeBusiness();
        }
        public string AddPromotionDetail(List<ModelPromotionItemEdit> promotionDetail, string promotion_code, string cuser)
        {
            string r = "";
            foreach (var item in promotionDetail)
            {
                if (item.presentation_flag)
                {
                    var presentationInformation = _itemMasterBusiness.GetInfoPresentationByIdInt(item.presentation_id);
                    var Tax = _itemMasterBusiness.GetItemIvaIepsValue(presentationInformation.PartNumber);
                    r = _promotionDetailRepository.AddPromotionDetail(item, promotion_code, Tax, cuser);
                    if (r != "")
                        return r;
                }
                else
                {
                    var Tax = _itemMasterBusiness.GetItemIvaIepsValue(item.part_number);
                    r = _promotionDetailRepository.AddPromotionDetail(item, promotion_code, Tax, cuser);
                    if (r != "")
                        return r;
                }
            }
            return r;
        }



        public ItemCommon GetItemPriceIvaIeps(string part_number)
        {
            var item = _itemMasterRepo.GetItemPriceByPartNumber(part_number);
            var itemIvaIeps = _MaTaxBusiness.GetIvaIepsByIdValue(part_number);
            ItemCommon itemC = new ItemCommon();
            itemC.Ieps = itemIvaIeps.Ieps;
            itemC.Iva = itemIvaIeps.Iva;
            if (item != null)
                itemC.weight_flag = item.weight_flag;
            else
                itemC.weight_flag = false;
            if (item == null)
            {
                var itemPresentation = _itemPresentationBusines.GetInformationBasic(part_number);
                if (itemPresentation != null)
                {
                    itemC.part_description = itemPresentation.Name;
                    itemC.Price = itemPresentation.TotalPrice.ToString();
                    itemC.Presentation = true;
                    itemC.presentation_id = itemPresentation.PresentationId;
                    itemC.forced_90 = _maCodeBusiness.ValidateItemForce90(itemPresentation.PartNumber);
                    return itemC;
                }
                else
                {
                    var infoItem = _itemMasterBusiness.GetItemByPartNumber(part_number);
                    itemC.part_description = infoItem.part_description;
                    itemC.Presentation = false;
                    itemC.Price = "0";
                    itemC.presentation_id = 0;
                    itemC.forced_90 = _maCodeBusiness.ValidateItemForce90(part_number);
                    return itemC;
                }
            }
            else
            {
                itemC.part_description = item.part_description;
                itemC.Price = item.price;
                itemC.Presentation = false;
                itemC.presentation_id = 0;
                itemC.forced_90 = _maCodeBusiness.ValidateItemForce90(part_number);
                return itemC;
            }

        }

        public List<PROMOTION_DETAIL> GetPromotionDetailById(string promotion_code)
        {
            return _promotionDetailRepository.GetPromotionDetailById(promotion_code);
        }
        public List<SiteModel> GetPromotionsSite(string promotion_code, string type_promotion)
        {
            List<SiteModel> sitesAll = new List<SiteModel>();
            sitesAll.Add(new SiteModel { SiteName = "Todas las Tiendas", SiteCode = "All" });
            var sites = siteBusiness.GetAllSitesOfertsViewModel();
            sitesAll.AddRange(sites);
            var sitesSelect = _promotionDetailRepository.GetPromotionsSite(promotion_code, type_promotion);
            foreach (var sitesS in sitesSelect)
            {
                foreach (var site in sitesAll)
                    if (sitesS.SiteCode == site.SiteCode)
                        site.Status = 1;
            }
            return sitesAll;
        }

        public List<PromotionEdit> GetItemsByPromotionCode(string promotion_code)
        {
            List<PromotionEdit> promoEdit = new List<PromotionEdit>();
            var items = _promotionDetailRepository.GetPromotionDetailById(promotion_code);
            foreach (var item in items)
            {
                if (item.presentation_id == null)
                {
                    var infoItem = GetItemPriceIvaIeps(item.part_number);
                    var Tax = _itemMasterBusiness.GetItemIvaIepsValue(item.part_number);
                    var price_show = (item.promotion_price_iva == null) ? (((1 * Tax.Iva) * item.promotion_price / 100) + item.promotion_price) : item.promotion_price_iva ?? 0;
                    var purchasePrice = new PromotionEdit()
                    {
                        part_number = item.part_number,
                        part_description = infoItem.part_description,
                        price_iva_current = Decimal.Parse(infoItem.Price),
                        quantity_promotion = 1,
                        total_price_current = Decimal.Parse(infoItem.Price) * 1,
                        total_price_promotion = price_show,
                        comun_flag = false,
                        delete_flag = false,
                        update_flag = false,
                        presentation_flag = false,
                        weight_flag = infoItem.weight_flag,
                        forced90 = infoItem.forced_90,
                        presentation_id = 0
                    };
                    promoEdit.Add(purchasePrice);
                }
                else
                {
                    var presentationInformation = _itemMasterBusiness.GetInfoPresentationByIdInt(item.presentation_id ?? 0);
                    string[] part_number = presentationInformation.PresentationCode.Split(',');
                    var infoItem = GetItemPriceIvaIeps(part_number[0]);
                    var Tax = _itemMasterBusiness.GetItemIvaIepsValue(part_number[0]);
                    var price_show = (item.promotion_price_iva == null) ? (((1 * Tax.Iva) * item.promotion_price / 100) + item.promotion_price) : item.promotion_price_iva ?? 0;
                    var purchasePrice = new PromotionEdit()
                    {
                        part_number = part_number[0],
                        part_description = presentationInformation.Name,
                        price_iva_current = Decimal.Parse(infoItem.Price),
                        quantity_promotion = 1,
                        total_price_current = Decimal.Parse(infoItem.Price) * 1,
                        total_price_promotion = price_show,
                        comun_flag = false,
                        delete_flag = false,
                        update_flag = false,
                        presentation_flag = true,
                        weight_flag = infoItem.weight_flag,
                        forced90 = infoItem.forced_90,
                        presentation_id = item.presentation_id ?? 0
                    };
                    promoEdit.Add(purchasePrice);
                }
            }
            return promoEdit;
        }
        public bool UpdateItemByCode(List<ModelPromotionItemEdit> items, string promotion_code, string User)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    if (item.presentation_flag)
                    {
                        var presentationInformation = _itemMasterBusiness.GetInfoPresentationByIdInt(item.presentation_id);
                        var tax = _itemMasterBusiness.GetItemIvaIepsValue(presentationInformation.PartNumber);
                        var promotionItem = _promotionDetailRepository.UpdateItemByCode(item, tax.Iva, promotion_code, User);
                        if (!promotionItem)
                            return false;
                    }
                    else
                    {
                        var tax = _itemMasterBusiness.GetItemIvaIepsValue(item.part_number);
                        var promotionItem = _promotionDetailRepository.UpdateItemByCode(item, tax.Iva, promotion_code, User);
                        if (!promotionItem)
                            return false;
                    }
                }
            }
            return true;
        }


        public bool UpdateItemDeleteByCode(List<ModelPromotionItemEdit> items, string promotion_code, string User)
        {
            if (items == null)
                return true;

            var newItems = items.Where(w => w.delete_flag == true).ToList();
            if (newItems != null)
            {
                foreach (var item in newItems)
                {
                    if (item.presentation_flag)
                    {
                        var presentationInformation = _itemMasterBusiness.GetInfoPresentationByIdInt(item.presentation_id);
                        var tax = _itemMasterBusiness.GetItemIvaIepsValue(presentationInformation.PartNumber);
                        var updateCode = _promotionDetailRepository.UpdateItemDeleteByCode(item, promotion_code, User);
                        if (!updateCode)
                            return false;
                    }
                    else
                    {
                        var updateCode = _promotionDetailRepository.UpdateItemDeleteByCode(item, promotion_code, User);
                        if (!updateCode)
                            return false;
                    }
                }
            }
            return true;
        }


        //patricio cazarez
        public bool AddPromotionDetailDFLPOS(DFLPOS_SALES_PROMOTIONS_DETAIL promotionDetail)
        {
            bool check = false;
            if (_promotionDetailRepository.AddPromotionDetailDFLPOS(promotionDetail) > 0) { check = true; }
            return check;
        }
    }
}
