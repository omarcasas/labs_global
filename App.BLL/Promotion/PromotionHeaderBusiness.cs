﻿using App.DAL.Promotion;
using App.Entities;
using App.Entities.ViewModels.Promotion;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Promotion
{
    public class PromotionHeaderBusiness
    {
        private PromotionHeaderRepository _promotionHeaderRepository;
        private UserMasterBusiness _userMasterBusiness;

        //constructor
        public PromotionHeaderBusiness()
        {
            _promotionHeaderRepository = new PromotionHeaderRepository();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public string AddPromotionHeader(PromotionModel modelHeader, string buyer_division, string cuser)
        {
            var currentCulture = CultureInfo.CurrentCulture;
            var week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, currentCulture.DateTimeFormat.CalendarWeekRule, currentCulture.DateTimeFormat.FirstDayOfWeek);
            int countHeaders = _promotionHeaderRepository.CountHeader();
            var nextNumber = "";
            if (countHeaders > 0)
            {
                var countHeadersString = countHeaders.ToString();
                int letras = countHeadersString.Length;
                var numero = 0;
                if (letras == 1 && countHeadersString != "9")
                {
                    numero = countHeaders + 1;
                    nextNumber = "000" + numero.ToString();
                }
                else if (letras == 1 && countHeadersString == "9")
                {
                    numero = countHeaders + 1;
                    nextNumber = "00" + numero.ToString();
                }
                else if (letras == 2)
                {
                    numero = countHeaders + 1;
                    nextNumber = "00" + numero.ToString();
                }
                else if (letras == 3)
                {
                    numero = countHeaders + 1;
                    nextNumber = "0" + numero.ToString();
                }
                else if (letras == 4)
                {
                    numero = countHeaders + 1;
                    nextNumber = numero.ToString();
                }
                else
                {
                    numero = countHeaders + 1;
                    nextNumber = numero.ToString();
                }
            }
            else
            {
                nextNumber = "0001";
            }

            modelHeader.promotion_code = week.ToString() + DateTime.Now.Year.ToString() + nextNumber;
            return _promotionHeaderRepository.AddPromotionHeader(modelHeader, buyer_division, cuser);
        }

        //public List<PromotionModel> GetSearchAllPromotion(DateTime start_date, DateTime? end_date, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetSearchAllPromotion(start_date, end_date, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}
        //public List<PromotionModel> GetSearchPromotionByPartNumber(DateTime start_date, DateTime? end_date, string part_number, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetSearchPromotionByPartNumber(start_date, end_date, part_number, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        //public List<PromotionModel> GetSearchPromotionBySite(DateTime start_date, DateTime? end_date, string site_code, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetSearchPromotionBySite(start_date, end_date, site_code, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        //public List<PromotionModel> GetSearchPromotionByPartNumberBySite(DateTime start_date, DateTime? end_date, string part_number, string site_code, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetSearchPromotionByPartNumberBySite(start_date, end_date, part_number, site_code, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        //public List<PromotionModel> GetPromotions(DateTime? BeginDate, DateTime? EndDate2, string site_code, string item, string type, bool AllStores)
        //{
        //    DateTime date = DateTime.Today;
        //    //var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
        //    //var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
        //    DateTime BeginDate2 = BeginDate ?? date;
        //    //DateTime EndDate2 = EndDate ?? lastDayOfMonth;
        //    if (site_code == "" && type == "" && item == "")
        //    {
        //        var promotions = GetSearchAllPromotion(BeginDate2, EndDate2, AllStores);
        //        return promotions;
        //    }
        //    else if(site_code == "" && item != "" && type == "")
        //    {
        //        var promotions = GetSearchPromotionByPartNumber(BeginDate2, EndDate2, item, AllStores);
        //        return promotions;
        //    }
        //    else if (site_code != "" && item != "" && type == "")
        //    {
        //        var promotions = GetSearchPromotionByPartNumberBySite(BeginDate2, EndDate2, item, site_code, AllStores);
        //        return promotions;
        //    }
        //    else if (site_code != "" && item == "" && type == "")
        //    {
        //        var promotions = GetSearchPromotionBySite(BeginDate2, EndDate2, site_code, AllStores);
        //        return promotions;
        //    }
        //    else if (site_code != "" && item != "" && type != "")
        //    {
        //        var promotions = GetPromotionPartNumberSiteType(BeginDate2, EndDate2, item, site_code, type, AllStores);
        //        return promotions;
        //    }
        //    else if (site_code == "" && item != "" && type != "")
        //    {
        //        var promotions = GetPromotionPartNumberType(BeginDate2, EndDate2, item, type, AllStores);
        //        return promotions;
        //    }
        //    else if (site_code == "" && item == "" && type != "")
        //    {
        //        var promotions = GetPromotionPartNumberType(BeginDate2, EndDate2, type, AllStores);
        //        return promotions;
        //    }
        //    else if (site_code != "" && item == "" && type != "")
        //    {
        //        var promotions = GetPromotionPartNumberTypeSite(BeginDate2, EndDate2, type, site_code, AllStores);
        //        return promotions;
        //    }
        //    return null;
        //}

        public List<PromotionItem> GetPromotionAdvancedByCode(string promotion_code)
        {
            return _promotionHeaderRepository.GetPromotionAdvancedByCode(promotion_code);
        }

        public List<PromotionItem> GetPromotionByCode(string promotion_code)
        {
            return _promotionHeaderRepository.GetPromotionAdvancedByCode(promotion_code);
        }

        public List<PromotionItem> GetPromotionId(string promotion_code, string type)
        {
            if (type == "AVANZADA")
                return _promotionHeaderRepository.GetPromotionAdvancedByCode(promotion_code);
            else
                return _promotionHeaderRepository.GetPromotionByCode(promotion_code);
        }

        //public List<PromotionModel> GetPromotionPartNumberSiteType(DateTime start_date, DateTime? end_date, string part_number, string site_code, string type, bool AllStores)
        //{
        //    return _promotionHeaderRepository.GetPromotionDatesPartNumberSiteType(start_date, end_date, part_number, site_code, type, AllStores);
        //}

        //public List<PromotionModel> GetPromotionPartNumberType(DateTime start_date, DateTime? end_date, string part_number, string type, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetPromotionPartNumberType(start_date, end_date, part_number, type, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        //public List<PromotionModel> GetPromotionPartNumbersiteType(DateTime start_date, DateTime? end_date, string part_number, string site_code, string type, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetPromotionPartNumberSiteType(start_date, end_date, part_number, site_code, type, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        //public List<PromotionModel> GetPromotionPartNumberType(DateTime start_date, DateTime? end_date, string type, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetPromotionPartNumberType(start_date, end_date, type, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        //public List<PromotionModel> GetPromotionPartNumberTypeSite(DateTime start_date, DateTime? end_date, string type, string site_code, bool AllStores)
        //{
        //    var promotions = _promotionHeaderRepository.GetPromotionPartNumberTypeSite(start_date, end_date, type, site_code, AllStores);
        //    foreach (var promotion in promotions)
        //    {
        //        promotion.promotion_create_user = _userMasterBusiness.getFirstLastName(promotion.promotion_create_user);
        //    }
        //    return promotions;
        //}

        public List<SiteModel> GetPromotionSites(string promotion_code)
        {
            return _promotionHeaderRepository.GetPromotionSites(promotion_code);
        }

        public PROMOTION_HEADER GetPromotion(string promotion_code)
        {
            return _promotionHeaderRepository.GetPromotion(promotion_code);
        }

        public bool PromotionHeaderEdit(PROMOTION_HEADER model, ModelPromotionEdit ModelEdit, string User)
        {
            return _promotionHeaderRepository.PromotionHeaderEdit(model, ModelEdit, User);
        }

        public bool PromotionHeaderDelete(PROMOTION_HEADER model, string User)
        {
            return _promotionHeaderRepository.PromotionHeaderDelete(model, User);
        }

        public bool PromotionHeaderActivate(string promotion_code, string User)
        {
            return _promotionHeaderRepository.PromotionHeaderActivate(promotion_code, User);
        }

        //patricio cazarez
        public bool AddPromotionDFLPOS(DFLPOS_SALES_PROMOTIONS promotionDFLPOS)
        {
            bool check = false;

            if (_promotionHeaderRepository.AddPromotionDFLPOS(promotionDFLPOS) > 0) { check = true; }
            return check;
        }
    }
}
