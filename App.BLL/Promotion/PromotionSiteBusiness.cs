﻿using App.BLL.Site;
using App.DAL.Promotion;
using App.Entities;
using App.Entities.ViewModels.Promotion;
using App.Entities.ViewModels.SysInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Promotion
{
    public class PromotionSiteBusiness
    {
        private PromotionSiteRepository _promotionSiteRepository;
        private SiteBusiness _siteBusiness;
        private PromotionAdvancedHeaderBusiness _promotionAdvancedHeaderBusiness;
        private PromotionHeaderBusiness _promotionHeaderBusiness;

        //constructor
        public PromotionSiteBusiness()
        {
            _promotionSiteRepository = new PromotionSiteRepository();
            _siteBusiness = new SiteBusiness();
            _promotionAdvancedHeaderBusiness = new PromotionAdvancedHeaderBusiness();
            _promotionHeaderBusiness = new PromotionHeaderBusiness();
        }

        public List<PromotionModel> GetPromotions(string Site_code, string Type_Promotion, string Part_Number, DateTime? BeginDate, DateTime? EndDate, bool allstores, int onlyStatus)
        {
            return _promotionSiteRepository.GetPromotions(Site_code, Type_Promotion, Part_Number, BeginDate, EndDate, allstores, onlyStatus);
        }

        public List<PromotionModel> GetPromotionsDetails(string Site_code, string Type_Promotion, string Part_Number, DateTime? BeginDate, DateTime? EndDate, bool allstores, int onlyStatus)
        {
            return _promotionSiteRepository.GetPromotionsDetails(Site_code, Type_Promotion, Part_Number, BeginDate, EndDate, allstores, onlyStatus);
        }

        public List<PromotionModel> GetPromotionRepeat(PromotionModel model, List<ModelPromotionItemEdit> items, string[] sitesPromotion)
        {
            List<PromotionModel> promotionRepeat = new List<PromotionModel>();
            if (sitesPromotion[0].ToString() == "All")
            {
                var sites = _siteBusiness.GetAllSitesOferts();
                foreach (var site in sites)
                {
                    foreach (var item in items)
                    {
                        if (site.site_code.ToString() != "")
                        {
                            var promotions = GetPromotions(site.site_code.ToString(), model.promotion_type, item.part_number, model.promotion_start_date, model.promotion_end_date, true, 9);
                            if (promotions != null)
                            {
                                var promotionNew = promotions.Where(w => !promotionRepeat.Select(s => s.promotion_code).Contains(w.promotion_code)).ToList();
                                if (promotionNew.Count() > 0)
                                    promotionRepeat.AddRange(promotionNew);
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var site in sitesPromotion)//Las tienda elegidas
                {
                    foreach (var item in items)
                    {
                        if (site != "")
                        {
                            var promotions = GetPromotions(site, model.promotion_type, item.part_number, model.promotion_start_date, model.promotion_end_date, true, 9);
                            if (promotions != null)
                            {
                                var promotionNew = promotions.Where(w => !promotionRepeat.Select(s => s.promotion_code).Contains(w.promotion_code)).ToList();
                                if (promotionNew.Count() > 0)
                                    promotionRepeat.AddRange(promotionNew);
                            }
                        }
                    }
                }
            }
            return promotionRepeat;
        }

        public string AddPromotionSiteAll(string[] sitesPromotion, string promotion_code, string promotion_type, string cuser)
        {
            string r = "";
            if (sitesPromotion[0].ToString() == "All")
            {
                var sites = _siteBusiness.GetAllSitesOferts();
                foreach (var site in sites)
                {
                    r = _promotionSiteRepository.AddPromotionSite(promotion_code, site.site_code.ToString(), promotion_type, cuser); //Todas las tiendas activas
                    if (r != "")
                        return r;
                }
            }
            else
            {
                foreach (var site in sitesPromotion)//Las tienda elegidas
                {
                    r = _promotionSiteRepository.AddPromotionSite(promotion_code, site.ToString(), promotion_type, cuser);
                    if (r != "")
                        return r;
                }
            }
            return r;
        }

        public bool UpdateSiteByCode(string sites, string promotion_code, string promotion_type, string User)
        {
            var sitesNew = sites.Split(',');
            foreach (var site in sitesNew)
            {
                var siteUpdate = _promotionSiteRepository.UpdateSiteByCode(site.ToString(), promotion_code, promotion_type, User);
                if (!siteUpdate)
                    return false;
            }
            return true;
        }

        public bool UpdateSiteDeleteByCode(string sites, string promotion_code, string promotion_type, string User)
        {
            if (sites != null || sites == "")
            {
                var sitesNew = sites.Split(',');
                foreach (var site in sitesNew)
                {
                    var siteUpdate = _promotionSiteRepository.UpdateSiteDeleteByCode(site.ToString(), promotion_code, promotion_type, User);
                    if (!siteUpdate)
                        return false;
                }
            }
            return true;
        }

        public bool UpdateSiteDeleteAll(string promotion_code, string promotion_type, string User)
        {
            return _promotionSiteRepository.UpdateSiteDeleteAll(promotion_code, promotion_type, User);
        }

        public bool DeleteSiteByCode(string promotion_code, string promotion_type, string User)
        {
            return _promotionSiteRepository.DeleteSiteByCode(promotion_code, promotion_type, User);
        }

        public bool UpdateAdvancedSiteByCode(string sites, string promotion_code, string User)
        {
            var sitesNew = sites.Split(',');
            foreach (var site in sitesNew)
            {
                if (site != "")
                {
                    var siteUpdate = _promotionSiteRepository.UpdateAdvancedSiteByCode(site, promotion_code, User);
                    if (!siteUpdate)
                        return false;
                }
            }
            return true;
        }

        public bool UpdateAdvancedSiteDeleteByCode(string sites, string promotion_code, string User)
        {
            if (sites != null)
            {
                var sitesNew = sites.Split(',');
                foreach (var site in sitesNew)
                {
                    if (site != "")
                    {
                        var siteUpdate = _promotionSiteRepository.UpdateAdvancedSiteDeleteByCode(site, promotion_code, User);
                        if (!siteUpdate)
                            return false;
                    }
                }
            }
            return true;
        }

        public StoreProcedureResult ReplicatePromotions(string promotion_code, string promotion_type, string site_code)
        {
            if (site_code == "")
                site_code = "0000";
            return _promotionSiteRepository.ReplicatePromotions(promotion_code, promotion_type, site_code);
        }

        public bool ActivatePromotion(string promotion_code, string promotion_type, string User)
        {
            if (promotion_type != "AVANZADA")
                return _promotionHeaderBusiness.PromotionHeaderActivate(promotion_code, User);
            else
                return _promotionAdvancedHeaderBusiness.PromotionHeaderActivate(promotion_code, User);
        }

        public bool ActivatePromotionSites(string promotion_code, string promotion_type, string User)
        {
            return _promotionSiteRepository.ActivatePromotionSites(promotion_code, promotion_type, User);
        }
        //patricio cazarez
        public bool AddPromotionSiteDFLPOS(DFLPOS_SALES_PROMOTIONS_SITES promotionSite)
        {
            bool check = false;
            if (_promotionSiteRepository.AddPromotionSiteDFLPOS(promotionSite) > 0) { check = true; }
            return check;
        }
        public bool AddPromotionSiteDFLPOSOnSites(DFLPOS_SALES_PROMOTIONS _pro, List<DFLPOS_SALES_PROMOTIONS_DETAIL> _listsDetails, string site)
        {
            bool check = false;
            if (_promotionSiteRepository.AddPromotionSiteDFLPOSOnSites(_pro, _listsDetails, site) > 0)
            {
                check = true;
            }
            return check;
        }
    }
}
