﻿using App.DAL.Donations;
using App.Entities;
using App.Entities.ViewModels.Donations;
using System.Collections.Generic;

namespace App.BLL.Donations
{
    public class DonationsItemBusiness
    {
        private DonationsItemsRepository _DonationsItemRepository;

        public DonationsItemBusiness()
        {
            _DonationsItemRepository = new DonationsItemsRepository();
        }


        public List<DonationItem> GetItemsByDonationNumber(int DonationId)
        {
            return _DonationsItemRepository.GetItemsByDonationNumber(DonationId);
        }

        public bool AddArticle(DonationItem Model)
        {
            var Item = new DFLPOS_DONATION_ITEMS();

            Item.active_flag = Model.IsActive;
            Item.item_description = Model.Article;
            Item.item_price = Model.Price;
            Item.donation_id = Model.DonationId;

            return _DonationsItemRepository.AddArticle(Item);
        }

        public DonationItem GetArticleById(int ArticleId)
        {
            return _DonationsItemRepository.GetArticleById(ArticleId);
        }

        public bool UpdateArticle(DonationItem Item)
        {
            var ItemEdit = new DFLPOS_DONATION_ITEMS();

            ItemEdit.active_flag = Item.IsActive;
            ItemEdit.donation_id = Item.DonationId;
            ItemEdit.item_description = Item.Article;
            ItemEdit.item_id = Item.Id;
            ItemEdit.item_price = Item.Price;

            return _DonationsItemRepository.UpdateArticle(ItemEdit);
        }
    }    
}
