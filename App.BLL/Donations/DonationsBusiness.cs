﻿using App.DAL.Donations;
using App.Entities;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Donations
{
    public class DonationsBusiness
    {
        private DonationsRepository _DonationsRepository;

        public DonationsBusiness()
        {
            _DonationsRepository = new DonationsRepository();
        }

        public bool AddDonationCampaign(Donation Model)
        {
            var Donation = new DFLPOS_DONATIONS();

            Donation.initial_date = DateTime.ParseExact(Model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Donation.final_date = DateTime.ParseExact(Model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Donation.active_flag = true;
            Donation.accumulated = Model.AmountRaised;
            Donation.name = Model.Name;
            Donation.organization = Model.Organization;

            return _DonationsRepository.AddDonationCampaign(Donation);
        }

        public List<Donation> GetDonationsCampaigns()
        {
            return _DonationsRepository.GetDonationsCampaigns();
        }

        public Donation GetDonationById(int DonationId)
        {
            return _DonationsRepository.GetDonationById(DonationId);
        }

        public bool UpdateDonationCampaigns(Donation Model)
        {
            var Item = new DFLPOS_DONATIONS();

            Item.initial_date = DateTime.ParseExact(Model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Item.final_date = DateTime.ParseExact(Model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Item.donation_id = Model.Id;
            Item.accumulated = Model.AmountRaised;
            Item.name = Model.Name;
            Item.organization = Model.Organization;
            Item.active_flag = true;

            return _DonationsRepository.UpdateDonationCampaigns(Item);
        }

        public bool DeleteCampaign(int DonationdId)
        {
            return _DonationsRepository.DeleteCampaign(DonationdId);
        }
    }
}
