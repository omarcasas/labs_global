﻿using App.DAL.Purchases;
using App.Entities;
using System.Collections.Generic;

namespace App.BLL.Purchases
{
    public class NegotiatonBusiness
    {
        private NegotiatonRepository _repository;

        public NegotiatonBusiness()
        {
            _repository = new NegotiatonRepository();
        }
        public NEGOTIATION_DETAIL GetNegotiatonDetailById(int NegotiatonDetailId, int MasterId)
        {

            return _repository.GetNegotiatonDetailById(NegotiatonDetailId, MasterId);

        }
        public NEGOTIATION_MASTER ValidateNegotiatonMasterIfExist(int supplierId)//, string BuyerNumber)
        {
            return _repository.ValidateNegotiatonMasterIfExist(supplierId);//, BuyerNumber);
        }
        public bool AddNegotiaton(NEGOTIATION_MASTER Negotiaton)
        {
            var check = false;
            var flag = _repository.AddNegotiaton(Negotiaton);

            if (flag > 0) check = true;

            return check;
        }
        public bool AddNegotiatonDetail(NEGOTIATION_DETAIL Negotiaton)
        {
            var check = false;
            var flag = _repository.AddNegotiatonDetail(Negotiaton);

            if (flag > 0) check = true;

            return check;
        }
        public List<NEGOTIATION_DETAIL> GetSupplierNegociationBySupplierId(int negotiatonId, string user)
        {
            List<NEGOTIATION_DETAIL> items = new List<NEGOTIATION_DETAIL>();
            if (user != null)
            {
                var NegotiatonList = _repository.GetSupplierNegociationBySupplierId(negotiatonId);

                foreach (var i in NegotiatonList)
                {
                    items.Add(new NEGOTIATION_DETAIL { nd_id = i.nd_id, nm_id = i.nm_id, status = i.status, valid_until = i.valid_until, negotiation_type = i.negotiation_type, payment_conditions = i.payment_conditions, delivery_points = i.delivery_points, supplier_references = i.supplier_references, general_notes = i.general_notes, negotiatied_discounts = i.negotiatied_discounts, conditions_prices = i.conditions_prices, penalties = i.penalties, quality_level = i.quality_level, packaging_type = i.packaging_type, filebase64 = i.filebase64, renovated_from = i.renovated_from, cuser = i.cuser, cdate = i.cdate, uuser = i.uuser, udate = i.udate, program = i.program });
                }
                return items;
            }
            return items;
        }
        public bool UpdateNegotiatonDetail(NEGOTIATION_DETAIL NegotiatonDetail)
        {
            var check = false;
            if (_repository.UpdateNegotiatonDetail(NegotiatonDetail) > 0) check = true;
            return check;
        }
        public bool AddNegotiatonMaCodeCatalogue(MA_CODE Negotiaton)
        {
            var check = false;
            var flag = _repository.AddNegotiatonMaCodeCatalogue(Negotiaton);

            if (flag > 0) check = true;

            return check;
        }
    }
}
