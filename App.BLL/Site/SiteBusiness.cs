﻿using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.PRT;

namespace App.BLL.Site
{
    public class SiteBusiness
    {
        private SiteRepository _siteRepository;

        //constructor
        public SiteBusiness()
        {
            _siteRepository = new SiteRepository();
        }

        public List<SITES> GetAllSites()
        {
            return _siteRepository.GetAllSites();
        }

        public List<SiteModel> GetAllSitesModel()
        {
            return _siteRepository.GetAllSitesModel();
        }

        public List<SITES> GetAllSitesOferts()
        {
            //return _siteRepository.GetAllSitesOferts();
            return _siteRepository.GetAllSitesOferts().Where(w => w.new_erp == true).ToList();
        }

        public List<SiteModel> GetAllSitesOfertsViewModel()
        {
            //return _siteRepository.GetAllSitesOfertsViewModel();
            return _siteRepository.GetAllSitesOfertsViewModel().Where(w => w.NewErp == true).ToList();
        }
        public List<SiteModel> GetAllSitesName()
        {
            //var sites = _siteRepository.GetAllSitesName();
            return _siteRepository.GetAllSitesName().Where(w => w.NewErp == true).ToList();
        }

        public List<SiteModel> GetAllSitesNameAll()
        {
            //var sites = _siteRepository.GetAllSitesName(); //Se agrega a 0094 para 'reportear' en una Vista
            return _siteRepository.GetAllSitesName().Where(w => (w.SiteCode == "0094" || w.SiteCode == "0004") || w.NewErp == true).ToList();
        }

        public SitesPOModel GetSiteAllInformation(string site_code)
        {
            return _siteRepository.GetSiteAllInformation(site_code);
        }

        public string GetSiteNameByCode(string site_code)
        {
            return _siteRepository.GetSiteNameByCode(site_code);
        }
        public List<SiteModel> GetAllSitesStore()
        {
            var list = _siteRepository.GetAllSitesStore();
            return list;
        }

        public string GetSiteIP(string site_code) => _siteRepository.GetSiteIP(site_code);
    }
}