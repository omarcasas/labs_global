﻿using App.DAL.SalesCalendar;
using App.Entities;
using App.Entities.ViewModels.ServicesCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.ServicesCalendar
{
    public class ServicesCalendarHeaderBusiness
    {
        private readonly ServicesCalendarHeaderRepository _servicesCalendarHeaderRepository;

        public ServicesCalendarHeaderBusiness()
        {
            _servicesCalendarHeaderRepository = new ServicesCalendarHeaderRepository();
        }

        public int AddServicesCalendar(string ServiceDate, string SiteCode, string User)
        {
            SERVICES_CALENDAR service = new SERVICES_CALENDAR
            {
                service_date = DateTime.Parse(ServiceDate).Date,
                site_code = SiteCode,
                service_status = 1,
                cuser = User,
                cdate = DateTime.Now,
                program_id = "MRO001.cshtml"
            };
            return _servicesCalendarHeaderRepository.AddServicesCalendarHeader(service);
        }

        public int GetServiceCalendarHeaderId(string ServiceDate, string SiteCode)
        {
            return _servicesCalendarHeaderRepository.GetServiceCalendarHeaderId(DateTime.Parse(ServiceDate).Date, SiteCode);
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDate(string site, DateTime start, DateTime finish)
        {
            var listServices = _servicesCalendarHeaderRepository.GetServicesHeaderByDate(site, start, finish);
            return listServices;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateAndStatus(string site, DateTime start, DateTime finish, int status)
        {
            var listServices = _servicesCalendarHeaderRepository.GetServicesHeaderByDateAndStatus(site, start, finish, status);
            return listServices;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateAndStatusNotSite(DateTime start, DateTime finish, int status)
        {
            var listServices = _servicesCalendarHeaderRepository.GetServicesHeaderByDateAndStatusNotSite(start, finish, status);
            return listServices;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateNotSite(DateTime start, DateTime finish)
        {
            var listServices = _servicesCalendarHeaderRepository.GetServicesHeaderByDateNotSite(start, finish);
            return listServices;
        }

        public List<ServicesCalendarDetailModel> GetServicesDetail(int id)
        {
            return _servicesCalendarHeaderRepository.GetServicesDetail(id);
        }

        //OCD
        public ServicesCalendarModel GetServicesId(int service_id) => _servicesCalendarHeaderRepository.GetServicesId(service_id);

        public bool ModifyServicesDetail(string user, int status, int service_id)
        {
            var x = _servicesCalendarHeaderRepository.ModifyServicesHeader(service_id, status, user);
            return x;
        }

        public ServicesCalendarModel ActionGetServicesCalendarDetail(int id)
        {
            return _servicesCalendarHeaderRepository.ActionGetServicesCalendarDetail(id);
        }

        public bool UpdateServicesCalendar(int ServiceId, string ServiceDate, string ServiceSite, string User, List<ServicesCalendarDetailModel> Services)
        {
            DateTime servicedate = DateTime.Parse(ServiceDate);
            return _servicesCalendarHeaderRepository.UpdateServicesCalendar(ServiceId, servicedate, ServiceSite, User, Services);
        }
    }
}