﻿using App.DAL.SalesCalendar;
using App.Entities;
using App.Entities.ViewModels.ServicesCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.ServicesCalendar
{
    public class ServicesCalendarDetailBusiness
    {
        private readonly ServicesCalendarDetailRepository _servicesCalendarDetailRepository;

        public ServicesCalendarDetailBusiness()
        {
            _servicesCalendarDetailRepository = new ServicesCalendarDetailRepository();
        }

        public int AddServicesCalendarDetail(SERVICES_CALENDAR_DETAIL Service, string Document, string User)
        {
            Service.cuser = User;
            Service.cdate = DateTime.Now;
            Service.program_id = "MRO001.cshtml";
            if (Document == null || Document == "")
                Service.quotation_document = null;
            else
                Service.quotation_document = Document.Split(',')[1];

            return _servicesCalendarDetailRepository.AddServicesCalendarDetail(Service);
        }

        public Tuple<List<ServicesCalendarDetailModel>, List<int>> GetServicesDetailByServiceId(int service_id)
        {
            List<ServicesCalendarDetailModel> detailsTable = _servicesCalendarDetailRepository.GetServicesDetailByServiceId(service_id);
            if (detailsTable.Count > 0)
            {
                List<int> Suppliers = new List<int>();
                foreach (var item in detailsTable)
                {
                    if (Suppliers.Contains(item.supplier_id)) { }
                    else
                        Suppliers.Add(item.supplier_id);
                }
                return Tuple.Create(detailsTable, Suppliers);
            }
            else
                return Tuple.Create<List<ServicesCalendarDetailModel>, List<int>>(null, null);
        }

        public List<ServicesCalendarDetailModel> FilterServicesBySupplierId(List<ServicesCalendarDetailModel> services, int supplier)
        {
            return services.Where(x => x.supplier_id == supplier).ToList();
        }
    }
}