﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Entities;
using System.Data;
using App.Entities.ViewModels.Invoices;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Globalization;

namespace App.BLL.Invoices
{
    class PagosDetails
    {
        public string Serie { get; set; }
        public string Folio { get; set; }
        public decimal ImporteSaldoIns { get; set; }
        public decimal ImportePagado { get; set; }
        public decimal ImporteSaldoAnt { get; set; }
        public int NumParcialidad { get; set; }
        public string MetodoPago { get; set; }
        public string MonedaDr { get; set; }
        public string IdDocumento { get; set; }

    }

    public class Cedulas
    {
        private PRTSAIEntities _context;
        private InvoiceCedulasBusiness _invoiceCedulas;

        public Cedulas()
        {
            _context = new PRTSAIEntities();
            _invoiceCedulas = new InvoiceCedulasBusiness();
        }

        private List<CedulaModel> GetEgresos(List<string> _cancelados, int _month, int _year)
        {

            DirectoryInfo _egresos = new DirectoryInfo(@"C:\CEDULAS\\" + _year.ToString() + "\\" + _getMonthName(_month.ToString()) + "\\EGRESOS");
            var FilesEgresos = _egresos.GetFiles("*.xml"); //Getting Text files
            // var _cancelados = GetCanceladados("E");
            var _listEgresos = new List<CedulaModel>();
            var _sust = _invoiceCedulas.PorTipoyEstado("E", 0);
            var _transDateList = _invoiceCedulas.XMLPorFechayTipo(_year, _month, "E");

            string _UUISust = null;
            //_cancelados.RemoveAt(0);

            // DateTime DateStart = DateTime.ParseExact("01/", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            /*DateTime DateEnd = DateTime.ParseExact("", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<XML_FILES> _listEgresosFromDB = new List<XML_FILES>();
            if (_month>=4 && _year>=2020)
             _listEgresosFromDB = _context.XML_FILES.Where(w => w.date.Month == _month && w.date.Year == _year && w.type=="E").ToList();
*/


            foreach (var file in FilesEgresos)
            {


                try
                {
                    //var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.xml));
                    //var doc = XDocument.Parse(xml);
                    byte[] AsBytes = File.ReadAllBytes(file.FullName);
                    XmlDocument oXML;

                    using (MemoryStream oStream = new MemoryStream(AsBytes))
                    {
                        oXML = new XmlDocument();
                        oXML.Load(oStream);
                    }

                    // var xml = Encoding.UTF8.GetString(AsBytes);
                    var doc = XDocument.Parse(oXML.InnerXml);
                    var root = doc?.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc");
                    var name_client = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre");
                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var _folioSerie = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio");
                    if (_cancelados.Where(w => w == (string)uuid).Select(s => s).FirstOrDefault() != null)
                    {
                        _UUISust = _sust.Where(w => w.folio == _folioSerie && w.estado > 0).Select(s => s.uuid).FirstOrDefault();
                    }
                    var _UUID = (string)uuid;
                    if (_UUISust != null)
                    {
                        _UUID = _UUISust;
                    }
                    //revisamso que no este cancelado en SAT (_cancelados) y que no este cancelado en ERP 
                    if (_cancelados.Where(w => w == _UUID).Select(s => s).FirstOrDefault() == null && GetCanceladados("E", _folioSerie + ".xml") == null)
                    {

                        var currency = (string)root?.Attribute("Moneda") ?? "";
                        var tasaIva = 0.0M;
                        var iepsTotal = 0.0M;
                        var ivaTotal = 0.0M;
                        var _conceptosValue = "";
                        try
                        {
                            iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Sum(e => (decimal?)e.Attribute("Importe") ?? 0) ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            iepsTotal = 0;
                        }
                        try
                        {
                            tasaIva = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("TasaOCuota") ?? 0).FirstOrDefault() ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            tasaIva = 0;
                        }
                        try
                        {
                            ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            ivaTotal = 0;
                        }
                        var _conceptosList = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                        {
                            return new XML_DETAIL
                            {
                                item_description = string.Format("*** {0} | {1} | {2} | {3} | {4} | {5} | {6}",
                               (string)e.Attribute("Cantidad"), (string)e.Attribute("ClaveProdServ"), (string)e.Attribute("NoIdentificacion"),
                                (string)e.Attribute("Descripcion"), (string)e.Attribute("ClaveUnidad"), (string)e.Attribute("ValorUnitario"), (string)e.Attribute("Importe"))
                            };

                        }).ToList();
                        foreach (var item in _conceptosList)
                        {
                            _conceptosValue += item.item_description;
                        }

                        var _metPago = (string)root?.Attribute("MetodoPago") ?? "";
                        var _Total = ((decimal?)root?.Attribute("Total") ?? 0);
                        var _SubTotal = ((decimal?)root?.Attribute("SubTotal") ?? 0);

                        var _MontoBaseT0 = 0.0M;
                        var _MontoBaseT8 = 0.0M;
                        var _MontoBaseT8Temp = 0.0M;
                        var _MontoBaseT16 = 0.0M;
                        if (tasaIva == 0.16M)
                        {
                            _MontoBaseT16 = (ivaTotal / tasaIva);
                        }
                        if (tasaIva == 0.08M)
                        {
                            _MontoBaseT8Temp = Math.Round((ivaTotal / tasaIva), 2);
                            if (_MontoBaseT8Temp > _SubTotal)
                                _MontoBaseT8 = (_MontoBaseT8Temp - _SubTotal) <= 0.38M ? _SubTotal : _MontoBaseT8Temp;
                            else
                                _MontoBaseT8 = _MontoBaseT8Temp;
                            _MontoBaseT0 = (_Total - _MontoBaseT8 - ivaTotal - _MontoBaseT16);

                        }

                        if (ivaTotal == 0)
                        {
                            _MontoBaseT0 = Math.Round(_SubTotal + iepsTotal, 2);
                        }
                        _MontoBaseT0 = _MontoBaseT0 > 1 ? _MontoBaseT0 : 0.0M;
                        var relacionado = GetRelationEgresosIngresos((string)uuid ?? "", (string)root?.Attribute("Serie") ?? "");
                        var _cedula = new CedulaModel()
                        {
                            Uuid = _UUID ?? "",
                            SerieFolio = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio"),
                            FolioRel = relacionado.folio ?? "",
                            Folio = (string)root?.Attribute("Folio"),
                            Serie = (string)root?.Attribute("Serie"),
                            Sucursal = GetSucursalBySerie((string)root?.Attribute("Serie") ?? "") ?? "",
                            Cliente = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre") ?? "",
                            Rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc") ?? "",
                            FechaEmi = (string)root?.Attribute("Fecha") ?? "",
                            FolioFiscal = _UUID ?? "",
                            ServicioPrestado = _conceptosValue ?? "",
                            Moneda = (string)root?.Attribute("Moneda") ?? "",
                            TipoCambio = "",

                            Tipo = _metPago == "PPD" ? "DIFERIDO " : "CONTADO",

                            tipo16 = tasaIva == 0.16M ? "SI" : "NO",
                            tipo8 = tasaIva == 0.08M ? "SI" : "NO",
                            tipo0 = _MontoBaseT0 > 0 ? "SI" : "NO",
                            /////impuestos
                            MontoOperacion = (_SubTotal * -1),
                            MontoBaseT0 = (_MontoBaseT0 * -1),
                            MontoBaseT16 = _MontoBaseT16,
                            IvaTrasladado16 = tasaIva == 0.16M ? ivaTotal : 0.0M,
                            MontoBaseT8 = _MontoBaseT8 * -1,
                            IvaTrasladado8 = tasaIva == 0.08M ? (ivaTotal * -1) : 0.0M,
                            Tasa = 0.0M,
                            IepsTrasladado = (iepsTotal * -1),
                            TotalImpuestos = ((ivaTotal + iepsTotal) * -1),
                            ValorTotalFactura = (_Total * -1),
                            Clave = "",
                            MetodoPago = (string)root?.Attribute("MetodoPago") ?? "",
                            NumeroParcialidad = "",
                            //complemento
                            FolioFiscaC = "",
                            FechaEmisionC = "",
                            ImporteSaldoAnterior = 0.0M,
                            ImportePagado = 0.0M,
                            ImporteSaldoInsoluto = 0.0M,
                            /////complemento
                            UsoCfdi = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("UsoCFDI") ?? "",
                            FormaPago = (string)root?.Attribute("FormaPago") ?? "",
                            AbonadoPorc = "",
                            MontoOperacionCobrado = relacionado.pm != "-1" ? (_SubTotal * -1).ToString("0.00") : "credito",
                            MontoBaseT0C = relacionado.pm != "-1" ? (_MontoBaseT0 * -1) : 0.0M,
                            //MontoBaseT0AdicionadoImp = relacionado.pm != "-1" ? ((_MontoBaseT0 + iepsTotal) * -1) : 0.0M,
                            MontoBaseT16C = _MontoBaseT16,
                            IvaTrasladadoCobrado16 = tasaIva == 0.16M ? ivaTotal : 0.0M,
                            MontoBaseT8C = relacionado.pm != "-1" ? _MontoBaseT8 * -1 : 0.0M,
                            IvaTrasladadoCobrado8 = relacionado.pm != "-1" ? tasaIva == 0.08M ? (ivaTotal * -1) : 0.0M : 0.0M,
                            IepsladadoCobrado = relacionado.pm != "-1" ? (iepsTotal * -1) : 0.0M,
                            ImporteTotalCobrado = relacionado.pm != "-1" ? (_Total * -1) : 0.0M,
                            ImporteComplemento = 0.0M,
                            FechaCobro = "",
                            ReferenciaBancaria = "",
                            NombreInstitucionFinanciera = "",
                            TotalPorRFC = 0.0M,
                            /////impuestos
                        };

                        var _monthXML = int.Parse(_cedula.FechaEmi.Substring(5, 2));
                        if (_cedula.Moneda != "MXN")
                        {

                        }
                        if (_monthXML < _month + 1)
                        {
                            _cedula.Tipo = _cedula.Tipo + " MES ANTIGUO";
                            _cedula.IepsTrasladadoMesAnt = _cedula.IepsTrasladado;
                            _cedula.IvaTrasladado16MesAnt = _cedula.IvaTrasladado16;
                            _cedula.IvaTrasladado8MesAnt = _cedula.IvaTrasladado8;
                            _cedula.MontoBaseT0MesAnt = _cedula.MontoBaseT0;
                            _cedula.MontoBaseT16MesAnt = _cedula.MontoBaseT16C;
                            _cedula.MontoBaseT8MesAnt = _cedula.MontoBaseT8;
                            _cedula.MontoOperacionMesAnt = _cedula.MontoOperacion;
                            _cedula.TasaMesAnt = _cedula.Tasa;
                            _cedula.TotalImpuestosMesAnt = _cedula.TotalImpuestos;
                            _cedula.ValorTotalFacturaMesAnt = _cedula.ValorTotalFactura;


                        }
                        else
                            _cedula.Tipo = _cedula.Tipo + " MES ACTUAL";

                        _listEgresos.Add(_cedula);
                        _UUISust = null;
                    }



                }
                catch (Exception ex)
                {
                    string msg = ex.Message;

                }



            }
            return _listEgresos;
        }

        private List<CedulaModel> GetIngresos(List<string> _cancelados, int _month, int _year)
        {
            DirectoryInfo _ing = new DirectoryInfo(@"C:\CEDULAS\\" + _year.ToString() + "\\" + _getMonthName(_month.ToString()) + "\\INGRESOS");
            FileInfo[] FilesIngresos = _ing.GetFiles("*.xml"); //Getting Text files

            // _cancelados.RemoveAt(0);
            var _listIngresos = new List<CedulaModel>();
            var _sust = _invoiceCedulas.PorTipoyEstado("I", 0);
            var _transDateList = _invoiceCedulas.XMLPorFechayTipo(_year, _month, "I");
            string _UUISust = null;
            foreach (var file in FilesIngresos)
            {

                try
                {
                    byte[] AsBytes = File.ReadAllBytes(file.FullName);
                    XmlDocument oXML;

                    using (MemoryStream oStream = new MemoryStream(AsBytes))
                    {
                        oXML = new XmlDocument();
                        oXML.Load(oStream);
                    }

                    // var xml = Encoding.UTF8.GetString(AsBytes);
                    var doc = XDocument.Parse(oXML.InnerXml);
                    var root = doc?.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc");
                    var name_client = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre");
                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var _folioSerie = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio");
                    if (_cancelados.Where(w => w == (string)uuid).Select(s => s).FirstOrDefault() != null)
                    {
                        _UUISust = _sust.Where(w => w.folio == _folioSerie && w.estado > 0).Select(s => s.uuid).FirstOrDefault();
                    }
                    var _UUID = (string)uuid;
                    if (_UUISust != null)
                    {
                        _UUID = _UUISust;
                    }
                    //revisamos que no este cancelado en SAT (_cancelados) y que no este cancelado en ERP 
                    if (_cancelados.Where(w => w == _UUID).Select(s => s).FirstOrDefault() == null && GetCanceladados("I", _folioSerie + ".xml") == null)
                    {
                        var currency = (string)root?.Attribute("Moneda") ?? "";
                        var iepsTotal = 0.0M;
                        var ivaTotal = 0.0M;
                        var tasaIva = 0.0M;
                        var _conceptosValue = "";
                        try
                        {
                            iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Sum(e => (decimal?)e.Attribute("Importe") ?? 0) ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            iepsTotal = 0;
                        }

                        try
                        {
                            ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            ivaTotal = 0;
                        }
                        try
                        {
                            tasaIva = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("TasaOCuota") ?? 0).FirstOrDefault() ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            tasaIva = 0;
                        }
                        var _conceptosList = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                        {
                            return new XML_DETAIL
                            {
                                item_description = string.Format("*** {0} | {1} | {2} | {3} | {4} | {5} | {6}",
                               (string)e.Attribute("Cantidad") ?? "", (string)e.Attribute("ClaveProdServ") ?? "", (string)e.Attribute("NoIdentificacion") ?? "",
                                (string)e.Attribute("Descripcion") ?? "", (string)e.Attribute("ClaveUnidad") ?? "", (string)e.Attribute("ValorUnitario") ?? "", (string)e.Attribute("Importe") ?? "")
                            };

                        }).ToList();
                        foreach (var item in _conceptosList)
                        {
                            _conceptosValue += item.item_description;
                        }

                        var _metPago = (string)root?.Attribute("MetodoPago") ?? "";


                        var _Total = ((decimal?)root?.Attribute("Total") ?? 0.0M);
                        var _SubTotal = ((decimal?)root?.Attribute("SubTotal") ?? 0.0M);

                        var _MontoBaseT0 = 0.0M;

                        var _MontoBaseT8 = 0.0M;
                        var _MontoBaseT8Temp = 0.0M;
                        var _MontoBaseT16 = 0.0M;
                        if (tasaIva == 0.16M)
                        {
                            _MontoBaseT16 = (ivaTotal / tasaIva);
                        }
                        if (tasaIva == 0.08M)
                        {
                            _MontoBaseT8Temp = Math.Round((ivaTotal / tasaIva), 2);
                            if (_MontoBaseT8Temp > _SubTotal)
                                _MontoBaseT8 = (_MontoBaseT8Temp - _SubTotal) <= 0.38M ? _SubTotal : _MontoBaseT8Temp;
                            else
                                _MontoBaseT8 = _MontoBaseT8Temp;
                            _MontoBaseT0 = (_Total - _MontoBaseT8 - ivaTotal - _MontoBaseT16);

                        }
                        if (ivaTotal == 0)
                        {
                            _MontoBaseT0 = Math.Round(_SubTotal + iepsTotal, 2);
                        }
                        _MontoBaseT0 = _MontoBaseT0 > 1 ? _MontoBaseT0 : 0.0M;
                        /*
                         * El "Monto Base 0" no coincide de 15710 facturas, 
                         * este debe generarse por calculo, 
                         * si la factura no contiene impuesto de IVA,
                         * el importe Base 0% debe ser la Base+el IEPS y en el importe depues del punto decimal, 
                         * manejarlo solo hasta el segundo digito (centesimos). 

                         * */
                        //var _MontoBaseT0 = (_Total - (ivaTotal / tasaIva) - ivaTotal);
                        var _serieFolio = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio");
                        var _fechaTrans = _transDateList.Where(w => w.folio == _serieFolio).Select(s => s.trans_date).FirstOrDefault();
                        var _cedula = new CedulaModel()
                        {
                            Uuid = _UUID ?? "",
                            SerieFolio = _serieFolio,
                            FolioRel = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio"),
                            Folio = (string)root?.Attribute("Folio"),
                            Serie = (string)root?.Attribute("Serie"),
                            Sucursal = GetSucursalBySerie((string)root?.Attribute("Serie") ?? "") ?? "",
                            Cliente = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre") ?? "",
                            Rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc") ?? "",
                            FechaEmi = (string)root?.Attribute("Fecha") ?? "",
                            FolioFiscal = _UUID ?? "",
                            ServicioPrestado = _conceptosValue ?? "",
                            Moneda = (string)root?.Attribute("Moneda") ?? "",
                            TipoCambio = "",
                            Tipo = _metPago == "PPD" ? "DIFERIDO " : "CONTADO",

                            tipo16 = tasaIva == 0.16M ? "SI" : "NO",
                            tipo8 = tasaIva == 0.08M ? "SI" : "NO",
                            tipo0 = _MontoBaseT0 > 0 ? "SI" : "NO",
                            ImpuestoRetenido = 0.0M,
                            /////impuestos
                            MontoOperacion = _SubTotal,
                            MontoBaseT0 = _MontoBaseT0,
                            MontoBaseT16 = _MontoBaseT16,
                            IvaTrasladado16 = tasaIva == 0.08M ? 0 : ivaTotal,
                            MontoBaseT8 = _MontoBaseT8,
                            IvaTrasladado8 = tasaIva == 0.08M ? ivaTotal : 0,
                            Tasa = 0.0M,
                            IepsTrasladado = iepsTotal,
                            TotalImpuestos = (ivaTotal + iepsTotal),
                            ValorTotalFactura = _Total,
                            Clave = "",
                            MetodoPago = _metPago,//(string)root?.Attribute("MetodoPago") ?? "",
                            NumeroParcialidad = "",
                            //complemento
                            FolioFiscaC = "",
                            FechaEmisionC = "",
                            ImporteSaldoAnterior = 0.0M,
                            ImportePagado = 0.0M,
                            ImporteSaldoInsoluto = 0.0M,
                            /////complemento
                            UsoCfdi = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("UsoCFDI") ?? "",
                            FormaPago = (string)root?.Attribute("FormaPago") ?? "",

                            AbonadoPorc = "",
                            MontoOperacionCobrado = _metPago == "PPD" ? "credito" : _SubTotal.ToString("0.00") ?? "",
                            MontoBaseT0C = _metPago == "PPD" ? 0.0M : _MontoBaseT0,
                            //MontoBaseT0AdicionadoImp = _metPago == "PPD" ? 0.0M : (_MontoBaseT0 + iepsTotal),
                            MontoBaseT16C = _metPago == "PPD" ? 0.0M : _MontoBaseT16,
                            IvaTrasladadoCobrado16 = _metPago == "PPD" ? 0.0M : tasaIva == 0.08M ? 0 : ivaTotal,
                            MontoBaseT8C = _metPago == "PPD" ? 0.0M : _MontoBaseT8,
                            IvaTrasladadoCobrado8 = _metPago == "PPD" ? 0.0M : tasaIva == 0.08M ? ivaTotal : 0,
                            IepsladadoCobrado = _metPago == "PPD" ? 0.0M : iepsTotal,
                            ImporteTotalCobrado = _metPago == "PPD" ? 0.0M : _Total,
                            ImporteComplemento = _metPago == "PPD" ? 0.0M : _Total,
                            FechaCobro = _fechaTrans.ToString() ?? (string)root?.Attribute("Fecha") ?? "",
                            ReferenciaBancaria = "",
                            NombreInstitucionFinanciera = "",
                            TotalPorRFC = 0.0M,
                            //ImpuestoRetenido=0.0M,
                            /////impuestos
                        };
                        var _monthXML = int.Parse(_cedula.FechaEmi.Substring(5, 2));
                        if (_cedula.Moneda != "MXN")
                        {

                        }
                        if (_monthXML < _month + 1)
                        {
                            _cedula.Tipo = _cedula.Tipo + " MES ANTIGUO";
                            _cedula.IepsTrasladadoMesAnt = _cedula.IepsTrasladado;
                            _cedula.IvaTrasladado16MesAnt = _cedula.IvaTrasladado16;
                            _cedula.IvaTrasladado8MesAnt = _cedula.IvaTrasladado8;
                            _cedula.MontoBaseT0MesAnt = _cedula.MontoBaseT0;
                            _cedula.MontoBaseT16MesAnt = _cedula.MontoBaseT16C;
                            _cedula.MontoBaseT8MesAnt = _cedula.MontoBaseT8;
                            _cedula.MontoOperacionMesAnt = _cedula.MontoOperacion;
                            _cedula.TasaMesAnt = _cedula.Tasa;
                            _cedula.TotalImpuestosMesAnt = _cedula.TotalImpuestos;
                            _cedula.ValorTotalFacturaMesAnt = _cedula.ValorTotalFactura;


                        }
                        else
                            _cedula.Tipo = _cedula.Tipo + " MES ACTUAL";

                        _listIngresos.Add(_cedula);
                        _UUISust = null;
                        //Console.WriteLine("VIGENTE: " + (string)uuid);
                    }
                    /*else
                    {
                        var cancelado = (string)uuid;
                        var _canel = _cancelados.Find(f => f == cancelado);
                        Console.WriteLine("CANCELADO: " + cancelado);

                    }*/

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }







            }
            return _listIngresos;
        }
        private List<CedulaModel> GetPagos(List<string> _cancelados, int _month, int _year)
        {

            DirectoryInfo _pagos = new DirectoryInfo(@"C:\CEDULAS\\" + _year.ToString() + "\\" + _getMonthName(_month.ToString()) + "\\PAGOS");
            FileInfo[] FilesPagos = _pagos.GetFiles("*.xml"); //Getting Text files
            //_cancelados.RemoveAt(0);
            // var _cancelados = GetCanceladados("P");
            var _listPagos = new List<CedulaModel>();
            var _sust = _invoiceCedulas.PorTipoyEstado("P", 0);
            var _transDateList = _invoiceCedulas.XMLPorFechayTipo(_year, _month, "P");

            string _UUISust = null;
            foreach (var file in FilesPagos)
            {

                try
                {

                    byte[] AsBytes = File.ReadAllBytes(file.FullName);
                    XmlDocument oXML;

                    using (MemoryStream oStream = new MemoryStream(AsBytes))
                    {
                        oXML = new XmlDocument();
                        oXML.Load(oStream);
                    }

                    // var xml = Encoding.UTF8.GetString(AsBytes);
                    var doc = XDocument.Parse(oXML.InnerXml);
                    var root = doc?.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc");
                    var name_client = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre");
                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var pagosSpace = XNamespace.Get("http://www.sat.gob.mx/Pagos").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var _folioSerie = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio");
                    if (_cancelados.Where(w => w == (string)uuid).Select(s => s).FirstOrDefault() != null)
                    {
                        _UUISust = _sust.Where(w => w.folio == _folioSerie && w.estado > 0).Select(s => s.uuid).FirstOrDefault();
                    }
                    var _UUID = (string)uuid;
                    if (_UUISust != null)
                    {
                        _UUID = _UUISust;
                    }
                    //revisamso que no este cancelado en SAT (_cancelados) y que no este cancelado en ERP 
                    if (_cancelados.Where(w => w == _UUID).Select(s => s).FirstOrDefault() == null && GetCanceladados("P", _folioSerie + ".xml") == null)
                    {
                        var currency = (string)root?.Attribute("Moneda") ?? "";
                        var iepsTotal = 0.0;
                        var ivaTotal = 0.0;
                        var _conceptosValue = "";
                        /*try
                        {
                            iepsTotal = (double?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Sum(e => (decimal?)e.Attribute("Importe") ?? 0) ?? 0;

                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            iepsTotal = 0;
                        }

                        try
                        {
                            ivaTotal = (double?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            ivaTotal = 0;
                        }*/
                        /* var _conceptosList = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                         {
                             return new XML_DETAIL
                             {
                                 item_description = string.Format("*** {0} | {1} | {2} | {3} | {4} | {5} | {6}",
                                (string)e.Attribute("Cantidad"), (string)e.Attribute("ClaveProdServ"), (string)e.Attribute("NoIdentificacion"),
                                 (string)e.Attribute("Descripcion"), (string)e.Attribute("ClaveUnidad"), (string)e.Attribute("ValorUnitario"), (string)e.Attribute("Importe"))
                             };

                         }).ToList();*/

                        var _pagosList = root?.Descendants(XName.Get("Pago", pagosSpace)).Select(e =>
                        {

                            return new PagosDetails
                            {
                                Folio = (string)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("Folio")).FirstOrDefault(),
                                IdDocumento = (string)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("IdDocumento")).FirstOrDefault(),
                                ImportePagado = (decimal)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("ImpPagado")).FirstOrDefault(),
                                ImporteSaldoAnt = (decimal)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("ImpSaldoAnt")).FirstOrDefault(),
                                ImporteSaldoIns = (decimal)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("ImpSaldoInsoluto")).FirstOrDefault(),
                                MetodoPago = (string)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("MetodoDePagoDR")).FirstOrDefault(),
                                MonedaDr = (string)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("MonedaDR")).FirstOrDefault(),
                                NumParcialidad = (int)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("NumParcialidad")).FirstOrDefault(),
                                Serie = (string)e.Descendants(XName.Get("DoctoRelacionado", pagosSpace)).Select(a => a.Attribute("Serie")).FirstOrDefault(),



                            };

                        }).ToList();
                        /* foreach (var item in _conceptosList)
                           {
                               _conceptosValue += item.item_description;
                           }*/
                        var _metPago = (string)root?.Attribute("MetodoPago") ?? "";
                        var _Total = ((double?)root?.Attribute("Total") ?? 0);
                        var _SubTotal = ((double?)root?.Attribute("SubTotal") ?? 0);
                        var _MontoBaseT8 = (ivaTotal / 0.08).ToString("0.00");
                        var _MontoBaseT0 = (_Total - (ivaTotal / 0.08) - ivaTotal);
                        _MontoBaseT0 = _MontoBaseT0 > 1 ? _MontoBaseT0 : 0.0;
                        var _fecha = (string)root?.Attribute("Fecha");
                        foreach (var item in _pagosList)
                        {
                            var _cedula = new CedulaModel()
                            {
                                Uuid = "",
                                SerieFolio = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio"),
                                FolioRel = item.Serie + item.Folio,
                                Folio = (string)root?.Attribute("Folio") ?? "",
                                Serie = (string)root?.Attribute("Serie") ?? "",
                                Sucursal = GetSucursalBySerie(item.Serie),
                                Cliente = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre") ?? "",
                                Rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc") ?? "",
                                FechaEmi = _fecha,
                                FolioFiscal = item.IdDocumento,
                                ServicioPrestado = "0",
                                Moneda = item.MonedaDr,
                                TipoCambio = "",
                                Tipo = _metPago == "PPD" ? "DIFERIDO " : "CONTADO",

                                tipo16 = "NO",
                                tipo8 = "NO",
                                tipo0 = "SI",
                                ImpuestoRetenido = 0.0M,



                                /////impuestos
                                MontoOperacion = 0.0M,// _SubTotal.ToString("0.00"),
                                MontoBaseT0 = 0.0M,// _MontoBaseT0.ToString("0.00"),
                                MontoBaseT16 = 0.0M,
                                IvaTrasladado16 = 0.0M,
                                MontoBaseT8 = 0.0M,// _MontoBaseT8,
                                IvaTrasladado8 = 0.0M,//ivaTotal.ToString("0.00"),
                                Tasa = 0.0M,
                                IepsTrasladado = 0.0M,// iepsTotal.ToString("0.00"),
                                TotalImpuestos = 0.0M,// (ivaTotal + iepsTotal).ToString("0.00"),
                                ValorTotalFactura = 0.0M,// _Total.ToString("0.00"),
                                Clave = "",
                                MetodoPago = item.MetodoPago,
                                NumeroParcialidad = item.NumParcialidad.ToString(),
                                //complemento
                                FolioFiscaC = (string)uuid,
                                FechaEmisionC = _fecha,
                                ImporteSaldoAnterior = item.ImporteSaldoAnt,
                                ImportePagado = item.ImportePagado,
                                ImporteSaldoInsoluto = item.ImporteSaldoIns,
                                /////complemento
                                UsoCfdi = "",// (string)root?.Element(XName.Get("Receptor", space))?.Attribute("UsoCFDI"),
                                FormaPago = "",//(string)root?.Attribute("FormaPago") ?? "",
                                AbonadoPorc = "",
                                MontoOperacionCobrado = item.ImportePagado.ToString("0.00"),
                                MontoBaseT0C = item.ImportePagado,
                                MontoBaseT0AdicionadoImp = item.ImportePagado,
                                MontoBaseT16C = 0.0M,
                                IvaTrasladadoCobrado16 = 0.0M,
                                MontoBaseT8C = 0.0M,
                                IvaTrasladadoCobrado8 = 0.0M,
                                IepsladadoCobrado = 0.0M,//iepsTotal.ToString("0.00"),
                                ImporteTotalCobrado = item.ImportePagado,
                                ImporteComplemento = item.ImportePagado,
                                FechaCobro = _fecha,
                                ReferenciaBancaria = "",
                                NombreInstitucionFinanciera = "",
                                TotalPorRFC = 0.0M,
                                /////impuestos
                            };
                            var _monthXML = int.Parse(_cedula.FechaEmi.Substring(5, 2));
                            if (_cedula.Moneda != "MXN")
                            {

                            }
                            if (_monthXML < _month + 1)
                            {
                                _cedula.Tipo = _cedula.Tipo + " MES ANTIGUO";
                                _cedula.IepsTrasladadoMesAnt = _cedula.IepsTrasladado;
                                _cedula.IvaTrasladado16MesAnt = _cedula.IvaTrasladado16;
                                _cedula.IvaTrasladado8MesAnt = _cedula.IvaTrasladado8;
                                _cedula.MontoBaseT0MesAnt = _cedula.MontoBaseT0;
                                _cedula.MontoBaseT16MesAnt = _cedula.MontoBaseT16C;
                                _cedula.MontoBaseT8MesAnt = _cedula.MontoBaseT8;
                                _cedula.MontoOperacionMesAnt = _cedula.MontoOperacion;
                                _cedula.TasaMesAnt = _cedula.Tasa;
                                _cedula.TotalImpuestosMesAnt = _cedula.TotalImpuestos;
                                _cedula.ValorTotalFacturaMesAnt = _cedula.ValorTotalFactura;


                            }
                            else
                                _cedula.Tipo = _cedula.Tipo + " MES ACTUAL";
                            _listPagos.Add(_cedula);
                            _UUISust = null;
                        }
                    }




                }
                catch (Exception ex)
                {
                    string msg = ex.Message;

                }

            }
            return _listPagos;
        }

        private RELACION_CEDULAS GetRelationEgresosIngresos(string IdEgreso, string serie)
        {


            var result = _invoiceCedulas.RelacionCedulasPorIdEgreso(IdEgreso);
            //solo lo del mes facturado

            if (result != null)
                return result;
            else
            {
                result = new RELACION_CEDULAS()
                {
                    folio = serie + "0",
                    pm = "0"
                };
                return result;
            }

        }
        private string GetCanceladados(string tipo, string folio)
        {
            return _invoiceCedulas.CanceladoCedulaPorFolioyTipo(folio, tipo);


        }
        private string GetUUDIvigente(string _folio)
        {
            return _invoiceCedulas.PorFolioyEstado(_folio, 0).FirstOrDefault().uuid;
        }

        private List<string> GetCanceladosSAT(string _year, string _mes)
        {
            DirectoryInfo _ing = new DirectoryInfo(@"C:\CEDULAS\" + _year + "\\");
            FileInfo[] FileCancelado = _ing.GetFiles("*.txt"); //Getting Text files 

            var result = new List<string>();
            try
            {
                using (StreamReader lector = new StreamReader(FileCancelado[0].FullName))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            result.Add(linea.Substring(0, 36).ToLower());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return result;
        }

        private string GetSucursalBySerie(string serie)
        {

            return _invoiceCedulas.NombreTiendaPorSerie(serie);

        }

        public List<CedulaModel> CreatCedula(string _year, string _mes, int tipo, int rfc) //tipo1: Cedula con  pagos y notas de credito, tipo2:por RFC,  tipo3 solo lo facturado
        {
            var _cancelados = GetCanceladosSAT(_year, _mes);




            var _listIngresos = GetIngresos(_cancelados, Int32.Parse(_mes), Int32.Parse(_year));
            var _listEgresos = GetEgresos(_cancelados, Int32.Parse(_mes), Int32.Parse(_year));
            var _listPagos = new List<CedulaModel>();
            if (tipo == 1)
                _listPagos = GetPagos(_cancelados, Int32.Parse(_mes), Int32.Parse(_year));
            var _listPagosIngresos = new List<CedulaModel>();
            var _ingresoRelacionado = new CedulaModel();
            var _litIngresosRelacionando = new List<CedulaModel>();
            var _idRelacionado = "";
            var _pagosOrdered = _listPagos.OrderBy(o => o.FolioRel);
            var _ingresosCredito = _listIngresos.Where(w => w.MetodoPago == "PPD").ToList();
            var _listCedulasModel = new List<CedulaModel>();
            var _monto8Temp = 0.0M; var _monto8Value = 0.0M; var _SubTotal = 0.0M;

            if (tipo == 1)
            {
                foreach (var item in _pagosOrdered)
                {
                    //if (_idRelacionado != item.FolioFiscal)
                    //{
                    _ingresoRelacionado = _ingresosCredito.Where(w => w.Uuid == item.FolioFiscal).FirstOrDefault();
                    if (_ingresoRelacionado != null)//no está en los ingresos del més, busca en la carpeta de todos los ingresos por el  nombre del archivo
                    {
                        //imuestos proporcionales al monto del adeudo

                        //if (_ingresoRelacionado.TotalImpuestos > 0)
                        //{
                        var _prop = item.ImporteTotalCobrado / _ingresoRelacionado.ValorTotalFactura;

                        var _ieps = _ingresoRelacionado.IepsTrasladado * _prop;
                        var _iva = Math.Round(_ingresoRelacionado.IvaTrasladado8 * _prop, 2);
                        _SubTotal = (item.ImporteTotalCobrado - _ieps - _iva);
                        item.MontoOperacionCobrado = _SubTotal.ToString("0.00");
                        _monto8Temp = Math.Round(_iva / 0.08M, 2);
                        if (_monto8Temp > _SubTotal)
                            _monto8Value = (_monto8Temp - _SubTotal) <= 0.38M ? _SubTotal : _monto8Temp;
                        else
                            _monto8Value = _monto8Temp;
                        //_MontoBaseT0 = (_Total - _MontoBaseT8 - ivaTotal - _MontoBaseT16);
                        if (_iva > 0)
                            item.MontoBaseT0C = ((item.ImporteTotalCobrado - _ieps - _iva) - _monto8Value);
                        else
                            item.MontoBaseT0C = item.ImporteTotalCobrado;  //item.MontoBaseT0AdicionadoImp = (item.MontoBaseT0C + _ieps);

                        item.MontoBaseT8C = _monto8Value;
                        item.IepsladadoCobrado = _ieps;
                        item.IvaTrasladadoCobrado8 = _iva;
                        item.tipo0 = _ingresoRelacionado.tipo0;
                        item.tipo16 = _ingresoRelacionado.tipo16;
                        item.tipo8 = _ingresoRelacionado.tipo8;


                    }
                    //_listPagosIngresos.Add(_ingresoRelacionado);
                    //_listIngresos.Remove(_ingresoRelacionado);



                    else
                    {
                        _ingresoRelacionado = GetIngresosRelacionPagos(@"C:/CEDULAS/" + _year + "/TODOS/" + item.FolioRel + ".xml", Int32.Parse(_mes));
                        if (_ingresoRelacionado != null)
                        {
                            //imuestos proporcionales al monto del adeudo
                            if (_ingresoRelacionado.TotalImpuestos > 0)
                            {
                                var _prop = item.ImporteTotalCobrado / _ingresoRelacionado.ValorTotalFactura;
                                var _ieps = _ingresoRelacionado.IepsTrasladado * _prop;
                                var _iva = Math.Round(_ingresoRelacionado.IvaTrasladado8 * _prop, 2);

                                _SubTotal = (item.ImporteTotalCobrado - _ieps - _iva);
                                item.MontoOperacionCobrado = _SubTotal.ToString("0.00");
                                _monto8Temp = Math.Round(_iva / 0.08M, 2);
                                if (_monto8Temp > _SubTotal)
                                    _monto8Value = (_monto8Temp - _SubTotal) <= 0.38M ? _SubTotal : _monto8Temp;
                                else
                                    _monto8Value = _monto8Temp;  //_MontoBaseT0 = (_Total - _MontoBaseT8 - ivaTotal - _MontoBaseT16);
                                if (_iva > 0)
                                    item.MontoBaseT0C = ((item.ImporteTotalCobrado - _ieps - _iva) - _monto8Value);
                                else
                                    item.MontoBaseT0C = item.ImporteTotalCobrado;//item.MontoBaseT0AdicionadoImp = (item.MontoBaseT0C + _ieps);
                                item.MontoBaseT8C = _monto8Value;
                                item.IepsladadoCobrado = _ieps;
                                item.IvaTrasladadoCobrado8 = _iva;
                                item.tipo0 = _ingresoRelacionado.tipo0;
                                item.tipo16 = _ingresoRelacionado.tipo16;
                                item.tipo8 = _ingresoRelacionado.tipo8;
                            }
                            if (_idRelacionado != item.FolioFiscal)
                                _litIngresosRelacionando.Add(_ingresoRelacionado);
                            //_listIngresos.Remove(_ingresoRelacionado);

                        }
                        else
                        {
                            if (Int32.Parse(_year) > 2019)
                            {
                                var _lastYear = Int32.Parse(_year) - 1;

                                _ingresoRelacionado = GetIngresosRelacionPagos(@"C:/CEDULAS/" + _lastYear.ToString() + "/TODOS/" + item.FolioRel + ".xml", Int32.Parse(_mes));
                                if (_ingresoRelacionado != null)
                                {
                                    //imuestos proporcionales al monto del adeudo
                                    if (_ingresoRelacionado.TotalImpuestos > 0)
                                    {
                                        var _prop = item.ImporteTotalCobrado / _ingresoRelacionado.ValorTotalFactura;
                                        var _ieps = _ingresoRelacionado.IepsTrasladado * _prop;
                                        var _iva = Math.Round(_ingresoRelacionado.IvaTrasladado8 * _prop, 2);

                                        _SubTotal = (item.ImporteTotalCobrado - _ieps - _iva);
                                        item.MontoOperacionCobrado = _SubTotal.ToString("0.00");
                                        _monto8Temp = Math.Round(_iva / 0.08M, 2);
                                        if (_monto8Temp > _SubTotal)
                                            _monto8Value = (_monto8Temp - _SubTotal) <= 0.38M ? _SubTotal : _monto8Temp;
                                        else
                                            _monto8Value = _monto8Temp; //_MontoBaseT0 = (_Total - _MontoBaseT8 - ivaTotal - _MontoBaseT16);
                                        if (_iva > 0)
                                            item.MontoBaseT0C = ((item.ImporteTotalCobrado - _ieps - _iva) - _monto8Value);
                                        else
                                            item.MontoBaseT0C = item.ImporteTotalCobrado;

                                        //item.MontoBaseT0AdicionadoImp = (item.MontoBaseT0C + _ieps);
                                        item.MontoBaseT8C = _monto8Value;
                                        item.IepsladadoCobrado = _ieps;
                                        item.IvaTrasladadoCobrado8 = _iva;
                                        item.tipo0 = _ingresoRelacionado.tipo0;
                                        item.tipo16 = _ingresoRelacionado.tipo16;
                                        item.tipo8 = _ingresoRelacionado.tipo8;
                                    }
                                    if (_idRelacionado != item.FolioFiscal)
                                        _litIngresosRelacionando.Add(_ingresoRelacionado);
                                    //_listIngresos.Remove(_ingresoRelacionado);

                                }
                                else
                                    item.Uuid = "PAGO SIN FACTURA";
                            }
                            else
                                item.Uuid = "PAGO SIN FACTURA";

                        }
                    }


                    _listPagosIngresos.Add(item);
                    /*}
                    else
                    {
                        _listPagosIngresos.Add(item);
                    }*/
                    _idRelacionado = item.FolioFiscal;
                }







                //_listCedulasModel.AddRange(_listIngresos.Where(w => w.MetodoPago != "PPD").ToList());
                // _litIngresosRelacionando.GroupBy(g => g.SerieFolio);
                List<CedulaModel> result1 = new HashSet<CedulaModel>(_litIngresosRelacionando).ToList(); //3700 ticks
                _listCedulasModel.AddRange(_listIngresos);
                _listCedulasModel.AddRange(_listPagosIngresos);
                _listCedulasModel.AddRange(_listEgresos);
                _listCedulasModel.AddRange(result1);

            }


            else
            {
                _listCedulasModel.AddRange(_listIngresos);
                _listCedulasModel.AddRange(_listEgresos);

            }
            if (rfc == 1)
            {
                return _listCedulasModel.OrderBy(O => O.Rfc).ToList();
            }
            else
                return _listCedulasModel.OrderBy(O => O.FolioRel).ToList();

        }
        private string _getMonthName(string _month)
        {
            var _mes = "";
            switch (_month)
            {

                case "0":
                    _mes = "ENERO";
                    break;
                case "1":
                    _mes = "FEBRERO";
                    break;
                case "2":
                    _mes = "MARZO";
                    break;

                case "3":
                    _mes = "ABRIL";
                    break;
                case "4":
                    _mes = "MAYO";
                    break;
                case "5":
                    _mes = "JUNIO";
                    break;
                case "6":
                    _mes = "JULIO";
                    break;
                case "7":
                    _mes = "AGOSTO";
                    break;
                case "8":
                    _mes = "SEPTIEMBRE";
                    break;

                case "9":
                    _mes = "OCTUBRE";
                    break;
                case "10":
                    _mes = "NOVIEMBRE";
                    break;
                case "11":
                    _mes = "DICIEMBRE";
                    break;


                default:
                    _mes = "";
                    break;
            }
            return _mes;
        }
        private CedulaModel GetIngresosRelacionPagos(string filePath, int _month)
        {




            var _listIngresos = new List<CedulaModel>();
            CedulaModel _cedula = null;
            byte[] AsBytes = null;
            try
            {
                AsBytes = File.ReadAllBytes(filePath);
            }
            catch (Exception e)
            {
                return null;
            }



            try
            {
                XmlDocument oXML;

                using (MemoryStream oStream = new MemoryStream(AsBytes))
                {
                    oXML = new XmlDocument();
                    oXML.Load(oStream);
                }

                // var xml = Encoding.UTF8.GetString(AsBytes);
                var doc = XDocument.Parse(oXML.InnerXml);
                var root = doc?.Root;
                var space = root?.Name.NamespaceName;
                var rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc") ?? "";
                var name_client = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre") ?? "";
                var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                var uuid = root?
                    .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                    .Attribute("UUID");
                var currency = (string)root?.Attribute("Moneda") ?? "";
                var iepsTotal = 0.0M;
                var ivaTotal = 0.0M;
                var _conceptosValue = "";
                var tasaIva = 0.0M;
                try
                {
                    iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Sum(e => (decimal?)e.Attribute("Importe") ?? 0) ?? 0;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    iepsTotal = 0;
                }

                try
                {
                    ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    ivaTotal = 0;
                }
                try
                {
                    tasaIva = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("TasaOCuota") ?? 0).FirstOrDefault() ?? 0;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    tasaIva = 0;
                }
                var _conceptosList = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                {
                    return new XML_DETAIL
                    {
                        item_description = string.Format("*** {0} | {1} | {2} | {3} | {4} | {5} | {6}",
                       (string)e.Attribute("Cantidad") ?? "", (string)e.Attribute("ClaveProdServ") ?? "", (string)e.Attribute("NoIdentificacion") ?? "",
                        (string)e.Attribute("Descripcion") ?? "", (string)e.Attribute("ClaveUnidad") ?? "", (string)e.Attribute("ValorUnitario") ?? "", (string)e.Attribute("Importe") ?? "")
                    };

                }).ToList();
                foreach (var item in _conceptosList)
                {
                    _conceptosValue += item.item_description;
                }
                var _Total = ((decimal?)root?.Attribute("Total") ?? 0);
                var _SubTotal = ((decimal?)root?.Attribute("SubTotal") ?? 0);

                var _MontoBaseT0 = 0.0M;

                var _MontoBaseT8 = 0.0M;
                var _MontoBaseT8Temp = 0.0M;
                var _MontoBaseT16 = 0.0M;
                if (tasaIva == 0.16M)
                {
                    _MontoBaseT16 = (ivaTotal / tasaIva);
                }
                if (tasaIva == 0.08M)
                {
                    _MontoBaseT8Temp = Math.Round((ivaTotal / tasaIva), 2);
                    if (_MontoBaseT8Temp > _SubTotal)
                        _MontoBaseT8 = (_MontoBaseT8Temp - _SubTotal) <= 0.38M ? _SubTotal : _MontoBaseT8Temp;
                    else
                        _MontoBaseT8 = _MontoBaseT8Temp;

                    _MontoBaseT0 = (_Total - _MontoBaseT8 - ivaTotal - _MontoBaseT16);

                }
                if (ivaTotal == 0)
                {
                    _MontoBaseT0 = Math.Round(_SubTotal + iepsTotal, 2);
                }


                var _metPago = (string)root?.Attribute("MetodoPago") ?? "";
                _cedula = new CedulaModel
                {
                    Uuid = (string)uuid,
                    SerieFolio = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio") ?? "",
                    FolioRel = (string)root?.Attribute("Serie") + (string)root?.Attribute("Folio") ?? "",
                    Folio = (string)root?.Attribute("Folio") ?? "",
                    Serie = (string)root?.Attribute("Serie") ?? "",
                    Sucursal = GetSucursalBySerie((string)root?.Attribute("Serie") ?? "") ?? "",
                    Cliente = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Nombre") ?? "",
                    Rfc = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc") ?? "",
                    FechaEmi = (string)root?.Attribute("Fecha") ?? "",
                    FolioFiscal = (string)uuid,
                    ServicioPrestado = _conceptosValue,
                    Moneda = (string)root?.Attribute("Moneda") ?? "",
                    TipoCambio = "",
                    Tipo = _metPago == "PPD" ? "DIFERIDO " : "CONTADO",

                    tipo16 = "NO",
                    tipo8 = "NO",
                    tipo0 = "SI",
                    ImpuestoRetenido = 0.0M,
                    /////impuestos
                    MontoOperacion = _SubTotal,
                    MontoBaseT0 = _MontoBaseT0,
                    MontoBaseT16 = _MontoBaseT16,
                    IvaTrasladado16 = tasaIva == 0.08M ? 0 : ivaTotal,
                    MontoBaseT8 = _MontoBaseT8,
                    IvaTrasladado8 = tasaIva == 0.08M ? ivaTotal : 0,
                    Tasa = 0.0M,
                    IepsTrasladado = iepsTotal,
                    TotalImpuestos = (ivaTotal + iepsTotal),
                    ValorTotalFactura = _Total,
                    Clave = "",
                    MetodoPago = _metPago,//(string)root?.Attribute("MetodoPago") ?? "",
                    NumeroParcialidad = "",
                    //complemento
                    FolioFiscaC = "",
                    FechaEmisionC = "",
                    ImporteSaldoAnterior = 0.0M,
                    ImportePagado = 0.0M,
                    ImporteSaldoInsoluto = 0.0M,
                    /////complemento
                    UsoCfdi = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("UsoCFDI") ?? "",
                    FormaPago = (string)root?.Attribute("FormaPago") ?? "",

                    AbonadoPorc = "",
                    MontoOperacionCobrado = _metPago == "PPD" ? "credito" : _SubTotal.ToString("0.00") ?? "",
                    MontoBaseT0C = _metPago == "PPD" ? 0.0M : _MontoBaseT0,
                    MontoBaseT0AdicionadoImp = _metPago == "PPD" ? 0.0M : (_MontoBaseT0 + iepsTotal),
                    MontoBaseT16C = _metPago == "PPD" ? 0.0M : _MontoBaseT16,
                    IvaTrasladadoCobrado16 = _metPago == "PPD" ? 0.0M : tasaIva == 0.08M ? 0 : ivaTotal,
                    MontoBaseT8C = _metPago == "PPD" ? 0.0M : _MontoBaseT8,
                    IvaTrasladadoCobrado8 = _metPago == "PPD" ? 0.0M : tasaIva == 0.08M ? ivaTotal : 0,
                    IepsladadoCobrado = _metPago == "PPD" ? 0.0M : iepsTotal,
                    ImporteTotalCobrado = _metPago == "PPD" ? 0.0M : _Total,
                    ImporteComplemento = 0.0M,
                    FechaCobro = "",
                    ReferenciaBancaria = "",
                    NombreInstitucionFinanciera = "",
                    TotalPorRFC = 0.0M,
                    /////impuestos
                };
                var _monthXML = int.Parse(_cedula.FechaEmi.Substring(5, 2));
                if (_cedula.Moneda != "MXN")
                {

                }
                if (_monthXML < _month + 1)
                {
                    _cedula.Tipo = _cedula.Tipo + " MES ANTIGUO";
                    _cedula.IepsTrasladadoMesAnt = _cedula.IepsTrasladado;
                    _cedula.IvaTrasladado16MesAnt = _cedula.IvaTrasladado16;
                    _cedula.IvaTrasladado8MesAnt = _cedula.IvaTrasladado8;
                    _cedula.MontoBaseT0MesAnt = _cedula.MontoBaseT0;
                    _cedula.MontoBaseT16MesAnt = _cedula.MontoBaseT16C;
                    _cedula.MontoBaseT8MesAnt = _cedula.MontoBaseT8;
                    _cedula.MontoOperacionMesAnt = _cedula.MontoOperacion;
                    _cedula.TasaMesAnt = _cedula.Tasa;
                    _cedula.TotalImpuestosMesAnt = _cedula.TotalImpuestos;
                    _cedula.ValorTotalFacturaMesAnt = _cedula.ValorTotalFactura;


                }
                else
                    _cedula.Tipo = _cedula.Tipo + " MES ACTUAL";
            }
            catch (Exception ex)
            {

            }

            // _listIngresos.Add(_cedula);





            return _cedula;
        }
    }
}
