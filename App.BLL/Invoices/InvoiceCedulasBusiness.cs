﻿using App.DAL.Invoices;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Invoices
{
    public class InvoiceCedulasBusiness
    {
        InvoiceCedulasRepository _invoiceCedulas;

        public InvoiceCedulasBusiness()
        {
            _invoiceCedulas = new InvoiceCedulasRepository();
        }


        public List<DUPLICADOS_CEDULA> PorTipoyEstado(string tipo, int estado)
        {
            return _invoiceCedulas.PorTipoyEstado(tipo, estado);
        }

        public List<DUPLICADOS_CEDULA> PorFolioyEstado(string folio, int estado)
        {
            return _invoiceCedulas.PorFolioyEstado(folio, estado);
        }

        public string NombreTiendaPorSerie(string serie)
        {
            return _invoiceCedulas.NombreTiendaPorSerie(serie);
        }

        public string CanceladoCedulaPorFolioyTipo(string folio, string tipo)
        {
            return _invoiceCedulas.CanceladoCedulaPorFolioyTipo(folio, tipo);
        }

        public RELACION_CEDULAS RelacionCedulasPorIdEgreso(string id)
        {
            return _invoiceCedulas.RelacionCedulasPorIdEgreso(id);
        }

        public List<XML_TRANS_DATE> XMLPorFechayTipo(int year, int month, string tipo)
        {
            return _invoiceCedulas.XMLPorFechayTipo(year, month, tipo);
        }


    }
}
