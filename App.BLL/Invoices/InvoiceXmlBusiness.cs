﻿using App.BLL.Site;
using App.DAL.Invoices;
using App.DAL.Purchases;
using App.Entities;
using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace App.BLL.Invoices
{
    public class InvoiceXmlBusiness
    {
        private readonly InvoiceXmlRepository _InvoiceXmlRepository;
        private readonly SiteBusiness _SiteBusiness;
        private readonly SupplierRespository _SupplierRespository;

        public InvoiceXmlBusiness()
        {
            _InvoiceXmlRepository = new InvoiceXmlRepository();
            _SiteBusiness = new SiteBusiness();
            _SupplierRespository = new SupplierRespository();
        }

        public List<Customer> GetCustomersBySiteCode(string SiteCode)
        {
            string SiteIP = _SiteBusiness.GetSiteIP(SiteCode);

            return _InvoiceXmlRepository.GetCustomersBySiteIP(SiteIP);
        }

        public List<InvoiceXml> GetXML()
        {
            return _InvoiceXmlRepository.GetXML();
        }

        public bool SaveCustomerXML(List<InvoiceXml> Xml, string User)
        {
            return _InvoiceXmlRepository.SaveXML(Xml, User);
        }

        public bool SaveSupplierXML(List<InvoiceXml> Xml, string InvoiceType, int SupplierId, string User, out string Status)
        {
            Status = "";
            foreach (var file in Xml)
            {
                var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.Xml));
                var doc = XDocument.Parse(xml);
                var root = doc.Root;

                var RFCEmisor = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Emisor").Attribute("Rfc");
                if (RFCEmisor != null)
                {
                    var supplier = _SupplierRespository.GetSupplierInfoById(SupplierId);
                    if (supplier.Rfc != RFCEmisor.Value)
                    {
                        Status = "Archivo xml no corresponde a proveedor";
                        break;
                    }
                }
            }

            if (Status == "")
                return _InvoiceXmlRepository.SaveCustomerXML(Xml, InvoiceType, SupplierId, User);
            else
                return false;
        }

        public INVOICE_XML GetPDF(int Id)
        {
            return _InvoiceXmlRepository.GetPDF(Id);
        }

        public List<INVOICE_XML> GetAllPDF()
        {
            return _InvoiceXmlRepository.GetAllPDF();
        }
    }
}
