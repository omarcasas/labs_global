//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class XML_HEADER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public XML_HEADER()
        {
            this.XML_DETAIL = new HashSet<XML_DETAIL>();
        }
    
        public int xml_id { get; set; }
        public Nullable<int> expense_id { get; set; }
        public string uuid_invoice { get; set; }
        public Nullable<System.DateTime> invoice_date { get; set; }
        public string invoice_no { get; set; }
        public Nullable<int> supplier_id { get; set; }
        public string currency { get; set; }
        public Nullable<decimal> subtotal { get; set; }
        public Nullable<decimal> ieps { get; set; }
        public Nullable<decimal> iva { get; set; }
        public Nullable<decimal> iva_retained { get; set; }
        public Nullable<decimal> isr_retained { get; set; }
        public Nullable<decimal> discount { get; set; }
        public Nullable<decimal> total_amount { get; set; }
        public string xml_name { get; set; }
        public string xml { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string confirmation_cancel { get; set; }
        public string authorization_cancel { get; set; }
        public Nullable<System.DateTime> xml_header_cancel_date { get; set; }
        public Nullable<int> xml_header_status { get; set; }
        public string remark { get; set; }
        public string program_id { get; set; }
        public string invoice_type { get; set; }
        public Nullable<decimal> exchange_currency { get; set; }
        public string payment_method { get; set; }
        public string payment_way { get; set; }
        public string version { get; set; }
    
        public virtual EXPENSE EXPENSE { get; set; }
        public virtual MA_SUPPLIER MA_SUPPLIER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<XML_DETAIL> XML_DETAIL { get; set; }
    }
}
