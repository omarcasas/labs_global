//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SATCatalogoProdServ
    {
        public string c_ClaveProdServ { get; set; }
        public string tipo { get; set; }
        public string Descripcion_ProdServ { get; set; }
        public System.DateTime FechaInicioVigencia { get; set; }
        public Nullable<System.DateTime> FechaFinVigencia { get; set; }
        public string Incluir_IVA_trasladado { get; set; }
        public string Incluir_IEPS_trasladado { get; set; }
        public string Complemento { get; set; }
        public string cve_clase { get; set; }
        public string cve_grupo { get; set; }
        public string cve_division { get; set; }
    }
}
