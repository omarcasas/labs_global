//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SERVICES_CALENDAR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SERVICES_CALENDAR()
        {
            this.SERVICES_CALENDAR_DETAIL = new HashSet<SERVICES_CALENDAR_DETAIL>();
        }
    
        public int service_id { get; set; }
        public System.DateTime service_date { get; set; }
        public int service_status { get; set; }
        public string site_code { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
        public Nullable<int> expense_id { get; set; }
    
        public virtual EXPENSE EXPENSE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SERVICES_CALENDAR_DETAIL> SERVICES_CALENDAR_DETAIL { get; set; }
        public virtual SERVICES_CALENDAR SERVICES_CALENDAR1 { get; set; }
        public virtual SERVICES_CALENDAR SERVICES_CALENDAR2 { get; set; }
    }
}
