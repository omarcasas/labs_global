//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PURCHASE_BASE_COST
    {
        public int base_cost_id { get; set; }
        public Nullable<int> supplier_part_number { get; set; }
        public Nullable<decimal> base_cost { get; set; }
        public string currency { get; set; }
        public Nullable<decimal> unit_of_price { get; set; }
        public Nullable<System.DateTime> valid_from { get; set; }
        public Nullable<System.DateTime> valid_to { get; set; }
        public string site_code { get; set; }
        public Nullable<int> status { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
    
        public virtual ITEM_SUPPLIER ITEM_SUPPLIER { get; set; }
    }
}
