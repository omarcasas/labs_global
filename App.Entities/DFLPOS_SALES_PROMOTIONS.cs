//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DFLPOS_SALES_PROMOTIONS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DFLPOS_SALES_PROMOTIONS()
        {
            this.DFLPOS_SALES_PROMOTIONS_DETAIL = new HashSet<DFLPOS_SALES_PROMOTIONS_DETAIL>();
            this.DFLPOS_SALES_PROMOTIONS_SITES = new HashSet<DFLPOS_SALES_PROMOTIONS_SITES>();
        }
    
        public long promotion_id { get; set; }
        public string promotion_name { get; set; }
        public Nullable<System.DateTime> start_date { get; set; }
        public Nullable<System.DateTime> end_date { get; set; }
        public bool status { get; set; }
        public Nullable<int> item_quantity { get; set; }
        public Nullable<decimal> item_amount { get; set; }
        public int promotions_by_ticket { get; set; }
        public string ticket_header { get; set; }
        public string ticket_footer { get; set; }
        public Nullable<decimal> sale_min { get; set; }
        public Nullable<int> promotion_value { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_SALES_PROMOTIONS_DETAIL> DFLPOS_SALES_PROMOTIONS_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_SALES_PROMOTIONS_SITES> DFLPOS_SALES_PROMOTIONS_SITES { get; set; }
    }
}
