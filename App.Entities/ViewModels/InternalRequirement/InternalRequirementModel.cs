﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class InternalRequirementModel
    {
        public string part_number { get; set; }
        public string description { get; set; }
        public string category { get; set; }
        public decimal quantity { get; set; }
        public decimal estimate_price { get; set; }
        public string type { get; set; }
        public decimal price { get; set; }
        public string supplier_name { get; set; }
        public int id_item_supplier { get; set; }
    }
}
