﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class AllItemSitesSupplier
    {
        public int folio { get; set; }
        public string part_number { get; set; }
        public string description { get; set; }
        public decimal quantity { get; set; }
        public decimal price { get; set; }
        public decimal iva { get; set; }
        public decimal import { get; set; }
        public decimal total { get; set; }
        public int supplier_id { get; set; }
        public string supplier_name { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string email_manager { get; set; }
        public string phone { get; set; }
        public string potal_code { get; set; }
    }
}
