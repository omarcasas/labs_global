﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class EmailPORequirements
    {
        public Stream Reporte { get; set; }
        public string site_code { get; set; }
        public int id_supplier { get; set; }
        public int purchase_no { get; set; }
        public string site_name { get; set; }
    }
}
