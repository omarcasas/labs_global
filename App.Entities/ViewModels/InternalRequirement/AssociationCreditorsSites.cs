﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class AssociationCreditorsSites
    {
        public string site_code { get; set; }
        public int supplier_id { get; set; }
    }
}
