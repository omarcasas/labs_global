﻿using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class PurchasesOrderRequirementInfo
    {
        public int purchase_no { get; set; }
        public List<InternalRequirementDetailPOModel> items { get; set; }
        public SitesPOModel site { get; set; }
        public CreditorPOModel CPOModel { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
    }
}
