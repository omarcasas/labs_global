﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.SysInfo
{
    public class SysPageMaster
    {
        public string page_name { get; set; }
        public string url { get; set; }
        public string description { get; set; }
    }
}
