﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Expenses
{
    public class ExpensesReportModel
    {
        public int ReferenceFolio { get; set; }
        public string SiteName { get; set; }
        public string Category { get; set; }
        public string ReferenceType { get; set; }
        public string Supplier { get; set; }
        public string Currency { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitCost { get; set; }
        public decimal TotalIVA { get; set; }
        public decimal TotalIEPS { get; set; }
        public decimal Discount { get; set; }
        public decimal IvaRet { get; set; }
        public decimal IsrRet { get; set; }
        public decimal SubTotal { get; set; }
        public string Invoice { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
    }
}
