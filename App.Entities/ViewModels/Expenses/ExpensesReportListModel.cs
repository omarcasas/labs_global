﻿
using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.MaSupplier;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Expenses
{
    public class ExpensesReportListModel
    {
        public List<ExpensesReportModel> ExpensesReportModelList { get; set; }
        public List<MaSupplierDropDownModel> Ma_SupplierList { get; set; }
        //public List<ItemCreditorsCategoryModel> ItemCreditorsCategoryList { get; set; }
        public List<SiteModel> Site { get; set; }
        public List<MA_CODE> ListCategory { get; set; }

        public ExpensesReportListModel()
        {
            ExpensesReportModelList = new List<ExpensesReportModel>();
            Ma_SupplierList = new List<MaSupplierDropDownModel>();
            //ItemCreditorsCategoryList = new List<ItemCreditorsCategoryModel>();
            Site = new List<SiteModel>();
            ListCategory = new List<MA_CODE>();
        }
    }
}