﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Expenses
{
    public class ExpensesChart
    {
        public string ColumnString { get; set; }
        public decimal Value { get; set; }
    }
}