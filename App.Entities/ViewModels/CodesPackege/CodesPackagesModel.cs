﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CodesPackege
{
    public class CodesPackagesModel
    {
        public string PartNumberOrigin { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public bool Active { get; set; }
        public string UnitSize { get; set; }
        public List<CodesPackagesModel> CodesPackages { get; set; }
        public CodesPackagesModel()
        {
            CodesPackages = new List<CodesPackagesModel>();
        }
    }
}