﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaCode
{
    public class MaCodeModel
    {
        public string code { get; set; }
        public string VKey { get; set; }
    }
}
