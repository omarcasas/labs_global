﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.FixedAsset
{
    public class FixedAsserReportModel
    {
        public int fixed_asset_id { get; set; }
        public string fixed_asset_folio { get; set; }
        public string fixed_asset_name { get; set; }
        public string description { get; set; }
        public string site_name { get; set; }
        public DateTime? acquisition_date { get; set; }
        public string location { get; set; }
        public decimal? depreciation { get; set; }
        public decimal? price { get; set; }
        public decimal? depreciation_year { get; set; }
        public int? service_year { get; set; }
        public decimal? real_price { get; set; }
    }
}
