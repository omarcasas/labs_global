﻿using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryCalendarModel
    {

        public int id { get; set; }//id_calendar
        public string title { get; set; }//inventory_name
        public string description { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
        public DateTime start { get; set; }//calendar_date
        public int calendar_status { get; set; }
        public int typeInv { get; set; }
        public DateTime cdate { get; set; }
        public string cuser { get; set; }
        public DateTime udate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }
        public List<InventoryCalendarModel> InventoryCalendarModellist { get; set; }
        public List<SiteModel> siteList { get; set; }
        public List<InventoryTypeModel> InventoryTypeList { get; set; }

        
    }
}
