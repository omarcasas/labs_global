﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryTypeModel
    {
        public int IdInventoryType { get; set; }
        public string InventoryName { get; set; }
        public string TypeReview { get; set; }
        public string Day { get; set; }
        public bool ActiveInventory { get; set; }
        public TimeSpan CloseTime { get; set; }
    }
}
