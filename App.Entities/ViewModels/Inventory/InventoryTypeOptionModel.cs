﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryTypeOptionModel
    {
        public List<MA_CLASS> ListMaClass{ get; set; }
        public List<MA_CODE> ListMaCode { get; set; }
        public List<InventoryTypeModel> ListInventory { get; set; }
    }
}
