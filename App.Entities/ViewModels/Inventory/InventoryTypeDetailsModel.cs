﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryTypeDetailsModel
    {
        public int IdInventoryType { get; set; }
        public int LevelMaClass { get; set; }
        public string LevelName { get; set; }
        public bool ActiveDetails { get; set; }
        public bool IsCreate { get; set; }
        public string CloseTime { get; set; }
    }
}
