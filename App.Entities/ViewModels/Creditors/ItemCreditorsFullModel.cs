﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Creditors
{
    public class ItemCreditorsFullModel
    {
        public List<ItemSupplierCreditorsModel> ItemSupplierCreditorsList { get; set; }
        public List<ItemCreditorsCategoryModel> ItemCreditorsCategoryList { get; set; }
        public List<ItemCreditorsModel> ItemCreditorsList { get; set; }
        public List<TypeCreditorsModel> TypeCreditorsList { get; set; }
        public List<MaSupplierModel> SupplierList { get; set; }
        public List<MA_CODE> Currency { get; set; }
        public ItemCreditorsFullModel()
        {
            ItemSupplierCreditorsList = new List<ItemSupplierCreditorsModel>();
            ItemCreditorsCategoryList = new List<ItemCreditorsCategoryModel>();
            ItemCreditorsList = new List<ItemCreditorsModel>();
            TypeCreditorsList = new List<TypeCreditorsModel>();
            SupplierList = new List<MaSupplierModel>();
            Currency = new List<MA_CODE>();
        }
    }
    public class MaSupplierModel
    {
        public int SupplierId { get; set; }
        public string CommercialName { get; set; }
        public string BusinessName { get; set; }

        public string Rfc { get; set; }
        public string SupplierAddress { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string supplier_state { get; set; }
        public string Country { get; set; }
        public string SupplierType { get; set; }
        public string AccountingAccount { get; set; }
        public int PurchaseAppliedReturn { get; set; }
        public string Program { get; set; }
        public string Cser { get; set; }
        public string Cdate { get; set; }
        public string Uuser { get; set; }
        public string Udate { get; set; }
        public int truck_food { get; set; }

        //MA_SUPPLIER_CONTACT
        public int supplier_contact_id { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string departament { get; set; }
    }
}
