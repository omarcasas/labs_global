﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Creditors
{
    public class ItemSupplierCreditorsModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public int SupplierId { get; set; }
        public decimal Price { get; set; }
        public bool ActiveFlag { get; set; }
        public bool RequisitionFlag { get; set; }
        public string Type { get; set; }
        public string SupplierName { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string Currency { get; set; }
    }
}
