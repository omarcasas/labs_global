﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Creditors
{
    public class ItemCreditorsModel
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDiscription { get; set; }
        public bool RequisitionFlag { get; set; }
        public bool ActiveFlag { get; set; }
        public string Type { get; set; }
    }
}
