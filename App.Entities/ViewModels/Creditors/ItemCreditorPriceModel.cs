﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Creditors
{
    public class ItemCreditorPriceModel
    {
        public int SupplierPartNumberCreditors { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public bool ActiveFlag { get; set; }
    }
}
