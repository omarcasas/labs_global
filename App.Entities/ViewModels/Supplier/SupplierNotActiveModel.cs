﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Supplier
{
    public class SupplierNotActiveModel
    {
        public int supplier_id { get; set; }
        public string rfc { get; set; }
        public string commercial_name { get; set; }
        public string business_name { get; set; }
        public string supplier_address { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string supplier_state { get; set; }
        public string country { get; set; }
        public string supplier_type { get; set; }
        public string accounting_account { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public string program { get; set; }
        //public bool Enable { get; set; }
        public int SuplierPartNumber { get; set; }
    }
}
