﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PaymentMethod
{
    public class PaymentMethodModel
    {
        public PaymentMethodModel()
        {
            AllowExcededBalance = false;
            CanReturnChange = false;
            AcceptWithdraw = false;
            AcceptDonation = false;
            Menu = true;
            PaymentsMethods = new List<PAYMENT_METHOD>();
        }

        public int Id { get; set; }

        [DisplayName("Nombre: ")]
        public string Name { get; set; }

        [DisplayName("Activo: ")]
        public bool Menu { get; set; }

        [DisplayName("Menú orden:")]
        public int? MenuOrder { get; set; }

        [DisplayName("Código SAT:")]
        public string SatCode { get; set; }

        [DisplayName("Código teclado:")]
        public int? Key { get; set; }

        [DisplayName("Monto máximo permitido por venta:")]
        public decimal? MaxAllowedPerSale { get; set; }

        [DisplayName("Cambio máximo por venta:")]
        public decimal? MaxChangedAllowedPerSale { get; set; }

        [DisplayName("Cantidad en efectivo máxima para dar al cliente:")]
        public decimal? MaxAllowedCashBack { get; set; }

        [DisplayName("Permitir excederse en el balance:")]
        public bool AllowExcededBalance { get; set; }

        [DisplayName("Permitir regresar cambio:")]
        public bool CanReturnChange { get; set; }

        [DisplayName("Permitir retiros:")]
        public bool AcceptWithdraw { get; set; }

        [DisplayName("Permitir donaciones:")]
        public bool AcceptDonation { get; set; }

        [DisplayName("Orden de cancelación:")]
        public int? CancellationOrder { get; set; }        

        [DisplayName("Moneda:")]
        public string Currency { get; set; }

        [DisplayName("Tipo de vale: ")]
        public string TypeV { get; set; }

        public string PM { get; set; }

        public List<PAYMENT_METHOD> PaymentsMethods { get; set; }

        public PAYMENT_METHOD ITEM
        {
            get
            {
                return new PAYMENT_METHOD
                {
                    pm_id = Id,
                    pm_name = Name,
                    pm_menu = Menu,
                    sat_pm_code = SatCode,
                    pm_key = Key,
                    max_allowed_per_sale = MaxAllowedPerSale,
                    max_changed_allowed_per_sale = MaxChangedAllowedPerSale,
                    max_allowed_cash_back = MaxAllowedCashBack,
                    allow_exceeded_balance = AllowExcededBalance,
                    can_return_change = CanReturnChange,
                    accept_withdraw = AcceptWithdraw,
                    accept_donation = AcceptDonation,
                    cancellation_order = CancellationOrder,
                    payment_method1 = PM,
                    currency = Currency,
                    typev = TypeV
                };
            }
        }
    }
}
