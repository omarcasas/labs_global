﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Site
{
    public class SiteModel
    {
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public int Status { get; set; }
        public DateTime NewErpDate { get; set; }
        public bool? NewErp { get; set; }

    }
}
