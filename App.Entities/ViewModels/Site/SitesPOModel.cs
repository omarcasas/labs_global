﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Site
{
    public class SitesPOModel
    {
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string SitePostalCode { get; set; }
        public string SiteCity { get; set; }
        public string SiteState { get; set; }
        public string SiteEmail { get; set; }
        public string SitePhone { get; set; }
    }
}
