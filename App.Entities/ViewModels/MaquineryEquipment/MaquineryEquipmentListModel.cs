﻿using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaquineryEquipment
{
    public class MaquineryEquipmentListModel
    {
        public List<MaquineryEquipmentModel> MaquineryEquipmentList { get; set; }
        public List<MA_CODE> Deparments { get; set; }
        public List<SiteModel> Sites { get; set; }

        public MaquineryEquipmentListModel()
        {
            MaquineryEquipmentList = new List<MaquineryEquipmentModel>();
            Deparments = new List<MA_CODE>();
            Sites = new List<SiteModel>();
        }
    }
}
