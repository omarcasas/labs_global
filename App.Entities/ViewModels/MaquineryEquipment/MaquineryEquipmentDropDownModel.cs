﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaquineryEquipment
{
    public class MaquineryEquipmentDropDownModel
    {
        public int equipment_id { get; set; }
        public string equipment_description { get; set; }
    }
}
