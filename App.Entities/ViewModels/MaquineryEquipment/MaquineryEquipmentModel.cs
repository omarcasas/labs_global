﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaquineryEquipment
{
    public class MaquineryEquipmentModel
    {
        public int EquipmentId { get; set; }
        public string SerialNumber { get; set; }
        public string EquipmentDescription { get; set; }
        public string SiteCode { get; set; }
        public string DepartmentId { get; set; }
        public string Site { get; set; }
        public string Department { get; set; }
        public string SerialNumberSupplier { get; set; }
        public bool FlagActive { get; set; }
    }
}
