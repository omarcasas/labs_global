﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.SalesPriceChange
{
    public class SalesPriceChangeViewModel
    {
        public int sales_price_id { get; set; }
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string family { get; set; }
        public string category { get; set; }
        public DateTime applied_date { get; set; }
        public decimal margin { get; set; }
        public decimal sale_price { get; set; }
        public decimal sale_price_iva { get; set; }
        public decimal last_price { get; set; }
        public string status { get; set; }
        public DateTime cdate { get; set; }
        public string cuser { get; set; }
    }
}