﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.ServicesCalendar
{
    public class ServicesCalendarHeaderModel
    {
        public int service_id { get; set; }
        public DateTime service_date { get; set; }
        public int service_status { get; set; }
        public string site_code { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public string program_id { get; set; }
    }
}