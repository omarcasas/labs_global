﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PRT
{
    public class ReportDetail
    {
        public string do_no { get; set; }
        public string destination_site { get; set; }
        public int ? purchase_no { get; set; }
        public string date_prt { get; set; }
        public string date_erp { get; set; }
        public decimal purchase_total_prt { get; set; }
        public decimal purchase_total_erp { get; set; }
        public string status_prt { get; set; }
        public string status_erp { get; set; }
        public string status { get; set; }
        public string purchase_comments { get; set; }
        public string prtOrStore { get; set; }
    }

}
