﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PRT
{
    public class ReportHeader
    {
        public string site_code { get; set; }
        public string site_name { get; set; }
        public string ip_address { get; set; }
        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal subtotal { get; set; }
        public decimal totalPRT { get; set; }
        public decimal totalStore { get; set; }
        public decimal totalDifference { get
            {
                return (totalPRT - totalStore);
            } }
        public bool checkERP { get; set; }
    }
}
