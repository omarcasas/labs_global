﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PRT
{
    public class TransferItems
    {
        public string part_number { get; set; }
        public string  part_description { get; set; }
        public decimal quantity { get; set; }
        public decimal amount { get; set; }
    }
}
