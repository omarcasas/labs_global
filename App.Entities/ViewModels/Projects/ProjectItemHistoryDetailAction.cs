﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectItemHistoryDetailAction
    {
        public string file_name { get; set; }
        public string file_type { get; set; }
        public string file_base64 { get; set; }
    }
}
