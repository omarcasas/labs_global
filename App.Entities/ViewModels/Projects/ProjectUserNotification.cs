﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectUserNotification
    {
        public int id_label_project { get; set; }
        public string id_label_project_default { get; set; }
        public string emp_no { get; set; }
        public string user_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string all_emails { get; set; }
    }
}
