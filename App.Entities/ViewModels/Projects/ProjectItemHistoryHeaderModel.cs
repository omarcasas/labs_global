﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectItemHistoryHeaderModel
    {
        public ProjectItemHistoryHeaderModel()
        {
            table_item_history = new List<ProjectItemHistoryModel>();
        }

        public int id_proyect { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public DateTime compromise_date { get; set; }
        public decimal total_cost { get; set; }
        public decimal estimate { get; set; }
        public int id_category { get; set; }
        public string category { get; set; }
        public int id_label { get; set; }
        public string label_name { get; set; }
        public string label_emp_no { get; set; }
        public string label_emp_no_user_name { get; set; }
        public int project_status { get; set; }

        public List<ProjectItemHistoryModel> table_item_history { get; set; }
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
    }
}
