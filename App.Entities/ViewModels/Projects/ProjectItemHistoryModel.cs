﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectItemHistoryModel
    {
        public ProjectItemHistoryModel()
        {
            table_attached = new List<ProjectItemHistoryDetail>();
        }
        public int id_label { get; set; }
        public int id_item { get; set; }
        public string item_description { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public DateTime? real_start_date { get; set; }
        public DateTime? real_end_date { get; set; }
        public decimal estimate { get; set; }
        public decimal total_cost { get; set; }
        public int consecutive_item { get; set; }
        public int item_status{ get; set; }
        public string ImageBase64
        {
            get
            {
                var image = "";
                if (user_photo != null)
                    image = Convert.ToBase64String(user_photo);
                this.user_photo = null;
                return image;
            }
        }
        public int id_history { get; set; }
        public byte[] user_photo { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public string user_comment { get; set; }
        public int post_numbers { get; set; }
        public DateTime? post_date { get; set; }
        public bool user_label_lead { get; set; }
        public bool user_label_follow { get; set; }

        public List<ProjectItemHistoryDetail> table_attached { get; set; }
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
    }
}
