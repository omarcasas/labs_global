﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectReportModel
    {
        public List<ProjectModel> Projects { get; set; }

        public List<ProjectCategoryModel> Categories { get; set; }

        public ProjectReportModel()
        {
            Projects = new List<ProjectModel>();
            Categories = new List<ProjectCategoryModel>();
        }
    }

    public class ProjectModel
    {
        public decimal today_cost_usd;
        public decimal cost_by_week_usd;
        public int ProjectId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Category { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime CompromiseDate { get; set; }
        public decimal EstimateCost { get; set; }
        public decimal TotalCost { get; set; }
        public string CreatedBy { get; set; }
        public decimal? TodayCost { get; set; }
        public decimal? CostByWeek { get; set; }

        public List<ItemModel> Items { get; set; }

        public List<CategoryUserModel> CategoryUsers { get; set; }

        public ProjectModel()
        {
            Items = new List<ItemModel>();
            CategoryUsers = new List<CategoryUserModel>();
        }
        
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
    }

    public class ItemModel
    {
        public int ItemId { get; set; }
        public int LabelId { get; set; }
        public string ItemDescription { get; set; }
        public string LabelDescription { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? RealEndDate { get; set; }
        public decimal EstimateCost { get; set; }
        public string CreatedBy { get; set; }
        public int? Days { get; set; }
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
    }

    public class CategoryUserModel
    {
        public string EmployeeNumber { get; set; }
        public string Name { get; set; }
        public string ImageBase64
        {
            get
            {
                var image = "";
                if (Photo != null)
                    image = Convert.ToBase64String(Photo);
                this.Photo = null;
                return image;
            }
        }

        public byte[] Photo { get; set; }
    }
}
