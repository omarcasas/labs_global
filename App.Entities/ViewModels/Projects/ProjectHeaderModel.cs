﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Projects;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectHeaderModel
    {
        public int id_project { get; set; }
        public DateTime start_date { get; set; }
        public DateTime? end_date { get; set; }
        public DateTime compromise_date { get; set; }
        public string compromise_date_str { get; set; }
        public decimal estimate { get; set; }
        public decimal total_cost { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public int id_category { get; set; }
        public string cuser { get; set; }
        public string currentuser { get; set; }
        public List<string> users_category { get; set; }
        public List<ProjectLabelModel> label { get; set; }
        
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
    }
}
