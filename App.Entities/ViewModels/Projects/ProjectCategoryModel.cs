﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectCategoryModel
    {
        public int id_category { get; set; }
        public string description { get; set; }
    }
}
