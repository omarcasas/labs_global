﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectItemHistoryDetail
    {
        public int id_history_detail { get; set; }
        public int id_history { get; set; }
        public string document_image { get; set; }
        public string document_pdf { get; set; }
        public DateTime post_Date { get; set; }
        public string post_user { get; set; }
    }
}
