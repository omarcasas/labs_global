﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectItemModel
    {
        public int IdItem { get; set; }
        public int IdLabelProject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? RealStartDate { get; set; }
        public DateTime? RealEndDate { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public decimal Estimate { get; set; }
        public decimal TotalCost { get; set; }
        public decimal NTotalCost { get; set; }
        public int ConsecutiveItem { get; set; }
        
        public int ActualInvoiceCount { get; set; }
        
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
        public decimal ntotal_cost_usd { get; set; }
    }
}
