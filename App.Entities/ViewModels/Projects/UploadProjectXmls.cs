using System.Collections.Generic;
using App.Entities.ViewModels.XML;

namespace App.Entities.ViewModels.Projects
{
    public class UploadProjectXmls
    {
        public string program_id;
        public ProjectLabelModel label { get; set; }
        public List<ProjectItemModel> items { get; set; }
        public List<XMLFile> xmls { get; set; }
    }
}