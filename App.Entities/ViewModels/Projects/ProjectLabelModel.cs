﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Projects
{
    public class ProjectLabelModel
    {
        public int IdLabelProject { get; set; }
        public int IdProject { get; set; }
        public string IdLabelProjectDefault { get; set; }
        public decimal Estimate { get; set; }
        public decimal TotalCost { get; set; }
        public int ItemsCount { get; set; }
        public string Description { get; set; }
        public string EmpNo { get; set; }
        public string ImageBase64
        {
            get
            {
                var image = Convert.ToBase64String(Image);
                //comentar en caso de no funcionar
                this.Image = null;
                return image;
            }
        }
        public System.Byte[] Image { get; set; }
        public string FullName { get; set; }
        public string userLeaderName { get; set; }
        public bool ActiveLabel { get; set; }
        public decimal EstimateCard { get; set; }
        public decimal EstimateCardUsd { get; set; }
        public List<string> usersLabel { get; set; }
        public List<string> usersNameLabels { get; set; }
        
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
    }
    
    public class RTPProjectLabelModel
    {
        public int IdLabelProject { get; set; }
        public int IdProject { get; set; }
        public string IdLabelProjectDefault { get; set; }
        public decimal Estimate { get; set; }
        public decimal TotalCost { get; set; }
        public string EmpNo { get; set; }
        public bool ActiveLabel { get; set; }
        public IEnumerable<ProjectItemModel> Items { get; set; }
        
        // total de facturas hasta este momento
        public decimal ActualTotalCost { get; set; }
        public int ActualInvoiceCount { get; set; }
        public string EmpName { get; set; }
        
        public decimal estimate_usd { get; set; }
        
        public decimal total_cost_usd { get; set; }
        public decimal ActualTotalCostUsd { get; set; }
    }
}
