﻿using App.Entities.ViewModels.CodesPackege;
using System.Collections.Generic;

namespace App.Entities.ViewModels.Item
{
    public class ItemMasterModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string unit_size { get; set; }
        public string part_type { get; set; }
        public string buyer_division { get; set; }
        public string part_iva { get; set; }
        public string part_ieps { get; set; }
        public string part_number_sat { get; set; }
        public string unit_sat { get; set; }
        public bool weight_flag { get; set; }
        public bool active_flag { get; set; }
        public string price { get; set; }
        public string cost { get; set; }

        public List<CodesPackagesModel> CodesPackages { get; set; }
        public ItemMasterModel()
        {
            CodesPackages = new List<CodesPackagesModel>();
        }
    }
}
