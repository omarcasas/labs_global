﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemBlockedModel
    {

        public string part_number { get; set; }
        public string site_code { get; set; }
        public bool active_flag { get; set; }


    }
}
