﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemComboModel
    {
        public string part_number_combo { get; set; }
        public string part_number { get; set; }
        public string part_description_combo { get; set; }
        public string combo_status { get; set; }
        public Nullable<decimal> quantity { get; set; }
        public Nullable<decimal> iva { get; set; }
        public string ivaDescription { get; set; }
        public string margen_part_number{ get; set; }
        public bool active_status { get; set; }
        public bool weightValue { get; set; }
        public Nullable<decimal> base_cost { get; set; }
        public Nullable<decimal> total_price_sale { get; set; }
        // statusInsert determina si el producto del combo se insertara o se actualizara 
        public bool statusInsert { get; set; }

    }
}
