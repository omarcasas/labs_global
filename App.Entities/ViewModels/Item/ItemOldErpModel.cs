﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemOldErpModel
    {
        //ERP Viejo Supplier Item

        //public Int64 Supplier_id { get; set; }
        public string Supplier_name { get; set; }
        public string vkey { get; set; }
        public int grupo_id { get; set; }
        public string grupo_desc { get; set; }
        public int level1_code { get; set; }
        public string level1_desc { get; set; }
        public string um_desc { get; set; }
        public int buyer_id { get; set; }
        public string description { get; set; }

        public string commercial_name { get; set; }
        public int supplier_id { get; set; }


    }
}
