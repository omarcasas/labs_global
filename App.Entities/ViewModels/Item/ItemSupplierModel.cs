﻿using System;
namespace App.Entities.ViewModels.Item
{
    public class ItemSupplierModel
    {
        public string item_number { get; set; }
        public string item_name { get; set; }
        public string item_cost { get; set; }
        public string item_currency { get; set; }
        public string item_base_cost_id { get; set; }
        public string item_supplier_id { get; set; }
        public decimal quantity { get; set; }
        public string type { get; set; }
        public DateTime fecha { get; set; }
        public string uDate { get; set; }
        public string margen { get; set; }
        public string iva { get; set; }
        public string ieps { get; set; }
        public string price { get; set; }
        public string currency { get; set; }
        public string supplier_name { get; set; }
        public string packing_size { get; set; }
    }
    public class itemsSuppier
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal promotion_price { get; set; }
        public int item_amount { get; set; }
        public int item_quantity { get; set; }
    }
}
