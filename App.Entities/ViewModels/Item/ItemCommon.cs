﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemCommon
    {
        public string Part_number { get; set; }
        public decimal Quantity { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public string Price { get; set; }
        public bool Presentation { get; set; }
        public string part_description { get; set; }
        public bool weight_flag { get; set; }
        public int presentation_id { get; set; }
        public bool forced_90 { get; set; }
    }
}
