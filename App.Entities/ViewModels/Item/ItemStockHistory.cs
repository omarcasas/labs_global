﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemStockHistory
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Monto { get; set; }
        public decimal? Costo { get; set; }
    }
}