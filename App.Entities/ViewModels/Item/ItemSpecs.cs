﻿using System;

namespace App.Entities.ViewModels.Item
{
    public class ItemSpecs
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public Nullable<decimal> packing_of_size { get; set; }
        public Nullable<decimal> pallet_of_size { get; set; }
        public Nullable<decimal> weight_of_packing { get; set; }
        public string dun14 { get; set; }
        public string gr_storage_location { get; set; }
    }
}
