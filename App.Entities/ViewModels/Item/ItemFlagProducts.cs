﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemFlagProducts
    {
        public ItemFlagProducts()
        {
            FlagFalse = new List<ItemMasterModel>();
            FlagTrue = new List<ItemMasterModel>();
        }
        public List<ItemMasterModel> FlagTrue { get; set; }
        public List<ItemMasterModel> FlagFalse { get; set; }
    }
}
