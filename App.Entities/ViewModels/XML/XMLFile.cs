namespace App.Entities.ViewModels.XML
{
    public class XMLFile
    {
        public string xml { get; set; }
        public string xml_name { get; set; }
    }
}