﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Donations
{
    public class Donation
    {        
        public Donation()
        {            
            AmountRaised = 0.0m;
            StartDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.Date.AddDays(1).ToString("dd/MM/yyyy");
            DonationsList = new List<Donation>();          
        }

        public int Id { get; set; }

        [DisplayName("Nombre de la Campaña: ")]
        public string Name { get; set; }

        [DisplayName("Organización: ")]
        public string Organization { get; set; }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Cantidad Recaudada: ")]
        public decimal AmountRaised { get; set; }

        public List<Donation> DonationsList { get; set; }        
    }
}
