﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Donations
{
    public class DonationItem
    {

        public DonationItem()
        {
            IsActive = true;
            IsFromEdit = false;
        }

        public int Id { get; set; }

        [DisplayName("Nombre del Artículo: ")]
        public string Article { get; set; }

        [DisplayName("Precio: ")]
        public decimal Price { get; set; }

        [DisplayName("Status: ")]
        public bool IsActive { get; set; }

        public bool IsFromEdit { get; set; }
        public int DonationId { get; set; }
    }
}
