﻿using App.Entities.ViewModels.CodesPackege;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.ItemPresentation
{
    public class PresentationViewModel
    {
        public List<ItemPresentationModel> Presentations { get; set; }
        public List<ItemMasterModel> Items { get; set; }
        public List<CodesPackagesModel> CodesPackages { get; set; }

        public PresentationViewModel()
        {
            Presentations = new List<ItemPresentationModel>();
            Items = new List<ItemMasterModel>();
            CodesPackages = new List<CodesPackagesModel>();
        }
    }
}