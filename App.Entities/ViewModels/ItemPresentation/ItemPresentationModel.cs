﻿using App.Entities.ViewModels.CodesPackege;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.ItemPresentation
{
    public class ItemPresentationModel
    {
        public int PresentationId { get; set; }
        public string Name { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public string PackingType { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string PresentationCode { get; set; }
        public string FactorUnitSize { get; set; }
        public bool ActiveFlag { get; set; }
        public bool priceChangeNow { get; set; }
        public bool ShowInLabel { get; set; }
        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime appliedDate { get; set; }
        public List<CodesPackagesModel> CodesPackages { get; set; }

        public ItemPresentationModel()
        {
            CodesPackages = new List<CodesPackagesModel>();
        }
    }
}