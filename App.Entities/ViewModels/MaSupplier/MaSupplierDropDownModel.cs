﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaSupplier
{
    public class MaSupplierDropDownModel
    {
        public int supplier_id { get; set; }
        public string business_name { get; set; }
    }
}
