﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  App.Entities.ViewModels.Invoices
{
   public  class CedulasModel
    {
        public string _month { get; set; }
    }
    public class CedulaModel
    {
        public CedulaModel() { }

        public string Uuid { get; set; }
        public string SerieFolio { get; set; }
        public string FolioRel { get; set; }
        public string Folio { get; set; }

        public string Serie { get; set; }
        public string Sucursal { get; set; }

        public string Cliente { get; set; }
        public string Rfc { get; set; }
        public string FechaEmi { get; set; }
        public string FolioFiscal { get; set; }
        public string ServicioPrestado { get; set; }
        public decimal MontoOperacionMonedaExt { get; set; }
        public decimal ivaMonedaExt { get; set; }
        public decimal ImporteMonedaExtra { get; set; }
        public string FechaPublicacion{ get; set; }
        public decimal ImpuestoRetenido { get; set; }
        public string tipo16 { get; set; }
        public string tipo8 { get; set; }
        public string tipo0 { get; set; }

        public string Moneda { get; set; }
        public string TipoCambio { get; set; }

        public decimal MontoOperacion { get; set; }
        public decimal MontoBaseT0 { get; set; }
        public decimal MontoBaseT16 { get; set; }
        public decimal IvaTrasladado16 { get; set; }
        public decimal MontoBaseT8 { get; set; }
        public decimal IvaTrasladado8 { get; set; }
        public decimal Tasa { get; set; }
        public decimal IepsTrasladado { get; set; }
        public decimal TotalImpuestos { get; set; }
        public decimal ValorTotalFactura { get; set; }

       
        //diferido mes anterior
        public decimal MontoOperacionMesAnt { get; set; }
        public decimal MontoBaseT0MesAnt { get; set; }
        public decimal MontoBaseT16MesAnt { get; set; }
        public decimal IvaTrasladado16MesAnt { get; set; }
        public decimal MontoBaseT8MesAnt { get; set; }
        public decimal IvaTrasladado8MesAnt { get; set; }
        public decimal TasaMesAnt { get; set; }
        public decimal IepsTrasladadoMesAnt { get; set; }
        public decimal TotalImpuestosMesAnt { get; set; }
        public decimal ValorTotalFacturaMesAnt { get; set; }
        public string Tipo { get; set; }
        //
        public string Clave { get; set; }
        public string MetodoPago { get; set; }
        public string NumeroParcialidad { get; set; }
        //Complemento
        public string FolioFiscaC { get; set; }
        public string FechaEmisionC { get; set; }
        public decimal ImporteSaldoAnterior { get; set; }
        public decimal ImportePagado { get; set; }
        public decimal ImporteSaldoInsoluto { get; set; }
        public string UsoCfdi { get; set; }
        public string FormaPago { get; set; }
        public string AbonadoPorc { get; set; }
        public string MontoOperacionCobrado { get; set; }
        public decimal MontoBaseT0C { get; set; }
        public decimal MontoBaseT0AdicionadoImp { get; set; }
        public decimal MontoBaseT16C { get; set; }
        public decimal IvaTrasladadoCobrado16 { get; set; }
        public decimal MontoBaseT8C { get; set; }
        public decimal IvaTrasladadoCobrado8 { get; set; }
        public decimal IepsladadoCobrado { get; set; }
        public decimal ImporteTotalCobrado { get; set; }
        public decimal ImporteComplemento { get; set; }
        public string FechaCobro { get; set; }
        public string ReferenciaBancaria { get; set; }
        public string NombreInstitucionFinanciera { get; set; }
     
        public decimal TotalPorRFC { get; set; }








    }
}
