﻿using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceXml
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public string InvoiceNo { get; set; }
        public string Serie { get; set; }
        public string FileName { get; set; }
        public string Xml { get; set; }
        public string InvoiceType { get; set; }
        public string CDFI { get; set; }
        public int? SupplierId { get; set; }
        public string CustomerCode { get; set; }
        public string SiteCode { get; set; }
        public bool Active { get; set; }
        public int Status { get; set; }
        public string UserCreated { get; set; }
        public DateTime CreateDate { get; set; }
        public string UserUpdated { get; set; }
        public DateTime UpdateDate { get; set; }
        public List<MA_SUPPLIER> Suppliers { get; set; }
        public List<Customer> Customers { get; set; }
        public List<SiteModel> Sites { get; set; }

        public InvoiceXml()
        {
            Suppliers = new List<MA_SUPPLIER>();
            Customers = new List<Customer>();
            Sites = new List<SiteModel>(); 
        }
    }
}