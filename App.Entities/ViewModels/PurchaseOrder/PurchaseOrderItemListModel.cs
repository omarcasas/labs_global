﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderItemListModel
    {
        public string PartNumber { get; set; }
        public decimal Quantity { get; set; }
        public decimal grQuantity { get; set; }
        public string UnitSize { get; set; }
        public decimal PurchasePrice { get; set; }
        public int Parcking { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal ItemTotalAmount { get; set; }
        public decimal TotalPacking { get; set; }
        public string Description { get; set; }
        public int itemStatus { get; set; }

        //cantidad solicitada
        public decimal requestedQuantity { get; set; }
        //cantidad surtidad
        public decimal assortedQuantity { get; set; }
        //eficiencia en monto
        public decimal efficiencyInAmount { get; set; }
        //eficiencia en cantidad
        public decimal efficiencyInQuantity { get; set; }
        //monto solicitado
        public decimal requestedAmount { get; set; }
        //monto surtido
        public decimal assortedAmount { get; set; }
        public int folio { get; set; }
        public string iva_text { get; set; }
        public string ieps_text { get; set; }
    }

    public class PurchaseItemReportModel
    {
        public DateTime Date { get; set; }
        public string PurchaseNo { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public string Currency { get; set; }
        public int SupplierId { get; set; }
        public string Supplier { get; set; }
        public string InvoiceNo { get; set; }
    }
}
