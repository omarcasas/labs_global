﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class UserModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string BuyerDivicion { get; set; }
    }
    public class PurchesOrderAndUserModel
    {
        public PurchesOrderAndUserModel()
        {
            this.ListPurchase = new List<PurchaseOrderXMLModel>();
            this.User = new UserModel();
        }
        public List<PurchaseOrderXMLModel> ListPurchase { get; set; }
        public UserModel User { get; set; }
    }

    public class PurchaseAndMaCode
    {
        public List<MA_CODE> MaCodeList { get; set; }
        public List<PurchaseOrderXMLModel> PurchaseList { get; set; }
        public PurchaseAndMaCode()
        {
            MaCodeList = new List<MA_CODE>();
            PurchaseList = new List<PurchaseOrderXMLModel>();
        }
    }

    public class PurchaseOrderXMLModel
    {
        public string PurchaseNo { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public int supplier_id { get; set; }
        public string supplier_name { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime Eta { get; set; }
        public string Currency { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public int Factor { get; set; }
        public decimal Quantity { get; set; }
        public string UnitSize { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal ItemTotalAmount { get; set; }
        public int purchase_status_value { get; set; }
        public int Folio { get; set; }
        public string invoice { get; set; }
        public string purchase_status_description { get; set; }
        //cantidad solicitada
        public decimal requestedQuantity { get; set; }
        //cantidad surtidad
        public decimal assortedQuantity { get; set; }
        //eficiencia en monto
        public decimal efficiencyInAmount { get; set; }
        //eficiencia en cantidad
        public decimal efficiencyInQuantity { get; set; }
        //monto solicitado
        public decimal requestedAmount { get; set; }
        //monto surtido
        public decimal assortedAmount { get; set; }
        public string TransationType { get; set; }
    }
}