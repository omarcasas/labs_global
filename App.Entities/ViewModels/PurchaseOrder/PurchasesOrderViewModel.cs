﻿using App.Entities.ViewModels.MaCode;
using App.Entities.ViewModels.MaSupplier;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchasesOrderViewModel
    {
        public int Supplier { get; set; }
        public DateTime OrderDate { get; set; }
        public string Currency { get; set; }
        public int Status { get; set; }
        public string Comment { get; set; }
        public virtual List<PurchaseOrderModel> PurchasesOrder { get; set; }
        public string PartName { get; set; }
        //cantidad solicitada
        public string RequestedQuantity { get; set; }
        //cantidad surtidad
        public string AssortedQuantity { get; set; }
        //eficiencia en monto
        public string EfficiencyInAmount { get; set; }
        //eficiencia en cantidad
        public string EfficiencyInQuantity { get; set; }
        //monto solicitado
        public string RequestedAmount { get; set; }
        //monto surtido
        public string AssortedAmount { get; set; }
        //numero orden
        public string Purchase_no { get; set; }
        public List<SiteModel> Sites { get; set; }
        public virtual List<MaSuppliersModel> Suppliers { get; set; }
        public List<MA_CLASS> Departaments { get; set; }
        public List<MaCodeModel> CurrencyList { get; set; }
        public PurchasesOrderViewModel()
        {
            Sites = new List<SiteModel>();
            Departaments = new List<MA_CLASS>();
            CurrencyList = new List<MaCodeModel>();
        }

        public IEnumerable<SelectListItem> SuppliersList
        {
            get
            {
                return FillSelectListItems();
            }
        }

        public IEnumerable<SelectListItem> DepartamentsList
        {
            get
            {
                return FillSelectListDepartaments();
            }
        }

        public List<SelectListItem> FillSelectListItems()
        {
            List<SelectListItem> suppliersL = new List<SelectListItem>
            {
                new SelectListItem { Text = "Seleccione una opción", Value = "" }
            };
            foreach (var n in Suppliers)
            {
                suppliersL.Add(new SelectListItem { Text = n.BusinessName, Value = n.SupplierId.ToString() });
            }
            return suppliersL;
        }

        public List<SelectListItem> FillSelectListDepartaments()
        {
            List<SelectListItem> departmentsL = new List<SelectListItem>
            {
                new SelectListItem { Text = "Seleccione un departamento", Value = "" }
            };
            foreach (var n in Departaments)
                departmentsL.Add(new SelectListItem { Text = n.description, Value = n.class_id.ToString() });

            return departmentsL;
        }
    }
}