﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderModel
    {
        public int purchase_no { get; set; }
        public int folio_requirement { get; set; }
        public int supplier_id { get; set; }
        public string site_code { get; set; }
        public DateTime purchase_date { get; set; }
        public int purchase_status { get; set; }
        public string currency { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime? udate { get; set; }
        public string program_id { get; set; }
    }
}
