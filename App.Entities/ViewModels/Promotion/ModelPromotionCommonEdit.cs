﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class ModelPromotionCommonEdit
    {
        public string part_number{ get; set; }
        public string part_description{ get; set; }
        public string promotion_past_price { get; set; }
        public int comun_id { get; set; }
    }
}
