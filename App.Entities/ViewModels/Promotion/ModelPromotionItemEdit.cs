﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class ModelPromotionItemEdit
    {
        public string part_number { get; set; }
        public decimal promotion_price { get; set; }
        public int promotion_detail_id { get; set; }
        public bool presentation_flag { get; set; }
        public int presentation_id { get; set; }
        public int quantity_total { get; set; }
        public decimal price { get; set; }
        public int common_id { get; set; }
        public bool common_flag { get; set; }
        public bool delete_flag { get; set; }
        public bool update_flag { get; set; }
    }
}
