﻿using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class PromotionEdit
    {
        public PromotionEdit()
        {
            siteModel = new List<SiteModel>();
            CommonEdit = new List<ModelPromotionCommonEdit>();
        }

        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal ieps { get; set; }
        public decimal iva { get; set; }
        public decimal price_iva_current { get; set; }
        public int quantity_promotion { get; set; }
        public decimal total_price_current { get; set; }
        public decimal total_price_promotion { get; set; }
        public bool comun_flag { get; set; }
        public bool delete_flag { get; set; }
        public bool update_flag { get; set; }
        public bool weight_flag { get; set; }
        public bool forced90 { get; set; }
        public int presentation_id { get; set; }
        public string promotion_baseTable { get; set; }
        public string base_priceTable { get; set; }
        public decimal promotion_price { get; set; }
        public bool presentation_flag { get; set; }
        public int pos { get; set; }
        public int promotion_quantity { get; set; }
        public string promotion_past_price { get; set; }
        public decimal promotion_past_total { get; set; }
        public int promotion_group { get; set; }
        public decimal promotion_origin_price { get; set; }

        public List<SiteModel> siteModel { get; set; }
        public List<ModelPromotionCommonEdit> CommonEdit { get; set; }

    }
}