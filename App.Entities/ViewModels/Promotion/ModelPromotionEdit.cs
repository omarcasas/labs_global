﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class ModelPromotionEdit
    {
        public string promotion_code { get; set; }
        public string promotion_type { get; set; }
        public DateTime promotion_start_date { get; set; }
        public DateTime promotion_end_date { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string program { get; set; }
        public string description { get; set; }

        //public PROMOTION_HEADER promotionH
        //{
        //    get
        //    {
        //        return new PROMOTION_HEADER
        //        {
        //            promotion_type = promotion_type,
        //            promotion_start_date = promotion_start_date,
        //            promotion_end_date = promotion_end_date,
        //            cuser = cuser,
        //            cdate = cdate,
        //            program_id = ""
        //        };
        //    }
        //}
    }
}
