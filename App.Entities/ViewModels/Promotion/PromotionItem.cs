﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class PromotionItem
    {
        public string promotion_code { get; set; }
        public string description { get; set; }
        public string part_number { get; set; }
        public int? quantity { get; set; }
        public decimal price { get; set; }
        public int common { get; set; }
    }
}
