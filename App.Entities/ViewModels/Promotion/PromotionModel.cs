﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class PromotionModel
    {
        public string promotion_code { get; set; }
        public string promotion_type { get; set; }
        public string promotion_descrip { get; set; }
        public string part_number { get; set; }
        public decimal promotion_price_iva { get; set; }
        public DateTime promotion_start_date { get; set; }
        public DateTime promotion_end_date { get; set; }
        public DateTime promotion_date_create { get; set; }
        public string promotion_create_user { get; set; }
        public string promotion_site_code { get; set; }
        public string promotion_site_name { get; set; }
        public string store { get; set; }
        public int replication_status { get; set; }
    }
}
