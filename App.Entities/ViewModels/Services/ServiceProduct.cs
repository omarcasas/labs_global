﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Entities.ViewModels.Services
{
    public class ServiceProduct
    {
        public ServiceProduct()
        {
            Enabled = true;
            Type = TypeEnum.One;
            Products = new List<ServiceProduct>();
        }

        [DisplayName("Descripción: ")]
        public string Description { get; set; }

        [DisplayName("Compañía: ")]
        public int CompanyId { get; set; }

        [DisplayName("Código: ")]
        public string PartNumber { get; set; }

        [DisplayName("Sku: ")]
        public string Sku { get; set; }

        [DisplayName("Precio: ")]
        public decimal Price { get; set; }

        [DisplayName("Comisión: ")]
        public decimal Commission { get; set; }

        [DisplayName("Tipo: ")]
        public TypeEnum Type { get; set; }

        [DisplayName("Activo: ")]
        public bool Enabled { get; set; }

        [DisplayName("Orden: ")]
        public int Order { get; set; }

        [DisplayName("Dígito Verificador:")]
        public string VerifyDigit { get; set; }

        [DisplayName("Código de comisión: ")]
        public string CommissionPartNumber { get; set; }

        [DisplayName("Provedor: ")]
        public string Provider { get; set; }
    
        [DisplayName("Servicio: ")]
        public int ServiceId { get; set; }

        public List<ServiceProduct> Products { get; set; }

        public string ServiceName { get; set; }
        public string CompanyName { get; set; }
        public int ProductId { get; set; }

        public DFLPOS_SERVICES_PRODUCTS ITEM
        {
            get
            {
                return new DFLPOS_SERVICES_PRODUCTS
                {
                    service_id = ServiceId,
                    description = Description,
                    part_number = PartNumber,
                    sku = PartNumber + VerifyDigit,
                    price = Price,
                    commission = Commission,
                    enabled = Enabled,
                    order = Order,
                    company_id = CompanyId,
                    commission_part_number = CommissionPartNumber,
                    provider = Provider                    
                };
            }
        }
    }

    public enum TypeEnum
    {
        [Display(Name = "1")]
        One = 1,

        [Display(Name = "3")]
        Three = 3
    }    
}
