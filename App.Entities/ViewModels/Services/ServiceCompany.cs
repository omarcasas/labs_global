﻿using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Services
{
    public class ServiceCompany
    {
        public ServiceCompany()
        {
            Companies = new List<ServiceCompany>();
            IsActive = true;
        }

        [DisplayName("Nombre: ")]
        public string Name { get; set; }

        [DisplayName("Orden: ")]
        public int Order { get; set; }

        [DisplayName("Activo: ")]
        public bool IsActive { get; set; }

        public int CompanyId { get; set; }

        public List<ServiceCompany> Companies { get; set; }
    }
}
