﻿using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Services
{
    public class Service
    {
        public Service()
        {
            IsActive = true;
            Services = new List<Service>();
        }

        [DisplayName("Nombre: ")]
        public string Name { get; set; }

        [DisplayName("Orden: ")]
        public int Order { get; set; }

        [DisplayName("Activo: ")]
        public bool IsActive { get; set; }

        public int ServiceId { get; set; }

        public List<Service> Services { get; set; }
    }
}
