﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfers
{
    public class TransferStoresSPModel
    {
        public string site_code { get; set; }
        public string site_name { get; set; }
        public decimal? enviado { get; set; }
        public decimal? recibido { get; set; }
        public string site_selected { get; set; }
    }
}
