﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfers
{
    public class TransferHeaderModel
    {
        public string TransferDocument { get; set; }
        public string TransferSiteCode { get; set; }
        public string TransferSiteName { get; set; }
        public string TransferSiteAddress { get; set; }
        public string TransferDate { get; set; }
        public DateTime TransferDateD { get; set; }
        public string DriverCode { get; set; }
        public string DriverName { get; set; }
        public string Comment { get; set; }
        public int TransferStatus { get; set; }
        public int PrintStatus { get; set; }
        public string Status { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? GR_quantity { get; set; }
        public string CUser { get; set; }
        public string UUser { get; set; }
        public string AutorizeName { get; set; }
        public string AccomplishedName { get; set; }
        public string DestinationSiteCode { get; set; }
        public string DestinationSiteName { get; set; }
        public string DestinationSiteAddress { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalIva { get; set; }
        public decimal TotalIEPS { get; set; }
        public string Diferences { get; set; }
        public List<TransferDetailModel> Items { get; set; }
    }

}
