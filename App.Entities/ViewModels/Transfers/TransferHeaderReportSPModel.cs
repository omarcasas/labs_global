﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfers
{
    public class TransferHeaderReportSPModel
    {
        public string transfer_document { get; set; }
        public string site_name { get; set; }
        public string site_code { get; set; }
        public string transfer_date_in { get; set; }
        public string transfer_date_out { get; set; }
        public string transfer_type { get; set; }
        public int transfer_status { get; set; }
        public decimal subtotal { get; set; }
        public decimal total { get; set; }
        public decimal iva { get; set; }
        public decimal ieps { get; set; }
        public string transfer_site_name { get; set; }
    }
}
