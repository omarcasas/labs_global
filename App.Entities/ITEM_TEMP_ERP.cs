//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ITEM_TEMP_ERP
    {
        public string product_id { get; set; }
        public string product_name { get; set; }
        public string product_description { get; set; }
        public string product_nick { get; set; }
        public string um { get; set; }
        public string iva { get; set; }
        public string ieps { get; set; }
        public Nullable<int> class_division { get; set; }
        public Nullable<int> class_section { get; set; }
        public Nullable<int> class_family { get; set; }
        public Nullable<int> class_subfamily { get; set; }
        public string class_duration { get; set; }
        public string class_conservation { get; set; }
        public string class_rotation { get; set; }
        public string distribution_type { get; set; }
        public string buyer_division { get; set; }
        public string product_type { get; set; }
        public Nullable<decimal> ERP_part_cost { get; set; }
        public string ERP_part_cost_cur { get; set; }
        public Nullable<decimal> ERP_part_price { get; set; }
        public Nullable<decimal> ERP_part_price3 { get; set; }
        public string ERP_part_price_cur { get; set; }
        public Nullable<long> ERP_supplier_id { get; set; }
        public Nullable<int> ERP_level1_code { get; set; }
        public string ERP_part_status { get; set; }
        public string ERP_part_um { get; set; }
        public Nullable<int> ERP_grupo_id { get; set; }
        public Nullable<decimal> ERP_part_margen { get; set; }
        public Nullable<decimal> ERP_part_margen3 { get; set; }
        public string ERP_part_pesado { get; set; }
        public Nullable<int> ERP_buyer_id { get; set; }
        public string mrp_type { get; set; }
        public Nullable<bool> active_fag { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string sat_code { get; set; }
    }
}
