//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class INVENTORY_TYPE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVENTORY_TYPE()
        {
            this.INVENTORY_TYPE_DETAILS = new HashSet<INVENTORY_TYPE_DETAILS>();
        }
    
        public int id_inventory_type { get; set; }
        public string inventory_name { get; set; }
        public string type_review { get; set; }
        public string day { get; set; }
        public bool active_inventory { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
        public Nullable<System.TimeSpan> close_time { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVENTORY_TYPE_DETAILS> INVENTORY_TYPE_DETAILS { get; set; }
    }
}
