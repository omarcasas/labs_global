//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PROJECT_ITEM_HISTORY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PROJECT_ITEM_HISTORY()
        {
            this.PROJECT_ITEM_HISTORY_DETAIL = new HashSet<PROJECT_ITEM_HISTORY_DETAIL>();
        }
    
        public int id_history { get; set; }
        public int id_item { get; set; }
        public string commentary { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
    
        public virtual PROJECT_ITEM PROJECT_ITEM { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROJECT_ITEM_HISTORY_DETAIL> PROJECT_ITEM_HISTORY_DETAIL { get; set; }
    }
}
