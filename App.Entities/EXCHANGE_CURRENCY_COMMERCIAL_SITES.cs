//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class EXCHANGE_CURRENCY_COMMERCIAL_SITES
    {
        public int id_exchange_currency_commercial { get; set; }
        public string site_code { get; set; }
        public int replication_status { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string program_id { get; set; }
    
        public virtual SITES SITES { get; set; }
    }
}
