//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ORDER_SUPPLIER_DETAIL
    {
        public int order_supplier_id { get; set; }
        public string part_number { get; set; }
        public decimal existence { get; set; }
        public string po_no { get; set; }
        public decimal average_sale { get; set; }
        public decimal system_suggested { get; set; }
        public decimal supplier_suggested { get; set; }
        public decimal authorized { get; set; }
        public string packing { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program { get; set; }
    
        public virtual ITEM ITEM { get; set; }
        public virtual ORDER_SUPPLIER_HEADER ORDER_SUPPLIER_HEADER { get; set; }
    }
}
