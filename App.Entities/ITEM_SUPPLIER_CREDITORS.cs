//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ITEM_SUPPLIER_CREDITORS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ITEM_SUPPLIER_CREDITORS()
        {
            this.INTERNAL_REQUIREMENT_DETAIL = new HashSet<INTERNAL_REQUIREMENT_DETAIL>();
            this.ITEM_CREDITORS_PRICE = new HashSet<ITEM_CREDITORS_PRICE>();
        }
    
        public int supplier_part_number_creditors { get; set; }
        public Nullable<int> supplier_id { get; set; }
        public string part_number_creditors { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
        public Nullable<bool> flag_active { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INTERNAL_REQUIREMENT_DETAIL> INTERNAL_REQUIREMENT_DETAIL { get; set; }
        public virtual ITEM_CREDITORS ITEM_CREDITORS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ITEM_CREDITORS_PRICE> ITEM_CREDITORS_PRICE { get; set; }
        public virtual MA_SUPPLIER MA_SUPPLIER { get; set; }
    }
}
