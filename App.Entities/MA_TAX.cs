//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class MA_TAX
    {
        public string tax_code { get; set; }
        public string name { get; set; }
        public Nullable<decimal> tax_value { get; set; }
        public string tax_type { get; set; }
        public string acc_no { get; set; }
        public string type_stc { get; set; }
        public string factor_type { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
    }
}
