﻿//---------------------------------------------STAR Animacion Inicio Formas
jQuery(document).ready(function () {
    window.setTimeout(function () {//HACE CHICO EL TITULO DE LA PAGINA
        $(".normalheader").addClass("small-header");
        $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        $("#hbreadcrumb").removeClass("m-t-lg");
    }, 5000);
});
//---------------------------------------------END  Animacion Inicio Formas
//---------------------------------------------
//---------------------------------------------STAR validar forma 
$("#form").validate({
    rules: {//REGLAS DE ELEMENTOS
        page_id: {
            required: true,
            minlength: 4
        },
        page_name: {
            required: true,
            minlength: 4
        },
        description: {
            required: true,
            minlength: 4
        },
        url: {
            required: true,
            minlength: 4
        }

    },
    messages: {//MENSAJES DE VALIDACION POR ELEMENTO
        page_id: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        },
        page_name: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres"
        },
        description: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres"
        },
        url: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres"
        }
    },
    submitHandler: function (form) {//SI TODO CORRECTO MANDA EL POST
        StartLoading();
            $.ajax({
                type: "POST",
                url: "@(Url.Action("EditPages", "Configuration"))",
                data: { "page_id": $('#page_id').val(), "page_name": $('#page_name').val(), "description": $('#description').val(), "url": $('#url').val(), "active_flag": $('#active_flag_HasValue').prop("checked") },
            //data: $(form).serialize(),
            success: function (returndate) {//RECIBIMOS RESPUESTA 
                if (returndate.success == true) {
                    toastr.success('' + returndate.responseText + '.');
                    EndLoading();
                }
                else {
                    toastr.warning('' + returndate.responseText + '');
                    EndLoading();
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
            }
    });
return false;
});
//---------------------------------------------END validar forma 
//---------------------------------------------
//---------------------------------------------STAR Ejemplo de Grid 
//---HTML---
<table id="TablePages" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th id="Bu"><button type="button" class="btn btn-xs btn-success " onclick="AddPage();">Agregar</button></th>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Url</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (var item in Model.Page)
{
                        <tr>
                            <td></td>
                            <td>@item.page_id.ToString()</td>
                            <td>@item.page_name.ToString()</td>
                            <td>@item.description.ToString()</td>
                            <td>@item.url</td>
                            <td>@item.active_flag</td>
                        </tr>
}
                </tbody>
</table>
//---HTML---
var table = $('#TablePages').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        //{
        //    text: 'Agregar nuevo', className: 'btn-sm btn-success', init: function (api, node, config) {
        //        $(node).removeClass('btn-default')
        //    }, action: function () { clear(); $('#ModalAddPages').modal('show'); }
        //},
        { extend: 'csv', text: 'Excel', title: 'ExampleFile', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'ExampleFile', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { data: null},
        { data: 'page_id' },
        { data: 'page_name' },
        { data: 'description' },
        { data: 'url' },
        { data: 'active_flag' }
    ],
    columnDefs: [
    {
        targets: [1],
        visible: false,
        searchable: false
    }, {
        targets: [5],
        render: function (data, type, row) {
            if (type === 'display') {
                if (data == true | data == 'True') {
                    return '<div checked><label><input type="checkbox" checked></label></div>'
                }
                else {
                    return '<div><label><input type="checkbox"></label></div>'
                }
            }
            return data;
        }
    }, {
        data: null,
        width: "1%",
        defaultContent: '<button class="btn btn-xs btn-outline btn-danger" type="submit">Editar</button>',
        targets: [0]
    }
    ],
    drawCallback: function (settings) {
        $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
    }
});
//------------------------------------------- END ejemplo de Grid
//-------------------------------------------
//-------------------------------------------STAR ejemplo Modal Insertar o editar
<div class="modal fade" id="ModalAddPages" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="color-line"></div>
<div class="modal-header">
<h4 id="ModalTitle" class="modal-title">Agregar Pagina</h4>
<small id="ModalDescription" class="font-bold">Formulario para registrar una nueva pagina.</small>
</div>
@using (Html.BeginForm("InsertPages", "Configuration", FormMethod.Post, new { name = "form", id = "form" }))
{
<div class="modal-body">

<div class="row">
    <div class="form-group col-lg-6">
        <label for="page_id" title="ID Pagina." data-toggle="tooltip" data-placement="right">ID Pagina</label>
        @Html.TextBoxFor((x => x.page_id), new { @class = "form-control" })
    </div>
    <div class="form-group col-lg-6">
        <label for="page_name" title="Nombre de la página." data-toggle="tooltip" data-placement="right">Nombre de Pagina</label>
        @Html.TextBoxFor((x => x.page_name), new { @class = "form-control" })
    </div>
    <div class="form-group col-lg-6">
        <label title="description" data-toggle="Descripción de la página." data-placement="right">Descripción</label>
        @Html.TextBoxFor((x => x.description), new { @class = "form-control" })
    </div>
    <div class="form-group col-lg-6">
        <label title="URL de la página" data-toggle="tooltip" data-placement="right">URL</label>
        @Html.TextBoxFor((x => x.url), new { @class = "form-control" })
    </div>
    <div class="form-group col-lg-6">
        <label title="Status" data-toggle="tooltip" data-placement="right">Activo/Inactivo</label>
        <div id="Check" class="i-checks">@Html.CheckBoxFor((x => x.active_flag.HasValue), new { @class = "form-control" })</div>
    </div>
</div>

</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
<button class="btn btn-primary">Guardar Cambios</button>
</div>
}
</div>
</div>
</div>

//-------------------------------------------END ejemplo Modal Insertar o editar
//-------------------------------------------
//-------------------------------------------STAR ejemplo pasar valores de Grid a Modal para editar 

$('#TablePages tbody').on('click', 'button', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'page_id':
                $("#page_id").val(value);
                break;
            case 'page_name':
                $("#page_name").val(value);
                break;
            case 'description':
                $("#description").val(value);
                break;
            case 'url':
                $("#url").val(value);
                break;
            case 'active_flag':
                checkBoxStyle(value);
                break;
        }
    });
    $('#page_id').prop('readonly', true);
    $("#ModalTitle").html('Modificar Pagina');
    $("#ModalDescription").html('Formulario para modificar pagina.');
    $("#ModalAddPages").removeClass("hmodal-success").addClass("hmodal-warning");
    edit = true;
    indexEdit = table.row(this).index();
    $('#ModalAddPages').modal('show');
});


function checkBoxStyle(s) {
    if (s == true | s == 'True') {
        $('#Check').children().addClass("checked").length;
        $('#active_flag_HasValue').prop("checked", true);
    }
    else {
        $('#Check').children().removeClass("checked").length;
        $('#active_flag_HasValue').prop("checked", false);
    }
}

//-------------------------------------------END ejemplo pasar valores de Grid a Modal para editar 
//-------------------------------------------
    //-------------------------------------------STAR filtrar List 
    //Textbox para filtrar se envia el ID DE LA LISTA Y EL ID DEL TEXTBOX   (searchLists('rolList,#searchRol');)
    //HTML-
<input id="searchRol" type="text" class="form-control input-xs" placeholder="Buscar Rol..." onkeyup="searchLists('rolList,#searchRol');" />

           <ul id="rolList" class="list-group">
                                        @foreach (var item in Model.RolesCollection)
{
    <li class="list-group-item">
        <p class="radio"><input type="radio" name="Rols" value="@item.role_id.ToString()"> @item.role_name.ToString() </p>
    </li>
    }
</ul>
    //HTML-
    //FUNCION RECIBE ID Y BUSCA POR <P> PARRAFO
function searchLists(s) {
    var input, filter, ul, li, a, i, element;
    element = s.split(",");
    filter = $(element[1]).val().toUpperCase();
    ul = document.getElementById(element[0]);
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("P")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
    input = null, filter = null, ul = null, li = null, a = null, i = null, element = null;
}
//------------------------------------------END STAR filtrar List 
//------------------------------------------------
//------------------------------------------STAR Agregar estilo y propiedades a checkbox  de una lista con el grupo de nombre Rols
        //http://formvalidation.io/examples/icheck/ ejemplo
//ifChecked,ifChanged,ifClicked,ifUnchecked,ifToggled,ifDisabled,ifEnabled

$('#rolList').find('input[name="Rols"]').iCheck({
    radioClass: 'iradio_square-green'
})
          .on('ifChanged', function (e) {
              var field = $(this).attr('name');
          })
.end();

//------------------------------------------END STAR filtrar List 
//------------------------------------------------
//------------------------------------------USUARIO
HttpContext.User.Identity.Name;
//------------------------------------------END USUARIO
    //------------------------------------Accordion List
$('#negotiationList').on('click', 'li', function () {
 $('#negotiationList li div').slideUp();
 $(this).children('div').slideDown();
});
    //----------------------------------------END Accordion List
    //------------------------------------Descargar archivo base 64

function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}
function saveByteArray(reportName, byte) {
    var blob = new Blob([byte]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName + ".pdf";
    link.download = fileName;
    link.click();
};
    //------------------------Valor nulo Html

<input type="hidden" id="BasePriceId" value="">
    //---------------------------------------