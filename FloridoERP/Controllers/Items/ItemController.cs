﻿using App.BLL;
using App.BLL.Barcodes;
using App.BLL.ChangePrice;
using App.BLL.CodesPackages;
using App.BLL.CommonBusiness;
using App.BLL.ExchangeCurrency;
using App.BLL.Item;
using App.BLL.ItemPresentationBusiness;
using App.BLL.MaCode;
using App.BLL.MaEmail;
using App.BLL.Purchases;
using App.BLL.SAT;
using App.Common;
using App.DAL.ChangePrice;
using App.DAL.Purchases.PurchasesModels;
using App.Entities;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Supplier;
using FloridoERP.ViewModels.Purchases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Item
{
    public class ItemController : Controller
    {
        private readonly ItemMasterBusiness _itemBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly ItemSupplierBusiness _ItemSupplierBusiness;
        private readonly ItemMasterRepository _itemMasterRepo;
        private readonly ItemSpecsBusiness _ItemSpecsBusiness;
        private readonly SysMaClassBusiness _MaClassBussines;
        private readonly SalesPriceChangeBusiness _SalesPriceChangeBusiness;
        private readonly productCatalogueSatBusiness _productCatalogueSatBusiness;
        private readonly SysMaTaxBusiness _MaTaxBusiness;
        private readonly BarcodesBusiness _barcodesBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly ItemOldErpBusiness _ItemOldErpBusiness;
        private readonly ItemPresentationBusiness _itemPresentationBusines;
        private readonly CodesPackagesBusiness _codesPackagesBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;
        private readonly SendMail sendMail;
        private readonly Common common;
        private readonly MaEmailBusiness _maEmailBusiness;
        private readonly ExchangeCurrencyBusiness _exchangeCurrencyBusiness;

        public ItemController()
        {
            _supplierBusiness = new SupplierBusiness();
            _itemBusiness = new ItemMasterBusiness();
            _ItemSupplierBusiness = new ItemSupplierBusiness();
            _itemMasterRepo = new ItemMasterRepository();
            _MaClassBussines = new SysMaClassBusiness();
            _SalesPriceChangeBusiness = new SalesPriceChangeBusiness();
            _productCatalogueSatBusiness = new productCatalogueSatBusiness();
            _ItemSpecsBusiness = new ItemSpecsBusiness();
            _MaTaxBusiness = new SysMaTaxBusiness();
            _barcodesBusiness = new BarcodesBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _ItemOldErpBusiness = new ItemOldErpBusiness();
            _itemPresentationBusines = new ItemPresentationBusiness();
            _codesPackagesBusiness = new CodesPackagesBusiness();
            _maCodeBusiness = new MaCodeBusiness();
            sendMail = new SendMail();
            common = new Common();
            _maEmailBusiness = new MaEmailBusiness();
            _exchangeCurrencyBusiness = new ExchangeCurrencyBusiness();
        }

        public ActionResult ActionSearchItem(string Filter)
        {
            return new JsonResult() { Data = new { Json = _itemBusiness.SerchItem(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionSearchItemBarcode(string Filter)
        {
            return new JsonResult() { Data = new { Json = _itemBusiness.SerchItemBarcode(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionSearchItemBarcodeSupplier(string Filter, int Supplier)
        {
            return new JsonResult() { Data = new { Json = _itemBusiness.SearchItemBarcodeSupplier(Filter, Supplier) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionSearchBarcode(string partNumber)
        {
            return new JsonResult() { Data = _itemBusiness.SearchBarcode(partNumber), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionSaveBarcode(string partNumber, string barcode)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _itemBusiness.SaveBarcode(partNumber, barcode, User.Identity.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionInsertItem(PURCH004ViewModel viewModel)
        {
            viewModel.cuser = HttpContext.User.Identity.Name;
            if (_ItemOldErpBusiness.AddItemTemp(viewModel.ItemWMS, viewModel.Item)) {
                // EJECUTA SP. (REVISAR PROVEEDOR)
                _ItemOldErpBusiness.Sp_Insert_New_Produ(viewModel.part_number, viewModel.cuser);
                return Json(new { success = true, id = viewModel.ItemWMS.product_id, name = viewModel.ItemWMS.product_name }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, id = viewModel.ItemWMS.product_id, name = viewModel.ItemWMS.product_name }, JsonRequestBehavior.AllowGet);

            // Correr SP para aprobar productos.

            //Inserta Directo sin autorizacion 
            //if (Session["User"] != null)
            //{
            //    viewModel.cuser = HttpContext.User.Identity.Name;
            //    viewModel.cdate = DateTime.Now;
            //    if (ModelState.IsValid)
            //    {
            //        var Item = viewModel.Item;

            //        if (_itemBusiness.AddItemMaster(Item))
            //        {
            //            BARCODES barcode = new BARCODES();
            //            barcode.part_barcode = viewModel.part_number;
            //            barcode.part_number = viewModel.part_number;
            //            barcode.cuser = HttpContext.User.Identity.Name;
            //            barcode.cdate = DateTime.Now;

            //            ITEM_SPECS item_spec = new ITEM_SPECS
            //            {
            //                part_number = viewModel.part_number,
            //                gr_storage_location = "SL01"
            //            };
            //            if (_ItemSpecsBusiness.AddItemSpecs(item_spec))
            //            {
            //                if (_barcodesBusiness.AddBarcode(barcode) == 1)
            //                {
            //                    if (viewModel.combo_flag)
            //                    {
            //                        viewModel.combo_status = "1";
            //                        if (_ItemComboBusiness.AddComboHeader(viewModel.comboHeader))
            //                            return Json(new { success = true, id = Item.part_number, name = Item.part_description }, JsonRequestBehavior.AllowGet);
            //                        else
            //                            return Json(new { success = false, id = Item.part_number, name = Item.part_description }, JsonRequestBehavior.AllowGet);
            //                    }
            //                    return Json(new { success = true, id = Item.part_number, name = Item.part_description }, JsonRequestBehavior.AllowGet);
            //                }
            //                else
            //                {
            //                    return Json(new { success = false, message = "Error al agregar producto" }, JsonRequestBehavior.AllowGet);
            //                }
            //            }
            //            else
            //                return Json(new { success = false, message = "Error al agregar producto" }, JsonRequestBehavior.AllowGet);
            //        }
            //        else
            //        {
            //            return Json(new { success = false, message = "Error al agregar producto" }, JsonRequestBehavior.AllowGet);
            //        }
            //    }
            //    else
            //    {
            //        IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
            //        return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
            //    }
            //}
            //else
            //{
            //    return Json(new { success = false, message = "Tu sesion termino." }, JsonRequestBehavior.AllowGet);
            //}
        }

        // se insertan specs en tabla 
        [HttpPost]
        public ActionResult ActionInsertItemSpecs(PURCH004ViewModel viewModel)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var Item = viewModel.itemSpecs;
                    if (_ItemSpecsBusiness.AddItemSpecs(Item))
                        return Json(new { success = true, message = "Registro exitoso." }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = false, message = "Error al registrar" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, message = "Tu session termino" }, JsonRequestBehavior.AllowGet);
        }

        // se Editan specs en tabla 
        [HttpPost]
        public ActionResult ActionEditItemSpecs(PURCH004ViewModel viewModel)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var Item = viewModel.itemSpecs;
                    if (_ItemSpecsBusiness.UpdateItemSpecs(Item))
                        return Json(new { success = true, message = "Registro exitoso." }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = false, message = "Error al registrar" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, message = "Tu session termino" }, JsonRequestBehavior.AllowGet);
        }

        //Obtener Producto por Numero de Parte o informacion relacionada a ese producto como precio, proveedores, etc
        [HttpGet]
        public ActionResult ActionGetItemById(string partNumber, string parameter)
        {
            if (Session["User"] != null)
            {
                if (parameter == null)
                {
                    if (_barcodesBusiness.BarcodeExist(partNumber))
                        return Json(new { BarcodeExist = true }, JsonRequestBehavior.AllowGet);
                }

                var itemFromDatabase = _itemBusiness.GetItemByPartNumber(partNumber);

                if (itemFromDatabase == null)
                    return Json(new { success = true, purch005 = (string)null }, JsonRequestBehavior.AllowGet);

                var purch005 = new PURCH005ViewModel2()
                {
                    buyer_division = itemFromDatabase.buyer_division,
                    part_iva = parameter == "GetToEdit" ? itemFromDatabase.part_iva_purchase : _MaTaxBusiness.GetMaTaxByCode(itemFromDatabase.part_iva_purchase).tax_value.ToString(),
                    part_iva_sale = parameter == "GetToEdit" ? itemFromDatabase.part_iva_sale : itemFromDatabase.part_iva_sale == null ? ""
                    : itemFromDatabase.part_iva_sale != null ? _MaTaxBusiness.GetMaTaxByCode(itemFromDatabase.part_iva_sale).tax_value.ToString()
                    : "",
                    part_ieps = parameter == "GetToEdit" ? itemFromDatabase.part_ieps : _MaTaxBusiness.GetMaTaxByCode(itemFromDatabase.part_ieps).tax_value.ToString(), //itemFromDatabase.part_ieps,
                    part_name = itemFromDatabase.part_description,
                    part_number = itemFromDatabase.part_number,
                    part_type = itemFromDatabase.part_type,
                    part_number_sat = itemFromDatabase.part_number_sat,
                    unit_sat = itemFromDatabase.unit_sat != null ? itemFromDatabase.unit_sat.Trim() : null,
                    unit = itemFromDatabase.unit_size,
                    cuser = itemFromDatabase.cuser,
                    cdate = itemFromDatabase.cdate != null ? itemFromDatabase.cdate.Value.Date.ToString("dd/MM/yyyy") : "",
                    udate = itemFromDatabase.udate != null ? itemFromDatabase.udate.Value.Date.ToString("dd/MM/yyyy") : "",
                    weight_flag = itemFromDatabase.weight_flag.Value.ToString(),
                    active_flag = itemFromDatabase.active_flag.Value.ToString(),
                    extra_items_flag = itemFromDatabase.extra_items_flag != null ? (bool)itemFromDatabase.extra_items_flag : false,
                    combo_flag = itemFromDatabase.combo_flag != null ? (bool)itemFromDatabase.combo_flag : false,
                    table_price = parameter != "GetToEdit" ? ListPrices(_supplierBusiness.GetPURCHASEBASEPRICE(partNumber)) : null,
                    table_supplier = parameter != "GetToEdit" ? ListMaSupplier(partNumber) : null,
                    table_Drop = parameter != "GetToEdit" ? ListDrop2(partNumber) : null,
                    level_header = itemFromDatabase.level_header != null ? (int)itemFromDatabase.level_header : 0,
                    level1_code = itemFromDatabase.level1_code != null ? (int)itemFromDatabase.level1_code : 0,
                    level2_code = itemFromDatabase.level2_code != null ? (int)itemFromDatabase.level2_code : 0,
                    level3_code = itemFromDatabase.level3_code != null ? (int)itemFromDatabase.level3_code : 0,
                    isForced90 = _maCodeBusiness.ValidateItemForce90(partNumber),
                    expiration_days = itemFromDatabase.expiration_days ?? 0,
                    exchange_price = _exchangeCurrencyBusiness.GetLastExchangeCurrent(),
                };

                if (purch005.cuser == "" || purch005.cuser == "System")
                    purch005.cuser = "Por sistema.";
                else
                    purch005.cuser = _userMasterBusiness.getFirstLastName(purch005.cuser);

                List<MA_CLASS> Levels = new List<MA_CLASS>();
                var Level2 = Levels;
                var Level3 = Levels;
                var Level4 = Levels;
                if (parameter == "GetToEdit")
                {
                    Level2 = _MaClassBussines.GetMaClassDivision((int)itemFromDatabase.level_header, 2);
                    Level3 = _MaClassBussines.GetMaClassDivision((int)itemFromDatabase.level1_code, 3);
                    Level4 = _MaClassBussines.GetMaClassDivision(itemFromDatabase.level2_code != null ? (int)itemFromDatabase.level2_code : 0, 4);
                }
                var itemSpecs = _ItemSpecsBusiness.getSpecsByPartNumber(partNumber);
                var itemPrice = _SalesPriceChangeBusiness.GetSalePriceByPartNumber(partNumber);
                var itemValidateCodes = _codesPackagesBusiness.ValidateCodePackage(partNumber);
                var BlockedSites = _itemBusiness.GetAllStoresItemBlocked(partNumber);

                return new JsonResult() { Data = new { success = true, purch005, JsonLevel2 = Level2, JsonLevel3 = Level3, JsonLevel4 = Level4, JsonSpecs = itemSpecs, JsonPrice = itemPrice, JsonCodes = itemValidateCodes, JsonBlocked = BlockedSites }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return Json(new { success = false, message = "Tu session termino" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetAllSATGrupo(string cve_division)
        {
            return Json(new { success = true, Json = ListSATGrupo(cve_division) }, JsonRequestBehavior.AllowGet);
        }

        public List<SATGrupo> SATGrupo { get; set; }

        public List<SelectListItem> ListSATGrupo(string valueFilter)
        {
            SATGrupo = _productCatalogueSatBusiness.GetAllSATGrupo(valueFilter);
            List<SelectListItem> table_Drop = new List<SelectListItem>();
            table_Drop.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in SATGrupo)
                table_Drop.Add(new SelectListItem { Text = "(" + n.cve_grupo + ") " + n.grupo, Value = n.cve_grupo });

            return table_Drop;
        }

        [HttpGet]
        public ActionResult ActionGetAllSATClase(string cve_grupo)
        {
            return Json(new { success = true, Json = ListSATClase(cve_grupo) }, JsonRequestBehavior.AllowGet);
        }

        public List<SATClase> SATClase { get; set; }

        public List<SelectListItem> ListSATClase(string valueFilter)
        {
            SATClase = _productCatalogueSatBusiness.GetAllSATClase(valueFilter);
            List<SelectListItem> table_Drop = new List<SelectListItem>();
            table_Drop.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in SATClase)
                table_Drop.Add(new SelectListItem { Text = "(" + n.cve_clase + ") " + n.clase, Value = n.cve_clase });

            return table_Drop;
        }

        public List<MA_SUPPLIER> SupplierList { get; set; }

        public List<SelectListItem> ListDrop(string partNumber)
        {
            SupplierList = _supplierBusiness.GetAvailableSuppliersByProduct2(partNumber);
            List<SelectListItem> table_Drop = new List<SelectListItem>();
            foreach (var n in SupplierList)
            {
                table_Drop.Add(new SelectListItem
                {
                    Text = n.business_name,
                    Value = n.supplier_id.ToString()
                });
            }
            return table_Drop;
        }

        public List<SelectListItem> ListDrop2(string partNumber)
        {
            SupplierList = _supplierBusiness.GetAvailableSuppliersByProduct2(partNumber);
            List<SelectListItem> table_Drop = new List<SelectListItem>();
            foreach (var n in SupplierList)
            {
                table_Drop.Add(new SelectListItem
                {
                    Text = n.business_name,
                    Value = n.supplier_id.ToString()
                });
            }
            return table_Drop;
        }

        public List<SupplierNotActiveModel> ListMaSupplier(string partNumber)
        {
            //List<MaSupplierViewModel> table_supplier = new List<MaSupplierViewModel>();
            var list = _supplierBusiness.GetSuppliersByProduct(partNumber);
            //foreach (var n in list)
            //{
            //    var Ma = new MaSupplierViewModel()
            //    {
            //        supplier_id = n.supplier_id > 0 ? n.supplier_id.ToString() : "",
            //        city = n.city != "" ? n.city : "",
            //        accounting_account = n.accounting_account != "" ? n.accounting_account : "",
            //        business_name = n.business_name != "" ? n.business_name : "",
            //        commercial_name = n.commercial_name != "" ? n.commercial_name : "",
            //        country = n.country != "" ? n.country : "",
            //        rfc = n.rfc != "" ? n.rfc : "",
            //        supplier_state = n.supplier_state != "" ? n.supplier_state : "",
            //        supplier_address = n.supplier_address != "" ? n.supplier_address : "",
            //        supplier_type = n.supplier_type != "" ? n.supplier_type : "",
            //        //zip_code = n.zip_code != "" ? n.zip_code : "",
            //        //cdate = n.cdate.ToString(),
            //        //cuser = n.cuser != "" ? n.cuser : "",
            //        //program = n.program != "" ? n.program : "",
            //        //udate = n.udate.ToString(),
            //    };
            //    table_supplier.Add(Ma);
            //}
            //return table_supplier;
            return list;
        }

        public List<PurchasePriceViewModel> ListPrices(List<PurchasesPriceModel> Purchase)
        {
            var list = new List<PurchasePriceViewModel>();
            foreach (var item in Purchase)
            {
                var purchasePrice = new PurchasePriceViewModel()
                {
                    base_price = item.base_price,
                    base_price_id = item.base_price_id,
                    business_name = item.business_name,
                    currency = item.currency,
                    part_name = item.part_name,
                    profit_margin = item.profit_margin,
                    sales_price = item.sales_price,
                    supplier_part_number = item.supplier_part_number,
                    valid_from = item.valid_from,
                    valid_to = item.valid_to
                };
                list.Add(purchasePrice);
            }
            return list;
        }

        public List<PurchaseBasePriceModel> SuppliersPriceByProduct { get; set; }

        public string GetPricesOfSuppliersByProduct(string partNumber)
        {
            SuppliersPriceByProduct = _supplierBusiness.GetPricesOfSuppliersByProduct(partNumber);
            var jsonTablePrices = JsonConvert.SerializeObject(SuppliersPriceByProduct, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return jsonTablePrices;
        }

        public string GetSuppliersByProduct(string partNumber)
        {
            var list = _supplierBusiness.GetSuppliersByProduct(partNumber);
            var jsonTable = JsonConvert.SerializeObject(list, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return jsonTable;
        }

        public string GetAvailableSuppliersByProduct(string partNumber)
        {
            SupplierList = _supplierBusiness.GetAvailableSuppliersByProduct2(partNumber);
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var n in SupplierList)
                items.Add(new SelectListItem { Text = n.business_name, Value = n.supplier_id.ToString() });

            var jsonDrop = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return jsonDrop;
        }

        [HttpPost]
        public ActionResult ActionUpdateItem(PURCH004ViewModel item)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    if (_itemBusiness.UpdateItemMaster(item.Item, HttpContext.User.Identity.Name))
                    {
                        var items = _itemBusiness.GetAllItems();
                        List<ItemViewModel> itemlist = new List<ItemViewModel>();
                        foreach (var itm in items)
                        {
                            var itemN = new ItemViewModel()
                            {
                                active_flag = itm.active_flag,
                                buyer_division = itm.buyer_division,
                                part_iva = itm.part_iva_purchase,
                                part_iva_sale = itm.part_iva_sale,
                                part_ieps = itm.part_ieps,
                                part_description = itm.part_description,
                                part_number = itm.part_number,
                                part_number_sat = itm.part_number_sat,
                                part_type = itm.part_type,
                                unit = itm.unit_size,
                                unit_sat = itm.unit_sat,
                                weight_flag = itm.weight_flag,
                                combo_flag = itm.combo_flag,
                                extra_items_flag = itm.extra_items_flag == null ? false : itm.extra_items_flag.Value
                            };
                            itemlist.Add(itemN);
                        }
                        return new JsonResult() { Data = new { success = true, itemlist, item.part_number, item.part_description }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue }; //Json(new { success = true, itemlist, item.part_number, item.part_description }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "Error al agregar producto" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, message = "Tu sesion termino." }, JsonRequestBehavior.AllowGet);
        }

        //El mismo de arriba pero tiene todos los productos
        [HttpPost]
        public ActionResult ActionUpdateItemPURCH004(PURCH004ViewModel item)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    if (_itemBusiness.UpdateItemMaster(item.Item, HttpContext.User.Identity.Name))
                    {
                        var items = _itemBusiness.GetAllItemsAll();
                        List<ItemViewModel> itemlist = new List<ItemViewModel>();
                        foreach (var itm in items)
                        {
                            var itemN = new ItemViewModel()
                            {
                                active_flag = itm.active_flag,
                                buyer_division = itm.buyer_division,
                                part_iva = itm.part_iva_purchase,
                                part_iva_sale = itm.part_iva_sale,
                                part_ieps = itm.part_ieps,
                                part_description = itm.part_description,
                                part_number = itm.part_number,
                                part_number_sat = itm.part_number_sat,
                                part_type = itm.part_type,
                                unit = itm.unit_size,
                                unit_sat = itm.unit_sat,
                                weight_flag = itm.weight_flag,
                                combo_flag = itm.combo_flag,
                                extra_items_flag = itm.extra_items_flag == null ? false : itm.extra_items_flag.Value
                            };
                            itemlist.Add(itemN);
                        }
                        return new JsonResult() { Data = new { success = true, itemlist, item.part_number, item.part_description }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue }; //Json(new { success = true, itemlist, item.part_number, item.part_description }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "Error al agregar producto" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, message = "Tu sesion termino." }, JsonRequestBehavior.AllowGet);
        }

        //Asigna proveedores a productos
        [HttpPost]
        public ActionResult ActionAssingSupplier(PURCH005ViewModel AssingSup)
        {
            if (Session["User"] != null)
            {
                AssingSup.cuser = HttpContext.User.Identity.Name;
                AssingSup.cdate = DateTime.Now;
                if (ModelState.IsValid)
                {
                    var inserted = _ItemSupplierBusiness.AssignSupplierToProduct(AssingSup.ITEM_SUPPLIER);
                    if (inserted > 0)
                    {
                        var jsonTable = GetSuppliersByProduct(AssingSup.part_number);
                        var jsonDrop = GetAvailableSuppliersByProduct(AssingSup.part_number);
                        var jsonTablePrices = GetPricesOfSuppliersByProduct(AssingSup.part_number);
                        var jsonPrices = ListPrices(_supplierBusiness.GetPURCHASEBASEPRICE(AssingSup.part_number));
                        return Json(new { success = true, JsonPrices = jsonPrices, JsonTable = jsonTable, JsonDrop = jsonDrop, JsonTablePrices = jsonTablePrices, responseText = "Registro Exitoso!", supplier_part_number = inserted }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Se actualiza precio de producto por proveedor 
        [HttpPost]
        public ActionResult ActionUpdateBasePrice(string part_number, int BasePriceId, int SupplierPartNumberId, decimal BasePrice, string Currency, DateTime ValidFrom, string program_id)
        {
            if (Session["User"] != null)
            {
                string User = HttpContext.User.Identity.Name;
                if (ModelState.IsValid)
                {
                    int Result = _supplierBusiness.sp_Purchases_UpdateBasePrice(BasePriceId, SupplierPartNumberId, BasePrice, Currency, ValidFrom, User, program_id);
                    if (Result >= 1)
                    {
                        var jsonTablePrices = GetPricesOfSuppliersByProduct(part_number);
                        var jsonDropBasePrices = ListPrices(_supplierBusiness.GetPURCHASEBASEPRICE(part_number));
                        var jsonDrop = GetAvailableSuppliersByProduct(part_number);
                        var jsonTable = GetSuppliersByProduct(part_number);
                        var exchange = _exchangeCurrencyBusiness.GetLastExchangeCurrent();
                        return Json(new { success = true, JsonDrop = jsonDrop, JsonTable = jsonTable, JsonTablePrices = jsonTablePrices, responseText = "Registro Exitoso!", jsonDropBasePrices = jsonDropBasePrices, exchange_price = exchange }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateCostBySupplier(List<ItemSupplierModel> items, string program_id)
        {
            if (Session["User"] != null)
            {
                string User = HttpContext.User.Identity.Name;
                for (int i = 0; i < items.Count; i++)
                {
                    try
                    {
                        decimal base_cost = 0;
                        var tipo = items[i].type;
                        if (tipo == "%")
                            base_cost = Decimal.Parse(items[i].item_cost) + (Decimal.Parse(items[i].item_cost) * (items[i].quantity / 100));
                        else if (tipo == "NO")
                            base_cost = items[i].quantity;
                        else
                            base_cost = Decimal.Parse(items[i].item_cost) + items[i].quantity;

                        _supplierBusiness.sp_Purchases_UpdateBasePrice(Int32.Parse(items[i].item_base_cost_id), Int32.Parse(items[i].item_supplier_id), base_cost, items[i].item_currency, items[i].fecha, User, program_id);
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, responseText = e }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = true, responseText = "Cambios Aplicados Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveChangePriceStores(List<SalesPriceChangeModel> itemChangePrice, string ProgramId)
        {
            var result = 0;
            if (Session["User"] != null)
            {
                var Sesion = Session["User"].ToString();
                var dateNow = DateTime.Now;

                //var ProgramId = "PURCH012.cshtml";
                foreach (var InformationProduct in itemChangePrice)
                {
                    if (dateNow.Date == InformationProduct.appliedDate.Date)
                    {
                        InformationProduct.priceChangeNow = true;
                    }
                    result = _SalesPriceChangeBusiness.addItemSalesPriceChange(InformationProduct.partNumber, InformationProduct.appliedDate, InformationProduct.margin, InformationProduct.priceUser, Sesion, ProgramId);
                    if (result == 0)
                        return Json(new { success = false, responseText = "Error al colocar el precio, Contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }

                var ChangePriceToday = itemChangePrice.Where(w => w.priceChangeNow == true).ToList();
                if (ChangePriceToday.Count() > 0)
                {
                    bool errorEmail = true;
                    var emailUser = _userMasterBusiness.GetEmailByUser(Sesion);
                    var emailGerencia = _maEmailBusiness.GetEmailGenericList("gerencia");
                    var emailMesaControl = _maEmailBusiness.GetEmailGenericList("mesa");
                    var emailCajaGral = _maEmailBusiness.GetEmailGenericList("caja");
                    var tableEmail = _itemMasterRepo.GeneraTableByEmail(ChangePriceToday);
                    var EmailSystem = _maCodeBusiness.GetEmailsPriceChange();
                    var email = emailMesaControl + "," + emailCajaGral + "," + (!string.IsNullOrWhiteSpace(EmailSystem) ? EmailSystem + "," : "") + (!string.IsNullOrWhiteSpace(emailUser) ? emailUser : "");
                    var email2 = emailGerencia + "," + (!string.IsNullOrWhiteSpace(EmailSystem) ? EmailSystem + "," : "") + (!string.IsNullOrWhiteSpace(emailUser) ? emailUser : "");

                    sendMail.subject = "CAMBIOS DE PRECIO DEL DÍA DE HOY (" + dateNow.Date.ToString("dd/MM/yyyy") + "): ACTUALIZAR PUNTOS DE VENTA";
                    sendMail.from = "EL FLORIDO LABORATORIOS GLOBAL";
                    sendMail.email = email;
                    var resultEmail = common.MailMessageChangePrice(sendMail, tableEmail);
                    sendMail.email = email2;
                    if (resultEmail != "success")
                        errorEmail = false;
                    var resultEmail2 = common.MailMessageChangePrice(sendMail, tableEmail);
                    if (resultEmail2 != "success")
                        errorEmail = false;
                    if (errorEmail)
                        return Json(new { success = true, responseText = "Se ha Guardado exitosamente, se aviso a sucursales." }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = false, responseText = "Se ha Guardado exitosamente, pero ocurrio un error al mandar el correo." }, JsonRequestBehavior.AllowGet);
                }


                return Json(new { success = true, responseText = "Se ha Guardado exitosamente" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Tu sesion termino." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionItems()
        {
            return Json(_itemMasterRepo.GetAllItems2(), JsonRequestBehavior.AllowGet);
        }

        //traer usuarios y productos
        [HttpPost]
        public ActionResult ActionFilterSupplierAndProduct(int? Supplier, string Item)
        {
            if (Session["User"] != null)
            {
                if (Supplier != null)
                    return Json(_ItemSupplierBusiness.getSupplierItem(Supplier.Value), JsonRequestBehavior.AllowGet);
                if (Item.Trim() != "")
                    return Json(_ItemSupplierBusiness.getItemSupplier(Item), JsonRequestBehavior.AllowGet);
                else
                    return Json("", JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        //traer tabla purchaseBasePrice
        public ActionResult ActionListPurchaseBasePrice(int Supplier, int MaClass, string PartNumber)
        {
            if (Session["User"] != null)
            {
                //return Json(_supplierBusiness.GetPricesOfSuppliersByProducts(PartNumber))
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPriceProduct(string part_number)
        {
            var item = _itemMasterRepo.GetItemPriceByPartNumber(part_number);
            if (item == null)
                return Json(new { success = false, responseText = "" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, responseText = item.price }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemDescription(string part_number)
        {
            var item = _itemMasterRepo.GetItemByPartNumber(part_number);

            if (item == null) return Json(new { success = false, error = $"No se encontro producto" }, JsonRequestBehavior.AllowGet);
            return Json(new { success = true, item.part_description }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemPriceIvaIeps(string part_number)
        {
            if (Session["User"] != null)
            {
                var item = _itemMasterRepo.GetItemPriceByPartNumber(part_number);
                var itemIvaIeps = _MaTaxBusiness.GetIvaIepsByIdValue(part_number);
                ItemCommon itemC = new ItemCommon();
                itemC.Ieps = itemIvaIeps.Ieps;
                itemC.Iva = itemIvaIeps.Iva;
                if (item != null)
                    itemC.weight_flag = item.weight_flag;
                else
                    itemC.weight_flag = false;
                if (item == null)
                {
                    var itemPresentation = _itemPresentationBusines.GetInformationBasic(part_number);
                    if (itemPresentation != null)
                    {
                        itemC.Price = itemPresentation.TotalPrice.ToString();
                        itemC.Presentation = true;
                        itemC.presentation_id = itemPresentation.PresentationId;
                        itemC.forced_90 = _maCodeBusiness.ValidateItemForce90(itemPresentation.PartNumber);
                        return Json(new { success = true, itemC }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        itemC.Presentation = false;
                        itemC.Price = "0";
                        itemC.presentation_id = 0;
                        itemC.forced_90 = _maCodeBusiness.ValidateItemForce90(part_number);
                        return Json(new { success = true, itemC }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    itemC.Price = item.price;
                    itemC.Presentation = false;
                    itemC.presentation_id = 0;
                    itemC.forced_90 = _maCodeBusiness.ValidateItemForce90(part_number);
                    return Json(new { success = true, itemC }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemIvaIepsValue(string part_number)
        {
            if (Session["User"] == null) return Json("SF", JsonRequestBehavior.AllowGet);
            var data = _itemBusiness.GetItemIvaIepsValue(part_number);
            if (data == null) return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            data.Ieps = data.Ieps / 100;
            data.Iva = data.Iva / 100;
            return Json(new { success = true, Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemIvaForIeps(string PartNumber)
        {
            if (Session["User"] != null)
                return Json(_itemBusiness.GetItemIvaForIeps(PartNumber), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFlagTrue()
        {
            if (Session["User"] != null)
                return Json(_itemBusiness.GetItemsTrueFlag(), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFlagFalse()
        {
            if (Session["User"] != null)
                return Json(_itemBusiness.GetItemsFalseFlag(), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItems()
        {
            var viewModel = new PURCH004ViewModel();
            return new JsonResult() { Data = new { Model = viewModel.itemlist }, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public ActionResult ActionGetOldERPSuppliersLike(string Filter)
        {
            return Json(new { success = true, Json = _ItemOldErpBusiness.getOLDERPSupplier(Filter) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetERPSuppliersLike(string Filter)
        {
            return Json(new { success = true, Json = _ItemOldErpBusiness.getERPSupplier(Filter) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetItemPresentationLike(string Filter, int status)
        {
            return Json(new { success = true, Json = _itemMasterRepo.getItemsByLike(Filter, status) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetAllSATCatalogoProdServ(string Filter)
        {
            return Json(new { success = true, Json = _productCatalogueSatBusiness.GetAllSATCatalogoProdServ(Filter) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetAllSATum(string Filter)
        {
            return Json(new { success = true, Json = _productCatalogueSatBusiness.GetAllSATum(Filter) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetAllSATumById(string Filter)
        {
            return Json(new { success = true, Json = _productCatalogueSatBusiness.GetAllSATumById(Filter) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetFamili(string Filter)
        {
            return Json(new { success = true, Json = _ItemOldErpBusiness.getOLDERPFamili(Filter) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionSearchItemLike(string Filter)
        {
            if (Session["User"] != null)
                return Json(_itemBusiness.SearchItemLike(Filter), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActionItemBlocked(string Sites, string Part_number)
        {
            var retu = _itemBusiness.AddItemBlocked(Part_number, Sites, HttpContext.User.Identity.Name);
            return Json(new { success = true, Json = "Nopuedo papi" }, JsonRequestBehavior.AllowGet);
        }
    }
}
