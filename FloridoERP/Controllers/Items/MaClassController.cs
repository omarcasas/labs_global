﻿using App.BLL.CommonBusiness;
using FloridoERP.ViewModels.Purchases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Items
{
    public class MaClassController : Controller
    {
        private readonly SysMaClassBusiness _MaClassBussines;

        public MaClassController()
        {
            _MaClassBussines = new SysMaClassBusiness();
        }

        [HttpGet]
        public ActionResult ActionGetMaClassById(int parent, int level)
        {
            var modelObj = new PURCH004ViewModel();
            var itemFromDatabase = _MaClassBussines.GetMaClassDivision(parent, level);
            var itemsLsitDrop = modelObj.FillSelectListItemMaClass(level);
            if (itemFromDatabase != null)
                return Json(new { success = true, Json = itemFromDatabase, JsonDrop = itemsLsitDrop }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "error al cargar lista!!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddMaClass(string ClassName, string Description, int parent, int level)
        {
            var modelObj = new PURCH004ViewModel
            {
                cuser = HttpContext.User.Identity.Name,
                MaClassclass_name = ClassName.ToUpper().Trim(),
                MaClassDescription = Description.ToUpper().Trim(),
                MaClassLevelCategory = level,
                MaClassParentClass = parent
            };
            if (ModelState.IsValid)
            {
                var MaClass = modelObj.MA_CLASS;
                if (_MaClassBussines.AddMaClass(MaClass))
                {
                    var itemFromDatabase = _MaClassBussines.GetMaClassDivision(0, 1);

                    if (itemFromDatabase != null)
                        return Json(new { success = true, Json = itemFromDatabase }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = false, responseText = "error al cargar lista!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Error al agregar producto" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        /*busqueda por producto*/
        [HttpPost]
        public ActionResult ActionGetItemMaClass(string PartNumber)
        {
            return Json(_MaClassBussines.GetItemMaClass(PartNumber));
        }

        [HttpPost]
        public ActionResult ActionGetItemMaClass2(string PartNumber)
        {
            return Json(_MaClassBussines.GetItemMaClass2(PartNumber));
        }

        /*busqueda por categoria*/
        [HttpPost]
        public ActionResult ActionGetMaClassPrimary(int MaClassId)
        {
            return Json(_MaClassBussines.GetMaClassPrimary(MaClassId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetMaClassSecundary(int MaClassId)
        {
            return Json(_MaClassBussines.GetMaClassSecundary(MaClassId), JsonRequestBehavior.AllowGet);
        }

        /*filtrar producto mediante categoria*/
        [HttpPost]
        public ActionResult ActionGetProductMaClass(int MaClassId)
        {
            return Json(_MaClassBussines.GetProductMaClass(MaClassId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetProductMaClass2(int MaClassId)
        {
            return Json(_MaClassBussines.GetProductMaClass2(MaClassId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetFamily(string deptoo)
        {
            int level2 = Int32.Parse(deptoo);
            var levelsGet = _MaClassBussines.GetAllFamilyByParentClass(level2);
            if (levelsGet.Count != 0)
            {
                return Json(new { success = true, Json = levelsGet }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}