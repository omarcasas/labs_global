﻿using App.BLL.CommonBusiness;
using App.BLL.Item;
using App.DAL.Purchases;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Items
{
    public class ItemComboController : Controller
    {
        // GET: ItemCombo
        private readonly ItemMasterRepository _itemMasterRepo;
        private readonly ItemCombosBusiness _ItemComboBusiness;
        private readonly SysMaTaxBusiness _MaTaxBusiness;
        private readonly PurchaseBasePriceRespository _PurchaseBasePriceRespository;

        public ItemComboController()
        {
            _itemMasterRepo = new ItemMasterRepository();
            _ItemComboBusiness = new ItemCombosBusiness();
            _MaTaxBusiness = new SysMaTaxBusiness();
            _PurchaseBasePriceRespository = new PurchaseBasePriceRespository();
        }

        [HttpGet]
        public ActionResult ActionGetProductCombo(string PartNumberCombo)
        {
            if (Session["User"] != null)
            {
                var itemIvaIeps = _MaTaxBusiness.GetIvaIepsByIdValue(PartNumberCombo);
                ItemCommon itemC = new ItemCommon();
                itemC.Ieps = itemIvaIeps.Ieps / 100;
                itemC.Iva = itemIvaIeps.Iva / 100;

                var baseCostCombo = _ItemComboBusiness.getCombosBaseCostByPartNumber(PartNumberCombo).base_cost;
                var comboDetailList = _ItemComboBusiness.getCombosDetailByPartNumberCombo(PartNumberCombo);
                var comboHeaderInf = _itemMasterRepo.GetItemByPartNum(PartNumberCombo);
                if (comboHeaderInf != null)
                {
                    return Json(new { success = true, DetailList = comboDetailList, headerCombo = comboHeaderInf, JsonIVA = itemC.Iva, JsonIEPS = itemC.Ieps, JsonBaseCost = baseCostCombo }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult ActionGetProduct(string PartNumber)
        {
            if (Session["User"] != null)
            {
                var Product = _itemMasterRepo.GetItemByPartNum(PartNumber);
                var itemIvaIeps = _MaTaxBusiness.GetIvaIepsByIdValue(PartNumber);
                var baseCostCombo = _PurchaseBasePriceRespository.GetCostProductDesc(PartNumber);
                ItemCommon itemC = new ItemCommon();
                itemC.Ieps = itemIvaIeps.Ieps / 100;
                itemC.Iva = itemIvaIeps.Iva / 100;

                Product.part_iva = itemIvaIeps.Iva.ToString();
                if (Product != null)
                {
                    return Json(new { success = true, ProductInfo = Product, JsonIVA = itemC.Iva, JsonIEPS = itemC.Ieps, JsonBaseCost = baseCostCombo }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ActionAddComboDetail(List<ItemComboModel> itemsDetail, decimal newComboCost, decimal newComboPrice)
        {
            if (Session["User"] != null && HttpContext.User.Identity.Name != null)
            {
                var Product = _ItemComboBusiness.AddComboDetail(itemsDetail, HttpContext.User.Identity.Name, newComboCost, newComboPrice);
                if (Product == true)
                {
                    return Json(new { success = true, responseText = "Registro Exitoso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
