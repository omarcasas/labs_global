﻿using App.BLL.Lookups;
using App.BLL.PaymentMethod;
using App.Common;
using App.Entities.ViewModels.PaymentMethod;
using FloridoERP.ViewModels.Lookups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.PaymentsMethods
{
    public class PaymentsMethodsController : Controller
    {
        private SatPaymentMethodCodeBusiness _SatPaymentMethodCodeBusiness;
        private PaymentsMethodsBusiness _PaymentsMethodsBusiness;

        public PaymentsMethodsController()
        {
            _SatPaymentMethodCodeBusiness = new SatPaymentMethodCodeBusiness();
            _PaymentsMethodsBusiness = new PaymentsMethodsBusiness();
        }

        // GET: PaymentMethods
        public ActionResult PAME001()
        {
            FillLookups();
            var Model = new PaymentMethodModel();
            return View(Model);
        }

        [HttpPost]
        public ActionResult PAME001 (PaymentMethodModel Model)
        {
            if(Session["User"] != null)
            {
                if (_PaymentsMethodsBusiness.AddPaymentMethod(Model))
                    return Json(new { success = true, Json = Model, responseText = "registro exitoso" }, JsonRequestBehavior.AllowGet);
            }
            return View(Model);
        }

        public ActionResult PAME002()
        {
            var Model = new PaymentMethodModel();
            Model.PaymentsMethods = _PaymentsMethodsBusiness.GetPaymentMethods();
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionDisablePaymentMethod(int PaymentMethodId)
        {
            if (_PaymentsMethodsBusiness.DisablePaymentMethod(PaymentMethodId))
                return Json(new { success = true, Json = PaymentMethodId, responseText = "Método desactivado" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionEditPaymentMethod(int PaymentMethodId)
        {
            var PaymentMethod = new PaymentMethodModel();
            PaymentMethod = _PaymentsMethodsBusiness.GetPaymentMethodById(PaymentMethodId);
            FillLookups();
            ModelState.Clear();
            return PartialView("_PAME002", PaymentMethod);
        }

        [HttpPost]
        public ActionResult ActionEditPaymentMethod(PaymentMethodModel Model)
        {
            if (_PaymentsMethodsBusiness.UpdatePaymentMethod(Model))
                return Json(new { success = true, Json = Model, responseText = "Método de pago editado" }, JsonRequestBehavior.AllowGet);

            return PartialView("_PAME002", Model);
        }

        void FillLookups()
        {
            var Bla = new Lookups();
            ViewBag.SatCode = new SelectList(_SatPaymentMethodCodeBusiness.SatPaymentMethodCodeList(), "sat_pm_code", "sat_pm_description");
            ViewBag.Currency = Bla.Currencies();
            ViewBag.TypeV = Bla.VoucherType();
        }
    }
}