﻿using App.BLL;
using App.BLL.ItemPresentationBusiness;
using App.BLL.MaCode;
using App.BLL.Promotion;
using App.BLL.Site;
using App.Entities;
using App.Entities.ViewModels.Promotion;
using FloridoERP.ViewModels.Purchases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Promotions
{
    public class PromotionController : Controller
    {
        private readonly PromotionHeaderBusiness _promotionHeaderBusinees;
        private readonly PromotionDetailBusiness _promotionDetailBusiness;
        private readonly PromotionSiteBusiness _promotionSiteBusiness;
        private readonly PromotionAdvancedHeaderBusiness _promotionAdvaHeaderBusiness;
        private readonly PromotionAdvancedDetailBusiness _promotionAdvaDetailBusiness;
        private readonly PromotionAdvancedCommonBusiness _promotionAdvaCommonBusiness;
        private readonly ItemPresentationBusiness _itemPresentationBusines;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;
        private ItemMasterBusiness _ItemMasterBusines;
        private SiteBusiness _siteBusiness;

        public PromotionController()
        {
            _promotionHeaderBusinees = new PromotionHeaderBusiness();
            _promotionDetailBusiness = new PromotionDetailBusiness();
            _promotionSiteBusiness = new PromotionSiteBusiness();
            _promotionAdvaHeaderBusiness = new PromotionAdvancedHeaderBusiness();
            _promotionAdvaDetailBusiness = new PromotionAdvancedDetailBusiness();
            _promotionAdvaCommonBusiness = new PromotionAdvancedCommonBusiness();
            _itemPresentationBusines = new ItemPresentationBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _maCodeBusiness = new MaCodeBusiness();
            _ItemMasterBusines = new ItemMasterBusiness();
            _siteBusiness = new SiteBusiness();
        }

        //Agregar Promocion POST
        [HttpPost]
        public ActionResult ActionCreatePromotion(PromotionModel model, List<ModelPromotionItemEdit> items, AddPromotionDetailViewModel sites)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    string cuser = Session["User"].ToString();
                    var promotion_sites_list = sites.site_code.Split(',');
                    string BuyerDivision = _userMasterBusiness.GetBuyerDivision(cuser);
                    var GetPromotionRepeat = _promotionSiteBusiness.GetPromotionRepeat(model, items, promotion_sites_list);
                    if (GetPromotionRepeat.Count() > 0) //Promociones repetidas
                        return Json(new { success = false, responseText = "", responseTextNumber = 10, promotion = GetPromotionRepeat }, JsonRequestBehavior.AllowGet);

                    if (BuyerDivision != null)
                    {   //Creacion de promocion
                        var promotionHeader = _promotionHeaderBusinees.AddPromotionHeader(model, BuyerDivision, cuser);
                        if (promotionHeader != "")
                        {   //Creacion de detalle
                            var codePromotion = promotionHeader;
                            var promotion_detail = _promotionDetailBusiness.AddPromotionDetail(items, codePromotion, cuser);
                            if (promotion_detail == "")
                            {   //Creacion de sitios
                                var promotion_sites = _promotionSiteBusiness.AddPromotionSiteAll(promotion_sites_list, codePromotion, model.promotion_type, cuser);
                                if (promotion_sites == "")
                                    return Json(new { success = true, responseText = codePromotion, responseTextNumber = 6 }, JsonRequestBehavior.AllowGet);
                                else
                                    return Json(new { success = false, responseText = promotion_sites, responseTextNumber = 5 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                                return Json(new { success = false, responseText = promotion_detail, responseTextNumber = 4 }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Error al agregar Promocion. Contacte a sistemas.", responseTextNumber = 3 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Usted no es comprador. Favor de contactar a sistemas.", responseTextNumber = 2 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, responseText = allErrors.ToString(), responseTextNumber = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "SF", responseTextNumber = 0 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCreatePromotionAdvanced(PromotionModel model, List<ModelPromotionItemEdit> items, AddPromotionDetailViewModel sites, List<PromotionCommonEdit> itemsCommon)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                    string cuser = Session["User"].ToString();
                    if (BuyerDivision != null)
                    {   //Insertar el header
                        var promotion_header = _promotionAdvaHeaderBusiness.AddPromotionAdvanced(model, BuyerDivision, cuser);
                        if (promotion_header != "")
                        {   //Insertar detalle
                            var codePromotion = promotion_header;
                            var promotion_detail = _promotionAdvaDetailBusiness.AddPromotionAdvanced(items, codePromotion, cuser);
                            if (promotion_detail == "")
                            {
                                var promotion_common = _promotionAdvaCommonBusiness.AddPromotionCommon(itemsCommon, codePromotion, cuser);
                                if (promotion_common == "")
                                {
                                    var promotion_sites_list = sites.site_code.Split(',');
                                    var promotion_sites = _promotionSiteBusiness.AddPromotionSiteAll(promotion_sites_list, codePromotion, model.promotion_type, cuser);
                                    if (promotion_sites == "")
                                        return Json(new { success = true, responseText = codePromotion, responseTextNumber = 7 }, JsonRequestBehavior.AllowGet);
                                    else
                                        return Json(new { success = false, responseText = promotion_sites, responseTextNumber = 6 }, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new { success = false, responseText = promotion_common, responseTextNumber = 5 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                                return Json(new { success = false, responseText = promotion_detail, responseTextNumber = 4 }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "No se puedo insertar en el cabera de los ofertas.", responseTextNumber = 3 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Usted no es comprador. Contacte a sistemas. ", responseTextNumber = 2 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas", responseTextNumber = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "SF", responseTextNumber = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotion(DateTime? BeginDate, DateTime? EndDate, string site_code, string item, string type, bool AllStores, int onlystatus)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _promotionSiteBusiness.GetPromotions(site_code, type, item, BeginDate, EndDate, AllStores, onlystatus) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionDetails(DateTime? BeginDate, DateTime? EndDate, string site_code, string item, string type, bool AllStores, int onlystatus)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _promotionSiteBusiness.GetPromotionsDetails(site_code, type, item, BeginDate, EndDate, AllStores, onlystatus) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionById(string FolioPromotion, string type)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _promotionHeaderBusinees.GetPromotionId(FolioPromotion, type), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionByIdSites(string FolioPromotion)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _promotionHeaderBusinees.GetPromotionSites(FolioPromotion), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionItemsEdit(string FolioPromotion, string type_promotion)
        {
            if (Session["User"] != null)
            {
                string User = Session["User"].ToString();
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(User);
                string BuyerDivisionSystem = _maCodeBusiness.GetBuyerDivisionSistemas();
                var PromotionCodeInfo = _promotionHeaderBusinees.GetPromotion(FolioPromotion);
                return Json(new { Data = _promotionDetailBusiness.GetItemsByPromotionCode(FolioPromotion), Sites = _promotionDetailBusiness.GetPromotionsSite(FolioPromotion, type_promotion) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Data = false, responseText = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionAdvItemsEdit(string FolioPromotion, string type_promotion)
        {
            if (Session["User"] != null)
            {
                string User = Session["User"].ToString();
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(User);
                string BuyerDivisionSystem = _maCodeBusiness.GetBuyerDivisionSistemas();
                var PromotionCodeInfo = _promotionAdvaHeaderBusiness.GetPromotion(FolioPromotion);
                return Json(new { Data = _promotionAdvaDetailBusiness.GetItemsByPromotionCode(FolioPromotion), DataComun = _promotionAdvaDetailBusiness.GetItemsByPromotionComunCode(FolioPromotion), Sites = _promotionDetailBusiness.GetPromotionsSite(FolioPromotion, type_promotion) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Data = false, responseText = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionCreatePromotionEdit(ModelPromotionEdit model, List<ModelPromotionItemEdit> items, string sites, string sitesD)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    string User = Session["User"].ToString();
                    string promotion_code = model.promotion_code;
                    string BuyerDivision = _userMasterBusiness.GetBuyerDivision(User);
                    var PromotionCodeInfo = _promotionHeaderBusinees.GetPromotion(model.promotion_code);
                    string BuyerDivisionSystem = _maCodeBusiness.GetBuyerDivisionSistemas();
                    if (PromotionCodeInfo.delete_flag == false)
                    {
                        var promotionHeaderR = _promotionHeaderBusinees.PromotionHeaderEdit(PromotionCodeInfo, model, User);
                        if (promotionHeaderR)
                        {
                            var promotionDetailR = _promotionDetailBusiness.UpdateItemByCode(items, promotion_code, User);
                            if (promotionDetailR)
                            {
                                var promotionDetailDelR = _promotionDetailBusiness.UpdateItemDeleteByCode(items, promotion_code, User);
                                if (promotionDetailDelR)
                                {
                                    var promotionSites = _promotionSiteBusiness.UpdateSiteByCode(sites, promotion_code, model.promotion_type, User);
                                    if (promotionSites)
                                    {
                                        var promotionSitesR = _promotionSiteBusiness.UpdateSiteDeleteByCode(sitesD, promotion_code, model.promotion_type, User);
                                        if (promotionSitesR)
                                            return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                                        else
                                            return Json(new { success = false, responseText = "Error al eliminar los sitios. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                        return Json(new { success = false, responseText = "Error al actualizar los sitios. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                    return Json(new { success = false, responseText = "Error al eliminar los productos. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                                return Json(new { success = false, responseText = "Error al editar los productos. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Error al editar información de la Promocion. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Esta promoción se encuentra eliminada." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, responseText = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionCreateAdvPromotionEdit(ModelPromotionEdit model, List<ModelPromotionItemEdit> items, string sites, string sitesD, List<PromotionCommonEdit> itemsCommon)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    string User = Session["User"].ToString();
                    string promotion_code = model.promotion_code;
                    string BuyerDivision = _userMasterBusiness.GetBuyerDivision(User);
                    var PromotionCodeInfo = _promotionAdvaHeaderBusiness.GetPromotion(model.promotion_code);
                    string BuyerDivisionSystem = _maCodeBusiness.GetBuyerDivisionSistemas();
                    if (PromotionCodeInfo.delete_flag == false)
                    {
                        var promotionHeaderR = _promotionAdvaHeaderBusiness.PromotionHeaderEdit(PromotionCodeInfo, model, User);
                        if (promotionHeaderR)
                        {
                            var promotionDetailR = _promotionAdvaDetailBusiness.UpdateItemByPromotionCode(items, promotion_code, User);
                            if (promotionDetailR)
                            {
                                var promotionDetailDelR = _promotionAdvaDetailBusiness.UpdateItemDeleteByPromotionCode(items, promotion_code, User);
                                if (promotionDetailDelR)
                                {
                                    var promotionCommonR = _promotionAdvaCommonBusiness.UpdateCommonByPromotionCode(itemsCommon, promotion_code, User);
                                    if (promotionCommonR)
                                    {
                                        var promotionCommonDelR = _promotionAdvaCommonBusiness.UpdateCommonDeleteByPromotionCode(itemsCommon, promotion_code, User);
                                        if (promotionCommonDelR)
                                        {
                                            var promotionSites = _promotionSiteBusiness.UpdateAdvancedSiteByCode(sites, promotion_code, User);
                                            if (promotionSites)
                                            {
                                                var promotionSitesR = _promotionSiteBusiness.UpdateAdvancedSiteDeleteByCode(sitesD, promotion_code, User);
                                                if (promotionSitesR)
                                                    return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                                                else
                                                    return Json(new { success = false, responseText = "Error al actualizar los sitios. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                                            }
                                            else
                                                return Json(new { success = false, responseText = "Error al actualizar los sitios. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                            return Json(new { success = false, responseText = "Error al eliminar los productos comunes. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                        return Json(new { success = false, responseText = "Error al actualizar los productos comunes. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);

                                }
                                else
                                    return Json(new { success = false, responseText = "Error al eliminar los productos. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                                return Json(new { success = false, responseText = "Error al editar los productos. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Error al editar información de la Promocion. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Esta promoción se encuentra eliminada." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, responseText = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        public ActionResult ActionPromotionDeleteType(string promotion_code, string promotion_type)
        {
            if (Session["User"] != null)
            {
                string User = Session["User"].ToString();
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(User);
                if (promotion_type == "VOLANTE")
                    promotion_type = "NORMAL";

                if (promotion_type != "AVANZADA")
                {
                    var PromotionCodeInfo = _promotionHeaderBusinees.GetPromotion(promotion_code);
                    var promotionHeaderD = _promotionHeaderBusinees.PromotionHeaderDelete(PromotionCodeInfo, User);
                    if (promotionHeaderD)
                    {
                        var promotionsDelete = _promotionSiteBusiness.DeleteSiteByCode(promotion_code, promotion_type, User);
                        if (promotionsDelete)
                        {
                            return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Error al editar al eliminar los sitios de la promoción." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Solo pueden editar la promocion la misma división del comprador creador." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var PromotionAdvCodeInfo = _promotionAdvaHeaderBusiness.GetPromotion(promotion_code);
                    var promotionHeaderD = _promotionAdvaHeaderBusiness.PromotionHeaderDelete(PromotionAdvCodeInfo, User);
                    if (promotionHeaderD)
                    {
                        var promotionsDelete = _promotionSiteBusiness.DeleteSiteByCode(promotion_code, promotion_type, User);
                        if (promotionsDelete)
                        {
                            return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Error al editar al eliminar los sitios de la promoción." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Solo pueden editar la promocion la misma división del comprador creador." }, JsonRequestBehavior.AllowGet);
                }

            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionPromotionReplicate(string promotion_code, string promotion_type, string site_code)
        {
            if (Session["User"] != null)
            {
                return Json(new { success = true, responseText = _promotionSiteBusiness.ReplicatePromotions(promotion_code, promotion_type, site_code) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionCommon(string promotion_code, string common)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _promotionAdvaCommonBusiness.GetPromotionAdvancedCommonByCode(promotion_code, common) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionPromotionAtivate(string promotion_code, string promotion_type)
        {
            if (Session["User"] != null)
            {
                if (promotion_type == "VOLANTE")
                    promotion_type = "NORMAL";
                string User = Session["User"].ToString();
                var promotionHeader = _promotionSiteBusiness.ActivatePromotion(promotion_code, promotion_type, User);
                if (promotionHeader)
                {
                    var promotionSites = _promotionSiteBusiness.ActivatePromotionSites(promotion_code, promotion_type, User);
                    if (promotionSites)
                    {
                        return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Error al cambiar el estatus en la promoción de los sitios." }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, responseText = "Error al cambiar el estatus en la promoción." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }


        ////patricio cazarez
        [HttpPost]
        public ActionResult ActionCreatePromotionPOS(AddPromotionDFLPOSModel model, List<AddPromotionDetailDFLPOSModel> items, AddPromotionDetailViewModel sites)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {


                    var promotionHeader = model.promotionH;

                    promotionHeader.promotion_name = model.promotion_name;
                    promotionHeader.start_date = model.start_date;
                    promotionHeader.end_date = model.end_date;
                    promotionHeader.status = model.status;
                    promotionHeader.promotions_by_ticket = model.promotions_by_ticket;
                    promotionHeader.ticket_header = model.ticket_header;
                    promotionHeader.ticket_footer = model.ticket_footer;
                    promotionHeader.sale_min = model.sale_min;
                    promotionHeader.promotion_value = model.promotion_value;
                    promotionHeader.cuser = HttpContext.User.Identity.Name;
                    promotionHeader.cdate = DateTime.Now;
                    promotionHeader.program_id = "PURCH024.cshtml";
                    promotionHeader.item_amount = model.item_amount;
                    promotionHeader.item_quantity = model.item_quantity;


                    var _ListDetail = new List<DFLPOS_SALES_PROMOTIONS_DETAIL>();
                    if (_promotionHeaderBusinees.AddPromotionDFLPOS(promotionHeader))
                    {
                        var codePromotion = promotionHeader.promotion_id;
                        foreach (var item in items)
                        {

                            DFLPOS_SALES_PROMOTIONS_DETAIL detail = new DFLPOS_SALES_PROMOTIONS_DETAIL();
                            detail.item_amount = item.item_amount;
                            detail.item_quantity = item.item_quantity;
                            detail.part_number = item.part_number;
                            detail.promotion_id = codePromotion;
                            _ListDetail.Add(detail);
                            var val = _promotionDetailBusiness.AddPromotionDetailDFLPOS(detail);
                            if (!val)
                                return Json(new { success = false, responseText = "Error al insertar el código " + item.part_number + ". Contacte a sistemas. " }, JsonRequestBehavior.AllowGet);
                        }
                        var sitess = sites.site_code.Split(',');
                        int con = sitess.Count() - 1;
                        bool all = false;
                        var sitesList = new List<SITES>();
                        if (sitess[0].ToString() == "All")
                        {

                            all = true;
                            con = _siteBusiness.GetAllSitesOferts().Count();
                            sitesList = _siteBusiness.GetAllSitesOferts();
                        }

                        for (int i = 0; i < con; i++)
                        {
                            var site = new DFLPOS_SALES_PROMOTIONS_SITES();
                            site.cdate = DateTime.Now;
                            site.site_code = all ? sitesList[i].site_code : sitess[i].ToString();

                            site.cuser = HttpContext.User.Identity.Name;
                            site.promotion_id = codePromotion;
                            var _proSites = _promotionSiteBusiness.AddPromotionSiteDFLPOSOnSites(promotionHeader, _ListDetail, all ? sitesList[i].site_code : sitess[i].ToString());
                            var val = _promotionSiteBusiness.AddPromotionSiteDFLPOS(site);
                            if (!val)
                                return Json(new { success = false, responseText = "Error al insertar las promociones en las sucursales. Contacte a sistemas. " }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Error al agregar Promoción. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);
                    }

                }

                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, responseText = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}