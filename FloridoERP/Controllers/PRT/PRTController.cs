﻿using App.BLL.PRT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FloridoERP.Report;
using App.BLL.Site;

namespace FloridoERP.Controllers.PRT
{
    public class PRTController : Controller
    {
        // GET: PRT
        private DeliveryOrderBusiness _deliveryOrderBusiness;
        private SiteBusiness _siteBusiness;
        public PRTController()
        {
            _deliveryOrderBusiness = new DeliveryOrderBusiness();
            _siteBusiness = new SiteBusiness();
        }
        // GET: PRTMES
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ActionGetTotalsTransfersByDate(DateTime dateInitial, DateTime dateFinish , string site)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _deliveryOrderBusiness.GetTotalsTransfersByDate(dateInitial, dateFinish,site) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetTransferDetailERPBOX(DateTime dateInitial, DateTime dateFinish, string site, string ip)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _deliveryOrderBusiness.GetTransferDetails(dateInitial, dateFinish, ip, site)}, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetReportPRT(string do_no , string purchase_no , string ip_address , string site_code , string date,string status)
        {
            if (Session["User"] != null)
            {
                var stream = new MemoryStream();
                string site_name = _siteBusiness.GetSiteNameByCode(site_code);
                TransferPRTReport report = new TransferPRTReport();
                var items = _deliveryOrderBusiness.GetTransferProductsPRT(do_no, purchase_no, ip_address, site_code);
                report.DataSource = report.printTable(items, do_no, purchase_no, date,site_name , status);
                report.ExportToPdf(stream);
                return Json(new { success = true, Data = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}