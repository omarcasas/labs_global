﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using App.BLL.Invoices;
using App.BLL.Purchases;
using App.BLL.Site;
using App.Entities.ViewModels.Invoices;
using DevExpress.XtraEditors.Filtering.Templates;
using DevExpress.XtraReports.UI;
using FloridoERP.Report;
using FloridoERPT.Report;

namespace FloridoERP.Controllers.Invoices
{
    public class InvoicesController : Controller
    {
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly InvoiceXmlBusiness _InvoiceXmlBusiness;

        public InvoicesController()
        {
            _supplierBusiness = new SupplierBusiness();
            _siteBusiness = new SiteBusiness();
            _InvoiceXmlBusiness = new InvoiceXmlBusiness();
        }




        public ActionResult CED001()
        {
            var Model = new CedulasModel();

            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionReportGralInvoices(string _month, string _year, int tipo, int _rfc)
        {


            Cedulas _getCedula = new Cedulas();
            CedulaIngresos report = new CedulaIngresos();
            // report.DataSource = report.printTable(_year,_mes);
            //report.ExportToXls(@"C:\FacturasCedulaIng\"+_month+".xls");
            var _data = _getCedula.CreatCedula(_year, _month, tipo, _rfc);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_data, _rfc);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            //report.ExportToXls(@"C:\FacturasCedulaIng\MAYO.xls");
            // return Json(new { success = true, Json = _data }, JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue);
            //   return new JsonResult() { Data = new { success = true, Json = _data }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return PartialView("~/Views/ReportsViews/Reportes.cshtml", report);
        }

        public ActionResult INV000()
        {
            return View();
        }

        public ActionResult INV001()
        {
            return View(_InvoiceXmlBusiness.GetXML());
        }

        public ActionResult ActionGetCustomersBySiteCode(string SiteCode)
        {
            return Json(new { success = true, Json = _InvoiceXmlBusiness.GetCustomersBySiteCode(SiteCode) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetXML()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            return Json(new { success = true, Json = serializer.Serialize(_InvoiceXmlBusiness.GetXML()), MaxJsonLength = Int32.MaxValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveXML(List<InvoiceXml> Xml)
        {
            if (Session["User"] != null)
            {
                if (_InvoiceXmlBusiness.SaveCustomerXML(Xml, Session["User"].ToString()))
                    return Json(new { result = true, model = _InvoiceXmlBusiness.GetXML() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetPDF(int Id)
        {
            if (Session["User"] != null)
            {
                MemoryStream stream = new MemoryStream();

                var model = _InvoiceXmlBusiness.GetPDF(Id);
                InvoicePDF report = new InvoicePDF();

                report.DataSource = report.printTable(model);
                report.ExportToPdf(stream);

                return Json(new { result = true, data = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}