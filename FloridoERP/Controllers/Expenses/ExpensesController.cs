﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Approval;
using App.BLL.Expense;
using App.BLL.InternalRequirement;
using App.Common;
using App.Entities.ViewModels.Expenses;
using System.IO;
using FloridoERP.Report;
using App.BLL.Purchases;
using App.BLL.Site;
using DevExpress.XtraReports.UI;

namespace FloridoERP.Controllers.Expenses
{
    public class ExpensesController : Controller
    {
        private readonly SupplierBusiness _supplierBusiness = new SupplierBusiness();
        private readonly UserMasterRepository _userMasterRepository = new UserMasterRepository();
        private readonly SiteBusiness _siteBusiness = new SiteBusiness();
        private readonly InternalRequirementBusiness _internalRequirementBusiness = new InternalRequirementBusiness();
        private readonly ExpenseBusiness _expenseBusiness = new ExpenseBusiness();
        private readonly ApprovalBusiness _approvalBusiness = new ApprovalBusiness();

        public JsonResult ActionExpenseList()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(_approvalBusiness.ApprovalProcessList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionHistoryList()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            var r = _approvalBusiness.ApprovalHistoryList();
            return Json(r, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionExpenseStart(ExpenseStartReq data)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            data.Username = Session["User"].ToString();
            return Json(_expenseBusiness.ExpenseStart(data), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionExpenseUpdate(ExpenseUpdateReq data)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionSearchItems(string search)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(Result.OK(_internalRequirementBusiness.SearchItemsCreditors(search)), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionExpensesReport(string siteCode, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _expenseBusiness.ExpensesReport(siteCode, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionExpensesReportDetail(string siteCode, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _expenseBusiness.ExpensesReportDetail(siteCode, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, true) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionExpensesReportPrintNew(string siteCode, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
            {
                var detail = _expenseBusiness.ExpensesReportDetail(siteCode, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, false);
                ExpenseConcentratedReport report = new ExpenseConcentratedReport();
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, siteCode, detail/*, _siteBusiness.GetInfoSite()*/);
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/Reportes.cshtml", report);
            }

            return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionExpensesReportDetailPrintNew(string siteCode, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
            {
                var detail = _expenseBusiness.ExpensesReportDetail(siteCode, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, true);
                ExpenseConcentratedDetailReport report = new ExpenseConcentratedDetailReport();
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, siteCode, detail/*, _siteBusiness.GetInfoSite()*/);
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/ExpensesReport.cshtml", report);
            }
            return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetExpenseReportById(int referende_document)
        {
            if (Session["User"] != null)
            {
                var xml_header = _expenseBusiness.getAllItemHeaderById(referende_document);
                var xml_detail = _expenseBusiness.getAllItemsById(referende_document);
                FixedExpensesReport report = new FixedExpensesReport();
                //if (status.print_status == 2)
                //{
                //    report.Watermark.Text = "RE-IMPRESION";
                //    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                //    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                //    report.Watermark.ForeColor = Color.DodgerBlue;
                //    report.Watermark.TextTransparency = 150;
                //    report.Watermark.ShowBehind = false;
                //    report.Watermark.PageRange = "1-99";
                //}
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(xml_header, xml_detail/*, _siteBusiness.GetInfoSite()*/);
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/Reportes.cshtml", report);
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        //public ActionResult ActionExpensesReportPrint(string siteCode, int SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate)
        //{
        //    if (Session["User"] != null)
        //    {
        //        var model = _expenseBusiness.ExpensesReport(siteCode, SupplierId, Category, BeginDate, EndDate);
        //        string siteName;
        //        ModelChartExpenses modelChart = new ModelChartExpenses();
        //        List<ItemXMLDetail> items = new List<ItemXMLDetail>();
        //        if (siteCode != "")
        //        {
        //            modelChart.CreditorsExpensive = _expenseBusiness.CreditorsExpensive(siteCode);
        //            modelChart.CreditorsExpensiveQuantity = _expenseBusiness.CreditorsExpensiveQuantity(siteCode);
        //            modelChart.CreditorsExpensiveReferenceType = _expenseBusiness.CreditorsExpensiveReferenceType(siteCode);
        //            siteName = _siteBusiness.GetSiteNameByCode(siteCode);
        //        }
        //        else
        //        {
        //            modelChart.CreditorsExpensive = _expenseBusiness.CreditorsExpensiveAll();
        //            modelChart.CreditorsExpensiveQuantity = _expenseBusiness.CreditorsExpensiveQuantityAll();
        //            modelChart.CreditorsExpensiveReferenceType = _expenseBusiness.CreditorsExpensiveReferenceTypeAll();
        //            siteName = "TODAS LAS TIENDAS";
        //        }

        //        var itemsModel = model.Select(x => x.ReferenceFolio).Distinct().ToList();

        //        foreach (var item in itemsModel)
        //        {
        //            var i = _expenseBusiness.getAllItemsById(item);
        //            if (i != null || i.Count() > 0)
        //            {
        //                foreach (var e in i)
        //                {
        //                    ItemXMLDetail XMLDetail = new ItemXMLDetail();
        //                    XMLDetail.id = e.id;
        //                    XMLDetail.xml_id = e.xml_id;
        //                    XMLDetail.item_no = e.item_no;
        //                    XMLDetail.item_description = e.item_description;
        //                    XMLDetail.item_amount = e.item_amount;
        //                    XMLDetail.unit_cost = e.unit_cost;
        //                    XMLDetail.quantity = e.quantity;
        //                    XMLDetail.discount = e.discount;
        //                    XMLDetail.iva = e.iva;
        //                    XMLDetail.ieps = e.ieps;
        //                    XMLDetail.ieps_retained = e.ieps_retained;
        //                    XMLDetail.iva_retained = e.iva_retained;
        //                    items.Add(XMLDetail);
        //                }
        //            }
        //        }

        //        ExpenseReport reporte = new ExpenseReport();
        //        (reporte.Bands["DetailReport"] as DetailReportBand).DataSource = reporte.printTable(siteName, _userMasterRepository.getFirstLastName(Session["User"].ToString()), _supplierBusiness.GetNameSupplier(SupplierId), Category, BeginDate, EndDate, model, modelChart);
        //        (reporte.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTable2";
        //        (reporte.Bands["DetailReport1"] as DetailReportBand).DataSource = reporte.printTable2(items);
        //        (reporte.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTable3";

        //        var stream = new MemoryStream();
        //        reporte.ExportToPdf(stream);
        //        return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
        //    }
        //    return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult ActionExpenseGetItemById(int ReferenceFolio)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _expenseBusiness.getAllItemsById(ReferenceFolio), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }
    }
}