﻿using App.BLL.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Department
{
    public class DepartmentController : Controller
    {
        private readonly MaClassBusiness _MaClassBusiness;

        public DepartmentController()
        {
            _MaClassBusiness = new MaClassBusiness();
        }

        // GET: Department
        public ActionResult ActionListDepartment(int Supplier)
        {
            return Json(_MaClassBusiness.getListDepartament(Supplier), JsonRequestBehavior.AllowGet);
        }
    }
}