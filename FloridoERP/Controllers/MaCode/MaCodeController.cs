﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.CommonBusiness;
using App.BLL.MaCode;

namespace FloridoERP.Controllers.MaCode
{
    public class MaCodeController : Controller
    {
        private readonly SysMaCodeBusiness _maCodeBusiness;
        private readonly MaCodeBusiness _MaCodeBusiness;

        public MaCodeController()
        {
            _maCodeBusiness = new SysMaCodeBusiness();
            _MaCodeBusiness = new MaCodeBusiness();
        }

        public JsonResult ActionGetAllUMCodes()
        {
            return Json(_maCodeBusiness.GetAllUM(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetListByCode(string NameTable)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _MaCodeBusiness.GetListByCode(NameTable), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}