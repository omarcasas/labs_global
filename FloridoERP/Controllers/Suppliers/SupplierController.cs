﻿using App.BLL;
using App.BLL.CommonBusiness;
using App.BLL.Purchases;
using App.BLL.Supplier;
using App.Entities;
using FloridoERP.Report;
using FloridoERP.ViewModels.Purchases;
using FloridoERP.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using App.Entities.ViewModels.Supplier;
using App.BLL.ExchangeCurrency;
using App.BLL.MaCode;

namespace FloridoERP.Controllers.Suppliers
{
    public class SupplierController : Controller
    {
        private readonly ItemSupplierBusiness _ItemSupplierBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SupplierContactBusiness _supplierContactBusiness;
        private readonly ItemMasterBusiness _itemMasterBussines;
        private readonly SysMaClassBusiness _maClassBusiness;
        private readonly ExchangeCurrencyBusiness _exchangeCurrencyBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;

        public SupplierController()
        {
            _supplierBusiness = new SupplierBusiness();
            _ItemSupplierBusiness = new ItemSupplierBusiness();
            _supplierContactBusiness = new SupplierContactBusiness();
            _itemMasterBussines = new ItemMasterBusiness();
            _maClassBusiness = new SysMaClassBusiness();
            _exchangeCurrencyBusiness = new ExchangeCurrencyBusiness();
            _maCodeBusiness = new MaCodeBusiness();
        }

        [HttpPost]
        public ActionResult ActionDisableItemSupplier(int Supplier)
        {
            if (Session["User"] != null)
            {                
                var supplier = _ItemSupplierBusiness.DisableItemSupplier(Supplier, Session["User"].ToString());
                if (supplier)                
                    return Json(supplier, JsonRequestBehavior.AllowGet);                
                else                
                    return Json(supplier, JsonRequestBehavior.AllowGet);                
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsBySupplierCode(string supplier_code)
        {
            if (Session["User"] != null)
            {
                var sCode = int.Parse(supplier_code);
                var supplier = _ItemSupplierBusiness.GetItemsBySupplierCodeList(new List<int> { sCode });
                if (supplier != null)                
                    return Json(new { success = true, Json = supplier }, JsonRequestBehavior.AllowGet);                
                else                
                    return Json(new { success = false, responseText = "Proveedor No Tiene Productos." }, JsonRequestBehavior.AllowGet);               
            }
            else            
                return Json(new { success = false, responseText = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }

        [HttpPost]
        public ActionResult ActionGetItemsBySupplierCodeList(List<int> supplier_code_list)
        {
            if (Session["User"] != null)
            {
                var supplier = _ItemSupplierBusiness.GetItemsBySupplierCodeList(supplier_code_list);
                if (supplier != null)                
                    return Json(new { success = true, Json = supplier }, JsonRequestBehavior.AllowGet);                
                else                
                    return Json(new { success = false, responseText = "Proveedor No Tiene Productos." }, JsonRequestBehavior.AllowGet);                
            }
            else            
                return Json(new { success = false, responseText = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult ActionGetItemsBySupplierCodeInSalePrice(string supplier_code)
        {
            if (Session["User"] != null)
            {
                var sCode = int.Parse(supplier_code);
                var supplier = _ItemSupplierBusiness.GetItemsBySupplierCodeInSalePrice(sCode);
                var exchange_price = _exchangeCurrencyBusiness.GetLastExchangeCurrent();
                if (supplier != null)                
                    return Json(new { success = true, Json = supplier, exchange = exchange_price }, JsonRequestBehavior.AllowGet);                
                else                
                    return Json(new { success = false, responseText = "Proveedor sin productos con precio asignado." }, JsonRequestBehavior.AllowGet);                
            }
            else            
                return Json(new { success = false, responseText = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }
        
        [HttpPost]        
        public ActionResult ActionAddSupplier(AddSupplierViewModel model, List<SupplierContactsModel> contacts)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var supplier = model.supplier;
                    supplier.rfc = model.rfc.ToUpper();
                    supplier.supplier_type = model.supplier_type;
                    supplier.merchandise_entry = model.merchandise_entry;
                    supplier.truck_foot = model.truck_foot;
                    supplier.apply_return = model.apply_return;
                    supplier.part_iva_purchase_supplier = model.part_iva_purchase_supplier;
                    if (model.supplier_type == "Acreedor")
                    {
                        supplier.merchandise_entry = 0;
                        supplier.truck_foot = 0;
                        supplier.apply_return = 0;
                    }
                    supplier.cuser = HttpContext.User.Identity.Name;
                    supplier.cdate = DateTime.Now;
                    supplier.program = "PURCH002.cshtml";
                    if (_supplierBusiness.AddSupplier(supplier))
                    {
                        var idSupplier = supplier.supplier_id;
                        foreach (var item in contacts)
                        {
                            MA_SUPPLIER_CONTACT newContact = new MA_SUPPLIER_CONTACT
                            {
                                supplier_id = idSupplier,
                                name = item.name,
                                last_name = item.last_name,
                                phone = item.phone,
                                email = item.email,
                                departament = item.department,
                                cuser = HttpContext.User.Identity.Name,
                                flagActive = true,
                                cdate = DateTime.Now,
                                program_id = "PURCH002.cshtml"
                            };
                            _supplierContactBusiness.AddSupplierContact(newContact);
                        }
                        return Json(new { success = true, id = supplier.supplier_id, name = supplier.business_name }, JsonRequestBehavior.AllowGet);
                    }
                    else                    
                        return Json(new { success = false, message = "Error al agregar Proveedor" }, JsonRequestBehavior.AllowGet);                    
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, message = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else            
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult ActionRFCExistDifferent(string rfc, string type)
        {
            if (Session["User"] != null)
            {
                var x = _supplierBusiness.RfcExists(rfc, type);
                if (x != null)                
                    return Json(new { success = true, id = x.supplier_id, edit = true }, JsonRequestBehavior.AllowGet);                
                else
                {
                    var y = _supplierBusiness.GetByRFC2(rfc);
                    if (y == null)                    
                        return Json(new { success = true, message = "Este RFC se encuentra disponible", edit = true }, JsonRequestBehavior.AllowGet);                    
                    else                    
                        return Json(new { success = true, message = "Este RFC ya se encuentra registrado", edit = false }, JsonRequestBehavior.AllowGet);                    
                }
            }
            else            
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult ActionEditRFC(string rfc, string type, int supplier_id)
        {
            if (Session["User"] != null)
            {
                var consultSupplier = _supplierBusiness.RfcExists(rfc, type);

                if (consultSupplier != null)
                {
                    if (consultSupplier.supplier_id == supplier_id)
                        return Json(new { success = true, num = 3, message = "" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = true, num = 1, message = "Este RFC le pertenece a : " + consultSupplier.business_name + ", intente con otro." }, JsonRequestBehavior.AllowGet);
                }
                else                
                    return Json(new { success = true, num = 2, message = "RFC disponible" }, JsonRequestBehavior.AllowGet);                
            }
            else
                return Json(new { success = false, num = 0, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult ActionRfcExists(string rfc, string type)
        {
            if (Session["User"] != null)
            {
                var x = _supplierBusiness.RfcExists(rfc, type);
                if (x == null)
                {
                    var supplierFromDatabase = _supplierBusiness.GetByRFC2(rfc);
                    var maSupplier = new MaSupplierViewModel();
                    if (supplierFromDatabase != null)
                    {
                        maSupplier = new MaSupplierViewModel()
                        {
                            supplier_id = supplierFromDatabase.supplier_id > 0 ? supplierFromDatabase.supplier_id.ToString() : "",
                            business_name = supplierFromDatabase.business_name,
                            commercial_name = supplierFromDatabase.commercial_name,
                            supplier_type = supplierFromDatabase.supplier_type,
                            zip_code = supplierFromDatabase.zip_code,
                            supplier_address = supplierFromDatabase.supplier_address,
                            city = supplierFromDatabase.city,
                            supplier_state = supplierFromDatabase.supplier_state,
                            country = supplierFromDatabase.country,
                            merchandise_entry = supplierFromDatabase.merchandise_entry,
                            apply_return = supplierFromDatabase.apply_return,
                            truck_foot = supplierFromDatabase.truck_foot,
                            part_iva_purchase_supplier = supplierFromDatabase.part_iva_purchase_supplier
                        };
                    }
                    var contacts = _supplierContactBusiness.GetAllContactsBySupplierId(maSupplier.supplier_id);
                    if (supplierFromDatabase != null)
                        return Json(new { success = true, supplier = maSupplier, message = "Este RFC ya se encuentra registrado pero disponible para " + type, contacts = contacts, edit = true }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = true, message = "Este RFC se encuentra disponible", edit = true }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "Este RFC ya se encuentra registrado como " + type, edit = false }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionEditSupplierType(string rfc, string type, List<SupplierContactsEditModel> contacts)
        {
            if (Session["User"] != null)
            {
                var json = _supplierBusiness.EditSupplier(rfc, type, Session["User"].ToString(), contacts);
                return Json(new { message = json }, JsonRequestBehavior.AllowGet);
            }
            else            
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult ActionEditSupplierIVA(string part_iva, int supplier)
        {
            if (Session["User"] != null)
            {
                var json = _supplierBusiness.EditSupplierPartIvaMetohd(supplier, part_iva, Session["User"].ToString());
                List<SupplierModel> supplierModel = new List<SupplierModel>();
                foreach (var item in _supplierBusiness.GetAllSuppliers())
                {
                    SupplierModel supplierItem = new SupplierModel
                    {
                        supplier_id = item.supplier_id,
                        rfc = item.rfc,
                        commercial_name = item.commercial_name,
                        business_name = item.business_name,
                        supplier_address = item.supplier_address,
                        city = item.city,
                        zip_code = item.zip_code,
                        supplier_state = item.supplier_state,
                        country = item.country,
                        supplier_type = item.supplier_type,
                        accounting_account = item.accounting_account,
                        part_iva_purchase_supplier = item.part_iva_purchase_supplier
                    };
                    supplierModel.Add(supplierItem);
                }
                return Json(new { success = true, supplierModel, responseText = json }, JsonRequestBehavior.AllowGet);
            }
            else            
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);            
        }
        
        public ActionResult ActionGetSupplierById(int id)
        {
            var supplierFromDatabase = _supplierBusiness.GetById(id);
            MaSupplierViewModel maSupplier = new MaSupplierViewModel()
            {
                supplier_id = supplierFromDatabase.supplier_id > 0 ? supplierFromDatabase.supplier_id.ToString() : "",
                city = supplierFromDatabase.city != "" ? supplierFromDatabase.city : "",
                accounting_account = supplierFromDatabase.accounting_account != "" ? supplierFromDatabase.accounting_account : "",
                business_name = supplierFromDatabase.business_name != "" ? supplierFromDatabase.business_name : "",
                commercial_name = supplierFromDatabase.commercial_name != "" ? supplierFromDatabase.commercial_name : "",
                country = supplierFromDatabase.country != "" ? supplierFromDatabase.country : "",
                rfc = supplierFromDatabase.rfc != "" ? supplierFromDatabase.rfc : "",
                supplier_state = supplierFromDatabase.supplier_state != "" ? supplierFromDatabase.supplier_state : "",
                supplier_address = supplierFromDatabase.supplier_address != "" ? supplierFromDatabase.supplier_address : "",
                supplier_type = supplierFromDatabase.supplier_type != "" ? supplierFromDatabase.supplier_type : "",
                zip_code = supplierFromDatabase.zip_code != "" ? supplierFromDatabase.zip_code : "",
                cdate = supplierFromDatabase.cdate != null ? supplierFromDatabase.cdate.Value.ToString() : "",
                cuser = supplierFromDatabase.cuser != "" ? supplierFromDatabase.cuser : "",
                program = supplierFromDatabase.program != "" ? supplierFromDatabase.program : "",
                udate = supplierFromDatabase.udate.ToString(),
                apply_return = supplierFromDatabase.apply_return ?? 0,
                truck_foot = supplierFromDatabase.truck_foot ?? 0,
                merchandise_entry = supplierFromDatabase.merchandise_entry ?? 0
            };
            
            return Json(new { success = true, maSupplier }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult ActionEditSupplier(ListSuppliersViewModel model, List<SupplierContactsModel> contacts)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var supplier = _supplierBusiness.GetById(model.supplier_id);
                    supplier.supplier_id = model.supplier_id;
                    supplier.rfc = model.rfc;
                    supplier.commercial_name = model.commercial_name;
                    supplier.business_name = model.business_name;
                    supplier.supplier_type = model.supplier_type;
                    supplier.supplier_address = model.supplier_address;
                    supplier.zip_code = model.zip_code;
                    supplier.city = model.city;
                    supplier.supplier_state = model.supplier_state;
                    supplier.country = model.country;
                    supplier.truck_foot = model.truck_foot;
                    supplier.merchandise_entry = model.merchandise_entry;
                    supplier.apply_return = model.apply_return;
                    supplier.purchase_applied_return = model.purchase_applied_return;
                    supplier.part_iva_purchase_supplier = model.part_iva_purchase_supplier;
                    supplier.uuser = Session["User"].ToString();
                    supplier.udate = DateTime.Now;

                    if(!_maCodeBusiness.GetAndSetSupplierXML(model.rfc, model.business_name, model.xml_avoidable, Session["User"].ToString()))
                        return Json(new { success = false, responseText = "Ocurrió un error al actualizar los datos.!" }, JsonRequestBehavior.AllowGet);
                    if (_supplierBusiness.UpdateSupplier(supplier))
                    {
                        List<SupplierModel> supplierModel = new List<SupplierModel>();
                        if (contacts != null)
                        {
                            for (int o = 0; o < contacts.Count; o++)
                            {
                                if (contacts[o].supplier_contact_id == 0)
                                {
                                    MA_SUPPLIER_CONTACT newContact = new MA_SUPPLIER_CONTACT
                                    {
                                        supplier_id = model.supplier_id,
                                        name = contacts[o].name,
                                        last_name = contacts[o].last_name,
                                        phone = contacts[o].phone,
                                        email = contacts[o].email,
                                        departament = contacts[o].department,
                                        cuser = Session["User"].ToString(),
                                        flagActive = contacts[o].flagActive,
                                        cdate = DateTime.Now,
                                        program_id = "PURCH003.cshtml"
                                    };
                                    _supplierContactBusiness.AddSupplierContact(newContact);
                                }
                                else
                                {
                                    var contact = _supplierContactBusiness.GetContactById(contacts[o].supplier_contact_id);
                                    contact.name = contacts[o].name;
                                    contact.last_name = contacts[o].last_name;
                                    contact.phone = contacts[o].phone;
                                    contact.email = contacts[o].email;
                                    contact.departament = contacts[o].department;
                                    contact.flagActive = contacts[o].flagActive;
                                    contact.udate = DateTime.Now;
                                    contact.uuser = Session["User"].ToString();
                                    _supplierContactBusiness.UpdateSupplierContact(contact);
                                }
                            }
                            foreach (var item in _supplierBusiness.GetAllSuppliers())
                            {
                                SupplierModel supplierItem = new SupplierModel
                                {
                                    supplier_id = item.supplier_id,
                                    rfc = item.rfc,
                                    commercial_name = item.commercial_name,
                                    business_name = item.business_name,
                                    supplier_address = item.supplier_address,
                                    city = item.city,
                                    zip_code = item.zip_code,
                                    supplier_state = item.supplier_state,
                                    country = item.country,
                                    supplier_type = item.supplier_type,
                                    accounting_account = item.accounting_account,
                                    part_iva_purchase_supplier = item.part_iva_purchase_supplier
                                };
                                supplierModel.Add(supplierItem);
                            }
                            return Json(new { success = true, supplierModel, responseText = "Registro Actualizado!" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            foreach (var item in _supplierBusiness.GetAllSuppliersAndCreditorInModel())
                            {
                                SupplierModel supplierItem = new SupplierModel
                                {
                                    supplier_id = item.supplier_id,
                                    rfc = item.rfc,
                                    commercial_name = item.commercial_name,
                                    business_name = item.business_name,
                                    supplier_address = item.supplier_address,
                                    city = item.city,
                                    zip_code = item.zip_code,
                                    supplier_state = item.supplier_state,
                                    country = item.country,
                                    supplier_type = item.supplier_type,
                                    accounting_account = item.accounting_account,
                                    apply_return = item.apply_return,
                                    purchase_applied_return = item.purchase_applied_return,
                                    truck_foot = item.truck_foot,
                                    merchandise_entry = item.merchandise_entry,
                                    part_iva_purchase_supplier = item.part_iva_purchase_supplier,
                                    xml_avoidable = item.xml_avoidable
                                };
                                supplierModel.Add(supplierItem);
                            }
                            return Json(new { success = true, supplierModel, responseText = "Registro Actualizado!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al actualizar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionListSupplier()
        {
            PURCH010ViewModel viewModel = new PURCH010ViewModel();
            viewModel.AddSupplierViewModel(_supplierBusiness.GetAllSuppliers());
            viewModel.AddProductViewModel(_itemMasterBussines.GetAllItems());
            viewModel.AddCategoryViewModel(_maClassBusiness.GetAllMaClass());
            return Json(new { viewModel.Supplier, viewModel.Product, viewModel.Category, viewModel.Category2 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionTablePurchasePrice(DateTime LaterDate, DateTime NextDate)
        {
            if (Session["User"] != null)            
                return Json(_supplierBusiness.GetPricesOfSuppliersByProducts(LaterDate, NextDate), JsonRequestBehavior.AllowGet);
            
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionPrintTablePurchasePrice(DateTime LaterDate, DateTime NextDate)
        {
            if (Session["User"] != null)
            {
                var Currency = _supplierBusiness.GetPricesOfSuppliersByProducts(LaterDate, NextDate);
                if (Currency != null && Currency.Count > 0)
                {
                    ChangeCost report = new ChangeCost();
                    report.DataSource = report.printTable(Currency, LaterDate, NextDate);

                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);

                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionSearchSupplier(string Filter)
        {
            return new JsonResult() { Data = new { Json = _supplierBusiness.SearchSupplier(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionListSitesXML()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _maCodeBusiness.GetListXMLSites(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }
    }
}