﻿using App.BLL.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Suppliers
{
    public class SupplierContactController : Controller
    {
        private readonly SupplierContactBusiness _supplierContactBusiness;

        public SupplierContactController()
        {
            _supplierContactBusiness = new SupplierContactBusiness();
        }

        public ActionResult ActionGetAllContactsBySupplierId(string supplier_id)
        {
            var listContacts = _supplierContactBusiness.GetAllContactsBySupplierId(supplier_id);
            if (listContacts != null && listContacts.Count > 0)
            {
                return Json(listContacts, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "¡Este código  esta disponible!" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}