﻿using App.BLL;
using App.BLL.Projects;
using App.Common;
using App.Entities.ViewModels.Projects;
using FloridoERP.ViewModels.Project;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Projects
{
    public class ProjectsController : Controller
    {
        private readonly ProjectItemHistoryBusiness _projectItemHistoryBusiness;
        private readonly ProjectReportBusiness _projectReportBusiness;
        private readonly ProjectItemBusiness _projectItemBusiness;
        private readonly ProjectHeaderBusiness _projectHeaderBusiness;
        private readonly ProjectCategoryBusiness _projectCategoryBusiness;
        private readonly UserMasterBusiness _userMasterBsuiness;
        private readonly ProjectLabelBusiness _projectLabelBsuiness;
        private readonly SendMail information;
        private readonly Common common;

        public ProjectsController()
        {
            _projectItemHistoryBusiness = new ProjectItemHistoryBusiness();
            _projectReportBusiness = new ProjectReportBusiness();
            _projectHeaderBusiness = new ProjectHeaderBusiness();
            _projectItemBusiness = new ProjectItemBusiness();
            _projectCategoryBusiness = new ProjectCategoryBusiness();
            _userMasterBsuiness = new UserMasterBusiness();
            _projectLabelBsuiness = new ProjectLabelBusiness();
            information = new SendMail();
            common = new Common();
        }

        public ActionResult PROJ000()
        {
            if (Session["User"] != null)
                return View();
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult PROJ001()
        {
            if (Session["User"] != null)
            {

                PROJ001Model model = new PROJ001Model
                {
                    Users = _userMasterBsuiness.GetAllUsers(),
                    Categories = _projectCategoryBusiness.GetGetAllCategory(),
                    Headers = _projectHeaderBusiness.GetHeaderByUser(Session["User"].ToString())
                };
                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult PROJ002()
        {
            if (Session["User"] != null)
                return View(_projectItemBusiness.GetProjectsHeader(Session["User"].ToString()));
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult PROJ003()
        {
            if (Session["User"] != null)
                return View(_projectItemBusiness.GetProjectsHeaderAll(Session["User"].ToString()));
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult PROJ004()
        {
            return View();
        }

        public ActionResult PROJ005()
        {
            if (Session["User"] != null)
            {
                ProjectReportModel model = new ProjectReportModel
                {
                    Categories = _projectCategoryBusiness.GetGetAllCategory(),
                    Projects = _projectReportBusiness.GetProjectsReport(Session["User"].ToString())
                };

                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PROJ006()
        {
            if (Session["User"] != null)
                return View();
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult PROJ007()
        {
            if (Session["User"] != null)
                return View();
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionProjectReport(int CategoryId, string LabelId, DateTime? StartDate, DateTime? EndDate)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _projectReportBusiness.GetProjectsReport(CategoryId, LabelId, StartDate, EndDate, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionReadyToPayLabels(int project_id)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(_projectLabelBsuiness.ReadyToPayLabels(Session["User"].ToString(), project_id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionActiveProjects()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(_projectHeaderBusiness.ActiveProjects(Session["User"].ToString()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionUploadXmls(UploadProjectXmls data)
        {
            if (Session["User"] == null) return Json(Result.SF());
            return Json(_projectItemBusiness.ActionUploadXmls(Session["User"].ToString(), data));
        }

        public ActionResult ActionGetProjectLabel(int Label, bool statusLabel)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemBusiness.GetProjectLabel(Label, statusLabel, Session["User"].ToString()), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateProjectLabel(ProjectLabelModel Label, ProjectHeaderModel ProjectHeader)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemBusiness.UpdateProjectLabel(Label, ProjectHeader, Session["User"].ToString()), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionPostProjectItem(ProjectItemModel ProjectItem)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemBusiness.PostProjectItem(ProjectItem, Session["User"].ToString()), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateProjectItem(ProjectItemModel ProjectItem)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemBusiness.UpdateProjectItem(ProjectItem, Session["User"].ToString()), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateStatusProjectHeader(ProjectHeaderModel ProjectHeader)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemBusiness.UpdateStatusProjectHeader(ProjectHeader, Session["User"].ToString()), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionAddCategory(string description)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectCategoryBusiness.AddCategory(description, Session["User"].ToString()), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetProjectItem(int IdLabel)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemBusiness.GetProjectItem(IdLabel), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemHistoryBasic(int id_project)
        {
            if (Session["User"] != null)
            {
                List<ProjectUserNotification> list = new List<ProjectUserNotification>();
                var userLabelLeader = _projectLabelBsuiness.GetAllUserLeaderLabel(id_project);
                var project = _projectItemHistoryBusiness.GetItemHistoryById(id_project);
                if (project.Count() > 0)
                {
                    foreach (var item in userLabelLeader)
                    {
                        var label = _projectLabelBsuiness.GetAllUserLabel(item.id_label_project);
                        list.AddRange(label);
                    }
                    var userLabel = list;
                    return new JsonResult() { Data = new { info = project, userCategory = _projectCategoryBusiness.GetAllUserCategory(project[0].id_category, id_project), userLabel = userLabel, userLabelLeader = userLabelLeader }, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                return new JsonResult() { Data = new { info = project }, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllCommentsByItem(int id_item)
        {
            if (Session["User"] != null)
            {
                var item = _projectItemHistoryBusiness.GetAllCommentsByItem(id_item);
                return new JsonResult() { Data = new { item = item, labelInfo = _projectLabelBsuiness.GetLabelInfo(item[0].id_label) }, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAttachedPDFByHistory(int id_hitosry_detail)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectItemHistoryBusiness.GetCommentsPdfByHistory(id_hitosry_detail), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionInsertCommentsWithAttached(int id_item, string comment, List<ProjectItemHistoryDetailAction> attached)
        {
            if (Session["User"] != null)
            {
                bool insertResult = _projectItemHistoryBusiness.InsertCommentsWithAttached(id_item, comment, HttpContext.User.Identity.Name, attached);
                if (insertResult)
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { status = false }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetLabelsByCategory(int id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _projectLabelBsuiness.GetAllLabelsDefaultByCategory(id), MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetProjectItems(int ProjectId, string LabelId)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _projectReportBusiness.GetProjectDetailReport(ProjectId, LabelId, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionAddHeader(ProjectHeaderModel model)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _projectHeaderBusiness.AddProjectHeader(model, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionUpdateHeader(ProjectHeaderModel model)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _projectHeaderBusiness.UpdateProjectHeader(model, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionEmail(int IdProject, int IdLabel, string item, bool history, bool allUsers)
        {
            if (Session["User"] != null)
            {
                Task task = new Task(() =>
                {
                    var Email = _projectLabelBsuiness.GetAllUser(IdProject, IdLabel, allUsers);
                    if (Email.Count() > 0)
                    {

                        if (history)
                        {
                            information.item = item;
                            information.label = Email[0].id_label_project_default;
                            information.subject = "Sistema de proyectos";
                            information.from = "Involucrados del proyecto";
                            information.email = Email[0].all_emails;
                            common.MailLabelAndItems(information, history, allUsers);
                        }
                        else
                        {
                            information.label = Email[0].id_label_project_default;
                            information.subject = "Sistema de proyectos";
                            information.from = "Involucrados del proyecto";
                            information.email = Email[0].all_emails; //"mpasillas@elflorido.com.mx";
                            common.MailLabelAndItems(information, history, allUsers);
                        }

                    }
                });
                task.Start();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionProjectReportPrint(int CategoryId, string LabelId, DateTime? StartDate, DateTime? EndDate)
        {
            if (Session["User"] != null)
            {
                var model = _projectReportBusiness.GetProjectsReport(CategoryId, LabelId, StartDate, EndDate, Session["User"].ToString());

                ProjectReport report = new ProjectReport();
                report.DataSource = report.PrintTable(model);

                var stream = new MemoryStream();
                report.ExportToPdf(stream);

                return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionNotification(string ProjectTitle, int IdProject, string nameLabel, string item)
        {
            if (Session["User"] != null)
            {
                Task task = new Task(() =>
                {
                    var Email = _projectLabelBsuiness.GetUserAllEmail(IdProject);
                    if (Email.Count() > 0)
                    {

                        information.item = item;
                        information.project = ProjectTitle;
                        information.label = nameLabel;
                        information.subject = "Sistema de proyectos: " + ProjectTitle;
                        information.from = "Involucrados del proyecto";
                        information.email = Email[0].all_emails;
                        common.MailLabelAndItems(information, true, false);
                    }
                });
                task.Start();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetHeadersByUser(string user)
        {
            if (Session["User"] != null)
                return Json(new { data = _projectHeaderBusiness.GetHeaderByUser(user) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDetailsByHeader(int id)
        {
            if (Session["User"] != null)
                return Json(new { data = _projectHeaderBusiness.GetDetailsByHeader(id) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionDeleteLabelById(string desc, int idProj, int idLabel)
        {
            if (Session["User"] != null)
                return Json(new { data = _projectLabelBsuiness.DeleteLabelById(desc, idProj, idLabel, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionAddLabelByID(ProjectLabelModel model, int idHeader, int idCategory)
        {
            if (Session["User"] != null)
                return Json(new { data = _projectLabelBsuiness.AddLabelById(model, idHeader, idCategory, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetProjectDetails(int ProjectId)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, data = _projectReportBusiness.GetProjectDetails(ProjectId, Session["User"].ToString()) }, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionUpdateStatusHeader(int id, int status)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _projectHeaderBusiness.UpdateStatusHeader(id, status, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionEditLabelUsers(List<string> users, int idLabel)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _projectLabelBsuiness.UpdateLabelUsers(users, idLabel, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
    }
}