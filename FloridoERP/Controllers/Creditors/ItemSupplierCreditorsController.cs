﻿using App.BLL;
using App.BLL.Creditors;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Creditors
{
    public class ItemSupplierCreditorsController : Controller
    {
        // GET: ItemSupplierCreditors
        private readonly ItemSupplierCreditorsBusiness _ItemSupplierCreditorsBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;

        public ItemSupplierCreditorsController()
        {
            _ItemSupplierCreditorsBusiness = new ItemSupplierCreditorsBusiness();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public ActionResult ActionGetAllSupplierCreditors()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.GetAllSupplierCreditors(BuyerDivision),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveSupplierCreditor(ItemSupplierCreditorsModel ItemSupplier)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.SaveSupplierCreditor(ItemSupplier, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionEditSupplierCreditor(ItemSupplierCreditorsModel ItemSupplier)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.SaveSupplierCreditor(ItemSupplier, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllSuppliersInModel(ItemSupplierCreditorsModel ItemSupplier)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.GetAllSuppliersInModel(),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionDisableItemCreditors(int IdSupplierPartNumber)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.DisableItemCreditors(IdSupplierPartNumber, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveItemCreditors(ItemCreditorPriceModel ItemPrice)
        {
            if (Session["User"] != null)
            {
                _ItemSupplierCreditorsBusiness.DisableItemCreditors(ItemPrice.SupplierPartNumberCreditors, Session["User"].ToString());
                _ItemSupplierCreditorsBusiness.DisabaleItemCreditorActive(ItemPrice, Session["User"].ToString());
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.SaveItemCreditors(ItemPrice, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionEditCategory(int Supplier, List<int> Category)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.EditCategory(Supplier, Category, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionCategorysCreditors(int Supplier)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemSupplierCreditorsBusiness.CategorysCreditors(Supplier),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}