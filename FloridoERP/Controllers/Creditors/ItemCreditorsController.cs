﻿using App.BLL;
using App.BLL.Creditors;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Creditors
{
    public class ItemCreditorsController : Controller
    {
        // GET: ItemCreditors
        private readonly ItemCreditorsBusiness _ItemCreditorsBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly ItemSupplierCreditorsBusiness _ItemSupplierCreditorsBusiness;

        public ItemCreditorsController()
        {
            _ItemCreditorsBusiness = new ItemCreditorsBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _ItemSupplierCreditorsBusiness = new ItemSupplierCreditorsBusiness();
        }

        public ActionResult ActionAllType()
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemCreditorsBusiness.TypeCreditorsList(),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionAddType(TypeCreditorsModel type)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _ItemCreditorsBusiness.AddTypeCreditorsList(type),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveItem(ItemCreditorsModel Item)
        {
            if (Session["User"] != null)
            {
                List<ItemSupplierCreditorsModel> items = new List<ItemSupplierCreditorsModel>();
                var itemCreditor = _ItemCreditorsBusiness.SaveItem(Item, Session["User"].ToString());
                if (itemCreditor.PartNumber != "")
                {
                    if (_ItemCreditorsBusiness.SaveItemNewsuppliers(itemCreditor, Session["User"].ToString())) //Agrega a sus respectivos proveedores
                    {
                        string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                        items = _ItemSupplierCreditorsBusiness.GetAllSupplierCreditors(BuyerDivision);
                    }
                }
                return new JsonResult()
                {
                    Data = new { Products = items, Item = itemCreditor },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionEditItem(ItemCreditorsModel Item)
        {
            if (Session["User"] != null)
            {
                List<ItemSupplierCreditorsModel> items = new List<ItemSupplierCreditorsModel>();
                if (_ItemCreditorsBusiness.EditItem(Item, Session["User"].ToString()))
                {
                    string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                    items = _ItemSupplierCreditorsBusiness.GetAllSupplierCreditors(BuyerDivision);
                }

                return new JsonResult()
                {
                    Data = items,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllItem()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return new JsonResult()
                {
                    Data = _ItemCreditorsBusiness.GetAllCreditorsItems(BuyerDivision),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}