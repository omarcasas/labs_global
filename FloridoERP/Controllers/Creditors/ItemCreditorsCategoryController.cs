﻿using App.BLL;
using App.BLL.Creditors;
using App.Entities.ViewModels.Creditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Creditors
{
    public class ItemCreditorsCategoryController : Controller
    {
        // GET: ItemCreditorsCategory
        private readonly ItemCreditorsCategoryBusiness _itemCreditorsCategoryBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;

        public ItemCreditorsCategoryController()
        {
            _itemCreditorsCategoryBusiness = new ItemCreditorsCategoryBusiness();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public ActionResult ActionGetAllCategorys()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return new JsonResult()
                {
                    Data = _itemCreditorsCategoryBusiness.GetAllCreditorsCategorys(BuyerDivision),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveCategorys(ItemCreditorsCategoryModel category)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _itemCreditorsCategoryBusiness.SaveCategory(category, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionEditCategorys(ItemCreditorsCategoryModel category)
        {
            if (Session["User"] != null)
            {
                return new JsonResult()
                {
                    Data = _itemCreditorsCategoryBusiness.EditCategory(category, Session["User"].ToString()),
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}