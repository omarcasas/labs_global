﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Transfers;
using System.Text;

namespace FloridoERP.Controllers.Transfers
{
    public class TransfersController : Controller
    {
        // GET: Transfers
        private TransfersBusiness _transferBusiness;
        public TransfersController()
        {
            _transferBusiness = new TransfersBusiness();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ActionGetTransfersByDateAndTransferSite(string site, DateTime dateInitial, DateTime dateFinish)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _transferBusiness.GetTransfersByDateAndTransferSite(site, dateInitial, dateFinish) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetTransferHeaderInDetail(DateTime date1, DateTime date2, string TransferType, string status, string originSite, string partNumber, string department, string family, string mainSite)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetAllTransferDetail(date1, date2, TransferType, status, originSite, partNumber, department, family, mainSite), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        public ActionResult ActionGetTransferHeaderInInputs(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family, string mainSite)
        {
            if (Session["User"] != null)
            {
                var list = _transferBusiness.GetTransfersHeaderIn(BeginDate, EndDate, TransferType, status, originSite, partNumber, department, family, mainSite);
                return Json(new { result = true, Data = list, JsonRequestBehavior.AllowGet });
            }
            else
            {
                return Json(new { result = "SF", JsonRequestBehavior.AllowGet });
            }
        }
        public ActionResult ActionGetTransferDetailIn(string document, string transferSiteCode)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetTransferDetailsInReport(document, transferSiteCode), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        public ActionResult ActionGetAllTransferDetailsReport(string Document, string mainSite)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetAllTransferDetailWithoutStatus(Document, mainSite), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        public ActionResult ActionGetTransfersTotalsSP(DateTime dateInit, DateTime dateFinish , int statusTransfers , string part_type)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetTransfersTotalsSP(dateInit, dateFinish , statusTransfers , part_type), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        public ActionResult ActionGetTransfersReportSiteSP(DateTime dateInit, DateTime dateFinish, string site_code , int statusTransfers , string part_type)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetTransfersReportSiteSP(dateInit, dateFinish, site_code , statusTransfers , part_type), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        public ActionResult ActionGetAllTransfersSites(DateTime dateInit, DateTime dateFinish, string site_code_des, string site_code_tra , int statusTransfers , string part_type)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetAllTransfersSites(dateInit, dateFinish, site_code_des , site_code_tra , statusTransfers , part_type), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        public ActionResult ActionGetAllTransferDetailsReportByType(string Document, string mainSite, string status_transfer, string product_type , string transfer_type)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferBusiness.GetAllTransferDetailType(Document, mainSite , status_transfer , product_type , transfer_type), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
    }
}