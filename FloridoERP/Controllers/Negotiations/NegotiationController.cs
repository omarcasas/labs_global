﻿using App.BLL.Purchases;
using App.BLL.Supplier;
using FloridoERP.ViewModels.Purchases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Negotiations
{
    public class NegotiationController : Controller
    {
        private readonly NegotiatonBusiness _NegotiatonBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SupplierContactBusiness _supplierContactBusiness;

        public NegotiationController()
        {
            _NegotiatonBusiness = new NegotiatonBusiness();
            _supplierBusiness = new SupplierBusiness();
            _supplierContactBusiness = new SupplierContactBusiness();
        }

        //Obtener Proveedor y negociaciones por supplier_id
        public ActionResult ActionGetSupplierNegociationBySupplierId(int supplier_id)
        {
            var supplierFromDatabase = _supplierBusiness.GetById(supplier_id);

            var modelObj = new PURCH008ViewModel();
            modelObj.supplier_id = supplierFromDatabase.supplier_id;
            modelObj.rfc = supplierFromDatabase.rfc;
            modelObj.commercial_name = supplierFromDatabase.commercial_name;
            modelObj.business_name = supplierFromDatabase.business_name;
            modelObj.supplier_address = supplierFromDatabase.supplier_address;
            modelObj.city = supplierFromDatabase.city;
            modelObj.zip_code = supplierFromDatabase.zip_code;
            modelObj.supplier_state = supplierFromDatabase.supplier_state;
            modelObj.country = supplierFromDatabase.country;
            modelObj.supplier_type = supplierFromDatabase.supplier_type;
            modelObj.accounting_account = supplierFromDatabase.accounting_account;
            var JsonSupplierInfo = JsonConvert.SerializeObject(modelObj.MA_SUPPLIER, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            var supplierNegotiatonDetail = _NegotiatonBusiness.GetSupplierNegociationBySupplierId(supplier_id, HttpContext.User.Identity.Name);
            var JsonNegotiatonDetail = JsonConvert.SerializeObject(supplierNegotiatonDetail, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            var JsonNegotiationDiscount = JsonConvert.SerializeObject(modelObj.DiscountListItems, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            var JsonDatePenalties = JsonConvert.SerializeObject(modelObj.PenaltiesListItems, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            var JsonDatePoint = JsonConvert.SerializeObject(modelObj.PointListItems, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            var JsonDatePrice = JsonConvert.SerializeObject(modelObj.PriceDiscListItems, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Json(new { success = true, JsonSupplierInfo = JsonSupplierInfo, JsonNegotiatonDetail = JsonNegotiatonDetail, JsonDatePenalties = JsonDatePenalties, JsonDatePoint = JsonDatePoint, JsonDatePrice = JsonDatePrice, JsonNegotiationDiscount = JsonNegotiationDiscount, JsonContact = _supplierContactBusiness.GetAllContactsBySupplierId(supplier_id.ToString()) }, JsonRequestBehavior.AllowGet);
        }
        // Inserta Negociacion PURCH008 
        [HttpPost]
        public ActionResult ActionInsertNegotiation(PURCH008ViewModel Negotiation)
        {
            if (ModelState.IsValid)
            {
                Negotiation.cuser = HttpContext.User.Identity.Name;
                Negotiation.cdate = DateTime.Now;
                Negotiation.program = "PURCH008";
                var NegotiatonMaster = _NegotiatonBusiness.ValidateNegotiatonMasterIfExist(Negotiation.supplier_id);
                if (NegotiatonMaster == null)
                {
                    if (_NegotiatonBusiness.AddNegotiaton(Negotiation.NegotiationMaster))
                    {
                        NegotiatonMaster = _NegotiatonBusiness.ValidateNegotiatonMasterIfExist(Negotiation.supplier_id);
                        Negotiation.nm_id = NegotiatonMaster.nm_id;
                        Negotiation.status = "Creado";
                        if (_NegotiatonBusiness.AddNegotiatonDetail(Negotiation.NegotiationDetail))
                        {
                            var supplierNegotiatonDetail = _NegotiatonBusiness.GetSupplierNegociationBySupplierId(Negotiation.supplier_id, HttpContext.User.Identity.Name);
                            var JsonNegotiatonDetail = JsonConvert.SerializeObject(supplierNegotiatonDetail, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                            Negotiation = new PURCH008ViewModel();
                            var JsonNegotiationDiscount = JsonConvert.SerializeObject(Negotiation.DiscountListItems, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                            var JsonDatePenalties = JsonConvert.SerializeObject(Negotiation.PenaltiesListItems, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                            var JsonDatePoint = JsonConvert.SerializeObject(Negotiation.PointListItems, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                            var JsonDatePrice = JsonConvert.SerializeObject(Negotiation.PriceDiscListItems, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                            return Json(new { success = true, responseText = "Registro Exitoso!", JsonNegotiatonDetail = JsonNegotiatonDetail, JsonDatePenalties = JsonDatePenalties, JsonDatePoint = JsonDatePoint, JsonDatePrice = JsonDatePrice, JsonNegotiationDiscount = JsonNegotiationDiscount }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                        return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    Negotiation.nm_id = NegotiatonMaster.nm_id;
                    Negotiation.status = "Creado";
                    if (_NegotiatonBusiness.AddNegotiatonDetail(Negotiation.NegotiationDetail))
                    {
                        var supplierNegotiatonDetail = _NegotiatonBusiness.GetSupplierNegociationBySupplierId(Negotiation.supplier_id, HttpContext.User.Identity.Name);
                        var JsonNegotiatonDetail = JsonConvert.SerializeObject(supplierNegotiatonDetail, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        Negotiation = new PURCH008ViewModel();
                        var JsonNegotiationDiscount = JsonConvert.SerializeObject(Negotiation.DiscountListItems, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        var JsonDatePenalties = JsonConvert.SerializeObject(Negotiation.PenaltiesListItems, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        var JsonDatePoint = JsonConvert.SerializeObject(Negotiation.PointListItems, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        var JsonDatePrice = JsonConvert.SerializeObject(Negotiation.PriceDiscListItems, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, responseText = "Registro Exitoso!", JsonNegotiatonDetail = JsonNegotiatonDetail, JsonDatePenalties = JsonDatePenalties, JsonDatePoint = JsonDatePoint, JsonDatePrice = JsonDatePrice, JsonNegotiationDiscount = JsonNegotiationDiscount }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Edita la negociacion seleccionada

        [HttpPost]
        public ActionResult ActionUpdateNegotiation(PURCH008ViewModel Negotiation)
        {
            if (ModelState.IsValid)
            {

                var NegotiatonDetail = _NegotiatonBusiness.GetNegotiatonDetailById(Negotiation.nd_id, Negotiation.nm_id);
                NegotiatonDetail.udate = DateTime.Now;
                NegotiatonDetail.uuser = HttpContext.User.Identity.Name;
                NegotiatonDetail.negotiation_type = Negotiation.negotiation_type;
                NegotiatonDetail.payment_conditions = Negotiation.payment_conditions;
                NegotiatonDetail.supplier_references = Negotiation.supplier_references;
                NegotiatonDetail.delivery_points = Negotiation.delivery_points;
                NegotiatonDetail.general_notes = Negotiation.general_notes;
                NegotiatonDetail.negotiatied_discounts = Negotiation.negotiatied_discounts;
                NegotiatonDetail.conditions_prices = Negotiation.conditions_prices;
                NegotiatonDetail.penalties = Negotiation.penalties;
                NegotiatonDetail.quality_level = Negotiation.quality_level;
                NegotiatonDetail.packaging_type = Negotiation.packaging_type;
                NegotiatonDetail.valid_until = Negotiation.valid_until;
                if (_NegotiatonBusiness.UpdateNegotiatonDetail(NegotiatonDetail))
                {
                    var supplierNegotiatonDetail = _NegotiatonBusiness.GetSupplierNegociationBySupplierId(Negotiation.supplier_id, HttpContext.User.Identity.Name);
                    var JsonNegotiatonDetail = JsonConvert.SerializeObject(supplierNegotiatonDetail, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    Negotiation = new PURCH008ViewModel();
                    var JsonNegotiationDiscount = JsonConvert.SerializeObject(Negotiation.DiscountListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    var JsonDatePenalties = JsonConvert.SerializeObject(Negotiation.PenaltiesListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    var JsonDatePoint = JsonConvert.SerializeObject(Negotiation.PointListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    var JsonDatePrice = JsonConvert.SerializeObject(Negotiation.PriceDiscListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    return Json(new { success = true, responseText = "Registro Exitoso!", JsonNegotiatonDetail = JsonNegotiatonDetail, JsonDatePenalties = JsonDatePenalties, JsonDatePoint = JsonDatePoint, JsonDatePrice = JsonDatePrice, JsonNegotiationDiscount = JsonNegotiationDiscount }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Edita la negociacion seleccionada
        [HttpPost]
        public JsonResult ActionUploadNegotiatonDocument()
        {
            try
            {
                var nm_id = Request["nm_id"];
                var nd_id = Request["nd_id"];
                var supplier_id = Request["supplier_id"];
                var FileName = Request["file_name_ext"];
                var NegotiatonDetail = _NegotiatonBusiness.GetNegotiatonDetailById(Convert.ToInt32(nd_id), Convert.ToInt32(nm_id));
                HttpPostedFileBase fs = Request.Files[0];
                BinaryReader br = new BinaryReader(Request.Files[0].InputStream);
                byte[] bytes = br.ReadBytes((Int32)fs.ContentLength);
                NegotiatonDetail.filebase64 = Convert.ToBase64String(bytes);
                NegotiatonDetail.file_name_ext = FileName;
                if (_NegotiatonBusiness.UpdateNegotiatonDetail(NegotiatonDetail))
                {
                    var supplierNegotiatonDetail = _NegotiatonBusiness.GetSupplierNegociationBySupplierId(Convert.ToInt32(supplier_id), HttpContext.User.Identity.Name);
                    var JsonNegotiatonDetail = JsonConvert.SerializeObject(supplierNegotiatonDetail, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });

                    return Json(new { success = true, JsonNegotiatonDetail = JsonNegotiatonDetail, responseText = "El documento se subio con exito!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception df)
            {
                var k = df.Message;
                throw;
            }
        }
        public ActionResult ActionDownloadNegotiationDocument(PURCH008ViewModel Negotiation)
        {
            var NegotiatonDetail = _NegotiatonBusiness.GetNegotiatonDetailById(Negotiation.nd_id, Negotiation.nm_id);
            if (NegotiatonDetail.filebase64 != null)
            {
                return Json(new { success = true, responseText = NegotiatonDetail.filebase64, fileName = NegotiatonDetail.file_name_ext }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "No se encontro documento para esta negociacion" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Inserta  catalogo de negociacion en ma_code 
        [HttpPost]
        public ActionResult ActionInsertMaCodeCatalogue(PURCH008ViewModel Macode)
        {
            if (ModelState.IsValid)
            {
                //Macode = _NegotiatonBusiness.ValidateNegotiatonMasterIfExist(Negotiation.supplier_id);
                Macode.cuser = HttpContext.User.Identity.Name;
                Macode.cdate = DateTime.Now;
                Macode.program = "PURCH008";
                Macode.code = "NEGOTIATON";
                switch (Macode.used)
                {
                    case "Points":
                        Macode.used = "1";
                        break;
                    case "Discounts":
                        Macode.used = "2";
                        break;
                    case "prices":
                        Macode.used = "3";
                        break;
                    case "penalties":
                        Macode.used = "4";
                        break;
                }

                if (_NegotiatonBusiness.AddNegotiatonMaCodeCatalogue(Macode.NegotiationMaCodeAddCatalogue))
                {
                    Macode = new PURCH008ViewModel();
                    var JsonNegotiationDiscount = JsonConvert.SerializeObject(Macode.DiscountListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    var JsonDatePenalties = JsonConvert.SerializeObject(Macode.PenaltiesListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    var JsonDatePoint = JsonConvert.SerializeObject(Macode.PointListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    var JsonDatePrice = JsonConvert.SerializeObject(Macode.PriceDiscListItems, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                    return Json(new { success = true, responseText = "Registro Exitoso!", JsonDatePenalties = JsonDatePenalties, JsonDatePoint = JsonDatePoint, JsonDatePrice = JsonDatePrice, JsonNegotiationDiscount = JsonNegotiationDiscount }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}