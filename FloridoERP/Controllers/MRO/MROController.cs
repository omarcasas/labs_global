﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.CommonBusiness;
using App.BLL.Site;
using FloridoERP.ViewModels.MRO;
using App.Entities.ViewModels.MaquineryEquipment;
using App.BLL.MaquineryEquipment;
using App.BLL.MaCode;
using App.BLL.Purchases;

namespace FloridoERP.Controllers.MRO
{
    public class MROController : Controller
    {
        private readonly SiteBusiness _siteBusiness;
        private readonly SysMaCodeBusiness _maCodeBusiness;
        private readonly MaquineryEquipmentBusiness _MaquineryEquipmentBusiness;
        private readonly SiteBusiness _SiteBusiness;
        private readonly MaCodeBusiness _MaCodeBusiness;
        private readonly SupplierBusiness _supplierBusiness;

        public MROController()
        {
            _siteBusiness = new SiteBusiness();
            _maCodeBusiness = new SysMaCodeBusiness();
            _MaquineryEquipmentBusiness = new MaquineryEquipmentBusiness();
            _SiteBusiness = new SiteBusiness();
            _MaCodeBusiness = new MaCodeBusiness();
            _supplierBusiness = new SupplierBusiness();
        }

        //Menu MRO
        public ActionResult MMRO000()
        {
            return View();
        }

        //Menu Maquinaria y Equipo
        public ActionResult MRO000()
        {
            return View();
        }

        public ActionResult MRO003()
        {
            return View(_siteBusiness.GetAllSites());
        }

        //Vista programacion de servicios para maquinaria y equipo
        public ActionResult MRO001()
        {
            ServiceCalendarModel Model = new ServiceCalendarModel
            {
                Site = _siteBusiness.GetAllSitesOferts(),
                Code = _maCodeBusiness.GetAllCodeByCode("MAQUINERY_TYPE").Where(w => w.vkey_seq != 0).ToList(), //Remover Servicios
                Supplier = _supplierBusiness.GetAllSuppliersToDropDown(),
                MaCode = _maCodeBusiness.GetAllCurrency()
            };
            return View(Model);
        }

        //Vista registro de maquinaria y equipo
        public ActionResult MRO002()
        {
            if (Session["User"] != null)
            {
                MaquineryEquipmentListModel model = new MaquineryEquipmentListModel
                {
                    MaquineryEquipmentList = _MaquineryEquipmentBusiness.getAll(),
                    Deparments = _MaCodeBusiness.GetListByCode("MAQUINERY_TYPE").Where(w => w.vkey_seq != 0).ToList(), //Remover Servicios
                    Sites = _SiteBusiness.GetAllSitesModel()
                };
                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }
    }
}