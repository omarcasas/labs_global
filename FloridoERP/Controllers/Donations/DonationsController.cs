﻿using System.Web.Mvc;
using App.Entities.ViewModels.Donations;
using App.BLL.Donations;

namespace FloridoERP.Controllers.Donations
{
    public class DonationsController : Controller
    {
        private DonationsBusiness _DonationsBusiness;
        private DonationsItemBusiness _DonationsItemBusiness;

        public DonationsController()
        {
            _DonationsBusiness = new DonationsBusiness();
            _DonationsItemBusiness = new DonationsItemBusiness();
        }

        // GET: Donations
        public ActionResult DONA001()
        {
            var Model = new Donation();
            Model.DonationsList = _DonationsBusiness.GetDonationsCampaigns();
            return View(Model);
        }

        [HttpPost]
        public ActionResult DONA001(Donation Model)
        {
            if (Session["User"] != null)
            {
                if (_DonationsBusiness.AddDonationCampaign(Model))
                    return RedirectToAction("DONA001");
            }

            return View(Model);
        }        

        public ActionResult ActionGetArticles(int DonationId)
        {
            var Articles = _DonationsItemBusiness.GetItemsByDonationNumber(DonationId);
            return Json(new { success = true, Json = Articles, responseText = "Articulos de las campañas" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionArticles(int DonationId, bool IsFromEdit, int ArticleId)
        {
            var Model = new DonationItem();
            if (IsFromEdit)
                Model = _DonationsItemBusiness.GetArticleById(ArticleId);
            else
                Model.DonationId = DonationId;

            Model.IsFromEdit = IsFromEdit;
            ModelState.Clear();
            return PartialView("_DONA001ARTICLES", Model);
        }

        [HttpPost]
        public ActionResult ActionArticles(DonationItem Model)
        {
            if (!Model.IsFromEdit)
                if (_DonationsItemBusiness.AddArticle(Model))
                    return Json(new { success = true, Json = Model, responseText = "Articulo guardado" }, JsonRequestBehavior.AllowGet);

            if (Model.IsFromEdit)
                if (_DonationsItemBusiness.UpdateArticle(Model))
                    return Json(new { success = true, Json = Model, responseText = "Articulo editado" }, JsonRequestBehavior.AllowGet);

            return PartialView("_DONA001ARTICLES", Model);
        }

        [HttpGet]
        public ActionResult ActionCampaigns(int DonationId)
        {
            var Model = new Donation();
            Model = _DonationsBusiness.GetDonationById(DonationId);
            ModelState.Clear();
            return PartialView("_DONA001CAMPAIGNS", Model);
        }

        [HttpPost]
        public ActionResult ActionCampaigns(Donation Model)
        {
            if(_DonationsBusiness.UpdateDonationCampaigns(Model))
                return Json(new { success = true, Json = Model, responseText = "Campaña editada" }, JsonRequestBehavior.AllowGet);

            return PartialView("_DONA001CAMPAIGNS", Model);
        }

        [HttpPost]
        public ActionResult ActionDeleteCampaign(int DonationId)
        {
            if (_DonationsBusiness.DeleteCampaign(DonationId))
                return Json(new { success = true, Json = DonationId, responseText = "Campaña borrada" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }
    }
}
