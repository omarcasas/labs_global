﻿using App.BLL.CodesPackages;
using App.Entities.ViewModels.CodesPackege;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.CodesPackages
{
    public class CodesPackagesController : Controller
    {
        private readonly CodesPackagesBusiness _codesPackagesBusiness;

        public CodesPackagesController()
        {
            _codesPackagesBusiness = new CodesPackagesBusiness();
        }

        [HttpPost]
        public ActionResult ActionSaveListCodesPackages(string PartNumberOrigin, List<CodesPackagesModel> Codes)
        {
            if (Session["User"] != null)
                return Json(new { result = _codesPackagesBusiness.SaveCodePackage(PartNumberOrigin, Codes, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetCodesPackages(string PartNumberOrigin)
        {
            if (Session["User"] != null)
                return Json(new { Data = _codesPackagesBusiness.GetCodePackage(PartNumberOrigin) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}