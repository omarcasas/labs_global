﻿using App.BLL;
using App.BLL.CodesPackages;
using App.BLL.CommonBusiness;
using App.BLL.Creditors;
using App.BLL.Expense;
using App.BLL.InternalRequirement;
using App.BLL.ItemPresentationBusiness;
using App.BLL.MaCode;
using App.BLL.Promotion;
using App.BLL.PurchaseOrder;
using App.BLL.Purchases;
using App.BLL.SalesPriceChange;
using App.BLL.Site;
using App.Entities;
using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Expenses;
using App.Entities.ViewModels.ItemPresentation;
using App.Entities.ViewModels.PurchaseOrder;
using FloridoERP.ViewModels.Purchases;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using static FloridoERP.ViewModels.Purchases.PromotionsModel;

namespace FloridoERP.Controllers.Purchases
{
    [Authorize]
    public class PurchasesController : Controller
    {
        private readonly ExpenseBusiness _expensesBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly ItemMasterBusiness _itemMasterBussines;
        private readonly PromotionSiteBusiness _promotionSiteBussiness;
        private readonly SysMaClassBusiness _maClassBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly SysMaCodeBusiness _maCodeBusiness;
        private readonly MaCodeBusiness _maCodeeBusinessSS;
        private readonly InternalRequirementBusiness _internalRequirementBusiness;
        private readonly ItemCreditorsCategoryBusiness _itemCreditorsCategoryBusiness;
        private readonly ItemCreditorsBusiness _ItemCreditorsBusiness;
        private readonly ItemSupplierCreditorsBusiness _ItemSupplierCreditorsBusiness;
        private readonly PromotionHeaderBusiness _promotionHeaderBusiness;
        private readonly ItemPresentationBusiness _itemPresentationBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly MaCodeBusiness _maCodeB;
        private readonly SupplierBusiness _supplierBussines;
        private readonly ItemSupplierBusiness _itemBussines;
        private readonly SysMaTaxBusiness _sysMaTaxBusiness;
        private readonly CodesPackagesBusiness _CodesPackagesBusiness;
        private readonly PurchaseOrderBusiness _PurchaseOrderBusiness;
        private readonly SalesPriceChangesBusiness _SalesPriceChangesBusiness;

        public PurchasesController()
        {
            _expensesBusiness = new ExpenseBusiness();
            _supplierBusiness = new SupplierBusiness();
            _itemMasterBussines = new ItemMasterBusiness();
            _maClassBusiness = new SysMaClassBusiness();
            _siteBusiness = new SiteBusiness();
            _maCodeBusiness = new SysMaCodeBusiness();
            _internalRequirementBusiness = new InternalRequirementBusiness();
            _itemCreditorsCategoryBusiness = new ItemCreditorsCategoryBusiness();
            _ItemCreditorsBusiness = new ItemCreditorsBusiness();
            _ItemSupplierCreditorsBusiness = new ItemSupplierCreditorsBusiness();
            _promotionHeaderBusiness = new PromotionHeaderBusiness();
            _itemPresentationBusiness = new ItemPresentationBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _maCodeB = new MaCodeBusiness();
            _promotionSiteBussiness = new PromotionSiteBusiness();
            _supplierBussines = new SupplierBusiness();
            _itemBussines = new ItemSupplierBusiness();
            _sysMaTaxBusiness = new SysMaTaxBusiness();
            _maCodeeBusinessSS = new MaCodeBusiness();
            _CodesPackagesBusiness = new CodesPackagesBusiness();
            _PurchaseOrderBusiness = new PurchaseOrderBusiness();
            _SalesPriceChangesBusiness = new SalesPriceChangesBusiness();
        }

        //Menu Compras
        public ActionResult MENPURCH000()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        //Menu Proveedores
        public ActionResult PURCH001()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        //Menu productos
        public ActionResult PURCH007()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        //Menu Negociacion
        public ActionResult PURCH006()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        // Negociacion
        public ActionResult PURCH008()
        {
            if (Session["User"] != null)
                return View(new PURCH008ViewModel());

            return RedirectToAction("Login", "Home");
        }

        //Asignar Proveedores a productos
        public ActionResult PURCH005()
        {
            if (Session["User"] != null)
                return View(new PURCH005ViewModel());

            return RedirectToAction("Login", "Home");
        }

        //Cambios de Precio por Proveedor
        public ActionResult PURCH009()
        {
            if (Session["User"] != null)
            {
                PUCHR009ViewModel Model = new PUCHR009ViewModel
                {
                    Suplier = _supplierBusiness.GetAllOnlySuppliers()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Agregar Proveedor GET
        [HttpGet]
        public ActionResult PURCH002()
        {
            if (Session["User"] != null)
            {
                AddSupplierViewModel Model = new AddSupplierViewModel
                {
                    TipoProveedor = _maCodeBusiness.GetAllCodeByCode("SUPPLIER_TYPE"),
                    TipoProveedorIVA = _sysMaTaxBusiness.GetAllIVANormal()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Listado Proveedores
        public ActionResult PURCH003()
        {
            if (Session["User"] != null)
            {
                ListSuppliersViewModel model = new ListSuppliersViewModel
                {
                    TipoProveedor = _maCodeBusiness.GetAllCodeByCode("SUPPLIER_TYPE"),
                    TipoProveedorIVA = _sysMaTaxBusiness.GetAllIVANormal()
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH004()
        {
            if (Session["User"] != null)
            {
                var viewModel = new PURCH004ViewModel();
                return View(viewModel);
            }
            return RedirectToAction("Login", "Home");
        }

        //Reporte Cambio de Costos 
        public ActionResult PURCH010()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH011()
        {
            if (Session["User"] != null)
            {
                PURCH010ViewModel viewModel = new PURCH010ViewModel();
                viewModel.AddSupplierViewModel(_supplierBusiness.GetAllSuppliers());
                viewModel.AddProductViewModel(_itemMasterBussines.GetAllItems());
                viewModel.AddCategoryViewModel(_maClassBusiness.GetAllMaClass());
                return View(viewModel);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH012()
        {
            if (Session["User"] != null)
            {
                PUCHR009ViewModel Model = new PUCHR009ViewModel
                {
                    Suplier = _supplierBusiness.GetAllOnlySuppliers()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Programacion de Ofertas
        public ActionResult PURCH013()
        {
            if (Session["User"] != null)
            {
                PURCH013ViewModel Model = new PURCH013ViewModel
                {
                    Site = _siteBusiness.GetAllSitesOferts(),
                    Ofertas = _maCodeBusiness.GetAllCodeByCode("OFERTA")
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Menu de Caja
        public ActionResult PURCH014()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        //Tipo de cambio
        public ActionResult PURCH015()
        {
            if (Session["User"] != null)
            {
                PURCH015ViewModel model = new PURCH015ViewModel
                {
                    Site = _siteBusiness.GetAllSitesOferts()
                };
                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Configurar Combos Vista
        public ActionResult PURCH016()
        {
            if (Session["User"] != null)
                return View(new PURCH016ViewModel());

            return RedirectToAction("Login", "Home");
        }

        //Captura Contenido Codigos en Paquetes
        public ActionResult PURCH017()
        {
            if (Session["User"] != null)
            {
                var model = new PresentationViewModel
                {
                    Items = _itemMasterBussines.GetItemsFalseFlag()
                };
                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Catalogo de servicios y productos
        public ActionResult PURCH018()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                var ListCreditors = new ItemCreditorsFullModel
                {
                    ItemCreditorsCategoryList = _itemCreditorsCategoryBusiness.GetAllCreditorsCategorys(BuyerDivision),
                    ItemCreditorsList = _ItemCreditorsBusiness.GetAllCreditorsItems(BuyerDivision),
                    ItemSupplierCreditorsList = _ItemSupplierCreditorsBusiness.GetAllSupplierCreditors(BuyerDivision),
                    TypeCreditorsList = _ItemCreditorsBusiness.TypeCreditorsList(),
                    SupplierList = _ItemSupplierCreditorsBusiness.GetAllSuppliersInModel(),
                    Currency = _maCodeBusiness.GetAllCurrency()
                };
                return View(ListCreditors);
            }
            return RedirectToAction("Login", "Home");
        }

        //Agrupar Ordenes de Compras
        public ActionResult PURCH019()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        //Requisicion Consumo Interno
        public ActionResult PURCH020()
        {
            if (Session["User"] != null)
            {
                PURCH020ViewModel Model = new PURCH020ViewModel
                {
                    InternalRequierement = _internalRequirementBusiness.getAllItemsCreditors(),
                    Sites = _siteBusiness.GetAllSitesName()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        //Requisicion Gasto
        public ActionResult PURCH021()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH022()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    ExpensesReportModelList = _expensesBusiness.ExpensesReport("", "", "", firstDayOfMonth, lastDayOfMonth, "", "", ""),
                    Ma_SupplierList = _supplierBusiness.GetAllByType("Acreedor"),
                    Site = _siteBusiness.GetAllSitesNameAll(),
                    ListCategory = _maCodeeBusinessSS.GetListByCode("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult COST001()
        {
            return View();
        }

        public ActionResult PURCH023()
        {
            if (Session["User"] != null)
            {
                string User = Session["User"].ToString();
                PURCH023ViewModel Model = new PURCH023ViewModel
                {
                    Items = _itemMasterBussines.GetAllItemsToDropDownList(),
                    Site = _siteBusiness.GetAllSitesOferts(),
                    Ofertas = _maCodeBusiness.GetAllCodeByCode("OFERTA"),
                    ItemsPresentation = _itemPresentationBusiness.GetAllBasicInformation(),
                    Promotions = _promotionSiteBussiness.GetPromotionsDetails("", "", "", null, null, true, 9)
                };
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(User);
                string BuyerDivisionSystem = _maCodeB.GetBuyerDivisionSistemas();
                if (BuyerDivisionSystem == BuyerDivision)
                    Model.BuyerDivisionSistemas = true;
                else
                    Model.BuyerDivisionSistemas = false;

                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        //promociones para PV impresion de ticket adicional o boletos
        public ActionResult PURCH024()
        {
            if (Session["User"] != null)
            {
                PromotionsModel Model = new PromotionsModel()
                {
                    Items = _itemMasterBussines.GetAllItemsToDropDownList(),
                    Site = _siteBusiness.GetAllSitesOferts(),
                    Ofertas = _maCodeBusiness.GetAllCodeByCode("OFERTA")
                };
                var _SuppList = new List<SuppliersListProm>();
                foreach (MA_SUPPLIER sup in _supplierBussines.GetAllOnlySuppliersForPromotions())
                {
                    var _Supp = new SuppliersListProm
                    {
                        supplier_id = sup.supplier_id,
                        Name = sup.commercial_name
                    };
                    _SuppList.Add(_Supp);
                }
                Model.SuppliersList = _SuppList;
                ViewBag.SupplierCode = new List<SelectListItem>();
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public ActionResult ActionGetItemsBySupplierId(int supplier_id)
        {
            return Json(new { status = true, model = _itemBussines.GetItemsBySupplierIdToPromotions(supplier_id) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PURCH025()
        {
            if (Session["User"] != null)
            {
                PURCH025Model Model = new PURCH025Model
                {
                    Site = _siteBusiness.GetAllSitesStore()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH026()
        {
            if (Session["User"] != null)
            {
                PURCH026ViewModel model = new PURCH026ViewModel
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1),
                    Supplier = _supplierBusiness.GetAllOnlySuppliers(),
                    Site = _siteBusiness.GetAllSitesName()
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH028()
        {
            if (Session["User"] != null)
            {
                PUCHR009ViewModel Model = new PUCHR009ViewModel
                {
                    Suplier = _supplierBusiness.GetAllOnlySuppliers(),
                    items = _itemMasterBussines.GetAllItemsToDropDownList()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH029()
        {
            if (Session["User"] != null)
                return View(_CodesPackagesBusiness.GetCodesPackages());

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionGetCodePackages(string OriginPartNumber)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _CodesPackagesBusiness.GetCodePackagesByOriginPartNumber(OriginPartNumber) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PURCH030()
        {
            if (Session["User"] != null)
                return View(_siteBusiness.GetAllSitesName());

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionGetStockHistory(string SiteCode, string Date)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _itemMasterBussines.GetStockHistory(SiteCode, Date) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PURCH031()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH032()
        {
            if (Session["User"] != null)
            {
                PurchasesOrderViewModel model = new PurchasesOrderViewModel
                {
                    Sites = _siteBusiness.GetAllSitesNameAll(),
                    Departaments = _maClassBusiness.GetAllDepartments(1),
                    CurrencyList = _maCodeBusiness.GetCurrency()
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH033()
        {
            if (Session["User"] != null)
            {
                return View(_maClassBusiness.GetAllDepartments(1));
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionGetAllPurchasesOrdersXML(DateTime date1, DateTime date2, string siteCode, int supplier, string partNumber, string department, string family, string currency)
        {
            return Json(new { success = true, Json = _PurchaseOrderBusiness.GetAllPurchasesOrdersXML(date1, date2, siteCode, supplier, partNumber, department, family, currency) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetPurchaseDetail(string PoNumber, string SiteCode)
        {
            return Json(new { success = true, Json = _PurchaseOrderBusiness.NewGetAllPurchasesDetailStatusFormatView(PoNumber, SiteCode) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllPurchasesOrdersStatusSupplierDetailXML(DateTime Date1, DateTime Date2, string siteCode, int supplier, string partNumber, string department, string family, string currency)
        {
            return new JsonResult() { Data = _PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplierDetailXML(Date1, Date2, siteCode, supplier, partNumber, department, family, currency), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionGetSalesPriceChanges(DateTime BeginDate, DateTime EndDate, string Department, string Family)
        {
            return new JsonResult() { Data = _SalesPriceChangesBusiness.GetSalesPriceChanges(BeginDate, EndDate, Department, Family), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionGetSalesPriceChangesDetail(DateTime BeginDate, DateTime EndDate, string PartNumber)
        {
            return new JsonResult() { Data = _SalesPriceChangesBusiness.GetSalesPriceChangesDetail(BeginDate, EndDate, PartNumber), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionGetSalesPriceChangesPresentations(DateTime BeginDate, DateTime EndDate, string Department, string Family)
        {
            return new JsonResult() { Data = _SalesPriceChangesBusiness.GetSalesPriceChangesPresentations(BeginDate, EndDate, Department, Family), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionGetSalesPriceChangesDetailPresentations(DateTime BeginDate, DateTime EndDate, int PresentationId)
        {
            return new JsonResult() { Data = _SalesPriceChangesBusiness.GetSalesPriceChangesPresentationsDetail(BeginDate, EndDate, PresentationId), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
    }
}