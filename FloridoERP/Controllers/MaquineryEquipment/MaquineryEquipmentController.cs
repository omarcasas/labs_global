﻿using App.BLL.MaquineryEquipment;
using App.Entities.ViewModels.MaquineryEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers
{
    public class MaquineryEquipmentController : Controller
    {
        private readonly MaquineryEquipmentBusiness _MaquineryEquipmentBusiness;

        public MaquineryEquipmentController()
        {
            _MaquineryEquipmentBusiness = new MaquineryEquipmentBusiness();
        }

        public ActionResult ActionGetMaquineryEquipment()
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _MaquineryEquipmentBusiness.getAll() }, JsonRequestBehavior.AllowGet);

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetMaquineryEquipment(int EquipmentId)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _MaquineryEquipmentBusiness.getEquipmentById(EquipmentId) }, JsonRequestBehavior.AllowGet);

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddMaquineryEquipment(MaquineryEquipmentModel model)
        {
            if (Session["User"] != null)
            {
                if (_MaquineryEquipmentBusiness.validateMaquineryEquipment(model.SerialNumber))
                {
                    if (_MaquineryEquipmentBusiness.addMaquineryEquipment(model, Session["User"].ToString()))
                        return Json(new { status = true, valid = true, model = _MaquineryEquipmentBusiness.getAll() }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = false, valid = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateMaquineryEquipment(MaquineryEquipmentModel model)
        {
            if (Session["User"] != null)
            {
                if (_MaquineryEquipmentBusiness.validateExistingSerialNumber(model.SerialNumber, model.EquipmentId))
                {
                    if (_MaquineryEquipmentBusiness.setMaquineryEquipment(model, Session["User"].ToString()))
                        return Json(new { status = true, valid = true, model = _MaquineryEquipmentBusiness.getAll() }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = false, valid = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionRemoveMaquineryEquipment(int EquipmentId, bool FlagActive)
        {
            if (Session["User"] != null)
            {
                if (_MaquineryEquipmentBusiness.removeMaquineryEquipment(EquipmentId, FlagActive, Session["User"].ToString()))
                    return Json(new { status = true, model = _MaquineryEquipmentBusiness.getAll() }, JsonRequestBehavior.AllowGet);

                return Json(new { status = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetEquipmentBySiteAndDepartment(string Department, string SiteCode, string ServiceDate)
        {
            if (Session["User"] != null)
            {
                var list = _MaquineryEquipmentBusiness.GetAllBySiteAndDepartmentToDropDown(Department, SiteCode, ServiceDate);
                if (list.Item1 != null)
                    return Json(new { success = true, Json = list.Item1 }, JsonRequestBehavior.AllowGet);
                else
                {
                    if (list.Item2 == 0)
                        return Json(new { success = false, message = "La maquinaria ya cuenta con una programacion para este dia." }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = false, message = "Sin maquinaria y equipo para esa Tienda y Departamento." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);
        }
    }
}