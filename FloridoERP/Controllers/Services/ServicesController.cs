﻿using App.BLL.Services;
using App.Entities.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Services
{
    public class ServicesController : Controller
    {
        private ServicesCompaniesBusiness _ServicesCompaniesBusiness;
        private ServicesBusiness _ServicesBusiness;
        private ServicesProductsBusiness _ServicesProductsBusiness;

        public ServicesController()
        {
            _ServicesCompaniesBusiness = new ServicesCompaniesBusiness();
            _ServicesBusiness = new ServicesBusiness();
            _ServicesProductsBusiness = new ServicesProductsBusiness();
        }
        
        public ActionResult SERV001()
        {
            var Model = new ServiceProduct();
            FillLookups();
            return View(Model);
        }

        [HttpPost]
        public ActionResult SERV001(ServiceProduct Model)
        {
            if (Session["User"] != null)
            {
                if (_ServicesProductsBusiness.AddServicesProduct(Model))
                    return Json(new { success = true, Json = Model, responseText = "registro exitoso" }, JsonRequestBehavior.AllowGet);                
            }
            return View(Model);
        }        

        public ActionResult SERV004()
        {
            var Model = new ServiceProduct();
            Model.Products = _ServicesProductsBusiness.GetProductsList();

            return View(Model);
        }

        [HttpGet]
        public ActionResult ActionProducts(int ProductId)
        {
            var Product = new ServiceProduct();
            Product = _ServicesProductsBusiness.GetProductById(ProductId);
            ModelState.Clear();
            return PartialView("_SERV004", Product);
        }

        [HttpPost]
        public ActionResult ActionProducts(ServiceProduct Model)
        {
            if (_ServicesProductsBusiness.UpdateServiceProduct(Model))
                return Json(new { success = true, Json = Model, responseText = "Artículo editado" }, JsonRequestBehavior.AllowGet);

            return PartialView("_SERV004", Model);
        }

        [HttpPost]
        public ActionResult ActionDisableProduct(int ProductId)
        {
            if (_ServicesProductsBusiness.DisableServiceProduct(ProductId))
                return Json(new { success = true, Json = ProductId, responseText = "Artículo desactivado" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }

        void FillLookups()
        {
            ViewBag.CompanyId = new SelectList(_ServicesCompaniesBusiness.ServicesCompanies(), "id", "company");
            ViewBag.ServiceId = new SelectList(_ServicesBusiness.Services(), "id", "description");
        }

        #region Services Companies
        public ActionResult SERV002()
        {
            var Model = new ServiceCompany();
            Model.Companies = _ServicesCompaniesBusiness.GetCompaniesList();

            return View(Model);
        }

        [HttpPost]
        public ActionResult SERV002(ServiceCompany Model)
        {
            if (Session["User"] != null)
            {
                if (_ServicesCompaniesBusiness.AddCompany(Model))
                    return RedirectToAction("SERV002");
            }

            return View(Model);
        }

        [HttpGet]
        public ActionResult ActionServiceCompany(int CompanyId)
        {
            var Model = new ServiceCompany();
            Model = _ServicesCompaniesBusiness.GetServiceCompanyById(CompanyId);
            ModelState.Clear();
            return PartialView("_SERV002", Model);
        }

        [HttpPost]
        public ActionResult ActionServiceCompany(ServiceCompany Model)
        {
            if (_ServicesCompaniesBusiness.UpdateServiceCompany(Model))
                return Json(new { success = true, Json = Model, responseText = "Compañia editada" }, JsonRequestBehavior.AllowGet);

            return PartialView("_SERV002", Model);
        }

        [HttpPost]
        public ActionResult ActionDisableServiceCompany(int CompanyId)
        {
            if (_ServicesCompaniesBusiness.DisableServiceCompany(CompanyId))
                return Json(new { success = true, Json = CompanyId, responseText = "Compañía desactivada" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Services
        public ActionResult SERV003()
        {
            var Model = new Service();
            Model.Services = _ServicesBusiness.GetServices();

            return View(Model);
        }

        [HttpPost]
        public ActionResult SERV003(Service Model)
        {
            if (Session["User"] != null)
            {
                if (_ServicesBusiness.AddService(Model))
                    return RedirectToAction("SERV003");
            }

            return View(Model);
        }

        [HttpGet]
        public ActionResult ActionService(int ServiceId)
        {
            var Model = new Service();
            Model = _ServicesBusiness.GetServiceById(ServiceId);
            ModelState.Clear();
            return PartialView("_SERV003", Model);
        }

        [HttpPost]
        public ActionResult ActionService(Service Model)
        {
            if (_ServicesBusiness.UpdateService(Model))
                return Json(new { success = true, Json = Model, responseText = "Servicio editado" }, JsonRequestBehavior.AllowGet);

            return PartialView("_SERV003", Model);
        }

        [HttpPost]
        public ActionResult ActionDisableService(int ServiceId)
        {
            if (_ServicesBusiness.DisableService(ServiceId))
                return Json(new { success = true, Json = ServiceId, responseText = "Servicio desactivado" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
