﻿using App.BLL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Inventory
{
    public class InventoryCalendarController : Controller
    {
        private readonly InventoryCalendarBusiness _InventoryCalendarBussines;

        public InventoryCalendarController()
        {
            _InventoryCalendarBussines = new InventoryCalendarBusiness();
        }

        [HttpGet]
        public ActionResult ActionMonthCalendar(DateTime DateStart)
        {
            var lastDayOfMonth = DateStart.AddMonths(2).AddDays(-1);
            DateStart = DateStart.AddMonths(-1);
            var Inventorys = _InventoryCalendarBussines.getInventoryCalendarDatesBetween(DateStart, lastDayOfMonth);
            return Json(Inventorys, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateInventoryCalendarOnlyDate(InventoryCalendarModel InventoryObj)
        {
            if (HttpContext.User.Identity.Name != null)
            {
                var updateReturn = _InventoryCalendarBussines.UpdateInventoryCalendarOnlyDate(InventoryObj, HttpContext.User.Identity.Name, InventoryObj.program_id);
                if (updateReturn)
                    return new JsonResult() { Data = new { success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                else
                    return Json(new { success = false, responseText = "Error al registrar!" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionInventoryCalendar(InventoryCalendarModel InventoryObj, bool typeOf)
        {
            if (HttpContext.User.Identity.Name != null)
            {
                bool updateReturn;
                if (typeOf)//true inserta
                {
                    InventoryObj.cuser = HttpContext.User.Identity.Name;
                    updateReturn = _InventoryCalendarBussines.InsertInventoryCalendar(InventoryObj);
                }
                else//false hace update
                {
                    InventoryObj.uuser = HttpContext.User.Identity.Name;
                    updateReturn = _InventoryCalendarBussines.UpdateInventoryCalendar(InventoryObj);
                }
                if (updateReturn)
                {
                    var lastDayOfMonth = InventoryObj.start.AddMonths(2).AddDays(-1);
                    InventoryObj.start = InventoryObj.start.AddMonths(-1);
                    var Inventorys = _InventoryCalendarBussines.getInventoryCalendarDatesBetween(InventoryObj.start, lastDayOfMonth);

                    return new JsonResult() { Data = new { success = true, Inventorys, responseText = "Registro Exitoso!" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                    return Json(new { success = false, responseText = "Error al registrar!" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddInventoryCyclic(InventoryCalendarModel Inventory, List<string> part_numbers)
        {
            if (HttpContext.User.Identity.Name != null)
                return new JsonResult() { Data = new { success = true, Inventory = _InventoryCalendarBussines.AddInventoryCyclic(Inventory, part_numbers, HttpContext.User.Identity.Name), responseText = "Registro Exitoso!" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionCloseInventory(int id, DateTime start)
        {
            if (HttpContext.User.Identity.Name != null)
            {
                var r = _InventoryCalendarBussines.CancelInventory(id, HttpContext.User.Identity.Name);
                if (r)
                {
                    var lastDayOfMonth = start.AddMonths(2).AddDays(-1);
                    start = start.AddMonths(-1);
                    var Inventorys = _InventoryCalendarBussines.getInventoryCalendarDatesBetween(start, lastDayOfMonth);
                    return new JsonResult() { Data = new { success = r, Inventories = Inventorys}, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    return new JsonResult() { Data = new { success = r }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
            }             
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}