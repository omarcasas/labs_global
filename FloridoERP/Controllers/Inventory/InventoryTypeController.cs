﻿using App.BLL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Inventory
{
    public class InventoryTypeController : Controller
    {
        private readonly InventoryTypeBusiness _InventoryTypeBusiness;

        public InventoryTypeController()
        {
            _InventoryTypeBusiness = new InventoryTypeBusiness();
        }

        [HttpPost]
        public ActionResult ActionSaveInventoryType(InventoryTypeModel InventoryType, List<InventoryTypeDetailsModel> InventoryTypeDetails)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _InventoryTypeBusiness.SaveInventory(InventoryType, InventoryTypeDetails, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionEditInventoryType(InventoryTypeModel InventoryType, List<InventoryTypeDetailsModel> InventoryTypeDetails)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _InventoryTypeBusiness.EditInventory(InventoryType, InventoryTypeDetails, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllInventoryType()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _InventoryTypeBusiness.GetAllInventoryType(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryTypeDatails(int IdInventoryType)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _InventoryTypeBusiness.GetInventoryTypeDatails(IdInventoryType), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionIsActiveInventory(int Inventory, bool Active)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _InventoryTypeBusiness.IsActiveInventory(Inventory, Active, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}