﻿using App.BLL.CommonBusiness;
using App.BLL.Inventory;
using App.BLL.MaCode;
using App.BLL.Site;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.Controllers.Inventory
{
    public class InventoryController : Controller
    {
        private readonly SysMaClassBusiness _MaClassBussines;
        private readonly MaCodeBusiness _MaCodeBusiness;
        private readonly InventoryCalendarBusiness _InventoryCalendarBussines;
        private readonly SiteBusiness _siteBusiness;
        private readonly InventoryTypeBusiness _InventoryTypeBusiness;

        // GET: Inventory
        public InventoryController()
        {
            _MaClassBussines = new SysMaClassBusiness();
            _MaCodeBusiness = new MaCodeBusiness();
            _InventoryCalendarBussines = new InventoryCalendarBusiness();
            _siteBusiness = new SiteBusiness();
            _InventoryTypeBusiness = new InventoryTypeBusiness();
        }

        public ActionResult INVE000()
        {
            return View();
        }

        public ActionResult INVE001()
        {
            var Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var startDate = Date.AddMonths(-1);
            var endDate = Date.AddMonths(2).AddDays(-1);
            InventoryCalendarModel model = new InventoryCalendarModel
            {
                siteList = _siteBusiness.GetAllSitesName(),
                InventoryTypeList = _InventoryTypeBusiness.GetAllInventoryTypeActives(),
                InventoryCalendarModellist = _InventoryCalendarBussines.getInventoryCalendarDatesBetween(startDate, endDate)
            };
            return View(model);
        }

        public ActionResult INVE002()
        {
            var Options = new InventoryTypeOptionModel
            {
                ListMaClass = _MaClassBussines.GetMaClassDivisionForLevel(1),
                ListMaCode = _MaCodeBusiness.GetListByCode("TYPE_REVIEW"),
                ListInventory = _InventoryTypeBusiness.GetAllInventoryType()
            };
            return View(Options);
        }

        public ActionResult INVE003()
        {
            InventoryCalendarModel model = new InventoryCalendarModel
            {
                siteList = _siteBusiness.GetAllSitesName()
            };
            return View(model);
        }

        public ActionResult MENWSM000()
        {
            return View(_siteBusiness.GetAllSitesName());
        }
    }
}