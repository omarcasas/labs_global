﻿using App.BLL.ExchangeCurrency;
using App.BLL.Site;
using App.Entities;
using FloridoERP.Report;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace FloridoERP.Controllers.ExchangeCurrency
{
    public class ExchangeCurrencyController : Controller
    {
        private readonly ExchangeCurrencyBusiness _ExchangeCurrencyBusiness;
        private readonly SiteBusiness _siteBusiness;

        public ExchangeCurrencyController()
        {
            _ExchangeCurrencyBusiness = new ExchangeCurrencyBusiness();
            _siteBusiness = new SiteBusiness();
        }

        [HttpPost]
        public ActionResult ActionPostTypeCurrency(decimal Price, DateTime Date, string Sites)
        {
            if (Session["User"] != null)
            {
                var ExchangeCurrency = new EXCHANGE_CURRENCY_COMMERCIAL()
                {
                    price = Price,
                    program_id = "PURCH015.cshtml",
                    currency = "USD",
                    currency_date = Date.Date,
                    cuser = Session["User"].ToString(),
                    cdate = DateTime.Now
                };
                var exchange = _ExchangeCurrencyBusiness.postExchangeCurrency(ExchangeCurrency);
                if (exchange > 0)
                {
                    var sitess = Sites.Split(',');
                    int con = sitess.Count() - 1;
                    if (sitess[0].ToString() == "All")
                    {
                        var value = _ExchangeCurrencyBusiness.AddExchangeSiteAll(exchange, HttpContext.User.Identity.Name);
                        if (value)
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        for (int i = 0; i < con; i++)
                        {
                            EXCHANGE_CURRENCY_COMMERCIAL_SITES site = new EXCHANGE_CURRENCY_COMMERCIAL_SITES();
                            site.id_exchange_currency_commercial = exchange;
                            site.cuser = HttpContext.User.Identity.Name;
                            site.replication_status = 0;
                            site.cdate = DateTime.Now;
                            site.program_id = "PURCH015.cshtml";
                            site.site_code = sitess[i].ToString();
                            var value = _ExchangeCurrencyBusiness.AddExchangeSite(site);
                            if (value <= 0)
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllCurrency()
        {
            return Json(_ExchangeCurrencyBusiness.getAllCurrency(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetDateCurrency(DateTime DateInit, DateTime DateFin, int Type, string siteCode)
        {
            if (Session["User"] != null)
            {
                var Currency = _ExchangeCurrencyBusiness.GetExchangeByDateAndType(DateInit, DateFin, Type, siteCode);
                if (Currency != null && Currency.Count > 0)
                {
                    string siteName = "Todas las Tiendas";
                    if (siteCode != "All")
                        siteName = _siteBusiness.GetSiteNameByCode(siteCode);
                    ExchangeCurrencyReport report = new ExchangeCurrencyReport();
                    report.DataSource = report.printTable(Currency, siteName);

                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);

                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetExchangeByIdSites(int exchange_id, int type)
        {
            if (Session["User"] != null)
                return Json(new { data = _ExchangeCurrencyBusiness.GetExchanceCurrencySites(exchange_id, type) }, JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetTypeExchangeCurrency(DateTime DateInit, DateTime DateFin, string site_code, int type)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _ExchangeCurrencyBusiness.GetExchangeByDateAndType(DateInit, DateFin, type, site_code) }, JsonRequestBehavior.AllowGet);

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }
    }
}