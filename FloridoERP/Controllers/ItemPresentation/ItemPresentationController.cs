﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL;
using App.BLL.ItemPresentationBusiness;
using App.BLL.MaCode;
using App.BLL.MaEmail;
using App.Common;
using App.Entities.ViewModels.ItemPresentation;

namespace FloridoERP.Controllers.ItemPresentation
{
    public class ItemPresentationController : Controller
    {
        private readonly ItemPresentationBusiness _presentations;
        private readonly ItemMasterBusiness _itemMasterBusiness;
        private readonly SendMail sendMail;
        private readonly Common common;
        private readonly MaEmailBusiness _maEmailBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;

        private readonly UserMasterBusiness _userMasterBusiness;
        public ItemPresentationController()
        {
            _presentations = new ItemPresentationBusiness();
            _itemMasterBusiness = new ItemMasterBusiness();
            sendMail = new SendMail();
            common = new Common();
            _maEmailBusiness = new MaEmailBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _maCodeBusiness = new MaCodeBusiness();
        }

        public JsonResult ActionGetPresentationByProductId(string part_number)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(_presentations.GetPresentationByProductId(part_number), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionAddPresentation(ItemPresentationModel model)
        {

            if (Session["User"] == null) return Json(Result.SF());
            var dateNow = DateTime.Now;
            var user = Session["User"].ToString();
            var r = _presentations.Add(model, Session["User"].ToString(), "PURCH005");
            if (r.Ok)
                if (dateNow.Date == model.appliedDate.Date)
                    MethodSendEmail(user, model, dateNow);
            return Json(r);
        }

        [HttpPost]
        public JsonResult ActionEditPresentation(ItemPresentationModel model)
        {
            if (Session["User"] == null) return Json(Result.SF());
            var dateNow = DateTime.Now;
            var user = Session["User"].ToString();
            var r = _presentations.Edit(model, Session["User"].ToString(), "PURCH005");
            if (r.Ok)
                if (dateNow.Date == model.appliedDate.Date)
                    MethodSendEmail(user, model, dateNow);
            return Json(r);
        }

        [HttpPost]
        public JsonResult ActionRemovePresentation(int id)
        {
            if (Session["User"] == null) return Json(Result.SF());
            var r = _presentations.Remove(id);
            return Json(r);
        }

        public ActionResult ActionGetPresentationById(string PresentationId)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _presentations.GetPresentationById(PresentationId) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPartNumberById(string part_number)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _itemMasterBusiness.GetInformationProduct(part_number) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        public int MethodSendEmail(string user, ItemPresentationModel model, DateTime date)
        {
            var emailUser = _userMasterBusiness.GetEmailByUser(user);
            var emailGerencia = _maEmailBusiness.GetEmailGenericList("gerencia");
            var emailMesaControl = _maEmailBusiness.GetEmailGenericList("mesa");
            var emailCajaGral = _maEmailBusiness.GetEmailGenericList("caja");
            var tableEmail = _presentations.GetTablePresentation(model);
            var EmailSystem = _maCodeBusiness.GetEmailsPriceChange();
            var email = emailMesaControl + "," + emailCajaGral + "," + (!string.IsNullOrWhiteSpace(EmailSystem) ? EmailSystem + "," : "") + (!string.IsNullOrWhiteSpace(emailUser) ? emailUser : "");
            var email2 = emailGerencia + "," + (!string.IsNullOrWhiteSpace(EmailSystem) ? EmailSystem + "," : "") + (!string.IsNullOrWhiteSpace(emailUser) ? emailUser : "");

            sendMail.subject = "CAMBIOS DE PRECIO DEL DÍA DE HOY ("+ date.Date.ToString("dd/MM/yyyy") + "): ACTUALIZAR PUNTOS DE VENTA, BÁSCULAS Y CENEFAS";
            sendMail.from = "FLORIDO GLOBAL";
            sendMail.email = email;
            var resultEmail = common.MailMessageChangePrice(sendMail, tableEmail);
            sendMail.email = email2;
            var resultEmail2 = common.MailMessageChangePrice(sendMail, tableEmail);

                return 1;
        }
    }
}