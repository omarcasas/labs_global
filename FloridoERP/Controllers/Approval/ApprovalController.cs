﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Common;

namespace FloridoERP.Controllers
{
    public class AprovalController : Controller
    {
        public JsonResult ActionGetProcessList()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(new {}, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionProcessStart()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionProcessUpdate()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
    }
}