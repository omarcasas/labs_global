﻿using App.BLL.InternalRequirement;
using App.BLL.Site;
using App.BLL.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.InternalRequirement;
using App.Common;
using FloridoERP.Report;
using App.BLL.Purchases;
using App.BLL.PurchaseOrder;
using App.Entities.ViewModels.Creditors;
using System.IO;
using System.Threading.Tasks;
using App.BLL.MaEmail;
using App.BLL;

namespace FloridoERP.Controllers.InternalRequirement
{
    public class InternalRequirementHeaderController : Controller
    {
        private readonly InternalRequirementBusiness _internalRequirementBusiness;
        private readonly PurchaseOrderBusiness _purchaseBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SupplierContactBusiness _supplierContactBusiness;
        private readonly SendMail information;
        private readonly Common common;
        private readonly MaEmailBusiness _maEmailBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;

        public InternalRequirementHeaderController()
        {
            _internalRequirementBusiness = new InternalRequirementBusiness();
            _siteBusiness = new SiteBusiness();
            _supplierBusiness = new SupplierBusiness();
            _purchaseBusiness = new PurchaseOrderBusiness();
            _supplierContactBusiness = new SupplierContactBusiness();
            information = new SendMail();
            common = new Common();
            _maEmailBusiness = new MaEmailBusiness();
            _userMasterBusiness = new UserMasterBusiness();
        }

        [HttpGet]
        public ActionResult ActionGetRequirementsAll()
        {
            if (Session["User"] != null)
            {
                var folios = _internalRequirementBusiness.GetRequirementsAll();

                if (folios != null)
                    return Json(new { success = true, folios }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetSentRequirementsByDates(DateTime BeginDate, DateTime EndDate)
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return Json(new { Status = true, SentOrders = _internalRequirementBusiness.GetSentRequirementsByDates(BeginDate, EndDate, BuyerDivision) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddInternalRequirement(List<InternalRequirementModel> items)
        {
            string site = "0025";
            var resultado = _internalRequirementBusiness.AddInternalRequierement(items, Session["User"].ToString(), site);

            if (resultado)
                return Json(new { success = "Correcto" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = "Error" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddInternalRequirementSF(List<InternalRequirementModel> items, string site, string user)
        {
            var resultado = _internalRequirementBusiness.AddInternalRequierement(items, user, site);

            if (resultado)
                return new JsonResult() { Data = "Correcto", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return new JsonResult() { Data = "Error", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionGetDetailsBySuplier(int supplier_id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _internalRequirementBusiness.GetRequirementsDetailsBySupplier(supplier_id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetSitesName()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _siteBusiness.GetAllSitesName(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetAllRequirementsHeaderBySite(string site)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsHeaderBySite(site), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetAllRequirementsHeaderBySiteSF(string site)
        {
            return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsHeaderBySite(site), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionGetAllRequirementsDetailsSF(string folio)
        {
            return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsDetails(folio), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionGetAllRequirementsDetails(string folio)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsDetails(folio), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetAllItemsCreditors()
        {
            return new JsonResult() { Data = _internalRequirementBusiness.getAllItemsCreditors(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetRequirementsHeaderAll()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return Json(new { Data = _internalRequirementBusiness.GetRequirementsHeaderAll(BuyerDivision), SentOrders = _internalRequirementBusiness.GetSentRequirements(BuyerDivision) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetRequirementsAllSites()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                var folios = _internalRequirementBusiness.GetRequirementsAllSites(BuyerDivision);
                var SentOrders = _internalRequirementBusiness.GetSentRequirements(BuyerDivision);

                if (folios.Count() > 0)
                    return Json(new { success = true, folios, SentOrders }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, responseText = folios, SentOrders }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetRequirementDetailsBySuplier(int supplier_id)
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return new JsonResult() { Data = _internalRequirementBusiness.GetRequirementDetailsBySupplier(supplier_id, BuyerDivision), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetRequirementsDetailsAll(string folio)
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return Json(new { Data = _internalRequirementBusiness.GetRequirementsDetailsAll(folio, BuyerDivision) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetItemsCreditorsAll()
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return Json(new { Data = _internalRequirementBusiness.getItemsCreditorsAll(BuyerDivision), JsonRequestBehavior.AllowGet });
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetItemsCreditorsByFolio(int folio)
        {
            if (Session["User"] != null)
            {
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                return Json(new { Data = _internalRequirementBusiness.getItemsCreditorsByFolio(folio, BuyerDivision), JsonRequestBehavior.AllowGet });
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionUpdateItemsRequirement(List<InternalRequirementModel> items, int folio)
        {
            if (Session["User"] != null)
            {
                string uuser = HttpContext.User.Identity.Name;
                var result = _internalRequirementBusiness.UpdateItemsRequirement(items, uuser, folio);
                return Json(new { success = true, responseText = result.ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionDeleteFolio(int folio)
        {
            if (Session["User"] != null)
            {
                string uuser = HttpContext.User.Identity.Name;
                var result = _internalRequirementBusiness.DeleteInternalHeaderFolio(folio, uuser);
                return Json(new { success = true, responseText = result.ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionRelationStoreSupplier()
        {
            if (Session["User"] != null)
            {
                var sites = _internalRequirementBusiness.GetRequirementsAllStatus0();
                var creditors = _internalRequirementBusiness.GetRequirementsAllCreditors();
                List<AssociationCreditorsSites> listAssociation = new List<AssociationCreditorsSites>();
                bool boolCreditor = true;
                string BuyerDivision = _userMasterBusiness.GetBuyerDivision(Session["User"].ToString());
                string creditorNames = "";
                foreach (var c in creditors)
                {
                    foreach (var s in sites)
                    {
                        var items = _internalRequirementBusiness.GetRequirementsDetailsBySupplierSite(c.id_supplier, s.site_code, BuyerDivision);
                        if (items.Count() > 0)
                        {
                            //Realiza la relación para ser más sencilla
                            listAssociation.Add(new AssociationCreditorsSites() { site_code = s.site_code, supplier_id = c.id_supplier });

                            //Comprobar si hay contacto en ventas
                            var creditor = _supplierContactBusiness.GetContactFirstByDeparment(c.id_supplier, "Ventas");
                            if (creditor == null)
                            {
                                if (boolCreditor)
                                    creditorNames += _supplierBusiness.GetNameSupplier(c.id_supplier);
                                else
                                    creditorNames += ", " + _supplierBusiness.GetNameSupplier(c.id_supplier);

                                boolCreditor = false;
                            }
                        }
                    }
                }
                var listItems = _internalRequirementBusiness.GetRequirementsRelationItems();
                return Json(new { success = true, listItems, listAssociation, responseText = creditorNames }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSendEmailCreditors(List<AssociationCreditorsSites> listRelation, List<AllItemSitesSupplier> listItems, List<int> listFolios)
        {
            if (Session["User"] != null || HttpContext.User.Identity.Name != "")
            {
                List<ModelR> pdfs = new List<ModelR>();
                List<int> folios = new List<int>();
                MemoryStream stream = new MemoryStream();
                string uuser = HttpContext.User.Identity.Name;
                int purchase_no = 0, purchase_no_detail = 0;
                var firstElement = listRelation.First();
                var itemLast = listRelation.Count - 1;
                decimal taxSupplier = 0;
                int count = 0;
                bool passValue = false;

                //Delimitar los productos por los folios
                var listItemsNew = listItems.Where(w => listFolios.Contains(w.folio));

                //Actualizar folios
                foreach (var folio in listFolios)
                {
                    int updateFolio = _internalRequirementBusiness.SendInternalHeaderFolio(folio, uuser);

                    if (updateFolio < 1)
                        return Json(new { success = false, error = "3", responseText = folio }, JsonRequestBehavior.AllowGet);
                }

                //Actualizar antes de enviar folios.
                foreach (var list in listRelation)
                {
                    //Crear orden de compra e ingresar cada uno de los productos en purchase_order_item
                    var items = listItemsNew.Where(x => x.site_code == list.site_code && x.supplier_id == list.supplier_id).ToList();

                    if (items.Count > 0)
                    {
                        //Agregar al purchase Order
                        purchase_no = _purchaseBusiness.CreatePuchaseOrder(items[0].folio, list.supplier_id, list.site_code, uuser);
                        if (purchase_no > 0)
                            purchase_no_detail = _purchaseBusiness.CreatePurchaseOrderItems(purchase_no, items, list.supplier_id, uuser);
                        else
                            return Json(new { success = false, error = "2", responseText = items[0].folio.ToString() + " de la tienda: " + items[0].site_name }, JsonRequestBehavior.AllowGet);
                    }
                }
                //Busqueda del proveedor-------------------------------
                var creditor = _supplierContactBusiness.GetContactFirstByDeparment(firstElement.supplier_id, "Ventas");
                var alleEmails = _supplierContactBusiness.GetContactAllByDeparment(firstElement.supplier_id, "Ventas");
                var uuserEmail = _userMasterBusiness.GetEmailByUser(uuser);
                Task task = new Task(() =>
                {
                    foreach (var list in listRelation)
                    {
                        //Verifica la existencia de items
                        var nextItems = listItemsNew.Where(x => x.site_code == list.site_code && x.supplier_id == list.supplier_id).ToList();

                        if (nextItems.Count() > 0)
                        {
                            //Solo entrará cuando el proveedor sean diferentes o sea el ultimo proveedor (madará correo)
                            if (firstElement.supplier_id != list.supplier_id)
                            {
                                if (creditor != null)
                                {
                                    passValue = true;
                                    //Mandar correo al proveedor anterior
                                    information.store = creditor.CreditorNameBusiness;
                                    information.subject = "Orden de Compra de parte del Florido";
                                    information.from = "FLORIDO GLOBAL";
                                    information.email = alleEmails + "," + uuserEmail; //"mpasillas@elflorido.com.mx";
                                    var pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                    if (pdfSupplier.Count() <= 0)
                                    {
                                        //Selecciónar productos
                                        var itemsSupplier = listItemsNew.Where(x => x.site_code == firstElement.site_code && x.supplier_id == firstElement.supplier_id).ToList();

                                        //Buscar el iva del proveedor
                                        taxSupplier = _supplierBusiness.GetValueSupplierPartIva(firstElement.supplier_id);

                                        //Agregar PDF'S
                                        pdfs.Add(new ModelR() { CPOModel = creditor, id_supplier = firstElement.supplier_id, items = itemsSupplier, purchase_no = purchase_no, User = uuser, site_code = list.site_code, folio = itemsSupplier[0].folio, taxSupplierValue = taxSupplier });

                                        //Seleccionar
                                        pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                        //Enviar correo
                                        common.MailMessagePOCreditor(information, pdfSupplier);
                                    }
                                    else
                                        common.MailMessagePOCreditor(information, pdfSupplier);

                                    firstElement.supplier_id = list.supplier_id;
                                    firstElement.site_code = list.site_code;
                                }

                                //Agrupar la información del actual proveedor (acreedor)
                                if (count != itemLast)
                                {
                                    creditor = _supplierContactBusiness.GetContactFirstByDeparment(list.supplier_id, "Ventas");
                                    alleEmails = _supplierContactBusiness.GetContactAllByDeparment(list.supplier_id, "Ventas");
                                }
                            }

                            //Crear orden de compra e ingresar cada uno de los productos en purchase_order_item
                            var items = listItemsNew.Where(x => x.site_code == list.site_code && x.supplier_id == list.supplier_id).ToList();

                            //Este bloque de codigo fue sacado por los nuevos metodos TASK
                            //////Agregar al purchase Order
                            ////purchase_no = _purchaseBusiness.CreatePuchaseOrder(items[0].folio, list.supplier_id, list.site_code, uuser);
                            ////if (purchase_no > 0)
                            ////    purchase_no_detail = _purchaseBusiness.CreatePurchaseOrderItems(purchase_no, items, uuser);
                            ////else
                            ////    return Json(new { success = false, error = "2", responseText = items[0].folio.ToString() + " de la tienda: " + items[0].site_name }, JsonRequestBehavior.AllowGet);

                            //////Para no volver a actulizar el mismo códigp
                            ////if (!folios.Contains(items[0].folio))
                            ////{
                            ////    int updateFolio = _internalRequirementBusiness.SendInternalHeaderFolio(items[0].folio, uuser);
                            ////    if (updateFolio < 1)
                            ////        return Json(new { success = false, error = "3", responseText = items[0].folio.ToString() + " de la tienda: " + items[0].site_name }, JsonRequestBehavior.AllowGet);
                            ////    folios.Add(items[0].folio);
                            ////}

                            if (creditor != null)
                            {
                                //Agrega el PDF
                                pdfs.Add(new ModelR() { CPOModel = creditor, id_supplier = list.supplier_id, items = items, purchase_no = purchase_no, User = uuser, site_code = list.site_code, folio = items[0].folio });
                            }

                            //Mandar correo al ultimo proveedor
                            if (count == itemLast)
                            {
                                if (creditor != null)
                                {
                                    passValue = true;
                                    information.store = creditor.CreditorNameBusiness;
                                    information.subject = "Orden de Compra de parte del Florido";
                                    information.from = "FLORIDO GLOBAL";
                                    information.email = alleEmails + "," + uuserEmail;//"mpasillas@elflorido.com.mx";
                                    var pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                    if (pdfSupplier.Count() <= 0)
                                    {
                                        //Selecciónar productos
                                        var itemsSupplier = listItemsNew.Where(x => x.site_code == firstElement.site_code && x.supplier_id == firstElement.supplier_id).ToList();

                                        //Buscar el iva del proveedor
                                        taxSupplier = _supplierBusiness.GetValueSupplierPartIva(firstElement.supplier_id);

                                        //Agregar PDF'S
                                        pdfs.Add(new ModelR() { CPOModel = creditor, id_supplier = firstElement.supplier_id, items = itemsSupplier, purchase_no = purchase_no, User = uuser, site_code = list.site_code, folio = itemsSupplier[0].folio, taxSupplierValue = taxSupplier });

                                        //Seleccionar
                                        pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                        //Enviar correo
                                        common.MailMessagePOCreditor(information, pdfSupplier);
                                    }
                                    else
                                        common.MailMessagePOCreditor(information, pdfSupplier);

                                    firstElement.supplier_id = list.supplier_id;
                                    firstElement.site_code = list.site_code;
                                }
                            }
                        }
                        //contador para saber cuando sera el ultimo
                        count++;

                        if (!passValue)
                        {
                            //Solo entrará cuando el proveedor sean diferentes o sea el ultimo proveedor (madará correo)
                            if (firstElement.supplier_id != list.supplier_id)
                            {
                                if (creditor != null)
                                {
                                    //Mandar correo al proveedor anterior
                                    information.store = creditor.CreditorNameBusiness;
                                    information.subject = "Orden de Compra de parte del Florido";
                                    information.from = "FLORIDO GLOBAL";
                                    information.email = alleEmails + "," + uuserEmail; //"mpasillas@elflorido.com.mx";
                                    var pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                    if (pdfSupplier.Count() <= 0)
                                    {
                                        //Selecciónar productos
                                        var itemsSupplier = listItemsNew.Where(x => x.site_code == firstElement.site_code && x.supplier_id == firstElement.supplier_id).ToList();

                                        if (itemsSupplier != null)
                                        {
                                            //Buscar el iva del proveedor
                                            taxSupplier = _supplierBusiness.GetValueSupplierPartIva(firstElement.supplier_id);

                                            //Agregar PDF'S
                                            pdfs.Add(new ModelR() { CPOModel = creditor, id_supplier = firstElement.supplier_id, items = itemsSupplier, purchase_no = purchase_no, User = uuser, site_code = list.site_code, folio = itemsSupplier[0].folio, taxSupplierValue = taxSupplier });

                                            //Seleccionar
                                            pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                            //Enviar correo
                                            common.MailMessagePOCreditor(information, pdfSupplier);
                                        }
                                    }
                                    else
                                        common.MailMessagePOCreditor(information, pdfSupplier);

                                    firstElement.supplier_id = list.supplier_id;
                                    firstElement.site_code = list.site_code;
                                }

                                //Agrupar la información del actual proveedor (acreedor)
                                if (count != itemLast)
                                {
                                    creditor = _supplierContactBusiness.GetContactFirstByDeparment(list.supplier_id, "Ventas");
                                    alleEmails = _supplierContactBusiness.GetContactAllByDeparment(list.supplier_id, "Ventas");
                                }
                            }

                            //Mandar correo al ultimo proveedor
                            if (count == itemLast)
                            {
                                if (creditor != null)
                                {
                                    information.store = creditor.CreditorNameBusiness;
                                    information.subject = "Orden de Compra de parte del Florido - REQUISICIONES INTERNAS";
                                    information.from = "FLORIDO GLOBAL";
                                    information.email = alleEmails + "," + uuserEmail;//"mpasillas@elflorido.com.mx";
                                    var pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                    if (pdfSupplier.Count() <= 0)
                                    {
                                        //Selecciónar productos
                                        var itemsSupplier = listItemsNew.Where(x => x.site_code == firstElement.site_code && x.supplier_id == firstElement.supplier_id).ToList();

                                        if (itemsSupplier != null)
                                        {
                                            //Buscar el iva del proveedor
                                            taxSupplier = _supplierBusiness.GetValueSupplierPartIva(firstElement.supplier_id);

                                            //Agregar PDF'S
                                            pdfs.Add(new ModelR() { CPOModel = creditor, id_supplier = firstElement.supplier_id, items = itemsSupplier, purchase_no = purchase_no, User = uuser, site_code = list.site_code, folio = itemsSupplier[0].folio, taxSupplierValue = taxSupplier });

                                            //Seleccionar
                                            pdfSupplier = pdfs.Where(x => x.id_supplier == firstElement.supplier_id).Select(s => new EmailPORequirements { Reporte = s.Reporte, site_code = s.site_code }).ToList();

                                            //Enviar correo
                                            common.MailMessagePOCreditor(information, pdfSupplier);
                                        }
                                    }
                                    else
                                        common.MailMessagePOCreditor(information, pdfSupplier);

                                    firstElement.supplier_id = list.supplier_id;
                                    firstElement.site_code = list.site_code;
                                }
                            }
                        }

                        passValue = false;
                    }
                    //Enviar al sitio todos los PDF
                    var listSiteDistinct = pdfs.Select(x => x.site_code).Distinct().OrderBy(x => x).ToList();
                    foreach (var l in listSiteDistinct)
                    {
                        var storeEmail = _maEmailBusiness.GetMaEmail(l, "Gerencia");
                        var storeMesaC = _maEmailBusiness.GetMaEmail(l, "Mesa");
                        var storeRecibo = _maEmailBusiness.GetMaEmail(l, "Recibo");
                        var store = _siteBusiness.GetSiteAllInformation(l);
                        information.store = store.SiteName;
                        information.subject = "Orden de Compra Confirmadas - Requisiciones Internas";
                        information.from = "FLORIDO GLOBAL";
                        information.email = storeEmail+","+ storeMesaC+","+storeRecibo+","+ uuserEmail;// o tambien store.SiteEmail; ////<--mandara al correo del sitio de la tienda "mpasillas@elflorido.com.mx"; 
                        var pdf = pdfs.Where(x => x.site_code == l).Select(x => new EmailPORequirements { Reporte = x.Reporte, site_code = x.site_code }).ToList();
                        if (pdf.Count() > 0)
                            common.MailMessagePOStore(information, pdf);
                    }
                });
                task.Start();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, error = "SF" }, JsonRequestBehavior.AllowGet);
        }

        //No es posible generar esta clase en ViewModels por el Reporte no se puede importar.
        //Y si se remueve el reporte de clase, muestra como inaccesible y no se puede enviar el reporte.
        //Por eso se encuentra aqui.
        public class ModelR
        {
            public Stream Reporte
            {
                get
                {
                    MemoryStream stream = new MemoryStream();
                    PurchasesOrderRequirement report = new PurchasesOrderRequirement();
                    report.DataSource = report.printTable(folio, purchase_no, User, items, CPOModel, taxSupplierValue);
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                    return stream;
                }
            }
            public string site_code { get; set; }
            public int id_supplier { get; set; }
            public int purchase_no { get; set; }
            public string site_name { get; set; }
            public CreditorPOModel CPOModel { get; set; }
            public string User { get; set; }
            public List<AllItemSitesSupplier> items { get; set; }
            public int folio { get; set; }
            public decimal taxSupplierValue { get; set; }
        }
    }
}