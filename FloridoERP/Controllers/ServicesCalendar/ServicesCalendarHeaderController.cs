﻿using App.BLL.MaEmail;
using App.BLL.ServicesCalendar;
using App.BLL.Supplier;
using App.Common;
using App.Entities;
using App.Entities.ViewModels.ServicesCalendar;
using DevExpress.XtraReports.UI;
using FloridoERP.Report;
using FloridoERP.ViewModels.MRO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace FloridoERP.Controllers.ServicesCalendar
{
    public class ServicesCalendarHeaderController : Controller
    {
        private readonly ServicesCalendarHeaderBusiness _servicesCalendarHeaderBusiness;
        private readonly ServicesCalendarDetailBusiness _servicesCalendarDetailBusiness;
        private readonly SupplierContactBusiness _supplierContactBusiness;
        private readonly MaEmailBusiness _maEmailBusiness;
        private readonly SendMail sendMail;
        private readonly Common common;

        public ServicesCalendarHeaderController()
        {
            _servicesCalendarHeaderBusiness = new ServicesCalendarHeaderBusiness();
            _servicesCalendarDetailBusiness = new ServicesCalendarDetailBusiness();
            _maEmailBusiness = new MaEmailBusiness();
            _supplierContactBusiness = new SupplierContactBusiness();
            sendMail = new SendMail();
            common = new Common();
        }

        [HttpPost]
        public ActionResult ActionGetServicesHeaderByDate(DateTime start, DateTime finish, string site)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _servicesCalendarHeaderBusiness.GetServicesHeaderByDate(site, start, finish), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetServicesHeaderByDateAndStatus(DateTime start, DateTime finish, int status, string site)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _servicesCalendarHeaderBusiness.GetServicesHeaderByDateAndStatus(site, start, finish, status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetServicesHeaderByDateAndStatusNotSite(DateTime start, DateTime finish, int status)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _servicesCalendarHeaderBusiness.GetServicesHeaderByDateAndStatusNotSite(start, finish, status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetServicesHeaderByDateNotSite(DateTime start, DateTime finish)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _servicesCalendarHeaderBusiness.GetServicesHeaderByDateNotSite(start, finish), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetServicesDetail(int id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _servicesCalendarHeaderBusiness.GetServicesDetail(id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionPrintService(int id)
        {
            if (Session["User"] != null)
            {
                MROReport report = new MROReport();

                (report.Bands["DetailReport1"] as DetailReportBand).DataSource = report.printTable(_servicesCalendarHeaderBusiness.GetServicesId(id), _servicesCalendarHeaderBusiness.GetServicesDetail(id));
                (report.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTable1";

                var stream = new MemoryStream();
                report.ExportToPdf(stream);
                return Json(new { responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionAddService(string ServiceDate, string ServiceSite, string ServiceSiteName, List<ServiceCalendarModel> Services)
        {
            if (Session["User"] != null)
            {
                var serviceExists = _servicesCalendarHeaderBusiness.GetServiceCalendarHeaderId(ServiceDate, ServiceSite);
                if (serviceExists != 0)
                {
                    foreach (var item in Services)
                    {
                        SERVICES_CALENDAR_DETAIL service = new SERVICES_CALENDAR_DETAIL
                        {
                            service_id = serviceExists,
                            equipment_id = int.Parse(item.equipment_id),
                            supplier_id = int.Parse(item.supplier_id),
                            service_cost = decimal.Parse(item.service_cost),
                            service_department = item.service_department,
                            currency = item.currency
                        };
                        _servicesCalendarDetailBusiness.AddServicesCalendarDetail(service, item.quotation_document, Session["User"].ToString());
                    }
                    //regresa Tupla con lista de details y lista de ids proveedores
                    var servicesTable = _servicesCalendarDetailBusiness.GetServicesDetailByServiceId(serviceExists);
                    if (servicesTable.Item1 != null && servicesTable.Item2 != null)
                    {
                        //reporte general para gerente
                        ServiceCalendarReport reporte = new ServiceCalendarReport();
                        reporte.DataSource = reporte.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, servicesTable.Item1);
                        Stream stream = new MemoryStream();
                        reporte.ExportToPdf(stream);
                        sendMail.title = "";
                        sendMail.date = ServiceDate;
                        sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                        sendMail.from = "El Florido, Departamento de Mantenimiento.";
                        sendMail.email = _maEmailBusiness.GetMaEmail(ServiceSite, "gerencia");
                        common.MailMessageServicesCalendar(sendMail, stream);
                        //recorre lista de ids de proveedores
                        foreach (var item in servicesTable.Item2)
                        {
                            //filtra lista de detalle por proveedor
                            var detailsSupplier = _servicesCalendarDetailBusiness.FilterServicesBySupplierId(servicesTable.Item1, item);
                            //reporte por proveedor
                            ServiceCalendarToSupplierReport reportSup = new ServiceCalendarToSupplierReport();
                            reportSup.DataSource = reportSup.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, detailsSupplier);
                            Stream streams = new MemoryStream();
                            reportSup.ExportToPdf(streams);
                            string email;
                            var contact = _supplierContactBusiness.GetContactFirstByDeparment(detailsSupplier[0].supplier_id, "Compras");
                            if (contact != null)
                                email = contact.CreditorEmail;
                            else
                                email = "";
                            sendMail.title = "";
                            sendMail.date = ServiceDate;
                            sendMail.store = ServiceSiteName;
                            sendMail.supplier = detailsSupplier[0].supplier_name;
                            sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                            sendMail.from = "El Florido, Departamento de Mantenimiento.";
                            sendMail.email = email;
                            common.MailMessageServicesCalendartoSupplier(sendMail, streams);
                        }
                        return Json(new { success = true, message = "Servicio Generado Correctamente, se envio notificacion a Gerencia y a Proveedores." }, JsonRequestBehavior.AllowGet);
                    }
                    else if (servicesTable.Item1 != null && servicesTable.Item2 == null)
                    {
                        //reporte general para gerente
                        ServiceCalendarReport reporte = new ServiceCalendarReport();
                        reporte.DataSource = reporte.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, servicesTable.Item1);
                        Stream stream = new MemoryStream();
                        reporte.ExportToPdf(stream);
                        sendMail.title = "";
                        sendMail.date = ServiceDate;
                        sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                        sendMail.from = "El Florido, Departamento de Mantenimiento.";
                        sendMail.email = _maEmailBusiness.GetMaEmail(ServiceSite, "gerencia");
                        common.MailMessageServicesCalendar(sendMail, stream);
                        return Json(new { success = true, message = "Servicio Generado Correctamente, no fue posible enviar correo a Proveedores." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = true, message = "Servicio Generado Correctamente, no fue posible enviar correo a Proveedores ni Gerencia." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var addService = _servicesCalendarHeaderBusiness.AddServicesCalendar(ServiceDate, ServiceSite, Session["User"].ToString());
                    if (addService != 0)
                    {
                        var idService = addService;
                        foreach (var item in Services)
                        {
                            SERVICES_CALENDAR_DETAIL service = new SERVICES_CALENDAR_DETAIL
                            {
                                service_id = idService,
                                equipment_id = int.Parse(item.equipment_id),
                                supplier_id = int.Parse(item.supplier_id),
                                service_cost = decimal.Parse(item.service_cost),
                                service_department = item.service_department,
                                currency = item.currency
                            };
                            _servicesCalendarDetailBusiness.AddServicesCalendarDetail(service, item.quotation_document, Session["User"].ToString());
                        }
                        //regresa Tupla con lista de details y lista de ids proveedores
                        var servicesTable = _servicesCalendarDetailBusiness.GetServicesDetailByServiceId(idService);
                        if (servicesTable.Item1 != null && servicesTable.Item2 != null)
                        {
                            //reporte general para gerente
                            ServiceCalendarReport reporte = new ServiceCalendarReport();
                            reporte.DataSource = reporte.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, servicesTable.Item1);
                            Stream stream = new MemoryStream();
                            reporte.ExportToPdf(stream);
                            sendMail.title = "";
                            sendMail.date = ServiceDate;
                            sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                            sendMail.from = "El Florido, Departamento de Mantenimiento.";
                            sendMail.email = _maEmailBusiness.GetMaEmail(ServiceSite, "gerencia");
                            common.MailMessageServicesCalendar(sendMail, stream);
                            //recorre lista de ids de proveedores
                            foreach (var item in servicesTable.Item2)
                            {
                                //filtra lista de detalle por proveedor
                                var detailsSupplier = _servicesCalendarDetailBusiness.FilterServicesBySupplierId(servicesTable.Item1, item);
                                //reporte por proveedor
                                ServiceCalendarToSupplierReport reportSup = new ServiceCalendarToSupplierReport();
                                reportSup.DataSource = reportSup.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, detailsSupplier);
                                Stream streams = new MemoryStream();
                                reportSup.ExportToPdf(streams);
                                string email;
                                var contact = _supplierContactBusiness.GetContactFirstByDeparment(detailsSupplier[0].supplier_id, "Compras");
                                if (contact != null)
                                    email = contact.CreditorEmail;
                                else
                                    email = "";
                                sendMail.title = "";
                                sendMail.date = ServiceDate;
                                sendMail.store = ServiceSiteName;
                                sendMail.supplier = detailsSupplier[0].supplier_name;
                                sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                                sendMail.from = "El Florido, Departamento de Mantenimiento.";
                                sendMail.email = email;
                                common.MailMessageServicesCalendartoSupplier(sendMail, streams);
                            }
                            return Json(new { success = true, message = "Servicio Generado Correctamente, se envio notificacion a Gerencia y a Proveedores." }, JsonRequestBehavior.AllowGet);
                        }
                        else if (servicesTable.Item1 != null && servicesTable.Item2 == null)
                        {
                            //reporte general para gerente
                            ServiceCalendarReport reporte = new ServiceCalendarReport();
                            reporte.DataSource = reporte.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, servicesTable.Item1);
                            Stream stream = new MemoryStream();
                            reporte.ExportToPdf(stream);
                            sendMail.title = "";
                            sendMail.date = ServiceDate;
                            sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                            sendMail.from = "El Florido, Departamento de Mantenimiento.";
                            sendMail.email = _maEmailBusiness.GetMaEmail(ServiceSite, "gerencia");
                            common.MailMessageServicesCalendar(sendMail, stream);
                            return Json(new { success = true, message = "Servicio Generado Correctamente, no fue posible enviar correo a Proveedores." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = true, message = "Servicio Generado Correctamente, no fue posible enviar correo a Proveedores ni Gerencia." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "Error al guardar servicio. Contacte a Sistemas." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, message = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddXML(int status, int service_id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _servicesCalendarHeaderBusiness.ModifyServicesDetail(Session["User"].ToString(), status, service_id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionGetServicesCalendarDetail(int ServiceId)
        {
            if (Session["User"] != null)
                return Json(new { response = true, model = _servicesCalendarHeaderBusiness.ActionGetServicesCalendarDetail(ServiceId) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { response = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateService(int ServiceId, string ServiceDate, string ServiceSite, string ServiceSiteName, List<ServicesCalendarDetailModel> Services)
        {
            if (Session["User"] != null)
            {
                if (_servicesCalendarHeaderBusiness.UpdateServicesCalendar(ServiceId, ServiceDate, ServiceSite, Session["User"].ToString(), Services))
                {
                    //regresa Tupla con lista de details y lista de ids proveedores
                    var servicesTable = _servicesCalendarDetailBusiness.GetServicesDetailByServiceId(ServiceId);
                    if (servicesTable.Item1 != null && servicesTable.Item2 != null)
                    {
                        //reporte general para gerente
                        ServiceCalendarReport reporte = new ServiceCalendarReport();
                        reporte.DataSource = reporte.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, servicesTable.Item1);
                        Stream stream = new MemoryStream();
                        reporte.ExportToPdf(stream);
                        sendMail.title = "";
                        sendMail.date = ServiceDate;
                        sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                        sendMail.from = "El Florido, Departamento de Mantenimiento.";
                        sendMail.email = _maEmailBusiness.GetMaEmail(ServiceSite, "gerencia");
                        common.MailMessageServicesCalendar(sendMail, stream);
                        //recorre lista de ids de proveedores
                        foreach (var item in servicesTable.Item2)
                        {
                            //filtra lista de detalle por proveedor
                            var detailsSupplier = _servicesCalendarDetailBusiness.FilterServicesBySupplierId(servicesTable.Item1, item);
                            //reporte por proveedor
                            ServiceCalendarToSupplierReport reportSup = new ServiceCalendarToSupplierReport();
                            reportSup.DataSource = reportSup.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, detailsSupplier);
                            Stream streams = new MemoryStream();
                            reportSup.ExportToPdf(streams);
                            string email;
                            var contact = _supplierContactBusiness.GetContactFirstByDeparment(detailsSupplier[0].supplier_id, "Compras");
                            if (contact != null)
                                email = contact.CreditorEmail;
                            else
                                email = "";
                            sendMail.title = "";
                            sendMail.date = ServiceDate;
                            sendMail.store = ServiceSiteName;
                            sendMail.supplier = detailsSupplier[0].supplier_name;
                            sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                            sendMail.from = "El Florido, Departamento de Mantenimiento.";
                            sendMail.email = email;
                            common.MailMessageServicesCalendartoSupplier(sendMail, streams);
                        }

                        return Json(new { success = true, message = "Servicio Generado Correctamente, se envio notificacion a Gerencia y a Proveedores." }, JsonRequestBehavior.AllowGet);
                    }
                    else if (servicesTable.Item1 != null && servicesTable.Item2 == null)
                    {
                        //reporte general para gerente
                        ServiceCalendarReport reporte = new ServiceCalendarReport();
                        reporte.DataSource = reporte.printTable(ServiceSiteName, Session["Name"].ToString(), ServiceDate, servicesTable.Item1);
                        Stream stream = new MemoryStream();
                        reporte.ExportToPdf(stream);
                        sendMail.title = "";
                        sendMail.date = ServiceDate;
                        sendMail.subject = "Servicio Programado para Sucursal: " + ServiceSiteName;
                        sendMail.from = "El Florido, Departamento de Mantenimiento.";
                        sendMail.email = _maEmailBusiness.GetMaEmail(ServiceSite, "gerencia");
                        common.MailMessageServicesCalendar(sendMail, stream);
                        return Json(new { success = true, message = "Servicio Generado Correctamente, no fue posible enviar correo a Proveedores." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = true, message = "Servicio Generado Correctamente, no fue posible enviar correo a Proveedores ni Gerencia." }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Alerta - Error inesperado  contactar a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
        }
    }
}