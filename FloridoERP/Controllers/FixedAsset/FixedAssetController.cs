﻿using App.BLL.CommonBusiness;
using App.BLL.ExchangeCurrency;
using App.BLL.FixedAsset;
using App.BLL.Purchases;
using App.BLL.Site;
using App.DAL.FixedAsset;
using App.Entities;
using FloridoERP.ViewModels.FixedAsset;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;

namespace FloridoERP.Controllers.FixedAsset
{
    public class FixedAssetController : Controller
    {
        private FixedAssetBusiness _fixedAsset;
        private FixedAssetRequisitionBusiness _fixedAssetRequisition;
        private FixedAssetItemBusiness _fixedAssetItem;
        private SysMaCodeBusiness _maCode;
        private SupplierBusiness _supllier;
        private SiteBusiness _sites;
        private ExchangeCurrencyBusiness _exchangeCurrency;
        private FixedAssetPurchaseBusiness _fixedPurchase;
        private FixedAssetTransferHeaderBusiness _fixedTransferHeader;
        private FixedAssetTransferDetailBusiness _fixedTransferDetail;


        public FixedAssetController()
        {
            _fixedAsset = new FixedAssetBusiness();
            _fixedAssetRequisition = new FixedAssetRequisitionBusiness();
            _fixedAssetItem = new FixedAssetItemBusiness();
            _maCode = new SysMaCodeBusiness();
            _supllier = new SupplierBusiness();
            _sites = new SiteBusiness();
            _exchangeCurrency = new ExchangeCurrencyBusiness();
            _fixedPurchase = new FixedAssetPurchaseBusiness();
            _fixedTransferHeader = new FixedAssetTransferHeaderBusiness();
            _fixedTransferDetail = new FixedAssetTransferDetailBusiness();
        }

        #region Pages
        public ActionResult FXD001(FixedAssetViewModel model)
        {
            if (Session["User"] != null)
            {
                model.ListFixedAsset = _fixedAsset.GetAllFixedAsset();
                model.ListCategory = _maCode.GetAllFixedAsset();
                model.ListDepreciation = _maCode.GetAllFixedAssetDepreciation();
                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult AFX001(FixedAssetRequisitionViewModel model)
        {
            if (Session["User"] != null)
            {
                model.ListPendinRequisition = _fixedAssetRequisition.GetPendingRequisitionAsset();
                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }


        public ActionResult RFX001(FixedAssetRegistrationViewModel model)
        {
            if (Session["User"] != null)
            {

                model.Sites = _sites.GetAllSitesModel();
                model.FixedAsset = _fixedAsset.GetAllFixedAsset();
                model.Currency = _maCode.GetAllCurrency();
                model.Supplier = _supllier.GetAllOnlySuppliers();

                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }


        public ActionResult RFX002(FixedAssetRegistrationViewModel model)
        {
            if (Session["User"] != null)
            {

                model.Sites = _sites.GetAllSitesModel();

                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }


        public ActionResult TFX001(FixedAssetRegistrationViewModel model)
        {
            if (Session["User"] != null)
            {

                model.Sites = _sites.GetAllSitesModel();

                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }

        public ActionResult TFX002(FixedAssetRegistrationViewModel model)
        {
            if (Session["User"] != null)
            {
                model.ListProcess = _maCode.GetAllCodeByCode("FIXED_TRANSFER_TYPE");
                return View(model);
            }
            else
                return RedirectToAction("Login", "Home");
        }
        #endregion


        #region Fixeed Asset
        public ActionResult ActionAdd(string name, string category, decimal depreciation)
        {
            if (Session["User"] != null)
            {
                if (_fixedAsset.AddFixedAseet(name, category, Session["User"].ToString(), depreciation))
                {
                    return Json(new { success = true, message = "Registro exitoso.", json = _fixedAsset.GetAllFixedAsset() }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Error al registrar" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActionGetFixedAsset(int id)
        {
            if (Session["User"] != null)
            {
                var item = _fixedAsset.GetFixedAssetById(id);
                if (item != null)
                {
                    return Json(new { success = true, message = "Activo encontrado", json = item }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Error al buscar activo" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionEdit(int id, string name, string category, bool status, decimal depreciation)
        {
            if (Session["User"] != null)
            {
                var item = _fixedAsset.GetFixedAssetById(id);
                if (item != null)
                {
                    if (_fixedAsset.UpdateFixedAsset(id, name, category, Session["User"].ToString(), status, depreciation))
                    {
                        return Json(new { success = true, message = "Activo editado", json = _fixedAsset.GetAllFixedAsset() }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "Error al editar activo" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Error al buscar activo" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActionRegisterFixedAsset()
        {
            if (Session["User"] != null)
            {

                //REVISAR DOLAR.
                //REVISAR ERRORES
                //REVISAR SI NO HAY FACTURA

                var acquisition = Request["Acquisition"];
                int requisition = Convert.ToInt32(Request["Requisition"]);
                var supplier_id = Request["Supplier"];
                var currency = Request["Currency"];
                var policy = Request["Policy"];

                var Itemsjson = Request["Items[]"];

                // CARGAR XML.
                string totalXML = "";
                DateTime fechaXML = DateTime.MinValue;
                string xmlD = "";
                var invoice_no = "";

                HttpPostedFileBase file = Request.Files[0];
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string mimeType = file.ContentType;
                System.IO.Stream fileContent = file.InputStream;
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(file.InputStream);
                var xmlN = xmlDocument.GetElementsByTagName("cfdi:Comprobante");
                var insideXML = xmlN[0];
                var atributes = insideXML.Attributes;
                invoice_no = ((atributes["Serie"] != null) ? atributes["Serie"].Value : "") + ((atributes["Folio"] != null) ? atributes["Folio"].Value : "");
                totalXML = atributes["Total"].Value;
                fechaXML = Convert.ToDateTime(atributes["Fecha"].Value);
                xmlD = xmlDocument.InnerXml.ToString();
                // FIN CARGAR XML.

                var Items = JsonConvert.DeserializeObject<List<FixedAssetItemModel>>(Itemsjson);

                //TOMAR TIPO DE CAMBIO.
                FIXED_PURCHASE_ORDER purchase = new FIXED_PURCHASE_ORDER();
                purchase.supplier_id = Convert.ToInt32(supplier_id);
                purchase.site_code = "0011";
                purchase.po_date = fechaXML;
                purchase.currency = currency;
                purchase.status = true;
                purchase.invoice_no = invoice_no;
                purchase.import_date = null;
                purchase.xml = xmlD;
                purchase.exchange_rate = _exchangeCurrency.GetLastExchangeCurrent();
                purchase.cdate = DateTime.Now;
                purchase.cuser = Session["User"].ToString();
                purchase.program_id = "RFX001";

                var po_no = _fixedPurchase.AddFixedAseetPurchase(purchase);


                int index = 0;
                foreach (var item in Items)
                {
                    var fixedItem = _fixedAssetItem.AddItem(Convert.ToDateTime(acquisition), item.fixed_asset_folio, Session["User"].ToString(), "0011", item.fixed_asset_code.Value, item.description, item.brand, item.serie, item.model, requisition);

                    FIXED_PURCHASE_ORDER_ITEM itemPurchase = new FIXED_PURCHASE_ORDER_ITEM();
                    itemPurchase.po_no = purchase.po_no;
                    itemPurchase.item_no = index;
                    itemPurchase.fixed_asset_id = fixedItem.fixed_asset_id;
                    itemPurchase.price = item.cost;
                    itemPurchase.iva = item.iva;
                    itemPurchase.status = true;
                    itemPurchase.cuser = Session["User"].ToString();
                    itemPurchase.cdate = DateTime.Now;
                    itemPurchase.program_id = "RFX001";
                    _fixedPurchase.AddFixedAseetPurchaseItem(itemPurchase);

                    index++;

                }


                return Json(new { success = true, message = "Test" });

            }
            else
                return Json(new { success = false, message = "Favor de iniciar sesion" });
        }


        public ActionResult ActionGetReport(string site)
        {
            if (Session["User"] != null)
            {

                var query = _fixedAsset.GetFixedAssetReport(site);
                var sum = query.Sum(s => s.real_price);


                return Json(new { success = true, data = query, Amount = sum }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActionXMLAmount()
        {
            string subtotalXML = "";

            //Carga XML
            HttpPostedFileBase file = Request.Files[0];
            int fileSize = file.ContentLength;
            string fileName = file.FileName;
            string mimeType = file.ContentType;
            System.IO.Stream fileContent = file.InputStream;
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(file.InputStream);
            var xmlN = xmlDocument.GetElementsByTagName("cfdi:Comprobante");
            var insideXML = xmlN[0];
            var atributes = insideXML.Attributes;
            subtotalXML = atributes["Total"].Value;


            return Json(new { success = true, Amount = subtotalXML }, JsonRequestBehavior.AllowGet);

        }
        #endregion


        #region Fixed Asset Item
        public ActionResult ActionGetItemsByRequisition(int id)
        {
            if (Session["User"] != null)
            {
                var items = _fixedAssetItem.GetFixedAssetItemByRequisition(id);
                if (items != null)
                {
                    return Json(new { success = true, message = "Correcto", Json = items }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Error al buscar activo" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsByRequisitionBy(int id)
        {
            if (Session["User"] != null)
            {
                var response = _fixedAssetItem.GetFixedAssetItemByRequisition(id);
                return Json(new { success = true, message = "Correcto", Json = response }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        


        #endregion


        #region Fixed Asset Requisition
        public ActionResult ActionChangeStatus(int id, int status)
        {
            if (Session["User"] != null)
            {
                if (_fixedAssetRequisition.UpdateFixedAssetRequisition(id, status, Session["User"].ToString()))
                {
                    return Json(new { success = true, message = "Correcto" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "Error al buscar activo" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetRequisitionBySite(string site_code)
        {
            if (Session["User"] != null)
            {
                var response = _fixedAssetRequisition.GetFixedAssetBySite(site_code);
                return Json(new { success = true, message = "Correcto", Json = response }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }



        #endregion


        #region Transfer
        [HttpPost]
        public ActionResult ActionTransfer(string site, List<FixedAssetItemModel> list)
        {
            if (Session["User"] != null)
            {
                //Guardar 
                //Traer documento
                var doc = _fixedAsset.Transfer_Next_Document().Document;
                //Transfer Header
                FIXED_ASSET_TRANSFER_HEADER header = new FIXED_ASSET_TRANSFER_HEADER();
                header.document_no = doc;
                header.transfer_site_code = "0011";
                header.destination_site_code = site;
                header.transfer_date = DateTime.Now;
                header.status = 4;
                header.cuser = Session["User"].ToString();
                header.cdate = DateTime.Now;
                header.program_id = "TFX001";
                _fixedTransferHeader.AddFixedAseet(header);

                //Transfer Detail
                foreach (var item in list)
                {
                    //Pone el Activo en transferencia.
                    var fixedAsset = _fixedAssetItem.GetItemById(item.fixed_asset_id);
                    fixedAsset.status_asset = 4;
                    _fixedAssetItem.UpdateFixedAsset(fixedAsset);

                    //Lo agrega al envio.
                    FIXED_ASSET_TRANSFER_DETAIL detail = new FIXED_ASSET_TRANSFER_DETAIL();
                    detail.document_no = doc;
                    detail.transfer_site_code = "0011";
                    detail.destination_site_code = site;
                    detail.fixed_asset_id = item.fixed_asset_id;
                    detail.gi_status = "P";
                    detail.cuser = Session["User"].ToString();
                    detail.cdate = DateTime.Now;
                    detail.program_id = "TFX001";
                    _fixedTransferDetail.AddFixedAseet(detail);
                }

                //Crear Reporte de Envio.

                return Json(new { success = true, message = "Correcto" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }



        public ActionResult ActionCompleteTransfer(string folio)
        {
            if (Session["User"] != null)
            {
                var detail = _fixedTransferDetail.GetDetailByDocument(folio);
                var header = _fixedTransferHeader.GetHeaderByDocument(folio);

                foreach (var item in detail)
                {
                    //Actualiza Item
                    var fixedItem = _fixedAssetItem.GetItemById(item.fixed_asset_id);
                    fixedItem.site_code = "0011";
                    fixedItem.location = "RB";
                    fixedItem.status_asset = 1;
                    fixedItem.udate = DateTime.Now;
                    fixedItem.uuser = Session["User"].ToString();
                    _fixedAssetItem.UpdateFixedAsset(fixedItem);

                    //Actualiza Detail
                    item.gi_status = "C";
                    item.uuser = Session["User"].ToString();
                    item.udate = DateTime.Now;
                    _fixedTransferDetail.UpdateFixedAsset(item);
                }

                //Actualiza Header
                header.uuser = Session["User"].ToString();
                header.udate = DateTime.Now;
                header.status = 9;
                _fixedTransferHeader.UpdateFixedAsset(header);


                return Json(new { success = true, message = "Registro exitoso." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDetailTransfer(string folio)
        {
            if (Session["User"] != null)
            {
                var result = _fixedTransferDetail.GetDetailModelByDocument(folio);

                return Json(new { success = true, message = "Registro exitoso.", json = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetHeaderTransfer(string process)
        {
            if (Session["User"] != null)
            {
                var result = _fixedTransferHeader.GetTransferByType(process);

                return Json(new { success = true, message = "Registro exitoso.", json = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}