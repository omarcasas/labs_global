﻿
class Requests {
    constructor() {
        this._url = "";
        this._method = "get";
        this._url_params = {};
        this._data = {};

    }

    static get(url, url_params) {
        return new Requests().method("get").url(url).url_params(url_params);
    }
    static post(url, url_params) {
        return new Requests().method("post").url(url).url_params(url_params);
    }

    url(val) {
        this._url = val;
        return this;
    }
    method(val) {
        this._method = val;
        return this;
    }
    url_params(val) {
        this._url_params = val;
        return this;
    }
    data(val) {
        this._data = val;
        return this;
    }
    send_result() {
        return this.send();
    }
   
    async send() {
        try {
            switch (this._method) {
            case 'get':
                return this._request(await axios.get(this._url + Requests.url_serialize(this._url_params)));
            case 'post':
                return this._request(await axios.post(this._url + Requests.url_serialize(this._url_params), this._data));
            }
            throw `${this._method} not implemented`;
        } catch (e) {
            const code = e.response ? e.response.status : -1;
            let msg;
            if (code === 404) {
                msg = "Url no encontrada";
            } else {
                msg = `Error desconocido[${code}]`;
            }
            const ne = { Code: e.code, Msg: msg };
            //default
            toastr.error(ne.Msg);
            console.log("default error");
            return { Ok: false, Msg: ne.Msg, Code: e.code,Type:'error'};
        } 
        
    }
    async _request(res) {
        if (res.data.Ok) {
            return res.data;
        } else if (res.data.Type === "sf") {
            swal({
                    title: "Sesión terminada",
                    text: "Se redigira a la pagina de inicio de sesión",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Aceptar"

                },
                (va) => {
                    const loc = window.location;
                    window.location.href = `${loc.protocol}//${loc.host}/Home/Login`;
                });
            return res.data;
        } else {
            toastr.error(res.Msg);
            return res.data;
        }
    }
    static url_serialize(params) {
        let str = "";
        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                str += str ? "&" : "?";
                str += key + "=" + encodeURIComponent(params[key]);
            }
        }
        return str;
    }

    static result_default(res, resolve, reject) {
        
    }
}

