﻿var pages = [];
$.ajax({
    url: '/Home/GetPagesByUser/',
    type: "GET",
    data: {},
    success: function (returndata) {
        if (returndata == "SF") {
            SessionFalse("Su sesión a terminado.")
        } else {
            pages = returndata.pages;
        }
    },
    error: function () {
        toastr.error('Error inesperado contactar a sistemas.');
    }
});

$("#searchingMainNoRepeat").on('keyup', function () {
    inputValueSearching = $("#searchingMainNoRepeat").val();
    if (inputValueSearching.length > 2) {
        $('#search_containerNoRepeat').html('');
        $.each(pages, function (index, value) {
            var name = RemoveAccentsSearching(value.page_name.toLowerCase());
            var description = RemoveAccentsSearching(value.description.toLowerCase());
            if (name.indexOf(RemoveAccentsSearching(inputValueSearching.toLowerCase())) >= 0 || description.indexOf(RemoveAccentsSearching(inputValueSearching.toLowerCase())) >= 0) {
                $('#search_containerNoRepeat').append(`
                                                                                <a href="${value.url}" onclick="RedirectPageSearching();" class="list-group-item mx-0" style="background-color: white;">
                                                                                    <strong>${value.page_name}</strong> <span>${value.description}</span>
                                                                                </a>`);
            }
        });
    } else {
        $('#search_containerNoRepeat').html('');
    }
});

$('#searchingMainNoRepeat').blur(function () {
    setTimeout(function () { $('#search_containerNoRepeat').html(''); }, 200);
});

//Remover acentos
function RemoveAccentsSearching(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}

function RedirectPageSearching() {
    StartLoading();
}

function SessionFalse(data) {
    swal({
        title: data + " No se realizo ninguna acción.",
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Iniciar Sesión"
    }, function (isConfirm) {
        if (isConfirm) {
            location.reload();
        }
    });
}