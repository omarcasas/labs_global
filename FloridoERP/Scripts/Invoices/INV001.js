﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    var model = JSON.parse($("#model").val());
    table.clear();
    table.rows.add(model);
    table.columns.adjust().draw();
});

function onFileLoad(elementId, event) {
    listXML = event.target.result;
}

updateList = function (event, onLoadFileHandler) {
    var input = document.getElementById('file');
    var output = document.getElementById('fileList');
    var c = 0;

    for (var i = 0; i < input.files.length; ++i) {
        c = c + 1;
    }

    if (c > 1)
        output.placeholder = c + " archivos";
    else if (c == 1)
        output.placeholder = "1 archivo";
    else
        output.placeholder = "";

    if (typeof window.FileReader !== 'function')
        throw ("The file API isn't supported on this browser.");
    let input2 = event.target;
    if (!input2)
        throw ("The browser does not properly implement the event object");
    if (!input2.files)
        throw ("This browser does not support the `files` property of the file input.");
    if (!input2.files[0])
        return undefined;

    listXML = input2.files;

    convertToBase64XML(listXML);
}

function contains(a, obj) {
    var c = 0;
    var i = a.length;

    while (i--) {
        if (a[i] === obj) {
            c++;
        }
    }
    return c;
}

var docs = [];
var listXML = [];
var b = 0;
function convertToBase64XML(selectedFile) {
    docs = [];
    b = 0;
    if (selectedFile != null) {
        if (selectedFile.length > 0) {
            for (var i = 0; i < selectedFile.length; i++) {
                var xmlName = selectedFile[i].name
                var services = docs.filter(e => e.xml_name == xmlName).length
                if (services == 0) {
                    var fileToLoad = selectedFile[i];
                    var xml_name = fileToLoad.name
                    var fileReader = new FileReader();
                    docs = [...docs, { Xml: "", FileName: xml_name }]
                    fileReader.onload = function (fileLoadedEvent) {
                        var base64 = window.btoa(unescape(encodeURIComponent(fileLoadedEvent.target.result)));
                        var xml = base64;
                        docs[b].Xml = xml;
                        b++;
                    };
                    fileReader.readAsText(fileToLoad);
                }
                else
                    toastr.warning("No se pudo leer el archivo.");
            }
        }
    }
}

function SaveXML() {
    if (docs.length == 0) {
        toastr.warning("Agregue archivos XML");
        return;
    }
    else {
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/Invoices/ActionSaveXML",
            data: { "Xml": docs },
            success: function (response) {
                EndLoading();
                if (response.result == "SF")
                    SessionFalse("Terminó tu sesión.");
                else if (response.result) {
                    document.getElementById("fileList").placeholder = "";
                    listXML = [];
                    docs = [];
                    table.clear();
                    table.rows.add(response.model);
                    table.columns.adjust().draw();
                    $("#TableDiv").animatePanel();
                    swal({
                        title: "Guardado Correctamente",
                        text: "Archivos XML Guardados Correctamente.",
                        type: "success"
                    });
                }
                else {
                    document.getElementById("fileList").placeholder = "";
                    listXML = [];
                    docs = [];
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            },
            error: function () {
                EndLoading();
                toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
            }
        });
    }
}

function GetXML() {
    var dateBegin = $("#dateBegin").val().trim();
    var dateEnd = $("#dateEnd").val().trim();

    if (dateBegin != "" && dateEnd != "" && dateEnd != null && dateBegin != null) {
        Begin = moment($("#dateBegin").val());
        End = moment($("#dateEnd").val());

        if (Begin <= End) {
            var invoiceTypeC = $("#invoicetypeC").val();
            var siteCode = $("#siteCode").val();
            var customer = $("#customer").val();

            if (siteCode == "0") {
                toastr.warning("Seleccione una tienda");
                return;
            }
            else if (customer == "00") {
                toastr.warning("Seleccione un cliente");
                return;
            }
            else {
                StartLoading();
                $.ajax({
                    url: "/Invoices/ActionGetXML",
                    data: { "BeginDate": dateBegin, "EndDate": dateEnd, "InvoiceType": invoiceTypeC, "SiteCode": siteCode, "CustomerCode": customer },
                    type: "POST",
                    success: function (response) {
                        EndLoading();
                        if (response.success) {
                            var model = JSON.parse(response.Json);
                            table.clear();
                            table.rows.add(model);
                            table.columns.adjust().draw();
                            $("#TableDiv").animatePanel();
                        }
                        else
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    },
                    error: function () {
                        EndLoading();
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                });
            }
        }
        else {
            toastr.warning("La fecha final no puede ser menor a la inicial");
            return;
        }
    }
    else {
        toastr.warning("Seleccione un rango de fechas");
        return;
    }
}

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

var table = $("#xmlCustomers").DataTable({
    autoWidth: true,
    responsive: true,
    order: [[0, "desc"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'copy', className: 'btn-sm' },
        { extend: 'csv', title: 'FacturacionXML', className: 'btn-sm' },
    ],
    columns: [
        { data: 'Id', width: "1%" },
        { data: 'Uuid', width: "1%" },
        { data: 'Serie', width: "1%" },
        { data: 'InvoiceNo', width: "1%" },
        { data: 'CreateDate', width: "1%" },
        { data: 'CDFI', width: "1%" },
        { data: 'UserCreated', width: "1%" },
        { data: null, width: "1%", orderable: false }
    ],
    columnDefs:
        [
            {
                targets: [4],
                width: "1%",
                render: function (data, type, row) {
                    return moment(data).format('L');
                }
            },
            {
                data: null,
                width: "1%",
                targets: [7],
                className: "text-nowrap",
                defaultContent: '<div class="text-center"><button class="btn btn-xs btn-outline btn-info btnReport" type="button"><i class="fa fa-file-text-o"></i> Factura</button>&nbsp;&nbsp;<button class="btn btn-xs btn-outline btn-info btnDownload" type="button"><i class="fa fa-file-text-o"></i> Descargar</button></div>'
            }
        ]
});

$('#xmlCustomers tbody').on('click', 'button.btnReport', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    model = row.data();
    StartLoading();
    $.ajax({
        url: "/Invoices/ActionGetPDF",
        type: "POST",
        data: { "Id": model.Id },
        success: function (response) {
            EndLoading();
            if (response.result) {
                $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${response.data}'></iframe>`);
                $("#modalReference").modal("show");
            }
            else if (response.result == "SF")
                SessionFalse("Terminó tu sesión.");
            else
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        },
        error: function () {
            EndLoading();
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }
    });
});