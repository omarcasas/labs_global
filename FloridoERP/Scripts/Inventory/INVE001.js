﻿
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); $(".hide-menu").click(); }, 4500);
});

var Inventorys = JSON.parse($("#Inventorys").val());
var formatx = x => ({ start: moment(x.start).format("MM/DD/YYYY HH:mm:ss"), title: x.title , site_name: x.site_name , id: x.id , calendar_status: x.calendar_status , cdate:x.cdate , description:x.description , site_code:x.site_code , site_name:x.site_name , typeInv:x.typeInv , udate:x.udate});
Inventorys = Inventorys.map(x=> formatx(x));
var InventorysTypesJS = JSON.parse($("#InventorysTypesJS").val());
$(".drop").select2();

$('#calendar').fullCalendar({
    header: {
        left: 'prevYear,prev,next,nextYear today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
    },
    editable: true,
    droppable: true,
    drop: function () {
        if ($('#drop-remove').is(':checked')) {
            $(this).remove();
        }
    },
    eventDrop: function (event, delta, revertFunc) {
        UpdateInventoryOnlyDate(event, delta, revertFunc);
    },
    eventClick: function (event, jsEvent, view) {
        validator.resetForm();
        if (event.calendar_status == 0) {
            $("#ModalTitle").html('<i class="fa fa-archive"></i> Editar Inventario');
            $("#ModalDescription").html('Formulario para editar inventario');
            $("#ModalAddInventory").removeClass("hmodal-success");
            $("#ModalAddInventory").removeClass("hmodal-info");
            $("#ModalAddInventory").removeClass("hmodal-danger");
            $("#ModalAddInventory").addClass("hmodal-warning");
            $("#btnCancel").show();
            $("#btExec").removeClass("btn btn-success hide").addClass("btn btn-warning");
            $("#btExec").text('  Editar');
        } else if (event.calendar_status == 2) {
            $("#ModalTitle").html('<i class="fa fa-archive"></i> Error al Trasladar inventario a tienda.');
            $("#ModalDescription").html('Contacte a Sistemas');
            $("#ModalAddInventory").removeClass("hmodal-success");
            $("#ModalAddInventory").removeClass("hmodal-info");
            $("#ModalAddInventory").removeClass("hmodal-warning");
            $("#ModalAddInventory").addClass("hmodal-danger");            
            $("#btExec").removeClass("btn btn-success hide").addClass("btn btn-warning");
            $("#btExec").text('  Editar');
        }
        else {
            $("#ModalTitle").html('<i class="fa fa-archive"></i> Información de Inventario');
            $("#ModalDescription").html('Información adicional sobre el inventario seleccionado.');
            $("#ModalAddInventory").removeClass("hmodal-success hmodal-warning").addClass("hmodal-info");
            $("#ModalAddInventory").removeClass("hmodal-success");
            $("#ModalAddInventory").removeClass("hmodal-warning");
            $("#ModalAddInventory").addClass("hmodal-info");
            $("#btExec").removeClass("btn btn-success hmodal-warning").addClass("hide");
            $("#btExec").text('   Editar');
            $("#btnCancel").hide();
        } $("#idInventoryDate").val(event.start.format());
        $("#title").val(event.title);
        $("#description").val(event.description);
        $("#SiteValue").val(event.site_code).trigger("change");
        $("#TypeValue").val(event.typeInv).trigger("change");
        $("#idInventory").val(event.id);
        $('#ModalAddInventory').modal('show');
    },
    dayClick: function (date, jsEvent, view) {
        validator.resetForm();
        if (moment(date).add(7, 'hours') >= moment(new Date())) {
            $("#ModalTitle").html('<i class="fa fa-archive"></i> Calendarizar Inventario');
            $("#ModalDescription").html('Formulario para calendarizar nuevo inventario.');
            $("#ModalAddInventory").removeClass("hmodal-warning").addClass("hmodal-success");
            $("#btExec").removeClass("btn btn-warning hide").addClass("btn btn-success");
            $("#btExec").text('  Registrar');
            $("#title").val(null);
            $("#description").val(null);
            $("#SiteValue").val("").trigger("change");
            $("#TypeValue").val("").trigger("change");
            $("#idInventoryDate").val(date.format());
            $('#ModalAddInventory').modal('show');
            $("#btnCancel").hide();
        } else {
            toastr.warning("Solo puede calendarizar un inventario después del día actual");
        }       
    },
    eventRender: function (event, element, view) {
        element.find('.fc-title').append('<div class="hr-line-solid-no-margin"></div><span style="font-size: 10px">' + event.site_name + '</span></div>');
        element.find('.fc-list-item-title').append('<div class="row"><div class="form-group col-lg-3"><strong>' + event.site_name + '</strong></div><div class="form-group col-lg-3"><strong class="text-info">Descripción: </strong>' + event.description + '</div><div class="form-group col-lg-3"><strong class="text-info">Tipo: </strong>' + InventorysTypesJS.filter(x => x.typeInv === event.type)[0].InventoryName + '</div></div>');
        if (event.calendar_status == 2) {
            element.css('background-color', '#FF2301');
            element.css('borderColor', '#FF2301');
        }
        else if(event.calendar_status == 0 & view.type != 'listMonth') {
            element.css('background-color', '#62cb31');
            element.css('borderColor', '#62cb31');
        }
        else if (event.calendar_status == 0) {
            element.find('.fc-event-dot').css('background-color', '#8d2244');
        }
    },
    eventSources: [Inventorys]
});
$('.fc-next-button,.fc-today-button,.fc-prev-button,.fc-month-button,.fc-agendaWeek-button,.fc-agendaDay-button,.fc-listMonth-button').click(function () {
    var date = $("#calendar").fullCalendar('getDate');
    date = moment(date).format('L');
    $.ajax({
        url: "/InventoryCalendar/ActionMonthCalendar/",
        data: { "DateStart": date },
        type: "GET",
        success: function (Data) {
            $('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('renderEvents', Data.map(x=> formatx(x)));
        }
    });
});
$('.fc-icon-right-double-arrow').click(function () {
    var date = $("#calendar").fullCalendar('getDate');
    date = moment(date).add(1, 'year').format('L');
    $.ajax({
        url: "/InventoryCalendar/ActionMonthCalendar/",
        data: { "DateStart": date },
        type: "GET",
        success: function (Data) {
            $('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('renderEvents', Data.map(x=> formatx(x)));
        }
    });
});
$('.fc-icon-left-double-arrow').click(function () {
    var date = $("#calendar").fullCalendar('getDate');
    date = moment(date).add(-1, 'year').format('L');
    $.ajax({
        url: "/InventoryCalendar/ActionMonthCalendar/",
        data: { "DateStart": date },
        type: "GET",
        success: function (Data) {
            $('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('renderEvents', Data.map(x=> formatx(x)));
        }
    });
});

var validator = $("#form").validate({
    ignore: [],
    rules: {
        title: {
            required: true,
            minlength: 4,
            maxlength: 25
        },
        description: {
            required: true,
            minlength: 6,
            maxlength: 25
        },
        SiteValue: {
            required: true
        },
        TypeValue: {
            required: true
        }
    },
    messages: {
        title: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar mas de 4 caracteres',
            maxlength: 'Máximo 25 caracteres'
        },
        description: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar mas de 6 caracteres',
            maxlength: "Máximo 25 caracteres"
        },
        SiteValue: {
            required: "El Campo no puede ser nulo"
        },
        TypeValue: {
            required: "El Campo no puede ser nulo"
        }

    }
});
function UpdateInventoryOnlyDate(event, delta, revertFunc) {
    $.ajax({
        url: "/InventoryCalendar/ActionUpdateInventoryCalendarOnlyDate/",
        data: { "id": event.id, "start": event.start.format(), "program_id": "INVE001.cshtml" },
        type: "POST",
        success: function (Data) {
            if (!Data.success) {
                if (Data.responseText == "Termino tu sesion.") {
                    SessionFalse(Data.responseText);
                }
                revertFunc();
            }
        }
    });
}
function InsertInventory() {
    var titleVar = $('#title').val();
    var descriptionVar = $('#description').val();

    $('#title').val(titleVar.trim());
    $('#description').val(descriptionVar.trim());

    if (validator.element("#title") && validator.element("#description") && validator.element("#SiteValue") && validator.element("#TypeValue")) {
        StartLoading()
        var typeOf = true;
        if ($('#btExec').text() == '  Editar')
            typeOf = false;
        $.ajax({
            url: "/InventoryCalendar/ActionInventoryCalendar/",
            data: { "id": $("#idInventory").val(), "start": $("#idInventoryDate").val(), "title": $("#title").val(), "description": $("#description").val(), "typeInv": $("#TypeValue").val(), "site_code": $("#SiteValue").val(), "program_id": "INVE001.cshtml", "typeOf": typeOf },
            type: "POST",
            success: function (Data) {
                if (!Data.success) {
                    if (Data.responseText == "Termino tu sesion.") {
                        SessionFalse(Data.responseText);
                    }
                    else {
                        toastr.warning(Data.responseText);
                    }
                }
                else {
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('renderEvents', Data.Inventorys);
                    $('#ModalAddInventory').modal('hide');
                    toastr.success(Data.responseText);
                }
                EndLoading();
            }
        });
    }
}
var tour = new Tour({
    backdrop: true,//Bloquea contenido Atras
    keyboard: true,//Cambia de paso con flechas del teclado
    //Lenguaje Español
    template: "<div class='popover tour' style='max-width: 400px;width: 400px;'> <div class='arrow'></div> <h3 class='popover-title'></h3> <div class='popover-content'></div> <nav class='popover-navigation'> <div class='btn-group'> <button class='btn btn-default' data-role='prev' type='button'>Atras</button> <button class='btn btn-default' data-role='next' type='button'>Siguiente</button> <button class='btn btn-primary' data-role='end' type='button'>Fin</button> </div> </nav> </div>",
    onShown: function (tour) {//Animacion
        $('.animated').removeClass('fadeIn');
        $('.animated-panel').removeClass('zoomIn');
    },
    steps: [//Pasos del tour
        {
            //delay: 300,

            element: "#CalendarPanel",
            title: "Calendario",
            content: "Recuadro donde se muestran los dias del mes.",
            placement: "top",
        }, {
            //delay: 300,
            backdropPadding: 8,
            element: ".fc-left",
            title: "Botones de filtrado",
            content: "Botones que permiten cambiar la forma de visualizar el mes seleccionado.",
            placement: "right",
            onPrev: function () {
                //PanelGridShow();
            }
        },
        {
            //delay: 300,
            backdropPadding: 8,
            element: ".fc-center",
            title: "Mes seleccionado",
            content: "Muestra el mes seleccionado por el usuario y el año del mismo.",
            placement: "top",
            onNext: function () {
                //PanelInfoShow();
            }
        }, {
            //delay: 300,
            backdropPadding: 8,
            element: ".fc-right",
            title: "Botones de filtrado",
            content: "Botones que permiten cambiar la forma de visualizar el mes seleccionado.",
            placement: "left",
            onNext: function () {
                //PanelInfoShow();
            }
        }
    ]
});
tour.init();
function HelpTour() {
    tour.restart();
}
function CancelInventory() {
    swal({
        title: "¿Esta seguro?",
        text: "Se cancelara el inventario",
        type: 'warning',
        closeOnConfirm: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            confirmCancel()
    });
}
function confirmCancel() {
    StartLoading();
    $.ajax({
        url: "/InventoryCalendar/ActionCloseInventory/",
        data: { "id": $("#idInventory").val(), "start": $("#idInventoryDate").val() },
        type: "POST",
        success: function (Data) {
            if (!Data.success) {
                if (Data.responseText == "SF") {
                    SessionFalse("Se termino su sesion.");
                    EndLoading();
                }
                else {
                    toastr.warning("Ha ocurrido un error , contacte a sistemas");
                    EndLoading();
                }
            }
            else {
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('renderEvents', Data.Inventories);
                $('#ModalAddInventory').modal('hide');
                toastr.success("Inventario Cancelado");
            }
            EndLoading();
        }
    });
}