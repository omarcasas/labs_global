﻿
var LevelList = []

var ListInventoryType = []

var IsEdit = false;

var IdInventory = 0;

const sesion = "Se termino su sesion.";

function selectGeneric(id, list, array, firstOption) {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    const selectList = arrayOption => elements => selectOption(elements[arrayOption[0]])(elements[arrayOption[1]]);
    $(id).html([`${selectOption(firstOption.id)(firstOption.text)}`, ...list.map(elements => selectList(array) (elements))])
}

/*Bottones */
$("#invetoryButton").click(() => {
    let nameInventory = $("#nameInventory").val();
    if (IsEdit) {
        if (ListInventoryType.filter(x => x.InventoryName.toLowerCase() == nameInventory.trim().toLowerCase() && x.IdInventoryType == IdInventory).length == 0) {
            toastr.warning("Ya existe ese tipo de inventario favor de ingresar otro")
        }
        else if (nameInventory.trim() != "") {
            ValidationsFormInventory();
        }
        else {
            toastr.warning("Favor de poner el nombre")
        }
    }
    else {
        if (ListInventoryType.filter(x => x.InventoryName.toLowerCase() == nameInventory.trim().toLowerCase()).length > 0) {
            toastr.warning("Ya existe ese typo de inventario favor de ingresar otro")
        }
        else if (nameInventory.trim() != "") {
            ValidationsFormInventory();
        }
        else {
            toastr.warning("Favor de poner el nombre")
        }
    }
})

function ValidationsFormInventory() {
    if ($("#typeReviewSelect").val().trim() != "0") {
        if ($("#daySelect").val().trim() != "0" || $("#typeReviewSelect").val().trim() == "Indefinido") {
            let selectValue = $("#departamentSelect").val();
            if (selectValue.trim() != "0") {
                if (LevelList.filter(x => x.ActiveDetails && x.LevelMaClass == selectValue).length == 0) {
                    InventoryDetailsReload(true);
                    if (!IsEdit) {
                        StatusForm(true);
                    }
                    //LevelList = LevelList.filter(x => x.ActiveDetails);
                }
                else
                    toastr.warning("Ya seleccionaste esa opcion")
            }
            else
                toastr.warning("Favor de seleccionar un nivel");
        }
        else
            toastr.warning("Selecciona un dia")
    }
    else
        toastr.warning("Selecciona una revision")
}

$("#invetorySaveButton").click(() => {
    if ($("#nameInventory").val().trim() != "" && $("#typeReviewSelect").val().trim() != "0" && ($("#daySelect").val().trim() != "0" || $("#typeReviewSelect").val().trim() == "Indefinido") && LevelList.filter(x => x.ActiveDetails).length >= 0) {
        swal({
            title: "Seguro que deseas guardar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    let newInventaryType = {
                        IdInventoryType: 0,
                        InventoryName: $("#nameInventory").val().trim().toUpperCase(),
                        TypeReview: $("#typeReviewSelect").val().trim(),
                        Day: $("#daySelect").val().trim() == "0" ? "Indefinido" : $("#daySelect").val().trim(),
                        ActiveInventory: $("#isActiveForm").is(":checked"),
                        CloseTime: $("#closeTime").val()
                    }
                    axios.post("/InventoryType/ActionSaveInventoryType/", { InventoryType: newInventaryType, InventoryTypeDetails: LevelList })
                        .then(data => {
                            if (data.data == "SF") {
                                setTimeout(function () {
                                    SessionFalse(sesion)
                                }, 800)
                            }
                            else if (data.data.IdInventoryType > 0) {
                                toastr.success("Guardado con exito")
                                newInventaryType.IdInventoryType = data.data.IdInventoryType;
                                ListInventoryType = [...ListInventoryType, newInventaryType];
                                FillTableInventory(ListInventoryType)
                                ClearForm();
                            }
                            else {
                                toastr.error("Hubo un problema al guardar")
                            }
                        })
                        .catch(error => {
                            toastr.error('Error inesperado contactar a sistemas.');
                        })
                }
                else {

                }
            })
    }
    else
        toastr.warning("Favor de rellenar todos los campos y agregar al menos un departamento");
})

$("#invetoryEditButton").click(() => {
    if ($("#nameInventory").val().trim() != "" && $("#typeReviewSelect").val().trim() != "0" && ($("#daySelect").val().trim() != "0" || $("#typeReviewSelect").val().trim() == "Indefinido") && LevelList.filter(x => x.ActiveDetails).length > 0 && $("#closeTime").val() != "" && $("#closeTime").val() != null) {
        swal({
            title: "Seguro que deseas editar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    let newInventaryType = {
                        IdInventoryType: IdInventory,
                        InventoryName: $("#nameInventory").val().trim().toUpperCase(),
                        TypeReview: $("#typeReviewSelect").val().trim(),
                        Day: $("#daySelect").val().trim() == "0" ? "Indefinido" : $("#daySelect").val().trim(),
                        ActiveInventory: $("#isActiveForm").is(":checked"),
                        CloseTime: $("#closeTime").val()
                    }

                    axios.post("/InventoryType/ActionEditInventoryType/", { InventoryType: newInventaryType, InventoryTypeDetails: LevelList })
                        .then(data => {
                            if (data.data == "SF") {
                                setTimeout(function () {
                                    SessionFalse(sesion)
                                }, 800)
                            }
                            else if (data.data.IdInventoryType > 0) {
                                toastr.success("Editado con exito")
                                let index = ListInventoryType.findIndex(x => x.IdInventoryType == IdInventory);
                                ListInventoryType[index] = newInventaryType;
                                FillTableInventory(ListInventoryType)
                                ClearForm();
                            }
                            else {
                                toastr.error("Hubo un problema al editado")
                            }
                        })
                        .catch(error => {
                            toastr.error('Error inesperado contactar a sistemas.');
                        })
                }
                else {

                }
            })
    }
    else
        toastr.warning("Favor de rellenar todos los campos y agregar al menos un departamento ");
})

function deleteInventory(IdInventoryType) {
    swal({
        title: "Seguro que deseas borrar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) {

            axios.get(`/InventoryType/ActionRemoveInventory/?Inventory=${IdInventoryType.split("N_")[1]}`)
                .then(data => {
                    if (data.data == "SF") {
                        SessionFalse(sesion)
                    }
                    else if (data.data) {
                        toastr.success("Borrado con exito")
                        ListInventoryType = ListInventoryType.filter(x => x.IdInventoryType != IdInventoryType.split("N_")[1]);
                        FillTableInventory(ListInventoryType)
                        ClearForm();
                    }
                    else {
                        toastr.error("Hubo un problema al borrar")
                    }
                })
                .catch(error => {
                    toastr.error('Error inesperado contactar a sistemas.');
                })

        }
        else {

        }
    })
}

function IsDay() {
    if ($("#typeReviewSelect").val().trim() == "Indefinido") {
        $("#dayForm").hide();
        $('#daySelect').val(0).trigger('change');
    }
    else {
        $("#dayForm").show();
    }
}

function InventoryDetailsReload(value) {
    let level = $("#departamentSelect").val();
    if (IsEdit) {
        if (LevelList.filter(x => x.LevelMaClass == parseInt(level)).length > 0) {
            LevelList.filter(x => x.LevelMaClass == parseInt(level))[0].ActiveDetails = true;
        }
        else if (LevelList.filter(x => x.LevelMaClass == parseInt(value)).length > 0) {
            LevelList.filter(x => x.LevelMaClass == value)[0].ActiveDetails = true
        }
        else {
            InventoryFormReload(value);
        }
    }
    else if (!IsEdit) {
        InventoryFormReload(value);
    }
    FillTableLevel(LevelList.filter(x => x.ActiveDetails))
    $("#departamentSelect").val(0).trigger('change');
}

function InventoryFormReload(value) {
    LevelList = [...LevelList, {
        IdInventoryType: IsEdit ? IdInventory : 0,
        LevelMaClass: $("#departamentSelect").val(),
        LevelName: $("#departamentSelect option:selected").text(),
        ActiveDetails: value,
        IsCreate: false,
    }];
}
/* Fin de botones */

/* tablas */
var tableLevel = $('#levelInventoryTable').DataTable({
    autoWidth: false,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [0, 1, 3, 4],
            visible: false,
            searchable: false,
            width: "1%"
        },
        {
            targets: [2],
            width: "90%"
        },
        {
            targets: [5],
            render: function (data, type, full, meta) {
                return `<button onclick="RemoveOption('N_${data.IdInventoryType}','N_${data.LevelMaClass}')" class="btn btn-danger"><i class="fa fa-trash"></i>  Eliminar</button>`;
            },
            width: "1%"
        },
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
    ],
    columns:
        [
            { data: 'IdInventoryType', width: "1%" },
            { data: 'LevelMaClass', width: "1%" },
            { data: 'LevelName', width: "80%" },
            { data: 'ActiveDetails', width: "1%" },
            { data: 'IsCreate', width: "1%" },
            { data: null, width: "20%" }
        ]
});

function RemoveOption(inventory, level) {
    console.log(LevelList.filter(x => x.IdInventoryType == inventory.split("N_")[1] && x.LevelMaClass == level.split("N_")[1])[0]);
    LevelList.filter(x => x.IdInventoryType == inventory.split("N_")[1] && x.LevelMaClass == level.split("N_")[1])[0].ActiveDetails = false;
    FillTableLevel(LevelList.filter(x => x.ActiveDetails));
    if (LevelList.filter(x => x.ActiveDetails).filter(x => x.IdInventoryType == inventory.split("N_")[1]).length == 0) {
        if (!IsEdit) {
            StatusForm(false);
        }
        else {
            $("#typeReviewSelect").prop("disabled", false);
            $("#daySelect").prop("disabled", false);
        }
    }
}

// -----------------------------------------------------
var tableInventory = $('#inventoryTypeTable').DataTable({
    autoWidth: false,
    responsive: true,
    order: [ 5, 'asc' ],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [0],
            visible: false,
            searchable: false,
            width: "1%"
        },
        {
            targets: [5],
            render: function (data, type, full, meta) {
                if (data) {
                    return `<div class="icheckbox_square-green
                            checked disabled" style="position: relative;">
                            <input type="checkbox" disabled="" style="position: absolute;
                            opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%;
                            left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                            background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`
                }
                else
                    return `<div class="icheckbox_square-green
                            disabled" style="position: relative;"><input type="checkbox" disabled=""
                            style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute;
                            top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                            background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`;
            },
            width: "1%"
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                return `

                <button type="button" onclick="ModalOpen('N_${full.IdInventoryType}')" class="btn btn-xs btn-outline btn-info" data-toggle="modal" data-target="#ModalDetalles" style="margin-left:10px;">
                       <i class='fa fa-info-circle'></i> Detalles
                    </button>`;
            },
            width: "1%"
        },
        {
            targets: [1],
            render: function (data, type, full, meta) {
                return `<button data-toggle="tooltip" data-placement="top" data-original-title="Mantener abierto el tipo de inventario" class="btn btn-xs btn-outline btn-warning" onclick="EditInventory('N_${full.IdInventoryType}')" style="margin-left:10px;"><i class="fa fa-pencil"></i>Editar</button> `;
            },
            width: "1%"
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                return moment(data).format('HH:mm');
            },
            width: "1%"
        },
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
    ],
    columns:
        [
            { data: 'IdInventoryType', width: "1%" },
            { data: null, width: "1%" },
            { data: 'InventoryName', width: "1%" },
            { data: 'TypeReview', width: "1%" },
            { data: 'Day', width: "1%" },
            { data: 'ActiveInventory', width: "1%" },
            { data: 'CloseTime', width: "1%" },
            { data: null, width: "1%" }

        ],
});

/* fin de las tablas*/
/* validaciones */
function StatusForm(value) {
    if (value) {
        $("#nameInventory").prop("disabled", true);
        $("#typeReviewSelect").prop("disabled", true);
        $("#daySelect").prop("disabled", true);
    }
    else {
        $("#nameInventory").prop("disabled", false);
        $("#typeReviewSelect").prop("disabled", false);
        $("#daySelect").prop("disabled", false);
    }
}

function FormClear() {
    $("#nameInventory").val("");
    $("#daySelect").val(0).trigger('change');
    $("#typeReviewSelect").val(0).trigger('change');
    $("#departamentSelect").val(0).trigger('change');
    $("#isActiveForm").iCheck('uncheck');
}

function FillTableLevel(List) {
    tableLevel.clear().draw();
    tableLevel.rows.add(List)
    tableLevel.columns.adjust().draw();
}

function FillTableInventory(List) {
    tableInventory.clear().draw();
    tableInventory.rows.add(List)
    tableInventory.columns.adjust().draw();
}

function SaveOrEdit(value) {
    if (value) {
        $("#editForm").hide();
        $("#saveForm").show();
    }
    else {
        $("#editForm").show();
        $("#saveForm").hide();
    }
}

function ClearForm() {
    StatusForm(false);
    FormClear();
    LevelList = []
    FillTableLevel(LevelList)
    IdInventory = 0;
    IsEdit = false;
    SaveOrEdit(true)
    $("#closeTime").val("00:00");
}

function EditInventory(idInventoryType) {
    let id = idInventoryType.split('N_')[1]
    axios.get(`/InventoryType/ActionGetInventoryTypeDatails/?IdInventoryType=${id}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data.length > 0) {
                $("#closeTime").val(data.data[0].CloseTime.substring(0, 5));
                LevelList = data.data;
                FillTableLevel(LevelList.filter(x => x.ActiveDetails));
                var inventoryType = ListInventoryType.filter(x => x.IdInventoryType == id)[0]
                IsEdit = true;
                IdInventory = id;
                $('#typeReviewSelect').val(inventoryType.TypeReview).trigger('change');
                if (inventoryType.TypeReview != "Indefinido") {
                    $('#daySelect').val(inventoryType.Day).trigger('change');
                }
                $("#departamentSelect").val(0).trigger('change');
                $("#nameInventory").val(inventoryType.InventoryName);
                $("#nameInventory").prop("disabled", true);

                if (inventoryType.ActiveInventory)
                    $("#isActiveForm").iCheck('check');
                else
                    $("#isActiveForm").iCheck('uncheck');
                SaveOrEdit(false)

                var position = $("#departamentSelect").offset()
                $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
            }
            else {
                toastr.error("No contiene Departamentos")
            }
        })
        .catch(error => {
            toastr.error('Error inesperado contactar a sistemas.');
        })
}

function ModalOpen(idInventoryType) {
    let id = idInventoryType.split('N_')[1]
    axios.get(`/InventoryType/ActionGetInventoryTypeDatails/?IdInventoryType=${id}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data.length > 0) {
                var inventoryType = ListInventoryType.filter(x => x.IdInventoryType == id)[0]
                console.log(inventoryType);
                $("#inventoryNameP").html(inventoryType.InventoryName)
                $("#typeReviewP").html(inventoryType.TypeReview)
                if (inventoryType.TypeReview != "Indefinido")
                    $("#dayP").html(moment(inventoryType.CloseTime).format('HH:mm'));
                else
                    $("#dayP").html("Indefinido")

                if (inventoryType.ActiveInventory)
                    $("#statusP").html('Activado');
                else
                    $("#statusP").html('Desactivado');

                let li = level => `<li>${level.LevelName}</li>`
                $("#levelUl").html(data.data.filter(x => x.ActiveDetails).map(x => li(x)))

            }
            else {
                toastr.error("No contiene Departamentos")
            }
        })
        .catch(error => {
            toastr.error('Error inesperado contactar a sistemas.');
        })

}
/*Fin de las validaciones*/

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    // ClockPicker
    $('.clockpicker').clockpicker({ autoclose: true });

    $("#daySelect").select2({
        formatInputTooShort: function () {
            return "Favor de poner una fecha";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#typeReviewSelect").select2({
        formatInputTooShort: function () {
            return "Favor de poner la revision";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#departamentSelect").select2({
        formatInputTooShort: function () {
            return "Favor de poner un departamento";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    var listModel = JSON.parse($("#model").val());
    ListInventoryType = listModel;
    FillTableLevel([]);
    FillTableInventory(listModel);
});