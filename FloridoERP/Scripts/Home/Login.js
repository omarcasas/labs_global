﻿
//VARIABLES RETURN DEL URL
var viewR = ""
var controller = ""
var urll = ""
$(document).ready(function () {
    urll = window.location.href;
    if (urll.split('ReturnUrl')[0] != "") {
        controller = urll.split('%2f')[1];
        viewR = urll.split('%2f')[2];
    }
});

$("#LoginForm").validate({
    rules: {
        Username: {
            required: true
        },
        Password: {
            required: true
        }
    },
    messages: {
        Username: {
            required: "El Campo no puede ser nulo"
        },
        Password: {
            required: "El Campo no puede ser nulo",
        }
    },
    submitHandler: function (form) {
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/Home/LoginEnter/",
            data: $(form).serialize(),
            success: function (returndate) {
                if (returndate.success == true) {
                    if (viewR == undefined && controller == undefined || urll.split('%2f')[3] != null)
                        window.location.href = 'http://' + window.location.host + '/Home/Index';
                    else {
                        window.location.href = 'http://' + window.location.host + '/' + controller + '/' + viewR;
                    }
                }
                else {
                    toastr.warning('' + returndate.responseText + '');
                    EndLoading();
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
            }
        });
        return false;
    }
});