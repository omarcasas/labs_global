/* da formato de moneda */
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal}).format(money);
/* valida que sea un decimal con los digitos que le mandaste */
const isDecimal = decimal => number => {
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
/* valida que tenga terminacion en .90 */
const is90 = number => {
    const numberis90 = /^\d+\.9$/;
    return numberis90.test(number)
}

/*sumar valores de una lista*/
const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0)
export { fomartmoney, isDecimal, is90, Sum}