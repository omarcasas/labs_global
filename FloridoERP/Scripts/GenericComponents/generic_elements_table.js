/* genera un td normal */
const tableTd = value => `<td>${value}</td>`;
/* genera un td con un input y su id */
const tableTdInputNumber = id =>  `<td><input class="form-control" type="number" min="0" id="${id}"></td>`;
/* genera un td con un input  check y su id */
const tableTdCheck = id =>  `<td><input class="form-control" type="checkbox" id="${id}"></td>`;
/* genera un td con un input, su id y funcion estandar */
const tableTdInputNumberFuction = id => type => `<td><input class="form-control" onkeyup="${type}('N_${id}')" type="number" min="0" id="N_${id}"></td>`;
/* genera un td con un input check, su id y funcion estandar */
const tableTdCheckFuction = id => type => `<td><input class="form-control" onchange="${type}Fuction(${id})" type="checkbox" id="${id}"></td>`;
/* genera un td con un id y agrega un valor */
const tableTdTotal = id => value => `<td id="${id}">${value}</td>`;

/* genera un td con color amarillo representando el error*/
const tableTdError = value => `<td style="color: black;background-color: yellow;">${value}</td>`;

/*agrega un th para la parte superior de la tabla*/
const tableTh = value => `<th>${value}</th>`

/* genera un boton de busqueda */
const tableTdClass =  className => value =>  `<td class="${className}">${value}</td>`;
export { tableTd, tableTdInputNumber, tableTdCheck, tableTdTotal, tableTdInputNumberFuction, tableTdCheckFuction, tableTdClass, tableTdError, tableTh}