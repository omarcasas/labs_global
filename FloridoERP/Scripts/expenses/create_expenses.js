﻿"use strict";
class CreateExpensesViewModel {
    constructor() {
        
        this.name = ko.observable("create");
        this.search_results = ko.observableArray();
        this.expense_list = ko.observableArray();
        this.search_text_input = ko.observable();
        this.to_new_projects = ko.observable(false);
        this.to_new_projects.subscribe(c => console.log(c));
        this.to_new_projects(false);
        this.search_text_input.extend({ rateLimit: { timeout: 300, method: "notifyWhenChangesStop" } });
        this.search_text_input.subscribe(this.search_on_text_input.bind(this));
        this.total_quantity = ko.observable(0);
        this.total_estimate_price = ko.observable(0);

        this.search_item_onclick = this.search_item_onclick.bind(this);
        this.registry_expense = this.registry_expense.bind(this);
        this.on_change_quantity = this.on_change_quantity.bind(this);
        this.on_change_estimate_price = this.on_change_estimate_price.bind(this);
    }
    search_item_onclick(item) {
        console.log(item);
        this.search_text_input("");
        if (this.expense_list().find(e =>
            e.part_number === item.part_number
            && e.supplier_name === item.supplier_name)) {
            this.search_text_input("");
            toastr.warning("este elemento ya esta en la lista");
            return;
        }
        this.expense_list.push(item);
        
    }

    async search_on_text_input(value) {
        console.log(value);
        if (!value) {
            this.search_results.removeAll();
             return;
        }
        const res = await Requests
            .get("/Expenses/ActionSearchItems", { search: value })
            .send_result();
        if (res.Ok) {
            this.search_results.removeAll();
            ko.utils.arrayPushAll(this.search_results,res.Data);
        }
    }
    async registry_expense() {
        if (this.expense_list().length < 1) return;
        const list = this.expense_list().filter(e => e.quantity > 0);
        let msg = "";
        if (this.total_estimate_price() >= 10000 || this.to_new_projects()) {
            msg = " se requerira confirmación de nuevos proyectos";
        }else if (this.total_estimate_price() >= 5000) {
            msg = " se requerira confirmación de distrital";
        }
        
        swal({
                title: "Registro de Gasto",
                text: msg,
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Aceptar"

            },
            async (va) => {
                if (!va) return;
                const r = await Requests.post("/Expenses/ActionExpenseStart")
                    .data({
                        Username: null,
                        ProgramId: "PURCH021",
                        Products: this.expense_list(),
                        ToNewProjects: this.to_new_projects,
                    }).send();
                if (r.Ok) {
                    toastr.success("Requisición registrada exitosamente");
                }
            });
    }

    on_change_quantity() {
        this.on_change_estimate_price();

    }
    on_change_estimate_price() {
        this.total_quantity(this.expense_list()
            .map(e => parseFloat(e.quantity))
            .reduce((a, b) => a + b), 0);
        this.total_estimate_price(this.expense_list()
            .map(e => parseFloat(e.quantity) * parseFloat(e.estimate_price))
            .reduce((a, b) => a + b),0);
    }

}


ko.applyBindings(new CreateExpensesViewModel(), document.getElementById('tab2'));