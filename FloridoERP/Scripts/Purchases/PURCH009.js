﻿listCheck = [];
listItem = [];
list = [];

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });

    $("#SuppliersDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#tipo").select2();
});

$('#fechaInicio').datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: new Date(),
});

//se agrego para validar
function CheckAndUncheck(id) {
    if ($("#" + id).is(':checked'))
        $("#" + id).iCheck('check');
    else
        $("#" + id).iCheck('check');
}

function OrderList() {
    if ($('#SuppliersDrop').val() != "" && $('#SuppliersDrop').val() != null) {
        $.ajax({
            url: "/Supplier/ActionGetItemsBySupplierCode",
            type: "GET",
            data: { "supplier_code": $('#SuppliersDrop').val() },
            success: function (returndates) {
                if (returndates.success == true) {
                    listCheck = [];
                    var table = "";
                    i = 0;
                    listItem = returndates.Json;
                    $.each(returndates.Json, function (index, value) {
                        if (list.length > 0) {
                            const result = list.filter(x => x.item_supplier_id == value.item_supplier_id);
                            if (result.length > 0) {
                                //se agrego la funcion en el input check si truena quitenla
                                table += "<tr class='active success'><td><div class='checkbox'><label id='" + 'l' + value.item_base_cost_id + "'><input type='checkbox' onclick='CheckAndUncheck('" + value.item_base_cost_id + "')' class='i-checks prueba' id='" + value.item_base_cost_id + "'></label></div></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_number + "</td><td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_name + "</td><td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_cost + "</td>";
                                table += "<td><input class='form-control' type='text' id='" + 'new_cost' + value.item_base_cost_id + "' style='text-align:center;font-family: Verdana; font-size:10pt' onkeyup='BlockGlobalQuantity(" + value.item_base_cost_id + ", " + i + ")'></td>";
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_currency + "</td></tr>"
                                listCheck[i] = { id: value.item_base_cost_id, supplier_item: value.item_supplier_id, currency: value.item_currency, base_cost: value.item_cost, new_cost: 0, check: false };
                            }
                            else {
                                table += "<tr class=''><td><div class='checkbox'><label id='" + 'l' + value.item_base_cost_id + "'><input type='checkbox' onclick='CheckAndUncheck('" + value.item_base_cost_id + "')'  class='i-checks prueba' id='" + value.item_base_cost_id + "'></label></div></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_number + "</td><td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_name + "</td><td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_cost + "</td>";
                                table += "<td><input class='form-control' type='text' id='" + 'new_cost' + value.item_base_cost_id + "' style='text-align:center;font-family: Verdana; font-size:10pt' onkeyup='BlockGlobalQuantity(" + value.item_base_cost_id + ", " + i + ")'></td>";
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_currency + "</td></tr>"
                                listCheck[i] = { id: value.item_base_cost_id, supplier_item: value.item_supplier_id, currency: value.item_currency, base_cost: value.item_cost, new_cost: 0, check: false };
                            }
                        }
                        else {
                            table += "<tr class=''><td><div class='checkbox'><label id='" + 'l' + value.item_base_cost_id + "'><input type='checkbox' onclick='CheckAndUncheck('" + value.item_base_cost_id + "')'  class='i-checks prueba' id='" + value.item_base_cost_id + "'></label></div></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_number + "</td><td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_name + "</td><td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_cost + "</td>";
                            table += "<td><input class='form-control' type='text' id='" + 'new_cost' + value.item_base_cost_id + "' style='text-align:center;font-family: Verdana; font-size:10pt' onkeyup='BlockGlobalQuantity(" + value.item_base_cost_id + ", " + i + ")'></td>";
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'>" + value.item_currency + "</td></tr>"
                            listCheck[i] = { id: value.item_base_cost_id, supplier_item: value.item_supplier_id, currency: value.item_currency, base_cost: value.item_cost, new_cost: 0, check: false };
                        }
                        i++;
                    });
                    $("#tableOrdered").html(table);
                    $(".textHidde").hide();
                    $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
                }
                else {
                    if (returndates.responseText == "Termino tu sesion.") {
                        SessionFalse(returndates.responseText);
                    }
                    else {
                        toastr.warning('Proveedor No Tiene Productos.');
                        $("#SuppliersDrop").select2('open');
                        $("#SuppliersDrop").val(0).trigger("change");
                        $("#tableOrdered").html("");
                    }
                }
            },
            error: function (returndates) {
                toastr.error('DESCONOCIDO, CONTACTE A SISTEMAS.');
            }
        });
    }
    else {
        $("#tableOrdered").html("");
    }
}

function SaveCost(listFinals) {
    listFinals = listFinals.filter(Boolean);
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Item/ActionUpdateCostBySupplier/",
        data: { items: listFinals, program_id: 'PURCH009.cshtml' },
        success: function (returndate) {
            EndLoading();
            if (returndate.success == true) {
                toastr.success(returndate.responseText + '.');
                Clear();
                list = listFinals;
            }
            else {
                if (returndate.responseText == "Termino tu sesión") {
                    SessionFalse(returndate.responseText)
                }
                else {
                    toastr.warning(returndate.responseText);
                }
            }
        },
        error: function (returndate) {
            EndLoading();
            toastr.error('DESCONOCIDO, CONTACTE A SISTEMAS.');
        }
    });
}

function BlockGlobalQuantity(id, e) {
    var x = "#new_cost" + id;
    if (!isNaN($(x).val())) {
        if (parseFloat($(x).val()) >= 0 && parseFloat($(x).val()) < 999999) {
            if ($(x).val() != "") {
                if ($(x).val() != null && $(x).val() >= 0) {
                    $('#cantidad').attr('disabled', 'disabled');
                    $('#tipo').attr('disabled', 'disabled');
                    $("#l" + id + " div.icheckbox_square-green").addClass('icheckbox_square-green checked');
                    $("#" + id).attr('checked', 'checked');
                    listCheck[e].new_cost = $(x).val();
                } else {
                    $(x).val("");
                    $('#cantidad').removeAttr('disabled', 'disabled');
                    $('#tipo').removeAttr('disabled', 'disabled');
                    $("#" + id).removeAttr('checked', 'checked');
                    $("#l" + id + " div.icheckbox_square-green").removeClass('checked');
                    $("#" + id).iCheck('uncheck');
                    listCheck[e].new_cost = $(x).val();
                }
            } else {
                toastr.warning('Ingresa un numero correcto.');
                $(x).val("");
                $('#cantidad').removeAttr('disabled', 'disabled');
                $('#tipo').removeAttr('disabled', 'disabled');
                $("#" + id).removeAttr('checked', 'checked');
                $("#l" + id + " div.icheckbox_square-green").removeClass('checked');
                $("#" + id).iCheck('uncheck');
                listCheck[e].new_cost = $(x).val();
            }
        } else {
            toastr.warning('Ingresa un numero correcto.');
            $(x).val("");
            $('#cantidad').removeAttr('disabled', 'disabled');
            $('#tipo').removeAttr('disabled', 'disabled');
            $("#" + id).removeAttr('checked', 'checked');
            $("#l" + id + " div.icheckbox_square-green").removeClass('checked');
            $("#" + id).iCheck('uncheck');
            listCheck[e].new_cost = $(x).val();
        }
    } else {
        toastr.warning('Ingresa un numero correcto.');
        $(x).val("");
        $('#cantidad').removeAttr('disabled', 'disabled');
        $('#tipo').removeAttr('disabled', 'disabled');
        $("#" + id).removeAttr('checked', 'checked');
        $("#l" + id + " div.icheckbox_square-green").removeClass('checked');
        $("#" + id).iCheck('uncheck');
        listCheck[e].new_cost = $(x).val();
    }
    ValidateNewCost(id);
}

function ValidateCost(valor) {
    if (valor != null && valor > 0) { }
    else {
        $('#cantidad').val("");
    }
}

function Clear() {
    $('#SuppliersDrop').val($("#SuppliersDrop :first").val()).trigger('change');
    $('#tipo').prop("selectedIndex", 0).trigger('change');
    $('#cantidad').val(0);
    $('#cantidad').removeAttr('disabled', 'disabled');
    $('#tipo').removeAttr('disabled', 'disabled');
    $('#fechaInicio').val("");
    $('#tableOrdered').html();
}

function ValidateNewCost(id) {
    const result = listCheck.filter(x => x.new_cost > 0).length;
    if (result > 0) {
        $('#cantidad').attr('disabled', 'disabled');
        $('#tipo').attr('disabled', 'disabled');
    }
    else {
        $('#cantidad').removeAttr('disabled', 'disabled');
        $('#tipo').removeAttr('disabled', 'disabled');
    }
}

function SaveNewCost() {
    if ($('#SuppliersDrop').val() == "") {
        toastr.warning('Selecciona Un Proveedor');
    }
    else if ($("#fechaInicio").val() == "") {
        toastr.warning('Selecciona Una Fecha');
    }
    else if ($("#cantidad").is(':disabled')) {
        listFinals = [];
        for (var x = 0; x < listCheck.length; x++) {
            listCheck[x].check = $('#' + listCheck[x].id).is(':checked')
            if ($('#' + listCheck[x].id).is(':checked') && $('#new_cost' + listCheck[x].id).val() != "" || $('#' + listCheck[x].id).is(':checked') && $('#new_cost' + listCheck[x].id).val() == "0") {
                listFinals[x] = { item_base_cost_id: listCheck[x].id, item_supplier_id: listCheck[x].supplier_item, item_currency: listCheck[x].currency, item_cost: listCheck[x].base_cost, fecha: $("#fechaInicio").val(), quantity: $('#new_cost' + listCheck[x].id).val(), type: "NO" };
            }
        }
        if (listFinals.length < 1) {
            toastr.warning('Selecciona productos para aplicar el cambio');
        }
        else {
            swal({
                title: "¿Esta seguro que desea guardar el cambio de costo?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#55dd6b",
                confirmButtonText: "Continuar",
                cancelButtonText: "Cancelar"
            }, function (isConfirm) {
                if (isConfirm)
                    SaveCost(listFinals);
            });
        }
    }
    else if ($("#tipo").val() == "0") {
        toastr.warning('Selecciona Un Tipo de Incremento');
    }
    else if (($("#cantidad").val() == "" | $("#cantidad").val() == 0 | $("#cantidad").val() < 0)) {
        toastr.warning('Cantidad invalida, debe ser mayor a 0.');
        $("#cantidad").val("");
    }
    else {
        listFinals = [];
        for (var x = 0; x < listCheck.length; x++) {
            listCheck[x].check = $('#' + listCheck[x].id).is(':checked')
            if ($('#' + listCheck[x].id).is(':checked')) {
                listFinals[x] = { item_base_cost_id: listCheck[x].id, item_supplier_id: listCheck[x].supplier_item, item_currency: listCheck[x].currency, item_cost: listCheck[x].base_cost, fecha: $("#fechaInicio").val(), quantity: $("#cantidad").val(), type: $('#tipo').val() };
            }
        }
        if (listFinals.length < 1) {
            toastr.warning('Selecciona productos para aplicar el cambio');
        }
        else {
            swal({
                title: "¿Esta seguro que desea guardar el cambio de costo?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#55dd6b",
                confirmButtonText: "Continuar",
                cancelButtonText: "Cancelar"
            }, function (isConfirm) {
                if (isConfirm)
                    SaveCost(listFinals);
            });
        }
    }
}