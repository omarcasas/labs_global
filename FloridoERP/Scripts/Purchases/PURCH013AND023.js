﻿var part_number_origin_comun_actual = ""; //Variable global del produto origin
var type_promotions_globlal = "";  //Variable global de promociones
var count = 0;
var IdPage = {
    Colors: [
        { status: 3, color: "warning", hex: false, liColor: "#f2dd79" }, { status: 1, color: "success", hex: false, liColor: "#9dd188" }, { status: 2, color: "danger", hex: false, liColor: "#d28d8d" },
        { status: 4, color: "#E0C6F9", hex: true, liColor: "#be88d1" }, { status: 5, color: "info", hex: false, liColor: "#7bc0e4" }
    ]
}
const isDecimal = decimal => number => { //Función para validar decimales
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
$(document).on("keypress", '.Numeric', function (event) { //Validador de numeros enteros
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});
$(document).on("keypress", '.Decimal', function (event) { //Validador de numero decimales
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    else
        return true;
});
function DateF(id) { //Inputs: Fechas genericas
    $(id).datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: moment().add(0, "days").format("MM/DD/YYYY")
    }).on("changeDate", function (e) { ValidateDate(); });
}
function ValidateDate() { //Validar fecha
    if ($('#fechaInicio').val() != "" && $('#fechaFin').val() != "") {
        if (moment($('#fechaInicio').val()) > moment($('#fechaFin').val())) {
            toastr.warning('La fecha inicial no puede ser mayor a la fecha final.');
            $('#fechaFin').val("");
        }
    }
}
$('#assignedList').slimScroll({ height: '200px' }); // Limite LARGO para las lista de tiendas.
const ValidateDateTable = data => moment(data).format('DD/MM/YYYY') != 'Invalid date' ? moment(data).format('DD/MM/YYYY') : "";
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money); //Formato de Decimales
const is90 = number => { //Validación .90
    const numberis90 = /^\d+\.9$/;
    return numberis90.test(number)
}
function blockSites() { $("#bodySites").css({ 'display': "none" }); } // Para desmarcar los precios
function ErrorList(value, type, text) { return !value ? ToastError(type, text) : type == "Succes" ? ToastError(type, text) : true; } //Texto generico
function HidenOrCheckFull(id, value) { value ? $(id).show() : $(id).hide() }  // Oculta y muestra inputs
function ClearRepeatProduct(inputName, productName) {
    $(`#${inputName}`).val('').trigger('change');
    $(`#${inputName}`).select2("open");
    toastr.warning(`Producto ya seleccionado: ${productName}`);
    return false;
}
function ToastError(type, text) {
    if (type == "Succes") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error")
        toastr.error(text)
    else if (type == "Warning")
        toastr.warning(text)
    return false;
}
function ClearTable(table, list) { /// Limpia tablas
    table.clear().draw();
    table.rows.add(list);
    table.columns.adjust().draw();
}
function ValidateTableEmpty() { //Valida que si la tabla no tiene datos muestre visibles muestre por defecto un mensaje predeterminado
    if (tableComun.data().toArray().filter(f => f.delete_flag == false && f.part_number_origin == part_number_origin_comun_actual).length == 0) {
        $("#tableThree > tbody ").empty();
        $("#tableThree > tbody ").append(`<tr class='odd'><td valign='top' colspan='4' class='dataTables_empty'>Ningún dato disponible en esta tabla</td></tr>`);
    }
    if (TableItemsGlobalPromotions.data().toArray().filter(f => f.delete_flag == false).length == 0) {
        $("#TableGlobalPromotions > tbody ").empty();
        var cant = type_promotions_globlal == 'AVANZADA' ? 8 : 5;
        $("#TableGlobalPromotions > tbody ").append(`<tr class='odd'><td valign='top' colspan='${cant}' class='dataTables_empty'>Ningún dato disponible en esta tabla</td></tr>`);
    }
}
function clearProduct() { //Limpiar inputs los productos
    $("#ItemsDrop").val("").trigger("change");
    $("#select2-chosen-1").html("Busque un Producto.");
    $("#productoComunDrop").val('').trigger('change');
    $("#productoComunDrop").select2("open");
}
function ClearAll() {  //Limpiar todas las variables
    $("#tipo").prop("disabled", false);
    $("#promotion_description").prop("disabled", false);
    $('#totalFinal').html("0.0");
    $('input[name="available"]:checked').iCheck('uncheck');
    ["promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, false));
    ["fechaInicio", "fechaFin", "promotion_description", "ItemsDrop"].map(id => $(`#${id}`).val(""))
    $("#tipo").val("").trigger("change");
    blockSites();
    clearProduct();
}
function SwalFunc(titleSwal) { //Mensaje generico de swal
    swal({
        title: titleSwal,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    },
        function (isConfirm) { if (isConfirm) { SavePromotion(); } });
}
function validateTypePromotion(type_promotion) { //Hace dinamica con las tablas
    if (type_promotion != "AVANZADA") {
        ["promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, false));
        $('#text_promotion').html("Agregar Productos");
        $('#ItemsDrop').empty();
        //type_promotions_globlal = 'AVANZADA';
        type_promotions_globlal = $("#tipo").val();
        TableItemsGlobalPromotions.columns([3, 4, 6]).visible(false);
    } else {
        ["promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, true));
        $('#ItemsDrop').empty();
        $('#text_promotion').html("Descripcion de la oferta avanzada y agregar productos.");
        //type_promotions_globlal = $("#tipo").val();
        type_promotions_globlal = 'AVANZADA';
        TableItemsGlobalPromotions.columns([3, 4, 6]).visible(true);
    }
    ClearTable(TableItemsGlobalPromotions, []);
    ClearTable(tableComun, [])
    clearProduct();
}
function clearAllInputsComun() { /// Limpiar los input y variables de modal
    ["cantidadComun", "precioComun"].map(id => $(`#${id}`).val(""))
    $('#productoComunDrop').val($("#productoComunDrop :first").val()).trigger('change');
    part_number_origin_comun_actual = '';
}
function Select2F(id, text) { //Select2 genericos
    $(id).select2({
        minimumInputLength: 2,
        dropdownAutoWidth: true,
        height: '300px',
        formatInputTooShort: function () { return text; },
        matcher: function (term, text, option) { return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0; }
    });
}
function ValidateFieldsPromotions() { //Validaciones: Cuenta si ha seleccionado tiendas, fechas, tipos de promoción
    if ($('#tipo').val() == "") {
        toastr.warning('Seleccione un tipo de promoción');
        return false;
    }
    type_promotions_globlal = $("#tipo").val();
    sitesL = [];
    $.each($("input[name='available']:checked"), function () {
        sitesL += $(this).val() + ',';
    });
    if (type_promotions_globlal == "AVANZADA") {
        if ($('#promotion_description').val() == null || $('#promotion_description').val().trim() == "") {
            document.getElementById("promotion_description").focus();
            toastr.warning('Ingrese alguna descripción de la promoción.');
            $("#ItemsDrop").val("");
            $("#select2-chosen-1").html("Buscar un Producto.");
            return false;
        }
    }
    if (ErrorList($('#fechaInicio').val() != "", "Warning", "Ingrese una fecha inicial.") &&
        ErrorList($('#fechaFin').val() != "", "Warning", "Ingrese una fecha final.") &&
        ErrorList(moment($('#fechaInicio').val()) <= moment($('#fechaFin').val()), "Warning", "La fecha inicial no puede ser mayor a la fecha final.") &&
        ErrorList(type_promotions_globlal != "", "Warning", 'Selecciona Un Tipo de Oferta') &&
        ErrorList(sitesL.length > 0, "Warning", 'Selecciona al menos 1 Tienda')) {
        return true;
    } else {
        clearProduct();
        return false;
    }
}
function Select2AjaxF(name, status3) { //Select2 generico
    $("#" + name).select2({
        minimumInputLength: 3,
        allowClear: true,
        dropdownAutoWidth: true,
        height: '300px',
        placeholder: "Seleccione una producto",
        initSelection: function (element, callback) { callback({ id: "", text: "Busque un producto." }); },
        ajax: {
            url: "/Item/ActionGetItemPresentationLike",
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (Filter) {
                return {
                    Filter: Filter,
                    status: status3 ? 0 : (type_promotions_globlal.value != undefined ? type_promotions_globlal.value != 'AVANZADA' : type_promotions_globlal != 'AVANZADA') ? 1 : 0,
                };
            },
            results: function (data) {
                return {
                    results: $.map(data.Json, function (item) {
                        return {
                            text: item.part_description,
                            id: item.part_number
                        }
                    }),
                };
            }
        }
    });
}
jQuery(document).ready(function () {
    $("#tipo").select2();
    DateF('#fechaInicio')
    DateF('#fechaFin')
    $('#assignedList').find('input[name="available"]').iCheck({ checkboxClass: 'icheckbox_square-green' });//Le da formato a los checks
    //Chequea todos los checks
    $('.iCheck-helper:first').on('click', function () {
        if ($("input[type='checkbox']:checked:first").is(':checked') && count == 1) {
            $('input[name="available"]').iCheck('check');
            count = 2;
        }
        else {
            count = 1;
            $('input[name="available"]:checked').iCheck('uncheck');
        }
    });

    $('.iCheck-helper:gt(0)').on('click', function () {
        if ($("input[name='available']:gt(0)").length == $("input[name='available']:gt(0):checked").length) {
            $('input[name="available"]:first').iCheck('check');
            count = 2;
        }
        else {
            count = 1;
            $('input[name="available"]:first').iCheck('uncheck');
        }
    });

});
var TableItemsGlobalPromotions = $('#TableGlobalPromotions').DataTable({ // Tabla de promociones volates y avanzadas
    "paging": false, "ordering": false, "searching": false, "ordering": false, "paging": false, "info": false, "searching": false, responsive: true, buttons: [], order: [[2, "asc"]], dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>", lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (aData.delete_flag == true) //Esconde los eliminados
            $(nRow).hide();
    },
    columnDefs: [
        {
            targets: [12, 11, 10, 9, 8],
            visible: false
        },
        {
            targets: [2, 4],
            render: function (data, type, full) {
                return `<p>${fomartmoney(data)(4)}</p>`
            }
        },
        {
            width: "1%",
            targets: [3],
            render: function (data, type, full, meta) {
                return `<div class='input-group'><span class='input-group-addon Numeric'><span class='fa fa-cubes'></span></span><input placeholder='Cantidad' type='text' onchange='ValidaQuantityNew(this.value,${meta.row});' value=${data} class='form-control Numeric'></div>`
            }
        },
        {
            width: "1%",
            targets: [5],
            render: function (data, type, full, meta) {
                return `<div class='input-group'><span class='input-group-addon'><span class='fa fa-dollar'></span></span><input placeholder='Precio' type='text' onchange='ValidatePriceNew(this.value,${meta.row});' value=${data} class='form-control Decimal'></div>`
            }
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                return data ? `<button class='btn btn-xs btn-success' onclick='AddEditCommun("${full.part_number}");'><i class='fa fa-cubes'></i> </button>` : `<button class='btn btn-xs btn-primary' onclick='AddEditCommun("${full.part_number}")'><i class='fa fa-cubes'></i></button>`
            }
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                return "<button class='btn btn-xs btn-danger btn-circle' onclick='DeleteProductPromotionToTable(" + meta.row + ")'  style='text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;'><i class='fa fa-times'></i> </button>"
            }
        }
    ],
    columns: [
        { data: 'part_number', width: "1%", }, //0
        { data: 'part_description', width: "3%", }, //1
        { data: 'price_iva_current', width: "1%", }, //2
        { data: 'quantity_promotion', width: "1%", }, //3
        { data: 'total_price_current', width: "1%", }, //4
        { data: 'total_price_promotion', width: "1%", }, //5
        { data: 'comun_flag', width: "1%", }, //6
        { data: 'delete_flag', width: "1%", }, //7
        { data: 'update_flag', width: "1%", visible: false }, //8
        { data: 'presentation_flag', width: "1%", visible: false }, //9
        { data: 'weight_flag', width: "1%", className: "text-center", visible: false }, //10
        { data: 'forced90', width: "1%", className: "text-center", visible: false }, //11
        { data: 'presentation_id', width: "1%", className: "text-center", visible: false } //12
    ]
});
var tableComun = $('#tableThree').DataTable({ /// Tabla común (modal)
    "paging": false, "ordering": false, "searching": false, "ordering": false, "paging": false, "info": false, "searching": false, responsive: true, buttons: [], order: [[2, "asc"]], dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>", lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (part_number_origin_comun_actual != "") {
            if (aData.delete_flag == false && aData.part_number_origin != part_number_origin_comun_actual || aData.delete_flag == true && aData.part_number_origin != part_number_origin_comun_actual || aData.delete_flag == true && aData.part_number_origin == part_number_origin_comun_actual) // No mostrar
                $(nRow).hide();
            else if (aData.delete_flag == false && aData.part_number_origin == part_number_origin_comun_actual) //Solo mostrar cuando no este eliminado y sea del código 'part_number_origin_comun_actual'
                $(nRow).show();
        }
    },
    columns: [
        { data: 'part_number_origin', visible: false }, //0
        { data: 'part_number' }, //1
        { data: 'part_description' }, //2
        { data: 'promotion_past_price' }, //3
        { data: null }, //4
        { data: 'delete_flag', visible: false }, //5
        { data: 'update_flag', visible: false }, //6
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6],
        width: "1%"
    }],
    aoColumnDefs: [
        {
            aTargets: [3],
            mRender: function (data, type, full) {
                return `<p>${fomartmoney(data)(4)}</p>`
            }
        },
        {
            aTargets: [4],
            render: function (data, type, full, meta) {
                return "<button class='btn btn-xs btn-danger btn-circle' onclick='DeleteProductPromotionToTableCommun(" + meta.row + ")'  style='text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;'><i class='fa fa-times'></i> </button>"
            }
        }]
});
var tableCommons = $('#TableCommons').DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "NingÃºn dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns: [
        { targets: 0, data: 'part_number' },
        { targets: 1, data: 'description' },

    ],
    columnDefs: [{
        targets: [0, 1],
        width: "1%"
    }]
});

var tblPromotionsRepeat = $(`#TablePromotionsRepeat`).DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": ">>",
        },
        { targets: 1, data: 'promotion_code' },
        { targets: 2, data: 'promotion_descrip' },
        { targets: 3, data: 'promotion_type' },
        { targets: 4, data: 'promotion_start_date' },
        { targets: 5, data: 'promotion_end_date' },
        { targets: 6, data: 'promotion_create_user' },
        { targets: 7, data: 'promotion_date_create' },
        { targets: 8, data: 'promotion_site_code', visible: false },
        { targets: 9, data: 'promotion_site_name', visible: false },
        { targets: 10, data: 'replication_status', visible: false },
        { targets: 11, data: null, visible: true },
        { targets: 12, data: null, visible: false },
        { targets: 13, data: null, visible: false }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        width: "1%"
    },
    {
        targets: [2],
        render: function (data, type, row) {
            if (data != '')
                return data;
            else
                return 'Sin descripción.';
        }
    },
    {
        targets: [4, 5, 7],
        render: function (data, type, row) {
            return ValidateDateTable(data)
        }
    },
    {
        targets: [11],
        render: function (data, type, full, meta) {
            return `<button class="btn btn-xs btn-outline btn-info" onclick="ShowStore('${data.promotion_code}', '${data.promotion_descrip}', '${data.promotion_type}', ${data.promotion_date_start}, ${data.promotion_date_end}, '${data.promotion_create_user}', ${data.promotion_date_create})"><i class="fa fa-cart-arrow-down"></i>  Tiendas</button>`;
        }
    }]
});
ClearAll(); /// Dibujar tablas y esconder algunos valores
function SaveMessage() { // Mensaje de confirmación para guardar
    var plipo = type_promotions_globlal.value != undefined ? type_promotions_globlal.value : type_promotions_globlal;
    if (ValidateFieldsPromotions()) {
        if (plipo == "AVANZADA") {
            if (ValidateSumProducts(true)) {
                SwalFunc("¿Esta seguro que desea guardar la oferta AVANZADA? El precio final de la promoción es " + document.getElementById("totalFinal").innerHTML);
            }
        } else {
            if (ValidateAllProductsIs90()) {
                SwalFunc("¿Esta seguro que desea guardar la oferta?");
            }
        }
    }
}
function SavePromotion() { // Envia los productos para actualizar la base de datos - PURCH013
    if (ValidateFieldsPromotions()) {
        var url = "";
        var productItems = [];
        var productItemsComun = [];
        var model = [];
        var sitess = [];
        if (type_promotions_globlal == "AVANZADA") {
            if (!ValidateSumProducts(true)) //Validación que la suma termine en .90
                return false;
            url = "/Promotion/ActionCreatePromotionAdvanced";
            productItemsComun = tableComun.data().toArray()
                .filter(f => f.delete_flag == false)
                .map((value, i, array) => { return { part_number_origin: value.part_number_origin, part_number: value.part_number } });
            productItems = TableItemsGlobalPromotions.data().toArray()
                .filter(element => element.delete_flag == false)
                .map((value, i, array) => { return { part_number: value.part_number, promotion_price: value.total_price_promotion, quantity_total: value.quantity_promotion, common_id: TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == value.part_number && f.delete_flag == false), common_flag: value.comun_flag, presentation_id: 0 } });
        } else {
            if (!ValidateAllProductsIs90()) //Validacición que termine la todos los productos en .90
                return false;
            url = "/Promotion/ActionCreatePromotion/";
            productItems = TableItemsGlobalPromotions.data().toArray()
                .filter(element => element.delete_flag == false)
                .map((value, i, array) => { return { part_number: value.part_number, promotion_price: value.total_price_promotion, quantity_total: value.quantity_promotion, common_id: 0, common_flag: value.comun_flag, presentation_id: value.presentation_id, presentation_flag: value.presentation_flag } });
        }
        model = { "promotion_type": type_promotions_globlal, "promotion_start_date": $('#fechaInicio').val(), "promotion_end_date": $('#fechaFin').val(), promotion_descrip: $('#promotion_description').val() }
        sitess = { "site_code": sitesL }
        StartLoading();
        $.ajax({
            type: "POST",
            url: url,
            data: { "model": model, "items": productItems, "sites": sitess, "itemsCommon": productItemsComun },
            success:
                function (returndata) {
                    EndLoading();
                    if (returndata.success) {
                        swal({ title: 'Promoción generada: ' + returndata.responseText, type: 'success' })
                        ClearAll();
                    }
                    else {
                        if (returndata.responseTextNumber == 10)
                        {
                            console.log("returndata.promotion");
                            console.log(returndata.promotion);
                            tblPromotionsRepeat.clear();
                            tblPromotionsRepeat.rows.add(returndata.promotion);
                            tblPromotionsRepeat.columns.adjust().draw();
                            $('#ModalPromotionsRepeat').modal('show');
                        } else {
                            if (returndata.responseText == "SF")
                                SessionFalse("Su sesión a terminado.")
                            else
                                toastr.warning(returndata.responseText);
                        }
                    }
                },
            error: function () {
                toastr.error('Error inesperado contactar a sistemas');
                EndLoading();
            }
        });
    }
}
function ItemViewModelPromotion() {
    var self = this;
    self.part_number = ko.observable();
    self.part_description = ko.observable();
    self.price_iva_current = ko.observable();
    self.quantity_promotion = ko.observable();
    self.total_price_current = ko.observable();
    self.total_price_promotion = ko.observable();
    self.comun_flag = ko.observable();
    self.delete_flag = ko.observable();
    self.update_flag = ko.observable();
    self.presentation_flag = ko.observable();
    self.weight_flag = ko.observable();
    self.price_part_ieps = ko.observable();
    self.price_part_iva = ko.observable();
    self.price_part_iva = ko.observable();
    self.forced90 = ko.observable();
    self.presentation_id = ko.observable();
    self.GetInfoProduct = function (inputValue) { //Buscar informacion del producto
        if ($(`#${inputValue}`).val() == "" || $(`#${inputValue}`).val() == 0 || $(`#${inputValue}`).val() == null)
            return false;
        var splitDescription = $(`#${inputValue}`).select2('data').text.split('~');
        var splitPartNumber = $(`#${inputValue}`).select2('data').id.split('~');
        if (ValidateFieldsPromotions()) {
            $.ajax({
                url: '/Item/ActionGetItemPriceIvaIeps',
                type: "GET",
                data: { "part_number": $(`#${inputValue}`).val(), },
                success:
                    function (returndata) {
                        if (returndata.success == true) {
                            self.part_number(splitPartNumber[0]);
                            self.part_description(splitDescription[1]);
                            self.price_part_ieps(returndata.itemC.Ieps);
                            self.price_part_iva(returndata.itemC.Iva);
                            self.weight_flag(returndata.itemC.weight_flag);
                            self.price_iva_current(parseFloat(returndata.itemC.Price));
                            self.presentation_flag(returndata.itemC.Presentation);
                            self.forced90(returndata.itemC.forced_90);
                            self.presentation_id(returndata.itemC.presentation_id);
                            if (type_promotions_globlal == 'AVANZADA')
                                self.forced90(false);
                            return true;
                        } else {
                            toastr.error('Este producto no tiene información.');
                            return false;
                        }
                    },
                error:
                    function () {
                        toastr.error('Error inesperado contactar a sistemas.');
                        return false;
                    }
            });
        }
    }
    self.AddProductToTable = function () { //Agregar Producto a la tabla
        var checkProduct = true;
        var productNew = [];
        if (ValidateFieldsPromotions() &&
            ErrorList($('#ItemsDrop').val() != "", "Warning", "Seleccione un producto.")) {
            if (TableItemsGlobalPromotions.data().count() > 0) {
                $(TableItemsGlobalPromotions.data()).each(function (index, elem) { //Saber si un producto se encuentra repetido
                    if (self.presentation_flag()) {//Presentaciones 
                        if (elem.presentation_id == self.presentation_id() && elem.delete_flag == false)
                            checkProduct = ClearRepeatProduct("ItemsDrop", elem.part_description);
                    }
                    else                        //Producto
                        if (elem.part_number == self.part_number() && elem.delete_flag == false)
                            checkProduct = ClearRepeatProduct("ItemsDrop", elem.part_description);

                });
            }
            if (!checkProduct)
                return false; //Si encontro un producto repetido
            productNew.push({ 'part_number': self.part_number(), 'part_description': self.part_description(), 'price_iva_current': self.price_iva_current(), 'quantity_promotion': 1, 'total_price_current': 1 * self.price_iva_current(), 'total_price_promotion': 0, 'comun_flag': false, 'delete_flag': false, 'update_flag': true, 'presentation_flag': self.presentation_flag(), 'weight_flag': self.weight_flag(), 'forced90': self.forced90(), 'presentation_id': self.presentation_id() });
            TableItemsGlobalPromotions.rows.add($(productNew));
            TableItemsGlobalPromotions.columns.adjust().draw();
            clearProduct();
        }
    }
    self.AddProductToTableComun = function () { //Agregar Producto a la tabla
        var checkProduct = true;
        var productNew = [];
        if (part_number_origin_comun_actual == $(`#productoComunDrop`).select2('data').id) //Si el producto es el mismo que el 'origin'
            return ClearRepeatProduct("productoComunDrop", $(`#productoComunDrop`).select2('data').text);
        if (ValidateFieldsPromotions() &&
            ErrorList($('#productoComunDrop').val() != "", "Warning", "Seleccione un producto.")) {
            if (TableItemsGlobalPromotions.data().count() > 0) {
                $(tableComun.data()).each(function (index, elem) { //Saber si un producto se encuentra repetido
                    if (elem.part_number_origin == part_number_origin_comun_actual && elem.part_number == self.part_number() && elem.delete_flag == false)
                        checkProduct = ClearRepeatProduct("productoComunDrop", elem.part_description);
                });
            }
            if (!checkProduct)
                return false; //Si encontro un producto repetido
            productNew.push({ 'part_number_origin': part_number_origin_comun_actual, 'part_number': self.part_number(), 'part_description': self.part_description(), 'promotion_past_price': self.price_iva_current(), 'delete_flag': false, 'update_flag': true });
            tableComun.rows.add($(productNew));
            tableComun.columns.adjust().draw();
            TableItemsGlobalPromotions.cell(TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false), 6).data(true).draw(); //Update button green
            TableItemsGlobalPromotions.cell(TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false), 8).data(true).draw(); //Update_flag
            TableItemsGlobalPromotions.columns.adjust().draw();
            clearProduct();
        }
    }
}
var vm = new ItemViewModelPromotion();
ko.applyBindings(vm);
function DeleteProductPromotionToTable(index) { //Eliminación de productos
    TableItemsGlobalPromotions.cell(index, 7).data(true).draw(); //Delete_flag
    TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
    TableItemsGlobalPromotions.columns.adjust().draw();//Actualizar
    if (tableComun.data().toArray().filter(f => f.delete_flag == false && f.part_number_origin == TableItemsGlobalPromotions.cell(index, 0).data()).length > 0) { //Eliminar productos comunes
        $(tableComun.data()).each(function (indexComun, elem) {
            if (elem.part_number_origin == TableItemsGlobalPromotions.cell(index, 0).data() && elem.delete_flag == false) {
                tableComun.cell(indexComun, 5).data(true).draw(); //Comunes: Delete_flag
                tableComun.cell(indexComun, 6).data(true).draw(); //Comunes: Update_flag
            }
        });
    }
    ValidateSumProducts(false);
    ValidateTableEmpty();
}
function DeleteProductPromotionToTableCommun(index) { //Elimina productos comunes
    tableComun.cell(index, 5).data(true).draw(); //Delete_flag
    tableComun.cell(index, 6).data(true).draw(); //Update_flag
    tableComun.columns.adjust().draw();
    ValidateTableEmpty();
}
function AddEditCommun(part_number_origin) {
    clearAllInputsComun();
    part_number_origin_comun_actual = part_number_origin; //Importante: siempre sabra cual es el código original
    $('#descripctionComun').val(part_number_origin + " - " + TableItemsGlobalPromotions.data().toArray().filter(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false)[0].part_description);
    $('#precioComun').val(TableItemsGlobalPromotions.data().toArray().filter(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false)[0].total_price_promotion);
    $('#cantidadComun').val(TableItemsGlobalPromotions.data().toArray().filter(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false)[0].quantity_promotion);
    tableComun.columns.adjust().draw();
    ValidateTableEmpty();
    $('#ModalAddPages').modal('show');
}
function ValidatePriceNew(value, index) { //Validaciones de nuevo precio
    var isRestricted90 = TableItemsGlobalPromotions.cell(index, 11).data();
    if (ErrorList(!isNaN(value), "Warning", "Ingrese un numero correcto por favor.") &&
        ErrorList(isDecimal(4)(parseFloat(value)), "Warning", "Ingrese un numero con cuatro decimales o menos.") &&
        ErrorList((value >= 0.0001 && value < 99999999), "Warning", "Ingrese una cantidad razonable.")) {
        if (type_promotions_globlal != 'AVANZADA') {
            if (isRestricted90) { //Valida si es necesario el terminacion .90 exclusivamente este producto
                if (ErrorList(is90(parseFloat(value)), "Warning", "Precio debe terminar en .90")) { //Debe terminar .90
                    TableItemsGlobalPromotions.cell(index, 5).data(value).draw();
                    TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
                    return true;
                } else {
                    TableItemsGlobalPromotions.cell(index, 5).data(0).draw();
                    TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
                    return false;
                }
            } else { //No es necesario terminar en .90 exclusivamente este producto
                TableItemsGlobalPromotions.cell(index, 5).data(value).draw();
                TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
                return true;
            }
        } else { //No es necesario terminar en .90 por promoción avanzada
            TableItemsGlobalPromotions.cell(index, 5).data(value).draw();
            TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
            ValidateSumProducts(false);
            return true;
        }
    } else { //Error en el precio y coloca 0
        TableItemsGlobalPromotions.cell(index, 5).data(0).draw();
        TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
    }
    return false;
}
function ValidaQuantityNew(value, index) {//Valida la cantidad a modificar
    var quantity = parseInt(value);
    if (ErrorList(isDecimal(0)(parseFloat(value)), "Warning", "Ingrese una cantidad sin decimales.") &&
        ErrorList((parseFloat(quantity) > 0.1 && parseFloat(quantity) < 999999), "Warning", "Ingrese una cantidad correcta.")) {
        TableItemsGlobalPromotions.cell(index, 3).data(quantity);
        TableItemsGlobalPromotions.cell(index, 4).data(TableItemsGlobalPromotions.cell(index, 2).data() * quantity).draw();
        return true;
    } else
        TableItemsGlobalPromotions.cell(index, 3).data(1).draw();
    return false;
}
function ValidateAllProductsIs90() { //Valida los precios que todos terminen en .90
    if (TableItemsGlobalPromotions.data().toArray().filter(f => f.delete_flag == false).length == 0) {
        toastr.warning("Ingrese al menos un producto.");
        return false;
    }
    var prices = TableItemsGlobalPromotions.data().toArray()
        .filter(element => element.delete_flag == false && element.forced90 == true)
        .reduce(function (filtered, option) {
            if (!is90(parseFloat(option.total_price_promotion))) {
                var priceInvalid = { description: option.part_description, part_number: option.part_number };
                filtered.push(priceInvalid);
            }
            return filtered;
        }, []);
    if (prices.length == 0)
        return true;
    else {
        prices.forEach(function (value) {
            toastr.warning(`El producto ${value.part_number} - ${value.description} su precio debe terminar en .90`);
        });
        return false;
    }
}
function ValidateSumProducts(validate) {//Valida que la suma sea .90
    if (validate && (TableItemsGlobalPromotions.data().toArray().filter(f => f.delete_flag == false).length == 0)) {
        toastr.warning("Ingrese al menos un producto.");
        return false;
    }
    var sumPrices = TableItemsGlobalPromotions.data().toArray()
        .filter(element => element.delete_flag == false)
        .reduce((a, b) => parseFloat(a) + parseFloat(b.total_price_promotion), 0);
    $('#totalFinal').html(fomartmoney(sumPrices)(4));
    if (is90(sumPrices))
        return true;
    else {
        if (validate) {
            toastr.warning("La suma de los productos debe terminar en .90");
            return false;
        }
    }
}


function ShowStore(folio, descrip, type, start, end, user, date) {
    $.ajax({
        type: "GET",
        url: "/Promotion/ActionGetPromotionByIdSites",
        data: { "FolioPromotion": folio },
        success: function (response) {
            if (response) {
                var l = response.length;
                [
                    { id: "listStores1", range: { number1: 0, number2: 11 } }, { id: "listStores2", range: { number1: 11, number2: 21 } },
                    { id: "listStores3", range: { number1: 21, number2: l } }
                ].map(elements => {
                    var ul = document.getElementById(elements.id);
                    ul.innerHTML = '';
                    LiGenerator(elements.range.number1, elements.range.number2, response, ul)
                })
                if (descrip == "")
                    descrip = "Sin descripción.";
                var dateStart = moment(start).format('DD/MM/YYYY');
                var dateEnd = moment(end).format('DD/MM/YYYY');
                var dateCreate = moment(date).format('DD/MM/YYYY');
                $('#folioOferta').html(folio);
                $('#descriptionOferta').html(descrip);
                $('#tipoOferta').html(type);
                $('#inicioStart').html(dateStart);
                $('#finOferta').html(dateEnd);
                $('#userOferta').html(user);
                $('#createOferta').html(dateCreate);
                $('#ModalPromotionsRepeat').modal('toggle');
                setTimeout(function () { $('#showStoresRepeat').modal({ backdrop: 'static', keyboard: false }); }, 400);
            } else {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}

function LiGenerator(number1, number2, response, ul) {
    for (var i = number1; i < number2; i++) {
        var li = document.createElement("li");
        if (typeof response[i] !== 'undefined') {
            li.appendChild(document.createTextNode(`  ${response[i].SiteName}  `));
            ul.appendChild(li);
            color = IdPage.Colors.find(element => element.status == response[i].Status);
            li.style.backgroundColor = color != undefined ? color.liColor : "";
        }
    }
}

var detailRows = [];
$('#TablePromotionsRepeat tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tblPromotionsRepeat.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tblPromotionsRepeat.row('.details').length) {
            $('.details-control', tblPromotionsRepeat.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

tblPromotionsRepeat.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    var styleColor = '';
    $.ajax({
        url: "/Promotion/ActionGetPromotionById",
        data: { "FolioPromotion": d.promotion_code, "type": d.promotion_type },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                let ifExists = TableItemsGlobalPromotions.data().toArray().filter(element => element.delete_flag == false && element.part_number == value.part_number);
                if (ifExists.length > 0)
                    styleColor = "class='warning';";
                else
                    styleColor = '';
                if (d.promotion_type == "Avanzada") {
                    if (value.common == 0) {
                        detailItems += `<tr ${styleColor}><td> ${value.part_number}</td><td>${value.description}</td><td>${value.quantity}</td><td>${fomartmoney(value.price)(4)}</td><td><button class='btn btn-xs btn-primary' onclick='MessageNoProducts()'><i class='fa fa-cubes'></i> Productos</button></td>`;
                    } else {
                        detailItems += `<tr ${styleColor}><td> ${value.part_number}</td><td>${value.description}</td><td>${value.quantity}</td><td>${fomartmoney(value.price)(4)}</td><td><button class="btn btn-xs btn-success" onclick="ShowPromotionCommonItem('${value.promotion_code}', '${value.part_number}', '${value.price}', '${value.quantity}', '${value.description}')"><i class="fa fa-cubes"></i> Productos</button></td>`;
                    }
                }
                else
                    detailItems += `<tr ${styleColor}><td>${value.part_number}</td><td>${value.description}</td><td>${fomartmoney(value.price)(4)}</td>`
            });
            if (d.promotion_type == "Avanzada")
                tabledetail.html('<table id="tabledetail' + d.promotion_code + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Monto Original</th><th>Precio Promocion</th><th>Comun</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            else
                tabledetail.html('<table id="tabledetail' + d.promotion_code + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Precio Promocion</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable(d);
        }
    });
    return tabledetail;
}

function reloadStyleTable(d) {
    $('#tabledetail' + d.promotion_code).DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningun dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function ShowPromotionCommonItem(promotion_code, common, price_promo, quantity_promo, descrip_part_number) {
    $.ajax({
        type: "GET",
        url: "/Promotion/ActionGetPromotionCommon",
        data: { "promotion_code": promotion_code, "common": common, },
        success: function (response) {
            if (response.status) {
                $('#folioOferta').html(promotion_code);
                $('#descriptionOferta').html(common);
                $('#tipoOferta').html(descrip_part_number);
                $('#precioOferta').html("$ " + price_promo);
                $('#cantidadOFerta').html(quantity_promo);
                tableCommons.clear();
                tableCommons.rows.add(response.model);
                tableCommons.columns.adjust().draw();
                $('#ModalPromotionsRepeat').modal('toggle');
                setTimeout(function () { $('#showItemComunRepeat').modal({ backdrop: 'static', keyboard: false }); }, 400);
                //$('#showStores').modal('show');
            } else {
                SessionFalse("Termino su sesión");
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}

function CloseModalOne() {
    $('#showStoresRepeat').modal('hide');//ModalAddPages
    setTimeout(function () { $('#ModalPromotionsRepeat').modal({ backdrop: 'static', keyboard: false }); }, 400);
}

function CloseModalTwo() {
    $('#showItemComunRepeat').modal('hide');//ModalAddPages
    setTimeout(function () { $('#ModalPromotionsRepeat').modal({ backdrop: 'static', keyboard: false }); }, 400);
}