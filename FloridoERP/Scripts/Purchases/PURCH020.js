﻿var items;
//Obtencion de datos del viewModel
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    items = JSON.parse($("#model").val());
    table.clear();
    table.rows.add(items);
    table.columns.adjust().draw();
});

// Tabla Principal
var table = $('#tableItems').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'part_number' },
            { data: 'description' },
            { data: 'category' },
            { data: 'supplier' },
            { data: null }
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4],
            width: "1%"
        },
        {
            targets: [4],
            render: function (data, type, full, meta) {

                return `<input class="form-control" id="${data.part_number}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange= "get(${data.part_number},'${data.description}','${data.category}','${data.supplier}')" value = "0" style="width:80px"></input>`;

            }
        }]
});

// Tabla Principal
var table2 = $('#tableItems2').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'part_number' },
            { data: 'description' },
            { data: 'category' },
            { data: 'supplier' },
            { data: null },
            { data: null }
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5],
            width: "1%"
        },
        {
            targets: [4],
            render: function (data, type, full, meta) {
                return `<input class="form-control" id="selected${data.part_number}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange= "set(${data.part_number},'${data.description}','${data.category}','${data.supplier}')" value = "${data.quantity}" style="width:80px"></input>`;
            }
        },
        {
            targets: [5],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-danger btn-circle" onclick="deleteItem(${data.part_number})"><i class="fa fa-times"></i></button>`;
            }
        }]
});

list = [];
items = 0;
function get(id, description, category, supplier) {
    if ($("#" + id + "").val() > 0) {
        var index = list.findIndex(elements => elements.part_number == id);
        console.log(index);
        if (index >= 0) {
            list[index] = { part_number: id, description: description, category: category, supplier: supplier, quantity: $("#" + id + "").val() };
        }
        else {
            list.push({ part_number: id, description: description, category: category, supplier: supplier, quantity: $("#" + id + "").val() });
        }

    } else {
        list = list.filter(x => x.part_number != id);
    }
    table2.clear();
    table2.rows.add(list);
    table2.columns.adjust().draw();
}

function set(id, description, category, supplier) {
    var quantity = $("#selected" + id + "").val();
    if ($("#selected" + id + "").val() > 0) {
        var index = list.findIndex(elements => elements.part_number == id);
        if (index >= 0) {
            list[index] = { part_number: id, description: description, category: category, supplier: supplier, quantity: $("#selected" + id + "").val() };
        }
        else {
            list.push({ part_number: id, description: description, category: category, supplier: supplier, quantity: $("#selected" + id + "").val() });
        }

    } else {
        list = list.filter(x => x.part_number != id);
    }
    table2.clear();
    table2.rows.add(list);
    table2.columns.adjust().draw();
    $("#" + id + "").val(quantity);
}

function deleteItem(id) {
    list = list.filter(x => x.part_number != id);
    table2.clear();
    table2.rows.add(list);
    table2.columns.adjust().draw();
    $("#" + id + "").val("0");
}

function save() {
    $('#items').hide();
    $('#selected').show();
    StartLoading();
}

function aprovalSave() {
    $.ajax({
        url: "/InternalRequirementHeader/ActionAddInternalRequirement",
        type: "POST",
        data: { "items": list },
        success: function (returndata) {
            if (returndata.success == "Correcto") {
                EndLoading();
                swal({
                    title: 'Requisición Guardada',
                    text: "Requisición guardada con éxito.",
                    type: "success"
                }, function (isConfirm) {
                    if (isConfirm)
                        window.location.href = '/Purchases/PURCH020';
                });
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}

//Tabla principal
var tableReq = $('#TableRequirements').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'folio' },
        { data: 'cdate2' },
        { data: 'requirement_status' },
    ],
    columnDefs: [
    ]
});
var detailRows = [];

$('#TableRequirements tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableReq.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tableReq.row('.details').length) {
            $('.details-control', tableReq.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function format(d) {
    var detailItems = "";
    console.log(d.folio);
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetAllRequirementsDetails",
        data: { "folio": d.folio },
        type: "POST",
        success: function (data) {
            console.log(data);
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else {
                $.each(data, function (index, value) {
                    detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td>" + value.quantity + "</td><td>" + value.name_creditor + "</td>";
                });
                var Apro = '';
                if (data.length != 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Proveedor</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Proveedor</th></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail
}

function viewRequirements() {
    var site = $("#site").val();
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetAllRequirementsHeaderBySite",
        data: { "site": site },
        type: "POST",
        success: function (data) {
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else {
                if (data.length > 0) {
                    $("#TableReqDiv").show();
                    console.log(data);
                    tableReq.clear();
                    tableReq.rows.add(data);
                    tableReq.columns.adjust().draw();
                } else {
                    toastr.error("No existen requisiciones en la tienda");
                    $("#TableReqDiv").hide();
                }

            }
        }
    });
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

function save() {
    if (list.length > 0) {
        swal({
            title: "¿Desea guardar la requisición?",
            text: "No podrá realizar otra requisición hasta el próximo Jueves",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                aprovalSave();
        });
    } else {
        swal({
            title: 'No ha seleccionado ningun producto',
            text: "Seleccione por lo menos 1 producto para requisición",
            type: "error"
        });
    }
}