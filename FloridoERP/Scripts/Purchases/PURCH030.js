﻿document.addEventListener("DOMContentLoaded", function (event) {
    window.setTimeout(function () { document.getElementById("fa").click(); }, 1000);

    $("#mainSite").select2();

    $("#DateHistory").datepicker({
        autoclose: true,
        todayHighlight: true
    });
});

var table = $('#Codes').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    responsive: true,
    autoWidth: true,
    buttons: [
        { extend: 'copy', text: 'Copiar', className: 'btn-sm' },
        { extend: 'csv', text: 'Excel', title: 'Historial de Existencias', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Historial de Existencias', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            { targets: 0, data: 'part_number', width: "1%" },
            { targets: 1, data: 'part_description', width: "1%" },
            { targets: 2, data: 'Quantity', width: "1%" },
            { targets: 3, data: 'Monto', width: "1%" },
            { targets: 4, data: 'Costo', width: "1%" }
        ],
    columnDefs: [
        {
            targets: [3],
            type: "numeric-comma",
            render: function (data, type, row) {
                if (data != null) {
                    if (data != 0)
                        return "$" + data.toFixed(3);
                    else
                        return "$0";
                }
                else
                    return "$0";
            }
        },
        {
            targets: [4],
            type: "numeric-comma",
            render: function (data, type, row) {
                if (data != null) {
                    if (data != 0)
                        return "$" + data.toFixed(3);
                    else
                        return "$0";
                }
                else
                    return "$0";
            }
        }]
});

function SearchStockHistory() {
    var Site = document.getElementById("mainSite").value;
    var Date = document.getElementById("DateHistory").value;

    if (Site != 0) {
        if (Date != "") {
            StartLoading();
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    EndLoading();
                    var response = JSON.parse(this.responseText);
                    if (response.result == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.result) {
                        table.clear();
                        table.rows.add(response.model);
                        table.columns.adjust().draw();
                    }
                    else
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
                else if (this.readyState == 4 && this.status != 200) {
                    EndLoading();
                    toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                }
            }
            xhttp.open("GET", "/Purchases/ActionGetStockHistory?SiteCode=" + Site + "&Date=" + Date, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(null);
        }
        else
            toastr.warning("Seleccione una fecha.");
    }
    else
        toastr.warning("Seleccione una tienda.");
}