﻿var edit = false;
var indexEdit = -1;
var nameEdit = "";
var idSupplier = "";
var tableContacts = "";
var iContacts = 0;
var idNegative = -1;
var typeSupplier = '';
var boolErrorContact = false;
var boolErrorContactLocal = [false, false, false, false];
var boolErrorContactLocalNew = [false, false, false, false];
var boolNewErrorContact = false;
arrayContact = [{ id: "NewNameContact", texto: "nombre del contacto", value: "name" }, { id: "NewLast_nameContact", texto: "apellido del contacto", value: "last_name" }, { id: "NewPhoneContact", texto: "teléfono del contacto", value: "phone" }, { id: "NewEmailContact", texto: "correo del contacto", value: "email" }, { id: "NewTypeContact", texto: "tipo de contacto", value: "department" }];
listContacts = [];
listEdit = [];
listFinals = [];
$('#supplier_type').select2();
$('#part_iva_purchase_supplier').select2();
$(document).ready(function () {
    window.setTimeout(function () {
        $(".normalheader").addClass("small-header");
        $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        $("#hbreadcrumb").removeClass("m-t-lg");
        $("#NewTypeContact").select2();
    }, 1000);
    $('#apply_return').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
    var suppliers = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(suppliers));
    table.columns.adjust().draw();
});

var table = $('#tableSuppliers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ Registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            { data: null },
            { data: 'supplier_id' },
            { data: 'rfc' },
            { data: 'commercial_name' },
            { data: 'business_name' },
            { data: 'supplier_type' },
            { data: 'supplier_address' },
            { data: 'zip_code' },
            { data: 'city' },
            { data: 'supplier_state' },
            { data: 'country' },
            { data: null }
        ],
    columnDefs:
        [
            {
                data: null,
                width: "1%",
                defaultContent: '<button class="btn btn-xs btn-outline btn-info" id="info-button"><i class="fa fa-info-circle"></i> Detalle</button>',
                targets: [0]
            },
            {
                visible: false,
                searchable: false,
                targets: [1, 3]
            },
            {
                data: null,
                width: "1%",
                defaultContent: '<button class="btn btn-xs btn-outline btn-warning" id="edit-button" type="submit"><i class="fa fa-pencil"></i> Editar</button>',
                targets: [11]
            }
        ]
});

$('#formEdit').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

function validateLetras() {
    var city = $('#city').val().toString();
    var state = $('#supplier_state').val().toString();
    var country = $('#country').val().toString();
    if (city == "") {

    }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(city)) {
        toastr.error("La Ciudad debe contener solo letras.");
        $('#city').focus();
        $('#city').val("");
    }
    else if (state == "") {

    }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(state)) {
        toastr.error("El Estado debe contener solo letras.");
        $('#supplier_state').focus();
        $('#supplier_state').val("");
    }
    else if (country == "") {

    }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(country)) {
        toastr.error("El país debe contener solo letras.");
        $('#country').focus();
        $('#country').val("");
    }
}

function validateRfc() {
    var rfc = $('#rfc').val();
    if (rfc == "" || rfc == null) {
        $('#rfc').focus();
    }
    else {
        const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
        var r = rfc.trim();
        if (r.includes('-') && r.length == 10) {
            axios.get('/Supplier/ActionEditRFC?rfc=' + r + '&type=' + typeSupplier + '&supplier_id=' + idSupplier)
                .then(function (response) {
                    var json = response.data;
                    if (json.success) {
                        if (json.num == 1) {
                            toastr.error(json.message);
                            $('#rfc').focus();
                            $('#rfc').val("");
                            $("#btnSave").prop("disabled", true);
                        } else if (json.num == 2) {
                            toastr.success(json.message);
                            $("#btnSave").prop("disabled", false);
                        } else
                            $("#btnSave").prop("disabled", false);

                    }
                    else
                        SessionFalse(json.message);
                })
                .catch(function (error) {
                });
        }
        else if (r.length == 12 || r.length == 13) {
            var validado = r.toUpperCase().match(re);
            if (validado != null) {
                axios.get('/Supplier/ActionEditRFC?rfc=' + r + '&type=' + typeSupplier + '&supplier_id=' + idSupplier)
                    .then(function (response) {
                        var json = response.data;
                        if (json.success) {
                            if (json.num == 1) {
                                toastr.error(json.message);
                                $('#rfc').focus();
                                $('#rfc').val("");
                                $("#btnSave").prop("disabled", true);
                            } else if (json.num == 2) {
                                toastr.success(json.message);
                                $("#btnSave").prop("disabled", false);
                            } else
                                $("#btnSave").prop("disabled", false);

                        }
                        else
                            SessionFalse(json.message);
                    })
                    .catch(function (error) {
                    });
            }
            else {
                toastr.error("Formato RFC no valido");
                $("#btnSave").prop("disabled", true);
                $('#rfc').focus();
                $('#rfc').val("");
            }
        }
        else {
            toastr.error("Longitud Invalida.");
            $("#btnSave").prop("disabled", true);
            $('#rfc').focus();
            $('#rfc').val("");
        }
    }
}

function validateZIP() {
    var data = $('#zip_code').val().toString().length;
    var code = $('#zip_code').val().trim().toString();
    if (!/^([0-9])*$/.test(code)) {
        toastr.error("Debe contener solo Numeros.");
        $('#zip_code').focus();
        $('#zip_code').val("");
    }
    else if (data == 5) {
        var zipCode = $('#zip_code').val();
        axios.get('https://api-codigos-postales.herokuapp.com/v2/codigo_postal/' + zipCode)
            .then(function (response) {
                var json = response.data;
                if (json.municipio != null) {
                    clearAddress();
                    $('#city').val(json.municipio);
                    $('#supplier_state').val(json.estado);
                    $('#country').val("Mexico")
                }
                else {
                    clearAddress();
                }
            })
            .catch(function (error) {
            });
    }
}

function searchSupplier() {
    var input, filter, h3;
    input = document.getElementById("inptSearch");
    filter = input.value.toUpperCase();
    var data = document.getElementsByClassName("florido-search");
    for (i = 0; i < data.length; i++) {
        var d = data[i];
        if (d.innerHTML.toUpperCase().indexOf(filter) > -1) {
            d.style.display = "";
        } else {
            d.style.display = "none";

        }
    }
}

function showInfoSupplier(supplierId) {
    axios.get('/Supplier/ActionGetSupplierById?id=' + supplierId)
        .then(function (response) {
            var json = response.data.maSupplier;
            $('#txtSupplierName').html('<i class="fa fa-briefcase"></i> ' + json.business_name);
            $('#txtSupplierRfc').html(json.rfc);
            $('#txtSupplierAccountingAccount').html(json.accounting_account);
            $('#txtSupplierType').html(json.supplier_type);
            $('#street_namee').html(json.supplier_address);
            $('#cityy').html(json.city);
            $('#zip_codee').html(json.zip_code);
            $('#statee').html(json.supplier_state);
            $('#countryy').html(json.country);
            $('#cuser').html(json.cuser);
            $('#cdate').html(json.cdate);
            $('#showSupplier').modal('show');
        })
        .catch(function (error) {
        });
}

$('#tableSuppliers tbody').on('click', '#info-button', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'supplier_id':
                $("#supplier_id").val(value);
                showInfoSupplier($("#supplier_id").val());
                break;
        }
    });
});

$("#contact_add_button").click(function () {
    var boolContact = true;
    $.each(arrayContact, function (key, value) {
        if ($("#" + value.id).val() == "" || $("#" + value.id).val() == null) {
            if (boolContact) {
                toastr.warning("Falta colocar el " + value.texto + ".");
            }
            boolContact = false;
            return;
        }
    });
    if (boolNewErrorContact) {
        toastr.warning("Ingresa datos correctos en los campos rojos antes de agregar el contacto. ");
    }

    if (boolContact && !boolNewErrorContact) {
        tableContacts += "<tr class='table-danger' id='" + idNegative + "'><td style='display:none;'><div class='checkbox'><label id='" + 'l' + '0_' + idNegative + "'><input type='checkbox' class='i-checks prueba' id='" + '0_' + idNegative + "'></label></div></td>";
        tableContacts += "<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='80' value='" + $("#NewNameContact").val() + "' id='" + 'name' + idNegative + "' onfocusout='UpdateName(" + idNegative + ", " + iContacts + ")'></td>";
        tableContacts += "<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='80' value='" + $("#NewLast_nameContact").val() + "' id='" + 'last_name' + idNegative + "' onfocusout='UpdateLastName(" + idNegative + ", " + iContacts + ")'></td>";
        tableContacts += "<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='15' value='" + $("#NewPhoneContact").val() + "' id='" + 'phone' + idNegative + "' onfocusout='UpdatePhone(" + idNegative + ", " + iContacts + ")'></td>";
        tableContacts += "<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='80' value='" + $("#NewEmailContact").val() + "' id='" + 'email' + idNegative + "' onfocusout='UpdateEmail(" + idNegative + ", " + iContacts + ")'></td>";
        if ($("#NewTypeContact").val() == 'Recepcion')
            tableContacts += "<td><select id='" + 'NewTypeContact' + idNegative + "' name='" + 'NewTypeContact' + idNegative + "' onfocusout='UpdateType(" + idNegative + ", " + iContacts + ")' class='form-control'><option selected value='Recepcion'>Recepcion.</option> <option value='Ventas'>Ventas.</option><option value='Compras'>Compras.</option><option value='Contabilidad'>Contabilidad.</option></select></td>";
        else if ($("#NewTypeContact").val() == 'Ventas')
            tableContacts += "<td><select id='" + 'NewTypeContact' + idNegative + "' name='" + 'NewTypeContact' + idNegative + "' onfocusout='UpdateType(" + idNegative + ", " + iContacts + ")' class='form-control'><option value='Recepcion'>Recepcion.</option> <option selected value='Ventas'>Ventas.</option><option value='Compras'>Compras.</option><option value='Contabilidad'>Contabilidad.</option></select></td>";
        else if ($("#NewTypeContact").val() == 'Compras')
            tableContacts += "<td><select id='" + 'NewTypeContact' + idNegative + "' name='" + 'NewTypeContact' + idNegative + "' onfocusout='UpdateType(" + idNegative + ", " + iContacts + ")' class='form-control'><option value='Recepcion'>Recepcion.</option> <option value='Ventas'>Ventas.</option><option selected value='Compras'>Compras.</option><option value='Contabilidad'>Contabilidad.</option></select></td>";
        else if ($("#NewTypeContact").val() == 'Contabilidad')
            tableContacts += "<td><select id='" + 'NewTypeContact' + idNegative + "' name='" + 'NewTypeContact' + idNegative + "' onfocusout='UpdateType(" + idNegative + ", " + iContacts + ")' class='form-control'><option value='Recepcion'>Recepcion.</option> <option value='Ventas'>Ventas.</option><option value='Compras'>Compras.</option><option selected value='Contabilidad'>Contabilidad.</option></select></td>";
        tableContacts += `<td align='center'><div align='center'><label class="sck_container" style='text-align:center'><input style="text-align:right;" id="check${idNegative}" type='checkbox' checked onclick='DeleteContact("${idNegative}", ${iContacts})'><span class="checkmark"></span></label></div></td></tr>`;
        listEdit[iContacts] = { id: 0, name: $("#NewNameContact").val(), last_name: $("#NewLast_nameContact").val(), phone: $("#NewPhoneContact").val(), email: $("#NewEmailContact").val(), department: $("#NewTypeContact").val(), check: true, flagActive: true };
        $('#tipo' + idNegative).select2();
        iContacts++;
        idNegative--;
        $("#tableContacts").html(tableContacts);
        $(".textHidde").hide();
        clearContactInput();
    }

    $('#NewTypeContact').val($("#NewTypeContact :first").val()).trigger('change');
});

function clearContactInput() {
    $('#NewNameContact').val('');
    $('#NewLast_nameContact').val('');
    $('#NewPhoneContact').val('');
    $('#NewEmailContact').val('');
    $('#NewTypeContact').val('');
    $("#truck_foot").iCheck('uncheck')
    $("#merchandise_entry").iCheck('uncheck')
    $("#apply_return").iCheck('uncheck')
    $("#purchase_applied_return").iCheck('uncheck')
}


function checkDetailDG() {
    if ($("#apply_return").is(':checked'))
        $("#checksSupplierDG").show();
    else
        $("#checksSupplierDG").hide();
}

function OrderList(id) {
    if (id != "" || id != null) {
        $.ajax({
            url: "/SupplierContact/ActionGetAllContactsBySupplierId",
            type: "GET",
            data: { "supplier_id": id },
            success: function (returndates) {
                /*agrege este if y else*/
                $("#tableContacts").html("")
                if (!returndates.success && returndates.success != undefined) {
                    toastr.warning("Este proveedor no contiene contactos.");
                }
                else {
                    tableContacts = "";
                    iContacts = 0;
                    $.each(returndates, function (index, value) {
                        tableContacts += "<tr class='table-success' id='" + value.supplier_contact_id + "' ><td style='display:none;'><div class='checkbox'><label id='" + 'l' + value.supplier_contact_id + "'><input type='checkbox' class='i-checks prueba' id='" + value.supplier_contact_id + "'></label></div></td>";
                        tableContacts += `<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='80' value='${value.name}' id='${arrayContact[0].id + value.supplier_contact_id}' onfocusout='UpdateContact("${arrayContact[0].id + value.supplier_contact_id}", ${iContacts}, "name")'></td>`;
                        tableContacts += `<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='80' value='${value.last_name}' id='${arrayContact[1].id + value.supplier_contact_id}' onfocusout='UpdateContact("${arrayContact[1].id + value.supplier_contact_id}", ${iContacts}, "last_name")'></td>`;
                        tableContacts += `<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='15' value='${value.phone}' id='${arrayContact[2].id + value.supplier_contact_id}' onfocusout='UpdateContact("${arrayContact[2].id + value.supplier_contact_id}", ${iContacts}, "phone")'></td>`;
                        tableContacts += `<td><input class='form-control' type='text' style='text-align:center; font-size:10pt' maxlength='80' value='${value.email}' id='${arrayContact[3].id + value.supplier_contact_id}' onfocusout='UpdateContact("${arrayContact[3].id + value.supplier_contact_id}", ${iContacts}, "email")'></td>`;
                        if (value.department == 'Recepcion')
                            tableContacts += `<td><select id='NewTypeContact${value.supplier_contact_id}' name='NewTypeContact${value.supplier_contact_id}"'  onchange='UpdateContact("${arrayContact[4].id + value.supplier_contact_id}", ${iContacts}, "department")' class='form-control'><option selected value='Recepcion'>Recepcion.</option> <option value='Ventas'>Ventas.</option><option value='Compras'>Compras.</option><option value='Contabilidad'>Contabilidad.</option></select></td>`;
                        else if (value.department == 'Ventas')
                            tableContacts += `<td><select id='NewTypeContact${value.supplier_contact_id}' name='NewTypeContact${value.supplier_contact_id}"'  onchange='UpdateContact("${arrayContact[4].id + value.supplier_contact_id}", ${iContacts}, "department")' class='form-control'><option value='Recepcion'>Recepcion.</option> <option selected value='Ventas'>Ventas.</option><option value='Compras'>Compras.</option><option value='Contabilidad'>Contabilidad.</option></select></td>`;
                        else if (value.department == 'Compras')
                            tableContacts += `<td><select id='NewTypeContact${value.supplier_contact_id}' name='NewTypeContact${value.supplier_contact_id}"'  onchange='UpdateContact("${arrayContact[4].id + value.supplier_contact_id}", ${iContacts}, "department")' class='form-control'><option value='Recepcion'>Recepcion.</option> <option value='Ventas'>Ventas.</option><option selected value='Compras'>Compras.</option><option value='Contabilidad'>Contabilidad.</option></select></td>`;
                        else if (value.department == 'Contabilidad')
                            tableContacts += `<td><select id='NewTypeContact${value.supplier_contact_id}' name='NewTypeContact${value.supplier_contact_id}"'  onchange='UpdateContact("${arrayContact[4].id + value.supplier_contact_id}", ${iContacts}, "department")' class='form-control'><option value='Recepcion'>Recepcion.</option> <option value='Ventas'>Ventas.</option><option value='Compras'>Compras.</option><option selected value='Contabilidad'>Contabilidad.</option></select></td>`;
                        if (value.flagActive)
                            tableContacts += `<td align='center'><div align='center'><label class="sck_container" style='text-align:center'><input style="text-align: center;" id="check${value.supplier_contact_id}" type='checkbox' checked onclick='DeleteContact("${value.supplier_contact_id}", ${iContacts})'><span class="checkmark"></span></label></div></td></tr>`;
                        else
                            tableContacts += `<td align='center'><div align='center'><label class="sck_container" style='text-align:center'><input style="text-align:right;" id="check${value.supplier_contact_id}" type='checkbox' onclick='DeleteContact("${value.supplier_contact_id}", ${iContacts})'><span class="checkmark"></span></label></div></td></tr>`;

                        listEdit[iContacts] = { id: value.supplier_contact_id, name: value.name, last_name: value.last_name, phone: value.phone, email: value.email, department: value.department, check: false, flagActive: value.flagActive };
                        iContacts++;
                    });
                    $("#tableContacts").html(tableContacts);
                    $.each(returndates, function (index, value) {
                        $('#NewTypeContact' + value.supplier_contact_id).select2();
                    });
                    $(".textHidde").hide();

                }
            },
            error: function (returndates) {
            }
        });
    }
}

function DeleteContact(id, position) {
    var checkbox = document.getElementById('check' + id);
    if (checkbox.checked != true) {
        listEdit[position].flagActive = false;
    } else {
        listEdit[position].flagActive = true;
    }
    listEdit[position].check = true;
}

function UpdateContact(id, e, n) {
    if (n == 'name') {
        if ($("#" + id).val().trim() == "" || !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test($("#" + id).val().trim())) {
            toastr.error('Ingrese un nombre correcto.');
            document.getElementById(id).style.background = "#F5C6CB";
            boolErrorContact = true;
            boolErrorContactLocal[0] = true;
        } else {
            boolErrorContactLocal[0] = false;
            document.getElementById(id).style.background = "#FFFFFF";
        }
    }
    else if (n == 'last_name') {
        if ($("#" + id).val().trim() == "" || !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test($("#" + id).val())) {
            toastr.error('Ingrese un apellido correcto.');
            document.getElementById(id).style.background = "#F5C6CB";
            boolErrorContact = true;
            boolErrorContactLocal[1] = true;
        } else {
            boolErrorContactLocal[1] = false;
            document.getElementById(id).style.background = "#FFFFFF";
        }
    }
    else if (n == 'phone') {
        if ($("#" + id).val().trim() == "") {
            toastr.error('Ingrese un número telefónico correcto.');
            document.getElementById(id).style.background = "#F5C6CB";
            boolErrorContact = true;
            boolErrorContactLocal[2] = true;
        } else {
            boolErrorContactLocal[2] = false;
            document.getElementById(id).style.background = "#FFFFFF";
        }
    }
    else if (n == 'email') {
        if ($("#" + id).val().trim() == "" || !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#" + id).val().trim())) {
            toastr.error('Ingrese un correo electrónico correcto.2');
            document.getElementById(id).style.background = "#F5C6CB";
            boolErrorContact = true;
            boolErrorContactLocal[3] = true;
        } else {
            boolErrorContactLocal[3] = false;
            document.getElementById(id).style.background = "#FFFFFF";
        }
    }
    else if (n == 'department') {
        listEdit[e].department = document.getElementById(id).value;
        listEdit[e].check = true;
    }

    //Guardar todos los valores que se han ido editando (aunque esten mal)
    if ($("#" + id).val().trim() != listEdit[e][n]) {
        listEdit[e][n] = $("#" + id).val().trim();
        listEdit[e].check = true;
    }
    UpdateErrors();
}

function UpdateErrors() {
    //Busca los errores
    var arrErrors = boolErrorContactLocal.filter(x => x == true);
    var arrErrorsNew = boolErrorContactLocalNew.filter(x => x == true);

    //Cuando no surga un error problemas locales coloca true la bandara( para que permita guardar: esta variable sabe si hay errores  )
    if (arrErrors.length == 0 && arrErrorsNew == 0) {
        boolErrorContact = false;
    }
}

function UpdateType(id, e) {
    var x = "#NewTypeContact" + id;
    if ($(x).val().trim() != listEdit[e].department) {
        listEdit[e].department = $(x).val().trim();
        listEdit[e].check = true;
    }
}

function UpdateEmail(id, e) {
    var x = "#email" + id;
    if ($(x).val().trim() == "" || !/^(([^<>()\[\]\\.,;:\s@@"]+(\.[^<>()\[\]\\.,;:\s@@"]+)*)|(".+"))@@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(x)) {
        toastr.error('Ingrese un correo electrónico correcto.3');
        document.getElementById(x).style.background = "#F5C6CB";
        boolErrorContact = true;
        boolErrorContactLocalNew[3] = true;
    } else {
        boolErrorContactLocalNew[3] = false;
        document.getElementById(x).style.background = "#FFFFFF";
        if ($(x).val().trim() != listEdit[e].email) {
            listEdit[e].email = $(x).val().trim();
            listEdit[e].check = true;
        }
        UpdateErrors();
    }
}

function UpdatePhone(id, e) {
    var x = "#phone" + id;
    if ($(x).val().trim() == "" || !/^\d{10,12}$/.test($(x).val())) {
        toastr.error('Ingrese un número telefónico correcto.');
        document.getElementById(x).style.background = "#F5C6CB";
        boolErrorContact = true;
        boolErrorContactLocalNew[2] = true;
    } else {
        boolErrorContactLocalNew[2] = false;
        document.getElementById(x).style.background = "#FFFFFF";
        if ($(x).val().trim() != listEdit[e].phone) {
            listEdit[e].phone = $(x).val().trim();
            listEdit[e].check = true;
        }
        UpdateErrors();
    }
}

function UpdateLastName(id, e) {
    var x = "#last_name" + id;
    if ($(x).val().trim() == "" || !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(x)) {
        toastr.error('Ingrese un apellido correcto.');
        document.getElementById(x).style.background = "#F5C6CB";
        boolErrorContact = true;
        boolErrorContactLocalNew[1] = true;
    } else {
        boolErrorContactLocalNew[1] = false;
        document.getElementById(x).style.background = "#FFFFFF";
        if ($(x).val().trim() != listEdit[e].last_name) {
            listEdit[e].last_name = $(x).val().trim();
            listEdit[e].check = true;
        }
        UpdateErrors();
    }
}

function UpdateName(id, e) {
    var x = "#name" + id;
    var x = "#last_name" + id;
    if ($(x).val().trim() == "" || !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(x)) {
        toastr.error('Ingrese un nombre correcto.');
        document.getElementById(x).style.background = "#F5C6CB";
        boolErrorContact = true;
        boolErrorContactLocalNew[0] = true;
    } else {
        boolErrorContactLocalNew[0] = false;
        document.getElementById(x).style.background = "#FFFFFF";
        if ($(x).val().trim() != listEdit[e].name) {
            listEdit[e].name = $(x).val().trim();
            listEdit[e].check = true;
        }
        UpdateErrors();
    }
}

$('#tableSuppliers tbody').on('click', '#edit-button', function () {
    listEdit = [];
    clearModal();
    validator.resetForm();
    $('#formEdit .form-control').removeClass('error');
    $("#btnSave").prop("disabled", false);
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'supplier_id':
                $("#supplier_id").val(value);
                idSupplier = $("#supplier_id").val();
                OrderList(idSupplier);
                break;
            case 'rfc':
                $("#rfc").val(value);
                break;
            case 'commercial_name':
                $("#commercial_name").val(value);
                break;
            case 'business_name':
                $("#business_name").val(value);
                break;
            case 'supplier_address':
                $("#supplier_address").val(value);
                break;
            case 'city':
                $("#city").val(value);
                break;
            case 'supplier_state':
                $("#supplier_state").val(value);
                break;
            case 'country':
                $("#country").val(value);
                break;
            case 'zip_code':
                $("#zip_code").val(value);
                break;
            case 'part_iva_purchase_supplier':
                $("#part_iva_purchase_supplier").val(value).trigger('change');
                break;
            case 'supplier_type':
                $("#supplier_type").val(value).trigger('change');
                typeSupplier = value;
                if (value == 'Acreedor') {
                    $("#checksSupplierAcreedor").show();
                    $('#checksSupplier').hide();
                    $('#checksSupplierRMA').hide();
                }
                else {
                    $("#checksSupplierAcreedor").hide();
                    $('#checksSupplier').show();
                    $('#checksSupplierRMA').show();
                }
                break;
            case 'merchandise_entry':
                if (value == 1) { $("#merchandise_entry").iCheck('check'); }
                else
                    $("#merchandise_entry").iCheck('uncheck');
                break;
            case 'apply_return':
                if (value == 1) {
                    $("#apply_return").iCheck('check');
                    $("#checksSupplierDG").show();
                }
                else {
                    $("#apply_return").iCheck('uncheck');
                    $("#checksSupplierDG").hide();
                }
                break;
            case 'purchase_applied_return':
                if (value == 1) { $("#purchase_applied_return").iCheck('check'); }
                else
                    $("#purchase_applied_return").iCheck('uncheck');
                break;
            case 'truck_foot':
                if (value == 1) { $("#truck_foot").iCheck('check'); }
                else
                    $("#truck_foot").iCheck('uncheck');
                break;
            case 'xml_avoidable':
                if (value == 1) {
                    $("#xml_avoidable_acreedor_c").iCheck('check');
                    $("#xml_avoidable_c").iCheck('check');
                }
                else {
                    $("#xml_avoidable_acreedor_c").iCheck('uncheck');
                    $("#xml_avoidable_c").iCheck('uncheck');
                }
                break;
        }
    });

    $('#modal-edit-supplier').modal({ backdrop: 'static', keyboard: false });
    edit = true;
    nameEdit = $('#supplier_name').val()
    indexEdit = table.row(this).index();
    $('#modal-edit-supplier').modal('show');
});

function clear() {
    $("#supplier_id").val(null);
    $("#rfc").val(null);
    $("#commercial_name").val(null);
    $("#business_name").val(null);
    $("#business_name").val(null);
    $("#supplier_type").val('').trigger('change');
    $("#part_iva_purchase_supplier").val('').trigger('change');
    $("#supplier_address").val(null);
    $("#zip_code").val(null);
    $("#city").val(null);
    $("#supplier_state").val(null);
    $("#country").val(null);
    $('#modal-edit-supplier').modal({ backdrop: 'static', keyboard: false });
    $("#modal-edit-supplier").removeClass("hmodal-warning").addClass("hmodal-success");
    $("#submit-button").removeClass("btn btn-info").addClass("btn btn-success");
    indexEdit = null;
    edit = false;
}

function clearModal() {
    clear();
    listEdit = [];
    iContacts = 0;
    tableContacts = "";
    idNegative = -1;
    clearContactInput();
    document.getElementById("NewEmailContact").style.background = "#FFFFFF";
    document.getElementById("NewNameContact").style.background = "#FFFFFF";
    document.getElementById("NewLast_nameContact").style.background = "#FFFFFF";
    document.getElementById("NewPhoneContact").style.background = "#FFFFFF";
}

var validator = $("#formEdit").validate({
    rules: {
        rfc: {
            required: true
        },
        commercial_name: {
            required: true,
            minlength: 4,
            maxlength: 200
        },
        business_name: {
            required: true,
            minlength: 4,
            maxlength: 200
        },
        supplier_address: {
            required: true,
            minlength: 5,
            maxlength: 200
        },
        zip_code: {
            required: true,
            maxlength: 15
        },
        city: {
            required: true,
            minlength: 5,
            maxlength: 20
        },
        supplier_state: {
            required: true,
            minlength: 5,
            maxlength: 20
        },
        country: {
            required: true,
            minlength: 5,
            maxlength: 20
        },
    },
    messages: {
        rfc: {
            required: "El Campo no puede ser nulo",
        },
        commercial_name: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar un nombre comercial',
            maxlength: 'Máximo 200 caracteres'
        },
        business_name: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar la razón social',
            maxlength: "Máximo 200 caracteres"
        },
        supplier_address: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar la dirección',
            maxlength: 'Máximo 200 caracteres'
        },
        zip_code: {
            required: "El Campo no puede ser nulo",
            maxlength: 'Ingrese un Código postal correcto.'
        },
        city: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar una ciudad',
            maxlength: 'Máximo 20 caracteres'
        },
        supplier_state: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar un estado',
            maxlength: 'Máximo 20 caracteres'
        },
        country: {
            required: "El Campo no puede ser nulo",
            minlength: 'Favor de ingresar un país',
            maxlength: "Máximo 20 caracteres"
        },
    },
    submitHandler: function (form) {
        validateZIP();
        validateLetras();

        var rfcVar = $('#rfc').val();
        var commercial_nameVar = $('#commercial_name').val();
        var business_nameVar = $('#business_name').val();
        var zip_codeVar = $('#zip_code').val();
        var supplier_addressVar = $('#supplier_address').val();
        var cityVar = $('#city').val();
        var supplier_stateVar = $('#supplier_state').val();
        var countryVar = $('#country').val();
        $('#rfc').val(rfcVar.trim());
        $('#commercial_name').val(commercial_nameVar.trim());
        $('#business_name').val(business_nameVar.trim());
        $('#supplier_address').val(supplier_addressVar.trim());
        $('#zip_code').val(zip_codeVar.trim());
        $('#city').val(cityVar.trim());
        $('#supplier_state').val(supplier_stateVar.trim());
        $('#country').val(countryVar.trim());


        var xml_premission = 0;
        if (typeSupplier == 'Acreedor') {
            xml_premission = ($("#xml_avoidable_acreedor_c").is(':checked')) ? 1 : 0;
        } else {
            xml_premission = ($("#xml_avoidable_c").is(':checked')) ? 1 : 0;
        }

        if (validator.element("#commercial_name") &&
            validator.element("#business_name") &&
            validator.element("#zip_code") &&
            validator.element("#city") &&
            validator.element("#supplier_state") &&
            validator.element("#country")) {
            if ($("#commercial_name").val() == "" || $("#commercial_name").val() == null || $('#commercial_name').val().length < 5) {
                toastr.error("Ingresa correctamente un nombre comercial.");
                $('#commercial_name').focus();
            }
            else if ($("#business_name").val() == "" || $("#business_name").val() == null || $('#business_name').val().length < 5) {
                toastr.error("Ingresa correctamente una razón social.");
                $('#business_name').focus();
            }
            else if ($('#supplier_type').val() == "") {
                toastr.error("Selecciona un tipo de proveedor.");
                $("#supplier_type").select2('open');
            }
            else if ($("#supplier_address").val() == "" || $("#supplier_address").val() == null || $('#supplier_address').val().length < 4) {
                toastr.error("Ingresa correctamente una dirección.");
                $('#supplier_address').focus();
            }
            else if ($("#zip_code").val() == "" || $("#zip_code").val() == null || $('#zip_code').val().length < 5) {
                toastr.error("Ingresa correctamente un código postal.");
                $('#zip_code').focus();
            }
            else if ($("#city").val() == "" || $("#city").val() == null || $('#city').val().length < 5) {
                toastr.error("Ingresa correctamente una ciudad.");
                $('#city').focus();
            }
            else if ($("#supplier_state").val() == "" || $("#supplier_state").val() == null || $('#supplier_state').val().length < 5) {
                toastr.error("Ingresa correctamente un estado.");
                $('#supplier_state').focus();
            }
            else if ($("#country").val() == "" || $("#country").val() == null || $('#country').val().length < 5) {
                toastr.error("Ingresa correctamente un país.");
                $('#country').focus();
            }
            else if ($("#part_iva_purchase_supplier").val() == "") {
                toastr.error("Ingresa un IVA de Compra correcto.");
            }
            else if (boolErrorContact) {
                toastr.error("Ingrese correcamente los datos contactos.");
            }
            else {

                for (var x = 0; x < listEdit.length; x++) {
                    if (listEdit[x].check == true) {
                        listFinals[x] = { supplier_contact_id: listEdit[x].id, name: listEdit[x].name, last_name: listEdit[x].last_name, phone: listEdit[x].phone, email: listEdit[x].email, department: listEdit[x].department, flagActive: listEdit[x].flagActive };
                    }
                }
                var model =
                {
                    "supplier_id": $("#supplier_id").val(),
                    "rfc": $("#rfc").val(),
                    "commercial_name": $("#commercial_name").val(),
                    "business_name": $("#business_name").val(),
                    "supplier_type": $("#supplier_type").val(),
                    "part_iva_purchase_supplier": $("#part_iva_purchase_supplier").val(),
                    "supplier_address": $("#supplier_address").val(),
                    "zip_code": $("#zip_code").val(),
                    "city": $("#city").val(),
                    "supplier_state": $("#supplier_state").val(),
                    "country": $("#country").val(),
                    "truck_foot": ($("#truck_foot").is(':checked')) ? 1 : 0,
                    "merchandise_entry": ($("#merchandise_entry").is(':checked')) ? 1 : 0,
                    "apply_return": ($("#apply_return").is(':checked')) ? 1 : 0,
                    "purchase_applied_return": ($("#purchase_applied_return").is(':checked')) ? 1 : 0,
                    "xml_avoidable": xml_premission,
                }
                listFinals = listFinals.filter(Boolean)
                if (listFinals.length > 0) {
                    StartLoading();
                    $.ajax({
                        type: "POST",
                        url: "/Supplier/ActionEditSupplier",
                        data: { "model": model, "contacts": listFinals },
                        success: function (response) {
                            if (response.success == true) {
                                table.clear().draw();
                                table.rows.add(response.supplierModel);
                                table.columns.adjust().draw();
                                $('#modal-edit-supplier').modal('hide');
                                toastr.info(response.responseText);
                                listEdit = [];
                                listFinals = [];
                                iContacts = 0;
                                idNegative = -1;
                                tableContacts = "";
                                EndLoading();
                                $("#truck_foot").iCheck('uncheck');
                                $("#merchandise_entry").iCheck('uncheck');
                                $("#apply_return").iCheck('uncheck');
                                $("#purchase_applied_return").iCheck('uncheck');
                            }
                            else {
                                SessionFalse("Termino tu sesión")
                                EndLoading();
                            }
                        },
                        error: function () {
                            toastr.error('Error inesperado contactar a sistemas.');
                            EndLoading();
                        }
                    });
                    return false;
                }
                else {
                    StartLoading();
                    list = [];
                    $.ajax({
                        type: "POST",
                        url: "/Supplier/ActionEditSupplier",
                        data: {
                            "model": model,
                            "contacts": list
                        },
                        success: function (response) {
                            if (response.success == true) {
                                table.clear().draw();
                                table.rows.add(response.supplierModel);
                                table.columns.adjust().draw();
                                $('#modal-edit-supplier').modal('hide');
                                toastr.info(response.responseText);
                                EndLoading();
                                $("#truck_foot").iCheck('uncheck');
                                $("#merchandise_entry").iCheck('uncheck');
                                $("#apply_return").iCheck('uncheck');
                                $("#purchase_applied_return").iCheck('uncheck');
                            }
                            else {
                                SessionFalse("Termino tu sesión")
                                EndLoading();
                            }
                        },
                        error: function () {
                            toastr.error('Error inesperado contactar a sistemas.');
                            EndLoading();
                        }
                    });
                    return false;
                }
            }
        }
    }
});

function validateEmail() {
    if ($("#NewEmailContact").val().trim() == "" || !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#NewEmailContact").val().trim())) {
        toastr.warning('Ingrese un correo electrónico correcto.111');
        document.getElementById("NewEmailContact").style.background = "#F5C6CB";
        boolNewErrorContact = true;
    } else {
        document.getElementById("NewEmailContact").style.background = "#FFFFFF";
        boolNewErrorContact = false;
    }
}

function validateName() {
    var nombre = $('#NewNameContact').val();
    if (nombre == "" || !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(nombre)) {
        toastr.warning("Ingrese un nombre correcto.");
        document.getElementById("NewNameContact").style.background = "#F5C6CB";
        boolNewErrorContact = true;
    } else {
        document.getElementById("NewNameContact").style.background = "#FFFFFF";
        boolNewErrorContact = false;
    }
}

function validateLastName() {
    var last_name = $('#NewLast_nameContact').val();
    if (last_name == "" || !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(last_name)) {
        toastr.warning("Ingrese un apellido correcto.");
        document.getElementById("NewLast_nameContact").style.background = "#F5C6CB";
        boolNewErrorContact = true;
    } else {
        document.getElementById("NewLast_nameContact").style.background = "#FFFFFF";
        boolNewErrorContact = false;
    }
}

function validatePhone() {
    var phone = $('#NewPhoneContact').val();
    if (phone == "" || !/^\d{10,12}$/.test(phone)) {
        document.getElementById("NewPhoneContact").style.background = "#F5C6CB";
        toastr.warning("Ingrese un número correcto por ejemplo: 6641234567");
        boolNewErrorContact = true;
    } else {
        document.getElementById("NewPhoneContact").style.background = "#FFFFFF";
        boolNewErrorContact = false;
    }
}

function UpdatePartIvaSupplierSave() {

    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Supplier/ActionEditSupplierIVA",
        data: { "part_iva": $("#part_iva_purchase_supplier").val(), "supplier": $("#supplier_id").val() },
        success: function (response) {
            if (response.success != false) {
                table.clear().draw();
                table.rows.add(response.supplierModel);
                table.columns.adjust().draw();
                if (response.responseText == 0) {
                    toastr.error("Hubo un error. Favor de contactar a sistemas.");
                } else if (response.responseText == 1) {
                    toastr.warning("Asigne un IVA Correcto al proveedor.");
                } else if (response.responseText == 2) {
                    toastr.warning("El proveedor no tiene productos relacionados para cambiar el IVA.");
                } else if (response.responseText == 3) {
                    toastr.warning("El proveedor no tiene productos relacionados con IVA, no es posible cambiar productos si no estan relacionados con IVA.");
                } else if (response.responseText == 4) {
                    toastr.success("Cambiado con exito el IVA de los productos.");
                }
                EndLoading();
                $("#truck_foot").iCheck('uncheck');
                $("#merchandise_entry").iCheck('uncheck');
                $("#apply_return").iCheck('uncheck');
                $("#purchase_applied_return").iCheck('uncheck');
            }
            else {
                SessionFalse("Termino tu sesión")
                EndLoading();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
    return false;

}
function UpdatePartIvaSupplier() {
    if ($("#part_iva_purchase_supplier").val() == "" || $("#part_iva_purchase_supplier").val() == "N/A") {
        toastr.warning("Seleccione un IVA correcto.");
    } else {
        swal({
            title: "¿Desea cambiar el IVA de los productos relacionados al proveedor?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        },
            function (isConfirm) {
                if (isConfirm) { UpdatePartIvaSupplierSave(); }
            });
    }

}

function ShowStoreWithXMLPermission() {
    $.ajax({
        type: "GET",
        url: "/Supplier/ActionListSitesXML",
        data: {  },
        success: function (response) {
            if (response) {
                console.log("response");
                console.log(response);
                var l = response.length;
                [
                    { id: "listStores1", range: { number1: 0, number2: 11 } }, { id: "listStores2", range: { number1: 11, number2: 21 } },
                    { id: "listStores3", range: { number1: 21, number2: l } }
                ].map(elements => {
                    var ul = document.getElementById(elements.id);
                    ul.innerHTML = '';
                    LiGenerator(elements.range.number1, elements.range.number2, response, ul)
                })
                $('#showSupplier').modal('toggle');
                setTimeout(function () { $('#showStoresRepeat').modal({ backdrop: 'static', keyboard: false }); }, 400);
            } else {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });

    //$('#showProductCommon').modal('toggle');
    //setTimeout(function () { $('#showStores').modal({ backdrop: 'static', keyboard: false }); }, 400);
}


function LiGenerator(number1, number2, response, ul) {
    for (var i = number1; i < number2; i++) {
        var li = document.createElement("li");
        if (typeof response[i] !== 'undefined') {
            li.appendChild(document.createTextNode(`  ${response[i].description}  `));
            ul.appendChild(li);
            //color = IdPage.Colors.find(element => element.status == response[i].Status);
            //li.style.backgroundColor = color != undefined ? color.liColor : "";
        }
    }
}
