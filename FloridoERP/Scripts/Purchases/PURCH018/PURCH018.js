﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $("#selectBuyerDivision").select2();

    $("#productSelect").select2({
        formatInputTooShort: function () {
            return "Ingrese una categoria";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#productType").select2({
        formatInputTooShort: function () {
            return "Ingrese un tipo";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#creditorsProductSelect").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Ingrese un producto o servicio";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#creditorsSupplierSelect").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Ingrese un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#creditorsSupplierSelect2").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Ingrese un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#currencySelect").select2({
        formatInputTooShort: function () {
            return "Ingrese una moneda";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    var listModel = JSON.parse($("#model").val());
    /*remplazo con reizor*/
    categoryList = listModel.ItemCreditorsCategoryList;
    table.clear();
    table.rows.add($(categoryList));
    table.columns.adjust().draw();
    SelectProduct();

    typeList = listModel.TypeCreditorsList;
    SelectType();

    productList = listModel.ItemCreditorsList;
    tableProducts.clear();
    tableProducts.rows.add($(productList));
    tableProducts.columns.adjust().draw();
    SelectItemCreditors();

    supplierList = listModel.SupplierList;
    SelectSupplierCreditors();

    SelectCurrencyType(listModel.Currency);
    /*asta aqui*/
    creditorList = listModel.ItemSupplierCreditorsList;
    console.log("creditorList");
    console.log(creditorList);
    tableCreditors.clear();
    tableCreditors.rows.add($(creditorList));
    tableCreditors.columns.adjust().draw();
    CheckList();
    SelectSupplierCreditors2();
});

function ChangeTextMenu(s) {
    $('#MenuLeftAction').html(s);
    SelectProduct();
    DivProductsOption(true);
    DivCreditorsOption(true);
    CheckList();
    SelectItemCreditors();
    $('#creditorsSupplierSelect2').val("0").trigger('change');
}

function Clear() {

}