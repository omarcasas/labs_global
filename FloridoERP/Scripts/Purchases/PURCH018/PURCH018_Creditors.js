﻿/*Proveedores Productos*/
var creditorList = []
var supplierList = []
const liCategory = nombre => id => `<li class="list-group-item">
                                                                            <p class="checkbox">
                                                                                <input type="checkbox" name="available" value="${id}">&nbsp;&nbsp;
                                                                                <label id="Label_${id}" for="Label_${id}">${nombre}</label>
                                                                            </p>
                                                                        </li>`
var IsPostCreditor = true;
const creditorModal = { id: 0, price: document.getElementById("creditorsPriceForm") };
var tableCreditors = $('#creditorsTable').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [0],
            visible: false,
            searchable: false,
            width: "1%"
        },
        {
            targets: [0],
            width: "1%"
        },
        {
            targets: [8],
            render: function (data, type, full, meta) {
                return `$${data}`;
            },
            width: "1%"
        },
        {
            targets: [9],
            render: function (data, type, full, meta) {
                return `${data == 0 ? "N/A" : data}`;
            },
            width: "1%"
        },
        {
            targets: [10],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-outline btn-warning" onclick="EditCreditor('N_${data.Id}')"><i class='fa fa-pencil'></i> Editar</button>`;
            },
            width: "1%"
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                var texto = data ? "Si" : "No"
                return `${texto}`;
            },
            width: "1%"
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                var texto = data ? "Activo" : "Desactivado"
                return `${texto}`;
            },
            width: "1%"
        }
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
    ],
    columns:
        [
            { data: 'Id', width: "1%" },
            { data: 'SupplierName', width: "1%" },
            { data: 'PartNumber', width: "1%" },
            { data: 'Description', width: "1%" },
            { data: 'Type', width: "1%" },
            { data: 'CategoryName', width: "1%" },
            { data: 'RequisitionFlag', width: "1%" },
            { data: 'ActiveFlag', width: "1%" },
            { data: 'Price', width: "1%" },
            { data:'Currency', width: "1%"},
            { data: null, width: "1%" }

        ],
});
function SelectItemCreditors() {
    selectGeneric("#creditorsProductSelect", productList, ["PartNumber", "Description"], { id: "0", text: "Seleccione un producto o servicio" })
    $("#select2-chosen-3").html("Seleccione un producto o servicio")
}
function SelectSupplierCreditors() {
    selectGeneric("#creditorsSupplierSelect", supplierList, ["SupplierId", "CommercialName"], { id: "0", text: "Seleccione un proveedor" })
    $("#select2-chosen-4").html("Seleccione un proveedor")
}
function SelectSupplierCreditors2() {
    selectGeneric("#creditorsSupplierSelect2", supplierList, ["SupplierId", "CommercialName"], { id: "0", text: "Seleccione un proveedor" })
    $("#select2-chosen-5").html("Seleccione un proveedor")
}

function DivCreditorsOption(value) {
    if (value) {
        $("#formCreditorsDiv").hide()
        $("#tableCategoryDiv").show()
        $("#creditorsDivRelation").show()
        $('#creditorsActive').iCheck('uncheck');
    }
    else {
        $("#formCreditorsDiv").show()
        $("#tableCategoryDiv").hide()
        $("#creditorsDivRelation").hide()
    }
}

function FormCreditor() {
    DivCreditorsOption(false)
    IsPostCreditor = true;
    creditorModal.price.value = 0;
    $("#creditorsProductSelect").prop('disabled', false);
    $("#creditorsSupplierSelect").prop('disabled', false);
    $("#buttonSaveCreditors").html("Guardar")
    SelectSupplierCreditors()
    SelectItemCreditors()
}

function EditCreditor(id) {
    $("#productTitle").html("Agregar/Editar Precios")
    var index = creditorList.findIndex(elements => elements.Id == id.split("N_")[1]);
    $('#creditorsSupplierSelect').val(creditorList[index].SupplierId).trigger('change');
    $('#creditorsProductSelect').val(creditorList[index].PartNumber).trigger('change');
    if (creditorList[index].ActiveFlag)
        $('#creditorsActive').iCheck('check');
    else
        $('#creditorsActive').iCheck('uncheck');
    if (creditorList[index].Currency == "N/A" ) 
        $('#currencySelect').val("0").trigger('change');    
    else
        $('#currencySelect').val(creditorList[index].Currency).trigger('change');

    $("#creditorsProductSelect").prop('disabled', true);
    $("#creditorsSupplierSelect").prop('disabled', true);
    $("#buttonSaveCreditors").html("Editar")
    creditorModal.price.value = creditorList[index].Price;
    creditorModal.id = creditorList[index].Id;
    DivCreditorsOption(false)
    IsPostCreditor = false;
    var position = $("#creditorsPriceForm").offset()
    $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
}

function SelectCurrencyType(list) {
    selectGeneric("#currencySelect", list, ["vkey", "vkey"], { id: "0", text: "Seleccione un tipo de moneda" })
    $("#select2-chosen-6").html("Seleccione un Seleccione un tipo de moneda")
}

$("#buttonSaveCreditors").click(() => {
    ValidationNumber();
    let ProductVal = $("#creditorsProductSelect").val();
    let SupplierVal = $("#creditorsSupplierSelect").val();
    let Currency = $("#currencySelect").val();
    if ($("#buttonSaveCreditors").html() == "Guardar" && IsPostCreditor) {

        if (creditorList.filter(element => element.PartNumber == ProductVal && element.SupplierId == SupplierVal).length == 0) {
            if (SupplierVal.trim() != "0") {
                if (ProductVal.trim() != "0") {
                    if (Currency != "0") {
                        axios.post("/ItemSupplierCreditors/ActionSaveSupplierCreditor/", { PartNumber: ProductVal, SupplierId: SupplierVal })
                            .then(data => {
                                if (data.data == "SF") {
                                    SessionFalse(sesion)
                                }
                                else if (data.data.Id > 0) {
                                    var indexProduct = productList.findIndex(elements => elements.PartNumber == ProductVal);
                                    var indexSupplier = supplierList.findIndex(elements => elements.SupplierId == SupplierVal);
                                    creditorList = [...creditorList,
                                    {
                                        Id: data.data.Id,
                                        PartNumber: ProductVal,
                                        SupplierId: SupplierVal,
                                        SupplierName: supplierList[indexSupplier].CommercialName,
                                        Description: productList[indexProduct].Description,
                                        ActiveFlag: productList[indexProduct].ActiveFlag,
                                        RequisitionFlag: productList[indexProduct].RequisitionFlag,
                                        Type: productList[indexProduct].Type,
                                        CategoryName: productList[indexProduct].CategoryName,
                                        CategoryId: productList[indexProduct].CategoryId,
                                        Price: creditorModal.price.value,
                                        Currency: Currency
                                    }]
                                    tableCreditors.clear();
                                    tableCreditors.rows.add(jQuery(creditorList));
                                    tableCreditors.columns.adjust().draw();
                                    //DisblePrices(data.data.Id)
                                    SavePrice(data.data.Id, creditorModal.price.value, Currency, true)
                                    toastr.success("Guardado con exito")
                                    DivCreditorsOption(true)
                                }
                                else {
                                    toastr.error("Hubo un problema al Guardar")
                                }
                            })
                            .catch(error => {
                                toastr.error("Contacte a sistemas.")
                            })
                    }
                    else
                        toastr.warning("Seleccione un tipo de moneda")
                }
                else {
                    toastr.warning("Selecciona un producto.")
                }
            }
            else {
                toastr.warning("Selecciona un proveedor.")
            }
        }
        else
            toastr.warning("Ya existe esa combinacion.")
    }
    if ($("#buttonSaveCreditors").html() == "Editar" && !IsPostCreditor) {

        var indexCreditor = creditorList.findIndex(elements => elements.Id == creditorModal.id);
        if (Currency != "0") {
            if (parseFloat(creditorModal.price.value) > 0) {
                //DisblePrices(creditorModal.id)
                creditorList[indexCreditor].Price = creditorModal.price.value
                creditorList[indexCreditor].Currency = Currency
                creditorList[indexCreditor].ActiveFlag = $("#creditorsActive").is(":checked");
                SavePrice(creditorModal.id, parseFloat(creditorModal.price.value), Currency, $("#creditorsActive").is(":checked"))
                tableCreditors.clear();
                tableCreditors.rows.add(jQuery(creditorList));
                tableCreditors.columns.adjust().draw();
                toastr.success("Editado con exito")
                DivCreditorsOption(true)
            }
            else
                toastr.warning("ingresa un precio")
        }
        else
            toastr.warning("Seleccione un tipo de moneda")
    }
    //else
    //    toastr.warning("Hubo un problema desconosido")
})

function DisblePrices(supplier) {
    axios.get("/ItemSupplierCreditors/ActionDisableItemCreditors/?IdSupplierPartNumber=" + supplier)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data) {
            }
            else {
                toastr.error("Hubo un problema al desactivar los precios anteriores")
            }
        })
        .catch(error => {
            toastr.error("Contacte a sistemas")
        })
}

function SavePrice(id, price, currency, active) {
    if (currency != "0") {
        if (price > 0) {
            axios.post("/ItemSupplierCreditors/ActionSaveItemCreditors/", { Price: price, SupplierPartNumberCreditors: id, Currency: currency, ActiveFlag: active})
                .then(data => {
                    if (data.data == "SF") {
                        SessionFalse(sesion)
                    }
                    else if (data.data) {
                    }
                    else {
                        toastr.error("Hubo un problema al activar el precio Favor de recargar la pagina y contactar a sistemas")
                    }
                })
                .catch(error => {
                    toastr.error("Contacte a sistemas")
                })
        }
    }
}

function NewSaveCategory() {
    let sitesL = [];
    var existCategory = 0;
    $.each($('input[type="checkbox"][name="available"]:checked'), function () {
        sitesL.push($(this).val())
        let id = $(this).val()
        console.log(id)
        console.table(productList)

        if (productList.filter(x => x.CategoryId == id).length > 0) {
        }
        else {
            existCategory++;
        }
    });
    if ($("#creditorsSupplierSelect2").val().trim() != "0") {
        if (sitesL.length > 0) {
            if (sitesL.length == 1 && existCategory > 0) {
                toastr.warning("La categoria seleccionada no tiene productos.")
            }
            else {
                if (existCategory > 0) {
                    toastr.warning("Unas Categorias no contienen productos y no se agregaron.")
                }
                axios.post("/ItemSupplierCreditors/ActionEditCategory", { Supplier: $("#creditorsSupplierSelect2").val(), Category: sitesL })
                    .then(data => {
                        if (data.data == "SF") {
                            SessionFalse(sesion)
                        }
                        else if (data.data) {
                            toastr.success("Cambios guardados con exito.");
                        }
                        else {
                            toastr.error("Hubo un problema al guardar una categoria.")
                        }
                    })
                    .catch(error => {
                        toastr.error("Contacte a sistemas")
                    }).then(() => {
                        GetAllSupplierCreditors()
                        CheckList()
                        $('#creditorsSupplierSelect2').val("0").trigger('change');
                        $("#PanelDiv").click()
                    })
            }
        }
        else {
            toastr.warning('Selecciona al menos una categoría.');
        }
    }
    else {
        toastr.warning("Agrega un acreedor.")
    }
}

function CategorysSupplier() {
    if ($("#creditorsSupplierSelect2").val() != "0") {
        axios.get("/ItemSupplierCreditors/ActionCategorysCreditors/?Supplier=" + $("#creditorsSupplierSelect2").val())
            .then(data => {
                if (data.data == "SF") {
                    SessionFalse(sesion)
                }
                if (data.data.length > 0) {
                    CheckList()
                    $.each(data.data, function (index, value) {
                        $(`input[type="checkbox"][name="available"][value="${value}"]`).iCheck('check');
                    });
                    
                }
            })
            .catch(error => {
                toastr.error("Contacte a sistemas")
            })
            .then(() => {
                tableCreditors.clear();
                var proveder = $("#creditorsSupplierSelect2").val()
                if (creditorList.filter(element => element.SupplierId == proveder).length > 0) {
                    tableCreditors.rows.add(jQuery(creditorList.filter(element => element.SupplierId == proveder)));

                }
                else {
                    tableCreditors.rows.add(jQuery(creditorList));
                    CheckList()
                }
                tableCreditors.columns.adjust().draw();
            })
    }
}
function CheckList() {
    $("#assignedList").html(categoryList.map(element => liCategory(element.Name)(element.Id)))
    $('#assignedList').find('input[type="checkbox"][name="available"]').iCheck({
        checkboxClass: 'icheckbox_square-green'
    });
    $('#assignedList').slimScroll({
        height: '200px'
    });
}
function GetAllSupplierCreditors() {
    if ($("#creditorsSupplierSelect2").val() != "0") {
        axios.get("/ItemSupplierCreditors/ActionGetAllSupplierCreditors/")
            .then(data => {
                if (data.data == "SF") {
                    SessionFalse(sesion)
                }
                if (data.data.length > 0) {
                    creditorList = data.data
                    tableCreditors.clear();
                    tableCreditors.rows.add(jQuery(creditorList));
                    tableCreditors.columns.adjust().draw();
                }
            })
            .catch(error => {
                toastr.error("Contacte a sistemas")
            })
    }

}
/*Termina Proveedores Productos*/
/*componente*/
function selectGeneric(id, list, array, firstOption) {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    const selectList = arrayOption => elements => selectOption(elements[arrayOption[0]])(elements[arrayOption[1]]);
    $(id).html([`${selectOption(firstOption.id)(firstOption.text)}`, ...list.map(elements => selectList(array)(elements))])
}
/*fin de los componentes*/
const isDecimal = decimal => number => {
    return number.toPrecision(decimal);
}
$("#creditorsPriceForm").keyup(() => {
    //const cantidad = parseFloat($("#creditorsPriceForm").val())
    //if (!isNaN(cantidad)) {
    //    if (cantidad >= 0) {
    //        if (isDecimal(4)(cantidad)) {
    //            $("#creditorsPriceForm").val(cantidad.toString())
    //        }
    //        else {
    //            toastr.warning("Superaste los decimales permitidos")
    //            $("#creditorsPriceForm").val(0)
    //        }
    //    }
    //    else {
    //        toastr.warning("Coloca un precio positivo")
    //        $("#creditorsPriceForm").val(0)
    //    }
    //}
    //else {
    //    toastr.warning("Favor de ingresar un precio valido")
    //    $("#creditorsPriceForm").val(0)
    //}
    //ValidationNumber();
})

function ValidationNumber() {
    const cantidad = parseFloat($("#creditorsPriceForm").val())
    if (!isNaN(cantidad)) {
        if (cantidad >= 0 && cantidad <= 9000000) {
            var newCantidad = isDecimal(4)(cantidad);
            $("#creditorsPriceForm").val(newCantidad);
            
        }
        else if (cantidad >= 9000000) {
            toastr.warning("Favor de no agregar un valor tan grande")
            $("#creditorsPriceForm").val(0)
        }
        else {
            toastr.warning("Coloca un precio positivo")
            $("#creditorsPriceForm").val(0)
        }
    }
    else {
        toastr.warning("Favor de ingresar un precio valido")
        $("#creditorsPriceForm").val(0)
    }
}