﻿/*productos*/
const productModal = { id: 0, part_number: document.getElementById("ProductPartNumberForm"), description: document.getElementById("ProductDescriptionForm")/*,category: document.getElementById("productSelect")*/ };
var productList = [];
var typeList = []
var IsPostProduct = true;
var tableProducts = $('#productTable').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [2],
            visible: false,
            searchable: false
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                return `<button class="btn  btn-xs btn-outline btn-warning" onclick="EditProduct('N_${data.PartNumber}')"><i class='fa fa-pencil'></i> Editar</button>`;
            }
        },
        {
            targets: [5],
            render: function (data, type, full, meta) {
                var texto = data ? "Si" : "No"
                return `${texto}`;
            }
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                var texto = data ? "Activo" : "Desactivado"
                return `${texto}`;
            }
        }
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
    ],
    columns:
        [
            { data: 'PartNumber' },
            { data: 'Description' },
            { data: 'CategoryId' },
            { data: 'CategoryName' },
            { data: 'Type' },
            { data: 'RequisitionFlag' },
            { data: 'ActiveFlag' },
            { data: null }

        ],
});
function SaveProducts() {
    //$("#ProductModal").modal("show");
    //$("#buttonSaveProduct").html("Guardar");
    $("#productsTitle").html("Agregar Producto")
    DivProductsOption(false)
    IsPostProduct = true;
    productModal.part_number.disabled = false;
    SelectProduct()
    productModal.description.value = ""
    productModal.part_number.value = ""
    productModal.id = 0
    SelectType()
    $("#ProductRequisitionForm").iCheck('uncheck');
    $("#ProductStateForm").iCheck('uncheck');
    $("#buttonSaveProduct").html("Guardar")
}

function EditProduct(id) {

    var index = productList.findIndex(elements => elements.PartNumber == id.split("N_")[1]);
    //if (productList.filter(element => element.PartNumber.toUpperCase() == productModal.part_number.value.toUpperCase().trim()).length == 0 || productList.filter(element => element.PartNumber.toUpperCase() == productModal.part_number.value.toUpperCase().trim())[0].part_number == productList[index].PartNumber) {
    DivProductsOption(false)
    IsPostProduct = false;
    //console.table(productList[index])
    SelectProduct()
    productModal.description.value = productList[index].Description
    productModal.part_number.value = productList[index].PartNumber;
    productModal.part_number.disabled = true;
    productModal.id = productList[index].PartNumber;
    $('#productType').val(productList[index].Type).trigger('change');
    $('#productSelect').val(productList[index].CategoryId).trigger('change');
    if (productList[index].RequisitionFlag) {
        $("#ProductRequisitionForm").iCheck('check');
    }
    else {
        $("#ProductRequisitionForm").iCheck('uncheck');
    }

    if (productList[index].ActiveFlag) {
        $("#ProductStateForm").iCheck('check');
    }
    else {
        $("#ProductStateForm").iCheck('uncheck');
    }

    $("#buttonSaveProduct").html("Editar")
    $("#productsTitle").html("Editar Producto")
    var position = $("#ProductPartNumberForm").offset()
    $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
    //}
    //else
    //    toastr.warning("Ya existe")
}

$("#buttonSaveProduct").click(() => {
    if ($("#buttonSaveProduct").html() == "Guardar" && IsPostProduct) {
        if (/"([^"]*)"|'([^']*)'/g.test(productModal.part_number.value.trim())) {
            toastr.warning("Favor de no poner comillas")
        }
        else if ($("#productSelect").val().trim() != "0") {
            if (productModal.description.value.trim() != "") {
                if (productModal.part_number.value.trim() != "") {
                    if ($("#productType").val().trim() != "0") {
                        //console.log({ Id: 0, PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') })
                        //productList = [...productList, { PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryName: categoryList.filter(element => element.CategoryId == $("#productSelect").val())[0].Name, CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') }]
                        //tableProducts.clear();
                        //tableProducts.rows.add(jQuery(productList));
                        //tableProducts.columns.adjust().draw();
                        //DivProductsOption(true)

                        if (productList.filter(element => element.PartNumber.toUpperCase() == productModal.part_number.value.toUpperCase().trim()).length == 0) {
                            let CategorySelectProducId = $("#productSelect").val()

                            axios.post("/ItemCreditors/ActionSaveItem", { PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryName: categoryList.filter(element => element.Id == CategorySelectProducId)[0].Name, CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') })
                                .then(data => {
                                    if (data.data == "SF") {
                                        SessionFalse(sesion)
                                    }
                                    else if (data.data.Item.PartNumber != 0) {
                                        productList = [...productList, data.data.Item];
                                        tableProducts.clear();
                                        tableProducts.rows.add(jQuery(productList));
                                        tableProducts.columns.adjust().draw();

                                        creditorList = data.data.Products;
                                        tableCreditors.clear();
                                        tableCreditors.rows.add($(creditorList));
                                        tableCreditors.columns.adjust().draw();

                                        DivProductsOption(true)
                                        toastr.success("Guardado con exito");
                                    }
                                    else {
                                        toastr.error("Hubo un problema al guardar. Contacte a sistemas.")
                                    }
                                })
                                .catch(error => {
                                    toastr.error("Contacte a sistemas.")
                                })
                        }
                        else
                            toastr.warning("Ya existe este código con otro producto/servicio.")
                    }
                    else {
                        toastr.warning("Seleccione un tipo.")
                    }
                }
                else {
                    toastr.warning("Agrega un codigo")
                    productModal.part_number.classList.add("error")
                    setTimeout(() => {
                        productModal.part_number.classList.remove("error")
                    }, 2500)
                }
            }
            else {
                toastr.warning("Agrega una descripción")
                productModal.description.classList.add("error")
                setTimeout(() => {
                    productModal.description.classList.remove("error")
                }, 2500)
            }
        }
        else {
            toastr.warning("Seleccione una categoría.")
        }
    }
    else if ($("#buttonSaveProduct").html() == "Editar" && !IsPostProduct) {
        if (productList.filter(element => element.PartNumber.toUpperCase() == productModal.part_number.value.toUpperCase().trim()).length == 1) {
            if ($("#productSelect").val().trim() != "0") {
                if (productModal.description.value.trim() != "") {
                    if (productModal.part_number.value.trim() != "") {
                        if ($("#productType").val().trim() != "0") {
                            //console.log({ Id: 0, PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') })
                            //productList = [...productList, { PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryName: "Perro", CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') }]

                            //tableProducts.clear();
                            //tableProducts.rows.add(jQuery(productList));
                            //tableProducts.columns.adjust().draw();
                            //DivProductsOption(true)

                            let CategorySelectProducId = $("#productSelect").val()
                            axios.post("/ItemCreditors/ActionEditItem", { PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryName: categoryList.filter(element => element.Id == CategorySelectProducId)[0].Name, CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') })
                                .then(data => {
                                    if (data.data == "SF") {
                                        SessionFalse(sesion)
                                    }
                                    else if (data.data.length > 0) {
                                        var index = productList.findIndex(elements => elements.PartNumber == productModal.id);
                                        productList[index] = { PartNumber: productModal.part_number.value.trim(), Description: productModal.description.value.trim(), CategoryName: categoryList.filter(element => element.Id == CategorySelectProducId)[0].Name, CategoryId: $("#productSelect").val(), Type: $("#productType").val(), RequisitionFlag: $("#ProductRequisitionForm").is(':checked'), ActiveFlag: $("#ProductStateForm").is(':checked') };
                                        tableProducts.clear();
                                        tableProducts.rows.add(jQuery(productList));
                                        tableProducts.columns.adjust().draw();

                                        creditorList = data.data;
                                        tableCreditors.clear();
                                        tableCreditors.rows.add($(creditorList));
                                        tableCreditors.columns.adjust().draw();

                                        DivProductsOption(true)
                                        toastr.success("Editado con exito")
                                    }
                                    else {
                                        toastr.error("Hubo un problema al Editar.")
                                    }
                                })
                                .catch(error => {
                                    toastr.error("Contacte a sistemas.")
                                })
                        }
                        else {
                            toastr.warning("Agrega un tipo.")
                        }
                    }
                    else {
                        toastr.warning("Agrega un codigo.")
                        productModal.part_number.classList.add("error")
                        setTimeout(() => {
                            productModal.part_number.classList.remove("error")
                        }, 2500)
                    }
                }
                else {
                    toastr.warning("Agrega una descripcion")
                    productModal.description.classList.add("error")
                    setTimeout(() => {
                        productModal.description.classList.remove("error")
                    }, 2500)
                }
            }
            else {
                toastr.warning("Seleccione una categorías.")
            }
        }
    }
    else {
        toastr.warning("Hubo un error. Contacte a sistemas.")
    }
})

function SelectProduct() {
    if (categoryList.length > 0) {
        selectGeneric("#productSelect", categoryList, ["Id", "Name"], { id: "0", text: "Seleccione una categoría" })
        $("#select2-chosen-1").html("Seleccione una categoría")
    }
}
function DivProductsOption(value) {
    if (value) {
        $("#formProductDiv").hide()
        $("#tableProductDiv").show()
    }
    else {
        $("#formProductDiv").show()
        $("#tableProductDiv").hide()
    }
}
function SelectType() {
    selectGeneric("#productType", typeList, ["Id", "Name"], { id: "0", text: "Seleccione un tipo" })
    $("#select2-chosen-2").html("Seleccione un tipo")
}
        /*termina productos*/