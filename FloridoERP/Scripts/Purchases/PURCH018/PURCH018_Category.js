﻿const sesion = "Se termino su sesion.";
/* Categoria */
const categoryModal = { id: 0, name: document.getElementById("CategoryNameForm"), description: document.getElementById("CategoryDescriptionForm") };
var categoryList = []
var table = $('#categoryTable').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [0],
            visible: false,
            searchable: false
        },
        {
            targets: [4],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-outline btn-warning" onclick="EditCategory(${data.Id})"><i class='fa fa-pencil'></i> Editar</button>`;
            }
        },
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
    ],
    columns:
        [
            { data: 'Id' },
            { data: 'Name' },
            { data: 'Description' },
            { data: 'BuyerDivision' },
            { data: null }

        ],
});
$("#buttonSaveCategory").click(() => {
    if ($("#buttonSaveCategory").html() == "Guardar") {
        if (categoryModal.name.value.trim() != "" && categoryModal.description.value.trim() != "") {
            if ($("#selectBuyerDivision").val().trim() != "0") {
                if (categoryList.filter(element => element.Name.toUpperCase() == categoryModal.name.value.toUpperCase().trim()).length == 0) {
                    axios.post("/ItemCreditorsCategory/ActionSaveCategorys", { Id: 0, Name: categoryModal.name.value.trim(), Description: categoryModal.description.value.trim(), BuyerDivisionCode: $("#selectBuyerDivision").val(), BuyerDivision: $('#selectBuyerDivision').select2('data').text })
                        .then(data => {
                            if (data.data == "SF") {
                                SessionFalse(sesion)
                            }
                            else if (data.data.Id != 0) {
                                categoryList = [...categoryList, { Id: data.data.Id, Name: data.data.Name, Description: data.data.Description, BuyerDivision: data.data.BuyerDivision }]
                                $("#CategoryModal").modal('hide')
                                table.clear();
                                table.rows.add(jQuery(categoryList));
                                table.columns.adjust().draw();
                                toastr.success("Guardado con exito")
                            }
                            else {
                                toastr.error("Hubo un problema con el producto")
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            toastr.error("Contacte a sistemas")
                        })
                        .then(() => {
                            categoryModal.name.value = ""
                            categoryModal.description.value = ""
                            categoryModal.id = 0;
                            categoryModal.BuyerDivision = "0";
                            SelectProduct()
                        })
                }
                else
                    toastr.warning("Esa categoria ya existe")
            }
            else
                toastr.warning("Seleccione una División de Comprador.");
        }
        else {
            toastr.warning("Favor de rellenar todos los campos")
            if (categoryModal.name.value.trim() == "") {
                categoryModal.name.classList.add("error")
            }
            if (categoryModal.description.value.trim() == "") {
                categoryModal.description.classList.add("error")
            }
            setTimeout(() => {
                categoryModal.description.classList.remove("error")
                categoryModal.name.classList.remove("error")
            }, 2500)

        }
    }
    else {
        var index = categoryList.findIndex(elements => elements.Id == categoryModal.id);
        if (categoryList.filter(element => element.Name.toUpperCase() == categoryModal.name.value.toUpperCase().trim()).length == 0 || categoryList.filter(element => element.Name.toUpperCase() == categoryModal.name.value.toUpperCase().trim())[0].Id == categoryList[index].Id) {
            categoryList[index] = { Id: categoryModal.id, Name: categoryModal.name.value.trim(), Description: categoryModal.description.value.trim(), BuyerDivisionCode: $("#selectBuyerDivision").val(), BuyerDivision: $('#selectBuyerDivision').select2('data').text }
            axios.post("/ItemCreditorsCategory/ActionEditCategorys", categoryList[index])
                .then(data => {
                    if (data.data == "SF") {
                        SessionFalse(sesion)
                    }
                    else if (data.data) {
                        table.clear();
                        table.rows.add(jQuery(categoryList));
                        table.columns.adjust().draw();
                        $("#CategoryModal").modal('hide')
                        toastr.success("Editado con exito")
                    }
                    else {
                        toastr.error("Hubo un problema al editar el producto")
                    }
                })
                .catch(error => {
                    console.log(error.data)
                    toastr.error("Contacte a sistemas")
                })
                .then(() => {
                    categoryModal.name.value = ""
                    categoryModal.description.value = ""
                    categoryModal.id = 0;
                    categoryModal.BuyerDivision = "0";
                })
        }
        else
            toastr.warning("Esa categoria ya existe.")
    }
});
function EditCategory(id) {
    $("#buttonSaveCategory").html("Editar");
    const e = categoryList.filter(element => element.Id == id)[0];
    categoryModal.name.value = e.Name;
    categoryModal.description.value = e.Description;
    categoryModal.id = id;
    categoryModal.BuyerDivision = e.BuyerDivisionCode;
    $('#selectBuyerDivision').val(e.BuyerDivisionCode).trigger('change');
    $("#CategoryModal").modal('show')
    $("#categoryTitle").html("<i class='fa fa-puzzle-piece'></i> Editar Categoria")
    $("#categorySubTitle").html("Se Editara la categoria")
}
function ModalOpenCategory() {
    $("#buttonSaveCategory").html("Guardar");
    $("#CategoryModal").modal('show')
    $("#categoryTitle").html("<i class='fa fa-puzzle-piece'></i> Agregar Categoria")
    $("#categorySubTitle").html("Se guardara la categoria")
    categoryModal.name.value = ""
    categoryModal.description.value = ""
    categoryModal.id = 0
    categoryModal.BuyerDivision = "0";
    $('#selectBuyerDivision').val("0").trigger('change');
}
        /*aqui termina la category*/