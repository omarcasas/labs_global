﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#datetimepicker, #datetimepickerPres').datepicker({
        autoclose: true,
        todayHighlight: true,
    }).on("changeDate", function (e) {
        //DateValidation();
    });

    $('#datetimepicker2, #datetimepicker2Pres').datepicker({
        autoclose: true,
        todayHighlight: true,
    }).on("changeDate", function (e) {
        //DateValidation();
    });

    $('#datetimepickerPres').datepicker({
        autoclose: true,
        todayHighlight: true,
    }).on("changeDate", function (e) {
        //DateValidationPres();
    });

    $('#datetimepicker2Pres').datepicker({
        autoclose: true,
        todayHighlight: true,
    }).on("changeDate", function (e) {
        //DateValidationPres();
    });

    $("#depto, #family, #deptoPres, #familyPres").select2();

    $("#buttonPrint").hide();
});

function tab1() {
    $('#tab-1').animatePanel();
}

function tab2() {
    $('#tab-2').animatePanel();
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("0").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("0").trigger('change');
    }
}

function GetFamily() {
    var deptoo = $("#depto").val();
    if (deptoo == "0") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
                $("#family").select2();
            }
            else {
                FillDropdown("family", null);
                $("#family").select2();
            }
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function FillDropdownPres(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("0").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("0").trigger('change');
    }
}

function GetFamilyPres() {
    var deptoo = $("#deptoPres").val();
    if (deptoo == "0") {
        FillDropdownPres("familyPres", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdownPres("familyPres", listFamily);
                $("#familyPres").select2();
            }
            else {
                FillDropdownPres("familyPres", null);
                $("#familyPres").select2();
            }
        },
        error: function () {
            FillDropdownPres("familyPres", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

var table = $("#tableOrdered").DataTable({
    autoWidth: true,
    responsive: true,
    order: [[2, "asc"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'copy', className: 'btn-sm' },
        { extend: 'csv', title: 'CambiosPrecios', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            "width": "1%"
        },
        //{ data: 'sales_price_id', width: "1%" },
        { data: 'part_number', width: "1%" },
        { data: 'part_description', width: "1%" },
        { data: 'family', width: "1%" },
        { data: 'category', width: "1%" },
        { data: 'applied_date', width: "1%" },
        { data: 'margin', width: "1%" },
        { data: 'status', width: "1%" },
        { data: 'sale_price', width: "1%" },
        { data: 'sale_price_iva', width: "1%" },
        { data: 'cdate', width: "1%" },
        { data: 'cuser', width: "1%" }
    ],
    columnDefs:
        [
            {
                targets: [5, 10],
                render: function (data, type, row) {
                    return moment(data).format('L');
                }
            },
            {
                targets: [8, 9],
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(parseFloat(data))(4)}`;
                }
            }
        ]
});

var detailRows = [];

$('#tableOrdered tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetSalesPriceChangesDetail",
        data: { "BeginDate": $("#dateInit").val(), EndDate: $("#dateFin").val(), "PartNumber": d.part_number },
        type: "GET",
        success: function (response) {
            $.each(response, function (index, value) {
                detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td>" + value.family + "</td><td>" + value.category + "</td><td>" + moment(value.applied_date).format('L') + "</td><td>" + value.margin + "</td><td>" + value.status + "</td><td>" + "$" + value.sale_price.toFixed(4) + "</td><td>" + "$" + value.sale_price_iva.toFixed(4) + "</td><td>" + moment(value.cdate).format('L') + "</td><td>" + value.cuser + "</td></tr>"
            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Descripción</th><th>Departamento</th><th>Categoria</th><th>Fecha Aplicacion</th><th>Margen</th><th>Estatus</th><th>Precio</th><th>Precio con IVA</th><th>Fecha registro</th><th>Usuario</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function DateValidation() {
    $("#buttonPrint").hide();
    if ($("#dateInit").val().trim() != "" && $("#dateFin").val().trim() != "" && $("#dateFin").val() != null && $("#dateInit").val() != null) {
        var dateInit = moment($("#dateInit").val());
        var dateFin = moment($("#dateFin").val());
        var department = $("#depto").val();
        var family = $("#family").val();
        if (dateInit <= dateFin) {
            StartLoading();
            axios.post("/Purchases/ActionGetSalesPriceChanges", { BeginDate: $("#dateInit").val(), EndDate: $("#dateFin").val(), "Department": department, "Family": family })
                .then(function (response) {
                    EndLoading();
                    if (response.data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.data.length > 0) {
                        table.clear();
                        table.rows.add($(response.data));
                        table.columns.adjust().draw();
                        $("#buttonPrint").show();
                    }
                    else {
                        toastr.warning('No se encontraron cambios de precio.');
                        table.clear();
                        table.columns.adjust().draw();
                    }
                }).catch(function (error) {
                    toastr.error("Contacta con sistemas");
                }).then(() => {
                    EndLoading();
                })
        }
        else {
            table.clear();
            table.columns.adjust().draw();
            toastr.error('La primera fecha no puede ser mayor a la segunda fecha');
        }
    }
    else
        table.clear().draw();
}

var tablePres = $("#Presentations").DataTable({
    autoWidth: true,
    responsive: true,
    order: [[2, "asc"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'copy', className: 'btn-sm' },
        { extend: 'csv', title: 'CambiosPrecios', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            "width": "1%"
        },
        //{ data: 'sales_price_presentation_id', width: "1%" },
        { data: 'presentation_id', width: "1%" },
        { data: 'presentation_name', width: "1%" },
        { data: 'family', width: "1%" },
        { data: 'category', width: "1%" },
        { data: 'applied_date', width: "1%" },
        { data: 'margin', width: "1%" },
        { data: 'status', width: "1%" },
        { data: 'sale_price', width: "1%" },
        { data: 'sale_price_iva', width: "1%" },
        { data: 'cdate', width: "1%" },
        { data: 'cuser', width: "1%" }
    ],
    columnDefs:
        [
            {
                targets: [5, 10],
                render: function (data, type, row) {
                    return moment(data).format('L');
                }
            },
            {
                targets: [8, 9],
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(parseFloat(data))(4)}`;
                }
            }
        ]
});

var detailRowsPres = [];

$('#Presentations tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tablePres.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tablePres.row('.details').length) {
            $('.details-control', tablePres.row('.details').node()).click();
        }
        row.child(formatPres(row.data())).show();
        tr.addClass('details');
    }
});

tablePres.on('draw', function () {
    $.each(detailRowsPres, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatPres(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetSalesPriceChangesDetailPresentations",
        data: { "BeginDate": $("#dateInitPres").val(), "EndDate": $("#dateFinPres").val(), "PresentationId": d.presentation_id },
        type: "GET",
        success: function (response) {
            $.each(response, function (index, value) {
                detailItems += "<tr><td>" + value.presentation_id + "</td><td>" + value.presentation_name + "</td><td>" + value.family + "</td><td>" + value.category + "</td><td>" + moment(value.applied_date).format('L') + "</td><td>" + value.margin + "</td><td>" + value.status + "</td><td>" + "$" + value.sale_price.toFixed(4) + "</td><td>" + "$" + value.sale_price_iva.toFixed(4) + "</td><td>" + moment(value.cdate).format('L') + "</td><td>" + value.cuser + "</td></tr>"
            });
            tabledetail.html('<table id="tabledetailPres" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Presentación</th><th>Departamento</th><th>Categoria</th><th>Fecha Aplicacion</th><th>Margen</th><th>Estatus</th><th>Precio</th><th>Precio con IVA</th><th>Fecha registro</th><th>Usuario</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTablePres();
        }
    });
    return tabledetail
}

function reloadStyleTablePres() {
    $('#tabledetailPres').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function DateValidationPres() {
    $("#buttonPrint").hide();
    if ($("#dateInitPres").val().trim() != "" && $("#dateFinPres").val().trim() != "" && $("#dateFinPres").val() != null && $("#dateInitPres").val() != null) {
        var dateInit = moment($("#dateInitPres").val());
        var dateFin = moment($("#dateFinPres").val());
        var department = $("#deptoPres").val();
        var family = $("#familyPres").val();
        if (dateInit <= dateFin) {
            StartLoading();
            axios.post("/Purchases/ActionGetSalesPriceChangesPresentations", { BeginDate: $("#dateInitPres").val(), EndDate: $("#dateFinPres").val(), "Department": department, "Family": family })
                .then(function (response) {
                    EndLoading();
                    if (response.data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.data.length > 0) {
                        tablePres.clear();
                        tablePres.rows.add($(response.data));
                        tablePres.columns.adjust().draw();
                        $("#buttonPrint").show();
                    }
                    else {
                        toastr.warning('No se encontraron cambios de precio.');
                        tablePres.clear();
                        tablePres.columns.adjust().draw();
                    }
                }).catch(function (error) {
                    toastr.error("Contacta con sistemas");
                }).then(() => {
                    EndLoading();
                })
        }
        else {
            tablePres.clear();
            tablePres.columns.adjust().draw();
            toastr.error('La primera fecha no puede ser mayor a la segunda fecha');
        }
    }
    else
        tablePres.clear().draw();
}

function PrintTable() {
    $("#iframes").html("")
    if ($("#dateInit").val().trim() != "" && $("#dateFin").val().trim() != "" && $("#dateFin").val() != null && $("#dateInit").val() != null) {
        var dateInit = moment($("#dateInit").val());
        var dateFin = moment($("#dateFin").val())
        if (dateInit <= dateFin) {
            StartLoading()
            axios.post("/Supplier/ActionPrintTablePurchasePrice", { LaterDate: $("#dateInit").val(), NextDate: $("#dateFin").val() })
                .then(function (date) {
                    if (date.data === "SF") {
                        window.location.href = 'http://' + window.location.host + '/Home/Login';
                    }
                    else if (date.data.success) {
                        $("#iframes").html(`<iframe src='data:application/pdf;base64,${date.data.responseText}' height='320px' width='100%'></iframe>`)
                    }
                    else if (!date.data.success) {
                        toastr.warning("No se encontraron precios en ese tiempo")
                    }
                    else {
                        toastr.warning("No se encontraron precios en ese tiempo")
                    }
                }).catch(function (error) {
                    toastr.error("Contacta con sistemas");
                })
                .then(() => { EndLoading() })
        }
        else
            toastr.warning("No se encontro ningun reporte para ser impreso");
    }
}