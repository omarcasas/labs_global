﻿const session = "Se termino su sesion.";
$("#Item").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Item/ActionSearchItem",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.PathNumber} - ${item.Description}`,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

var table = $('#Items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[0, "asc"]],
    columnDefs: [
        //{
        //    data: null,
        //    width: "1%",
        //    defaultContent: "<input type='text' placeholder='Nuevo Stock' class='form-control Numeric' />",
        //    orderable: false,
        //    targets: [4]
        //},
        //{
        //    data: 'PartNumber',
        //    width: "1%",
        //    render: function (data, type, row) {
        //        return "<table style='width:100%'><tr><td><input type='radio' name='movimiento_" + data + "' class='form-control' value='Credito'> Crédito</input></td></tr><tr><td><input type='radio' name='movimiento_" + data + "' class='form-control' value='Debito'> Débito</input></td></tr></table>";
        //    },
        //    orderable: false,
        //    targets: [5]
        //},
        //{
        //    data: null,
        //    width: "1%",
        //    defaultContent: "<input type='text' maxlength='49' placeholder='Comentarios' class='form-control' />",
        //    orderable: false,
        //    targets: [6]
        //},
        //{
        //    data: null,
        //    width: "1%",
        //    defaultContent: "<button class='btn btn-xs btn-outline btn-danger btn-cancel' type='button'><i class='fa fa-trash'></i> Remover</button>",
        //    targets: [7],
        //    orderable: false
        //}
    ],
    columns: [
        { targets: 0, data: 'PartNumber', width: "1%" },
        { targets: 1, data: 'Barcode', width: "1%" },
        { targets: 2, data: 'Cuser', width: "1%" },
    ],
});

function FullTable(table, List) {
    table.clear().draw();
    table.rows.add(List)
    table.columns.adjust().draw();
}

function SearchBarcode() {
    var item = document.getElementById("Item").value
    if (item != null && item.trim() != "") {
        StartLoading();
        InfoBarcode(item);
    }
    FullTable(table, [])
}

function InfoBarcode(item) {
    axios.get(`/item/ActionSearchBarcode/?partNumber=${item}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion);
            }
            else if (data.data.length > 0) {
                FullTable(table, data.data)
            }
            else {
                toastr.warning("No se encontraron codigos de barras")
            }
        }).catch(error => {
            toastr.error("A ocurrido un error favor de contactar a sistemas")
        }).then(() => { EndLoading(); })
}

function SaveBarcode() {
    var item = document.getElementById("Item").value;
    var barcode = document.getElementById("inputBarcode").value;
    var noValido = / /;
    var special = /[&\/\#,+()$~%'":*?<>{}°_!$¡¿'`[]|]/;
    if (item != null && item.trim() != "" && barcode != null && barcode.trim() != "" &&
        !noValido.test(barcode.trim()) && !noValido.test(item.trim()) && !special.test(barcode.trim())) {
        StartLoading();
        $("#modalAddBarcode").modal("hide");
        axios.post(`/item/ActionSaveBarcode/`, { partNumber: item, barcode: barcode })
            .then(data => {
                if (data.data == "SF") {
                    EndLoading();
                    SessionFalse(sesion);
                }
                else if (data.data == 1) {
                    toastr.success("Se a guardado correctamente")
                    InfoBarcode(item)
                }
                else if (data.data == 2) {
                    EndLoading();
                    toastr.warning("El codigo ya se encuentra ingresado favor de colocar otro")
                }
                else if (data.data == 3) {
                    EndLoading();
                    toastr.error("Problema al guardar el codigo de barra")
                }
                else {
                    EndLoading();
                    toastr.error("Error desconosido contacte a sistemas")
                }
            }).catch(error => {
                EndLoading();
                toastr.error("A ocurrido un error favor de contactar a sistemas")
            })
    }
    else
        toastr.warning("Ingrese un codigo de barras valido")
}

function ModalOpen() {
    var item = document.getElementById("Item").value;
    if (item != null && item.trim() != "") {
        document.getElementById("inputBarcode").value = "";
        document.getElementById("inputPartNumber").value = document.getElementById("Item").value;
        $("#modalAddBarcode").modal("show");
    }
    else
        toastr.warning("Seleccione una opcion valida")
}

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});