﻿var purchase_order_local = "";
var subtotal = 0, iva = 0, ieps = 0, total = 0
var subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd = 0
const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0)
var detailPO = "0";

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    /*var Products = JSON.parse($("#model").val());
    FillTableTotals(Products, "1");

    table.clear();
    table.rows.add($(Products));
    table.columns.adjust().draw();*/

    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
});

function checkDetailOC() {
    if ($("#cbox_ReportType").is(':checked')) {
        $("#TableDivDetail").show();
        $("#TableDiv").hide();
        detailPO = "1";

    } else {
        $("#TableDiv").show();
        $("#TableDivDetail").hide();
        detailPO = "0";
    }
    SerchPO();
}

function FillTableTotals(Products, productsType) {
    subtotal = 0, iva = 0, ieps = 0, total = 0
    subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd = 0
    if (productsType == "1") {
        Products.forEach(function (element) {
            if (element.Currency == "MXN") {
                subtotal += element.ItemAmount;
                iva += element.Iva;
                ieps += element.Ieps;
                total += element.ItemTotalAmount;
            }
            else {
                subtotalusd += element.ItemAmount;
                ivausd += element.Iva;
                iepsusd += element.Ieps;
                totalusd += element.ItemTotalAmount;
            }
        });
        document.getElementById("mxsubtotal").innerHTML = formatmoney(subtotal)(4);
        document.getElementById("usdsubtotal").innerHTML = formatmoney(subtotalusd)(4);
        document.getElementById("mxiva").innerHTML = formatmoney(iva)(4);
        document.getElementById("usdiva").innerHTML = formatmoney(ivausd)(4);
        document.getElementById("mxieps").innerHTML = formatmoney(ieps)(4);
        document.getElementById("usdieps").innerHTML = formatmoney(iepsusd)(4);
        document.getElementById("mxtotal").innerHTML = formatmoney(total)(4);
        document.getElementById("usdtotal").innerHTML = formatmoney(totalusd)(4);
    } else {
        Products.forEach(function (element) {
            if (element.Currency == "MXN") {
                subtotal += element.SubTotal;
                iva += element.IVA;
                ieps += element.IEPS;
                total += element.Amount;
            }
            else {
                subtotalusd += element.SubTotal;
                ivausd += element.IVA;
                iepsusd += element.IEPS;
                totalusd += element.Amount;
            }
        });
        document.getElementById("mxsubtotalDetail").innerHTML = formatmoney(subtotal)(4);
        document.getElementById("usdsubtotalDetail").innerHTML = formatmoney(subtotalusd)(4);
        document.getElementById("mxivaDetail").innerHTML = formatmoney(iva)(4);
        document.getElementById("usdivaDetail").innerHTML = formatmoney(ivausd)(4);
        document.getElementById("mxiepsDetail").innerHTML = formatmoney(ieps)(4);
        document.getElementById("usdiepsDetail").innerHTML = formatmoney(iepsusd)(4);
        document.getElementById("mxtotalDetail").innerHTML = formatmoney(total)(4);
        document.getElementById("usdtotalDetail").innerHTML = formatmoney(totalusd)(4);
    }
}

/*Recien agregado*/
$("#ItemSelect").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Item/ActionSearchItem/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

$("#site, #depto, #family, #currency").select2();

$("#SuppliersDrop").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Supplier/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.BusinessName,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

/*Recien agregado*/
$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
});

var table = $('#TablePurchases').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'PurchaseNo' },
        { data: 'SiteName' },
        { data: 'supplier_name' },
        { data: 'Eta' },
        { data: 'invoice' },
        { data: 'Currency' },
        { data: 'ItemAmount' },
        { data: 'Iva' },
        { data: 'Ieps' },
        { data: 'ItemTotalAmount' },
        { data: 'purchase_status_description' }
    ],
    columnDefs: [{
        targets: [0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        width: "1%"
    },
    {
        targets: [2, 3],
        width: "1%"
    },
    {
        targets: [6, 7, 8, 9, 10],
        className: 'text-right'
    },
    {
        targets: [4],
        render: function (data, type, row) {
            return moment(data).format('L');
        },
    },
    {
        type: 'numeric-comma',
        render: function (data, type, row) {
            return formatmoney(data)(4);
        },
        targets: [7, 8, 9, 10]
    },
    {
        data: null,
        width: "1%",
        targets: 12,
        defaultContent: '<button class="btn btn-xs btn-outline btn-info" type="submit"><i class="fa fa-file-text-o"></i> Reporte</button>'
    }]
});

var detailRows = [];
$("#TablePurchases").append('<tfoot><tr><th colspan = "2"></th><th colspan = "2" style="color:#008000;">Totales</th><th  colspan = "3" style="color:#008000;">MNX</th><th id="mxsubtotal" style="color:#008000;">$0</th><th id="mxiva" style="color:#008000;">$0</th><th id ="mxieps" style="color:#008000;">$0</th><th id ="mxtotal" colspan = "3" style="color:#008000;">$0</th></tr><tr><th colspan = "2"></th><th colspan = "2"  style="color:#0D0DFF;">Totales</th><th  colspan = "3"  style="color:#0D0DFF;">USD</th><th id="usdsubtotal" style="color:#0D0DFF;">$0</th><th id="usdiva"  style="color:#0D0DFF;">$0</th><th id ="usdieps"  style="color:#0D0DFF;">$0</th><th id ="usdtotal" colspan = "3" style="color:#0D0DFF;">$0</th></tr></tfoot>');

$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetPurchaseDetail",
        data: { "PoNumber": d.PurchaseNo, "SiteCode": d.SiteCode },
        type: "GET",
        success: function (returndate) {
            $.each(returndate.Json, function (index, value) {
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.UnitSize + "(" + value.Parcking + ")" + "</td><td class='text-right'>" + "$" + value.PurchasePrice.toFixed(4) + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.ItemAmount.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Iva.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Ieps.toFixed(4) + "</td><td class='text-right'>" + "$" + value.ItemTotalAmount.toFixed(4) + "</td></tr>"
            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Precio</th><th>Cantidad</th><th>Sub Total</th><th>Iva</th><th>Ieps</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function SerchPO() {

    if ($('#ValidDate2').val().trim() != "" && $('#ValidDate1').val().trim() != "") {
        if (moment($('#ValidDate2').val()) >= moment($('#ValidDate1').val())) {

            var sitecode = $("#site").val();
            if (sitecode == "0") {
                toastr.warning("Seleccione una tienda");
                return;
            }
            else {
                var supplier = $('#SuppliersDrop').val();
                if (supplier == "") {
                    supplier = 0;
                }

                var product = $("#ItemSelect").val();
                var sitecode = $("#site").val();
                var department = $("#depto").val();
                var family = $("#family").val();
                var currency = $("#currency").val();

                if (detailPO == "0") {
                    StartLoading();
                    $('#TableDiv').animatePanel();
                    $.ajax({
                        type: "GET",
                        url: "/Purchases/ActionGetAllPurchasesOrdersXML",
                        data: { "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "siteCode": sitecode, "supplier": supplier, "partNumber": product, "department": department, "family": family, "currency": currency },
                        success: function (response) {
                            EndLoading();
                            if (response.success) {
                                FillTableTotals(response.Json, "1")
                                table.clear();
                                table.rows.add($(response.Json));
                                table.columns.adjust().draw();
                            }
                            else
                                toastr.warning('Alerta - Error inesperado!!');
                        },
                        error: function () {
                            EndLoading();
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                        }
                    });
                } else {
                    StartLoading();
                    $('#TableDivDetail').animatePanel();
                    $.ajax({
                        type: "GET",
                        url: "/Purchases/ActionGetAllPurchasesOrdersStatusSupplierDetailXML",
                        data: { "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "siteCode": sitecode, "supplier": supplier, "partNumber": product, "department": department, "family": family, "currency": currency },
                        success: function (response) {
                            EndLoading();
                            if (response) {
                                tableDetail.clear();
                                tableDetail.rows.add($(response));
                                tableDetail.columns.adjust().draw();
                                FillTableTotals(response, "0");
                            }
                            else
                                toastr.warning('Alerta - Error inesperado!!');
                        },
                        error: function () {
                            EndLoading();
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                        }
                    });
                }
            }
        }
        else {
            toastr.warning("Favor de colocar la fecha correctamente")
            return;
        }
    }
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function SendEmailPurchases() {
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/PurchaseOrderItem/ActionSendEmailByPurchases",
        data: { "purchase": purchase_order_local },
        success: function (returndates) {
            EndLoading();
            if (returndates.success) {
                toastr.success("Email enviado correctamente.");
            } else {
                toastr.warning("El email no pudo ser enviado.");
            }
        },
        error: function (returndates) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}

$('#TablePurchases tbody').on('click', 'button', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'PurchaseNo':
                StartLoading();
                $.ajax({
                    type: "GET",
                    url: "/PurchaseOrderItem/ActionGetEmailsPurchase",
                    data: { "purchase": value },
                    success: function (returndates) {
                        purchase_order_local = value;
                        var elementTitle = document.getElementById('DivEmailsShow').title;
                        if (returndates.success) {
                            $("#DivEmailsShow").tooltip({ title: "" });
                            if (returndates.emailSuplier == "") {
                                $('#DivEmailsShow').attr('data-original-title', 'something else2');
                            } else {
                                $('#DivEmailsShow').attr('title', returndates.emailSuplier);
                            }
                        }
                        else {
                            if (returndates.emailSuplier == "SF")
                                SessionFalse("Terminó su sesión.");
                        }
                    },
                    error: function (returndates) {
                        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                        EndLoading();
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/OrderLists/ActionGetPurchasesOrderReport",
                    data: { "poNumber": value },
                    success: function (returndates) {
                        document.getElementById("iframe").srcdoc = returndates;
                        EndLoading();
                        $("#ModalDescription").html('Reporte de orden de compra Numero: ' + value + '');
                        $('#ModalReport').modal('show');
                    },
                    error: function (returndates) {
                        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                        EndLoading();
                    }
                });
                break;
        }
    });
});

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option value="">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

function GetFamily() {
    var deptoo = $("#depto").val();
    if (deptoo == "") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
                $("#family").select2();
            }
            else {
                FillDropdown("family", null);
                $("#family").select2();
            }
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

var tableDetail = $('#TablePurchasesDetail').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: 'Folio' },
        { data: 'PartNumber' },
        { data: 'DescriptionC' },
        { data: 'Department' },
        { data: 'Family' },
        { data: 'MerchandiseEntry' },
        { data: 'MerchandiseEntryEta', visible: false },
        { data: 'Supplier' },
        { data: 'Currency' },
        { data: 'Quantity' },
        { data: 'CostUnit' },
        { data: 'SubTotal' },
        { data: 'IVA' },
        { data: 'IEPS' },
        { data: 'Amount' },
        { data: 'Item_Status' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        width: "1%"
    },
    {
        render: function (data, type, full, meta) {
            if (data == null || data == "")
                return "Esperada para " + full.MerchandiseEntryEta;
            else
                return data;
        },
        targets: [5],
    },
    {
        type: 'numeric-comma',
        render: function (data, type, row) {
            return formatmoney(data)(4);
        },
        targets: [10, 11, 12, 13, 14],
    }]
});

$("#TablePurchasesDetail").append('<tfoot><tr> <th></th><th colspan = "3" style="color:#008000;">Totales</th><th  colspan = "6" style="color:#008000;">MNX</th><th id="mxsubtotalDetail" style="color:#008000;">$0</th><th id="mxivaDetail" style="color:#008000;">$0</th><th id ="mxiepsDetail" style="color:#008000;">$0</th><th id ="mxtotalDetail" colspan = "2" style="color:#008000;">$0</th> </tr> <tr> <th></th><th colspan = "3" style="color:#0D0DFF;">Totales</th><th  colspan = "6" style="color:#0D0DFF;">USD</th><th id="usdsubtotalDetail" style="color:#0D0DFF;">$0</th><th id="usdivaDetail" style="color:#0D0DFF;">$0</th><th id ="usdiepsDetail"  style="color:#0D0DFF;">$0</th><th id ="usdtotalDetail" colspan = "2"  style="color:#0D0DFF;">$0</th></tr></tfoot>');

function SerchPOReport() {
    var supplier = $('#SuppliersDrop').val();
    if (supplier == "") {
        supplier = 0;
    }
    var product = $("#ItemSelect").val();
    if ($('#ValidDate2').val().trim() != "" && $('#ValidDate1').val().trim() != "") {
        if (moment($('#ValidDate2').val()) >= moment($('#ValidDate1').val())) {

        }
        else {
            toastr.warning("Favor de colocar la fecha correctamente")
            return null;
        }
    }
    StartLoading();
    var department = $("#depto").val();
    var family = $("#family").val();
    var currency = $("#currency").val();
    var urlPurchase = "";

    if (detailPO == 0)
        urlPurchase = '/Purchases/ActionGetReportPurchaseXML';
    else
        urlPurchase = '/Purchases/ActionGetReportPurchaseDetailXML';

    $.ajax({
        type: "GET",
        url: urlPurchase,
        data: {
            "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "supplier": supplier, "partNumber": product, "department": department, "family": family, "currency": currency
        },
        success: function (report) {
            if (report != "SF") {
                document.getElementById("iframe").srcdoc = report;
                EndLoading();
                $("#ModalDescription").html('Reporte de Ordenes de Compra');
                $('#ModalReport').modal('show');
            } else {
                SessionFalse("Terminó su sesión.");
            }
        },
        error: function (returndates) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}