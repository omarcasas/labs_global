﻿var part_number_origin_comun_actual = ""; //Variable global del produto origin
var type_promotions_globlal = "";  //Variable global de promociones
const isDecimal = decimal => number => { //Función para validar decimales
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
$(document).on("keypress", '.Numeric', function (event) { //Validador de numeros enteros
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});
$(document).on("keypress", '.Decimal', function (event) { //Validador de numero decimales
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    else
        return true;
});
function DateF(id, type) { //Inputs: Fechas genericas
    $(id).datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: (type == 1 ? moment().add(0, "days").format("MM/DD/YYYY") : "")
        //startDate: moment().add(0, "days").format("MM/DD/YYYY")
    }).on("changeDate", function (e) { ValidateDate(); });
}
function ValidateDate() { //Validar fecha
    if ($('#fechaInicio').val() != "" && $('#fechaFin').val() != "") {
        if (moment($('#fechaInicio').val()) > moment($('#fechaFin').val())) {
            toastr.warning('La fecha inicial no puede ser mayor a la fecha final.');
            $('#fechaFin').val("");
        }
    }
}
$('#assignedList').slimScroll({ height: '200px' }); // Limite LARGO para las lista de tiendas.
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money); //Formato de Decimales
const is90 = number => { //Validación .90
    const numberis90 = /^\d+\.9$/;
    return numberis90.test(number)
}
function blockSites() { $("#bodySites").css({ 'display': "none" }); } // Para desmarcar los precios
function ErrorList(value, type, text) { return !value ? ToastError(type, text) : type == "Succes" ? ToastError(type, text) : true; } //Texto generico
function HidenOrCheckFull(id, value) { value ? $(id).show() : $(id).hide() }  // Oculta y muestra inputs
function ClearRepeatProduct(inputName, productName) {
    $(`#${inputName}`).val('').trigger('change');
    $(`#${inputName}`).select2("open");
    toastr.warning(`Producto ya seleccionado: ${productName}`);
    return false;
}
function ToastError(type, text) {
    if (type == "Succes") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error")
        toastr.error(text)
    else if (type == "Warning")
        toastr.warning(text)
    return false;
}
function ClearTable(table, list) { /// Limpia tablas
    table.clear().draw();
    table.rows.add(list);
    table.columns.adjust().draw();
}
function ValidateTableEmpty() { //Valida que si la tabla no tiene datos muestre visibles muestre por defecto un mensaje predeterminado
    if (tableComun.data().toArray().filter(f => f.delete_flag == false && f.part_number_origin == part_number_origin_comun_actual).length == 0) {
        $("#tableThree > tbody ").empty();
        $("#tableThree > tbody ").append(`<tr class='odd'><td valign='top' colspan='4' class='dataTables_empty'>Ningún dato disponible en esta tabla</td></tr>`);
    }
    if (TableItemsGlobalPromotions.data().toArray().filter(f => f.delete_flag == false).length == 0) {
        $("#TableGlobalPromotions > tbody ").empty();
        var cant = type_promotions_globlal == 'AVANZADA' ? 8 : 5;
        $("#TableGlobalPromotions > tbody ").append(`<tr class='odd'><td valign='top' colspan='${cant}' class='dataTables_empty'>Ningún dato disponible en esta tabla</td></tr>`);
    }
}
function clearProduct() { //Limpiar inputs los productos
    $("#ItemsDrop").val("").trigger("change");
    $("#select2-chosen-1").html("Busque un Producto.");
    $("#productoComunDrop").val('').trigger('change');
    $("#productoComunDrop").select2("open");
}
function ClearAll() {  //Limpiar todas las variables
    $("#tipo").prop("disabled", false);
    $("#promotion_description").prop("disabled", false);
    $('#totalFinal').html("0.0");
    $('input[name="available"]:checked').iCheck('uncheck');
    ["promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, false));
    ["fechaInicio", "fechaFin", "promotion_description", "ItemsDrop"].map(id => $(`#${id}`).val(""))
    $("#tipo").val("").trigger("change");
    blockSites();
    clearProduct();
}
function SwalFunc(titleSwal) { //Mensaje generico de swal
    swal({
        title: titleSwal,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    },
        function (isConfirm) { if (isConfirm) { SavePromotionEdit(); } });
}
function validateTypePromotion(type_promotion) { //Hace dinamica con las tablas
    if (type_promotion != "AVANZADA") {
        ["promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, false));
        $('#text_promotion').html("Agregar Productos");
        $('#ItemsDrop').empty();
        type_promotions_globlal = type_promotion;
        //type_promotions_globlal = 'AVANZADA';
        TableItemsGlobalPromotions.columns([3, 4, 6]).visible(false);
    } else {
        ["promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, true));
        $('#ItemsDrop').empty();
        $('#text_promotion').html("Descripcion de la oferta avanzada y agregar productos.");
        type_promotions_globlal = 'AVANZADA';
        //type_promotions_globlal = "NORMAL";
        TableItemsGlobalPromotions.columns([3, 4, 6]).visible(true);
    }
    ClearTable(TableItemsGlobalPromotions, []);
    ClearTable(tableComun, [])
    clearProduct();
}
function clearAllInputsComun() { /// Limpiar los input y variables de modal
    ["cantidadComun", "precioComun"].map(id => $(`#${id}`).val(""))
    $('#productoComunDrop').val($("#productoComunDrop :first").val()).trigger('change');
    part_number_origin_comun_actual = '';
}
function Select2F(id, text) { //Select2 genericos
    $(id).select2({
        minimumInputLength: 2,
        dropdownAutoWidth: true,
        height: '300px',
        formatInputTooShort: function () { return text; },
        matcher: function (term, text, option) { return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0; }
    });
}
function ValidateFieldsPromotions() { //Validaciones: Cuenta si ha seleccionado tiendas, fechas, tipos de promoción

    sitesL = [];
    $.each($("input[name='available']:checked"), function () {
        sitesL += $(this).val() + ',';
    });
    if (type_promotions_globlal == "AVANZADA") {
        if ($('#promotion_description').val() == null || $('#promotion_description').val().trim() == "") {
            document.getElementById("promotion_description").focus();
            toastr.warning('Ingrese alguna descripción de la promoción.');
            $("#ItemsDrop").val("");
            $("#select2-chosen-1").html("Buscar un Producto.");
            return false;
        }
    }
    if (ErrorList($('#fechaInicio').val() != "", "Warning", "Ingrese una fecha inicial.") &&
        ErrorList($('#fechaFin').val() != "", "Warning", "Ingrese una fecha final.") &&
        ErrorList(moment($('#fechaInicio').val()) <= moment($('#fechaFin').val()), "Warning", "La fecha inicial no puede ser mayor a la fecha final.") &&
        ErrorList(type_promotions_globlal != "", "Warning", 'Selecciona Un Tipo de Oferta') &&
        ErrorList(sitesL.length > 0, "Warning", 'Selecciona al menos 1 Tienda')) {
        return true;
    } else {
        clearProduct();
        return false;
    }
}
function Select2AjaxF(name, status3) { //Select2 generico
    $("#" + name).select2({
        minimumInputLength: 3,
        allowClear: true,
        dropdownAutoWidth: true,
        height: '300px',
        placeholder: "Seleccione una producto",
        initSelection: function (element, callback) { callback({ id: "", text: "Busque un producto." }); },
        ajax: {
            url: "/Item/ActionGetItemPresentationLike",
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (Filter) {
                return {
                    Filter: Filter,
                    status: status3 ? 0 : type_promotions_globlal = "AVANZADA" ? 1 : 0,
                };
            },
            results: function (data) {
                return {
                    results: $.map(data.Json, function (item) {
                        return {
                            text: item.part_description,
                            id: item.part_number
                        }
                    }),
                };
            }
        }
    });
}
jQuery(document).ready(function () {
    Select2AjaxF("ItemsDropSelect", false);
    Select2AjaxF("ItemsDrop", false);
    Select2AjaxF("productoComunDrop", true);
    DateF('#BeginDateDetail', 0);
    DateF('#EndDateDetail', 0);
    DateF('#BeginDate', 0);
    DateF('#EndDate', 0);
    $("#tipo").select2();
    $("#typeSelect").select2();
    $("#site").select2();
    $("#replicationStatus").select2();
    $("#typeSelect").select2();
    DateF('#fechaInicio', 0)
    DateF('#fechaFin', 1)
    $('#assignedList').find('input[name="available"]').iCheck({ checkboxClass: 'icheckbox_square-green' });//Le da formato a los checks
    $('#ckbox_excess').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
    check();
    $('.iCheck-helper:first').on('click', function () {
        if ($("input[type='checkbox']:checked:first").is(':checked') && count == 1) {
            $('input[name="available"]').iCheck('check');
            count = 2;
        }
        else {
            count = 1;
            $('input[name="available"]:checked').iCheck('uncheck');
        }
    });

    $('.iCheck-helper:gt(0)').on('click', function () {
        if ($("input[name='available']:gt(0)").length == $("input[name='available']:gt(0):checked").length) {
            $('input[name="available"]:first').iCheck('check');
            count = 2;
        }
        else {
            count = 1;
            $('input[name="available"]:first').iCheck('uncheck');
        }
    });

    //var itemsModel = JSON.parse($("#ItemsListModel").val());
    var Promotions = JSON.parse($("#PromotionsModel").val());

    TableAnimationDetail('#TableDiv', TablePromotionsDetail, Promotions)
});
var TableItemsGlobalPromotions = $('#TableGlobalPromotions').DataTable({ // Tabla de promociones volates y avanzadas
    "paging": false, "ordering": false, "searching": false, "ordering": false, "paging": false, "info": false, "searching": false, responsive: true, buttons: [], order: [[2, "asc"]], dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>", lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (aData.delete_flag == true) //Esconde los eliminados
            $(nRow).hide();
    },
    columnDefs: [
        {
            targets: [12, 11, 10, 9, 8],
            visible: false
        },
        {
            targets: [2, 4],
            render: function (data, type, full) {
                return `<p>${fomartmoney(data)(4)}</p>`
            }
        },
        {
            width: "1%",
            targets: [3],
            render: function (data, type, full, meta) {
                return `<div class='input-group'><span class='input-group-addon Numeric'><span class='fa fa-cubes'></span></span><input placeholder='Cantidad' type='text' onchange='ValidaQuantityNew(this.value,${meta.row});' value=${data} class='form-control Numeric'></div>`
            }
        },
        {
            width: "1%",
            targets: [5],
            render: function (data, type, full, meta) {
                return `<div class='input-group'><span class='input-group-addon'><span class='fa fa-dollar'></span></span><input placeholder='Precio' type='text' onchange='ValidatePriceNew(this.value,${meta.row});' value=${data} class='form-control Decimal'></div>`
            }
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                return data ? `<button class='btn btn-xs btn-success' onclick='AddEditCommun("${full.part_number}");'><i class='fa fa-cubes'></i> </button>` : `<button class='btn btn-xs btn-primary' onclick='AddEditCommun("${full.part_number}")'><i class='fa fa-cubes'></i></button>`
            }
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                return "<button class='btn btn-xs btn-danger btn-circle' onclick='DeleteProductPromotionToTable(" + meta.row + ")'  style='text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;'><i class='fa fa-times'></i> </button>"
            }
        }
    ],
    columns: [
        { data: 'part_number', width: "1%", }, //0
        { data: 'part_description', width: "3%", }, //1
        { data: 'price_iva_current', width: "1%", }, //2
        { data: 'quantity_promotion', width: "1%", }, //3
        { data: 'total_price_current', width: "1%", }, //4
        { data: 'total_price_promotion', width: "1%", }, //5
        { data: 'comun_flag', width: "1%", }, //6
        { data: 'delete_flag', width: "1%", }, //7
        { data: 'update_flag', width: "1%", visible: false }, //8
        { data: 'presentation_flag', width: "1%", visible: false }, //9
        { data: 'weight_flag', width: "1%", className: "text-center", visible: false }, //10
        { data: 'forced90', width: "1%", className: "text-center", visible: false }, //11
        { data: 'presentation_id', width: "1%", className: "text-center", visible: false } //12
    ]
});
var tableComun = $('#tableThree').DataTable({ /// Tabla común (modal)
    "paging": false, "ordering": false, "searching": false, "ordering": false, "paging": false, "info": false, "searching": false, responsive: true, buttons: [], order: [[2, "asc"]], dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>", lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (part_number_origin_comun_actual != "") {
            if (aData.delete_flag == false && aData.part_number_origin != part_number_origin_comun_actual || aData.delete_flag == true && aData.part_number_origin != part_number_origin_comun_actual || aData.delete_flag == true && aData.part_number_origin == part_number_origin_comun_actual) // No mostrar
                $(nRow).hide();
            else if (aData.delete_flag == false && aData.part_number_origin == part_number_origin_comun_actual) //Solo mostrar cuando no este eliminado y sea del código 'part_number_origin_comun_actual'
                $(nRow).show();
        }
    },
    columns: [
        { data: 'part_number_origin', visible: false }, //0
        { data: 'part_number' }, //1
        { data: 'part_description' }, //2
        { data: 'promotion_past_price' }, //3
        { data: null }, //4
        { data: 'delete_flag', visible: false }, //5
        { data: 'update_flag', visible: false }, //6
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6],
        width: "1%"
    }],
    aoColumnDefs: [
        {
            aTargets: [3],
            mRender: function (data, type, full) {
                return `<p>${fomartmoney(data)(4)}</p>`
            }
        },
        {
            aTargets: [4],
            render: function (data, type, full, meta) {
                return "<button class='btn btn-xs btn-danger btn-circle' onclick='DeleteProductPromotionToTableCommun(" + meta.row + ")'  style='text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;'><i class='fa fa-times'></i> </button>"
            }
        }]
});
ClearAll(); /// Dibujar tablas y esconder algunos valores
function SaveMessageEdit() { // Mensaje de confirmación para guardar
    if (ValidateFieldsPromotions()) {
        if (type_promotions_globlal == "AVANZADA") {
            if (ValidateSumProducts(true)) {
                SwalFunc("¿Esta seguro que desea guardar la oferta AVANZADA? El precio final de la promoción es " + document.getElementById("totalFinal").innerHTML);
            }
        } else {
            if (ValidateAllProductsIs90()) {
                SwalFunc("¿Esta seguro que desea guardar la oferta?");
            }
        }
    }
}
function ItemViewModelPromotion() {
    var self = this;
    self.part_number = ko.observable();
    self.part_description = ko.observable();
    self.price_iva_current = ko.observable();
    self.quantity_promotion = ko.observable();
    self.total_price_current = ko.observable();
    self.total_price_promotion = ko.observable();
    self.comun_flag = ko.observable();
    self.delete_flag = ko.observable();
    self.update_flag = ko.observable();
    self.presentation_flag = ko.observable();
    self.weight_flag = ko.observable();
    self.price_part_ieps = ko.observable();
    self.price_part_iva = ko.observable();
    self.price_part_iva = ko.observable();
    self.forced90 = ko.observable();
    self.presentation_id = ko.observable();
    self.GetInfoProduct = function (inputValue) { //Buscar informacion del producto
        if ($(`#${inputValue}`).val() == "" || $(`#${inputValue}`).val() == 0 || $(`#${inputValue}`).val() == null)
            return false;
        var splitDescription = $(`#${inputValue}`).select2('data').text.split('~');
        console.log("splitDescription");
        console.log(splitDescription);
        var splitPartNumber = $(`#${inputValue}`).select2('data').id.split('~');
        console.log("splitPartNumber");
        console.log(splitPartNumber);
        if (ValidateFieldsPromotions()) {
            $.ajax({
                url: '/Item/ActionGetItemPriceIvaIeps',
                type: "GET",
                data: { "part_number": $(`#${inputValue}`).val(), },
                success:
                    function (returndata) {
                        if (returndata.success == true) {
                            self.part_number(splitPartNumber[0]);
                            self.part_description(splitDescription[1]);
                            self.price_part_ieps(returndata.itemC.Ieps);
                            self.price_part_iva(returndata.itemC.Iva);
                            self.weight_flag(returndata.itemC.weight_flag);
                            self.price_iva_current(parseFloat(returndata.itemC.Price));
                            self.presentation_flag(returndata.itemC.Presentation);
                            self.forced90(returndata.itemC.forced_90);
                            self.presentation_id(returndata.itemC.presentation_id);
                            if (type_promotions_globlal == 'AVANZADA')
                                self.forced90(false);
                            return true;
                        } else {
                            toastr.error('Este producto no tiene información.');
                            return false;
                        }
                    },
                error:
                    function () {
                        toastr.error('Error inesperado contactar a sistemas.');
                        return false;
                    }
            });
        }
    }
    self.AddProductToTable = function () { //Agregar Producto a la tabla
        var checkProduct = true;
        var productNew = [];
        if (ValidateFieldsPromotions() &&
            ErrorList($('#ItemsDrop').val() != "", "Warning", "Seleccione un producto.")) {
            if (TableItemsGlobalPromotions.data().count() > 0) {
                $(TableItemsGlobalPromotions.data()).each(function (index, elem) { //Saber si un producto se encuentra repetido
                    if (self.presentation_flag()) {//Presentaciones 
                        if (elem.presentation_id == self.presentation_id() && elem.delete_flag == false)
                            checkProduct = ClearRepeatProduct("ItemsDrop", elem.part_description);
                    }
                    else                        //Producto
                        if (elem.part_number == self.part_number() && elem.delete_flag == false)
                            checkProduct = ClearRepeatProduct("ItemsDrop", elem.part_description);
                });
            }
            if (!checkProduct)
                return false; //Si encontro un producto repetido
            productNew.push({ 'part_number': self.part_number(), 'part_description': self.part_description(), 'price_iva_current': self.price_iva_current(), 'quantity_promotion': 1, 'total_price_current': 1 * self.price_iva_current(), 'total_price_promotion': 0, 'comun_flag': false, 'delete_flag': false, 'update_flag': true, 'presentation_flag': self.presentation_flag(), 'weight_flag': self.weight_flag(), 'forced90': self.forced90(), 'presentation_id': self.presentation_id() });
            console.log("productNew");
            console.log(productNew);
            TableItemsGlobalPromotions.rows.add($(productNew));
            TableItemsGlobalPromotions.columns.adjust().draw();
            clearProduct();
        }
    }
    self.AddProductToTableComun = function () { //Agregar Producto a la tabla
        var checkProduct = true;
        var productNew = [];
        if (part_number_origin_comun_actual == $(`#productoComunDrop`).select2('data').id) //Si el producto es el mismo que el 'origin'
            return ClearRepeatProduct("productoComunDrop", $(`#productoComunDrop`).select2('data').text);
        if (ValidateFieldsPromotions() &&
            ErrorList($('#productoComunDrop').val() != "", "Warning", "Seleccione un producto.")) {
            if (TableItemsGlobalPromotions.data().count() > 0) {
                $(tableComun.data()).each(function (index, elem) { //Saber si un producto se encuentra repetido
                    if (elem.part_number_origin == part_number_origin_comun_actual && elem.part_number == self.part_number() && elem.delete_flag == false)
                        checkProduct = ClearRepeatProduct("productoComunDrop", elem.part_description);
                });
            }
            if (!checkProduct)
                return false; //Si encontro un producto repetido
            productNew.push({ 'part_number_origin': part_number_origin_comun_actual, 'part_number': self.part_number(), 'part_description': self.part_description(), 'promotion_past_price': self.price_iva_current(), 'delete_flag': false, 'update_flag': true });
            tableComun.rows.add($(productNew));
            tableComun.columns.adjust().draw();
            TableItemsGlobalPromotions.cell(TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false), 6).data(true).draw(); //Update button green
            TableItemsGlobalPromotions.cell(TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false), 8).data(true).draw(); //Update_flag
            TableItemsGlobalPromotions.columns.adjust().draw();
            clearProduct();
        }
    }
}
var vm = new ItemViewModelPromotion();
ko.applyBindings(vm);
function DeleteProductPromotionToTable(index) { //Eliminación de productos
    TableItemsGlobalPromotions.cell(index, 7).data(true).draw(); //Delete_flag
    TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
    TableItemsGlobalPromotions.columns.adjust().draw();//Actualizar
    if (tableComun.data().toArray().filter(f => f.delete_flag == false && f.part_number_origin == TableItemsGlobalPromotions.cell(index, 0).data()).length > 0) { //Eliminar productos comunes
        $(tableComun.data()).each(function (indexComun, elem) {
            if (elem.part_number_origin == TableItemsGlobalPromotions.cell(index, 0).data() && elem.delete_flag == false) {
                tableComun.cell(indexComun, 5).data(true).draw(); //Comunes: Delete_flag
                tableComun.cell(indexComun, 6).data(true).draw(); //Comunes: Update_flag
            }
        });
    }
    ValidateSumProducts(false);
    ValidateTableEmpty();
}
function DeleteProductPromotionToTableCommun(index) { //Elimina productos comunes
    tableComun.cell(index, 5).data(true).draw(); //Delete_flag
    tableComun.cell(index, 6).data(true).draw(); //Update_flag
    tableComun.columns.adjust().draw();
    ValidateTableEmpty();
}
function AddEditCommun(part_number_origin) {
    clearAllInputsComun();
    part_number_origin_comun_actual = part_number_origin; //Importante: siempre sabra cual es el código original
    $('#descripctionComun').val(part_number_origin + " - " + TableItemsGlobalPromotions.data().toArray().filter(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false)[0].part_description);
    $('#precioComun').val(TableItemsGlobalPromotions.data().toArray().filter(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false)[0].total_price_promotion);
    $('#cantidadComun').val(TableItemsGlobalPromotions.data().toArray().filter(f => f.part_number == part_number_origin_comun_actual && f.delete_flag == false)[0].quantity_promotion);
    tableComun.columns.adjust().draw();
    ValidateTableEmpty();
    $('#ModalAddPages').modal('show');
}
function ValidatePriceNew(value, index) { //Validaciones de nuevo precio
    var isRestricted90 = TableItemsGlobalPromotions.cell(index, 11).data();
    if (ErrorList(!isNaN(value), "Warning", "Ingrese un numero correcto por favor.") &&
        ErrorList(isDecimal(4)(parseFloat(value)), "Warning", "Ingrese un numero con cuatro decimales o menos.") &&
        ErrorList((value >= 0.0001 && value < 99999999), "Warning", "Ingrese una cantidad razonable.")) {
        if (type_promotions_globlal != 'AVANZADA') {
            if (isRestricted90) { //Valida si es necesario el terminacion .90 exclusivamente este producto
                if (ErrorList(is90(parseFloat(value)), "Warning", "Precio debe terminar en .90")) { //Debe terminar .90
                    TableItemsGlobalPromotions.cell(index, 5).data(value).draw();
                    TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
                    return true;
                } else {
                    TableItemsGlobalPromotions.cell(index, 5).data(0).draw();
                    TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
                    return false;
                }
            } else { //No es necesario terminar en .90 exclusivamente este producto
                TableItemsGlobalPromotions.cell(index, 5).data(value).draw();
                TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
                return true;
            }
        } else { //No es necesario terminar en .90 por promoción avanzada
            TableItemsGlobalPromotions.cell(index, 5).data(value).draw();
            TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
            ValidateSumProducts(false);
            return true;
        }
    } else { //Error en el precio y coloca 0
        TableItemsGlobalPromotions.cell(index, 5).data(0).draw();
        TableItemsGlobalPromotions.cell(index, 8).data(true).draw(); //Update_flag
    }
    return false;
}
function ValidaQuantityNew(value, index) {//Valida la cantidad a modificar
    var quantity = parseInt(value);
    if (ErrorList(isDecimal(0)(parseFloat(value)), "Warning", "Ingrese una cantidad sin decimales.") &&
        ErrorList((parseFloat(quantity) > 0.1 && parseFloat(quantity) < 999999), "Warning", "Ingrese una cantidad correcta.")) {
        TableItemsGlobalPromotions.cell(index, 3).data(quantity);
        TableItemsGlobalPromotions.cell(index, 4).data(TableItemsGlobalPromotions.cell(index, 2).data() * quantity).draw();
        return true;
    } else
        TableItemsGlobalPromotions.cell(index, 3).data(1).draw();
    return false;
}
function ValidateAllProductsIs90() { //Valida los precios que todos terminen en .90
    if (TableItemsGlobalPromotions.data().toArray().filter(f => f.delete_flag == false).length == 0) {
        toastr.warning("Ingrese al menos un producto.");
        return false;
    }
    var prices = TableItemsGlobalPromotions.data().toArray()
        .filter(element => element.delete_flag == false && element.forced90 == true)
        .reduce(function (filtered, option) {
            if (!is90(parseFloat(option.total_price_promotion))) {
                var priceInvalid = { description: option.part_description, part_number: option.part_number };
                filtered.push(priceInvalid);
            }
            return filtered;
        }, []);
    if (prices.length == 0)
        return true;
    else {
        prices.forEach(function (value) {
            toastr.warning(`El producto ${value.part_number} - ${value.description} su precio debe terminar en .90`);
        });
        return false;
    }
}
function ValidateSumProducts(validate) {//Valida que la suma sea .90
    if (validate && (TableItemsGlobalPromotions.data().toArray().filter(f => f.delete_flag == false).length == 0)) {
        toastr.warning("Ingrese al menos un producto.");
        return false;
    }
    var sumPrices = TableItemsGlobalPromotions.data().toArray()
        .filter(element => element.delete_flag == false)
        .reduce((a, b) => parseFloat(a) + parseFloat(b.total_price_promotion), 0);
    $('#totalFinal').html(fomartmoney(sumPrices)(4));
    if (is90(sumPrices))
        return true;
    else {
        if (validate) {
            toastr.warning("La suma de los productos debe terminar en .901");
            return false;
        }
    }
}

var AprovalReplicate = false;
var replicateButton = JSON.parse($("#BuyerDivisionSistemasModel").val());
var type_promotions_globlal = "AVANZADA";
var editVariable = false;
var firstUpdate = true;
var typePromotionNow = "";
var promotionCodeNow = "";
var IdPage = {
    Selects: [
        { id: "#ItemsDrop", text: "Escriba un producto.", type: 1 }, { id: "#productoComunDrop", text: "Escriba un producto.", type: 1 },
        { id: "#site", text: "", type: 0 }, { id: "#supplier", text: "", type: 0 }, { id: "#ItemsDropSelect", text: "Escriba un producto.", type: 1 },
        { id: "#typeSelect", text: "", type: 0 }, { id: "#replicationStatus", text: "Seleccione una estatus", type: 0 }

    ],
    Date: [{ id: '#fechaInicio', type: 1 }, { id: '#fechaInicioTwo', type: 1 }, { id: '#fechaFin', type: 1 }, { id: "#BeginDate", type: 0 }, { id: "#EndDate", type: 0 }],
    DateDiv: [{ id: "#fechaInicioDiv" }, { id: "#fechaInicioTwoDiv" }],
    TableDivs: [{ id: "#TableDiv" }],
    Table: [{ id: "#TablePromotions" }, { id: "#TableCommons" }, { id: "#tableThree" }, { id: "#tableTwo" }, { id: "#tableOne" }],
    Divs: [{ id: "#SecondDiv" }, { id: "#MainDiv" }],
    Colors: [
        { status: 3, color: "warning", hex: false, liColor: "#f2dd79" }, { status: 1, color: "success", hex: false, liColor: "#9dd188" }, { status: 2, color: "danger", hex: false, liColor: "#d28d8d" },
        { status: 4, color: "#E0C6F9", hex: true, liColor: "#be88d1" }, { status: 5, color: "info", hex: false, liColor: "#7bc0e4" }
    ]
}
$('#Search').on('click', function () { SearchFunction(); });
$('#SearchDetail').on('click', function () { UpdatTables("1"); });
function MessageNo(text) { toastr.warning(text); }
const ValidateDateTable = data => moment(data).format('DD/MM/YYYY') != 'Invalid date' ? moment(data).format('DD/MM/YYYY') : "";
var IsChecked = checkStatus => checkStatus == 1 ? "checked" : "";
function OptionColor(status, row) {
    IdPage.Colors.filter(element => element.status == status)
        .map(element => !element.hex ? $('td', row).addClass(element.color) : $('td', row).css('background-color', element.color))
}
function Select2Full(id, text, type) {
    $(id).select2(type == 1 ? {
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return text;
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    } : {});
}
//function DateFull(id, type) {
//    $(id).datepicker({
//        autoclose: true,
//        todayHighlight: true,
//        startDate: (type == 1 ? moment().add(0, "days").format("MM/DD/YYYY") : "")
//    }).on("changeDate", function (e) {
//        id == "#fechaInicio" || id == "#fechaFin" ? ValidateDates(1) : ValidateDates(2);
//    });
//}
//const Select2Fill = (id, list, options) => list != undefined ? $(id).append(list.map(element => `<option value="${element[options.value]}">${element[options.text]}</option>`)) : null

function TableAnimation(id, table, list) {
    $(id).animatePanel();
    ClearTable(table, list/*!$.isEmptyObject(list) ? list : []*/);
}

function TableAnimationDetail(id, TablePromotionsDetail, list) {
    //$(id).animatePanel();
    ClearTable(TablePromotionsDetail, list/*!$.isEmptyObject(list) ? list : []*/);
}

//$(document).ready(function (model) {
//    $('#ckbox_excess').iCheck({
//        checkboxClass: 'icheckbox_square-green',
//    }).on('ifClicked', function () {
//        $(this).trigger("click");
//    });


//    //var itemsModel = JSON.parse($("#ItemsListModel").val());
//    var Promotions = JSON.parse($("#PromotionsModel").val());

//    TableAnimation('#TableDiv', table, Promotions)
//});

//var table = $(`${IdPage.Table[0].id}`).DataTable({
var table = $(`#TablePromotions`).DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        OptionColor(aData.replication_status, nRow);
    },
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { targets: 1, data: 'promotion_code' },
        { targets: 2, data: 'promotion_descrip' },
        { targets: 3, data: 'promotion_type' },
        { targets: 4, data: 'promotion_start_date' },
        { targets: 5, data: 'promotion_end_date' },
        { targets: 6, data: 'promotion_create_user' },
        { targets: 7, data: 'promotion_date_create' },
        { targets: 8, data: 'promotion_site_code', visible: false },
        { targets: 9, data: 'promotion_site_name' },
        { targets: 10, data: 'replication_status', visible: false },
        { targets: 11, data: null, visible: false },
        { targets: 12, data: null, visible: false },
        { targets: 13, data: null, visible: replicateButton }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        width: "1%"
    },
    {
        targets: [2],
        render: function (data, type, row) {
            if (data != '')
                return data;
            else
                return 'Sin descripción.';
        }
    },
    {
        targets: [4, 5, 7],
        render: function (data, type, row) {
            return ValidateDateTable(data)
        }
    },
    {
        targets: [11],
        render: function (data, type, full, meta) {
            return `<button class="btn btn-xs btn-outline btn-info" onclick="ShowStore('${data.promotion_code}', '${data.promotion_descrip}', '${data.promotion_type}', ${data.promotion_date_start}, ${data.promotion_date_end}, '${data.promotion_create_user}', ${data.promotion_date_create})"><i class="fa fa-cart-arrow-down"></i>  Tiendas</button>`;
        }
    },
    {
        targets: [12],
        render: function (data, type, full, meta) {
            return moment().format('MM/DD/YYYY') <= moment(data.promotion_date_end).format('MM/DD/YYYY') ? `<button class="btn btn-xs btn-outline btn-warning" onclick="ShowEdit('${data.promotion_type}', '${data.promotion_code}', '${data.promotion_descrip}', ${data.promotion_start_date}, ${data.promotion_end_date}, '${data.promotion_create_user}')"> <i class="fa fa-pencil"></i>  Editar</button>` :
                `<button class="btn btn-xs btn-outline btn-default" onclick="MessageNo('No se puede editar una promoción que ha terminado.')"> <i class="fa fa-pencil"></i>  Editar</button>`;
        }
    },
    {
        targets: [13],
        render: function (data, type, full, meta) {
            var datePromotion = moment(data.promotion_date_end).add(1, 'days').format('MM/DD/YYYY');
            var dateNow = moment().format('MM/DD/YYYY');
            if (dateNow <= datePromotion) {
                if (data.promotion_site_code != null) {
                    if (data.replication_status == 2 || data.replication_status == 4 || data.replication_status == 3 || data.replication_status == 0) {
                        //Replicar para tienda por que fue con error o fue modificada o con error en el precio
                        return `<button class="btn btn-xs btn-outline btn-danger" onclick="MessageReplicatePromotions('${data.promotion_code}', '${data.promotion_type}', '${data.promotion_site_code}', '${data.promotion_site_name}')"><i class="fa fa-tasks"></i> Replicar</button>`;
                    } else if (data.replication_status == 1 || data.replication_status == 5) {
                        //No replicar por que todo esta bien.
                        return `<button class="btn btn-xs btn-outline btn-default" onclick="MessageNo('Primero modifique la promoción para replicar.')"><i class="fa fa-tasks"></i> Replicar</button>`;
                    } else if (data.replication_status == 6)
                        return `<button class="btn btn-xs btn-outline btn-default" onclick="MessageNo('La promoción se esta replicando actualmente, intente cuanto termine de replicarse.')"><i class="fa fa-tasks"></i> Replicar</button>`;
                } else {
                    //Replicar a todos los sitios sin excepcion
                    return `<button class="btn btn-xs btn-outline btn-danger" onclick="MessageReplicatePromotions('${data.promotion_code}', '${data.promotion_type}','','')"><i class="fa fa-tasks"></i> Replicar</button>`;
                }
            } else {
                return `<button class="btn btn-xs btn-outline btn-default"onclick="MessageNo('Esta oferta ya ha terminado.')" ><i class="fa fa-tasks"></i> Replicar</button>`;
            }
        }
    }]
});

var tableCommons = $('#TableCommons').DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "NingÃºn dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns: [
        { targets: 0, data: 'part_number' },
        { targets: 1, data: 'description' },

    ],
    columnDefs: [{
        targets: [0, 1],
        width: "1%"
    }]
});

var TablePromotionsDetail = $(`#TablePromotionsDetail`).DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "", visible: false
        },
        { targets: 1, data: 'promotion_code' },
        { targets: 2, data: 'promotion_type' },
        { targets: 3, data: 'part_number' },
        { targets: 4, data: 'promotion_descrip' },
        { targets: 5, data: 'promotion_price_iva' },
        { targets: 6, data: 'promotion_start_date' },
        { targets: 7, data: 'promotion_end_date' },
        { targets: 8, data: 'promotion_create_user' },
        { targets: 9, data: 'promotion_date_create', visible: false },
        { targets: 10, data: 'promotion_site_code', visible: false },
        { targets: 11, data: 'promotion_site_name', visible: false },
        { targets: 12, data: 'replication_status', visible: false },
        { targets: 13, data: null, visible: true },
        { targets: 14, data: null, visible: false },
        { targets: 15, data: null, visible: false }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ],
        width: "1%"
    },
    {
        targets: [6, 7],
        render: function (data, type, row) {
            return ValidateDateTable(data)
        }
    },
    {
        targets: [13],
        render: function (data, type, full, meta) {
            return `<button class="btn btn-xs btn-outline btn-info" onclick="ShowStore('${data.promotion_code}', '${data.promotion_descrip}', '${data.promotion_type}', ${data.promotion_date_start}, ${data.promotion_date_end}, '${data.promotion_create_user}', ${data.promotion_date_create})"><i class="fa fa-cart-arrow-down"></i>  Tiendas</button>`;
        }
    }]
});


function SwalMessage(text, type, funSucces, funcError, parmas) {
    swal({
        title: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            funSucces(parmas);
        else
            funcError(parmas);
    });
}

//function ErrorListF(value, type, text, typeS, textSucces) {
//    return !value ? ToastError(type, text) : typeS != null && typeS != "" ? ToastError(typeS, textSucces) : true;
//}

//function ToastError(type, text) {
//    if (type == "Success") {
//        toastr.success(text)
//        return true;
//    }
//    else if (type == "Error") {
//        toastr.error(text)
//    }
//    else if (type == "Warning") {
//        toastr.warning(text)
//    }
//    return false;
//}

function ReplicatePromotionsAll(params) {
    StartLoading();
    toastr.success("Este proceso puedo durar varios minutos.");
    AjaXGeneric(params)
}

function AjaXGeneric(params) {
    $.ajax({
        type: params.type,
        url: params.url,
        data: params.data,
        success: function (response) {
            params.fun(response, params)
        },
        error: function (response) {
            EndLoading();
            toastr.error(params.error);
        }
    });
}

function PromotionFun(response) {
    EndLoading();
    if (ErrorListF(response.success, "", "", null, null)) {
        if (ErrorListF(response.responseText.ReturnValue == 9, "", "", "Warning", "En algunas tiendas pudo a ver una error, revisar program_id en PROMOTION_SITES.") ||
            ErrorListF(response.responseText.ReturnValue == 4, "", "", "Warning", "Problemas al actualizar los datos en tienda. (Problemas en conexión)") ||
            ErrorListF(response.responseText.ReturnValue == 5, "", "", "Warning", "Problemas al cambiar los precios en tienda (store procedure).") ||
            ErrorListF(response.responseText.ReturnValue == 6, "", "", "Warning", "Problemas al cambiar los precios en tienda en store procedure. (Problemas en conexión).") ||
            ErrorListF(response.responseText.ReturnValue == 7, "", "", "Warning", "No cambiaron todos los precios en tienda por algún motivo. (Revisar tienda)") ||
            ErrorListF(response.responseText.ReturnValue == 8, "", "", "Warning", "Problemas al verificar si habian cambiado precios. (Problemas de conexión)") ||
            ErrorListF(response.responseText.ReturnValue == 0, "", "", "Success", "Proceso realizado con exito")) {
        }
        else {
            toastr.error("Error revisar program_id de PROMOTION_SITES.");
        }
        window.setTimeout(function () { SearchFunction(); }, 3000);
        return null;
    }
    SessionFalse("Su sesión termino.");
}

function MessageReplicatePromotions(promotion_code, promotion_type, promotion_site_code, site_name) {
    if (AprovalReplicate || (promotion_site_code != "" && promotion_site_code != null)) {
        SwalMessage(`¿Deseas replicar esta promoción a ${site_name != "" ? site_name : "Todas las tiendas"}?`, "info",
            ReplicatePromotionsAll,
            function () { },
            {
                data: { "promotion_code": promotion_code, "promotion_type": promotion_type, "site_code": promotion_site_code },
                type: "GET",
                url: "/Promotion/ActionPromotionReplicate",
                fun: PromotionFun,
                error: 'Error inesperado, probablemente por TIMEOUT en la petición.'
            })
    }
    else {
        AprovalReplicate = true;
        setTimeout(function () { AprovalReplicate = false; }, 3000);
        toastr.success('Esta opción envia a todas tiendas que contenga la promoción. Presiona de nuevo el mismo boton.');
    }
}
function check() {
    var isCheck = $("#ckbox_excess").is(':checked');
    [11, 12].map(x => table.column(x).visible(isCheck))
    table.column(9).visible(!isCheck);
    table.column(13).visible(replicateButton);
    editVariable = isCheck;
    SearchFunction();
}
function BacktoReturn() {
    $('#SecondDiv').addClass("hide");
    $('#MainDiv').removeClass("hide");
    $('#MainDiv').animatePanel();
}

function LiGenerator(number1, number2, response, ul) {
    for (var i = number1; i < number2; i++) {
        var li = document.createElement("li");
        if (typeof response[i] !== 'undefined') {
            li.appendChild(document.createTextNode(`  ${response[i].SiteName}  `));
            ul.appendChild(li);
            color = IdPage.Colors.find(element => element.status == response[i].Status);
            li.style.backgroundColor = color != undefined ? color.liColor : "";
        }
    }
}

function ShowStore(folio, descrip, type, start, end, user, date) {
    $.ajax({
        type: "GET",
        url: "/Promotion/ActionGetPromotionByIdSites",
        data: { "FolioPromotion": folio },
        success: function (response) {
            if (response) {
                var l = response.length;
                [
                    { id: "listStores1", range: { number1: 0, number2: 11 } }, { id: "listStores2", range: { number1: 11, number2: 21 } },
                    { id: "listStores3", range: { number1: 21, number2: l } }
                ].map(elements => {
                    var ul = document.getElementById(elements.id);
                    ul.innerHTML = '';
                    LiGenerator(elements.range.number1, elements.range.number2, response, ul)
                })
                if (descrip == "")
                    descrip = "Sin descripción.";
                var dateStart = moment(start).format('DD/MM/YYYY');
                var dateEnd = moment(end).format('DD/MM/YYYY');
                var dateCreate = moment(date).format('DD/MM/YYYY');
                $('#folioOferta').html(folio);
                $('#descriptionOferta').html(descrip);
                $('#tipoOferta').html(type);
                $('#inicioStart').html(dateStart);
                $('#finOferta').html(dateEnd);
                $('#userOferta').html(user);
                $('#createOferta').html(dateCreate);
                $('#showStores').modal('show');
            } else {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}
var detailRows = [];
$(`#TablePromotions tbody`).on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});
table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});
FullTd = element => Option => `<td>${Option == 1 ? fomartmoney(element)(4) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
FullTh = element => `<th>${element}</th>`
FullTable = element => array => array.filter(title => title.option != 8).map(title => FullTd(element[title.title])(title.option));
FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`
function OptionCreateTable(status, type, list) {
    var color = IdPage.Colors.filter(element => element.status == status)
        .map(element => !element.hex ? `class='${element.color}'` : `style='background-color: ${element.color};'`);
    return list.map(value => `<tr ${color[0] == undefined ? "" : color[0]}>${FullTable(value)([
        { title: "part_number", option: 0 },
        { title: "description", option: 0 },
        { title: "quantity", option: type == "AVANZADA" ? 0 : 8 },
        { title: "price", option: 1 }])}${type == "AVANZADA" ? ButtonDetailTable(value) : ""}</tr>`).join().replace(/>,</g, '><').replace(/>, </g, '><')
}
function ButtonDetailTable(value) {
    return value.common == 0 ? `<td><button class='btn btn-xs btn-primary' onclick='MessageNo("Este producto no tiene productos en común.")'><i class='fa fa-cubes'></i> Productos</button></td>` :
        `<td><button class="btn btn-xs btn-success" onclick="ShowPromotionCommonItem('${value.promotion_code}', '${value.part_number}', '${value.price}', '${value.quantity}', '${value.description}')"><i class="fa fa-cubes"></i> Productos</button></td>`
}
function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Promotion/ActionGetPromotionById",
        data: { "FolioPromotion": d.promotion_code, "type": d.promotion_type },
        type: "GET",
        success: function (Data) {
            detailItems = OptionCreateTable(d.replication_status, d.promotion_type, Data)
            header = FullTableHeader(d.promotion_type == "AVANZADA" ? ["Código", "Descripción", "Cantidad", "Precio Promoción", "Comun"] : ["Código", "Descripción", "Precio Promoción"]).replace(/,/g, '')
            var Html = (`<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead>
                                       ${header}
                                        </thead><tbody>${detailItems.length > 0 ? detailItems : "<td colspan='8' style='text-align: center;'>No cuenta con productos</td>"}</tbody></table>`)
            tabledetail.html(Html).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

function SearchFunction() {
    //if (ValidateDates(2)) {
    if (true) {
        AjaXGeneric({
            data: {
                "BeginDate": $('#BeginDate').val(), "EndDate": $("#EndDate").val(), "site_code": $('#site').val(),
                "item": $('#ItemsDropSelect').val(), "type": $('#typeSelect').val(), "AllStores": editVariable, "onlystatus": $('#replicationStatus').val()
            },
            type: "GET",
            url: "/Promotion/ActionGetPromotion/",
            fun: function (result) {
                if (result.status) {
                    TableAnimation('#TableDiv', table, result.model)
                }
                else {
                    SessionFalse("Se terminó su sesion.");
                    TableAnimation('#TableDiv', table, [])
                }
                EndLoading();
            },
            error: 'Error inesperado contactar a sistemas.'
        });
    }
}

//function ValidateDates(type) {
//    var init = type == 1 ? 0 : 3;
//    var fin = type == 1 ? 2 : 4;
//    if (ErrorListF($(IdPage.Date[init].id).val() != "" && $(IdPage.Date[fin].id).val() != "" && !(moment($(IdPage.Date[init].id).val()) <= moment($(IdPage.Date[fin].id).val())),
//        "", "", "Warning", "La fecha inicial no puede ser mayor a la fecha final")) {
//        IdPage.Date[init, fin].map(value => $(value.id).val(""))
//        return false;
//    }
//    else {
//        return true;
//    }
//}

function ShowEdit(type_promotion, promotion_id, descrip, date_start, date_end, user) {
    type_promotions_globlal = type_promotion;
    firstUpdate = true;
    var dateStart = moment(date_start).format('MM/DD/YYYY');
    var dateEnd = moment(date_end).format('MM/DD/YYYY');
    var dateNow = moment().format('MM/DD/YYYY');
    $('#fechaInicio').datepicker('update', dateStart);
    $('#fechaFin').datepicker('update', dateEnd);

    if (dateEnd < dateNow) {
        toastr.warning('La oferta ya concluyo, favor de realizar otra de nuevo.');
        return false;
    }

    if (dateStart <= dateNow) {
        console.log("fecha menor " + dateStart);
        console.log("fechaFin " + dateEnd);
        $('#fechaInicioTwo').datepicker('update', dateStart);
        $('#fechaInicioDiv').show();
        $('#fechaInicioTwoDiv').hide();
    } else {
        console.log("fecha mayor al dia de hoy " + dateStart);
        console.log("fechaFin " + dateEnd);
        $('#fechaInicioTwo').datepicker('update', dateStart);
        $('#fechaInicioDiv').hide();
        $('#fechaInicioTwoDiv').show();
    }
    $('#promotion_description').val(descrip);
    validateTypePromotion(type_promotion);
    LoadInformation(type_promotion, promotion_id);
}

function LoadInformation(type_promotion, promotion_id) {
    promotionCodeNow = promotion_id;
    StartLoading();
    AjaXGeneric({
        data: { "FolioPromotion": promotion_id, "type_promotion": type_promotion },
        type: "GET",
        url: type_promotion != "AVANZADA" ? "/Promotion/ActionGetPromotionItemsEdit" : "/Promotion/ActionGetPromotionAdvItemsEdit",
        fun: LoadInformationAjax,
        type_promotion: type_promotion,
        error: 'Error inesperado contactar a sistemas.'
    });
    EndLoading();
    console.log("type_promotions_globlal333333333");
    console.log(type_promotions_globlal);
}


function LoadInformationAjax(Data, type) {
    if (!Data.Data) {
        if (Data.responseText == "") {
            SessionFalse("Su sesión a terminado.");
        } else if (Data.responseText == "ELIMINADA") {
            toastr.warning("Esta promoción fue eliminada.");
        }
        else {
            toastr.warning(Data.responseText);
        }
    } else {
        //$("#btnSave").prop("disabled", false);
        //type.type_promotion != "AVANZADA" ? "" : countGroupPlus = 1
        //Data.Data.map(value => (
        //    type.type_promotion != "AVANZADA" ? "" : (countPlus = countPlus + 1, countGroupPlus = value.pos),
        //    listAddProducts = [...listAddProducts, {
        //        part_number: value.part_number,
        //        part_description: value.part_description,
        //        promotion_price: value.promotion_price,
        //        ...(type.type_promotion != "AVANZADA" ?
        //            {
        //                base_priceTable: value.base_priceTable,
        //                ieps: value.ieps,
        //                iva: value.iva,
        //                promotion_baseTable: value.promotion_baseTable,
        //                presentation_flag: value.presentation_flag
        //            } :
        //            {
        //                pos: value.pos,
        //                promotion_quantity: value.promotion_quantity,
        //                promotion_past_total: value.promotion_past_total,
        //                promotion_group: value.promotion_group,
        //                promotion_past_price: value.promotion_past_price
        //            })
        //    }],
        //    listFinal = [...listFinal, {
        //        part_number: value.part_number,
        //        common_id: type.type_promotion != "AVANZADA" ? 0 : value.promotion_group,
        //        ...type.type_promotion != "AVANZADA" ?
        //            {
        //                presentation_flag: codigoPresentacion,
        //                promotion_price: value.promotion_price
        //            }
        //            :
        //            {
        //                pos: value.pos,
        //                price: value.pos
        //            }
        //    }],
        //    listNewPrice = [...listNewPrice, {
        //        id: value.pos,
        //        value: value.promotion_origin_price
        //    }]
        //));
        //clearProduct();
        //ClearTable(type.type_promotion != "AVANZADA" ? tableProducts : tableAdvanced, listAddProducts)
        //if (type.type_promotion == "AVANZADA") {
        //    if (Data.Data[0].CommonEdit != null) {
        //        $.each(Data.Data[0].CommonEdit, function (index, value) {
        //            var Product = { part_number: value.part_number, part_description: value.part_description, promotion_past_price: value.promotion_past_price, comun_id: value.comun_id };
        //            listProductComun.push(Product);
        //        });
        //    }
        //    addPrice();
        //    sumPriceIvaIeps();
        //}
        ClearTable(TableItemsGlobalPromotions, Data.Data);
        if (type.type_promotion == "AVANZADA") {
            ClearTable(tableComun, Data.DataComun);
            ValidateSumProducts(false);
            $(Data.DataComun).each(function (index, elem) {
                TableItemsGlobalPromotions.cell(TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == elem.part_number_origin && f.delete_flag == false), 6).data(true).draw(); //Update button green
            });
            TableItemsGlobalPromotions.columns.adjust().draw();
        }
        $('#assignedList').html(Data.Sites.map(value => `<li class="list-group-item">
                                                                        <p class="checkbox">
                                                                            <input ${IsChecked(value.Status)} type="checkbox" name="available" value="${value.SiteCode}">&nbsp;&nbsp;
                                                                            <label id="Label_${value.SiteCode}" for="Label_${value.SiteCode}">${value.SiteName}</label>
                                                                        </p>
                                                                    </li>`));
        $('#assignedList').find('input[name="available"]').iCheck({
            checkboxClass: 'icheckbox_square-green'
        });
        $('.iCheck-helper:first').on('click', function () {
            if ($("input[type='checkbox']:checked:first").is(':checked') && count == 1) {
                $('input[name="available"]').iCheck('check');
                count = 2;
            }
            else {
                count = 1;
                $('input[name="available"]:checked').iCheck('uncheck');
            }
        });

        $('.iCheck-helper:gt(0)').on('click', function () {
            if ($("input[name='available']:gt(0)").length == $("input[name='available']:gt(0):checked").length) {
                $('input[name="available"]:first').iCheck('check');
                count = 2;
            }
            else {
                count = 1;
                $('input[name="available"]:first').iCheck('uncheck');
            }
        });
        //$('.iCheck-helper:first').on('click', function () {
        //    if ($("input[type='checkbox']:checked:first").is(':checked') && count == 1) {
        //        $('input[name="available"]').iCheck('check');
        //        count = 2;
        //    }
        //    else {
        //        count = 1;
        //        $('input[name="available"]:checked').iCheck('uncheck');
        //    }
        //});

        //$('.iCheck-helper:gt(0)').on('click', function () {
        //    if ($("input[name='available']:gt(0)").length == $("input[name='available']:gt(0):checked").length) {
        //        $('input[name="available"]:first').iCheck('check');
        //        count = 2;
        //    }
        //    else {
        //        count = 1;
        //        $('input[name="available"]:first').iCheck('uncheck');
        //    }
        //});
        $('#MainDiv').addClass("hide");
        $('#SecondDiv').removeClass("hide");
        $('#SecondDiv').animatePanel();
    }
}
//function validateTypePromotion(type_promotion) {
//    if (type_promotion == "AVANZADA") {
//        ["base_price_span", "promotion_price_span", "tableOne"].map(x => HidenOrCheckFull(`#${x}`, false));
//        ["tableTwo", "name_promotion", "base_quantityAdvanced_span", "promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, true));
//        $('#ItemsDrop').empty();
//        $('#text_promotion').html("Descripcion de la oferta avanzada y agregar productos.");
//    } else if (type_promotion == "AVANZADA") {
//        ["base_price_span", "promotion_price_span", "tableOne"].map(x => HidenOrCheckFull(`#${x}`, true));
//        ["tableTwo", "name_promotion", "base_quantityAdvanced_span", "promotion_description_hide", "optionTable"].map(x => HidenOrCheckFull(`#${x}`, false));
//        $('#text_promotion').html("Agregar Productos");
//        $('#ItemsDrop').empty();
//    }
//    clearProduct();
//}

function SearchByStatus(status) {
    $("#replicationStatus").val(`${status}`).trigger("change");
    SearchFunction();
}

$("#ClearInput").click(function () {
    $('#BeginDate').val("");
    $('#EndDate').val("");
    $("#ItemsDrop").val("").trigger("change");
    $("#site").val("").trigger("change");
    $("#typeSelect").val("").trigger("change");
    $("#ItemsDropSelect").val("").trigger("change");
    $("#replicationStatus").val("9").trigger("change");
    SearchFunction();
});

function SaveMessageDelete() {
    swal({
        title: "¿Esta seguro que desea ELIMINAR la oferta?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            SavePromotionDelete();
    });
}

//
// Mensaje de confirmación para guardar
////
//function SaveMessageEdit() {
//    if (listAddProducts.length != 0 || listFinal.length != 0) {
//        if (ValidateFieldsPromotions()) {
//            if (type_promotions_globlal == "AVANZADA") {
//                if (validatePriceTotal()) {
//                    function a() {
//                        swal({
//                            title: "¿Esta seguro que desea editar la oferta avanzada?",
//                            type: "warning",
//                            showCancelButton: true,
//                            confirmButtonColor: "#55dd6b",
//                            confirmButtonText: "Continuar",
//                            cancelButtonText: "Cancelar"
//                        }, function (isConfirm) {
//                            if (isConfirm)
//                                SavePromotionEdit();
//                        });
//                    };
//                    a();
//                }
//            } else {
//                (function b() {
//                    swal({
//                        title: "¿Esta seguro que desea editar la oferta?",
//                        type: "info",
//                        showCancelButton: true,
//                        confirmButtonColor: "#55dd6b",
//                        confirmButtonText: "Continuar",
//                        cancelButtonText: "Cancelar"
//                    }, function (isConfirm) {
//                        if (isConfirm)
//                            SavePromotionEdit();
//                    });
//                })();
//            }
//        }
//    } else {
//        toastr.warning("Guarda al menos un producto.");
//    }
//}

function SavePromotionActivate() {
    StartLoading();
    $.ajax({
        url: "/Promotion/ActionPromotionAtivate/",
        type: "GET",
        data: { "promotion_code": promotionCodeNow, "promotion_type": type_promotions_globlal },
        success: function (response) {
            EndLoading();
            if (response.success) {
                swal({
                    title: 'Activado Correctamente correctamente.',
                    type: 'success'
                });
                SearchFunction();
            }
            else {
                if (response.responseText == "SF") {
                    SessionFalse("Termino Session");
                }
                else if (response.responseText != "") {
                    toastr.error(response.responseText);
                } else {
                    toastr.error("Hubo un error al actualizar la información a eliminado.");
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error("Hubo un error al actualizar la información a eliminado.");
            $('#SecondDiv').addClass("hide");
            $('#MainDiv').removeClass("hide");
            $('#MainDiv').animatePanel();
        }
    });
}

function SavePromotionDelete() {
    StartLoading();
    $.ajax({
        url: "/Promotion/ActionPromotionDeleteType",
        type: "GET",
        data: { "promotion_code": promotionCodeNow, "promotion_type": type_promotions_globlal },
        success: function (response) {
            EndLoading();
            if (response.success) {
                swal({
                    title: 'Eliminado correctamente.',
                    type: 'success'
                });
                $('#SecondDiv').addClass("hide");
                $('#MainDiv').removeClass("hide");
                $('#MainDiv').animatePanel();
            }
            else {
                if (response.responseText == "SF") {
                    SessionFalse("Termino Session");
                }
                else if (response.responseText != "") {
                    toastr.error(response.responseText);
                } else {
                    toastr.error("Hubo un error al actualizar la información a eliminado.");
                }
                $('#SecondDiv').addClass("hide");
                $('#MainDiv').removeClass("hide");
                $('#MainDiv').animatePanel();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error("Hubo un error al actualizar la información a eliminado.");
            $('#SecondDiv').addClass("hide");
            $('#MainDiv').removeClass("hide");
            $('#MainDiv').animatePanel();
        }
    });

}

function SavePromotionEdit() {
    //
    // Envia los productos para actualizar la base de datos
    //
    StartLoading();
    if (ValidateFieldsPromotions()) {
        var url = "";
        var productItems = [];
        var productItemsComun = [];
        var model = [];
        var sitess = [];
        if (type_promotions_globlal == "AVANZADA") {
            if (!ValidateSumProducts(true)) //Validación que la suma termine en .90
                return false;
            url = "/Promotion/ActionCreateAdvPromotionEdit";
            productItemsComun = tableComun.data().toArray()
                .filter(f => f.update_flag == true)
                .map((value, i, array) => { return { part_number_origin: value.part_number_origin, part_number: value.part_number, delete_flag: value.delete_flag } });
            productItems = TableItemsGlobalPromotions.data().toArray()
                .filter(element => element.update_flag == true)
                .map((value, i, array) => { return { part_number: value.part_number, promotion_price: value.total_price_promotion, quantity_total: value.quantity_promotion, common_id: TableItemsGlobalPromotions.data().toArray().findIndex(f => f.part_number == value.part_number && f.delete_flag == false), common_flag: value.comun_flag, presentation_id: 0, delete_flag: value.delete_flag } });
        } else {
            if (!ValidateAllProductsIs90()) //Validacición que termine la todos los productos en .90
                return false;
            url = "/Promotion/ActionCreatePromotionEdit/";
            productItems = TableItemsGlobalPromotions.data().toArray()
                .filter(element => element.update_flag == true)
                .map((value, i, array) => { return { part_number: value.part_number, promotion_price: value.total_price_promotion, quantity_total: value.quantity_promotion, common_id: 0, common_flag: value.comun_flag, presentation_id: value.presentation_id, presentation_flag: value.presentation_flag, delete_flag: value.delete_flag } });
        }
        sitesLDisable = [];
        $.each($("input[name='available']"), function () {
            if ($(this).prop("checked") == false) {
                sitesLDisable += $(this).val() + ",";
            }
        });
        var model = { "promotion_code": promotionCodeNow, "promotion_type": type_promotions_globlal, "promotion_start_date": $('#fechaInicio').val(), "promotion_end_date": $('#fechaFin').val(), "description": $('#promotion_description').val() }
        $.ajax({
            type: "POST",
            url: url,
            data: { "model": model, "items": productItems, "sites": sitesL, "sitesD": sitesLDisable, "itemsCommon": productItemsComun },
            success: function (returndata) {
                if (returndata.success) {
                    EndLoading();
                    swal({
                        title: 'Guardado Correctamente',
                        type: 'success'
                    });
                    ClearAll();
                    $("#bodySites").css({ 'display': "block" });
                    BacktoReturn();
                    SearchFunction();
                }
                else {
                    if (returndata.responseText == "SF") {
                        SessionFalse("Su sesión a terminado.")
                    }
                    BacktoReturn();
                    EndLoading();
                    toastr.error(returndata.responseText);
                }
            },
            error: function () {
                toastr.error('Error inesperado contactar a sistemas');
                EndLoading();
            }
        });
    }

    //if (ValidateFieldsPromotions()) {
    //    if (type_promotions_globlal == "AVANZADA") {
    //        var c = 0;
    //        for (var i = 0; i < listNewPrice.length; i++) {
    //            if (listNewPrice[i].value >= 0) {
    //                listFinal[c].price = listNewPrice[i].value;
    //                c++;
    //            }
    //        }

    //        for (var i = 0; i < listFinal.length; i++) {
    //            listFinal[i].promotion_quantity = listAddProducts[i].promotion_quantity;
    //        }
    //        var texto = "";
    //        var count = 0;
    //        var listF = [];
    //        var cantidad = 0;

    //        listFinal.sort(function (a, b) {
    //            return a.part_number - b.part_number;
    //        }).forEach(function (elemento) {
    //            if (texto != elemento.part_number || texto == "") {
    //                listF.push({ common_id: elemento.common_id, part_number: elemento.part_number, promotion_price: listFinal.filter(elements => elements.part_number == elemento.part_number).reduce((inicial, elements) => { count++; cantidad += parseInt(elements.promotion_quantity); return inicial + parseFloat(elements.price) }, 0), quantity: count, quantity_total: cantidad })
    //                count = 0;
    //                cantidad = 0;
    //            }
    //            texto = elemento.part_number
    //        });

    //        sitesLDisable = [];

    //        $.each($("input[name='available']"), function () {
    //            if ($(this).prop("checked") == false) {
    //                sitesLDisable += $(this).val() + ",";
    //            }
    //        });

    //        var model =
    //        {
    //            "promotion_code": promotionCodeNow,
    //            "promotion_type": type_promotions_globlal,
    //            "promotion_start_date": $('#fechaInicio').val(),
    //            "promotion_end_date": $('#fechaFin').val(),
    //            "promotion_quantity": listFinal.length,
    //            "description": $('#promotion_description').val()
    //        }

    //        sitesLDisable = [];

    //        $.each($("input[name='available']"), function () {
    //            if ($(this).prop("checked") == false) {
    //                sitesLDisable += $(this).val() + ",";
    //            }
    //        });

    //        StartLoading();
    //        $.ajax({
    //            type: "POST",
    //            url: "/Promotion/ActionCreateAdvPromotionEdit",
    //            data: { "model": model, "items": listF, "itemsD": listDeleteProducts, "sites": sitesL, "sitesD": sitesLDisable, "itemsCommon": listProductComun, "itemsCommonD": listDeleteComunProducts },
    //            success: function (returndata) {
    //                EndLoading();
    //                if (returndata.success) {
    //                    setTimeout(function () {
    //                        swal({
    //                            title: 'Guardado Correctamente',
    //                            type: 'success'
    //                        });
    //                    }, 500);
    //                    BacktoReturn();
    //                    ClearAll();
    //                    SearchFunction();
    //                }
    //                else {
    //                    if (returndata.responseText == "SF") {
    //                        SessionFalse("Su sesión a terminado.")
    //                    }
    //                    BacktoReturn();
    //                    toastr.error(returndata.responseText);
    //                }
    //            },
    //            error: function () {
    //                toastr.error('Error inesperado contactar a sistemas. Error por Timeout mas probablemente.');
    //                EndLoading();
    //            }
    //        });
    //    }
    //    else {
    //        sitesLDisable = [];

    //        $.each($("input[name='available']"), function () {
    //            if ($(this).prop("checked") == false) {
    //                sitesLDisable += $(this).val() + ",";
    //            }
    //        });

    //        var model =
    //        {
    //            "promotion_code": promotionCodeNow,
    //            "promotion_type": type_promotions_globlal,
    //            "promotion_start_date": $('#fechaInicio').val(),
    //            "promotion_end_date": $('#fechaFin').val(),
    //        }
    //        StartLoading();
    //        $.ajax({
    //            type: "POST",
    //            url: "/Promotion/ActionCreatePromotionEdit",
    //            data: { "model": model, "items": listFinal, "sites": sitesL, "sitesD": sitesLDisable, "itemsD": listDeleteProducts },
    //            success: function (returndata) {
    //                if (returndata.success) {
    //                    EndLoading();
    //                    ClearAll();
    //                    $("#bodySites").css({ 'display': "block" });
    //                    BacktoReturn();
    //                    SearchFunction();
    //                }
    //                else {
    //                    if (returndata.responseText == "SF") {
    //                        SessionFalse("Su sesión a terminado.")
    //                    }
    //                    BacktoReturn();
    //                    EndLoading();
    //                    toastr.error(returndata.responseText);
    //                }
    //            },
    //            error: function () {
    //                toastr.error('Error inesperado contactar a sistemas');
    //                EndLoading();
    //            }
    //        });
    //    }
    //}
}

function ShowPromotionCommonItem(promotion_code, common, price_promo, quantity_promo, descrip_part_number) {
    $.ajax({
        type: "GET",
        url: "/Promotion/ActionGetPromotionCommon",
        data: { "promotion_code": promotion_code, "common": common, },
        success: function (response) {
            if (response.status) {
                $('#folioOfertaCommon').html(promotion_code);
                $('#descriptionOfertaCommon').html(common);
                $('#descripPartCode').html(descrip_part_number);
                $('#precioOferta').html("$ " + price_promo);
                $('#cantidadOFerta').html(quantity_promo);
                tableCommons.clear();
                tableCommons.rows.add(response.model);
                tableCommons.columns.adjust().draw();
                $('#showProductCommon').modal('show');
            } else {
                SessionFalse("Termino su sesión");
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}

function UpdatTables(option) {
    if (option == "1") {
        AjaXGeneric({
            data: {
                "BeginDate": $('#BeginDateDetail').val(), "EndDate": $("#EndDateDetail").val(), "site_code": '',
                "item": '', "type": '', "AllStores": true, "onlystatus": 9
            },
            type: "GET",
            url: "/Promotion/ActionGetPromotionDetails/",
            fun: function (result) {
                if (result.status) {
                    TableAnimationDetail('#TableDiv', TablePromotionsDetail, result.model)
                }
                else {
                    SessionFalse("Se terminó su sesion.");
                    TableAnimationDetail('#TableDiv', TablePromotionsDetail, [])
                }
                EndLoading();
            },
            error: 'Error inesperado contactar a sistemas.'
        });
    } else {
        AjaXGeneric({
            data: {
                "BeginDate": $('#BeginDate').val(), "EndDate": $("#EndDate").val(), "site_code": $('#site').val(),
                "item": $('#ItemsDropSelect').val(), "type": $('#typeSelect').val(), "AllStores": editVariable, "onlystatus": $('#replicationStatus').val()
            },
            type: "GET",
            url: "/Promotion/ActionGetPromotion/",
            fun: function (result) {
                if (result.status) {
                    TableAnimation('#TableDiv', table, result.model)
                }
                else {
                    SessionFalse("Se terminó su sesion.");
                    TableAnimation('#TableDiv', table, [])
                }
                EndLoading();
            },
            error: 'Error inesperado contactar a sistemas.'
        });
    }
}