﻿//VARIABLES
var checkRfc = false;
var enable = true;
var listAddContacts = [];
var typeSupplier = "Proveedor";

//JQUERY READY
$(document).ready(function () {
    window.setTimeout(function () {
        $(".normalheader").addClass("small-header");
        $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        $("#hbreadcrumb").removeClass("m-t-lg");
    }, 500);
    $("#btnEditar").hide()
    tableContacts.clear().draw()
});
$('#department').select2();
$('#part_iva_purchase_supplier').select2();

//TABLA ---------------------------------------------------------------
var tableContacts = $('#table_contacts').DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    oLanguage:
    {
        "sZeroRecords": "",
        "sEmptyTable": "",
    },
    columns:
        [
            { data: 'name' },
            { data: 'last_name' },
            { data: 'email' },
            { data: 'phone' },
            { data: 'department' },
            { data: null }
        ],
    columnDefs:
        [
            {
                data: null,
                width: "1%",
                defaultContent: '<button class="btn btn-xs btn-danger btn-circle eliminarContacto" id="remove-button" ><i class="fa fa-times"></i></button>',
                targets: [5]
            },
            {
                targets: [1],
                visible: false,
                searchable: false
            }
        ]
});

//FUNCIONES------------------
function supplier_Type() {
    if (typeSupplier == 'Proveedor') {
        $("#supplierFlags").show();
        checksFlagsUncheck();//OCD
    } else {
        $("#supplierFlags").hide();
        checksFlagsUncheck();
    }
}

function checksFlagsUncheck() {
    $("#merchandise_entry").iCheck('uncheck');
    $("#apply_return").iCheck('uncheck');
    $("#truck_foot").iCheck('uncheck');
}

function addContact() {
    if ($("#Name").val().trim() == "") {
        toastr.warning('Ingresa Nombre.');
    }
    else if ($("#Last_Name").val().trim() == "") {
        toastr.warning('Ingresa Apellido.');
    }
    else if ($("#Email").val().trim() == "") {
        toastr.warning('Ingresa correo.');
    }
    else if ($("#Phone").val().trim() == "") {
        toastr.warning('Ingresa un teléfono.');
    }
    else if ($("#department").val() == "0") {
        toastr.warning('Selecciona un Tipo.');
    }
    else {
        $("#btnRegistrar").prop("disabled", false);
        var tipo = "";
        if ($("#department").val() == "R") {
            tipo = "Recepcion";
        }
        else if ($("#department").val() == "V") {
            tipo = "Ventas";
        }
        else if ($("#department").val() == "C") {
            tipo = "Compras";
        }
        else if ($("#department").val() == "CC") {
            tipo = "Contabilidad";
        }
        var Contact =
        {
            name: $("#Name").val(),
            last_name: $("#Last_Name").val(),
            email: $("#Email").val(),
            phone: $("#Phone").val(),
            department: tipo
        };
        listAddContacts.push(Contact);
        clearContact();
    }
}

var form2 = $("#form_contact").validate({
    rules: {},
    messages: {},
    submitHandler: function (form_contact) {
        addContact();
        tableContacts.clear().draw();
        tableContacts.rows.add($(listAddContacts));
        tableContacts.columns.adjust().draw();
        return false;
    }
});

function clearContact() {
    $("#Name").val("");
    $("#Email").val("");
    $("#Phone").val("");
    $("#Last_Name").val("");
    $("#department").prop("selectedIndex", 0).trigger('change');
}

function clearForm() {
    $('#rfc').val("");
    $('#commercial_name').val("");
    $('#business_name').val("");
    $('#supplier_address').val("");
    $('#city').val("");
    $('#zip_code').val("");
    $('#supplier_state').val("");
    $('#country').val("");
    $('#part_iva_purchase_supplier').val('').trigger('change');
    $('#type').val('').trigger('change');
    $('#department').val($("#department :first").val()).trigger('change');
    checksFlagsUncheck();
}

$('#table_contacts tbody').on('click', '#remove-button', function () {
    if (!enable) {
        return 0;
    }
    var index = tableContacts.row($(this).parents('tr')).index();
    listAddContacts.splice(index, 1);
    tableContacts.clear().draw();
    tableContacts.rows.add($(listAddContacts));
    tableContacts.columns.adjust().draw();
    if (listAddContacts.length == 0) {
        $("#btnRegistrar").prop("disabled", true);
    } else {
        validateRequired();
    }
});

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option  value="">Selecciona un Tipo</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Selecciona un Tipo</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

$('#btnRegistrar').on('click', function (e) {
    var rfc = $('#rfc').val();
    var trimRfc = rfc.trim();
    if ($("#business_name").val().trim() == "") {
        toastr.warning("Ingrese Razon Social");
        $("#business_name").focus();
        $("#business_name").val("");
    }
    else if ($("#commercial_name").val().trim() == "") {
        toastr.warning("Ingrese Nombre comercial.");
        $("#commercial_name").focus();
        $("#commercial_name").val("");
    }
    else if ($("#part_iva_purchase_supplier").val() == "") {
        toastr.warning("Ingrese un IVA de compra.");
        $("#part_iva_purchase_supplier").focus();
    }
    else if (listAddContacts.length == 0) {
        toastr.warning("Ingrese al menos 1 contacto");
        $('#Name').focus();
    }
    else if ($("#zip_code").val().trim() == "") {
        toastr.warning("Agregue un Código Postal");
        $("#zip_code").focus();
    }
    else if ($("#supplier_address").val().trim() == "") {
        toastr.warning("Agregue una dirección");
        $("#supplier_address").focus();
    }
    else if ($("#city").val().trim() == "") {
        toastr.warning("Agregue una Ciudad");
        $("#city").focus();
    }
    else if ($("#supplier_state").val().trim() == "") {
        toastr.warning("Agregue un Estado");
        $("#supplier_state").focus();
    }
    else if ($("#country").val().trim() == "") {
        toastr.warning("Agregue una País");
        $("#country").focus();
    }
    else if ($("#business_name").val().trim().length < 4) {
        toastr.warning("Ingrese correcto una razón social.");
        $("#business_name").focus();
    }
    else if ($("#commercial_name").val().trim().length < 4) {
        toastr.warning("Ingrese correcto un nombre comercial.");
        $("#commercial_name").focus();
    } else {
        var model =
        {
            "rfc": trimRfc,
            "commercial_name": $('#commercial_name').val(),
            "business_name": $('#business_name').val(),
            "supplier_address": $('#supplier_address').val(),
            "city": $('#city').val(),
            "zip_code": $('#zip_code').val(),
            "supplier_state": $('#supplier_state').val(),
            "part_iva_purchase_supplier": $('#part_iva_purchase_supplier').val(),
            "supplier_type": typeSupplier,
            "country": $('#country').val(),
            "truck_foot": ($("#truck_foot").is(':checked')) ? 1 : 0,
            "merchandise_entry": ($("#merchandise_entry").is(':checked')) ? 1 : 0,
            "apply_return": ($("#apply_return").is(':checked')) ? 1 : 0,
        }
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/Supplier/ActionAddSupplier",
            data: { "model": model, "contacts": listAddContacts },
            success: function (response) {
                EndLoading();
                if (response.success) {
                    toastr.success(response.name + ' agregado Correctamente!');
                    clearForm();
                    validateRequired()
                    listAddContacts = []
                    $('#table_contacts').DataTable().clear().draw();
                }
                else {
                    if (response.message == "Termino tu sesion.") {
                        SessionFalse(response.message)
                    }
                    else {
                        toastr.error(response.message);
                    }
                }
                validateRequired();
            },
            error: function () {
                EndLoading();
                toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
            }
        });
    }
});

$('#btnEditar').on('click', function (e) {
    var rfc = $('#rfc').val();
    var trimRfc = rfc.trim();
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Supplier/ActionEditSupplierType",
        data: { "rfc": trimRfc, "type": typeSupplier, "contacts": listAddContacts },
        success: function (response) {
            EndLoading();
            if (response.success) {
                toastr.success(response.name + ' agregado Correctamente!');
                clearForm();
                listAddContacts = []
                $('#table_contacts').DataTable().clear().draw();
            }
            else {
                if (response.message == "Termino tu sesion.") {
                    SessionFalse(response.message)
                }
                else {
                    if (response.message == "Guardado Correctamente") {
                        toastr.success(response.message);
                        listAddContacts = []
                        enableForm();
                    } else
                        toastr.error(response.message);
                }
            }
            validateRequired();
        },
        error: function () {
            EndLoading();
            toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
        }
    });
});

function validateLetras() {
    var city = $('#city').val().toString();
    var state = $('#supplier_state').val().toString();
    var country = $('#country').val().toString();
    var address = $('#supplier_address').val().toString();
    if (city == "") { }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(city)) {
        toastr.error("Debe contener solo Letras.");
        $('#city').focus();
        $('#city').val("");
    }
    else if (state == "") { }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(state)) {
        toastr.error("Debe contener solo Letras.");
        $('#supplier_state').focus();
        $('#supplier_state').val("");
    }
    else if (country == "") { }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(country)) {
        toastr.error("Debe contener solo Letras.");
        $('#country').focus();
        $('#country').val("");
    }
}

function validateName() {
    var nombre = $('#Name').val().trim();
    if (nombre == "") {
    }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(nombre)) {
        toastr.error("Debe contener solo Letras.");
        $('#Name').focus();
        $('#Name').val("");
    }
}

function validateLastName() {
    var apellido = $('#Last_Name').val().trim();
    if (apellido == "") {
    }
    else if (!/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(apellido)) {
        toastr.error("Debe contener solo Letras.");
        $('#Last_Name').focus();
        $('#Last_Name').val("");
    }
}

function validatePhone() {
    var phone = $('#Phone').val().toString();
    if (!/^([0-9])*$/.test(phone)) {
        toastr.error("Debe contener solo Numeros.");
        $('#Phone').focus();
        $('#Phone').val("");
    }
}

function validateZIP() {
    var data = $('#zip_code').val().toString().length;
    var code = $('#zip_code').val().toString();
    if (!/^([0-9])*$/.test(code)) {
        toastr.error("Debe contener solo Numeros.");
        $('#zip_code').focus();
        $('#zip_code').val("");
    }
    else if (data == 5) {
        var zipCode = $('#zip_code').val();
        axios.get('https://api-codigos-postales.herokuapp.com/v2/codigo_postal/' + zipCode)
            .then(function (response) {
                var json = response.data;
                if (json.municipio != null) {
                    clearAddress();
                    $('#city').val(json.municipio);
                    $('#supplier_state').val(json.estado);
                    $('#country').val("Mexico");
                }
                else {
                    clearAddress();
                }
            })
            .catch(function (error) {
            });
    }
}

function clearAddress() {
    $('#city').val(" ");
    $('#supplier_state').val(" ");
    $('#country').val(" ");
}

function validateRequired() {
    var flag = 0;
    var button = document.getElementById("btnRegistrar");
    var requireds = document.getElementsByClassName("required");
    for (var i = 0; i < requireds.length; i++) {
        var r = requireds[i];
        if (r.value != "") {
            flag++;
        }
    }
    if (requireds.length == flag) {
        $("#btnRegistrar").attr("disabled", false);
        return true;
    }
    else {
        $("#btnRegistrar").attr("disabled", true);
    }
}

function validateRfc() {
    var rfc = $('#rfc').val();
    if (rfc == "" || rfc == null) {
        $('#rfc').focus();
    }
    else {
        const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
        var r = rfc.trim();
        if (r.includes('-') && r.length == 10) {
            axios.get('/Supplier/ActionRfcExists?rfc=' + r + '&type=' + typeSupplier)
                .then(function (response) {
                    var json = response.data;
                    if (json.success == true) {
                        checkRfc = true;
                        toastr.error(json.message);
                        $('#rfc').focus();
                        $('#rfc').val("");
                    }
                    else {
                        if (response.message == "Termino tu sesion.") {
                            SessionFalse(response.message);
                        }
                        else {
                            checkRfc = false;
                        }
                    }
                })
                .catch(function (error) {
                });
        }
        else if (r.length == 12 || r.length == 13) {
            var validado = r.toUpperCase().match(re);
            if (validado != null) {
                axios.get('/Supplier/ActionRfcExists?rfc=' + r + '&type=' + typeSupplier)
                    .then(function (response) {
                        var json = response.data;
                        supplier_Type();
                        if (json.success == true) {
                            checkRfc = true;
                            //entra si ya existe como acreedor y proveedor
                            if (json.edit == false) {
                                toastr.error(json.message);
                                $('#rfc').focus();
                                $('#rfc').val("");
                                validateRequired()
                            }
                            else {//entra si existe pero puede registrar acreedor o proveedor                                                                
                                if (json.supplier != null) {
                                    disableForm(json);
                                } else {
                                    enableForm(1);
                                }
                                validateRequired();
                            }
                        }//entra si se puede editar
                        else {
                            if (response.message == "Termino tu sesion.") {
                                SessionFalse(response.message);
                            }
                            else {
                                clearForm();
                                enableForm(1);
                                checkRfc = false;
                            }
                        }
                    })
                    .catch(function (error) {
                    });
            }
            else {
                toastr.error("Formato RFC no valido");
                $('#rfc').focus();
                $('#rfc').val("");
            }
        }
        else {
            toastr.error("Longitud Invalida.");
            $('#rfc').focus();
            $('#rfc').val("");
        }
    }
}

//Desactivar Form
function disableForm(json) {
    $('#business_name').val(json.supplier.business_name).attr("disabled", true);
    $('#commercial_name').val(json.supplier.commercial_name).attr("disabled", true);
    $('#zip_code').val(json.supplier.zip_code).attr("disabled", true);
    $('#supplier_address').val(json.supplier.supplier_address).attr("disabled", true);
    $('#city').val(json.supplier.city).attr("disabled", true);
    $('#supplier_state').val(json.supplier.supplier_state).attr("disabled", true);
    $('#country').val(json.supplier.country).attr("disabled", true);
    $("#part_iva_purchase_supplier").val(json.supplier.part_iva_purchase_supplier).trigger('change').attr("disabled", true);
    tableContacts.clear();
    tableContacts.rows.add(json.contacts);
    tableContacts.columns.adjust().draw();
    listAddContacts = json.contacts
    $('#select2-chosen-1 option[1]');
    checkFlags(json.supplier.truck_foot, json.supplier.merchandise_entry, json.apply_return, 'true');
    supplier_Type();
}

//Activar Form
function enableForm(val) {
    $('#business_name').attr("disabled", false).val('');
    $('#commercial_name').attr("disabled", false).val('');
    $('#zip_code').attr("disabled", false).val('');
    $('#supplier_address').attr("disabled", false).val('');
    $('#city').attr("disabled", false).val('');
    $('#supplier_state').attr("disabled", false).val('');
    $('#country').attr("disabled", false).val('');
    $("#part_iva_purchase_supplier").val('').trigger('change').attr("disabled", false);
    listAddContacts = []
    tableContacts.clear()
    tableContacts.columns.adjust().draw();
    $('#type').select2("val", null);
    $("#phone_button").attr("disabled", false);
    checksFlagsUncheck();
    checkFlags(0, 0, 0, 'enable');
    if (val != 1)
        $('#rfc').val('')
}

//Revisar Checks truck, merchandise, apply
function checkFlags(truck, merchandise, apply, status) {
    if (truck == 1)
        $("#truck_foot").iCheck('check').iCheck(status);
    else
        $("#truck_foot").iCheck('uncheck').iCheck(status);
    if (merchandise == 1)
        $("#merchandise_entry").iCheck('check').iCheck(status);
    else
        $("#merchandise_entry").iCheck('uncheck').iCheck(status);
    if (apply == 1)
        $("#apply_return").iCheck('check').iCheck(status);
    else
        $("#apply_return").iCheck('uncheck').iCheck(status);
}

function validateEmail() {
    var email = $('#Email').val(), substring = ".";
    if ($('#Email').val() == null || $('#Email').val() == "") { }
    else {
        if (email.includes(substring)) { }
        else {
            toastr.warning("Email no valido");
            $('#Email').focus();
            $('#Email').val("");
        }
    }
}

//////////////////////CAMBIAR DE TABS ACREEDOR Y PROVEEDOR
function ChangeTab(type) {
    clearForm();
    enableForm();
    checksFlagsUncheck();
    typeSupplier = type;
    if (type == "Acreedor") {
        $("#titleInformation").html("Información del Acreedor");
        $("#supplierFlags").hide();
    } else {
        $("#titleInformation").html("Información del Proveedor");
        $("#supplierFlags").show();
    }
}