﻿
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});
var list;
$("#site").select2();
var ip_address = "";
var site_code = "";
const formatMoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    //searchTransfers();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,
}).on("changeDate", function (e) {
    //searchTransfers();
});

var table = $('#tableTransfers').DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningun dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        {
            extend: 'csv', text: 'Excel', title: 'Reporte Conciliacion PRT - MES', filename: function () {
                return 'Reporte Conciliacion PRT - MES ' + moment(new Date().getTime()).format("DD-MM-YYYY hh-mm");
            }, className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4]
            }
        },
        {
            extend: 'pdf', text: 'PDF', title: 'Reporte Conciliacion PRT - MES', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4]
            },
        },
        {
            extend: 'print', text: 'Imprimir', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4]
            }
        },
    ],
    createdRow: function (row, data, dataIndex) {
        if (data.checkERP == false) {
            $(row).css('background-color', '#F6FA7A');//yellow
            $(row).css('color', 'Gray');
        }
    },
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
                width: "1%"
            },
            { data: 'site_name' },
            { data: 'totalPRT' },
            { data: 'totalStore' },
            { data: 'totalDifference' },
            { data: 'checkERP' }
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5],
            width: "1%"
        }, {
            targets: [2, 3, 4],
            render: function (data, type, full, meta) {
                return formatMoney(data)(2);
            },
            width: "1%"
        }]
});

table.column(5).visible(false);

var detailRows = [];
$('#tableTransfers tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        ip_address = row.data().ip_address;
        site_code = row.data().site_code;
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

function ShowModal(do_no, purchase_no, date_erp, date_prt, status) {
    var date;
    if (date_prt != "") {
        date = date_prt;
    } else {
        date = date_erp;
    }
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/PRT/ActionGetReportPRT/",
        data: { "purchase_no": purchase_no, "do_no": do_no, "site_code": site_code, "ip_address": ip_address, "date": date, "status": status },
        success: function (json) {
            EndLoading();
            if (json.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(json.Data) + "'></iframe>")
                $("#modalReference").modal('show');
            } else {
                if (json.data == "SF") {
                    SessionFalse("Terminó tu sesión");
                }
            }
        },
        error: function (json) {
            EndLoading();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});
TrButton = element => `<button class="btn btn-xs btn-outline btn-info"
                        onclick="ShowModal('${element.do_no}','${element.purchase_no}','${element.date_erp}','${element.date_prt}','${element.status}')">
                        <i class="fa fa-eye"></i> Ver</button>`
FullTd = element => Option => `<td>${Option == 1 ? fomartmoney(element)(2) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
FullTh = element => `<th>${element}</th>`
FullTable = element => array => array.map(title => FullTd(element[title.title])(title.option)).toString();
FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`

//Tabla detalles
function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/PRT/ActionGetTransferDetailERPBOX/",
        data: { "dateInitial": $('#ValidDate1').val(), "dateFinish": $('#ValidDate2').val(), "ip": d.ip_address, "site": d.site_code },
        type: "POST",
        success: function (json) {
            if (json.success) {
                //$.each(json.data, function (index, value) {
                //    detailItems += "<tr><td>" + value.do_no + "</td><td>" + value.purchase_no + "</td><td>" + value.date_prt + "</td><td>" + value.date_erp + "</td><td>" + value.purchase_total_prt + "</td><td>" + value.purchase_total_erp + "</td><td>" + value.purchase_comments + "</td><td>" + value.status + "</td>" + "</td>";
                //});
                //if (json.data.length != 0) {
                //    tabledetail.html('<div class="table-responsive"><table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>#Transferencia</th><th>#Precompra</th><th>Fecha PRT</th><th>Fecha ERP</th><th>$Total PRT</th><th>$Total ERP</th><th>Comentarios</th><th>Status</th></tr></thead><tbody>' + detailItems + '</tbody></table></div>').removeClass('loading');
                //    reloadStyleTable();
                //}
                //tabledetail.html('<div class="table-responsive"><table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>#Transferencia</th><th>#Precompra</th><th>Fecha PRT</th><th>Fecha ERP</th><th>$Total PRT</th><th>$Total ERP</th><th>Comentarios</th><th>Status</th></tr></thead><tbody>' + detailItems + '</tbody></table></div>').removeClass('loading');
                //reloadStyleTable();

                detailItems = json.data.map(value => ` <tr>${FullTable(value)([
                    { title: "do_no", option: 0 },
                    { title: "purchase_no", option: 0 },
                    { title: "date_prt", option: 0 },
                    { title: "date_erp", option: 0 },
                    { title: "purchase_total_prt", option: 0 },
                    { title: "purchase_total_erp", option: 0 },
                    { title: "status", option: 0 },
                    { title: "purchase_comments", option: 0 },])}<td>${TrButton(value)}</td></tr>`).join().replace(/>,</g, '><').replace(/>, </g, '><')
                header = FullTableHeader(["#Transferencia", "#Precompra", "Fecha PRT", "Fecha ERP", "$Total PRT", "Total ERP", "Estatus", "Comentarios", "Reporte"]).replace(/,/g, '')
                var Html = (`<div class="table-responsive"><table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead>
                                       ${header}
                                        </thead><tbody>${detailItems.length > 0 ? detailItems : "<td colspan='8' style='text-align: center;'>No cuenta con productos</td>"}</tbody></table></div>`)
                tabledetail.html(Html).removeClass('loading');
                $("#tabledetail").append('<tfoot><th></th><th colspan = "2">Totales</th><th></th><th></th><th></th><th></th><th></th><th></th></tfoot>');
                reloadStyleTable();
            } else if (json.data == "SF") {
                SessionFalse("Terminó tu sesión");
            }


            $(".btn-success").on("click", function () {
                Aprobar($(this).attr("folio"));
            });

            $(".btn-danger").on("click", function () {
                Cancelar($(this).attr("folio"));
            });
        }
    });
    return tabledetail;
}

//Recargar tabla
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningun dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
        createdRow: function (row, data, dataIndex) {
            if (data[6] == 'Pendiente') {
                $(row).css('background-color', '#F6FA7A');//yellow
                $(row).css('color', 'Gray');
            }
            if (data[6] == 'Cancelado') {
                $(row).css('background-color', '#FF937F');//red
                $(row).css('color', 'White');
            }
        },
        buttons: [
            {
                extend: 'csv', text: 'Excel', title: 'Detalles Conciliacion PRT - MES' + moment(new Date().getTime()).format("DD-MM-YYYY hh-mm"), className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                },footer:true
            },
            {
                extend: 'pdf', text: 'PDF', title: 'Detalles Conciliacion PRT - MES', className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                },
            },
            {
                extend: 'print', text: 'Imprimir', className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                }
            },
        ],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
            // converting to interger to find total
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };
            for (var i = 4; i < 6; i++) {
                var monTotal = api
                    .column(i)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                $(api.column(i).footer()).html(`<span> ${formatMoney(monTotal)(2)}</span >`);
            }

            //for (var i = 10; i < 13; i++) {
            //    var monTotal = api
            //        .column(i)
            //        .data()
            //        .reduce(function (a, b) {
            //            return intVal(a) + intVal(b);
            //        }, 0);
            //    $(api.column(i).footer()).html(`<span> ${fomartmoney(monTotal)(2)}</span >`);
            //}

        },
        columnDefs:
            [{
                targets: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                width: "1%"
            }, {
                targets: [4],
                render: function (data, type, full, meta) {
                    return formatMoney(data)(2);
                },
                width: "1%"
            }, {
                targets: [5],
                render: function (data, type, full, meta) {
                    return formatMoney(data)(2);
                },
                width: "1%"
            }, {
                targets: [1, 6,7],
                render: function (data, type, full, meta) {
                    if (data == null)
                        return "";
                    else
                        return data;
                },
                width: "1%"
            },
                //{
                //    targets: [7],
                //    render: function (data, type, full, meta) {
                //        if (data.inventory_status == "Recibido")
                //            return `<button class="btn btn-xs btn-outline btn-info" onclick="PrintTransfer('${data.id_inventory_header}')"><i class="fa fa-close"></i> Cerrar</button>`;
                //        else if (data.inventory_status == 2 || data.inventory_status == 3) {
                //            var endDate = moment(data.inventory_date_end).format('DD/MM/YYYY');
                //            return `<button class="btn btn-xs btn-outline btn-danger" onclick="Finish('${data.id_inventory_header}')"><i class="fa fa-close"></i> Terminar</button>    <button class="btn btn-xs btn-outline btn-info" onclick="print('${data.id_inventory_header}','` + endDate + `')"><i class="fa fa-eye"></i> Mostrar</button>`;
                //        }
                //    }
                //}
            ]
    });
}

function searchTransfers() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/PRT/ActionGetTotalsTransfersByDate/",
                data: { "dateInitial": $('#ValidDate1').val(), "dateFinish": $('#ValidDate2').val(), "site": $('#site').val() },
                success: function (json) {
                    EndLoading();
                    if (json.success) {
                        list = json.data;
                        if (json.data.length == 0) {
                            toastr.warning('No se encontraron transferencias.');
                            table.clear();
                            table.columns.adjust().draw();
                            $('#tableTransfers').animatePanel();
                        }
                        else {
                            table.clear();
                            table.rows.add($(json.data));
                            table.columns.adjust().draw();
                            $('#tableTransfers').animatePanel();
                        }

                    } else {
                        if (json.data == "SF") {
                            SessionFalse("Terminó tu sesión");
                        }
                    }
                },
                error: function (json) {
                    EndLoading();
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}
function PrintReport(reference) {
    StartLoading();
    $.ajax({
        url: "/TransferHeader/ActionTransferReport",
        type: "POST",
        data: { "document": reference },
        success: function (response) {
            EndLoading();
            if (response.success == true) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.Data) + "'></iframe>")
                $("#modalReference").modal('show');
            }
            else {
                if (response.Data == "Termino tu sesión.") {
                    SessionFalse(response.Data);
                }
                else {
                    toastr.error(response.Data);
                }
            }
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}