﻿window.setTimeout(function () { $("#fa").click(); }, 1000);

var weightValue;

//Función para validar decimales
const isDecimal = decimal => number => {
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/^(\d*\.?\d*)$/))
        return true;
    else
        return false;
});

$('input').iCheck({ checkboxClass: 'icheckbox_square-green', });

$("#combodrop").select2({
    matcher: function (term, text, option) {
        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
    }
});


$("#itemsCombo").select2({
    minimumInputLength: 2,
    formatInputTooShort: function () {
        return "Busca un producto";
    },
    matcher: function (term, text, option) {
        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
    }
});

function animationPanel(s) {
    $(s).click();
}

var Products = $('#Items').DataTable({
    "paging": false,
    "ordering": false,
    "searching": false,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [0],
            visible: false
        },
        {
            targets: [10],
            data: null,
            visible: false
        },
        {
            width: "1%",
            targets: [3],
            render: function (data, type, full, meta) {
                return `<div class='input-group'><span class='input-group-addon'><span class='fa fa-cubes'></span></span><input placeholder='Cantidad' type='text' onchange='updateRowIva(${meta.row});' value=${data} class='form-control Numeric'></div>`
            }
        },
        {
            width: "1%",
            targets: [8],
            render: function (data, type, full, meta) {
                return `<div class='input-group'><span class='input-group-addon'><span class='fa fa-dollar'></span></span><input placeholder='Precio' type='text' onchange='updateRowIva(${meta.row});' value=${data} class='form-control Numeric'></div>`
            }
        },
        {
            width: "1%",
            render: function (data, type, row) {
                if (data)
                    return "<input class='che' type='checkbox' checked>"
                else
                    return "<input class='che' type='checkbox'>"
            },
            targets: [9]
        }
    ],
    columns: [
        { data: 'part_number_combo', width: "1%", },
        { data: 'part_number', width: "1%", },
        { data: 'part_description_combo', width: "20%", },
        { data: 'quantity', width: "10%", },
        { data: 'ivaDescription', width: "1%", },
        { data: 'iva', width: "1%", },
        { data: 'margen_part_number', width: "5%", },
        { data: 'base_cost', width: "1%", },
        { data: 'total_price_sale', width: "10%", },
        { data: 'active_status', width: "1%", className: "text-center" },
        { data: 'statusInsert' },
        { data: 'weightValue', visible: false }
    ]
});

var ValidateF = $("#formAdd").validate({
    ignore: [],
    rules: {
        itemsCombo: {
            required: true
        },
        quantity: {
            required: true,
            number: true,
            min: 0.1,
            max: 999999
        },
        total_price_sale: {
            required: true,
            number: true,
            min: 1,
            max: 999999
        }
    },
    messages: {
        itemsCombo: {
            required: "El Campo no puede ser nulo"
        },
        quantity: {
            required: "El Campo no puede ser nulo",
            min: "Ingrese una cantidad razonable.",
            max: "Ingrese una cantidad razonable."
        },
        total_price_sale: {
            required: "El Campo no puede ser nulo",
            min: "Ingrese una cantidad razonable.",
            max: "Ingrese una cantidad razonable."
        }
    }
});

var IVACOMBO = 0;
var BaseCostComboInfo = 0;

function ItemViewModel() {
    var self = this;

    self.PartNumberCombo = ko.observable();
    self.PartNumber = ko.observable();
    self.PartDescriptionCombo = ko.observable();
    self.Quantity = ko.observable();
    self.IvaP = ko.observable();
    self.Iva = ko.observable();
    self.TotalPrice = ko.observable();
    self.ActiveStatus = ko.observable();
    self.BaseCost = ko.observable();

    /// <summary>
    /// Consulta en la base de datos si el producto seleccionado
    /// por el usuario se encuentra en existencia
    /// </summary>
    self.GetItemsCombo = function () {
        var itemId = $("#combodrop").val();
        if (itemId != '') {
            $("#itemQuantity").val(null);
            $("#itemPrice").val(null);
            
            $.ajax({
                url: "/ItemCombo/ActionGetProductCombo",
                type: "GET",
                data: { "PartNumberCombo": itemId },
                success: function (data) {
                    if (data.success == true) {
                        var TotalCombo = 0;
                        var Iva = 0;
                        var IvaGrid = 0;
                        IVACOMBO = 0;
                        data.DetailList.forEach(function (e) {
                            TotalCombo = e.total_price_sale + TotalCombo;
                            IvaGrid = e.iva + IvaGrid;
                            if (typeof e === "object") {
                                e["insert"] = false
                            }
                            $(".btn-danger").prop("disabled", false);
                            $(".btn-success").prop("disabled", false);
                        });
                        $("#ComboBaseCost").val(data.JsonBaseCost > 0 ? data.JsonBaseCost : 1);
                        Iva = CalculateIva(TotalCombo, data.JsonIVA);
                        self.Iva(Iva);//se asigna iva a producto del grid
                        IVACOMBO = data.JsonIVA;
                        BaseCostComboInfo = data.JsonBaseCost;
                        IvaComboTotal = round(Iva + IvaGrid);
                        Iva = CalculateIva(TotalCombo, IVACOMBO);
                        IvaComboTotal = round(IvaComboTotal + Iva);

                        var subTotal = round(TotalCombo - IvaComboTotal);
                        $("#comboTotalLabel").html('$' + TotalCombo);
                        $("#comboIvaLabel").html('$' + IvaComboTotal);
                        $("#comboSubtotalLabel").html('$' + subTotal);
                        $("#combototalProductsLabel").html(data.DetailList.length);

                        Products.clear().draw();
                        Products.rows.add(data.DetailList);
                        Products.columns.adjust().draw();
                        self.PartNumberCombo($("#combodrop").val());
                        UpdateInfo();
                        ChangeMargeCombo();
                        $('input').iCheck({ checkboxClass: 'icheckbox_square-green', }).on('ifClicked', function () {
                            $(this).trigger("change");
                            setTimeout(function () { UpdateInfo(); }, 100);
                        });
                        $('#comboCodeLabel').html($('#combodrop').val());
                        $('#comboDescriptionLabel').html($('#combodrop').select2('data').text);
                        $("#collapseExample").show("show");
                    }
                    else {
                        if (data.responseText === 'Termino tu sesión.') {
                            SessionFalse(data.responseText);
                        }
                        else {
                            Products.clear().draw();
                            $("#collapseExample").hide();
                        }
                    }
                },
                error: function () {
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
    }

    /// <summary>
    /// Consulta en la base de datos si el producto seleccionado
    /// por el usuario se encuentra en existencia
    /// </summary>
    self.GetItem = function () {
        if ($('#itemsCombo').select2('data').text != "Seleccione una opción") {
            $('#comboCodeLabel').html($('#combodrop').val());
            $('#comboDescriptionLabel').html($('#combodrop').select2('data').text);
            $("#collapseExample").show("show");
            var itemId = $("#itemsCombo").val();
            $.ajax({
                url: "/ItemCombo/ActionGetProduct",
                type: "GET",
                data: { "PartNumber": itemId },
                success: function (data) {
                    if (data.success) {
                        self.PartNumber(data.ProductInfo.part_number);
                        self.PartDescriptionCombo(data.ProductInfo.part_description);
                        self.IvaP(data.ProductInfo.part_iva);
                        self.Iva(data.JsonIVA);
                        self.BaseCost(data.JsonBaseCost.base_price);
                        weightValue = data.ProductInfo.weight_flag;
                        $("#BasePriceItemSelect").html('$' + round(data.JsonBaseCost.base_price));
                        $('#itemQuantity').val(null);
                        $('#itemPrice').val(null);
                    }
                    else {
                        toastr.warning("Este producto no se encuentra en existencia actualmente.");
                    }
                },
                error: function () {
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
    }

    /// <summary>
    /// Agrega el producto seleccionado por el usuario a la vista(datatable) y al arreglo de productos
    /// Valida que el producto seleccionado por el usuario no sea repetido
    /// </summary>
    self.AddProductToTable = function () {
        if (ValidateF.element("#itemsCombo") && ValidateF.element("#itemQuantity") && ValidateF.element("#itemPrice")) {
            var product = [];
            var band = false;
            self.Quantity($("#itemQuantity").val());
            self.TotalPrice(parseFloat($("#itemPrice").val()));
            var TotalCombo = 0;
            var Iva = self.Iva();
            var IvaGrid = 0;
            var margen = 0;
            if (Products.data().count() > 0) {
                $(Products.data()).each(function (index, elem) {
                    if (elem.part_number == self.PartNumber()) {
                        band = true;
                        $("#itemsCombo").val('').trigger('change');
                        $("#itemsCombo").select2("open");
                        toastr.warning("Producto ya seleccionado: " + elem.part_description_combo);
                        return;
                    }
                    else {
                        TotalCombo = elem.total_price_sale + TotalCombo;
                        IvaGrid = elem.iva + IvaGrid;
                    }
                });
                if (!band) {
                    Iva = CalculateIva(self.TotalPrice(), Iva);
                    self.Iva(Iva);// IVA FINAL
                    IvaComboTotal = Iva + IvaGrid;
                    TotalCombo = parseFloat(TotalCombo) + parseFloat(self.TotalPrice());
                    Iva = CalculateIva(round(TotalCombo), IVACOMBO);
                    IvaComboTotal = round(parseFloat(Iva) + parseFloat(IvaComboTotal));
                    product.push({ "part_number_combo": self.PartNumberCombo(), "part_number": self.PartNumber(), "part_description_combo": self.PartDescriptionCombo(), "quantity": self.Quantity(), "ivaDescription": self.IvaP(), "iva": self.Iva(), "total_price_sale": self.TotalPrice(), "active_status": true, "statusInsert": true, "base_cost": self.BaseCost(), "margen_part_number": ChangeMarginPrice(self.TotalPrice(), self.BaseCost()), "weightValue": weightValue });
                    Products.rows.add($(product));
                    Products.columns.adjust().draw();
                    $("#itemsCombo").val('').trigger('change');
                    $("#itemsCombo").select2("open");
                    $(".btn-success").prop("disabled", false);
                    UpdateInfo();
                }
            }
            else {
                Iva = CalculateIva(parseFloat($("#itemPrice").val()), Iva);
                self.Iva(Iva);// IVA FINAL
                IvaComboTotal = Iva;
                Iva = CalculateIva(parseFloat($("#itemPrice").val()), IVACOMBO);
                IvaComboTotal = round(parseFloat(Iva) + parseFloat(IvaComboTotal));
                product.push({ "part_number_combo": self.PartNumberCombo(), "part_number": self.PartNumber(), "part_description_combo": self.PartDescriptionCombo(), "quantity": self.Quantity(), "ivaDescription": self.IvaP(), "iva": self.Iva(), "total_price_sale": self.TotalPrice(), "active_status": true, "statusInsert": true, "base_cost": self.BaseCost(), "margen_part_number": ChangeMarginPrice(self.TotalPrice(), self.BaseCost()), "weightValue": weightValue });
                Products.rows.add($(product));
                Products.columns.adjust().draw();
                $(".btn-success").prop("disabled", false);
                UpdateInfo();
            }
            $('#itemsCombo').val('').trigger('change');
            $("#itemQuantity").val(null);
            $("#itemPrice").val(null);
            $(".btn-danger").prop("disabled", false);
            $(".btn-success").prop("disabled", false);
            $('input').iCheck({ checkboxClass: 'icheckbox_square-green', }).on('ifClicked', function () {
                $(this).trigger("change");
                setTimeout(function () { UpdateInfo(); }, 100);
            });
        }
    }

    /// <summary>
    /// Limpia las variables y desabilita los botones Cancelar y Guardar
    /// </summary>
    self.Clear = function () {
        self.PartNumberCombo(null);
        self.PartNumber(null);
        self.PartDescriptionCombo(null);
        self.Quantity(null);
        self.IvaP(null);
        self.Iva(null);
        self.TotalPrice(null);
        self.ActiveStatus(null);
        Products.clear().draw();
        Products.columns.adjust().draw();
        $(".btn-danger").prop("disabled", true);
        $(".btn-success").prop("disabled", true);
        $("#combodrop").val('').trigger('change');
        $('#itemsCombo').val('').trigger('change');
        $("#collapseExample").hide("hide");
        $("#itemQuantity").val(null);
        $("#itemPrice").val(null);
        $("#BasePriceItemSelect").html('$' + 0);   
        $("#comboMargenLabel").html('$' + 0);
        IVACOMBO = 0;
        BaseCostComboInfo = 0;
    }

    /// <summary>
    /// Confirmación antes de grabar la solicitud de ajuste de inventario
    /// </summary>
    self.Confirm = function () {
        if (Products.data().count() > 0) {
            var Total = $("#comboTotalLabel").text().replace('$', '');
            if (!isNaN($('#ComboBaseCost').val())) {
                if ($('#ComboBaseCost').val() != "") {
                    if (parseFloat($('#ComboBaseCost').val()) > 0 && parseFloat($('#ComboBaseCost').val()) < 99999999) {
                        if ($('#ComboBaseCost').val() > 0 && Total > 0) {
                            swal({
                                title: "¿Esta seguro?",
                                text: "¿Desea guardar los cambios?",
                                type: "warning",
                                closeOnConfirm: true,
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Continuar",
                                cancelButtonText: "Cancelar"
                            }, function (isConfirm) {
                                setTimeout(function () {
                                    if (isConfirm) {
                                        StartLoading();
                                        self.Save();
                                    }
                                }, 400);
                            });
                        }
                        else {
                            toastr.warning("Precio base y total deben ser mayores a cero");
                            $('#ComboBaseCost').focus();
                        }
                    } else {
                        toastr.warning("Coloque una cantidad correcta en el costo base.");
                        $('#ComboBaseCost').focus();
                    }
                } else {
                    toastr.warning("Coloque una cantidad correcta en el costo base.");
                    $('#ComboBaseCost').focus();
                }
            } else {
                toastr.warning("Coloque una cantidad correcta en el costo base.");
                $('#ComboBaseCost').focus();
            }
        }
        else {
            toastr.warning("Favor de agregar por lo menos un producto al combo.");
        }
    }

    /// <summary>
    /// Valida que la captura este completa
    /// Graba en base de datos la solicitud de ajuste de inventario
    /// </summary>
    self.Save = function () {
        var oTable = $('#Items').dataTable();
        var band = false;
        var arrayFinish;
        /// Recorre el datatable para validar que la captura este completa
        $(oTable.fnGetNodes()).each(function (index, elem) {
            var aPos = oTable.fnGetPosition(this);
            var aData = oTable.fnGetData(aPos[0]);
            var tdArray = aData[index];
            var nRow = $('#Items tbody tr:eq(' + index + ') td').parent('tr')[0];
            var jqInputs = $('input', nRow);
            var che = jqInputs[2].checked;
            tdArray.active_status = che;
        });
        /// Si la captura esta completa envia al controlador
        /// la lista de productos para grabar en base de datos
        if (!band) {
            $.ajax({
                url: "/ItemCombo/ActionAddComboDetail",
                type: "POST",
                data: { itemsDetail: Products.data().toArray(), newComboCost: $('#ComboBaseCost').val(), newComboPrice: $("#comboTotalLabel").text().replace('$', '') },
                success: function (result) {
                    if (result) {
                        self.Clear();
                        toastr.success("Registo Exitoso!! <br> El combo fue actualizado con exito!");
                        EndLoading();
                    }
                    else {
                        toastr.error("Ocurrió un error al procesar.");
                        EndLoading();
                    }
                },
                error: function (result) {
                    EndLoading();
                    toastr.error("Ocurrió un error al procesar.");
                }
            });
        }
        else
            EndLoading();
    }
}

vm = new ItemViewModel();
ko.applyBindings(vm);

function round(value, decimals) {
    decimals = decimals || 4;
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function CalculateIva(total, Iva) {
    const price_iva = total / (1 + Iva);
    const iva = round(total - price_iva);
    return iva;
}

function UpdateInfo() {
    var oTable = $('#Items').dataTable();
    $(oTable.fnGetNodes()).each(function (index, elem) {
        var aPos = oTable.fnGetPosition(this);
        var aData = oTable.fnGetData(aPos[0]);
        var tdArray = aData[index];
        var nRow = $('#Items tbody tr:eq(' + index + ') td').parent('tr')[0];
        var jqInputs = $('input', nRow);
        var che = jqInputs[2].checked;
        tdArray.active_status = che;
    });
    var TotalIva = Products.cells(function (index, data, node) {
        return data.active_status === true ?
            true : false;
    }, 5).data().reduce(function (a, b) {
        return a + b;
    }, 0);
    var TotalPrice = Products.cells(function (index, data, node) {
        return data.active_status === true ?
            true : false;
    }, 8).data().reduce(function (a, b) {
        return a + b;
    }, 0);
    var TotalBase = Products.cells(function (index, data, node) {
        return data.active_status === true ?
            true : false;
    }, 7).data().reduce(function (a, b) {
        return a + b;
    }, 0);
    var IvaCombo = CalculateIva(round(TotalPrice), IVACOMBO);
    TotalIva = TotalIva + IvaCombo;

    var subTotal = round(parseFloat(TotalPrice) - parseFloat(TotalIva));

    $("#ComboBaseCost").val(round(TotalBase));
    $("#comboTotalLabel").html('$' + round(TotalPrice));
    $("#comboIvaLabel").html('$' + round(TotalIva));
    $("#comboSubtotalLabel").html('$' + round(subTotal));
    $("#combototalProductsLabel").html(Products.data().count());
    ChangeMargeCombo();
}

function isFloat(n) {
    if (n.match(/^-?\d*(\.\d+)?$/) && !isNaN(parseFloat(n)) && (n % 1 != 0))
        return true;
    return false;
}

function ValidateWeight(nameInput, value) {
    if (!weightValue) {
        if (isFloat(value)) {
            $('#' + nameInput).val('');
            toastr.warning("Este producto no es pesable, ingrese una cantidad entera.");
            return false;
        }
    }
}

function ValidateWeightTable(index) {
    var cellQuantity = Products.cell(index, 3);
    var cellWeight = Products.cell(index, 11);
    var Weight = cellWeight.data();
    var nRow = $('#Items tbody tr:eq(' + index + ') td').parent('tr')[0];
    var jqInputs = $('input', nRow);
    var Quantity = jqInputs[0].value;
    if (!Weight) {
        if (isFloat(Quantity)) {
            cellQuantity.data(0);
            toastr.warning("Este producto no es pesable, ingrese una cantidad entera.");
            return false;
        }
        else if (parseInt(Quantity) >= 1 && parseInt(Quantity) > 9999999) {
            cellQuantity.data(0);
            toastr.warning("Ingrese una cantidad razonable.");
            return false;
        }
    } else {
        if (parseFloat(Quantity) >= 0.1 && parseFloat(Quantity) > 9999999) {
            toastr.warning("Ingresa un cantidad correcta.");
            Products.cell(index, 3).data('1').draw();
            return false;
        }
        return true;
    }
}

function updateRowIva(index) {
    //Validar weight
    ValidateWeightTable(index);
    var cellQuantity = Products.cell(index, 3);
    var cellPrice = Products.cell(index, 8);
    var cellIvaPorciento = Products.cell(index, 4);
    var cellIva = Products.cell(index, 5);
    var nRow = $('#Items tbody tr:eq(' + index + ') td').parent('tr')[0];
    var jqInputs = $('input', nRow);
    var Quantity = jqInputs[0].value;
    var price = jqInputs[1].value;

    if (!isNaN(price)) {
        if (parseInt(price) > 0 && parseInt(price) < 9999999) {
            iva = CalculateIva(price, cellIvaPorciento.data() / 100);
            Quantity = Quantity > 0 ? Quantity : 1;
            Quantity = Quantity ? Quantity : 1;
            iva = iva ? iva : 0;
            price = price ? price : 0
            cellQuantity.data(round(Quantity));
            cellIva.data(round(iva));
            //cellIva.data(round(iva * Quantity));
            cellPrice.data(round(price));
            var cellCost = Products.cell(index, 7);
            var cellMarge = Products.cell(index, 6);
            var margen = ChangeMarginPrice(round(price), round(cellCost.data()));
            margen = margen ? margen : 0;
            cellMarge.data(margen);
            UpdateInfo();
        } else {
            toastr.warning("Ingresa un precio correcto.");
            Products.cell(index, 8).data('1').draw();
        }
    } else {
        toastr.warning("Ingresa un precio correcto.");
        Products.cell(index, 8).data('1').draw();
    }
}

function ChangeMargeCombo() {
    var Total = $("#comboTotalLabel").text().replace('$', '');
    var cost = $('#ComboBaseCost').val();
    if (!isNaN(cost)) {
        if (cost != "") {
            if (parseFloat(cost) > 0 && parseFloat(cost) < 99999999) {
                var margen = ChangeMarginPrice(Total, cost);
                $('#comboMargenLabel').html(margen);
            } else {
                $('#ComboBaseCost').val('');
                toastr.warning("Coloque una cantidad correcta en el costo base.");
            }
        } else {
            $('#ComboBaseCost').val('');
            toastr.warning("Coloque una cantidad correcta en el costo base.");
        }
    } else {
        $('#ComboBaseCost').val('');
        toastr.warning("Coloque una cantidad correcta en el costo base.");
    }
}

function ChangeMarginPrice(SalePrice, Cost) {
    var x = Cost / parseFloat(SalePrice);
    var margen = (1 - x) * 100;
    return round(margen);
}