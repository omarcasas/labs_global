﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#datetimepicker').datepicker({
        autoclose: true,
        format: "mm/dd/yyyy",
        startDate: "1/1/1980",
        endDate: "1/1/2200",
        todayHighlight: true,
        language: 'es'
    }).on("changeDate", function (e) {
        DateValidation()
    });

    $('#datetimepicker2').datepicker({
        autoclose: true,
        format: "mm/dd/yyyy",
        startDate: "1/1/1980",
        endDate: "1/1/2200",
        todayHighlight: true,
        language: 'es'
    }).on("changeDate", function (e) {
        DateValidation()
    });

    $("#buttonPrint").hide();
});

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
table = $("#tableOrdered").DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'copy', className: 'btn-sm' },
        { extend: 'csv', title: 'ReporteCambioCostos', className: 'btn-sm' },
    ],
    columns: [
        { data: 'part_number', },
        { data: 'part_name', },
        { data: 'business_name', },
        { data: 'base_price', },
        { data: 'currency', },
        { data: 'NameLevel1', },
        { data: 'NameLevel2', },
        { data: 'NameLevel3', },
        { data: 'valid_from', },
        { data: 'valid_to', }
    ],
    columnDefs:
        [
            {
                targets: [3],
                render: function (data, type, row) {
                    return `${fomartmoney(parseFloat(data))(4)}`;
                }
            }
        ]
});

function DateValidation() {
    $("#buttonPrint").hide();
    if ($("#dateInit").val().trim() != "" && $("#dateFin").val().trim() != "" && $("#dateFin").val() != null && $("#dateInit").val() != null) {
        var dateInit = moment($("#dateInit").val());
        var dateFin = moment($("#dateFin").val())
        if (dateInit <= dateFin) {
            StartLoading()
            axios.post("/Supplier/ActionTablePurchasePrice", { LaterDate: $("#dateInit").val(), NextDate: $("#dateFin").val() })
                .then(function (date) {
                    table.clear().draw();
                    if (date.data === "SF") {
                        window.location.href = 'http://' + window.location.host + '/Home/Login';
                    }
                    else if (date.data.length > 0) {
                        StartLoading()
                        toastr.success("Se encontraron reportes de cambio de costo")
                        table.rows.add($(date.data));
                        table.columns.adjust().draw();
                        $("#buttonPrint").show()
                        EndLoading()
                    }
                    else {
                        toastr.warning("No se encontraron reportes de cambio de costo")
                        table.rows.add($(date.data));
                        table.columns.adjust().draw();
                    }
                }).catch(function (error) {
                    toastr.error("Contacta con sistemas");
                }).then(() => { EndLoading(); table.columns.adjust().draw(); })
        }
        else {
            table.clear().draw();
            toastr.error('La primera fecha no puede ser mayor a la segunda fecha');
            setTimeout(function () {
                position = $("#dateInit").offset()
                $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
                $("#dateInitError").text("No puede ser mayor a la segunda fecha =>")
                $("#dateInitError").show();
                $("#dateInit").addClass("error");
                $("#dateInitError").fadeOut(5000, function () {
                    $("#dateInit").removeClass("error")
                })
            }, 100);
            $(".datepicker").hide();
        }
    }
    else
        table.clear().draw();
}

function PrintTable() {
    $("#iframes").html("")
    if ($("#dateInit").val().trim() != "" && $("#dateFin").val().trim() != "" && $("#dateFin").val() != null && $("#dateInit").val() != null) {
        var dateInit = moment($("#dateInit").val());
        var dateFin = moment($("#dateFin").val())
        if (dateInit <= dateFin) {
            StartLoading()
            axios.post("/Supplier/ActionPrintTablePurchasePrice", { LaterDate: $("#dateInit").val(), NextDate: $("#dateFin").val() })
                .then(function (date) {
                    if (date.data === "SF") {
                        window.location.href = 'http://' + window.location.host + '/Home/Login';
                    }
                    else if (date.data.success) {
                        $("#iframes").html(`<iframe src='data:application/pdf;base64,${date.data.responseText}' height='320px' width='100%'></iframe>`)
                    }
                    else if (!date.data.success) {
                        toastr.warning("No se encontraron precios en ese tiempo")
                    }
                    else {
                        toastr.warning("No se encontraron precios en ese tiempo")
                    }
                }).catch(function (error) {
                    toastr.error("Contacta con sistemas");
                })
                .then(() => { EndLoading() })
        }
        else
            toastr.warning("No se encontro ningun reporte para ser impreso");
    }
}