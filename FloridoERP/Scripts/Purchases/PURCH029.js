﻿document.addEventListener("DOMContentLoaded", function (event) {
    window.setTimeout(function () { document.getElementById("fa").click(); }, 1000);

    var model = JSON.parse(document.getElementById("model").value);
    table.clear();
    table.rows.add(model);
    table.columns.adjust().draw();
});

var table = $('#Codes').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    buttons: [
        { extend: 'copy', text: 'Copiar', className: 'btn-sm' },
        { extend: 'csv', text: 'Excel', title: 'Códigos en Empaques', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Códigos en Empaques', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            {
                targets: 0,
                class: "details-control",
                orderable: false,
                data: null,
                defaultContent: "",
                width: "1%"
            },
            { targets: 1, data: 'PartNumberOrigin', width: "1%" },
            { targets: 2, data: 'PartDescription', width: "1%" },
            { targets: 3, data: 'Cost', width: "1%" },
            { targets: 4, data: 'UnitSize', width: "1%" }
        ],
    columnDefs: [
        {
            targets: [3],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(4);
            }
        }]
});

var detailRows = [];
$('#Codes tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

var list = [];
function format(d) {
    list = [];
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetCodePackages",
        data: { "OriginPartNumber": d.PartNumberOrigin },
        type: "POST",
        success: function (response) {
            if (response.result == "SF")
                SessionFalse("Terminó tu sesión");
            else if (response.result) {
                response.model.forEach(function (item) {
                    detailItems += "<tr><td>" + item.PartNumber + "</td><td>" + item.PartDescription + "</td><td>" + item.Quantity + "</td><td>$" + item.Cost + "</td><td>$" + (item.Quantity * item.Cost).toFixed(2) + "</td>";
                });

                if (response.model.length > 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Costo Unitario</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }

                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Costo Unitario</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        order: [[0, "desc"]],
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs:
            [
                {
                    targets: [0],
                    width: "1%"
                },
                {
                    targets: [2],
                    width: "1%"
                }
            ]
    });
}