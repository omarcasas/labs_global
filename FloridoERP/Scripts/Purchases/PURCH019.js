﻿window.setTimeout(function () { $("#fa").click(); }, 1000);

var hRequirementsTotal = 0;
var hQuantityTotal = 0;
var hTotalTotal = 0;
var folioEdit = 0;

var table = $('#TableRequiriment').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[2, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%"
        },
        { data: 'id_supplier', width: "1%", visible: false },
        { data: 'supplier_name', width: "1%" },
        { data: 'quantity', width: "1%" },
        { data: 'total', width: "1%" }
    ],
    columnDefs: [
        {
            aTargets: [4],
            width: "1%",
            mRender: function (data, type, full) {
                return `<p> $ ${data}</p>`
            }
        },
        {
            targets: [0, 1, 2, 3, 4],
            width: "1%"
        }
    ]
});

var detailRows = [];

$('#TableRequiriment tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetRequirementDetailsBySuplier/",
        data: { "supplier_id": d.id_supplier },
        type: "GET",
        success: function (Data) {
            var Total = 0;
            $.each(Data, function (index, value) {
                detailItems += "<tr><td>" + value.site_name + "</td><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td> $ " + value.price + "</td><td class='text-center'>" + value.quantity + "</td><td class='text-center'> $ " + value.total + "</td>"
                Total = Total + value.total;
            });
            var htmlPrint = `<div id="optionTable">
                                        <div class ="col-lg-12" id="totalFinalForm">
                                            <div class="pull-right" style="width:26%;">
                                                <div class ="col-lg-6" style="border:1px solid #dddddd"><b> Total: </b></div><div class ="col-lg-6 text-right" style="border:1px solid #dddddd" id="totalFinal">$ ${Total.toFixed(2)}</div>
                                            </div>
                                        </div>
                                    </div>`;
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Tienda</th><th>Código</th><th>Descripción</th><th>Precio Estimado</th><th>Cantidad</th><th>Total Estimado</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + htmlPrint).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

function SendEmail() {
    var band = false;
    listFolios = [];

    tableReq.rows().every(function (index, element) {
        var row = $(this.node());

        var check = row.find('td').eq(0);
        var isChecked = $('input', check).prop('checked');
        var folio = $('input', check).prop('id');

        if (isChecked) {
            listFolios.push(folio);
            band = true;
        }
    });

    if (band && listFolios.length > 0) {
        $.ajax({
            url: "/InternalRequirementHeader/ActionSendEmailCreditors/",
            data: { "listRelation": listRelation, "listItems": listItems, "listFolios": listFolios },
            type: "POST",
            success: function (result) {
                EndLoading();
                ClearForm();
                if (result.success == true)
                    toastr.success('Folios mandados correctamente.');
                else if (result.error = "1")
                    toastr.error("La información no se ha actualizado por que no se ha podido encontrar contactos del proveedor '" + result.responseText + "'. Ingrese contactos al acreedor y vuelva a interntarlo.");
                else if (result.error = "2")
                    toastr.error("La información si se ha actualizado por que no se ha podido encontrar contactos del proveedor '" + result.responseText + "'. Ingrese contactos al acreedor y vuelva a interntarlo. Contacte a Sistemas");
                else if (result.error = "3")
                    toastr.error("La información si se ha actualizado por que no se ha podido encontrar agregar a orden de compra con el folio: '" + result.responseText + "'. Contacte a Sistemas");
                else if (result.responseText == 'SF')
                    SessionFalse('Su sessión ha sido a terminado.')
            },
            error: function () {
                EndLoading();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

listRelation = [];
listItems = [];

function Send() {
    var band = false;

    tableReq.rows().every(function (index, element) {
        var row = $(this.node());

        var check = row.find('td').eq(0);
        var isChecked = $('input', check).prop('checked');

        if (isChecked)
            band = true;
    });

    if (band) {
        listRelation = [];
        $.ajax({
            url: "/InternalRequirementHeader/ActionRelationStoreSupplier/",
            type: "GET",
            success: function (result) {
                if (result.success == true) {
                    listRelation = result.listAssociation;
                    listItems = result.listItems;
                    if (result.responseText != "")
                        toastr.warning('Lo(s) acreedore(s): ' + result.responseText + ' no contiene contacto(s) en el departamento de Ventas Activo, favor de ingresarlo antes de continuar.');
                    swal({
                        title: "¿Esta seguro que desea mandar la(s) ordene(s) de compra?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#55dd6b",
                        confirmButtonText: "Continuar",
                        cancelButtonText: "Cancelar"
                    }, function (isConfirm) {
                        if (isConfirm) { SendEmail(); }
                    });
                }
                else {
                    if (result.responseText == 'SF') {
                        SessionFalse('Su sessión ha sido a terminado.')
                    } else {
                        toastr.warning('Lo(s) acreedore(s): no contiene contactos en el departamento de Ventas Activo, favor de ingresarlo antes de continuar. ');
                    }
                }
            },
            error: function () {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
    else
        swal({
            title: "Seleccione al menos una requisición.",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar"
        });
}

function ClearForm() {
    table.clear();
    table.columns.adjust().draw();
    tableReq.clear();
    tableReq.columns.adjust().draw();
    $("#hRequirementsTotal").text(0);
    $("#hQuantityTotal").text(0);
    $("#hTotalTotal").text("$0");
    $("#checkView")[0].checked = false;
    ChangeView();
    $(".checkAll").iCheck('uncheck');
}

var tableReq = $('#TableRequirements').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: null, width: "1%" },
        { data: 'folio', width: "1%", visible: false },
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%"
        },
        { data: 'site_name', width: "1%" },
        { data: 'quantity', width: "1%" },
        { data: 'total', width: "1%" },
        { data: null, width: "1%", "orderable": false },
    ],
    fnDrawCallback: function () {
        $('.i-checks,.checkAll').iCheck({
            checkboxClass: 'icheckbox_square-green',
        }).on('ifClicked', function () {
            $(this).trigger("change");
        });

        $(".checkAll").on('change', function () {
            if (!$(this).is(':checked'))
                $(".i-checks").iCheck('check');
            else {
                $(".i-checks").iCheck('uncheck');
                var oTable = $('#TableRequirements').dataTable();
                oTable.$('input').removeAttr('checked');
            }
        });

        if ($(".checkAll").is(":checked"))
            $(".i-checks").iCheck('check');

        $(".i-checks").on('change', function () {
            if ($(this).is(':checked'))
                $(".checkAll").iCheck('uncheck');
        });
    },
    columnDefs: [
        {
            targets: [0, 1, 2],
            width: "1%",
            searchable: false,
            orderable: false
        },
        {
            targets: [0],
            width: "1%",
            className: "text-center",
            render: function (data, type, full, meta) {
                return "<div class='checkbox'><label> <input type='checkbox' id=" + data.folio + " class='i-checks child'></label></div>";
            }
        },
        {
            targets: [5],
            width: "1%",
            mRender: function (data, type, full) {
                return `<p> $ ${data}</p>`
            }
        },
        {
            targets: [6],
            width: "1%",
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-outline btn-info" onclick="ShowPanelEdit(${data.folio})"><i class="pe-7s-config"></i>  Configurar</button>`;
            }
        },
        {
            targets: [0, 1, 2, 3, 4, 5, 6],
            width: "1%"
        }
    ]
});

var SentOrders = $('#SentOrders').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            class: "details-control",
            orderable: false,
            data: null,
            defaultContent: "",
            width: "1%"
        },
        { data: "folio", width: "1%", visible: false },
        { data: "site_name", width: "1%" },
        { data: "quantity", width: "1%" },
        { data: "total", width: "1%" }
    ],
    columnDefs: [
        {
            targets: [0],
            width: "1%",
            searchable: false
        },
        {
            targets: [4],
            width: "1%",
            mRender: function (data, type, full) {
                return "<p>$" + data + "</p>";
            }
        },
        {
            targets: [0, 1, 2, 3, 4],
            width: "1%"
        }
    ]
});

var detailRows = [];

$('#TableRequirements tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableReq.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tableReq.row('.details').length) {
            $('.details-control', tableReq.row('.details').node()).click();
        }
        row.child(formatTwo(row.data())).show();
        tr.addClass('details');
    }
});

$('#SentOrders tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = SentOrders.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (SentOrders.row('.details').length) {
            $('.details-control', SentOrders.row('.details').node()).click();
        }
        row.child(formatSentOrders(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function formatTwo(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetRequirementsDetailsAll/",
        data: { "folio": d.folio },
        type: "POST",
        success: function (result) {
            if (result.Data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else {
                var Total = 0;
                $.each(result.Data, function (index, value) {
                    detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td>" + value.category_name + "</td><td>" + value.supplier_name + "</td><td> $ " + value.price + "</td><td>" + value.quantity + "</td><td> $ " + value.total + "</td>";
                    Total = Total + value.total;
                });

                var htmlPrint = `<div id="optionTable">
                                        <div class ="col-lg-12" id="totalFinalForm">
                                            <div class="pull-right" style="width:26%;">
                                                <div class ="col-lg-6" style="border:1px solid #dddddd"><b> Total: </b></div><div class ="col-lg-6 text-right" style="border:1px solid #dddddd" id="totalFinal"> $ ${Total.toFixed(4)}</div>
                                            </div>
                                        </div>
                                    </div>`;

                tabledetail.html('<table id="tabledetailViewAllRequirements" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripcion</th><th>Categoria</th><th>Proveedor</th><th>Precio Estimado</th><th>Cantidad</th><th>Total Estimado</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + htmlPrint).removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail;
}

function formatSentOrders(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetRequirementsDetailsAll/",
        data: { "folio": d.folio },
        type: "POST",
        success: function (result) {
            if (result.Data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else {
                var Total = 0;
                $.each(result.Data, function (index, value) {
                    detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td>" + value.category_name + "</td><td>" + value.supplier_name + "</td><td> $ " + value.price + "</td><td>" + value.quantity + "</td><td> $ " + value.total + "</td>";
                    Total = Total + value.total;
                });

                var htmlPrint = `<div id="optionTable">
                                        <div id="totalFinalForm" class="col-lg-12">
                                            <div class="pull-right" style="width:26%;">
                                                <div class="col-lg-6" style="border:1px solid #dddddd"><b> Total: </b></div><div id="totalFinal" class="col-lg-6 text-right" style="border:1px solid #dddddd"> $ ${Total.toFixed(4)}</div>
                                            </div>
                                        </div>
                                    </div>`;

                tabledetail.html('<table id="tabledetailViewAllRequirements" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripcion</th><th>Categoria</th><th>Proveedor</th><th>Precio Estimado</th><th>Cantidad</th><th>Total Estimado</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + htmlPrint).removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

var listRequirement = [];
var listRequirementOriginal = [];

function ShowPanelEdit(folio) {
    folioEdit = folio;
    $.ajax({
        type: "POST",
        url: "/InternalRequirementHeader/ActionGetItemsCreditorsByFolio/",
        data: { "folio": folio },
        success: function (result) {
            if (result.Data != null) {
                listRequirementOriginal = result.Data;
                listRequirement = result.Data;
                tableEdit.clear();
                tableEdit.rows.add(result.Data);
                tableEdit.columns.adjust().draw();

                $('#TableStoreFolio').addClass("hide");
                $('#PanelEdit').removeClass("hide");
                $('#PanelEdit').animatePanel();
            }
            else if (!result.success)
                SessionFalse("Terminó su sesión.");
        },
        error: function (response) {
            toastr.error("Error desconocido contacte a sistemas.");
        }
    });

    $.ajax({
        type: "POST",
        url: "/InternalRequirementHeader/ActionGetItemsCreditorsAll/",
        success: function (result) {
            if (result.Data != null) {
                var filtrados;
                for (var i = 0; i < result.Data.length; i++) {
                    filtrados = listRequirement.filter(x => x.id_item_supplier == result.Data[i].id_item_supplier);
                    if (filtrados.length > 0)
                        result.Data[i].quantity = filtrados[0].quantity;
                }
                tableAdd.clear();
                tableAdd.rows.add(result.Data);
                tableAdd.columns.adjust().draw();

                $('#TableStoreFolio').addClass("hide");
                $('#PanelEdit').removeClass("hide");
                $('#PanelEdit').animatePanel();
            }
            else if (!resul.success)
                SessionFalse("Terminó su sesión.");
        },
        error: function (response) {
            toastr.error("Error desconocido contacte a sistemas.");
        }
    });
}

//tableItems2
var tableEdit = $('#tableItemsEdit').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'id_item_supplier', width: "1%", visible: false },
            { data: 'part_number', width: "1%" },
            { data: 'description', width: "1%" },
            { data: 'category', width: "1%" },
            { data: 'supplier_name', width: "1%" },
            { data: 'price', width: "1%" },
            { data: null, width: "1%" },
            { data: null, width: "1%" }
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5, 6, 7],
            width: "1%"
        },
        {
            targets: [5],
            width: "1%",
            mRender: function (data, type, full) {
                return `<p> $ ${data}</p>`
            }
        },
        {
            targets: [6],
            width: "1%",
            render: function (data, type, full, meta) {
                var description = data.description.replace(/[`~!#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, ' ');
                return `<input maxlength="8" class="form-control" id="selected${data.id_item_supplier}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange="set('${data.part_number}','${description}','${data.category}','${data.supplier_name}','${data.price}','${data.id_item_supplier}')" value = "${data.quantity}" style="width:80px"></input>`;
            }
        },
        {
            targets: [7],
            width: "1%",
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-danger btn-circle" style="text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;" onclick="deleteItem(${data.id_item_supplier})"><i class="fa fa-times"></i></button>`;
            }
        }]
});

list = [];
listFinal = [];
items = 0;

function get(id, description, category, supplier_name, price, id_item_supplier) {
    var quantity = $("#" + id_item_supplier + "").val();
    if ($("#" + id_item_supplier + "").is(':checked')) {
        quantity = 1;
    }
    if ($("#" + id_item_supplier + "").val() > 0 || $("#" + id_item_supplier + "").is(':checked')) {

        var indexOriginal = listRequirementOriginal.findIndex(elements => elements.id_item_supplier == id_item_supplier);
        if (indexOriginal >= 0)
            listRequirementOriginal[indexOriginal] = { part_number: id, description: description, category: category, price: price, supplier_name: supplier_name, id_item_supplier: id_item_supplier, quantity: quantity };
        var index = listRequirement.findIndex(elements => elements.id_item_supplier == id_item_supplier);
        if (index >= 0) {
            listRequirement[index] = { part_number: id, description: description, category: category, price: price, supplier_name: supplier_name, id_item_supplier: id_item_supplier, quantity: quantity };
        }
        else {
            listRequirement.push({ part_number: id, description: description, category: category, supplier_name: supplier_name, price: price, id_item_supplier: id_item_supplier, quantity: quantity });
            if (indexOriginal < 0) {
                listRequirementOriginal = listRequirementOriginal.filter(x => x.id_item_supplier != id_item_supplier);
            }
        }
    } else {
        listRequirement = listRequirement.filter(x => x.id_item_supplier != id_item_supplier);
        var list = listRequirementOriginal.filter(x => x.id_item_supplier == id_item_supplier);
        if (list.length > 0) {
            $.each(listRequirementOriginal, function (index, value) {
                if (value.id_item_supplier == list[0].id_item_supplier) {
                    value.quantity = 0;
                }
            });
        }
    }

    tableEdit.clear();
    tableEdit.rows.add(listRequirement);
    tableEdit.columns.adjust().draw();
}

function set(id, description, category, supplier_name, price, id_item_supplier) {
    var quantity = $("#selected" + id_item_supplier + "").val();
    if ($("#selected" + id_item_supplier + "").val() > 0) {
        var indexOriginal = listRequirementOriginal.findIndex(elements => elements.id_item_supplier == id_item_supplier);
        if (indexOriginal >= 0)
            listRequirementOriginal[indexOriginal] = { part_number: id, description: description, category: category, price: price, supplier_name: supplier_name, id_item_supplier: id_item_supplier, quantity: quantity };
        var index = listRequirement.findIndex(elements => elements.id_item_supplier == id_item_supplier);
        if (index >= 0) {
            listRequirement[index] = { part_number: id, description: description, category: category, supplier_name: supplier_name, price: price, id_item_supplier: id_item_supplier, quantity: quantity };
        }
        else {
            listRequirement.push({ part_number: id, description: description, category: category, supplier_name: supplier_name, price: price, id_item_supplier: id_item_supplier, quantity: quantity });
            if (indexOriginal < 0) {
                listRequirementOriginal = listRequirementOriginal.filter(x => x.id_item_supplier != id_item_supplier);
            }
        }
    } else {
        listRequirement = listRequirement.filter(x => x.id_item_supplier != id_item_supplier);
        var list = listRequirementOriginal.filter(x => x.id_item_supplier == id_item_supplier);
        if (list.length > 0) {
            $.each(listRequirementOriginal, function (index, value) {
                if (value.id_item_supplier == list[0].id_item_supplier) {
                    value.quantity = 0;
                }
            });
        }
    }

    tableEdit.clear();
    tableEdit.rows.add(listRequirement);
    tableEdit.columns.adjust().draw();
    $("#" + id_item_supplier + "").prop('checked', false);
    $("#" + id_item_supplier + "").val(quantity);
}

function deleteItem(id_item_supplier) {
    var list = listRequirementOriginal.filter(x => x.id_item_supplier == id_item_supplier);
    if (list.length > 0) {
        $.each(listRequirementOriginal, function (index, value) {
            if (value.id_item_supplier == list[0].id_item_supplier) {
                value.quantity = 0;
            }
        });
    }

    listRequirement = listRequirement.filter(x => x.id_item_supplier != id_item_supplier);
    tableEdit.clear();
    tableEdit.rows.add(listRequirement);
    tableEdit.columns.adjust().draw();
    $("#" + id_item_supplier + "").prop('checked', false);
    $("#" + id_item_supplier + "").val("0");
}

function aprovalCancel() {
    swal({
        title: "¿Esta seguro que desea ELIMINAR esta requisición?",
        text: "Todos los productos se eliminarán.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar."
    }, function () {
        StartLoading();
        $.ajax({
            url: "/InternalRequirementHeader/ActionDeleteFolio/",
            data: { "folio": folioEdit },
            type: "GET",
            success: function (result) {
                if (result.success == true) {
                    if (result.responseText == "1")
                        toastr.success('Eliminado Correctamente.');
                    else
                        toastr.error('Error inesperado contactar a sistemas.');
                }
                else if (result.responseText == 'SF')
                    SessionFalse('Su sessión ha sido a terminado.')
            },
            error: function () {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });

        listRequirementOriginal = [];
        listRequirement = [];
        list = [];
        folioEdit = 0;
        EndLoading();
        Return();
    });
}

function aprovalSave() {
    const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0);
    var sumQuantityOriginal = Sum(listRequirementOriginal)('quantity');
    var sumQuantity = Sum(listRequirement)('quantity');
    if (sumQuantity > 0 || sumQuantityOriginal > 0) {
        swal({
            title: "¿Esta seguro que desea guardar los cambios de esta Requisición?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                for (var i = 0; i < listRequirement.length; i++) {
                    var list = listRequirementOriginal.filter(x => x.id_item_supplier == listRequirement[i].id_item_supplier);
                    if (list >= 0)
                        listRequirementOriginal.push(listRequirement[i]);
                }
                $.ajax({
                    url: "/InternalRequirementHeader/ActionUpdateItemsRequirement/",
                    type: "POST",
                    data: { "items": listRequirementOriginal, "folio": folioEdit },
                    success: function (Data) {
                        if (Data.success == true) {
                            if (Data.responseText == "1")
                                toastr.success('Requisición Actualizada.');
                            else
                                toastr.warning('Error inesperado al actualizar la información, favor de  contactar a sistemas.');
                        } else
                            SessionFalse("Su sesión ha terminado.");
                    },
                    error: function () {
                        toastr.error('Error inesperado contactar a sistemas.');
                    }
                });
                listRequirementOriginal = [];
                listRequirement = [];
                list = [];
                folioEdit = 0;
                EndLoading();
                Return();
            }
        });
    } else {
        swal({
            title: "¿Esta seguro que desea ELIMINAR esta requisición?",
            text: "Haz eliminado todos los productos manualmente.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar."
        }, function () {
            StartLoading();
            $.ajax({
                url: "/InternalRequirementHeader/ActionDeleteFolio/",
                data: { "folio": folioEdit },
                type: "GET",
                success: function (result) {
                    if (result.success == true) {
                        if (result.responseText == "1")
                            toastr.success('Eliminado Correctamente.');
                        else
                            toastr.error('Error inesperado contactar a sistemas.');
                    }
                    else {
                        if (result.responseText == 'SF')
                            SessionFalse('Su sessión ha sido a terminado.')
                    }
                },
                error:
                    function () {
                        toastr.error('Error inesperado contactar a sistemas.');
                    }
            });

            listRequirementOriginal = [];
            listRequirement = [];
            list = [];
            folioEdit = 0;
            EndLoading();
            Return();
        });
    }
}

var tableAdd = $('#tableItemsAdd').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'id_item_supplier', width: "1%", visible: false },
            { data: 'part_number', width: "1%" },
            { data: 'description', width: "1%" },
            { data: 'category', width: "1%" },
            { data: 'supplier_name', width: "1%" },
            { data: 'price', width: "1%" },
            { data: null, width: "1%" }
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5, 6],
            width: "1%"
        },
        {
            targets: [5],
            width: "1%",
            mRender: function (data, type, full) {
                return `<p> $ ${data}</p>`
            }
        },
        {
            targets: [6],
            width: "1%",
            render: function (data, type, full, meta) {
                if (data.quantity != 0)
                    return `<input maxlength="8" class="form-control" id="${data.id_item_supplier}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange= "get('${data.part_number}','${data.description}','${data.category}','${data.supplier_name}','${data.price}','${data.id_item_supplier}')" value = "${data.quantity}" style="width:80px"></input>`;
                else
                    return `<input maxlength="8" class="form-control" id="${data.id_item_supplier}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange= "get('${data.part_number}','${data.description}','${data.category}','${data.supplier_name}','${data.price}','${data.id_item_supplier}')" value = "0" style="width:80px"></input>`;
            }
        }]
});

function Return() {
    UpdateTables();
    $('#PanelEdit').addClass("hide");
    $('#TableStoreFolio').removeClass("hide");
    tableAdd.clear();
    tableEdit.clear();
    $('#TableStoreFolio').animatePanel();
}

function ChangeView() {
    var checkBox = document.getElementById("checkView");
    if (checkBox.checked == true) {
        UpdateTables();

        $('#labelCreditor').removeClass("project-label");
        $('#labelCreditor').removeClass("extra-bold");
        $('#labelCreditor').removeClass("text-success");

        $('#labelSite').addClass("project-label");
        $('#labelSite').addClass("extra-bold");
        $('#labelSite').addClass("text-info");

        $('#divMain').addClass("hide");
        $('#divSecond').removeClass("hide");

        $('#divSecond').animatePanel();
    } else {
        UpdateTables();

        $('#labelSite').removeClass("project-label");
        $('#labelSite').removeClass("extra-bold");
        $('#labelSite').removeClass("text-info");

        $('#labelCreditor').addClass("project-label");
        $('#labelCreditor').addClass("extra-bold");
        $('#labelCreditor').addClass("text-success");

        $('#divSecond').addClass("hide");
        $('#divMain').removeClass("hide");

        $('#divMain').animatePanel();
    }
}

function UpdateTables() {
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetRequirementsAllSites/",
        type: "GET",
        success: function (result) {
            if (result.success == true) {

                if (!$.isEmptyObject(result.folios)) {
                    var listFolios = result.folios;
                    const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0);
                    hRequirementsTotal = result.folios[0].folio;
                    hQuantityTotal = Sum(listFolios)('quantity');
                    hTotalTotal = Sum(listFolios)('total').toFixed(4);

                    $("#hRequirementsTotal").text(result.folios[0].folio);
                    $("#hQuantityTotal").text(hQuantityTotal);
                    $("#hTotalTotal").text("$" + hTotalTotal);

                    $("#btnSave1").removeAttr('disabled', 'disabled');
                    $("#btnSave2").removeAttr('disabled', 'disabled');

                    table.clear();
                    table.rows.add($(result.folios));
                    table.columns.adjust().draw();
                } else {
                    $("#hRequirementsTotal").text(0);
                    $("#hQuantityTotal").text(0);
                    $("#hTotalTotal").text("$0");

                    $("#btnSave1").attr('disabled', true);
                    $("#btnSave2").attr('disabled', true);

                    table.clear();
                    table.columns.adjust().draw();
                }
            }
            else if (result.responseText == 'SF')
                SessionFalse('Su sessión ha sido a terminado.')
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            $("#btnSave1").attr('disabled', true);
            $("#btnSave2").attr('disabled', true);
        }
    });

    $.ajax({
        url: "/InternalRequirementHeader/ActionGetRequirementsHeaderAll/",
        type: "POST",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Terminó tu sesión");
            else {
                tableReq.clear();
                tableReq.rows.add(data.Data);
                tableReq.columns.adjust().draw();

                SentOrders.clear();
                SentOrders.rows.add(data.SentOrders);
                SentOrders.columns.adjust().draw();
            }
        }
    });
}

function Search() {
    var begindate = $('#BeginDate').val();
    var enddate = $('#EndDate').val();

    if (begindate != "" & enddate != "") {
        if (Date.parse(enddate) < Date.parse(begindate))
            toastr.error("La fecha inicial no puede ser mayor a la fecha final");
        else {
            $.ajax({
                url: "/InternalRequirementHeader/ActionGetSentRequirementsByDates",
                type: "GET",
                data: { "BeginDate": begindate, "EndDate": enddate },
                success: function (result) {
                    if (result.Status) {
                        SentOrders.clear();
                        SentOrders.rows.add($(result.SentOrders));
                        SentOrders.columns.adjust().draw();
                        $('#TableSentOrders').animatePanel();
                    }
                    else {
                        SessionFalse("Se terminó su sesion.");
                        SentOrders.clear();
                        SentOrders.columns.adjust().draw();
                        $('#TableSentOrders').animatePanel();
                    }
                },
                error: function () {
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
        }
    }
}

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true
}).on("changeDate", function (e) {
    Search();
});

$("#EndDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function (e) {
    Search();
});

$(document).ready(function () {

    $.ajax({
        url: "/InternalRequirementHeader/ActionGetRequirementsAllSites/",
        type: "GET",
        success: function (result) {
            if (result.success == true) {

                if (!$.isEmptyObject(result.folios)) {
                    var listFolios = result.folios;
                    const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0);
                    hRequirementsTotal = result.folios[0].folio;
                    hQuantityTotal = Sum(listFolios)('quantity');
                    hTotalTotal = Sum(listFolios)('total').toFixed(4);

                    $("#hRequirementsTotal").text(result.folios[0].folio);
                    $("#hQuantityTotal").text(hQuantityTotal);
                    $("#hTotalTotal").text("$" + hTotalTotal);

                    $("#btnSave1").removeAttr('disabled', 'disabled');
                    $("#btnSave2").removeAttr('disabled', 'disabled');

                    table.clear();
                    table.rows.add($(result.folios));
                    table.columns.adjust().draw();
                }
                else {
                    $("#hRequirementsTotal").text("0");
                    $("#hQuantityTotal").text("0");
                    $("#hTotalTotal").text("$0");

                    $("#btnSave1").attr('disabled', true);
                    $("#btnSave2").attr('disabled', true);

                    table.clear();
                    table.columns.adjust().draw();
                }

                SentOrders.clear();
                SentOrders.rows.add($(result.SentOrders));
                SentOrders.columns.adjust().draw();
            }
            else if (result.responseText == 'SF')
                SessionFalse('Su sessión ha sido a terminado.')
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            $("#btnSave1").attr('disabled', true);
            $("#btnSave2").attr('disabled', true);
        }
    });

});