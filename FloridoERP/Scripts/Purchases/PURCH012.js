﻿listCheck = [];
listItem = [];
list = [];
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money); //Formato de Decimales
$(document).ready(function () {
    $(document).on("keypress", '.Numeric', function (event) {
        var keyValue = event.key;
        if (keyValue.match(/^(\d*\.?\d*)$/))
            return true;
        else
            return false;
    });

    $("#suppliersDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    window.setTimeout(function () {
        $(".normalheader").addClass("small-header");
        $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        $("#hbreadcrumb").removeClass("m-t-lg");
    }, 500);
});
var today = new Date();
$('#fechaGeneral').datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
}).on("change", function () {
    var v = $('#fechaGeneral').val();
    var l = $('input[name="fecha"]').length;
    if (l > 0) {
        $('input[name="fecha"]').val(v).trigger('change');
    }
});

function GetProductsCostBySupplier() {
    if ($('#suppliersDrop').val() != "" && $('#suppliersDrop').val() != null) {
        StartLoading();
        listCheck = [];
        listFinals = [];
        $.ajax({
            url: "/Supplier/ActionGetItemsBySupplierCodeInSalePrice",
            type: "GET",
            data: { "supplier_code": $('#suppliersDrop').val() },
            success: function (returndates) {
                if (returndates.success == true) {
                    var table = "";
                    i = 0;
                    listItem = returndates.Json;
                    exchange_price = returndates.exchange;
                    $.each(returndates.Json, function (Purch012, value) {
                        var CostMXN = 0;
                        var cost_unit = 0;
                        var showCostMXN = '';
                        var cost_local = 0;
                        if (parseFloat(value.packing_size) == 0)
                            value.packing_size = "1";
                        
                        cost_local = parseFloat(value.item_cost) / parseFloat(value.packing_size);
                        if (value.currency == "USD") {
                            cost_unit = parseFloat(value.item_cost) * exchange_price;
                            CostMXN = (cost_local * exchange_price).toFixed(4);
                            showCostMXN = ' - (' + fomartmoney(value.item_cost * exchange_price)(2) + ' MXN)';
                           
                        } else {
                            cost_unit = cost_local;
                            CostMXN = cost_local;
                            showCostMXN = '';
                        }
                        value.margen = OperationMarginCurrent(value.price, cost_unit, value.ieps);
                        var iva = parseInt(value.iva);
                        var ieps = parseInt(value.ieps);
                        var costo = parseFloat(cost_unit);
                        var costoIEPS = (ieps / 100) * (cost_unit);
                        var costoTotal = ((costo + costoIEPS) * (iva / 100)) + costo;
                        if (list.length > 0) {
                            const result = list.filter(x => x.partNumber == value.item_number);
                            if (result.length > 0) {
                                table += "<tr class='active success'><td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='UPC_" + Purch012 + "'>" + value.item_number + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='description'>" + value.item_name + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><input name='fecha' type='text' value='" + value.uDate + "' id='dateApplied_" + Purch012 + "' class='form-control' style='text-align:center;font-family: Verdana; font-size:8pt'/></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='packing_" + Purch012 + "'>" + value.packing_size + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='costSupplier_" + Purch012 + "'>" + fomartmoney(value.item_cost)(2)+ ' ' + value.currency + showCostMXN+ " </a></td>"
                                table += "<td style='display:none;'><a id='costRealSupplier_" + Purch012 + "'>$" + parseFloat(CostMXN).toFixed(4) + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='iva_" + Purch012 + "'>" + value.iva + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='ieps_" + Purch012 + "'>" + value.ieps + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='costTotalSupplier_" + Purch012 + "'>$" + costoTotal.toFixed(4) + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='margen_current_" + Purch012 + "'><b>" + value.margen + " </b></a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='price_current_" + Purch012 + "'><b>$" + value.price + " </b></a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'>q<a id='margen_" + Purch012 + "'>" + value.margen + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><div class='input-group m-b'><span class='input-group-addon'>$</span><input type='number' min='0' max='9999999' value='" + value.price + "' id='priceUser" + Purch012 + "' class='form-control Numeric' style='text-align:center;font-family: Verdana; font-size:10pt' onfocusout='CheckQuantity(" + Purch012 + ")' /></div></td></tr>"
                                listCheck[i] = { part_number: value.item_number, appliedDate: "", margin: 0, priceUser: 0, part_iva: value.iva, part_ieps: value.ieps, check: false };
                            }
                            else {
                                table += "<tr class=''><td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='UPC_" + Purch012 + "'>" + value.item_number + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='description'>" + value.item_name + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><input name='fecha' type='text' id='dateApplied_" + Purch012 + "' class='form-control' style='text-align:center;font-family: Verdana; font-size:8pt'/></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='packing_" + Purch012 + "'>" + value.packing_size + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='costSupplier_" + Purch012 + "'>" + fomartmoney(value.item_cost)(2) + ' ' + value.currency + showCostMXN + " </a></td>"

                                table += "<td style='display:none;'><a id='costRealSupplier_" + Purch012 + "'>" + parseFloat(CostMXN).toFixed(4) + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='iva_" + Purch012 + "'>" + value.iva + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='ieps_" + Purch012 + "'>" + value.ieps + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='costTotalSupplier_" + Purch012 + "'>$" + costoTotal.toFixed(4) + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='margen_current_" + Purch012 + "'><b>" + value.margen + " </b></a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:pt'><a id='price_current_" + Purch012 + "'><b>$" + value.price + " </b></a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='margen_" + Purch012 + "'></a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><div class='input-group m-b'><span class='input-group-addon'>$</span><input type='number' min='0' max='9999999' id='priceUser" + Purch012 + "' class='form-control Numeric' style='text-align:center;font-family: Verdana; font-size:10pt' onfocusout='CheckQuantity(" + Purch012 + ")' /></div></td></tr>"
                                listCheck[i] = { part_number: value.item_number, appliedDate: "", margin: 0, priceUser: 0, part_iva: value.iva, part_ieps: value.ieps, check: false };
                            }
                        }
                        else {
                            table += "<tr class=''><td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='UPC_" + Purch012 + "'>" + value.item_number + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='description'>" + value.item_name + " </a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><input name='fecha' type='text' id='dateApplied_" + Purch012 + "' class='form-control' style='text-align:center;font-family: Verdana; font-size:8pt'/></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='packing_" + Purch012 + "'>" + value.packing_size + " </a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='costSupplier_" + Purch012 + "'>" + fomartmoney(value.item_cost)(2) + ' ' + value.currency + showCostMXN + " </a></td>"

                            table += "<td style='display:none;'><a id='costRealSupplier_" + Purch012 + "'>" + parseFloat(CostMXN).toFixed(4) + " </a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='iva_" + Purch012 + "'>" + value.iva + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='ieps_" + Purch012 + "'>" + value.ieps + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='costTotalSupplier_" + Purch012 + "'>$" + costoTotal.toFixed(4) + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='margen_current_" + Purch012 + "'><b>" + value.margen + " </b></a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='price_current_" + Purch012 + "'><b>$" + value.price + " </b></a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><a id='margen_" + Purch012 + "'></a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:8pt'><div class='input-group m-b'><span class='input-group-addon'>$</span><input type='number' min='0' max='9999999' id='priceUser" + Purch012 + "' class='form-control Numeric' style='text-align:center;font-family: Verdana; font-size:10pt' onfocusout='CheckQuantity(" + Purch012 + ")' /></div></td></tr>"
                            listCheck[i] = { part_number: value.item_number, appliedDate: "", margin: 0, priceUser: 0, part_iva: value.iva, part_ieps: value.ieps, check: false };
                        }
                        i++;
                    });
                    $("#tableChangePriceBody").html(table);
                    $('input[name="fecha"]').datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
                    }).on("change", function (e) {
                        CheckQuantity(e.target.id.split('_')[1]);
                    });
                    $(".textHidde").hide();
                    EndLoading();
                }
                else {
                    EndLoading();
                    if (returndates.responseText == "Termino tu sesion.") {
                        SessionFalse(returndates.responseText);
                    }
                    else {
                        toastr.warning('' + returndates.responseText + '');
                        $("#suppliersDrop").select2('open');
                        $("#suppliersDrop").val("").trigger("change");
                        $("#tableChangePriceBody").html("");
                    }
                }
            },
            error: function () {
                EndLoading();
                console.log("Desconocido, contacte a sistemas.")
            }
        });
    }
    else {
        $("#tableChangePriceBody").html("");
    }
}

function Search() {
    var value = document.querySelector("#filter").value.toLowerCase()
    $("#tableChangePriceBody tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function CheckQuantity(index) {
    var price = $('#priceUser' + index).val();
    var y = $('#dateApplied_' + index).val();
    //isDecimal = /^\d+\.9$/;
    isDecimal = /^\d/;
    var cost = $('#costRealSupplier_' + index).text();
    var pIEPS = $('#ieps_' + index).text();
    if (!isNaN(price)) {
        if (price != "") {
            if (parseFloat(price) > 0 && parseFloat(price) < 99999999) {
                if (price != "" && y != "" && isDecimal.test(parseFloat(price))) {
                    listCheck[index].priceUser = price;
                    listCheck[index].appliedDate = y;
                    listCheck[index].check = true;
                    OperationMargin(index, price, cost, pIEPS);
                }
                else if (price != "" && y != "" && !isDecimal.test(parseFloat(price))) {
                    $('#priceUser' + index).focus();
                    //toastr.warning("Precio debe terminar en .9");
                    toastr.warning("Precio es un valor incorrecto");
                }
                //else if (price != "" && y == "" && isDecimal.test(parseFloat(price))) { //20210325
                else if (price != "" && y == "" && isDecimal.test(parseFloat(price))) {
                    toastr.warning("Ingresa Fecha");
                    $('#dateApplied_' + index).focus();
                    OperationMargin(index, price, cost, pIEPS);
                }
                else {
                    listCheck[index].priceUser = price;
                    listCheck[index].appliedDate = y;
                    listCheck[index].check = false;
                    return false;
                }
            } else {
                $('#priceUser' + index).val('');
                toastr.warning("Ingrese una cantidad correcta.");
                listCheck[index].priceUser = price;
                listCheck[index].appliedDate = y;
                listCheck[index].check = false;
            }
        } else {
            $('#priceUser' + index).val('');
            toastr.warning("Ingrese una cantidad correcta.");
            listCheck[index].priceUser = price;
            listCheck[index].appliedDate = y;
            listCheck[index].check = false;
        }
    } else {
        $('#priceUser' + index).val('');
        toastr.warning("Ingrese una cantidad correcta.");
        listCheck[index].priceUser = price;
        listCheck[index].appliedDate = y;
        listCheck[index].check = false;
        return false;
    }
}

function OperationMarginCurrent(PriceUser, costSupplier, pIEPS) {
    var ieps = parseInt(pIEPS);
    var ieps2 = 1 + (ieps / 100);
    var costoConIEPS = ieps2 * costSupplier;
    var x = costoConIEPS / parseFloat(PriceUser);
    var margen = fomartmoney((1 - x) * 100)(4);
    var NewMArgen = margen.split("$");
    return NewMArgen[0]+NewMArgen[1];
}

function OperationMargin(index, PriceUser, costSupplier, pIEPS) {
    var supplierCost = costSupplier.split("$");
    var ieps = parseInt(pIEPS);
    var ieps2 = 1 + (ieps / 100);
    var costoConIEPS = ieps2 * supplierCost;
    var x = costoConIEPS / parseFloat(PriceUser);
    var margen = (1 - x) * 100;
    $('#margen_' + index).text(margen.toFixed(4));
    listCheck[index].margin = margen.toFixed(4);
}

function SavePrices() {
    listFinals = [];
    for (var x = 0; x < listCheck.length; x++) {
        if (listCheck[x].check == true) {
            listFinals[x] = { partNumber: listCheck[x].part_number, appliedDate: listCheck[x].appliedDate, margin: listCheck[x].margin, priceUser: listCheck[x].priceUser, priceChangeNow: false, part_description: '' };
        }
    }
    if ($("#suppliersDrop").val() == '') {
        toastr.warning('Seleccione Un Proveedor');
    }
    else if (listFinals.length < 1) {
        toastr.warning("Ingresa precio y fecha de almenos 1 producto");
    }
    else {
        swal({
            title: "¿Esta seguro que desea guardar el cambio de precio?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) { SaveNewPrices(listFinals); }
        });
    }
}

function SaveNewPrices(listFinals) {
    listFinals = listFinals.filter(Boolean);
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Item/ActionSaveChangePriceStores/",
        data: { itemChangePrice: listFinals, ProgramId: "PURCH012.cshtml" },
        success: function (returndate) {
            if (returndate.success == true) {
                toastr.success(returndate.responseText);
                DeleteAll();
                EndLoading();
                DeleteAll();
            }
            else {
                EndLoading();
                if (returndate.responseText == 'Tu sesion termino.') {
                    setTimeout(function () {
                        SessionFalse(returndate.responseText);
                    }, 700)
                }
                else {
                    setTimeout(function () {
                        toastr.warning(returndate.responseText + '.');
                    }, 700)
                }
            }
        },
        error: function () {
            toastr.error("Error Inesperado, Contacte a sistemas");
        }
    });
}

function Clear() {
    $('#fechaGeneral').val("");
    $("#filter").val("");
}

function DeleteAll() {
    $('#tableChangePriceBody').html("");
    listCheck = [];
    listFinals = [];
    $('#fechaGeneral').val("");
    $('#suppliersDrop').prop("selectedIndex", 0).trigger('change');
    $("#filter").val("");
}