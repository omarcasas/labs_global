﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#SuppliersDrop").select2();
    $("#idContact").select2();
    $("#BuyersDrop").select2();
    $('#ValidDatePi').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: new Date()
    });
});
var JsonContact = [];

$('#negotiationList').slimScroll({
    height: '930px'
});

$('#negotiationList').on('click', 'a', function () {
    $(this).children().removeClass("fa-chevron-down").addClass("fa-chevron-left");
    $(this).parent().siblings("li").children(".collapsed").children().removeClass("fa-chevron-letf").addClass("fa-chevron-down");
    $(this).siblings(".panel-collapse").slideToggle();
    $(this).parent().siblings("li").children(".panel-collapse").slideUp();
});

function GetNego() {
    if ($('#SuppliersDrop').val() != 0) {
        $.ajax({
            type: "POST",
            url: "/Negotiation/ActionGetSupplierNegociationBySupplierId",
            data: { "supplier_id": $('#SuppliersDrop').val() },
            success: function (returndate) {
                if (returndate.success == true) {
                    var JsonSupplierInfo = JSON.parse(returndate.JsonSupplierInfo);
                    $('#showhideFilterSupplier')[0].click();
                    $('#SupplierID').val(JsonSupplierInfo.supplier_id);
                    $('#business_nameText').html(JsonSupplierInfo.business_name);
                    $('#commercial_nameText').html(JsonSupplierInfo.commercial_name);
                    $('#rfcText').html(JsonSupplierInfo.rfc);
                    $('#addressText').html(JsonSupplierInfo.supplier_address);
                    //quitar si falla
                    $('#cityText').html(((JsonSupplierInfo.city != null && JsonSupplierInfo.city != undefined) ? JsonSupplierInfo.city : "N/A") + ' / ' + ((JsonSupplierInfo.supplier_state != null && JsonSupplierInfo.supplier_state != undefined) ? JsonSupplierInfo.supplier_state : "N/A"));
                    $('#zip_codeText').html(JsonSupplierInfo.zip_code);
                    $('#name_salesText').html(JsonSupplierInfo.name_sales);
                    $('#phone_salesText').html(JsonSupplierInfo.phone_sales);
                    $('#email_salesText').html(JsonSupplierInfo.email_sales);

                    var JsonDeta = JSON.parse(returndate.JsonNegotiatonDetail);
                    var JsonDetaDiscount = JSON.parse(returndate.JsonNegotiationDiscount);
                    var JsonDatePrice = JSON.parse(returndate.JsonDatePrice);
                    var JsonDatePoint = JSON.parse(returndate.JsonDatePoint);
                    var JsonDatePenalties = JSON.parse(returndate.JsonDatePenalties);
                    RelodList(JsonDeta, JsonDetaDiscount, JsonDatePenalties, JsonDatePoint, JsonDatePrice);
                    JsonContact = returndate.JsonContact;

                    $("#idContact").empty();
                    if (JsonContact.length > 0) {
                        document.getElementById("idContact").disabled = false;
                        selectContact = document.getElementById('idContact');
                        JsonContact.forEach(function (valor, indice, array) {
                            if (valor.flagActive) {
                                if (indice == 0) {
                                    document.getElementById("contact_name").innerHTML = valor.name + " " + valor.last_name;
                                    document.getElementById("contact_phone").innerHTML = valor.phone;
                                    document.getElementById("contact_email").innerHTML = valor.email;
                                    document.getElementById("contact_department").innerHTML = valor.department;
                                }
                                var option = document.createElement("option");
                                option.value = valor.supplier_contact_id;
                                option.innerHTML = valor.name + " " + valor.last_name + " - " + valor.department;
                                selectContact.appendChild(option);
                            }
                        });
                    } else {
                        selectContactNull = document.getElementById('idContact');
                        var option = document.createElement("option");
                        option.value = "1";
                        option.innerHTML = "Seleccione un contacto.";
                        selectContactNull.appendChild(option);
                        $('#idContact').val($("#idContact :first").val()).trigger('change');
                        $('#idContact').empty().trigger("change");
                        document.getElementById("idContact").disabled = true;
                        document.getElementById("contact_name").innerHTML = "Sin Contacto";
                        document.getElementById("contact_phone").innerHTML = "Sin Contacto";
                        document.getElementById("contact_email").innerHTML = "Sin Contacto";
                        document.getElementById("contact_department").innerHTML = "Sin Contacto";
                    }

                    $('#PanelInf').removeClass("hide");
                    $('#PanelNegotiationList').removeClass("hide");
                    $('#PanelNegotiationNew').addClass("hide");
                    $('#PanelInf').animatePanel();
                }
                else {
                    toastr.warning('' + returndate.responseText + '');
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

function ChangeIdContact() {
    var id = document.getElementById("idContact").value;
    var result = JsonContact.filter(c => c.supplier_contact_id == id);
    var r = result.length;
    if (r == 0) {
        document.getElementById("idContact").disabled = true;
        document.getElementById("contact_name").innerHTML = "Sin Contacto";
        document.getElementById("contact_phone").innerHTML = "Sin Contacto";
        document.getElementById("contact_email").innerHTML = "Sin Contacto";
        document.getElementById("contact_department").innerHTML = "Sin Contacto";
    } else {
        document.getElementById("contact_name").innerHTML = result[0].name + " " + result[0].last_name;
        document.getElementById("contact_phone").innerHTML = result[0].phone;
        document.getElementById("contact_email").innerHTML = result[0].email;
        document.getElementById("contact_department").innerHTML = result[0].department;
    }
}

function AddNegotiaton() {
    ClearFormInsert();
    $('#PanelNegotiationList').addClass("hide");
    $('#PanelNegotiationNew').removeClass("hide");
    $('#PanelNegotiationNew').animatePanel();
}

function ReturnToList() {
    ClearFormInsert();
    $('#PanelNegotiationNew').addClass("hide");
    $('#PanelNegotiationList').removeClass("hide");
    $('#PanelNegotiationList').animatePanel();
    $('.Coll').slideUp();
    $('.fa-chevron-left').removeClass("fa-chevron-letf").addClass("fa-chevron-down");
}

$(function () {
    $('#ValidDatePi').datepicker();
});

$("#formInsertNegotiaton").validate({
    rules: {
        user_id: {
            required: true,
            minlength: 4
        },
        negotiation_type: {
            required: true
        },
        payment_conditions: {
            required: true,
            minlength: 4
        },
        supplier_references: {
            required: true,
            minlength: 4
        },
        delivery_points: {
            required: true
        }
        ,
        general_notes: {
            required: true,
            minlength: 4
        },
        negotiatied_discounts: {
            required: true
        },
        conditions_prices: {
            required: true
        },
        penalties: {
            required: true
        },
        quality_level: {
            required: true,
            minlength: 4
        }
        ,
        packaging_type: {
            required: true,
            minlength: 4
        }
        ,
        valid_until: {
            required: true,
            minlength: 4
        }

    },
    messages: {
        user_id: {
            required: true,
            minlength: 4
        },
        negotiation_type: {
            required: "El Campo no puede ser nulo"
        },
        payment_conditions: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        },
        supplier_references: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        }
        ,
        delivery_points: {
            required: "El Campo no puede ser nulo"
        }
        ,
        general_notes: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        },
        negotiatied_discounts: {
            required: "El Campo no puede ser nulo"
        },
        conditions_prices: {
            required: "El Campo no puede ser nulo"
        },
        penalties: {
            required: "El Campo no puede ser nulo"
        },
        quality_level: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        }
        ,
        packaging_type: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        }
        ,
        valid_until: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        }
    },
    submitHandler: function (form) {
        if ($('#BuyersDrop').val() != 0) {
            swal({
                title: "¿Esta seguro?",
                text: "De Registrar la negociación!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Continuar",
                cancelButtonText: "Cancelar"
            },
                function (isConfirm) {
                    if (isConfirm) {
                        StartLoading();
                        $.ajax({
                            type: "POST",
                            url: "/Negotiation/ActionInsertNegotiation",
                            data: { "supplier_id": $('#SupplierID').val(), "user_id": $('#BuyersDrop').val(), "negotiation_type": $('#negotiation_type').val(), "payment_conditions": $('#payment_conditions').val(), "supplier_references": $('#supplier_references').val(), "delivery_points": $('#PointsDrop').val(), "general_notes": $('#general_notes').val(), "negotiatied_discounts": $('#DiscountDrop').val(), "conditions_prices": $('#PricesDrop').val(), "penalties": $('#PenaltiesDrop').val(), "quality_level": $('#quality_level').val(), "packaging_type": $('#packaging_type').val(), "valid_until": $('#valid_until').val() },
                            success: function (returndate) {
                                if (returndate.success == true) {
                                    var JsonDeta = JSON.parse(returndate.JsonNegotiatonDetail);
                                    var JsonDetaDiscount = JSON.parse(returndate.JsonNegotiationDiscount);
                                    var JsonDatePrice = JSON.parse(returndate.JsonDatePrice);
                                    var JsonDatePoint = JSON.parse(returndate.JsonDatePoint);
                                    var JsonDatePenalties = JSON.parse(returndate.JsonDatePenalties);
                                    RelodList(JsonDeta, JsonDetaDiscount, JsonDatePenalties, JsonDatePoint, JsonDatePrice);
                                    toastr.success('' + returndate.responseText + '');
                                    EndLoading();
                                    ReturnToList();
                                }
                                else {
                                    toastr.warning('' + returndate.responseText + '');
                                    EndLoading();
                                }
                            },
                            error: function (returndate) {
                                toastr.error('Error inesperado contactar a sistemas.');
                                EndLoading();
                            }
                        });
                    }
                });
        }
        else {
            toastr.warning('Seleccione un Comprador');
        }
    }
});

function RelodList(s, JsonDateDiscount, JsonDatePenalties, JsonDatePoint, JsonDatePrice) {
    $("#negotiationList").empty();
    $.each(s, function (i, v) {
        $("#negotiationList").append("<li class='list-group-item'> <a class='collapsed'> <i class='fa fa-chevron-down pull-right text-muted'></i> Negociacion #" + v.nd_id + " </a> <div class='panel-collapse collapse Coll'> <p></p> <div class='panel-body'><form action='/Home/Register5' id='form" + i + "' method='post' name='form" + i + "' novalidate='novalidate'>  <input type='hidden' id='nd_id" + i + "' value='" + v.nd_id + "'><input type='hidden' id='nm_id" + i + "' value='" + v.nm_id + "'>  <div class='form-group col-lg-12'> <label for='negotiation_type' title='Tipo de Negociación' data-toggle='tooltip' data-placement='right'>Tipo</label><select class='form-control' id='negotiation_type" + i + "' name='negotiation_type' style='width: 100%'><option value=''>Seleccione una opción</option><option value='Unidireccional'>Unidireccional</option><option value='Bidireccional'>Bidireccional</option></select></div> <div class='form-group col-lg-12'> <label for='payment_conditions' title='Condiciones de pago' data-toggle='tooltip' data-placement='right'>Condiciones de pago</label> <input class='form-control' id='payment_conditions" + i + "' name='payment_conditions' type='text' value='" + v.payment_conditions + "'> </div> <div class='form-group col-lg-6'> <label for='supplier_references' title='Referencias del proveedor' data-toggle='tooltip' data-placement='right'>Referencias</label> <input class='form-control' id='supplier_references" + i + "' name='supplier_references' type='text' value='" + v.supplier_references + "'> </div> <div class='form-group col-lg-6'> <label for='delivery_points' title='Lugar donde entregara el proveedor' data-toggle='tooltip' data-placement='right'>Punto de entrega</label> <select id='delivery_points" + i + "' class='form-control' name='delivery_points' style='width: 100%'></select> </div> <div class='form-group col-lg-12'> <label for='general_notes' title='Notas generales' data-toggle='tooltip' data-placement='right'>Nota</label> <input class='form-control' id='general_notes" + i + "' name='general_notes' type='text' value='" + v.general_notes + "'> </div> <div class='form-group col-lg-6'> <label for='negotiatied_discounts' title='Descuentos de negociación' data-toggle='tooltip' data-placement='right'>Descuentos de negociación</label> <select id='negotiatied_discounts" + i + "' class='form-control' name='negotiatied_discounts' style='width: 100%'></select></div> <div class='form-group col-lg-6'> <label for='conditions_prices' title='Condiciones de precio' data-toggle='tooltip' data-placement='right'>Condiciones de precio</label> <select id='conditions_prices" + i + "' class='form-control' name='conditions_prices' style='width: 100%'></select> </div> <div class='form-group col-lg-6'> <label for='penalties' title='Sanciones' data-toggle='tooltip' data-placement='right'>Sanciones</label> <select id='penalties" + i + "' class='form-control' name='penalties' style='width: 100%'></select> </div> <div class='form-group col-lg-6'> <label for='quality_level' title='Nivel de calidad del producto' data-toggle='tooltip' data-placement='right'>Nivel de Calidad</label> <input class='form-control' id='quality_level" + i + "' name='quality_level' type='text' value='" + v.quality_level + "'> </div><div class='form-group col-lg-6'> <label for='valid_until' title='Fecha de validez' data-toggle='tooltip' data-placement='right'>Válido hasta</label> <div class='input-group date' id='ValidDatePi" + i + "'> <span class='input-group-addon'> <span class='fa fa-calendar'></span> </span> <input class='form-control' data-val='true' data-val-date='The field valid_until must be a date.' data-val-required='The valid_until field is required.' id='valid_until" + i + "' name='valid_until' type='text' value='" + v.valid_until + "'> </div> </div> <div class='form-group col-lg-6'> <div class='btn-group'> <label title='Subir Documento' data-toggle='tooltip' data-placement='top'> <input id='UpdateFile" + i + "' type='file' style='display: none;' onchange='UpdateDocument(this,nm_id" + i + ",nd_id" + i + ");' accept='application/pdf,application/vnd.ms-excel,application/msword,.xlsx' /> <i class='fa fa-cloud-upload btn btn-default btn-xl'></i> </label> </div> </div> <div class='form-group col-lg-6'> <div class='btn-group'> <a title='Descargar Documento' data-toggle='tooltip' data-placement='top' onclick='DownloadDoc(nm_id" + i + ",nd_id" + i + ");'><i class='fa fa-cloud-download btn btn-xl btn-default'></i></a> </div> </div><div class='form-group col-lg-12'> <div class='text-right'> <button type='button' class='btn btn-default' onclick='ReturnToList();'><i class='fa fa-close'></i> Cancelar</button> <button class='btn btn-warning'><i class='fa fa-save'></i> Editar</button> </div> </div> </div></form> </div> </li>");
        $("#form" + i + "").validate({
            rules: {
                user_id: {
                    required: true,
                    minlength: 4
                },
                negotiation_type: {
                    required: true
                },
                payment_conditions: {
                    required: true,
                    minlength: 4
                },
                supplier_references: {
                    required: true,
                    minlength: 4
                }
                ,
                delivery_points: {
                    required: true
                }
                ,
                general_notes: {
                    required: true,
                    minlength: 4
                },
                negotiatied_discounts: {
                    required: true
                },
                conditions_prices: {
                    required: true
                },
                penalties: {
                    required: true
                },
                quality_level: {
                    required: true,
                    minlength: 4
                }
                ,
                packaging_type: {
                    required: true,
                    minlength: 4
                }
                ,
                valid_until: {
                    required: true,
                    minlength: 4
                }

            },
            messages: {
                user_id: {
                    required: true,
                    minlength: 4
                },
                negotiation_type: {
                    required: "El Campo no puede ser nulo"
                },
                payment_conditions: {
                    required: "El Campo no puede ser nulo",
                    minlength: "Favor de ingresar mas de 4 caracteres",
                },
                supplier_references: {
                    required: "El Campo no puede ser nulo",
                    minlength: "Favor de ingresar mas de 4 caracteres",
                }
                ,
                delivery_points: {
                    required: "El Campo no puede ser nulo"
                }
                ,
                general_notes: {
                    required: "El Campo no puede ser nulo",
                    minlength: "Favor de ingresar mas de 4 caracteres",
                },
                negotiatied_discounts: {
                    required: "El Campo no puede ser nulo"
                },
                conditions_prices: {
                    required: "El Campo no puede ser nulo"
                },
                penalties: {
                    required: "El Campo no puede ser nulo"
                },
                quality_level: {
                    required: "El Campo no puede ser nulo",
                    minlength: "Favor de ingresar mas de 4 caracteres",
                }
                ,
                packaging_type: {
                    required: "El Campo no puede ser nulo",
                    minlength: "Favor de ingresar mas de 4 caracteres",
                }
                ,
                valid_until: {
                    required: "El Campo no puede ser nulo",
                    minlength: "Favor de ingresar mas de 4 caracteres",
                }
            },
            submitHandler: function (form) {
                swal({
                    title: "¿Esta seguro?",
                    text: "De Editar la negociación!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Continuar",
                    cancelButtonText: "Cancelar"
                }, function (isConfirm) {
                    if (isConfirm) {
                        StartLoading();
                        $.ajax({
                            type: "POST",
                            url: "/Negotiation/ActionUpdateNegotiation",
                            data: { "supplier_id": $('#SupplierID').val(), "nm_id": $('#nm_id' + i + '').val(), "nd_id": $('#nd_id' + i + '').val(), "user_id": $('#user_id' + i + '').val(), "negotiation_type": $('#negotiation_type' + i + '').val(), "payment_conditions": $('#payment_conditions' + i + '').val(), "supplier_references": $('#supplier_references' + i + '').val(), "delivery_points": $('#delivery_points' + i + '').val(), "general_notes": $('#general_notes' + i + '').val(), "negotiatied_discounts": $('#negotiatied_discounts' + i + '').val(), "conditions_prices": $('#conditions_prices' + i + '').val(), "penalties": $('#penalties' + i + '').val(), "quality_level": $('#quality_level' + i + '').val(), "packaging_type": $('#packaging_type' + i + '').val(), "valid_until": $('#valid_until' + i + '').val() },
                            success: function (returndate) {
                                if (returndate.success == true) {
                                    var JsonDeta = JSON.parse(returndate.JsonNegotiatonDetail);
                                    var JsonDetaDiscount = JSON.parse(returndate.JsonNegotiationDiscount);
                                    var JsonDatePrice = JSON.parse(returndate.JsonDatePrice);
                                    var JsonDatePoint = JSON.parse(returndate.JsonDatePoint);
                                    var JsonDatePenalties = JSON.parse(returndate.JsonDatePenalties);
                                    RelodList(JsonDeta, JsonDetaDiscount, JsonDatePenalties, JsonDatePoint, JsonDatePrice);
                                    EndLoading();
                                    toastr.success('Negociación editada con éxito!');

                                }
                                else {
                                    toastr.warning('' + returndate.responseText + '');
                                    EndLoading();
                                }
                            },
                            error: function (returndate) {
                                toastr.error('Error inesperado contactar a sistemas.');
                                EndLoading();
                            }
                        });
                    }
                });
            }
        });
        $('#valid_until' + i + '').datepicker({
            autoclose: true,
            todayHighlight: true,
            startDate: new Date()
        });

        FillDropdown("negotiatied_discounts" + i + "", JsonDateDiscount);
        $("#negotiatied_discounts" + i + "").val(v.negotiatied_discounts).trigger('change');
        FillDropdown("penalties" + i + "", JsonDatePenalties);
        $("#penalties" + i + "").val(v.penalties).trigger('change');
        $("#negotiation_type" + i + "").val(v.negotiation_type).trigger('change');
        FillDropdown("conditions_prices" + i + "", JsonDatePrice);
        $("#conditions_prices" + i + "").val(v.conditions_prices).trigger('change');
        FillDropdown("delivery_points" + i + "", JsonDatePoint);
        $("#delivery_points" + i + "").val(v.delivery_points).trigger('change');
    });
}

function UpdateDocument(s, nm_id, nd_id) {
    var fileInput = s;
    if (fileInput.files.length == 0) {
        toastr.warning("No selecciono ningun documento.");
    }
    else {
        var idxDot = fileInput.files[0].name.lastIndexOf(".") + 1;
        var extFile = fileInput.files[0].name.substr(idxDot, fileInput.files[0].name.length).toLowerCase();//Obtenemos extension de archivo
        if (extFile == "pdf" || extFile == "doc" || extFile == "xlsx") {
            var selectedFile = ($(s))[0].files[0];
            if (selectedFile) {
                var FileSize = 0;
                if (selectedFile.size > 1048576) {
                    FileSize = Math.round(selectedFile.size * 100 / 1048576) / 100 + " MB";
                }
                else if (selectedFile.size > 1024) {
                    FileSize = Math.round(selectedFile.size * 100 / 1024) / 100 + " KB";
                }
                else {
                    FileSize = selectedFile.size + " Bytes";
                }
                if (selectedFile.size < 5242880)// Menor a 5MB
                {
                    var formdata = new FormData();
                    formdata.append(fileInput.files[0].name, fileInput.files[0]);
                    formdata.append("nm_id", $(nm_id).val());
                    formdata.append("nd_id", $(nd_id).val());
                    formdata.append("supplier_id", $('#SupplierID').val());
                    formdata.append("file_name_ext", selectedFile.name);
                    $.ajax({
                        type: "POST",
                        url: "/Negotiation/ActionUploadNegotiatonDocument",
                        data: formdata,
                        cache: false,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (returndate) {
                            if (returndate.success == true) {
                                toastr.success('' + returndate.responseText + '');
                                var JsonDeta = JSON.parse(returndate.JsonNegotiatonDetail);
                                RelodList(JsonDeta);
                                EndLoading();
                            }
                            else {
                                toastr.warning('' + returndate.responseText + '');
                                EndLoading();
                            }
                        },
                        error: function () {
                            toastr.error('Error Inesperado');
                            EndLoading();
                        }
                    });
                }
                else {
                    toastr.warning('La imagen debe ser menor a 5MB');
                }
            }
        } else {
            toastr.warning("Solo se admiten archivos pdf/doc/xlsx. Intentar con otro archivo");
        }

    }
}

function DownloadDoc(nm_id, nd_id) {
    $.ajax({
        type: "GET",
        url: "/Negotiation/ActionDownloadNegotiationDocument",
        data: { "supplier_id": $('#SupplierID').val(), "nm_id": $(nm_id).val(), "nd_id": $(nd_id).val() },
        success: function (returndate) {
            if (returndate.success == true) {
                var sampleArr = base64ToArrayBuffer(returndate.responseText);
                saveByteArray(returndate.fileName, sampleArr);
            }
            else {
                toastr.warning('' + returndate.responseText + '');
            }
        },
        error: function (returndate) {
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

function saveByteArray(reportName, byte) {
    var blob = new Blob([byte]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName;
    link.download = fileName;
    link.click();
}

function ClearFormInsert() {
    $("#formInsertNegotiaton")[0].reset();
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        for (var i in vData) {
            vItems.push('<option value="' + vData[i].Value + '">' + vData[i].Text + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val(0).trigger('change');

    }
    else {
        var vItems = [];
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val(0).trigger('change');
    }
}

function AddCatalogue(s) {
    switch (s) {
        case "Points":
            $("#ModalTitle").html('<i class="fa fa-plus"></i> Agregar Punto de Entrega');
            $("#ModalDescription").html('Formulario para registrar un nuevo punto de entrega.');
            $('#ModalAdd').modal('show');
            $('#MaCodeType').val(s);
            break;
        case "Discounts":
            $("#ModalTitle").html('<i class="fa fa-plus"></i> Agregar Descuento');
            $("#ModalDescription").html('Formulario para registrar un nuevo descuento.');
            $('#ModalAdd').modal('show');
            $('#MaCodeType').val(s);
            break;
        case "prices":
            $("#ModalTitle").html('<i class="fa fa-plus"></i> Agregar Precio');
            $("#ModalDescription").html('Formulario para registrar una nueva condición de precio.');
            $('#ModalAdd').modal('show');
            $('#MaCodeType').val(s);
            break;
        case "penalties":
            $("#ModalTitle").html('<i class="fa fa-plus"></i> Agregar Sanción');
            $("#ModalDescription").html('Formulario para registrar una nueva Sanción.');
            $('#ModalAdd').modal('show');
            $('#MaCodeType').val(s);
            break;
    }
}

$("#formAddCatalogue").validate({
    rules: {
        vkey: {
            required: true,
            minlength: 3,
            maxlength: 50
        },
        description: {
            required: true,
            minlength: 3,
            maxlength: 100
        }
    },
    messages: {
        vkey: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 3 caracteres",
            maxlength: "Favor de ingresar menos de 50 caracteres"
        },
        description: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 3 caracteres",
            maxlength: "Favor de ingresar menos de 100 caracteres"
        }
    },
    submitHandler: function (form) {
        swal({
            title: "¿Esta seguro?",
            text: "De Registrar!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                $.ajax({
                    type: "POST",
                    url: "/Negotiation/ActionInsertMaCodeCatalogue",
                    data: { "vkey": $('#vkey').val(), "description": $(description).val(), "used": $(MaCodeType).val() },
                    success: function (returndate) {
                        EndLoading();
                        if (returndate.success == true) {
                            var JsonDetaDiscount = JSON.parse(returndate.JsonNegotiationDiscount);
                            var JsonDatePrice = JSON.parse(returndate.JsonDatePrice);
                            var JsonDatePoint = JSON.parse(returndate.JsonDatePoint);
                            var JsonDatePenalties = JSON.parse(returndate.JsonDatePenalties);
                            FillDropdown("DiscountDrop", JsonDetaDiscount);
                            FillDropdown("PenaltiesDrop", JsonDatePenalties);
                            FillDropdown("PricesDrop", JsonDatePrice);
                            FillDropdown("PointsDrop", JsonDatePoint);
                            $('#ModalAdd').modal('hide');
                        }
                        else {
                            toastr.warning('' + returndate.responseText + '');
                        }
                    },
                    error: function (returndate) {
                        EndLoading();
                        toastr.error('Error inesperado contactar a sistemas.');
                    }
                });
            }
        });
    }
});