﻿listCheck = [];
listItem = [];
list = [];
$('.divAdd').prop("disabled", true);
window.setTimeout(function () { $("#fa").click(); }, 1000);

$("#SuppliersDrop").val([]);
$("#ItemsDrop").val([]);
$("#SuppliersDrop").select2({
    minimumInputLength: 2,
    formatInputTooShort: function () {
        return "Busque un proveedor";
    },
    matcher: function (term, text, option) {
        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 ||
            option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
    }
});

$("#ItemsDrop").select2({
    minimumInputLength: 2,
    formatInputTooShort: function () {
        return "Busque un proveedor";
    },
    matcher: function (term, text, option) {
        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 ||
            option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
    }
});

$("#tipo").select2();
$('.ValidDatePi').datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: new Date()
});
$('#etxtBasePrice').val('');
$('#selectCurrency').val('');
const date = new Date();
$('#ValidDate').val(`${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`);

var table = $('#tbl_items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ Registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [

            { data: 'item_number' },
            { data: 'item_name' },
            { data: 'supplier_name' },
            { data: (m) => `<button class='btn_table btn btn-xs btn-outline btn-danger'>Eliminar</button>` }
        ]
});

function OrderList() {
    if (!$('#SuppliersDrop').val()) {
        $('.divAdd').prop("disabled", true);
        return;
    }
    if ($('#SuppliersDrop').val().length > 1) {
        $('.divAdd').prop("disabled", true);
        return;
    } else {
        $('.divAdd').prop("disabled", false);
    }
    let list;
    try {
        list = $('#SuppliersDrop').val().map(e => parseInt(e, 10));
    } catch (e) {
        toastr.error('Error al obtener informacion de proveedor, contacte a sistemas');
        return;
    }
    $.ajax({
        url: "/Supplier/ActionGetItemsBySupplierCodeList",
        type: "POST",
        data: { "supplier_code_list": list },
        success: function (returndates) {
            if (returndates.success === true) {
                const listItem = returndates.Json;
                table.clear();
                table.rows.add(jQuery(listItem));
                table.columns.adjust().draw();
            } else {
                if (returndates.responseText === "Termino tu sesion.") {
                    SessionFalse(returndates.responseText);
                }
                else {
                    toastr.warning('Proveedor No Tiene Productos.');
                }
                table.clear();
            }
        },
        error: function (returndates) {
            console.log(returndates);
            toastr.error('Error desconocido, contacte a sistemas');
        }
    });

    $('tbody').on('click', '.btn_table', function () {
        const row = table.row($(this).parents('tr'));
        const data = row.data();
        swal({
            title: "¿Esta seguro?",
            text: "Se eliminara la relacion del proveedor con el producto.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                axios.post("/Supplier/ActionDisableItemSupplier", { Supplier: data.item_supplier_id })
                    .then(data => {
                        if (data.data === "SF")
                            SessionFalse(sesion);
                        else if (data.data) {
                            row.remove().draw(false);
                            toastr.success("Se desactivo correctamente");
                        } else
                            toastr.warning("Error al descativar proveedor");
                    })
                    .catch(error => {
                        console.log(error);
                        toastr.error('Error inesperado contactar a sistemas.');
                    }).then(() => { EndLoading(); });
            }
            else { }
        });
    });
}

function addProduct() {
    let sups = $('#SuppliersDrop').val();
    let pro = $('#ItemsDrop').val();
    if (!sups || sups.length !== 1 || !pro) return;

    let sup = sups[0];
    const basePrice = $('#etxtBasePrice').val();
    const currency = $('#selectCurrency').val();
    const valid = $('#ValidDate').val();
    if (!basePrice || basePrice <= 0) {
        toastr.warning("El costo del producto debe ser mayor de $0");
        return;
    }
    if (!currency) {
        toastr.warning("Es necesario seleccionar un tipo de moneda");
        return;
    }
    if (!valid) {
        toastr.warning("Es necesario seleccionar una fecha de applicación");
        return;
    }
    swal({
        title: "¿Esta seguro?",
        text: "Se agregara el producto a los proveedores seleccionados.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            try {
                let r = axios.post("/Item/ActionAssingSupplier/",
                    { "part_number": pro, "supplier_id": sup });

                if (!r.data.success) {
                    toastr.warning(r.data.responseText);
                    return;
                }
                const data = {
                    "part_number": pro,
                    "BasePriceId": 0,
                    "SupplierPartNumberId": r.data.supplier_part_number,
                    "BasePrice": basePrice,
                    "Currency": currency,
                    "ValidFrom": valid,
                    "program_id": 'PURCH028.cshtml'
                };
                let r2 = axios.post("/Item/ActionUpdateBasePrice/", data);
                if (!r2.data.success) {
                    toastr.warning("El producto se guardo correctamente pero no se pudo guardar costo:");
                } else {
                    toastr.success("Producto y costo registrado correctamente");
                    $('#etxtBasePrice').val('');
                    $('#selectCurrency').val('');
                    $("#ItemsDrop").select2("val", "");
                    const date = new Date();
                    $('#ValidDate').val(`${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`);
                }
            } catch (e) {
                console.log(e);
            } finally {
                OrderList();
                EndLoading();
            }
        }
        else { }
    });
}