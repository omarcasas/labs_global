﻿var CurrencyList = [];
var count = 1;
const formatNumber = z => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(z);
const formatArray = y => ({ Id: y.Id, Price: formatNumber(y.Price), Currency: y.Currency, CurrencyDate: y.CurrencyDate, currency_type: y.currency_type, User: y.User });

$(document).ready(function () {
    $(document).on("keypress", '.Numeric', function (event) {
        var keyValue = event.key;
        if (keyValue.match(/^(\d*\.?\d*)$/))
            return true;
        else
            return false;
    });

    GetAllCurrency()
    $('#typeReference').select2();
    $('#supplier2').select2();

    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#datetimepicker').datepicker({
        autoclose: true,
        format: "mm/dd/yyyy",
        startDate: moment().format("MM/DD/YYYY"),
        endDate: "1/1/2100",
        todayHighlight: true,
        language: 'es'
    });

    $('#datetimepicker2').datepicker({
        autoclose: true,
        format: "mm/dd/yyyy",
        startDate: "1/1/1980",
        endDate: "1/1/2200",
        todayHighlight: true,
        language: 'es'
    });

    $('#datetimepicker3').datepicker({
        autoclose: true,
        format: "mm/dd/yyyy",
        startDate: "1/1/1980",
        endDate: "1/1/2200",
        todayHighlight: true,
        language: 'es'
    });

    $('#assignedList').find('input[name="available"]').iCheck({
        checkboxClass: 'icheckbox_square-green'
    });

    $('.iCheck-helper:first').on('click', function () {
        if ($("input[type='checkbox']:checked:first").is(':checked') && count == 1) {
            $('input[name="available"]').iCheck('check');
            count = 2;
        }
        else {
            count = 1;
            $('input[name="available"]:checked').iCheck('uncheck');
        }
    });

    $('.iCheck-helper:gt(0)').on('click', function () {
        if ($("input[name='available']:gt(0)").length == $("input[name='available']:gt(0):checked").length) {
            $('input[name="available"]:first').iCheck('check');
            count = 2;
        }
        else {
            count = 1;
            $('input[name="available"]:first').iCheck('uncheck');
        }
    });

    Save();
});

$('#assignedList').slimScroll({
    height: '200px'
});

table = $("#tableCurrency").DataTable({
    "order": [[2, "desc"]],
    "autoWidth": false,
    "searching": true,
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'copy', className: 'btn-sm' },
        { extend: 'csv', title: 'TipoDeCombioComercial', className: 'btn-sm' },
    ],
    columns: [
        { data: 'Price' },
        { data: 'Currency' },
        { data: 'CurrencyDate' },
        { data: 'User' },
        { data: 'currency_type' },
        { data: null }
    ],
    columnDefs: [
        {
            targets: [2],
            render: function (data, type, full, meta) {
                return moment(data).lang('es').format('L');
            }
        },
        {
            targets: [5],
            render: function (data, type, full, meta) {
                if (data.User == 'Sistema')
                    return `<button class="btn btn-xs btn-outline btn-info" onclick="ShowStore('${data.Id}', '${data.Price}', '${data.Currency}', '${data.CurrencyDate}', '${data.User}', '0')"><i class="fa fa-cart-arrow-down"></i>  Tiendas</button>`;
                else
                    return `<button class="btn btn-xs btn-outline btn-info" onclick="ShowStore('${data.Id}', '${data.Price}', '${data.Currency}', '${data.CurrencyDate}', '${data.User}', '1')"><i class="fa fa-cart-arrow-down"></i>  Tiendas</button>`;
            }
        }
    ],
});

function ShowStore(folio, Price, Currency, CurrencyDate, User, TypeExchange) {
    $.ajax({
        type: "GET",
        url: "/ExchangeCurrency/ActionGetExchangeByIdSites",
        data: { "exchange_id": folio, "type": TypeExchange },
        success: function (response) {
            if (response) {
                var ul1 = document.getElementById("listStores1");
                var ul2 = document.getElementById("listStores2");
                var ul3 = document.getElementById("listStores3");
                ul1.innerHTML = '';
                ul2.innerHTML = '';
                ul3.innerHTML = '';
                var l = response.data.length;
                for (var i = 0; i < 11; i++) {
                    var li1 = document.createElement("li");
                    if (typeof response.data[i] !== 'undefined') {
                        li1.appendChild(document.createTextNode(response.data[i].SiteName));
                        ul1.appendChild(li1);
                    }
                }
                for (var e = 11; e < 21; e++) {
                    var li2 = document.createElement("li")
                    if (typeof response.data[e] !== 'undefined') {
                        li2.appendChild(document.createTextNode(response.data[e].SiteName));
                        ul2.appendChild(li2);
                    }
                }

                for (var w = 21; w < l; w++) {
                    var li3 = document.createElement("li");
                    if (typeof response.data[w] !== 'undefined') {
                        li3.appendChild(document.createTextNode(response.data[w].SiteName));
                        ul3.appendChild(li3);
                    }
                }

                $('#ExchangePrice').html(Price);
                $('#ExchangeCurrency').html(Currency);
                $('#ExchangeCurrencyDate').html(moment(CurrencyDate).lang('es').format('L'));
                $('#ExchangeUser').html(User);
                $('#showStores').modal('show');
            } else {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}

var validator = $("#form").validate({
    rules: {
        dollar: {
            number: true,
            required: true,
            min: 1,
            max: 100,

        },
    },
    messages: {
        dollar: {
            number: "Ingresa numeros por favor",
            required: "El Campo no puede ser nulo",
            min: "Favor de poner un numero positivo",
            max: "Favor de poner un numero razonable."
        },
    }
});

function SaveTypeCurrency() {
    sitesL = [];

    $.each($("input[name='available']:checked"), function () {
        sitesL += $(this).val() + ',';
    });

    if (sitesL.length == 0) {
        toastr.warning('Selecciona al menos 1 Tienda.');
        $("#ItemsDrop").val("");
        $("#select2-chosen-1").html("Buscar un Producto.");
        return false;
    }

    axios.post("/ExchangeCurrency/ActionPostTypeCurrency", { Price: $("#dollar").val(), Date: $("#dateFin").val(), Sites: sitesL })
        .then(function (data) {
            if (data.data === "SF") {
                window.location.href = 'http://' + window.location.host + '/Home/Login';
            }
            else if (data.data.success) {
                toastr.success("Se guardo con exito el tipo de cambio")
                GetAllCurrency();
                $('input[name="available"]:checked').iCheck('uncheck');
                $("#dollar").val("")
                $("#dateFin").val("")
            }
            else {
                toastr.error("Contacte a sistemas. Error al actualizar.")
            }
        })
        .catch(function (error) {
            console.log(error.data)
            toastr.error("Contacta con sistemas")
        })

}

function SaveButton() {
    sitesL = [];
    $.each($("input[name='available']:checked"), function () {
        sitesL += $(this).val() + ',';
    });
    if ($("#dateFin").val() != null && $("#dateFin").val().trim() != "") {
        if ($("#dollar").val() > 0 && $("#dollar").val() < 100) {
            if (isDecimal.test($("#dollar").val())) {
                if (sitesL.length != 0) {
                    swal({
                        title: "Desea guardar el tipo de cambio?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm)
                            SaveTypeCurrency();
                    });
                } else {
                    toastr.warning('Selecciona al menos 1 Tienda.');
                }
            }
            else {
                toastr.error("Ingrese un tipo de cambio correcto.");
                $('#dollar').focus();
            }
        } else {
            toastr.warning('Ingrese una tipo de cambio razonable.');
        }
    }
    else {
        toastr.error('La fecha no puede dejar la fecha vacia.');
        $('#dateFin').focus();
    }
}

function Save() {
    isDecimal = /^\d+\.?\d{0,4}$/;
    console.log("Save()");
    $('#buttonSave').click(
        function (e) {
            console.log("function (e)");
            e.preventDefault();
            if ($("#dateFin").val() != null && $("#dateFin").val().trim() != "") {
                console.log("(#dateFin).val()");
                if (validator.element("#dollar")) {
                    if (isDecimal.test($("#dollar").val())) {
                        swal({
                            title: "Desea guardar el tipo de cambio?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Si",
                            cancelButtonText: "No",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }, function (isConfirm) {
                            if (isConfirm)
                                SaveTypeCurrency();
                        });
                    }
                    else {
                        toastr.error("Formato invalido limite 4 decimales");
                    }
                }
            }
            else {
                toastr.error('La fecha no puede estar nula');
            }
        })
}

function GetAllCurrency() {
    axios.get("/ExchangeCurrency/ActionGetAllCurrency")
        .then(function (data) {
            CurrencyList = data.data.map(x => formatArray(x));
            table.clear().draw();
            table.rows.add(jQuery(CurrencyList));
            table.columns.adjust().draw();
        }).catch(function (error) {
            console.log(error.data);
        })
}

function DateValidateionShow() {
    if ($("#dateInit1").val().trim() != "" && $("#dateFin1").val().trim() != "" && $("#dateFin1").val() != null && $("#dateInit1").val() != null) {
        var dateInit = moment($("#dateInit1").val());
        var dateFin = moment($("#dateFin1").val())
        var type = $('#typeReference').val();
        var siteCode = $('#supplier2').val();
        if (dateInit <= dateFin) {
            axios.post("/ExchangeCurrency/ActionGetTypeExchangeCurrency", { DateInit: $("#dateInit1").val(), DateFin: $("#dateFin1").val(), site_code: siteCode, type: type, })
                .then(function (data) {
                    if (!data.data.status) {
                        SessionFalse("Se terminó su sesion.");
                    } else if (data.data.status) {
                        CurrencyList = data.data.model.map(x => formatArray(x));
                        table.clear().draw();
                        table.rows.add(jQuery(CurrencyList));
                        table.columns.adjust().draw();
                    } else if (!data.data.status) {
                        toastr.warning("No se encontraron precios en ese tiempo")
                    }
                    else {
                        toastr.warning("No se encontraron precios en ese tiempo")
                    }
                }).catch(function (error) {
                    console.log(error.data)
                    toastr.error("Contacta con sistemas")
                })
        }
        else {
            toastr.error('La primera fecha no puede ser mayor a la segunda fecha');
        }
    } else {
        toastr.warning("Ingrese ambas fechas.")
    }
}

function DateValidation() {
    if ($("#dateInit1").val().trim() != "" && $("#dateFin1").val().trim() != "" && $("#dateFin1").val() != null && $("#dateInit1").val() != null) {
        var dateInit = moment($("#dateInit1").val());
        var dateFin = moment($("#dateFin1").val());
        var type = $('#typeReference').val();
        var siteCode = $('#supplier2').val();
        if (dateInit <= dateFin) {
            StartLoading();
            axios.post("/ExchangeCurrency/ActionGetDateCurrency", { DateInit: $("#dateInit1").val(), DateFin: $("#dateFin1").val(), Type: type, siteCode: siteCode })
                .then(function (date) {
                    EndLoading();
                    if (date.data === "SF") {
                        SessionFalse("Se terminó su sesion.");
                    }
                    else if (date.data.success) {
                        $("#modalReferenceBody").html(`<iframe src='data:application/pdf;base64,${date.data.responseText}' width='100%' height='550px'></iframe>`);
                        $("#modalReference").modal('show');
                    }
                    else if (!date.data.success) {
                        toastr.warning("No se encontraron precios en ese tiempo");
                    }
                    else {
                        toastr.warning("No se encontraron precios en ese tiempo");
                    }
                }).catch(function (error) {
                    console.log(error.data);
                    toastr.error("Contacta con sistemas");
                })
        }
        else {
            toastr.error('La primera fecha no puede ser mayor a la segunda fecha');
        }
    } else {
        toastr.warning("Ingrese ambas fechas.")
    }
}

function ClearForm() {
    $("#dateInit1").val("");
    $("#dateFin1").val("");
    $("#iframes").html("");
}