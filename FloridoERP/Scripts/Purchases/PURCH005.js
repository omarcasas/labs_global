﻿function onModalOpen() {
    $('#etxtBasePriceAdd').val('');
    $('#selectCurrencyAdd').val('');
    $("#SuppliersDrop").select2("val", "");
    const date = new Date();
    $('#ValidDateAdd').val(`${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`);
    $("#ModalAddSupplier").modal('show');
}

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money); //Formato de Decimales
const sesion = "Se termino su sesion.";
var part_number = "";
var validate_part_number_only_codes_packages = false;
var validate_part_number_forced_90 = false;
const isprice90 = /^\d+\.90{0,3}$/;
var listCostProduct = [];

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);


    $("#SuppliersDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busca un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 ||
                option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });
    $(document).on("keypress", '.Numeric', function (event) {
        var keyValue = event.key;
        if (keyValue.match(/^(\d*\.?\d*)$/))
            return true;
        else
            return false;
    });


    var today = new Date();
    $('#priceAppliedDate').datepicker({
        autoclose: true,
        todayHighlight: false,
        startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
    });
    $('#priceAppliedDatePresentation').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
    });

    $("#SupplierBaseCost").select2();
    $("#specsGrLocation").select2();
    $("#txtCurrecy").select2();

    $(".SpinAddPrice").TouchSpin({
        mousewheel: false,
        decimals: 4,
        step: 0.0001,
        boostat: 5,
        max: 10000,
        maxboostedstep: 10,
        prefix: '$',
        verticalbuttons: true
    });
    $(".SpinAddSpecs").TouchSpin({
        verticalbuttons: true,
        mousewheel: false,
        decimals: 4,
        step: 0.0001,
        boostat: 5,
        max: 10000,
        maxboostedstep: 10
    });
    $('.ValidDatePi').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
    });

    $('.ValidDatePiToday').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
    });

    var tipos = JSON.parse($("#model").val());

    $.each(tipos,
        function (index, value) {
            if (value.active_flag == true) {
                value.active_flag =
                    '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
            } else if (value.active_flag == false) {
                value.active_flag =
                    '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
            }
            if (value.weight_flag == true) {
                value.weight_flag =
                    '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
            } else if (value.weight_flag == false) {
                value.weight_flag =
                    '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
            }
        });
    table.clear();
    table.rows.add($(tipos));
    table.columns.adjust().draw();
    $('#SitesListBloq').slimScroll({
        height: '500px'
    });
    $('#SitesListBloq').find('input[name="SitesBloq"]').iCheck({
        checkboxClass: 'icheckbox_square-green'
    });
});



function CaractersString(id) {
    var value = $(id).val().trim();
    $('#specsDun14').val(value);
    if (value.length >= 20) {
        $(id).val(0)
        toastr.error("Use utilice menos caracteres.");
    }
    if (value != "") {
        if (!/^[a-zA-Z0-9]+$/.test(value)) {
            toastr.error("Debe contener solo letras y números.");
            $(id).focus();
            $(id).val("");
        }
    }
}

function PositiveNumber(id) {
    var value = $(id).val();
    if (parseFloat(value) <= 0 || isNaN(parseFloat(value)))
        $(id).val(0)
}

var foo = false;
var count = 0;
var table = $('#tblItems').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate":
        {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    columnDefs: [
        {
            targets: [0],
            orderable: false,
            data: null,
            width: "1%",
            defaultContent:
                '<button class="btn btn-xs btn-outline btn-info" id="bt1"><i class="pe-7s-config"></i> Configurar</button>'
        }
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50], [10, 25, 50]],
    buttons: [
    ],
    columns: [
        { data: null },
        { data: 'part_number' },
        { data: 'part_description', "width": "20%" },
        { data: 'unit_size' },
        { data: 'part_type' },
        { data: 'buyer_division' },
        { data: 'part_iva' },
        { data: 'part_ieps' },
        { data: 'part_number_sat' },
        { data: 'unit_sat' },
        { data: 'weight_flag' },
        { data: 'active_flag' },
    ],
});

var TableSuppliers = $('#TableSuppliers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    columnDefs: [
        {
            targets: [0, 1],
            visible: false,
            searchable: false
        },
        {
            targets: [4],
            render: function (data, type, full, meta) {
                //if (full.supplier_enable)
                return `<button class="btn btn-xs btn-outline btn-danger" onclick="DisableSupplier('${full.SuplierPartNumber}',true)"><i class="fa fa-remove"></i> Desactivar</button>`;
                //else
                //    return `<button class="btn btn-xs btn-outline btn-success" onclick="DisableSupplier('${full.supplier_id}')"><i class="fa fa-check"></i> Desactivar</button>`;
            },
            width: "1%"
        }
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
    ],
    columns: [
        { data: 'SuplierPartNumber' },
        { data: 'supplier_id' },
        { data: 'business_name' },
        { data: 'city' },
        //{data:'supplier_enable'},
        { data: null }
    ],
});

function DisableSupplier(supplier) {
    swal({
        title: "¿Esta seguro?",
        text: "Se eliminara la relacion del proveedor con el producto.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            axios
                .post("/Supplier/ActionDisableItemSupplier", { Supplier: supplier })
                .then(data => {
                    if (data.data == "SF")
                        SessionFalse(sesion);
                    else if (data.data) {
                        ReloadTableSupplier();
                        toastr.success("Se desactivo correctamente");
                    }
                    else
                        toastr.warning("Error al descativar proveedor");
                })
                .catch(error => {
                    toastr.error('Error inesperado contactar a sistemas.');
                }).then(() => { EndLoading(); })
        }
        else { }
    });
}

function ReloadTableSupplier() {
    //debugger
    $.ajax({
        type: "GET",
        url: "/Item/ActionGetItemById/",
        data: { "partNumber": part_number, "parameter": "GetAllInfo" },
        success: function (response) {
            debugger
            var json = response.purch005;
            if (json != null) {
                var table_price = response.purch005.table_price;
                for (var i = 0; i < table_price.length; i++) {
                    if (table_price[i].valid_from == "" || table_price[i].valid_from == null) {
                        table_price[i].valid_from = "-"
                    }
                }
                $('#tab1').click();
                var jsonDrop = response.purch005.table_Drop;
                FillDropdown("SuppliersDrop", jsonDrop);
                FillDropdownCost("SupplierBaseCost", response.purch005.table_price, response.purch005.exchange_price);
                TableSuppliers.clear().draw();
                TableSuppliers.rows.add(response.purch005.table_supplier)
                TableSuppliers.columns.adjust().draw();
                TablePrice.clear().draw();
                TablePrice.rows.add(table_price)
                TablePrice.columns.adjust().draw();


                if (response.JsonPrice != null) {
                    $('#priceAppliedDate')
                        .val(null);
                    $('#salePriceMargin').val(0);
                    $('#salePrice').val(response.JsonPrice.priceUser);
                } else {
                    $('#priceAppliedDate').val(null);
                    $('#salePriceMargin').val(null);
                    $('#salePrice').val(null);
                }
                if (response.JsonSpecs != null) {
                    $('#specsPackingSize').val(response.JsonSpecs.packing_of_size);
                    $('#specsPalletSize').val(response.JsonSpecs.pallet_of_size);
                    $('#specsWeightPacking').val(response.JsonSpecs.weight_of_packing);
                    $('#specsDun14').val(response.JsonSpecs.dun14);
                    $('#specsGrLocation').val(response.JsonSpecs.gr_storage_location);
                    Specs = 0;
                } else {
                    $('#specsPackingSize').val(0);
                    $('#specsPalletSize').val(0);
                    $('#specsWeightPacking').val(0);
                    Specs = 1;
                }
                $('#DescriptionText').html(json.part_name);
                $('#codeText').html(json.part_number);
                $('#NameText').html(json.part_name);
                $('#CodeText').html(json.part_number);
                $('#CodeText').html(json.part_number);
                $('#UMText').html(json.unit);
                $('#DiviText').html(json.buyer_division);
                $('#TypeText').html(json.part_type);
                $('#cUserText').html(json.cuser);
                $('#cDateText').html(json.cdate);
                $('#unitSAT').html(json.unit_sat);
                $('#partNumberSAT').html(json.part_number_sat);
                //si falla quitar if ternario
                $('#ActiveText').html(json.active_flag == "True" ? "Si" : "No");
                $('#weightText').html(json.weight_flag == "True" ? "Si" : "No");
                $('#IvaText').html(json.part_iva);
                $('#IepsText').html(json.part_ieps);
                $('#TableProduct').addClass("hide");
                $('#PanelEdit').removeClass("hide");
                $('#PanelEdit').animatePanel();
                number = json.part_number;
            }
        },
        error: function (response) {
            toastr.error(response.message);
        }
    });
}

var TablePrice = $('#TablePrice').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    columnDefs: [
        {
            targets: [0],
            orderable: false,
            data: null,
            width: "1%",
            defaultContent:
                '<button class="btn btn-xs btn-outline btn-warning" id="btPrice"><i class="fa fa-pencil"></i> Editar</button>'
        },
        {
            targets: [1],
            visible: false,
            searchable: false
        },
        {
            targets: [2],
            visible: false,
            searchable: false
        },
        {
            targets: [6],
            render: function (data, type, row) {
                if (data.length > 8) {
                    return moment(data).format('L');
                } else {
                    return "-"
                }
            },
        },
        {
            targets: [4],
            className: 'text-right',
            type: 'numeric-comma',
            render: function (data, type, row) {
                return '$' + data
            }
        },
        {
            targets: [5],
            render: function (data, type, row) {
                if (data != null) {
                    if (data.length > 2) {
                        return data;
                    } else {
                        return "-";
                    }
                } else {
                    return "-";
                }
            },
        }
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: null },
        { data: 'base_price_id' },
        { data: 'supplier_part_number' },
        { data: 'business_name' },
        { data: 'base_price' },
        { data: 'currency' },
        { data: 'valid_from' }
    ],
});

var number = "";
var Specs = null;
$('#tblItems tbody').on('click',
    '#bt1',
    function (e) {
        values = table.row($(this).parents('tr')).data();
        part_number = values.part_number;
        SearchBarcode(part_number)
        values = $.map(values,
            function (value, index) {
                switch (index) {
                    case 'part_number':
                        presentationModule({ part_number: value });

                        $.ajax({
                            type: "GET",
                            url: "/Item/ActionGetItemById/",
                            data: { "partNumber": value, "parameter": "GetAllInfo" },
                            success: function (response) {
                                var json = response.purch005;
                                if (json != null) {
                                    var table_price = response.purch005.table_price;
                                    for (var i = 0; i < table_price.length; i++) {
                                        if (table_price[i].valid_from == "" || table_price[i].valid_from == null) {
                                            table_price[i].valid_from = "-"
                                        }
                                    }
                                    $('#tab1').click();
                                    $('#excahnge_id_current').html('$' + response.purch005.exchange_price);
                                    var jsonDrop = response.purch005.table_Drop;
                                    FillDropdown("SuppliersDrop", jsonDrop);
                                    FillDropdownCost("SupplierBaseCost", response.purch005.table_price, response.purch005.exchange_price);
                                    TableSuppliers.clear().draw();
                                    TableSuppliers.rows.add(response.purch005.table_supplier)
                                    TableSuppliers.columns.adjust().draw();
                                    TablePrice.clear().draw();
                                    TablePrice.rows.add(table_price)
                                    TablePrice.columns.adjust().draw();

                                    if (response.JsonPrice != null) {
                                        $('#priceAppliedDate')
                                            .val(null);
                                        $('#salePriceMargin').val(response.JsonPrice.margin);
                                        $('#salePrice').val(response.JsonPrice.priceUser);
                                    } else {
                                        $('#priceAppliedDate').val(null);
                                        $('#salePriceMargin').val(null);
                                        $('#salePrice').val(null);
                                    }
                                    if (response.JsonSpecs != null) {
                                        $('#specsPackingSize').val(response.JsonSpecs.packing_of_size);
                                        $('#specsPalletSize').val(response.JsonSpecs.pallet_of_size);
                                        $('#specsWeightPacking').val(response.JsonSpecs.weight_of_packing);
                                        $('#specsDun14').val(response.JsonSpecs.dun14);
                                        $('#specsGrLocation').val(response.JsonSpecs.gr_storage_location);
                                        Specs = 0;
                                    } else {
                                        $('#specsPackingSize').val(0);
                                        $('#specsPalletSize').val(0);
                                        $('#specsWeightPacking').val(0);
                                        Specs = 1;
                                    }
                                    if (response.JsonCodes) {
                                        validate_part_number_only_codes_packages = true;
                                    } else {
                                        validate_part_number_only_codes_packages = false;
                                    }
                                    $('#DescriptionText').html(json.part_name);
                                    $('#codeText').html(json.part_number);
                                    $('#NameText').html(json.part_name);
                                    $('#CodeText').html(json.part_number);
                                    $('#CodeText').html(json.part_number);
                                    $('#UMText').html(json.unit);
                                    $('#DiviText').html(json.buyer_division);
                                    $('#TypeText').html(json.part_type);
                                    $('#cUserText').html(json.cuser);
                                    $('#cDateText').html(json.cdate);
                                    $('#unitSAT').html(json.unit_sat);
                                    $('#partNumberSAT').html(json.part_number_sat);
                                    //si falla quitar if ternario
                                    $('#ActiveText').html(json.active_flag == "True" ? "Si" : "No");
                                    $('#weightText').html(json.weight_flag == "True" ? "Si" : "No");
                                    $('#IvaText').html(json.part_iva);
                                    $('#IepsText').html(json.part_ieps);
                                    $('#TableProduct').addClass("hide");
                                    $('#PanelEdit').removeClass("hide");
                                    $('#PanelEdit').animatePanel();
                                    validate_part_number_forced_90 = json.isForced90;
                                    number = json.part_number;
                                    $('#SitesListBloq').find('input[name="SitesBloq"]').iCheck('uncheck');
                                    $.each(response.JsonBlocked, function (i, v) {
                                        $('#SitesListBloq').find('[value=' + v.site_code + ']').iCheck('check');
                                    });
                                }
                            },
                            error: function (response) {
                                toastr.error(response.message);
                            }
                        });
                        break;
                }
            });
    });

$('#TablePrice tbody').on('click',
    '#btPrice',
    function (e) {
        values = TablePrice.row($(this).parents('tr')).data();
        values = $.map(values,
            function (value, index) {
                switch (index) {
                    case 'base_price_id':
                        $('#BasePriceId').val(value);
                        break;
                    case 'supplier_part_number':
                        $('#SupplierPartNumberId').val(value);
                        break;
                    case 'base_price':
                        $('#spinBasePrice').val(value);
                        break;
                    case 'profit_margin':
                        $('#spinMargin').val(value);
                        break;
                    case 'sales_price':
                        $('#spinSalesPrice').val(value);
                        break;
                    case 'currency':
                        $('#txtCurrecy').val("");
                        $('#txtCurrecy').val(value);
                        break;
                    case 'valid_from':
                        if (value.length > 8)
                            $('#ValidDate').val(moment(value).format('L'));
                        else {
                            $('#ValidDate').val("");
                        }

                        break;
                }
                $('#ModalAddCost').modal('show');
            });
    });

function Clear() {
    $('#tab1').click();
    $('#PanelEdit').addClass("hide");
    $('#TableProduct').removeClass("hide");
    $('#TableProduct').animatePanel();
    $('#specsPackingSize').val("");
    $('#specsPalletSize').val("");
    $('#specsWeightPacking').val("");
    $('#specsDun14').val("");
    $('#specsGrLocation').val("");
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="0">Seleccione una opción</option>');
        for (var i in vData) {
            vItems.push('<option value="' + vData[i].Value + '">' + vData[i].Text + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val(0).trigger('change');
    } else {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una opción</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val(0).trigger('change');
    }
}

function FillDropdownCost(selector, vData, exchange_price) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="">Seleccione una opción</option>');
        for (var i in vData) {
            var local_cost = 0;
            var local_cost_decription = '';
            if (vData[i].currency == "USD") {
                local_cost = (vData[i].base_price * exchange_price).toFixed(4);
                local_cost_decription = '(' + fomartmoney(vData[i].base_price * exchange_price)(2) + ' MXN)';
            }
            else {
                local_cost = vData[i].base_price;
                local_cost_decription = '';
            }
            vItems.push('<option value="' + local_cost + '">$' + vData[i].base_price + ' ' + local_cost_decription + ' ( ' + vData[i].business_name + ')</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val('').trigger('change');
    } else {
        var vItems = [];
        vItems.push('<option value="">Seleccione una opción</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val('').trigger('change');
    }
}

function SalveAsigSupplier() {
    if ($('#SuppliersDrop').val() != 0) {
        const basePrice = $('#etxtBasePriceAdd').val();
        const currency = $('#selectCurrencyAdd').val();
        const valid = $('#ValidDateAdd').val();
        if (!basePrice || basePrice <= 0) {
            toastr.warning("El costo del producto debe ser mayor de $0");
            return;
        }
        if (!currency) {
            toastr.warning("Es necesario seleccionar un tipo de moneda");
            return;
        }
        if (!valid) {
            toastr.warning("Es necesario seleccionar una fecha de applicación");
            return;
        }

        swal({
            title: "¿Esta seguro?",
            text: "Se asignara el proveedor al producto.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, async function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                var sup = $('#SuppliersDrop').val();
                try {
                    let r = await axios.post("/Item/ActionAssingSupplier/", { "part_number": number, "supplier_id": sup });

                    if (!r.data.success) {
                        toastr.warning(r.data.responseText);
                        return;
                    }
                    const data = {
                        "part_number": number,
                        "BasePriceId": 0,
                        "SupplierPartNumberId": r.data.supplier_part_number,
                        "BasePrice": basePrice,
                        "Currency": currency,
                        "ValidFrom": valid,
                        "program_id": 'PURCH005.cshtml'
                    };
                    let r2 = await axios.post("/Item/ActionUpdateBasePrice/", data)
                    if (!r2.data.success) {
                        toastr.warning("El producto se guardo correctamente pero no se pudo guardar costo:");
                    } else {
                        $('#etxtBasePriceAdd').val('');
                        $('#selectCurrencyAdd').val('');
                        $("#SuppliersDrop").select2("val", "");
                        const date = new Date();
                        $('#ValidDateAdd').val(`${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`);
                        var jsonDrop = JSON.parse(r2.data.JsonDrop);
                        FillDropdown("SuppliersDrop", jsonDrop);
                        TableSuppliers.clear().draw();
                        TableSuppliers.rows.add($.parseJSON(r2.data.JsonTable))
                        TableSuppliers.columns.adjust().draw();
                        TablePrice.clear().draw();
                        TablePrice.rows.add($.parseJSON(r2.data.JsonTablePrices))
                        TablePrice.columns.adjust().draw();
                        FillDropdownCost("SupplierBaseCost", r2.data.jsonDropBasePrices, r2.data.exchange_price);
                        $('#ModalAddSupplier').modal('hide');
                        toastr.success("Producto y costo registrado correctamente");
                    }
                } catch (e) {
                    console.log(e);
                } finally {
                    EndLoading();
                }
            }
        });
    } else
        toastr.warning('<br>■ Codigo: ' + $('#SuppliersDrop').val() + '');
}

$("#formAddPrice").validate({
    rules: {
        spinBasePrice: {
            required: true,
            number: true,
            min: 0.1,
            max: 99999
        },
        currency: {
            required: true,
            min: 2
        },
        ValidDate: {
            required: true
        }
    },
    messages: {
        spinBasePrice: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
            number: "Se debe ingresar un valor numerico",
            min: 'Ingrese un número positivo.',
            max: 'Ingrese un número razonable.'
        },
        currency: {
            required: "El Campo no puede ser nulo",
            min: 'El Campo no puede ser nulo',
        },
        ValidDate:
        {
            required: "El Campo no puede ser nulo"
        }
    },
    submitHandler: function (form) {
        var e = document.getElementById("txtCurrecy");
        var strCurrency = e.options[e.selectedIndex].value;
        if (strCurrency == "") {
            toastr.warning('Seleccione un tipo de moneda.');
        } else {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/Item/ActionUpdateBasePrice/",
                data: {
                    "part_number": number,
                    "BasePriceId": $('#BasePriceId').val(),
                    "SupplierPartNumberId": $('#SupplierPartNumberId').val(),
                    "BasePrice": $('#spinBasePrice').val(),
                    "Currency": $('#txtCurrecy').val(),
                    "ValidFrom": $('#ValidDate').val(),
                    "program_id": 'PURCH005.cshtml'
                },
                success: function (returndate) {
                    if (returndate.success == true) {
                        TablePrice.clear().draw();
                        TablePrice.rows.add($.parseJSON(returndate.JsonTablePrices))
                        TablePrice.columns.adjust().draw();
                        FillDropdownCost("SupplierBaseCost", returndate.jsonDropBasePrices, returndate.exchange_price);
                        $('#ModalAddCost').modal('hide');
                        $('#txtCurrecy').val("");
                        $('#txtCurrecy').val($("#txtCurrecy :first").val()).trigger('change');
                        toastr.success('' + returndate.responseText + '.');
                        EndLoading();
                    } else {
                        toastr.warning('' + returndate.responseText + '');
                        $('#ModalAddCost').modal('hide');
                        EndLoading();
                    }
                },
                error: function (returndate) {
                    toastr.error('Error inesperado contactar a sistemas.');
                    $('#ModalAddCost').modal('hide');
                    EndLoading();
                }
            });

            return false;
        }
    }
});

function ChangeMargin(s, e) {
    if (foo == false) {
        foo = true;
        var PCompra = parseFloat($('#spinBasePrice').val());
        var pVenta = parseFloat($('#spinSalesPrice').val());
        var pmargin = 100 - ((PCompra * 100) / pVenta);
        pmargin = pmargin.toFixed(3);
        $('#spinMargin').val(pmargin);
        foo = false;
    }
}

function ChangeSale(s, e) {
    if (foo == false) {
        foo = true;
        var pBasePrice = parseFloat($('#spinBasePrice').val());
        var pmargin = pBasePrice / (1 - (pBasePrice / 100));
        pmargin = pmargin.toFixed(3);
        $('#spinSalesPrice').val(pmargin)
        foo = false;
    }
}

function AddSpecs() {
    var AddSpecsItem = "/Item/ActionInsertItemSpecs/";
    if (Specs == 0) {
        AddSpecsItem = "/Item/ActionEditItemSpecs/";
    }

    PositiveNumber('#specsPackingSize');
    PositiveNumber('#specsPalletSize');
    PositiveNumber('#specsWeightPacking');
    CaractersString('#specsDun14');
    swal({
        title: "¿Esta seguro?",
        text: "Se registraran los aspectos del producto.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            var specs = $('#specsDun14').val();
            specs = specs.trim();
            $.ajax({
                type: "POST",
                url: AddSpecsItem,
                data: {
                    "part_number": number,
                    "packing_of_size": $('#specsPackingSize').val(),
                    "pallet_of_size": $('#specsPalletSize').val(),
                    "weight_of_packing": $('#specsWeightPacking').val(),
                    "dun14": $('#specsDun14').val(),
                    "gr_storage_location": "SL01"
                },
                success: function (returndate) {
                    if (returndate.success == true) {
                        toastr.success('' + returndate.message + '.');
                        Specs = 0;
                        EndLoading();
                    } else {
                        toastr.warning('' + returndate.message + '');
                        EndLoading();
                    }
                },
                error: function (returndate) {
                    toastr.error(returndate.message);
                    EndLoading();
                }
            });
        }
    });
}

function ChangeMarginPrice() {
    var supplierCost = $('#SupplierBaseCost').val();
    var ieps = parseInt($('#IepsText').text());
    var ieps = 1 + (ieps / 100);
    var costoConIEPS = ieps * supplierCost;
    var x = costoConIEPS / parseFloat($('#salePrice').val());
    var margen = (1 - x) * 100;
    $('#salePriceMargin').val(margen.toFixed(4));
    if ($('#salePriceMargin').val() == "NaN")
        $('#salePriceMargin').val(0);
    if (parseInt($('#salePriceMargin').val()) == 100)
        $('#salePriceMargin').val(0);
}

const ValidatePriceSaleForm = $("#formAddPriceSale").validate({
    ignore: [],
    rules: {
        SupplierBaseCost: {
            required: true
        },
        priceAppliedDate: {
            required: true,
            minlength: 4
        },
        salePrice: {
            required: true,
            number: true,
            min: 1,
            max: 99999,
        }
    },
    messages: {
        SupplierBaseCost: {
            required: "El Campo no puede ser nulo",
        },
        priceAppliedDate: {
            required: "El Campo no puede ser nulo"
        },
        salePrice: {
            required: "El Campo no puede ser nulo",
            number: 'Ingrese solo números',
            min: 'Ingrese un número positivo',
            max: 'Ingrese un número razonable'
        }
    }
});

$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "el formato no es correcto"
);

function AddPriceSale() {
    if (validate_part_number_forced_90) {
        if (!isprice90.test($('#salePrice').val())) {
            toastr.warning('El precio debe terminar en .90');
            return false;
        }
        if (ValidatePriceSaleForm.element("#priceAppliedDate") && ValidatePriceSaleForm.element("#salePrice"))
            AddPriceSaeMethod();
    } else if (ValidatePriceSaleForm.element("#priceAppliedDate") && ValidatePriceSaleForm.element("#salePrice")) {
        AddPriceSaeMethod();
    }
}

function AddPriceSaeMethod() {
    listFinals = [];
    listFinals[0] = {
        partNumber: number,
        appliedDate: $('#priceAppliedDate').val(),
        margin: $('#salePriceMargin').val(),
        priceUser: $('#salePrice').val(),
        part_description: "",
        priceChangeNow: false
    };

    swal({
        title: "¿Esta seguro?",
        text: "Se registrara el precio de venta en $" + $('#salePrice').val() + ".",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/Item/ActionSaveChangePriceStores",
                data: { itemChangePrice: listFinals, ProgramId: "PURCH005.cshtml" },
                success: function (returndate) {
                    if (returndate.success == true) {
                        toastr.success(returndate.responseText);
                        EndLoading();
                    } else {
                        toastr.warning(returndate.responseText + '.');
                        EndLoading();
                    }
                },
                error: function (returndate) { }
            });
        }
    });
}

// script
const http = {
    get: function (url, options) {
        return this.request(axios.get(url, options));
    },
    post: function (url, data, options) {
        return this.request(axios.post(url, data, options));
    },
    request: function (axiosProm) {
        return new Promise((resolve, reject) => {
            axiosProm.then(res => {
                if (res.data.Ok) {
                    return resolve(res.data);
                } else if (res.data.Type === "sf") {
                    swal({
                        title: "Sesión terminada",
                        text: "Se redigira a la pagina de inicio de sesión",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Aceptar"
                    },
                        (va) => {
                            const loc = window.location;
                            window.location.href = `${loc.protocol}//${loc.host}/Home/Login`;
                        });
                    return Promise.resolve();
                } else {
                    return res.data.Ok ? resolve(res.data) : reject(res.data);
                }

            }).catch(e => {
                const code = e.response ? e.response.status : -1;
                let msg;
                if (code === 404) {
                    msg = "Url no encontrada";
                } else {
                    msg = `Error desconocido[${code}]`;
                }
                const ne = { Code: e.code, Msg: msg };

                if (reject) {
                    return reject(ne);
                }

                toastr.error(ne.Msg);
                return Promise.reject(ne);
            });
        });
    }
};
function price90Presentation() {
    var price = $('#txt_price_total').val();
    if (validate_part_number_forced_90) {
        if (!isprice90.test(price)) {
            $('#txt_price_total').val("0");
            toastr.warning("El precio de la presentación debe terminar en .90");
        }
    }

}

var pr_form;
var presentationModule = (filter) => {

    const modal = $("#pr_modal");
    pr_form = {
        edit_mode: false,
        presentationId: -1,
        test: -12,
        form: document.getElementById("form"),

        txt_name: document.getElementById("txt_name"),
        txt_presentation_code: document.getElementById("txt_presentation_code"),
        txt_part_number: document.getElementById("txt_part_number"),
        label_part_name: document.getElementById("label_part_name"),
        select_packing_type: document.getElementById("select_packing_type"),
        txt_quantity: document.getElementById("txt_quantity"),
        select_unit_size: document.getElementById("select_unit_size"),
        ckbox_active: document.getElementById("ckbox_active"),
        priceAppliedDatePresentation: document.getElementById("priceAppliedDatePresentation"),
        ckbox_show_in_label: document.getElementById("ckbox_show_in_label"),
        txt_price_total: document.getElementById("txt_price_total"),
        modal_title: document.getElementById("modal_title"),
        modal_description: document.getElementById("modal_description"),
        btn_form_cancel: document.getElementById("btn_form_cancel"),
        btn_form_accept: document.getElementById("btn_form_accept"),
        label_price_extra: document.getElementById("label_price_extra"),
    };
    pr_form.select_unit_size.innerHTML = "";
    axios.get("/MaCode/ActionGetAllUMCodes")
        .then((res) => {
            res.data.forEach(e => {
                const option = document.createElement("option");
                option.text = e.vkey;
                pr_form.select_unit_size.add(option);
            });
        });
    const table = $('#table').DataTable({
        destroy: true,
        oLanguage: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        buttons: [
            { extend: 'csv', text: 'Excel', title: 'ConfiguracionProductos', className: 'btn-sm' },
            {
                extend: 'pdf',
                text: 'PDF',
                title: 'ConfiguracionProductos',
                className: 'btn-sm',
                exportOptions: {
                    columns: [1, 2, 3]
                },
                customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
        ],
        columns: [
            {
                class: "",
                data: null,
                defaultContent: "<button class='btn btn-xs btn-outline btn-warning'><i class='fa fa-pencil'></i> Editar</button>",
                ordering: false,
                orderable: false
            },
            { data: 'PresentationCode' },
            { data: 'Name' },
            { data: 'PackingType' },
            { data: 'Quantity' },
            { data: 'FactorUnitSize' },
            { data: data => round(data.Price) },
            { data: data => round(data.IVA) },
            { data: data => round(data.IEPS) },
            { data: data => round(data.TotalPrice) },
            {
                data: (m) => {
                    return `
                                                                                                        <div style = "position: relative;" class= "icheckbox_square-green ${m.ActiveFlag
                            ? 'checked'
                            : ''}">
                                                                                                            <input type = "checkbox" disabled = "" style = "position: absolute; opacity: 0;"/>
                                                                                                            <ins class="iCheck-helper"></ins >
                                                                                                        </div>`;
                }
            }
        ]
    });
    const tbody = $("#tbody");
    tbody.off('click');
    tbody.on('click',
        'button',
        function () {
            const data = table.row($(this).parents('tr')).data();
            openForm(data, 'edit');
        });

    function updateTable(filter) {
        let url = `/ItemPresentation/ActionGetPresentationByProductId?part_number=${filter.part_number}`;
        http.get(url)
            .then(res => {
                var data = res.Data;
                table.clear();
                table.rows.add($(data));
                table.columns.adjust().draw();
                const tr = document.getElementById("pr_th_btn");
                tr.classList.remove("sorting_asc");
                tr.classList.add("sorting_disabled");
                EndLoading();
            });
    }


    function openForm(model, action) {
        if (action === 'edit' && model) {
            pr_form.presentationId = model.PresentationId || "";
            pr_form.txt_name.value = model.Name || "";
            pr_form.txt_presentation_code.value = model.PresentationCode || "";
            pr_form.txt_presentation_code.readOnly = true;
            pr_form.select_packing_type.value = model.PackingType;
            pr_form.txt_quantity.value = model.Quantity || 0;
            pr_form.select_unit_size.value = model.FactorUnitSize || 0;
            pr_form.txt_price_total.value = model.TotalPrice || 0;
            pr_form.ckbox_active.checked = model.ActiveFlag || false;
            pr_form.priceAppliedDatePresentation = $("#priceAppliedDatePresentation").val()
            $(pr_form.ckbox_active).iCheck('update');
            pr_form.ckbox_show_in_label.checked = model.ShowInLabel || false;
            pr_form.modal_title.innerHTML = "<i class='fa fa-object-group'></i> Editar Presentación";
            pr_form.modal_description.innerHTML = "Formulario para editar una presentación de producto.";

            form.edit_mode = true;
        } else {
            pr_form.txt_name.value = "";
            pr_form.txt_presentation_code.value = "";
            pr_form.txt_presentation_code.readOnly = false;
            pr_form.select_packing_type.value = "";
            pr_form.txt_quantity.value = "";
            pr_form.select_unit_size.value = "";
            pr_form.ckbox_active.checked = true;
            $(pr_form.ckbox_active).iCheck('update');
            pr_form.ckbox_show_in_label.checked = false;
            pr_form.txt_price_total.value = "";
            pr_form.priceAppliedDatePresentation = "";

            pr_form.modal_title.innerHTML = "<i class='fa fa-object-group'></i> Agregar Presentación";
            pr_form.modal_description.innerHTML = "Formulario para agregar una presentación de producto."
            form.edit_mode = false;
        }
        if (model) {
            pr_form.txt_part_number.value = model.PartNumber || "";
            pr_form.txt_part_number.readOnly = true;
            updateProductName(model.PartNumber);
            updatePriceExtras(model.PartNumber);
        } else {
            pr_form.txt_part_number.value = "";
        }
        //validaciones
        const str_rule = {
            required: true,
            maxlength: 50
        };
        const str_basic_rule = {
            required: true,
            minlength: 2,
        };
        const str_msg = {
            required: "El Campo no puede ser nulo",
            maxlength: "El campo puede tener hasta 50 caracteres",
        };
        const number_rule = {
            required: true,
            number: true,
            min: 1,
            max: 99999,
            regex: /^\d+(.\d{0,4})?$/,
        };

        const number_msg = {
            required: "El Campo no puede ser nulo",
            number: "Se debe ingresar un valor numerico",
            min: "El valor debe ser mayor o igual que 1",
            max: "Ingrese un número razonable.",
            regex: "El valor solo puede tener 4 decimales"
        };
        const price_rule = {
            required: true,
            number: true,
            min: 1,
            max: 10000,
        };
        const price_msg = {
            required: "El Campo no puede ser nulo",
            number: "Se debe ingresar un valor numerico",
            min: "El valor debe ser mayor o igual que 1",
            max: "El valor debe ser menor que 1000000",
        };
        const str_basic_msg = {
            required: "El Campo no puede ser nulo",
            minlength: "Ingrese una fecha."
        };
        $.validator.addMethod(
            "regex",
            function (value, element, regexp) {
                var check = false;
                return this.optional(element) || regexp.test(value);
            },
            "el formato no es correcto"
        );
        const validator = $(pr_form.form).validate({
            rules: {
                txt_name: str_rule,
                txt_presentation_code: str_rule,
                txt_part_number: str_rule,
                txt_quantity: number_rule,
                select_unit_size: str_rule,
                txt_price_total: price_rule,
                priceAppliedDatePresentation: str_basic_rule

            },
            messages: {
                txt_name: str_msg,
                txt_presentation_code: str_msg,
                txt_part_number: str_msg,
                txt_quantity: number_msg,
                select_unit_size: str_msg,
                txt_price_total: price_msg,
                priceAppliedDatePresentation: str_basic_msg
            },
            submitHandler: () => {
                pr_form.txt_name.value = pr_form.txt_name.value.trim();
                pr_form.txt_quantity.value = pr_form.txt_quantity.value.trim();
                pr_form.txt_presentation_code.value = pr_form.txt_presentation_code.value.trim();
                pr_form.txt_price_total.value = pr_form.txt_price_total.value.trim();
                pr_form.priceAppliedDatePresentation.value = $("#priceAppliedDatePresentation").val()
                if (validator.element("#txt_name") &&
                    validator.element("#txt_quantity") &&
                    validator.element("#txt_presentation_code") &&
                    validator.element("#txt_price_total") &&
                    validator.element("#priceAppliedDatePresentation")) {
                    const post = {
                        PresentationId: pr_form.presentationId,
                        Name: pr_form.txt_name.value,
                        PartNumber: pr_form.txt_part_number.value,
                        Quantity: pr_form.txt_quantity.value,
                        Price: 0,
                        PresentationCode: pr_form.txt_presentation_code.value,
                        PackingType: pr_form.select_packing_type.value,
                        FactorUnitSize: select_unit_size.value,
                        ActiveFlag: pr_form.ckbox_active.checked,
                        ShowInLabel: pr_form.ckbox_show_in_label.checked,
                        IVA: 0,
                        IEPS: 0,
                        appliedDate: $("#priceAppliedDatePresentation").val(),
                        TotalPrice: pr_form.txt_price_total.value,
                    };
                    let url = "";
                    if (!form.edit_mode) {
                        url = "/ItemPresentation/ActionAddPresentation";
                    } else {
                        url = "/ItemPresentation/ActionEditPresentation";

                    }
                    pr_form.form.disable = true;
                    http.post(url, post).then(() => {
                        pr_form.form.disable = false;
                        modal.modal('hide');
                        updateTable({ "part_number": number });
                        toastr.success("Presentacion guardada exitosamente");
                    }).catch(e => {
                        toastr.error(e.Msg);
                        if (e.Code == 11 || e.Code == 13) {
                            form.txt_presentation_code.value = "";
                        }
                        pr_form.form.disable = false;
                    });
                }
            },

        });
        validator.resetForm();
        $('#form .form-control').removeClass('error');
        modal.modal('show');
    }

    function deletePresentation(id) {
        http.post("/ItemPresentation/ActionRemovePresentation", { "id": id })
            .then(() => {
                modal.modal('hide');
                updateTable({ "part_number": filter.part_number });
            });
    }

    function updateProductName(part_number) {
        pr_form.label_part_name.innerHTML = "";
        axios.get(`/Item/ActionGetItemDescription?part_number=${part_number}`)
            .then(res => {
                if (!res.data.success) {
                    pr_form.label_part_name.innerHTML = res.data.error;
                } else {
                    pr_form.label_part_name.innerHTML = res.data.part_description;
                }
            }).catch(e => {
                pr_form.label_part_name.innerHTML = ("[Error al obtener nombre de producto]");
            });
    }

    function updatePriceExtras(part_number) {
        pr_form.label_price_extra.innerHTML = "";
        axios.get(`/Item/ActionGetItemIvaIepsValue?part_number=${part_number}`)
            .then(res => {
                if (res.data.success) {
                    pr_form.txt_price_total.oninput = () => {
                        const total = Number(pr_form.txt_price_total.value);
                        if (isNaN(total)) {
                            pr_form.label_price_extra.innerHTML = "";
                            return;
                        }
                        const price_iva = total / (1 + res.data.Data.Iva);
                        const iva = round(total - price_iva);
                        let price = price_iva / (1 + res.data.Data.Ieps);
                        const ieps = round(price_iva - price);
                        price = round(price);
                        if (isNaN(price) || isNaN(iva) || isNaN(ieps)) pr_form.label_price_extra.innerHTML = "";
                        else
                            pr_form.label_price_extra.innerHTML =
                                `<b>Precio base:</b> ${price} <b> IEPS:</b> ${ieps} <b> IVA:</b> ${iva}`;
                    }

                    pr_form.txt_price_total.oninput();
                }
            }).catch(e => {

            });
    }

    function round(value, decimals) {
        decimals = decimals || 4;
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }

    document.getElementById('btn_add').onclick =
        () => openForm({ "PartNumber": filter.part_number }, "create_from");
    updateTable({ "part_number": filter.part_number });
};



/**Nuevo Codigo */
const session = "Se termino su sesion.";
$("#Item").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Item/ActionSearchItem/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.PathNumber} - ${item.Description}`,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

var tableBarcodes = $('#Items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[0, "asc"]],
    columnDefs: [
    ],
    columns: [
        { targets: 0, data: 'PartNumber', width: "1%" },
        { targets: 1, data: 'Barcode', width: "1%" },
        { targets: 2, data: 'Cuser', width: "1%" },
    ],
});

function FullTable(table, List) {
    table.clear().draw();
    table.rows.add(List)
    table.columns.adjust().draw();
}

function SearchBarcode() {
    var item = part_number; //document.getElementById("Item").value
    if (item != null && item.trim() != "") {
        //StartLoading();
        InfoBarcode(item);
    }
    FullTable(tableBarcodes, [])
}

function InfoBarcode(item) {
    axios.get(`/item/ActionSearchBarcode/?partNumber=${item}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion);
            }
            else if (data.data.length > 0) {
                FullTable(tableBarcodes, data.data)
            }
            else {
                FullTable(tableBarcodes, [])
                toastr.warning("No se encontraron codigos de barras")
            }
        }).catch(error => {
            toastr.error("A ocurrido un error favor de contactar a sistemas")
        }).then(() => { EndLoading(); })
}

function SaveBarcode() {
    var item = part_number;//document.getElementById("Item").value;
    var barcode = document.getElementById("inputBarcode").value;
    var noValido = / /;
    var special = /[&\/\#,+()$~%'":*?<>{}°_!$¡¿'`[]|]/;
    if (item != null && item.trim() != "" && barcode != null && barcode.trim() != "" &&
        !noValido.test(barcode.trim()) && !noValido.test(item.trim()) && !special.test(barcode.trim())) {
        StartLoading();
        $("#modalAddBarcode").modal("hide");
        axios.post(`/item/ActionSaveBarcode/`, { partNumber: item, barcode: barcode })
            .then(data => {
                if (data.data == "SF") {
                    EndLoading();
                    SessionFalse(sesion);
                }
                else if (data.data == 1) {
                    toastr.success("Se a guardado correctamente")
                    InfoBarcode(item)
                }
                else if (data.data == 2) {
                    EndLoading();
                    toastr.warning("El codigo ya se encuentra ingresado favor de colocar otro")
                }
                else if (data.data == 3) {
                    EndLoading();
                    toastr.error("Problema al guardar el codigo de barra")
                }
                else {
                    EndLoading();
                    toastr.error("Error desconosido contacte a sistemas")
                }
            }).catch(error => {
                EndLoading();
                toastr.error("A ocurrido un error favor de contactar a sistemas")
            })
    }
    else
        toastr.warning("Ingrese un codigo de barras valido")
}

function ModalOpen() {
    var item = part_number;//document.getElementById("Item").value;
    if (item != null && item.trim() != "") {
        document.getElementById("inputBarcode").value = "";
        document.getElementById("inputPartNumber").value = part_number;//document.getElementById("Item").value;
        $("#modalAddBarcode").modal("show");
    }
    else
        toastr.warning("Seleccione una opcion valida")
}
/*terminacion*/
function setSiteCode() {
    var SitesToBlocked = '';
    $.each($("input[name='SitesBloq']:checked"), function () {
        SitesToBlocked += $(this).val() + ',';
    });
    if (number != '') {

        swal({
            title: "¿Esta seguro?",
            text: "Se bloqueará el producto para ENTRADAS y SALIDAS en las tiendas seleccionadas",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: "/Item/ActionItemBlocked",
                    data: { Sites: SitesToBlocked.slice(0, -1), Part_number: number },
                    success: function (returndate) {
                        if (returndate.success == true) {
                            toastr.success(returndate.responseText);
                        } else {
                            toastr.warning(returndate.responseText + '.');
                        }
                    },
                    error: function (returndate) { }
                });
            }
        });
    }
    else toastr.warning('Debe seleccionar por lo menos una tienda!');
}