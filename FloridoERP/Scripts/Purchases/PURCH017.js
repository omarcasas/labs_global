﻿var ItemsList = [];
var itemsPrice = 0;
const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
function LoadItems(data) {
    Codes.clear().draw();
    Codes.rows.add(data);
    Codes.columns.adjust().draw();
    CalculateTotal();
}

function DeleteItem(num) {
    ItemsList.splice(num, 1);
    LoadItems(ItemsList);

    if (ItemsList.length == 0) {
        document.getElementById("btnCancel").setAttribute("disabled", "disabled");
        document.getElementById("btnSave").setAttribute("disabled", "disabled");
    }
}

function ValidateDuplicates(ItemsList) {
    var newItemsList = [];

    ItemsList.forEach(function (item) {
        if (newItemsList.length > 0) {
            var find = newItemsList.find(element => element.PartNumber == item.PartNumber);

            if (find) {
                for (var i in newItemsList) {
                    if (newItemsList[i].PartNumber == item.PartNumber) {
                        newItemsList[i].Quantity = item.Quantity + find.Quantity;
                        newItemsList[i].Cost = item.Cost;
                        break;
                    }
                }
            }
            else {
                var newItem = { PartNumber: item.PartNumber, PartDescription: item.PartDescription, Quantity: item.Quantity, Cost: item.Cost }
                newItemsList.push(newItem);
            }
        }
        else {
            var newItem = { PartNumber: item.PartNumber, PartDescription: item.PartDescription, Quantity: item.Quantity, Cost: item.Cost }
            newItemsList.push(newItem);
        }
    });

    return newItemsList;
}

function AddItem() {
    toastr.remove();
    var presentation = document.getElementById("presentations").value;
    var item = document.getElementById("items").value;
    var quantity = document.getElementById("quantity").value;
    var cost = document.getElementById("cost").value;

    if (presentation == "0") {
        toastr.warning("Seleccione un centro de empaque");
        $("#presentation").select2('open');
        return;
    }
    else if (item == "0") {
        toastr.warning("Ingrese un producto");
        $("#items").select2('open');
        return;
    }
    else if (quantity == "") {
        toastr.warning("Ingrese una cantidad");
        document.getElementById("quantity").focus();
        return;
    }

    else if (cost == "") {
        toastr.warning("Ingrese el precio");
        document.getElementById("cost").focus();
        return;
    }

    else if (isNaN(cost)) {
        toastr.warning("Ingrese el precio");
        document.getElementById("cost").value = "";
        document.getElementById("cost").focus();
        return;
    }

    var part_number = item.split("*/*/*/")[0];
    var part_description = document.getElementById("items").options[document.getElementById("items").selectedIndex].text;
    var newItem = { PartNumber: part_number, PartDescription: part_description, Quantity: parseFloat(quantity), Cost: parseFloat(cost), TotalCost: parseFloat(quantity * cost) }
    ItemsList.push(newItem);

    ItemsList = ValidateDuplicates(ItemsList);
    LoadItems(ItemsList);

    document.getElementById("items").value = "0";
    $("#items").trigger("change");
    document.getElementById("quantity").value = "";
    document.getElementById("cost").value = "";
    document.getElementById("btnCancel").removeAttribute("disabled");
    document.getElementById("btnSave").removeAttribute("disabled");
}

function Cancel() {
    swal({
        title: "¿Esta seguro que desea cancelar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm) {
            document.getElementById("presentations").value = "0";
            $("#presentations").trigger("change");
            Clear();
            $('html, body').animate({ scrollTop: 0 }, "slow");
        }
    });
}

function Clear() {
    document.getElementById("item-pres").style.visibility = "hidden";
    document.getElementById("presentationid").value = "";
    document.getElementById("totalprice").value = "";
    document.getElementById("codeProduct").innerHTML = "";
    document.getElementById("descriptionProduct").innerHTML = "";
    document.getElementById("costProduct").innerHTML = "";
    document.getElementById("packingtypeProduct").innerHTML = "";
    document.getElementById("unitsizeProduct").innerHTML = "";
    document.getElementById("total_cost_sum").innerHTML = "$ 0";
    document.getElementById("total_cost").innerHTML = "$ 0";
    ItemsList = [];
    Codes.clear().draw();
    Codes.columns.adjust().draw();
    document.getElementById("items").value = "0";
    $("#items").trigger("change");
    document.getElementById("quantity").value = "";
    document.getElementById("cost").value = "";
    document.getElementById("items").setAttribute("disabled", "disabled");
    document.getElementById("addProducts").setAttribute("disabled", "disabled");
    document.getElementById("quantity").setAttribute("disabled", "disabled");
    document.getElementById("btnCancel").setAttribute("disabled", "disabled");
    document.getElementById("btnSave").setAttribute("disabled", "disabled");
}

$(document).on("keypress", '#quantity', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/^[0-9]*\.?[0-9]*$/g))
        return true;
    else
        return false;
});

$(document).on("keypress", '#cost', function (event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    else
        return true;
});

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

$(document).on("keypress", '.Decimal', function (event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    else
        return true;
});

$(document).on("focusout", '.Numeric', function (event) {
    var row = event.target.attributes.row.value;
    document.getElementById("rowTotal" + row).innerHTML = "$" + (parseFloat(document.getElementById("rowQuantity" + row).value) * parseFloat(document.getElementById("rowCost" + row).value)).toFixed(2);
    Codes.cell(row, 2).data(parseFloat(document.getElementById("rowQuantity" + row).value)).draw();
    CalculateTotal();
});


$(document).on("focusout", '.Decimal', function (event) {
    var row = event.target.attributes.row.value;
    document.getElementById("rowTotal" + row).innerHTML = "$" + (parseFloat(document.getElementById("rowQuantity" + row).value) * parseFloat(document.getElementById("rowCost" + row).value)).toFixed(2);
    Codes.cell(row, 3).data(parseFloat(document.getElementById("rowCost" + row).value)).draw();
    CalculateTotal();
});

function CalculateTotal() {
    itemsPrice = 0;
    ItemsList.forEach(function (item) {
        itemsPrice += parseFloat(item.Quantity) * parseFloat(item.Cost);
    });
    var newValue = formatmoney(itemsPrice)(2);
    document.getElementById("total_cost_sum").innerHTML = newValue;
}

function GetPresentation() {
    Clear();
    var code = $("#presentations").val();
    if (code != "0") {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                if (!response.result)
                    SessionFalse("Terminó tu sesión");
                else if (response.result) {
                    document.getElementById("presentationid").value = response.model.part_number;
                    document.getElementById("totalprice").value = parseFloat(response.model.cost).toFixed(2);
                    document.getElementById("codeProduct").innerHTML = response.model.part_number;
                    document.getElementById("descriptionProduct").innerHTML = response.model.part_description;
                    //document.getElementById("costProduct").innerHTML = parseFloat(response.model.cost).toFixed(2);
                    document.getElementById("costProduct").value = parseFloat(response.model.cost).toFixed(2);
                    document.getElementById("packingtypeProduct").innerHTML = "$ " + response.model.price;
                    document.getElementById("unitsizeProduct").innerHTML = response.model.unit_size;
                    document.getElementById("total_cost").innerHTML = "$ " + parseFloat(response.model.cost).toFixed(2);;
                    document.getElementById("item-pres").removeAttribute("style");
                    document.getElementById("items").removeAttribute("disabled");
                    document.getElementById("addProducts").removeAttribute("disabled");
                    document.getElementById("quantity").removeAttribute("disabled");
                    document.getElementById("cost").removeAttribute("disabled");
                    ItemsList = response.model.CodesPackages;
                    if (ItemsList.length > 0) {
                        LoadItems(ItemsList);
                        document.getElementById("btnCancel").removeAttribute("disabled");
                        document.getElementById("btnSave").removeAttribute("disabled");
                    }
                }
                else
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
            else if (this.readyState == 4 && this.status != 200) {
                EndLoading();
                toastr.error("Alerta - Error inesperado  contactar a sistemas.");
            }
        }
        xhttp.open("GET", "/ItemPresentation/ActionGetPartNumberById?part_number=" + code, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(null);
    }
}

function ValidateItems() {
    var List = [];

    Codes.rows().every(function (index, element) {
        var row = $(this.node());
        var PartNumber = $(row.find('td').eq(0)).text();
        var PartDescription = $(row.find('td').eq(1)).text();
        var quantity = row.find('td').eq(2);
        var quantityInput = $('input', quantity);
        var Quantity = parseFloat(quantityInput.val());
        var cost = row.find('td').eq(3);
        var costInput = $('input', cost);
        var Cost = parseFloat(costInput.val());

        var newItem = { "PartNumber": PartNumber, "PartDescription": PartDescription, "Quantity": Quantity, "Cost": Cost };
        List.push(newItem);
        CalculateTotal();
    });

    ItemsList = List;
}

function Save() {
    itemsPrice = 0;
    console.log("hola");
    var presentation = document.getElementById("presentationid").value;
    var presentationPrice = parseFloat(document.getElementById("totalprice").value);

    //ValidateItems();

    ItemsList.forEach(function (item) {
        itemsPrice += parseFloat(item.Quantity) * parseFloat(item.Cost);
    });


    if (formatmoney(presentationPrice)(2) === formatmoney(itemsPrice)(2)) {
        swal({
            title: "¿Esta seguro que desea guardar la presentación?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                ValidateItems();
                StartLoading();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        EndLoading();
                        var response = JSON.parse(this.responseText);
                        if (response.result == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (response.result) {
                            document.getElementById("presentations").value = "0";
                            $("#presentations").trigger("change");
                            Clear();
                            toastr.success("Guardado con exito");
                            $('html, body').animate({ scrollTop: 0 }, "slow");
                        }
                        else
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                    else if (this.readyState == 4 && this.status != 200) {
                        EndLoading();
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                    }
                }
                xhttp.open("POST", "/CodesPackages/ActionSaveListCodesPackages", true);
                xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xhttp.send(JSON.stringify({ "PartNumberOrigin": presentation, "Codes": ItemsList }));
            }
        });
    } else {
        toastr.error('El total de los precios de los productos no coincide con el empaque.');
    }
}

function UpdateCost(value) {
    if (!isNaN(value)) {
        var newValue = formatmoney(value)(2);
        document.getElementById("total_cost").innerHTML = newValue;
        document.getElementById("totalprice").value = value
    } else {
        document.getElementById("costProduct").value = 0;
        document.getElementById("totalprice").value = 0;
        document.getElementById("total_cost").innerHTML = "$ 0";
    }
}

var Codes = $('#Codes').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    columns:
        [
            { targets: 0, data: 'PartNumber', width: "1%" },
            { targets: 1, data: 'PartDescription', width: "1%" },
            { targets: 2, data: 'Quantity', width: "1%", className: "col-xs-1 text-center" },
            { targets: 3, data: 'Cost', width: "1%", className: "col-xs-1 text-center" },
            { targets: 4, data: 'TotalCost', width: "1%", className: "text-center" },
            { targets: 5, data: null, width: "1%", className: "text-center", orderable: false }
        ],
    columnDefs:
        [
            {
                targets: [2],
                width: "1%",
                render: function (data, type, full, meta) {
                    return '<div class="col-xs-1"><input type="text" id=rowQuantity' + meta.row + ' row=' + meta.row + ' class="form-control text-center Decimal" value = ' + parseFloat(full.Quantity) + ' ></div>';
                }
            },
            {
                targets: [3],
                width: "1%",
                render: function (data, type, full, meta) {
                    return '<div class="col-xs-1"><input type="text" id=rowCost' + meta.row + ' row=' + meta.row + ' class="form-control text-center Decimal" value=' + parseFloat(full.Cost).toFixed(2) + '></div>';
                }
            },
            {
                targets: [4],
                width: "1%",
                render: function (data, type, full, meta) {
                    return "<p id=rowTotal" + meta.row + "> $" + parseFloat(full.Quantity * full.Cost).toFixed(2) + "</p > ";
                }
            },
            {
                targets: [5],
                width: "1%",
                render: function (data, type, full, meta) {
                    return '<button class="btn btn-xs btn-outline btn-danger" onclick="DeleteItem(' + meta.row + ')"><i class="fa fa-close"></i> Eliminar</button>';
                }
            }
        ]
});

document.addEventListener("DOMContentLoaded", function (event) {
    window.setTimeout(function () { document.getElementById("fa").click(); }, 1000);

    $("#presentations").select2({
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#items").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Ingrese un producto";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });
});