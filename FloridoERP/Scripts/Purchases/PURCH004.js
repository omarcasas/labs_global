﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#PanelDiv").click();
    $("#PanelFami").click();
    $(".drop").select2();
    $("#txtIVASale").select2();
    $("#CatalogueMaClass").select2();
    $.fn.select2.locales['es'];
    $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
    var Products = JSON.parse($("#model").val());
    $.each(Products, function (index, value) {
        if (value.active_flag == true) {
            value.active_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
        }
        else if (value.active_flag == false) {
            value.active_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
        }
        if (value.weight_flag == true) {
            value.weight_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
        }
        else if (value.weight_flag == false) {
            value.weight_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
        }
    });
    table.clear();
    table.rows.add($(Products));
    table.columns.adjust().draw();
});
$(document).on("keypress", '.Decimal', function (event) { //Validador de numero decimales
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    else
        return true;
});
$(document).on("keypress", '.Numeric', function (event) { //Validador de numeros enteros
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});
function PutZero() {
    var days = $("#txtExpDays").val();
    if (days == "")
        $("#txtExpDays").val(0);
}
$(".SpinAddPrice").TouchSpin({
    mousewheel: false,
    decimals: 4,
    step: 0.0001,
    boostat: 5,
    max: 10000,
    maxboostedstep: 10,
    verticalbuttons: true
});

var DivList = 0;
var DepList = 0;
var FamiList = 0;
var LineList = 0;
function CategoListEvent() {
    $('#DivList').find('input[name="Level1"]').iCheck({
        radioClass: 'iradio_square-green'
    }).on('ifChecked', function (e) {
        DivList = $(this).val();
        $("#PanelFami").click();
        $.ajax({
            type: "GET",
            url: "/MaClass/ActionGetMaClassById",
            data: { "parent": DivList, "level": 2 },
            success: function (returndate) {
                if (returndate.success == true) {
                    $("#DepList").empty();
                    $("#FamiList").empty();
                    $("#LineList").empty();
                    DepList = 0;
                    FamiList = 0;
                    LineList = 0;
                    $.each(returndate.Json, function (i, v) {
                        $("#DepList").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level2' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                    });
                    $('#DepList').find('input[name="Level2"]').iCheck({
                        radioClass: 'iradio_square-green'
                    }).on('ifChecked', function (e) {
                        DepList = $(this).val();
                        $.ajax({
                            type: "GET",
                            url: "/MaClass/ActionGetMaClassById",
                            data: { "parent": DepList, "level": 3 },
                            success: function (returndate) {
                                if (returndate.success == true) {
                                    $("#FamiList").empty();
                                    $("#LineList").empty();
                                    FamiList = 0;
                                    LineList = 0;
                                    $.each(returndate.Json, function (i, v) {
                                        $("#FamiList").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level3' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                    });
                                    $('#FamiList').find('input[name="Level3"]').iCheck({
                                        radioClass: 'iradio_square-green'
                                    }).on('ifChecked', function (e) {
                                        FamiList = $(this).val();
                                        $.ajax({
                                            type: "GET",
                                            url: "/MaClass/ActionGetMaClassById",
                                            data: { "parent": FamiList, "level": 4 },
                                            success: function (returndate) {
                                                if (returndate.success == true) {
                                                    $("#LineList").empty();
                                                    LineList = 0;
                                                    $.each(returndate.Json, function (i, v) {
                                                        $("#LineList").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level4' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                                    });
                                                    $('#LineList').find('input[name="Level4"]').iCheck({
                                                        radioClass: 'iradio_square-green'
                                                    }).on('ifChecked', function (e) {
                                                        LineList = $(this).val();
                                                    }).end();
                                                }
                                                else {
                                                    toastr.warning('' + returndate.responseText + '');
                                                }
                                            },
                                            error: function (returndate) {
                                                toastr.error('Error inesperado contactar a sistemas.');
                                            }
                                        });
                                    }).end();
                                }
                                else {
                                    toastr.warning('' + returndate.responseText + '');
                                }
                            },
                            error: function (returndate) {
                                toastr.error('Error inesperado contactar a sistemas.');
                            }
                        });
                    }).end();
                }
                else {
                    toastr.warning('' + returndate.responseText + '');
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }).end();
}

function CategoListEventEdit() {
    $('#DivListEdit').find('input[name="Level1Edit"]').iCheck({
        radioClass: 'iradio_square-green'
    }).on('ifChecked', function (e) {
        DivList = $(this).val();
        $("#PanelFamiEdit").click();
        $.ajax({
            type: "GET",
            url: "/MaClass/ActionGetMaClassById",
            data: { "parent": DivList, "level": 2 },
            success: function (returndate) {
                if (returndate.success == true) {
                    $("#DepListEdit").empty();
                    $("#FamiListEdit").empty();
                    $("#LineListEdit").empty();
                    DepList = 0;
                    FamiList = 0;
                    LineList = 0;
                    $.each(returndate.Json, function (i, v) {
                        $("#DepListEdit").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level2Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                    });
                    $('#DepListEdit').find('input[name="Level2Edit"]').iCheck({
                        radioClass: 'iradio_square-green'
                    }).on('ifChecked', function (e) {
                        DepList = $(this).val();
                        $.ajax({
                            type: "GET",
                            url: "/MaClass/ActionGetMaClassById",
                            data: { "parent": DepList, "level": 3 },
                            success: function (returndate) {
                                if (returndate.success == true) {
                                    $("#FamiListEdit").empty();
                                    $("#LineListEdit").empty();
                                    FamiList = 0;
                                    LineList = 0;
                                    $.each(returndate.Json, function (i, v) {
                                        $("#FamiListEdit").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level3Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                    });
                                    $('#FamiListEdit').find('input[name="Level3Edit"]').iCheck({
                                        radioClass: 'iradio_square-green'
                                    }).on('ifChecked', function (e) {
                                        FamiList = $(this).val();
                                        $.ajax({
                                            type: "GET",
                                            url: "/MaClass/ActionGetMaClassById",
                                            data: { "parent": FamiList, "level": 4 },
                                            success: function (returndate) {
                                                if (returndate.success == true) {
                                                    $("#LineListEdit").empty();
                                                    LineList = 0;
                                                    $.each(returndate.Json, function (i, v) {
                                                        $("#LineListEdit").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level4Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                                    });
                                                    $('#LineListEdit').find('input[name="Level4Edit"]').iCheck({
                                                        radioClass: 'iradio_square-green'
                                                    }).on('ifChecked', function (e) {
                                                        LineList = $(this).val();
                                                    }).end();
                                                }
                                                else {
                                                    toastr.warning('' + returndate.responseText + '');
                                                }
                                            },
                                            error: function (returndate) {
                                                toastr.error('Error inesperado contactar a sistemas.');
                                            }
                                        });
                                    }).end();
                                }
                                else {
                                    toastr.warning('' + returndate.responseText + '');
                                }
                            },
                            error: function (returndate) {
                                toastr.error('Error inesperado contactar a sistemas.');
                            }
                        });
                    }).end();
                }
                else {
                    toastr.warning('' + returndate.responseText + '');
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }).end();
}

var count = 0;
var table = $('#tblItems').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [3, 13],
            visible: false,
            searchable: false
        },
        {
            targets: [0],
            orderable: false,
            data: null,
            width: "1%",
            defaultContent: '<button class="btn btn-xs btn-outline btn-warning" id="bt1"><i class="fa fa-pencil"></i> Editar</button>'
        },
        {
            targets: [14],
            orderable: false,
            data: null,
            width: "1%",
            defaultContent: '<button class="btn btn-xs btn-outline btn-info" id="bt2"><i class="fa fa-info-circle"></i> Detalle</button>'

        }
    ],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Productos', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Productos', className: 'btn-sm', orientation: 'landscape', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8]
            },
            customize: function (doc) {
                var cols = [];
                cols[0] = { text: 'Left part', alignment: 'left', margin: [20] };
                cols[1] = { text: 'Right part', alignment: 'right', margin: [0, 0, 20] };
                var objFooter = {};
                objFooter['columns'] = cols;
                doc['footer'] = objFooter;
                doc.content.splice(1, 0,
                    {
                        margin: [0, 0, 0, 12],
                        alignment: 'center'
                    }
                );
            }
        },
        {
            extend: 'print', text: 'Imprimir', className: 'btn-sm'
        },
    ],
    columns:
        [
            { data: null },
            { data: 'part_number' },
            { data: 'part_description' },
            { data: 'unit' },
            { data: 'part_type' },
            { data: 'buyer_division' },
            { data: 'active_flag' },
            { data: 'weight_flag' },
            { data: 'part_iva' },
            { data: 'part_iva_sale' },
            { data: 'part_ieps' },
            { data: 'part_number_sat' },
            { data: 'unit_sat' },
            { data: 'combo_flag' },
            { data: null }
        ],
});

$('#tblItems tbody').on('click', '#bt1', function (e) {
    values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'part_number':
                $.ajax({
                    type: "GET",
                    url: "/Item/ActionGetItemById",
                    data: { "partNumber": value, "parameter": "GetToEdit" },
                    success: function (response) {
                        var json = response.purch005;
                        if (json != null) {
                            ChangeTextMenu('Editar Producto');
                            $('#TableProduct').addClass("hide");
                            $('#PanelEdit').removeClass("hide");
                            $('#PanelEdit').animatePanel();
                            $('#txtPartNo').val(json.part_number);
                            $('#txtPartName').val(json.part_name);
                            $('#txtUnit').val(json.unit).trigger('change');
                            $('#txtType').val(json.part_type).trigger('change');
                            $('#txtDivision').val(json.buyer_division).trigger('change');
                            $('#txtIVA').val(json.part_iva).trigger('change');
                            $('#txtIVASale').val(json.part_iva_sale).trigger('change');
                            $('#txtIVASale').val(json.part_iva_sale);
                            $('#txtExpDays').val(json.expiration_days);
                            $('#txtIEPS').val(json.part_ieps).trigger('change');
                            if (json.part_number_sat != null) {
                                $("#codeSATedit").select2({
                                    minimumInputLength: 3,
                                    allowClear: true,
                                    placeholder: "Seleccione una opción",
                                    initSelection: function (element, callback) {
                                        callback({ id: json.part_number_sat, text: json.part_number_sat });
                                    },
                                    ajax: {
                                        url: "/Item/ActionGetAllSATCatalogoProdServ",
                                        dataType: 'json',
                                        type: "GET",
                                        quietMillis: 50,
                                        data: function (Filter) {
                                            return {
                                                Filter: Filter
                                            };
                                        },
                                        results: function (data) {
                                            return {
                                                results: $.map(data.Json, function (item) {
                                                    return {
                                                        text: item.Descripcion_ProdServ,
                                                        id: item.c_ClaveProdServ
                                                    }
                                                }),
                                            };
                                        }
                                    }
                                });
                                $('#codeSATedit').val(json.part_number_sat).trigger('change');
                            }
                            else {
                                $('#codeSATedit').empty();
                            }
                            $("#UMSATDropEdit").select2({
                                minimumInputLength: 3,
                                allowClear: true,
                                placeholder: "Seleccione una opción",
                                initSelection: function (element, callback) {
                                    callback({ id: json.unit_sat, text: json.unit_sat });
                                },
                                ajax: {
                                    url: "/Item/ActionGetAllSATum",
                                    dataType: 'json',
                                    type: "GET",
                                    quietMillis: 50,
                                    data: function (Filter) {
                                        return {
                                            Filter: Filter
                                        };
                                    },
                                    results: function (data) {
                                        return {
                                            results: $.map(data.Json, function (item) {
                                                return {
                                                    text: item.nombre,
                                                    id: item.c_ClaveUnidad
                                                }
                                            }),
                                        };
                                    }
                                }
                            });
                            $('#UMSATDropEdit').val(json.unit_sat).trigger('change');
                            if (json.active_flag == "True") {
                                $('#cbActive').prop("checked", true);
                            }
                            else {
                                $('#cbActive').prop("checked", false);
                            }
                            if (json.weight_flag == "False") {
                                $('#cbWeight').prop("checked", false);
                            }
                            else {
                                $('#cbWeight').prop("checked", true);
                            }
                            if (json.combo_flag == false) {
                                $('#cbCombo').prop("checked", false);
                            }
                            else {
                                $('#cbCombo').prop("checked", true);
                            }
                            if (json.extra_items_flag == false) {
                                $('#cbExtraItem').prop("checked", false);
                            }
                            else {
                                $('#cbExtraItem').prop("checked", true);
                            }
                            $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
                            $('#cbActive').iCheck('update');
                            $('#cbWeight').iCheck('update');
                            $('#cbCombo').iCheck('update');
                            $('#cbExtraItem').iCheck('update');
                            CategoListEventEdit();
                            $('#DivListEdit').find('input[value="' + json.level_header + '"]').prop('checked', true).iCheck('update');
                            $.each(response.JsonLevel2, function (i, v) {
                                $("#DepListEdit").append(" <li class='list-group-item '><p class='radio'><input type='radio' name='Level2Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                            });
                            $.each(response.JsonLevel3, function (i, v) {
                                $("#FamiListEdit").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level3Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                            });
                            $.each(response.JsonLevel4, function (i, v) {
                                $("#LineListEdit").append(" <li class='list-group-item '><p class='radio'><input type='radio' name='Level4Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                            });
                            $('#DepListEdit').find('input[name="Level2Edit"]').iCheck({
                                radioClass: 'iradio_square-green'
                            }).on('ifChecked', function (e) {
                                DepList = $(this).val();
                                $.ajax({
                                    type: "GET",
                                    url: "/MaClass/ActionGetMaClassById",
                                    data: { "parent": DepList, "level": 3 },
                                    success: function (returndate) {
                                        if (returndate.success == true) {
                                            $("#FamiListEdit").empty();
                                            $("#LineListEdit").empty();
                                            FamiList = 0;
                                            LineList = 0;
                                            $.each(returndate.Json, function (i, v) {
                                                $("#FamiListEdit").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level3Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                            });
                                            $('#FamiListEdit').find('input[name="Level3Edit"]').iCheck({
                                                radioClass: 'iradio_square-green'
                                            }).on('ifChecked', function (e) {
                                                FamiList = $(this).val();
                                                $.ajax({
                                                    type: "GET",
                                                    url: "/MaClass/ActionGetMaClassById",
                                                    data: { "parent": FamiList, "level": 4 },
                                                    success: function (returndate) {
                                                        if (returndate.success == true) {
                                                            $("#LineListEdit").empty();
                                                            LineList = 0;
                                                            $.each(returndate.Json, function (i, v) {
                                                                $("#LineListEdit").append(" <li class='list-group-item '><p class='radio'><input type='radio' name='Level4Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                                            });
                                                            $('#LineListEdit').find('input[name="Level4Edit"]').iCheck({
                                                                radioClass: 'iradio_square-green'
                                                            }).on('ifChecked', function (e) {
                                                                LineList = $(this).val();
                                                            }).end();
                                                        }
                                                        else {
                                                            toastr.warning('' + returndate.responseText + '');
                                                        }
                                                    },
                                                    error: function (returndate) {
                                                        toastr.error('Error inesperado contactar a sistemas.');
                                                    }
                                                });
                                            }).end();
                                        }
                                        else {
                                            toastr.warning('' + returndate.responseText + '');
                                        }
                                    },
                                    error: function (returndate) {
                                        toastr.error('Error inesperado contactar a sistemas.');
                                    }
                                });
                            }).end();
                            $('#FamiListEdit').find('input[name="Level3Edit"]').iCheck({
                                radioClass: 'iradio_square-green'
                            }).on('ifChecked', function (e) {
                                FamiList = $(this).val();
                                $.ajax({
                                    type: "GET",
                                    url: "/MaClass/ActionGetMaClassById",
                                    data: { "parent": FamiList, "level": 4 },
                                    success: function (returndate) {
                                        if (returndate.success == true) {
                                            $("#LineListEdit").empty();
                                            LineList = 0;
                                            $.each(returndate.Json, function (i, v) {
                                                $("#LineListEdit").append(" <li class='list-group-item'><p class='radio'><input type='radio' name='Level4Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                                            });
                                            $('#LineListEdit').find('input[name="Level4Edit"]').iCheck({
                                                radioClass: 'iradio_square-green'
                                            }).on('ifChecked', function (e) {
                                                LineList = $(this).val();
                                            }).end();
                                        }
                                        else {
                                            toastr.warning('' + returndate.responseText + '');
                                        }
                                    },
                                    error: function (returndate) {
                                        toastr.error('Error inesperado contactar a sistemas.');
                                    }
                                });
                            }).end();
                            $('#LineListEdit').find('input[name="Level4Edit"]').iCheck({
                                radioClass: 'iradio_square-green'
                            }).on('ifChecked', function (e) {
                                LineList = $(this).val();
                            }).end();
                            $('#DepListEdit').find('input[value="' + json.level1_code + '"]').prop('checked', true).iCheck('update');
                            $('#FamiListEdit').find('input[value="' + json.level2_code + '"]').prop('checked', true).iCheck('update');
                            $('#LineListEdit').find('input[value="' + json.level3_code + '"]').prop('checked', true).iCheck('update');
                            DivList = json.level_header;
                            DepList = json.level1_code;
                            FamiList = json.level2_code;
                            LineList = json.level3_code;
                        }
                    },
                    error: function (response) {
                        toastr.error('Error inesperado contactar a sistemas.' + response + '');
                    }
                });
                break;
        }
    });
});

$('#tblItems tbody').on('click', '#bt2', function (e) {
    values = table.row($(this).parents('tr')).data();
    //agrege parameter porque si no truena otra vista si no funciona quitar esa variable parameter
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'part_number':
                $.ajax({
                    type: "GET",
                    url: "/Item/ActionGetItemById",
                    data: { "partNumber": value, parameter: "OCD" },
                    success: function (response) {
                        var json = response.purch005;
                        if (json != null) {
                            $('#txtItemName').html(json.part_description);
                            $('#txtItemPartNumber').html(json.part_number);
                            $('#txtItemType').html(json.part_type);
                            $('#Dunit').html(json.unit);
                            $('#division').html(json.buyer_division);
                            $('#description').html(json.part_name);
                            if (json.active_flag == "True") {
                                $('#active').prop("checked", true);
                                $('#activeCheck').html('Activo');
                            }
                            else {
                                $('#active').prop("checked", false);
                                $('#activeCheck').html('No Activo');
                            }
                            if (json.weight_flag == "False") {
                                $('#weight').prop("checked", false);
                                $('#weightCheck').html('No Pesado');
                            }
                            else {
                                $('#weight').prop("checked", true);
                                $('#weightCheck').html('Pesado');
                            }
                            if (json.extra_items_flag == false) {
                                $('#cbExtraItem').prop("checked", false);
                            }
                            else {
                                $('#cbExtraItem').prop("checked", true);
                            }
                            $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
                            $('#active').iCheck('update');
                            $('#weight').iCheck('update');
                            $('#active').iCheck('disable');
                            $('#weight').iCheck('disable');
                            $('#SATUnit').html(json.unit_sat);
                            $('#SATPartNumber').html(json.part_number_sat);
                            $('#IVA').html(json.part_iva);
                            $('#IVASale').html(json.part_iva_sale);
                            $('#IEPS').html(json.part_ieps);
                            $('#cuser').html(json.cuser);
                            $('#cdate').html(json.cdate);
                            $('#showItem').modal('show');
                        }
                    },
                    error: function (response) {
                        toastr.error('Error inesperado contactar a sistemas.' + response + '');
                    }
                });
                break;
        }
    });
});

$('.list-purch004').slimScroll({
    height: '200px'
});

function ChangeTextMenu(s) {
    $('#MenuLeftAction').html(s);
}

function Clear() {
    $('#divisionSAT').val('').trigger('change');
    $('#unit').val('').trigger('change');
    $('#txtExpDays').val('');
    $('#part_iva').val('').trigger('change');
    $('#part_iva_sale').val('').trigger('change');
    $('#part_ieps').val('').trigger('change');
    $('#part_type').val('').trigger('change');
    $('#buyer_division').val('').trigger('change');

    $("#formEdit")[0].reset();
    $('#PanelEdit').addClass("hide");
    $('#TableProduct').removeClass("hide");
    $('#TableProduct').animatePanel();
    $("#form")[0].reset();
    $("#part_number").removeAttr("readonly");
    $("#part_number").focus();
    $("#collapseExample").hide("hide");
    $('#codeSAT').val('').trigger('change');
    $('#codeSAT').select2('val', '');
    $('#UMSATDrop').val('').trigger('change');
    $('#UMSATDrop').select2('val', '');
    $('#ERPOldSupplier').val('').trigger('change');
    $('#ERPDistrType').val('').trigger('change');
    $('#ERPItemGrup').val('').trigger('change');
    $('#ERPFamili').val('').trigger('change');
    $('#ERPItemUm').val('').trigger('change');
    $('#ERPFamili').attr('readonly', true);

    //No preguntes por que en el código de abajo
    $('#cbActive').iCheck('check');
    $('#cbActive').iCheck('uncheck');
    $('#cbWeight').iCheck('check');
    $('#cbWeight').iCheck('uncheck');
    $('#cbCombo').iCheck('check');
    $('#cbCombo').iCheck('uncheck');
    $('#cbExtraItem').iCheck('check');
    $('#cbExtraItem').iCheck('uncheck');
    $('#cbActiveNew').iCheck('check');
    $('#cbActiveNew').iCheck('uncheck');
    $('#cbweightNew').iCheck('check');
    $('#cbweightNew').iCheck('uncheck');
    $('#cbComboNew').iCheck('check');
    $('#cbComboNew').iCheck('uncheck');
    $('#cbExtraItemNew').iCheck('check');
    $('#cbExtraItemNew').iCheck('uncheck');

    ClearLists();
    DivList = 0;
}

function ClearLists() {
    DepList = 0;
    FamiList = 0;
    LineList = 0;
    $("#DepList").empty();
    $("#FamiList").empty();
    $("#LineList").empty();
    $("#DepListEdit").empty();
    $("#FamiListEdit").empty();
    $("#LineListEdit").empty();

    $('#cbActive').iCheck('update');
    $('#cbWeight').iCheck('update');
    $('#cbCombo').iCheck('update');
    $('#cbExtraItem').iCheck('update');

    $('#groupSATedit').val('').trigger('change');
    $('#claseSATedit').val('').trigger('change');
    $('#codeSATedit').val('').trigger('change');
}

var validator = $("#form").validate({
    ignore: [],
    rules: {
        part_number: {
            required: true,
            maxlength: 20
        },
        part_description: {
            required: true,
            minlength: 4
        },
        part_iva: {
            required: true
        },
        part_iva_sale: {
            required: true
        },
        part_ieps: {
            required: true
        },
        unit: {
            required: true
        },
        expiration_days: {
            required: true
        },
        part_type: {
            required: true
        },
        buyer_division: {
            required: true
        },
        codeSAT: {
            required: true
        },
        UMSATDrop: {
            required: true
        },
        ERPOldSupplier: {
            required: false
        },
        ERPDistrType: {
            required: false
        },
        ERPItemGrup: {
            required: false
        },
        ERPFamili: {
            required: false
        },
        ERPItemUm: {
            required: false
        }
    },
    messages: {
        part_number: {
            required: "El Campo no puede ser nulo",
            maxlength: "Favor de ingresar menos de 20 caracteres",
        },
        part_description: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
        },
        part_iva: {
            required: "El Campo no puede ser nulo"
        },
        part_iva_sale: {
            required: "El Campo no puede ser nulo"
        },
        part_ieps: {
            required: "El Campo no puede ser nulo"
        },
        unit: {
            required: "El Campo no puede ser nulo"
        },
        expiration_days: {
            required: "El Campo no puede ser nulo"
        },
        part_type: {
            required: "El Campo no puede ser nulo"
        },
        buyer_division: {
            required: "El Campo no puede ser nulo"
        },
        codeSAT: {
            required: "El Campo no puede ser nulo"
        },
        UMSATDrop: {
            required: "El Campo no puede ser nulo"
        },
        ERPOldSupplier: {
            required: "El Campo no puede ser nulo"
        },
        ERPDistrType: {
            required: "El Campo no puede ser nulo"
        },
        ERPItemGrup: {
            required: "El Campo no puede ser nulo"
        },
        ERPFamili: {
            required: "El Campo no puede ser nulo"
        },
        ERPItemUm: {
            required: "El Campo no puede ser nulo"
        }
    }
});

function AddItem() {
    var part_descriptionVar = $('#part_description').val();
    $('#part_description').val(part_descriptionVar.trim());
    if (!validator.element("#part_description")) {
        document.getElementById("part_description").focus();
    }
    if (!validator.element("#part_number")) {
        document.getElementById("part_number").focus();
    }
    if (!validator.element("#txtExpDays")) {
        document.getElementById("txtExpDays").focus();
    }
    //elimine esto del final: && validator.element("#ERPOldSupplier") && validator.element("#ERPDistrType") && validator.element("#ERPItemGrup") && validator.element("#ERPFamili") && validator.element("#ERPItemUm")
    if (validator.element("#part_number") && validator.element("#part_description") && validator.element("#unit") && validator.element("#txtExpDays") && validator.element("#part_type") && validator.element("#buyer_division") && validator.element("#part_iva") && validator.element("#part_iva_sale") && validator.element("#part_ieps") && validator.element("#codeSAT") && validator.element("#UMSATDrop") ) {
        if (DivList > 0 && DepList > 0) {
            swal({
                title: "¿Desea agregar un nuevo producto?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    StartLoading();
                    $('.ValidListProduct').addClass('hide');
                    $.ajax({
                        type: "POST",
                        url: "/Item/ActionInsertItem",
                        data: {
                            "part_number": $('#part_number').val().trim(), "part_description": $('#part_description').val(), "unit": $('#unit').val(), "expiration_days": $("#expiration_days").val(), "part_type": $('#part_type').val(), "buyer_division": $('#buyer_division').val(), "part_iva": $('#part_iva').val(), "part_iva_sale": $('#part_iva_sale').val(), "part_number_sat": $('#codeSAT').val(), "unit_sat": $('#UMSATDrop').val(), "active_flag": $('#cbActiveNew').is(":checked"), "weight_flag": $('#cbweightNew').is(":checked"), "level_header": DivList, "Level1_code": DepList, "Level2_code": FamiList, "Level3_code": LineList, "part_ieps": $('#part_ieps').val(), "combo_flag": $('#cbComboNew').is(":checked"), "extra_items_flag": $('#cbExtraItemNew').is(":checked")
                            , "WMSSupplier": $('#ERPOldSupplier').val()//, "WMSDistributionType": $('#ERPDistrType').val(), "WMSGroup": $('#ERPItemGrup').val(), "WMSFamili": $('#ERPFamili').val(), 
                            ,"WMSCurrency": $('#ERPmoneda').val(), "WMSUnitCost": $('#ERPCostoUnitario').val(), "WMSUnitPrice": $('#ERPPrecio').val(), "WMSMarg": $('#ERPMargen').val(), "WMSIvaPrice": $('#ERPPrecioIVA').val(), "WMSUm": $('#ERPItemUm').val()
                            
                        },
                        success: function (response) {
                            EndLoading();
                            if (response.success == true) {
                                toastr.success('Producto Agregado Correctamente!!<br>■ Código: ' + response.id + '<br>■ Nombre: ' + response.name + '');
                                Clear();
                                Update();
                            }
                            else {
                                EndLoading();
                                if (response.message == "Tu sesion termino.") {
                                    SessionFalse(response.message);
                                }
                                else {
                                    toastr.warning('' + response.message + '');
                                }
                            }
                        },
                        error: function (response) {
                            EndLoading();
                            toastr.error('Error inesperado contactar a sistemas.' + response + '');
                        }
                    });
                }
            });
        }
        else {
            $('.ValidListProduct').removeClass('hide');
        }
    }
    else {
        swal('Ingrese todos los campos por favor.',
            'Revise los campos que estan incorrectos o en blanco.',
            'error')
    }
}

var validatorEdit = $("#formEdit").validate({
    ignore: [],
    rules: {
        part_number: {
            required: true,
            maxlength: 20
        },
        part_description: {
            required: true,
            minlength: 4
        },
        part_iva: {
            required: true
        },
        part_iva_sale: {
            required: true
        },
        part_ieps: {
            required: true
        },
        unit: {
            required: true
        },
        txtType: {
            required: true
        },
        txtDivision: {
            required: true
        },
        codeSATedit: {
            required: true
        },
        UMSATDropEdit: {
            required: true
        }
    },
    messages: {
        part_iva: {
            required: "El Campo no puede ser nulo"
        },
        part_iva_sale: {
            required: "El Campo no puede ser nulo"
        },
        part_ieps: {
            required: "El Campo no puede ser nulo"
        },
        unit: {
            required: "El Campo no puede ser nulo"
        },
        part_number: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 2 caracteres",
        },
        part_description: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres"
        },
        txtType: {
            required: "El Campo no puede ser nulo"
        },
        txtDivision: {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres"
        },
        codeSATedit: {
            required: "El Campo no puede ser nulo"
        },
        UMSATDropEdit: {
            required: "El Campo no puede ser nulo"
        }
    }
});

function EditProduct() {
    var txtPartNameVar = $('#txtPartName').val();
    $('#txtPartName').val(txtPartNameVar.trim());
    if (validatorEdit.element("#txtPartNo") && validatorEdit.element("#txtPartName") && validator.element("#txtExpDays")  && validatorEdit.element("#txtUnit") && validatorEdit.element("#txtType") && validatorEdit.element("#txtDivision") && validatorEdit.element("#txtIVA") && validatorEdit.element("#txtIVASale") && validatorEdit.element("#txtIEPS") && validatorEdit.element("#codeSATedit") && validatorEdit.element("#UMSATDropEdit")) {
        if (DivList > 0 && DepList > 0) {
            swal({
                title: "¿Desea guardar los cambios?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $('.ValidListProductEdit').addClass('hide');
                    StartLoading();
                    $.ajax({
                        type: "POST",
                        //cambiar a ActionUpdateItem    por si falla.
                        url: "/Item/ActionUpdateItemPURCH004",
                        data: { "part_number": $('#txtPartNo').val(), "expiration_days": $('#txtExpDays').val(), "part_description": $('#txtPartName').val(), "unit": $('#txtUnit').val(), "part_type": $('#txtType').val(), "buyer_division": $('#txtDivision').val(), "part_iva": $('#txtIVA').val(), "part_iva_sale": $('#txtIVASale').val(), "part_number_sat": $('#codeSATedit').val(), "unit_sat": $('#UMSATDropEdit').val(), "active_flag": $('#cbActive').prop("checked"), "weight_flag": $('#cbWeight').prop("checked"), "level_header": DivList, "Level1_code": DepList, "Level2_code": FamiList, "Level3_code": LineList, "part_ieps": $('#txtIEPS').val(), "combo_flag": $('#cbCombo').prop("checked"), "extra_items_flag": $('#cbExtraItem').prop("checked") },
                        success: function (response) {
                            if (response.success == true) {
                                var ItemsReturn = response.itemlist;
                                $.each(ItemsReturn, function (index, value) {
                                    if (value.active_flag == true) {
                                        value.active_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                                    }
                                    else if (value.active_flag == false) {
                                        value.active_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                                    }
                                    if (value.weight_flag == true) {
                                        value.weight_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                                    }
                                    else if (value.weight_flag == false) {
                                        value.weight_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                                    }
                                    if (value.combo_flag == true) {
                                        value.combo_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                                    }
                                    else if (value.combo_flag == false) {
                                        value.combo_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                                    }
                                });
                                toastr.success('Producto Editado Correctamente!!<br>■ Código: ' + response.part_number + '<br>■ Nombre: ' + response.part_description + '');
                                table.clear().draw();
                                table.rows.add(response.itemlist)
                                table.columns.adjust().draw();
                                Clear();
                                EndLoading();
                            }
                            else {
                                EndLoading();
                                if (response.message == "Tu sesion termino.") {
                                    SessionFalse(response.message);
                                }
                                else {
                                    toastr.warning('' + response.message + '');
                                }
                            }
                        },
                        error: function (response) {
                            toastr.error('Error inesperado contactar a sistemas.' + response + '');
                            EndLoading();
                        }
                    });
                }
            });
        }
        else {
            $('.ValidListProductEdit').removeClass('hide');
        }
    }
}

function showModal(s) {
    $("#formModalAddMaClass")[0].reset();
    $("#ModalDescription").html('Agregar una nueva categoria de producto.');
    switch (s) {
        case "Department":
            $.ajax({
                type: "GET",
                url: "/MaClass/ActionGetMaClassById",
                data: { "parent": 0, "level": 1 },
                success: function (returndate) {
                    if (returndate.success == true) {
                        FillDropdown("CatalogueMaClass", returndate.JsonDrop);
                    }
                }
            });
            $('#IdCategoriDiv').removeClass('hide');
            $('#ModalAddMaClass').modal('show');
            $("#ModalTitle").html('<i class="fa fa-code-fork"></i> Nueva Categoria de DEPARTAMENTO');
            $("#DromModalAddMaClass").html('DEPARTAMENTO');
            $('#MaClassLevel').val(2);
            break;
        case "Families":
            $.ajax({
                type: "GET",
                url: "/MaClass/ActionGetMaClassById",
                data: { "parent": 0, "level": 2 },
                success: function (returndate) {
                    if (returndate.success == true) {
                        FillDropdown("CatalogueMaClass", returndate.JsonDrop);
                    }
                }
            });
            $('#IdCategoriDiv').removeClass('hide');
            $('#ModalAddMaClass').modal('show');
            $("#ModalTitle").html('<i class="fa fa-code-fork"></i> Nueva Categoria de FAMILIA');
            $("#DromModalAddMaClass").html('FAMILIA');
            $('#MaClassLevel').val(3);
            break;
        case "Lines":
            $.ajax({
                type: "GET",
                url: "/MaClass/ActionGetMaClassById",
                data: { "parent": 0, "level": 3 },
                success: function (returndate) {
                    if (returndate.success == true) {
                        FillDropdown("CatalogueMaClass", returndate.JsonDrop);
                    }
                }
            });
            $('#IdCategoriDiv').removeClass('hide');
            $('#ModalAddMaClass').modal('show');
            $("#ModalTitle").html('<i class="fa fa-code-fork"></i> Nueva Categoria de LINEA');
            $("#DromModalAddMaClass").html('LINEA');
            $('#MaClassLevel').val(4);
            break;
        default:
            FillDropdown("CatalogueMaClass", null);
            $('#IdCategoriDiv').addClass('hide');
            $('#ModalAddMaClass').modal('show');
            $("#ModalTitle").html('<i class="fa fa-code-fork"></i> Nueva Categoria de DIVISION');
            $('#MaClassLevel').val(1);
    }
    $('#CatalogueMaClass').val($("#CatalogueMaClass :first").val()).trigger('change');
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        for (var i in vData) {
            vItems.push('<option value="' + vData[i].Value + '">' + vData[i].Text + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Seleccione una opción</option>');
        $('#' + selector).empty();
    }
}

var ValidatorMaClassForm = $("#formModalAddMaClass").validate({
    rules: {
        MaClassName: {
            required: true,
            minlength: 4,
            maxlength: 50
        },
        CatalogueMaClass: {
            required: true
        },
        MaClassDescription: {
            required: true,
            minlength: 4,
            maxlength: 50
        }
    },
    messages: {
        MaClassName: {
            required: "El Campo no puede ser nulo",
            minlength: "Ingrese un más de 4 caracteres.",
            maxlength: "Ingrese un nombre razonable."
        },
        CatalogueMaClass: {
            required: "El Campo no puede ser nulo"
        },
        MaClassDescription: {
            required: "El Campo no puede ser nulo",
            minlength: "Ingrese un más de 4 caracteres.",
            maxlength: "Ingrese un nombre razonable."
        }
    }
});

function AddMaClass() {
    var validatorForm = false;
    var MaClassNameVar = $('#MaClassName').val();
    var MaClassDescriptionVar = $('#MaClassDescription').val();

    $('#MaClassName').val(MaClassNameVar.trim());
    $('#MaClassDescription').val(MaClassDescriptionVar.trim());

    var levelMaClass = $('#MaClassLevel').val();
    if (levelMaClass == 1) {
        validatorForm = true;
    } else {
        var catologue = $('#CatalogueMaClass').val();
        if (catologue === "" || catologue === null) {
            validatorForm = false;
            toastr.warning("Selecciona una valor de la lista.");
        } else {
            validatorForm = true;
        }
    }

    if (ValidatorMaClassForm.element("#CatalogueMaClass") && ValidatorMaClassForm.element("#MaClassName") && ValidatorMaClassForm.element("#MaClassDescription") && validatorForm) {
        var ParentValue = 0
        if ($('#CatalogueMaClass').val() != null)
            ParentValue = $('#CatalogueMaClass').val();

        $.ajax({
            type: "POST",
            url: "/MaClass/ActionAddMaClass",
            data: { "ClassName": $('#MaClassName').val(), "Description": $('#MaClassDescription').val(), "parent": ParentValue, "level": $('#MaClassLevel').val() },
            success: function (response) {
                if (response.success == true) {
                    toastr.success('Clasificacion Agregada Correctamente!!');
                    $('#CatalogueMaClass').val($("#CatalogueMaClass :first").val()).trigger('change');
                    $("#DivList").empty();
                    $("#DivListEdit").empty();
                    ClearLists();
                    $.each(response.Json, function (i, v) {
                        $("#DivList").append(" <li class='list-group-item'><p class='radio'><input id='level_header' type='radio' name='Level1' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                    });
                    $.each(response.Json, function (i, v) {
                        $("#DivListEdit").append(" <li class='list-group-item'><p class='radio'><input id='level_headerEdit' type='radio' name='Level1Edit' value='" + v.class_id + "'>&nbsp;" + v.class_name + " </p></li>");
                    });
                    CategoListEvent();
                    CategoListEventEdit();
                    $("#formModalAddMaClass")[0].reset();
                    $('#ModalAddMaClass').modal('hide');
                }
                else {
                    toastr.warning('' + response.message + '');
                    EndLoading();
                }
            },
            error: function (response) {
                toastr.error('Error inesperado contactar a sistemas.' + response + '');
                EndLoading();
            }
        });
    }
}

function ValidateItem(e) {
    if (validator.element("#part_number")) {
        var part = $('#part_number').val();
        if (e.keyCode == 13 && part.length >= 0 && !/^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/.test(part)) {
            $.ajax({
                url: "/Item/ActionGetItemById",
                type: "GET",
                data: { "partNumber": part },
                success: function (response) {
                    if (response.BarcodeExist) {
                        toastr.warning('Este producto ya se encuentra registrado <br>■ Código: ' + part + '');
                        Clear();
                    }
                    else if (response.purch005 != null) {
                        toastr.warning('Este producto ya se encuentra registrado <br>■ Código: ' + part + '');
                        Clear();
                    }
                    else {
                        $('#part_number').attr('readonly', true);
                        $("#collapseExample").show("show");
                        CategoListEvent();
                    }
                },
                error: function () {
                    toastr.warning('Error inesperado');
                }
            });
            return false;
        }
    }
}

function animationPanel(s) {
    $(s).click();
}

function GetFamili(s, e) {
    FillDropdown(e, null);
    $('#' + e + '').attr("readonly", false);
    if ($('#' + s + '').val() != 0) {
        $.ajax({
            type: "GET",
            url: "/Item/ActionGetFamili",
            data: { "Filter": $('#' + s + '').val() },
            success: function (returndate) {
                if (returndate.success == true) {
                    $('#' + e + '').select2({
                        allowClear: true,
                        placeholder: "Seleccione una opción",
                        data: $.map(returndate.Json, function (item) {
                            return {
                                text: item.level1_desc,
                                id: item.level1_code
                            }
                        }),
                    });
                }
                else {
                    toastr.warning('Error inesperado contactar a sistemas.');
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

function Getclase(s, e) {
    FillDropdown(e, null);
    if ($('#' + s + '').val() != 0) {
        $.ajax({
            type: "GET",
            url: "/Item/ActionGetAllSATClase",
            data: { "cve_grupo": $('#' + s + '').val() },
            success: function (returndate) {
                if (returndate.success == true) {
                    FillDropdown(e, returndate.Json);
                }
                else {
                    toastr.warning('Error inesperado contactar a sistemas.');
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

/*function Getcode(s, e) {
    FillDropdown(e, null);
    if ($('#' + s + '').val() != 0) {
        $.ajax({
            type: "GET",
            url: "/Item/ActionGetAllSATCatalogoProdServ",
            data: { "cve_clase": $('#' + s + '').val() },
            success: function (returndate) {
                if (returndate.success == true) {
                    FillDropdown(e, returndate.Json);
                }
                else {
                    toastr.warning('Error inesperado contactar a sistemas.');
                }
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
    if ($('#SATDivisionEdit').val() != "") {
        $("#select2-chosen-21").html("")
        $('#codeSATedit').val($("#codeSATedit :first").val()).trigger('change');
    }
}*/

function Update() {
    $.ajax({
        type: "GET",
        url: "/Item/ActionGetItems/",
        success: function (result) {
            $.each(result.Model, function (index, value) {
                if (value.active_flag == true) {
                    value.active_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                }
                else if (value.active_flag == false) {
                    value.active_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                }
                if (value.weight_flag == true) {
                    value.weight_flag = '<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                }
                else if (value.weight_flag == false) {
                    value.weight_flag = '<div checked><label><div class="icheckbox_square-green  disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
                }
            });
            table.clear();
            table.rows.add($(result.Model));
            table.columns.adjust().draw();
        },
        error: function (result) {
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

var ERPCal = false;
function CalculaMargen(s, e) {
    if (ERPCal == false) {
        ERPCal = true;
        var PCompra = parseFloat($('#ERPCostoUnitario').val()); //var PCompra = parseFloat(PurchPrice.GetText()).toFixed(2);
        var pVenta = parseFloat($('#ERPPrecio').val()); // var pVenta = parseFloat(SalesPrice.GetText(), 2).toFixed(2);
        var pmargin = 100 - ((PCompra * 100) / pVenta);
        pmargin = pmargin.toFixed(5);
        $('#ERPMargen').val(pmargin);
        ERPCal = false;
    }
    var IVATip = $('#part_iva').val();
    if (IVATip == 'N/A' | IVATip == null) {
        var iva = pVenta * 0 / 100 + pVenta;
        iva = iva.toFixed(5);
        $('#ERPPrecioIVA').val(iva);
    }
    else {
        var iva = pVenta * 16 / 100 + pVenta;
        iva = iva.toFixed(5);
        $('#ERPPrecioIVA').val(iva);
    }
}

function CalculaPrecioIVA(s, e) {
    if (ERPCal == false) {
        ERPCal = true;
        var pMargen = parseFloat($('#ERPMargen').val());
        var pVenta = parseFloat($('#ERPCostoUnitario').val());
        var pmargin1 = pVenta / (1 - (pMargen / 100));
        pmargin1 = pmargin1.toFixed(5);
        $('#ERPPrecio').val(pmargin1);
        ERPCal = false;
    }
    var pVenta = parseFloat($('#ERPPrecio').val());
    var IVATip = $('#part_iva').val()
    if (IVATip == 'N/A' | IVATip == null) {
        var iva = pVenta * 0 / 100 + pVenta;
        iva = iva.toFixed(5);
        $('#ERPPrecioIVA').val(iva);
    }
    else {
        var iva = pVenta * 16 / 100 + pVenta;
        iva = iva.toFixed(5);
        $('#ERPPrecioIVA').val(iva);
    }
}

function CalculaPrecio(s, e) {
    if (ERPCal == false) {
        ERPCal = true;
        var Precioiva = parseFloat($('#ERPPrecioIVA').val());

        var IVATip = $('#ASPxIVA').val();
        if (IVATip == 'N/A' | IVATip == null) {
            var resul = Precioiva / (1 + 0.00);
            resul = resul.toFixed(5);
            $('#ERPPrecio').val(resul);
            ERPCal = false;
            CalculaMargen();
        }
        else {
            var resul = Precioiva / (1 + 0.16);
            resul = resul.toFixed(5);
            $('#ERPPrecio').val(resul);
            ERPCal = false;
            CalculaMargen();
        }
    }
}

$("#ERPOldSupplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una opción",
    initSelection: function (element, callback) {
    },
    ajax: {
        url: "/Item/ActionGetERPSuppliersLike",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.commercial_name ,
                        id: item.supplier_id
                    }
                }),
            };
        }
    }
});

$(".CodeSatFilter").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una opción",
    ajax: {
        url: "/Item/ActionGetAllSATCatalogoProdServ",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Descripcion_ProdServ,
                        id: item.c_ClaveProdServ
                    }
                }),
            };
        }
    }
});

$(".UmSatFilter").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una opción",
    ajax: {
        url: "/Item/ActionGetAllSATumById",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.nombre,
                        id: item.c_ClaveUnidad
                    }
                }),
            };
        }
    }
});