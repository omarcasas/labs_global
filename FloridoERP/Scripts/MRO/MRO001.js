﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

function tab1() {
    $('#tab-1').animatePanel();
}

function tab2() {
    $('#tab-2').animatePanel();
}

/************************* TAB 1 **************************************** */
$("#EquipmentDrop").select2();
$("#Currency").select2();
$("#Currency").val($("#Currency :first").val()).trigger("change");

$("#EquipmentDrop").on('change', function () {
    var data = $("#EquipmentDrop option:selected").val();
    if (ServicesToPost.filter(x => x.equipment_id == data).length > 0) {
        toastr.error("Producto ya agregado, selecciona uno diferente");
        $("#EquipmentDrop").select2('open');
        $("#add_button").prop("disabled", true);
    }
    else
        $("#add_button").prop("disabled", false);
});

$("#SupplierDrop").select2();
$("#SiteDrop").select2();
$("#DepartmentDrop").select2();
var listAddServices = [];
var ServicesToPost = [];
var pdf = "";

$('#service_date').datepicker({
    autoclose: true,
    format: "mm/dd/yyyy",
    todayHighlight: true,
    startDate: new Date(),
    language: 'es'
}).on('changeDate', function () {
    GetEquipment($(this).val());
});

var tableProducts = $("#table_products").DataTable({
    responsive: true,
    autoWidth: true,
    paging: false,
    ordering: false,
    info: false,
    searching: false,
    oLanguage: { "sZeroRecords": "", "sEmptyTable": "", },
    columns:
        [
            { data: "equipment_id", width: "1%" },
            { data: "supplier_id", width: "1%" },
            { data: "service_cost", width: "1%" },
            { data: "currency", width: "1%" },
            { data: "service_department", width: "1%" },
            { data: "quotation_document", width: "1%" },
            { data: null, width: "1%", className: "text-center" }
        ],
    columnDefs:
        [
            {
                data: null,
                width: "1%",
                defaultContent: "<button type='button' class='btn btn-xs btn-outline btn-danger remove-button'><i class='fa fa-trash'></i> Eliminar</button>",
                targets: [6]
            },
            {
                targets: [2],
                width: "1%",
                render: function (data, type, row) {
                    return "$ " + data;
                }
            },
            {
                targets: [0, 1, 2, 3, 4, 5, 6],
                width: "1%"
            }
        ]
});

tableProducts.clear().draw();
tableProducts.columns.adjust().draw();

function GetEquipment() {
    var department = $("#DepartmentDrop").val();
    var sitecode = $("#SiteDrop").val();
    var servicedate = $('#service_date').val();

    if (department == "" || sitecode == "" || servicedate == "") {
        FillDropdown("EquipmentDrop", null);
        return false;
    }
    else {
        FillDropdown("EquipmentDrop", null);
        $.ajax({
            url: "/MaquineryEquipment/ActionGetEquipmentBySiteAndDepartment",
            type: "GET",
            data: { "Department": department, "SiteCode": sitecode, "ServiceDate": servicedate },
            success: function (response) {
                if (response.success) {
                    $.each(response.Json, function (index, value) {
                        var newOption = new Option(value.equipment_description, value.equipment_id, true, true);
                        $('#EquipmentDrop').append(newOption);
                    });
                }
                else if (response.message == "Termino tu sesion.")
                    SessionFalse(response.message);
                else {
                    FillDropdown("EquipmentDrop", null);
                    toastr.warning(response.message);
                }
            },
            error: function () {
                FillDropdown("EquipmentDrop", null);
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option  value="">Selecciona una opcion</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Selecciona una opcion</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

$('#add_button').on('click', function (e) {
    AddService();
});

function AddService() {
    var cost = $("#ServiceCost").val();
    if (!isNaN(cost) || cost != "") {
        if (parseFloat(cost) > 0 && parseFloat(cost) < 99999999) {
            if ($("#EquipmentDrop").val() == "") {
                toastr.warning('Selecciona un producto.');
                $("#EquipmentDrop").select2('open');
            }
            else if ($("#SupplierDrop").val() == "") {
                toastr.warning('Selecciona un Proveedor.');
                $("#SupplierDrop").select2('open');
            }
            else if ($("#ServiceCost").val() <= 0) {
                toastr.warning('Costo debe ser mayor a $0.');
                $("#ServiceCost").val(0);
                $("#ServiceCost").focus();
            }
            else if ($("#upload-file-info").text() != "")
                convertToBase64();
            else
                AddServicesToTableAndList("f");
        } else {
            toastr.warning('Ingrese un costo razonable.');
            $("#ServiceCost").val(0);
            $("#ServiceCost").focus();
        }
    } else {
        toastr.warning('Ingrese un costo razonable.');
        $("#ServiceCost").val(0);
        $("#ServiceCost").focus();
    }
}

function convertToBase64() {
    //Read File
    var selectedFile = document.getElementById("file").files;
    //Check File is not Empty
    if (selectedFile.length > 0) {
        // Select the very first file from list
        var fileToLoad = selectedFile[0];
        // FileReader function for read the file.
        var fileReader = new FileReader();
        var base64;
        // Onload of file read the file content
        fileReader.onload = function (fileLoadedEvent) {
            base64 = fileLoadedEvent.target.result;
            pdf = base64;
            AddServicesToTableAndList("t");
            return true;
        };
        fileReader.readAsDataURL(fileToLoad);
    }
    else
        AddServicesToTableAndList("f");
}

function AddServicesToTableAndList(parametro) {
    if (parametro == "t") {
        $("#btnRegistrar").prop("disabled", false);
        $("#SiteDrop").prop("disabled", true);
        var Service =
        {
            equipment_id: $("#EquipmentDrop option:selected").text(),
            supplier_id: $("#SupplierDrop option:selected").text(),
            service_cost: $("#ServiceCost").val(),
            service_department: $("#DepartmentDrop option:selected").text(),
            quotation_document: $("#upload-file-info").text(),
            currency: $("#Currency").val()
        };
        var ServicePost =
        {
            equipment_id: $("#EquipmentDrop").val(),
            supplier_id: $("#SupplierDrop").val(),
            service_cost: $("#ServiceCost").val(),
            service_department: $("#DepartmentDrop").val(),
            quotation_document: pdf,
            currency: $("#Currency").val()
        };
        listAddServices.push(Service);
        tableProducts.clear().draw();
        tableProducts.rows.add($(listAddServices));
        tableProducts.columns.adjust().draw();
        ServicesToPost.push(ServicePost);
        ClearProduct();
    }
    else {
        $("#btnRegistrar").prop("disabled", false);
        $("#SiteDrop").prop("disabled", true);
        var Service =
        {
            equipment_id: $("#EquipmentDrop option:selected").text(),
            supplier_id: $("#SupplierDrop option:selected").text(),
            service_cost: $("#ServiceCost").val(),
            service_department: $("#DepartmentDrop option:selected").text(),
            quotation_document: "",
            currency: $("#Currency").val()
        };
        var ServicePost =
        {
            equipment_id: $("#EquipmentDrop").val(),
            supplier_id: $("#SupplierDrop").val(),
            service_cost: $("#ServiceCost").val(),
            service_department: $("#DepartmentDrop").val(),
            quotation_document: "",
            currency: $("#Currency").val()
        };
        listAddServices.push(Service);
        tableProducts.clear().draw();
        tableProducts.rows.add($(listAddServices));
        tableProducts.columns.adjust().draw();
        ServicesToPost.push(ServicePost);
        ClearProduct();
    }
}

function ClearProduct() {
    $("#EquipmentDrop").val("").trigger("change");
    $("#SupplierDrop").val("").trigger("change");
    $("#ServiceCost").val(0);
    $("#upload-file-info").text("");
}

$('#btnRegistrar').on('click', function () {
    var servicedate = $("#service_date").val();
    var servicesite = $("#SiteDrop").val();
    var servicesitename = $("#SiteDrop option:selected").text();
    var department = $("#DepartmentDrop").val();

    if (servicedate == "") {
        toastr.error("Ingresa una fecha.");
        $("#service_date").focus();
    }
    else if (servicesite == "") {
        toastr.error("Selecciona una Tienda");
        $("#SiteDrop").select2('open');
    }
    else if (department == "") {
        toastr.error("Selecciona un Departamento.");
        $("#DepartmentDrop").select2('open');
    }
    else if (ServicesToPost.length == 0) {
        toastr.error("Ingresa al menos 1 maquinaria o equipo.");
        $("#EquipmentDrop").select2('open');
    }
    else {
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/ServicesCalendarHeader/ActionAddService",
            data: { "ServiceDate": servicedate, "ServiceSite": servicesite, "ServiceSiteName": servicesitename, "Department": department, "Services": ServicesToPost },
            success: function (response) {
                EndLoading();
                if (response.success) {
                    ClearForm();
                    swal({
                        title: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#55dd6b",
                        confirmButtonText: "Continuar"
                    });
                }
                else if (response.message == "Termino tu sesion.")
                    SessionFalse(response.message);
                else
                    toastr.error(response.message);
            },
            error: function () {
                EndLoading();
                toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.");
            }
        });
    }
});

function ClearForm() {
    $("#SiteDrop").val("").trigger("change");
    $("#DepartmentDrop").val("").trigger("change");
    $("#service_date").val("");
    $("#SiteDrop").prop("disabled", false);
    $("#btnRegistrar").prop("disabled", true);
    $("#Currency").val($("#Currency :first").val()).trigger("change");
    pdf = "";
    $('#table_products').DataTable().clear().draw();
    ServicesToPost = [];
    listAddServices = [];
}

$('#table_products tbody').on('click', '.remove-button', function () {
    var index = tableProducts.row($(this).parents('tr')).index();
    listAddServices.splice(index, 1);
    ServicesToPost.splice(index, 1);
    tableProducts.clear().draw();
    tableProducts.rows.add($(listAddServices));
    tableProducts.columns.adjust().draw();
    if (listAddServices.length == 0) {
        $("#btnRegistrar").prop("disabled", true);
        $("#SiteDrop").prop("disabled", false);
    }
    else
        $("#SiteDrop").prop("disabled", true);
});
/************************* TAB 1 **************************************** */


/************************* TAB 2 **************************************** */
$("#SelectSites").select2();

function searchService() {
    let status = 1 // status programado
    var url;
    var data;
    var begindate = $("#BeginDate").val();
    var enddate = $("#EndDate").val();
    var sites = $("#SelectSites").val();

    if (begindate != "" & enddate != "" || (begindate == "" & enddate == "")) {
        if (moment(begindate) <= moment(enddate) || (begindate == "" & enddate == "")) {
            if (sites == "0") {
                url = "/ServicesCalendarHeader/ActionGetServicesHeaderByDateAndStatusNotSite";
                data = { "start": begindate, "finish": enddate, "status": status }
                if (begindate == "" & enddate == "")
                    data = { "start": moment().add(-7, 'days').format("MM/DD/YYYY"), "finish": moment().add(1, 'days').format("MM/DD/YYYY"), "status": status }
            }
            else {
                url = "/ServicesCalendarHeader/ActionGetServicesHeaderByDateAndStatus";
                data = { "start": begindate, "finish": enddate, "status": status, site: sites }
                if (begindate == "" & enddate == "")
                    data = { "start": moment().add(-7, 'days').format("MM/DD/YYYY"), "finish": moment().add(1, 'days').format("MM/DD/YYYY"), "status": status, site: sites }
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (data == "error")
                        toastr.error("Error inesperado contactar a sistemas.");
                    else if (data.length == 0) {
                        table.clear();
                        table.rows.add($(data));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                        toastr.warning("No existen servicios programados en el rango de fecha");
                    }
                    else {
                        table.clear();
                        table.rows.add($(data));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                    }
                },
                error: function (returndate) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('BeginDate').value = "";
            document.getElementById('EndDate').value = "";
            $("#BeginDate").focus();
        }
    }
}

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    searchService();
});

$("#EndDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function (e) {
    searchService();
});

var service_id = 0;

//Tabla principal de servicios
var table = $('#TableTransfers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            class: "details-control",
            orderable: false,
            data: null,
            defaultContent: "",
            width: "1%"
        },
        { data: 'service_id', width: "1%" },
        { data: 'service_date', width: "1%" },
        { data: 'service_status_', width: "1%" },
        { data: 'site_name', width: "1%" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    }]
});

function Edit(serviceId) {
    $.ajax({
        url: "/ServicesCalendarHeader/ActionGetServicesCalendarDetail",
        type: "GET",
        data: { "ServiceId": serviceId },
        success: function (response) {
            if (response.response) {
                $("#ServiceIdEdit").val(response.model.service_id);
                $("#service_dateEdit").val(moment(response.model.service_date).format("MM/DD/YYYY"));
                $("#SiteDropEdit").val(response.model.site_code);
                $("#SiteDropEdit").trigger("change");
                $("#btnRegistrarEdit").prop("disabled", false);
                $("#SiteDropEdit").prop("disabled", true);

                response.model.ServicesCalendarDetails.forEach(function (item) {
                    var Service =
                    {
                        equipment_id: item.equipment_description,
                        supplier_id: item.supplier_name,
                        service_cost: item.service_cost,
                        service_department: item.service_department,
                        currency: item.currency,
                        quotation_document: item.quotation_document
                    };
                    var ServicePost =
                    {
                        equipment_id: item.equipment_id,
                        supplier_id: item.supplier_id,
                        service_cost: item.service_cost,
                        service_department: item.service_department,
                        currency: item.currency,
                        quotation_document: item.quotation_document
                    };

                    listAddServicesEdit.push(Service);
                    ServicesToPostEdit.push(ServicePost);
                });

                tableProductsEdit.clear().draw();
                tableProductsEdit.rows.add($(listAddServicesEdit));
                tableProductsEdit.columns.adjust().draw();

                ClearProductEdit();
                $("#ModalChange").modal();
            }
            else
                SessionFalse("Termino tu sesion.");
        },
        error: function () {
            toastr.error("Error inesperado contactar a sistemas.");
        }
    });
}

//Tabla detalles
function format(d) {
    service_id = d.service_id;
    var detailItems = "";
    var Apro = '';
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/ServicesCalendarHeader/ActionGetServicesDetail",
        data: { "id": d.service_id },
        type: "POST",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Terminó tu sesión");
            else {
                listAddServices = [];
                ServicesToPost = [];

                $.each(data, function (index, value) {
                    Apro = '<div class="row pull-right"><div class="col-lg-12">'
                    if (value.quotation_document != null)
                        detailItems += "<tr><td>" + value.equipment_description + "</td><td>" + value.supplier_name + "</td><td>$" + value.service_cost + "</td>" + "<td>" + value.currency + "</td>" + "<td>" + value.service_department + "</td><td><button class='btn btn-xs btn-outline btn-warning btn-edit text-center' type='button' onclick= viewDocument('" + value.quotation_document + "')><i class='fa fa-file-o'></i> Cotización</button></td>";
                    else
                        detailItems += "<tr><td>" + value.equipment_description + "</td><td>" + value.supplier_name + "</td><td>$" + value.service_cost + "</td>" + "<td>" + value.currency + "</td>" + "<td>" + value.service_department + "</td><td>No tiene cotizacón</td>" + "</td>";
                    if (d.service_status_ == "Programado")
                        Apro += '<button type="button" class="btn btn-sm btn-warning" onclick="Edit(' + value.service_id + ')"> <i class="fa fa-pencil"></i> Editar</button> </div></div>';
                });

                if (data.length != 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Equipo</th><th>Proveedor</th><th>Costo aproximado</th><th>Moneda</th><th>Departamento</th><th>Ver Cotización</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                    reloadStyleTable();
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Equipo</th><th>Proveedor</th><th>Costo aproximado</th><th>Moneda</th><th>Departamento</th><th>Ver Cotización</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail
}

//Recargar tabla
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: []
    });
}

var detailRows = [];
$('#TableTransfers tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

/************ Edicion ************/
$("#EquipmentDropEdit").select2();
$("#CurrencyEdit").select2();
$("#CurrencyEdit").val($("#CurrencyEdit :first").val()).trigger("change");

$("#EquipmentDropEdit").on('change', function () {
    var data = $("#EquipmentDropEdit option:selected").val();
    if (ServicesToPostEdit.filter(x => x.equipment_id == data).length > 0) {
        toastr.error("Producto ya agregado, selecciona uno diferente");
        $("#EquipmentDropEdit").select2('open');
        $("#add_buttonEdit").prop("disabled", true);
    }
    else
        $("#add_buttonEdit").prop("disabled", false);
});

$("#SupplierDropEdit").select2();
$("#SiteDropEdit").select2();
$("#DepartmentDropEdit").select2();
var listAddServicesEdit = [];
var ServicesToPostEdit = [];
var pdfEdit = "";

$('#service_dateEdit').datepicker({
    autoclose: true,
    format: "mm/dd/yyyy",
    todayHighlight: true,
    startDate: new Date(),
    language: 'es'
}).on('changeDate', function () {
    GetEquipmentEdit($(this).val());
});

var tableProductsEdit = $("#table_productsEdit").DataTable({
    responsive: true,
    autoWidth: true,
    paging: false,
    ordering: false,
    info: false,
    searching: false,
    oLanguage: { "sZeroRecords": "", "sEmptyTable": "", },
    columns:
        [
            { data: "equipment_id", width: "1%" },
            { data: "supplier_id", width: "1%" },
            { data: "service_cost", width: "1%" },
            { data: "currency", width: "1%" },
            { data: "service_department", width: "1%" },
            { data: "quotation_document", width: "1%" },
            { data: null, width: "1%", className: "text-center" }
        ],
    columnDefs:
        [
            {
                data: null,
                width: "1%",
                defaultContent: "<button type='button' class='btn btn-xs btn-outline btn-danger remove-buttonEdit'><i class='fa fa-trash'></i> Eliminar</button>",
                targets: [6]
            },
            {
                targets: [2],
                width: "1%",
                render: function (data, type, row) {
                    return "$ " + data;
                }
            },
            {
                targets: [0, 1, 2, 3, 4, 5, 6],
                width: "1%"
            }
        ]
});

tableProductsEdit.clear().draw();
tableProductsEdit.columns.adjust().draw();

function GetEquipmentEdit() {
    var department = $("#DepartmentDropEdit").val();
    var sitecode = $("#SiteDropEdit").val();
    var servicedate = $('#service_dateEdit').val();

    if (department == "" || sitecode == "" || servicedate == "") {
        FillDropdownEdit("EquipmentDropEdit", null);
        return false;
    }
    else {
        FillDropdownEdit("EquipmentDropEdit", null);
        $.ajax({
            url: "/MaquineryEquipment/ActionGetEquipmentBySiteAndDepartment",
            type: "GET",
            data: { "Department": department, "SiteCode": sitecode, "ServiceDate": servicedate },
            success: function (response) {
                if (response.success) {
                    $.each(response.Json, function (index, value) {
                        var newOption = new Option(value.equipment_description, value.equipment_id, true, true);
                        $('#EquipmentDropEdit').append(newOption);
                    });
                }
                else if (response.message == "Termino tu sesion.")
                    SessionFalse(response.message);
                else {
                    FillDropdownEdit("EquipmentDropEdit", null);
                    toastr.warning(response.message);
                }
            },
            error: function () {
                FillDropdownEdit("EquipmentDropEdit", null);
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

function FillDropdownEdit(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option  value="">Selecciona una opcion</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Selecciona una opcion</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

$('#add_buttonEdit').on('click', function (e) {
    AddServiceEdit();
});

function AddServiceEdit() {
    var cost = $("#ServiceCostEdit").val();
    if (!isNaN(cost) || cost != "") {
        if (parseFloat(cost) > 0 && parseFloat(cost) < 99999999) {
            if ($("#EquipmentDropEdit").val() == "") {
                toastr.warning('Selecciona un producto.');
                $("#EquipmentDropEdit").select2('open');
            }
            else if ($("#SupplierDropEdit").val() == "") {
                toastr.warning('Selecciona un Proveedor.');
                $("#SupplierDropEdit").select2('open');
            }
            else if ($("#ServiceCostEdit").val() <= 0) {
                toastr.warning('Costo debe ser mayor a $0.');
                $("#ServiceCostEdit").val(0);
                $("#ServiceCostEdit").focus();
            }
            else if ($("#upload-file-infoEdit").text() != "")
                convertToBase64Edit();
            else
                AddServicesToTableAndListEdit("f");
        } else {
            toastr.warning('Ingrese un costo razonable.');
            $("#ServiceCostEdit").val(0);
            $("#ServiceCostEdit").focus();
        }
    } else {
        toastr.warning('Ingrese un costo razonable.');
        $("#ServiceCostEdit").val(0);
        $("#ServiceCostEdit").focus();
    }
}

function convertToBase64Edit() {
    //Read File
    var selectedFile = document.getElementById("fileEdit").files;
    //Check File is not Empty
    if (selectedFile.length > 0) {
        // Select the very first file from list
        var fileToLoad = selectedFile[0];
        // FileReader function for read the file.
        var fileReader = new FileReader();
        var base64;
        // Onload of file read the file content
        fileReader.onload = function (fileLoadedEvent) {
            base64 = fileLoadedEvent.target.result;
            pdfEdit = base64;
            AddServicesToTableAndListEdit("t");
            return true;
        };
        fileReader.readAsDataURL(fileToLoad);
    }
    else
        AddServicesToTableAndListEdit("f");
}

function AddServicesToTableAndListEdit(parametro) {
    $("#btnRegistrarEdit").prop("disabled", false);
    $("#SiteDropEdit").prop("disabled", true);

    var Service =
    {
        equipment_id: $("#EquipmentDropEdit option:selected").text(),
        supplier_id: $("#SupplierDropEdit option:selected").text(),
        service_cost: $("#ServiceCostEdit").val(),
        service_department: $("#DepartmentDropEdit option:selected").text(),
        currency: $("#CurrencyEdit").val()
    };
    var ServicePost =
    {
        equipment_id: $("#EquipmentDropEdit").val(),
        supplier_id: $("#SupplierDropEdit").val(),
        service_cost: $("#ServiceCostEdit").val(),
        service_department: $("#DepartmentDropEdit").val(),
        currency: $("#CurrencyEdit").val()
    };

    if (parametro == "t") {
        Service.quotation_document = $("#upload-file-infoEdit").text();
        ServicePost.quotation_document = pdfEdit;
    }
    else {
        Service.quotation_document = "";
        ServicePost.quotation_document = "";
    }

    listAddServicesEdit.push(Service);
    tableProductsEdit.clear().draw();
    tableProductsEdit.rows.add($(listAddServicesEdit));
    tableProductsEdit.columns.adjust().draw();
    ServicesToPostEdit.push(ServicePost);
    ClearProductEdit();
}

function ClearProductEdit() {
    $("#EquipmentDropEdit").val("").trigger("change");
    $("#SupplierDropEdit").val("").trigger("change");
    $("#ServiceCostEdit").val(0);
    $("#upload-file-infoEdit").text("");
}

function UpdateServices() {
    let status = 1 // status programado
    var url;
    var data;
    var begindate = $("#BeginDate").val();
    var enddate = $("#EndDate").val();
    var sites = $("#SelectSites").val();

    if (begindate != "" & enddate != "" || (begindate == "" & enddate == "")) {
        if (moment(begindate) <= moment(enddate) || (begindate == "" & enddate == "")) {
            if (sites == "0") {
                url = "/ServicesCalendarHeader/ActionGetServicesHeaderByDateAndStatusNotSite";
                data = { "start": begindate, "finish": enddate, "status": status }
                if (begindate == "" & enddate == "")
                    data = { "start": moment().add(-7, 'days').format("MM/DD/YYYY"), "finish": moment().add(1, 'days').format("MM/DD/YYYY"), "status": status }
            }
            else {
                url = "/ServicesCalendarHeader/ActionGetServicesHeaderByDateAndStatus";
                data = { "start": begindate, "finish": enddate, "status": status, site: sites }
                if (begindate == "" & enddate == "")
                    data = { "start": moment().add(-7, 'days').format("MM/DD/YYYY"), "finish": moment().add(1, 'days').format("MM/DD/YYYY"), "status": status, site: sites }
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (data == "error")
                        toastr.error("Error inesperado contactar a sistemas.");
                    else {
                        table.clear();
                        table.rows.add($(data));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                    }
                },
                error: function (response) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('BeginDate').value = "";
            document.getElementById('EndDate').value = "";
            $("#BeginDate").focus();
        }
    }
}

$('#btnRegistrarEdit').on('click', function () {
    var serviceid = $("#ServiceIdEdit").val();
    var servicedate = $("#service_dateEdit").val();
    var servicesite = $("#SiteDropEdit").val();
    var servicesitename = $("#SiteDropEdit option:selected").text();

    if (servicedate == "") {
        toastr.error("Ingresa una fecha.");
        $("#service_dateEdit").focus();
    }
    else if (servicesite == "") {
        toastr.error("Selecciona una Tienda");
        $("#SiteDropEdit").select2('open');
    }
    else if (ServicesToPostEdit.length == 0) {
        toastr.error("Ingresa al menos 1 maquinaria o equipo.");
        $("#EquipmentDropEdit").select2('open');
    }
    else {
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/ServicesCalendarHeader/ActionUpdateService",
            data: { "ServiceId": serviceid, "ServiceDate": servicedate, "ServiceSite": servicesite, "ServiceSiteName": servicesitename, "Services": ServicesToPostEdit },
            success: function (response) {
                EndLoading();
                if (response.success) {
                    UpdateServices();
                    ClearFormEdit();
                    $("#ModalChange").modal("hide");
                    swal({
                        title: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#55dd6b",
                        confirmButtonText: "Continuar"
                    });
                }
                else if (response.message == "Termino tu sesion.")
                    SessionFalse(response.message);
                else
                    toastr.error(response.message);
            },
            error: function () {
                EndLoading();
                toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.");
            }
        });
    }
});

function ClearFormEdit() {
    $("#ServiceIdEdit").val("");
    $("#SiteDropEdit").val("").trigger("change");
    $("#DepartmentDropEdit").val("").trigger("change");
    $("#service_dateEdit").val("");
    $("#SiteDropEdit").prop("disabled", false);
    $("#btnRegistrarEdit").prop("disabled", true);
    $("#CurrencyEdit").val($("#CurrencyEdit :first").val()).trigger("change");
    pdfEdit = "";
    $('#table_productsEdit').DataTable().clear().draw();
    ServicesToPostEdit = [];
    listAddServicesEdit = [];
}

$('#table_productsEdit tbody').on('click', '.remove-buttonEdit', function () {
    var index = tableProductsEdit.row($(this).parents('tr')).index();
    listAddServicesEdit.splice(index, 1);
    ServicesToPostEdit.splice(index, 1);
    tableProductsEdit.clear().draw();
    tableProductsEdit.rows.add($(listAddServicesEdit));
    tableProductsEdit.columns.adjust().draw();
    if (listAddServicesEdit.length == 0) {
        $("#btnRegistrarEdit").prop("disabled", true);
        $("#SiteDropEdit").prop("disabled", false);
    }
    else
        $("#SiteDropEdit").prop("disabled", true);
});

$("#ModalChange").on("hidden.bs.modal", function () {
    ClearFormEdit();
});
/************ Edicion ************/

/************************* TAB 2 **************************************** */