﻿/// <reference path="../knockout-3.4.2.debug.js" />

window.setTimeout(function () { $("#fa").click(); }, 1000);

var Equipments = $('#Equipments').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[2, "asc"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Equipos', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Equipos', className: 'btn-sm', exportOptions: {
                columns: [2, 3, 4, 7, 8]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columnDefs: [
        {
            data: null,
            defaultContent: "<button class='btn btn-xs btn-outline btn-warning btn-edit' type='button'><i class='fa fa-pencil'></i> Editar</button>",
            targets: [0],
            className: "text-center",
            orderable: false,
            width: "1%"
        },
        {
            data: null,
            defaultContent: "<button class='btn btn-xs btn-outline btn-danger btn-remove' type='button'><i class='fa fa-trash'></i> Eliminar</button>",
            targets: [10],
            className: "text-center",
            orderable: false,
            width: "1%",
            render: function (data, type, full, meta) {
                if (full.FlagActive)
                    return `<button class='btn btn-xs btn-outline btn-danger btn-remove' type='button'><i class='fa fa-trash'></i> Eliminar</button>`;
                else
                    return `<button class='btn btn-xs btn-outline btn-success btn-remove' type='button'><i class='fa fa-check'></i> Activar</button>`;
            },
        },
        {
            targets: [9],
            className: "text-center",
            orderable: true,
            width: "1%",
            render: function (data, type, row) {
                return `<div checked><label><div class="icheckbox_square-green ${(data ? 'checked' : '')} disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>`;
            },
        }

        //'<div checked><label><div class="icheckbox_square-green checked disabled" style="position: relative;"><input type="checkbox" disabled="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></div>'
    ],
    columns: [
        { targets: 0, data: null, width: "1%" },
        { targets: 1, data: 'EquipmentId', width: "1%", visible: false },
        { targets: 2, data: 'EquipmentDescription', width: "1%" },
        { targets: 3, data: 'SerialNumber', width: "1%" },
        { targets: 4, data: 'SerialNumberSupplier', width: "1%" },
        { targets: 5, data: 'SiteCode', width: "1%", visible: false },
        { targets: 6, data: 'DepartmentId', width: "1%", visible: false },
        { targets: 7, data: 'Site', width: "1%" },
        { targets: 8, data: 'Department', width: "1%" },
        { targets: 9, data: 'FlagActive', width: "1%" },
        { targets: 10, data: null, width: "1%" },
    ]
});

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model.MaquineryEquipmentList)) {
    $('#TableDiv').animatePanel();
    Equipments.clear();
    Equipments.rows.add($(model.MaquineryEquipmentList));
    Equipments.columns.adjust().draw();
}
else {
    Equipments.clear();
    Equipments.columns.adjust().draw();
}

function viewModel() {
    var self = this;

    self.EquipmentId = ko.observable();
    self.SerialNumber = ko.observable();
    self.SerialNumberSupplier = ko.observable();
    self.EquipmentDescription = ko.observable();
    self.SiteCode = ko.observable();
    self.DepartmentId = ko.observable();

    self.Deparments = ko.observableArray(model.Deparments);
    self.Sites = ko.observableArray(model.Sites);

    self.SerialNumberSelected = ko.observable(false);
    self.SerialNumberSelectedSupplier = ko.observable(false);
    self.EquipmentDescriptionSelected = ko.observable(false);

    self.Clear = function () {
        self.EquipmentId("");
        self.SerialNumber("");
        self.SerialNumberSupplier("");
        self.EquipmentDescription("");
        self.SiteCode("");
        self.DepartmentId("");
    }

    self.Update = function (model) {
        $('#TableDiv').animatePanel();
        Equipments.clear();
        Equipments.rows.add($(model));
        Equipments.columns.adjust().draw();
    }

    self.Add = function () {
        if (self.Validate()) {
            StartLoading();
            $.ajax({
                url: "/MaquineryEquipment/ActionAddMaquineryEquipment/",
                type: "POST",
                data: ko.toJSON(self),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    EndLoading();
                    if (result.status) {
                        $('#ModalAdd').modal('hide');
                        toastr.success("Maquinaria/equipo registrado con exito");
                        self.Update(result.model);
                    }
                    else if (!result.status && !result.valid)
                        toastr.error("Número de serie ya existente.");
                    else
                        toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                },
                error: function (result) {
                    EndLoading();
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
    }

    self.Edit = function () {
        if (self.Validate()) {
            StartLoading();
            $.ajax({
                url: "/MaquineryEquipment/ActionUpdateMaquineryEquipment/",
                type: "POST",
                data: ko.toJSON(self),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    EndLoading();
                    if (result.status) {
                        $('#ModalChange').modal('hide');
                        toastr.success("Maquinaria/equipo actualizado con exito");
                        self.Update(result.model);
                    }
                    else if (!result.status && !result.valid)
                        toastr.error("Número de serie ya existente.");
                    else
                        toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                },
                error: function (result) {
                    EndLoading();
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
    }

    self.Get = function (Id) {
        $.ajax({
            url: "/MaquineryEquipment/ActionGetMaquineryEquipment/",
            type: "GET",
            data: { "EquipmentId": Id },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                EndLoading();
                if (result.status) {
                    self.EquipmentId(result.model.EquipmentId);
                    self.SerialNumber(result.model.SerialNumber);
                    self.SerialNumberSupplier(result.model.SerialNumberSupplier);
                    self.EquipmentDescription(result.model.EquipmentDescription);
                    self.SiteCode(result.model.SiteCode);
                    self.DepartmentId(result.model.DepartmentId);

                    $(".editSites").select2("val", self.SiteCode());
                    $(".editDepartments").select2("val", self.DepartmentId());
                    $('#ModalChange').modal('show');
                }
                else
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
            },
            error: function (result) {
                EndLoading();
                toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
            }
        });
    }

    self.RemoveActive = function (Id, FlagActive) {
        StartLoading();
        $.ajax({
            url: "/MaquineryEquipment/ActionRemoveMaquineryEquipment/",
            type: "GET",
            data: { "EquipmentId": Id, "FlagActive": FlagActive},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                EndLoading();
                if (result.status) {
                    toastr.success(`Maquinaria/equipo eliminado ${!FlagActive ? 'ACTIVADO' : 'DESACTIVADO'} con exito`);
                    self.Update(result.model);
                }
                else
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
            },
            error: function (result) {
                EndLoading();
                toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                self.Clear();
            }
        });
    }

    self.Validate = function () {
        var part_number = $.trim(self.SerialNumber());
        var supplier_part_number = $.trim(self.SerialNumberSupplier());
        var EquipmentDescrip = $.trim(self.EquipmentDescription());
        if (supplier_part_number == "") {
            self.SerialNumberSelectedSupplier(true);
            toastr.remove();
            toastr.error("Capture número de serie proveedor");
            return false;
        }
        if (supplier_part_number.length > 50) {
            self.SerialNumberSelectedSupplier(true);
            toastr.remove();
            toastr.error("Capture número de serie menor de 50 caracteres");
            return false;
        }
        if (part_number == "") {
            self.SerialNumberSelected(true);
            toastr.remove();
            toastr.error("Capture número de serie florido");
            return false;
        }
        if (part_number.length > 50) {
            self.SerialNumberSelected(true);
            toastr.remove();
            toastr.error("Capture número de serie menor de 50 caracteres");
            return false;
        }
        if (EquipmentDescrip == "") {
            self.EquipmentDescriptionSelected(true);
            toastr.remove();
            toastr.error("Capture descripción de maquinaria/equipo");
            return false;
        }
        if (EquipmentDescrip.length > 200) {
            self.EquipmentDescriptionSelected(true);
            toastr.remove();
            toastr.error("Capture descripción de maquinaria/equipo menor de 200 caracteres");
            return false;
        }
        if ($.trim(self.SiteCode()) == "") {
            toastr.remove();
            toastr.error("Seleccione un sitio");
            return false;
        }
        if ($.trim(self.DepartmentId()) == "") {
            toastr.remove();
            toastr.error("Seleccione un departamento");
            return false;
        }
        else {
            toastr.remove();
            return true;
        }
    }
}

var vmAdd = new viewModel();
var vmEdit = new viewModel();

ko.applyBindings(vmAdd, document.getElementById('ModalAdd'));
ko.applyBindings(vmEdit, document.getElementById('ModalChange'));

$('#ModalAdd').on('hidden.bs.modal', function () {
    vmAdd.Clear();
});

$('#ModalChange').on('hidden.bs.modal', function () {
    vmEdit.Clear();
});

$('#Equipments tbody').on('click', '.btn-edit', function () {
    var Id = Equipments.row($(this).parents('tr')).data().EquipmentId;
    vmEdit.Get(Id);
});

$('#Equipments tbody').on('click', '.btn-remove', function () {
    var Id = Equipments.row($(this).parents('tr')).data().EquipmentId;
    var EquipmentDescription = Equipments.row($(this).parents('tr')).data().EquipmentDescription;
    var FlagActive = Equipments.row($(this).parents('tr')).data().FlagActive;
    var text_swal = '';
    var text_format = '';
    var text_color = '';

    if (FlagActive) {
        text_swal = "Se ELIMINARÁ el equipo: " + EquipmentDescription;
        text_format = 'warning';
        text_color = '#DD6B55';
    }
    else {
        text_swal = "Se ACTIVARÁ el equipo: " + EquipmentDescription;
        text_format = 'success';
        text_color = '#55DD6B';
    }
      
    swal({
        title: "¿Esta seguro?",
        text: text_swal,
        type: text_format,
        closeOnConfirm: true,
        showCancelButton: true,
        confirmButtonColor: text_color,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            vmEdit.RemoveActive(Id, FlagActive);
    });
});

$(".sites, .departments, .editSites, .editDepartments").select2();