﻿/// <reference path="../knockout-3.4.2.debug.js" />

$(document).ready(function (model) {
    window.setTimeout(function () {
        $("#fa").click();
    }, 1000);

    $(".select2").select2();

    $(".dates").datepicker({
        autoclose: true,
        todayHighlight: true
    }).on("changeDate", function () {
        vm.checkDates();
    });

    var Projects = $('#Projects').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        responsive: true,
        autoWidth: true,
        order: [[2, "asc"]],
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        buttons: [
            { extend: 'csv', text: 'Excel', title: 'Proyectos', className: 'btn-sm' },
            {
                extend: 'pdf', text: 'PDF', title: 'Proyectos', className: 'btn-sm', exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7, 8]
                },
                customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
        ],
        columns: [
            { targets: 0, data: null, defaultContent: "", class: "details-control", width: "1%", orderable: false },
            { targets: 1, data: 'ProjectId', width: "1%", visible: false },
            { targets: 2, data: 'Title', width: "1%" },
            { targets: 3, data: 'Status', width: "1%" },
            { targets: 4, data: 'Category', width: "1%" },
            { targets: 5, data: 'StartDate', width: "1%" },
            { targets: 6, data: 'CompromiseDate', width: "1%" },
            { targets: 7, data: 'EstimateCost', width: "1%" },
            { targets: 8, data: 'TotalCost', width: "1%" },
            { targets: 9, data: 'estimate_usd', width: "1%" },
            { targets: 10, data: 'total_cost_usd', width: "1%" },
            { targets: 11, data: null, width: "1%" }
        ],
        columnDefs: [{
            targets: [1, 2, 3, 4, 5, 6, 7, 8, 9,10,11],
            width: "1%"
        },
        {
            targets: [5],
            width: "1%",
            render: function (data, type, row) {
                var date = moment(data).format('DD/MM/YYYY');
                if (date != 'Invalid date')
                    return date;
                else
                    return "";
            }
        },
        {
            targets: [6],
            width: "1%",
            render: function (data, type, row) {
                var date = moment(data).format('DD/MM/YYYY');
                if (date != 'Invalid date')
                    return date;
                else
                    return "";
            }
        },
        {
            targets: [7],
            width: "1%",
            render: function (data, type, row) {
                return "$ " + data.toFixed(2);
            }
        },
        {
            targets: [8],
            width: "1%",
            render: function (data, type, row) {
                return "$ " + data.toFixed(2);
            }
        },
        {
            targets: [9],
            width: "1%",
            render: function (data, type, row) {
                return "$ " + data.toFixed(2);
            }
        },
        {
            targets: [10],
            width: "1%",
            render: function (data, type, row) {
                return "$ " + data.toFixed(2);
            }
        },
        {
            data: null,
            defaultContent: "<button class='btn btn-xs btn-outline btn-info btn-remove' type='button'>Detalle</button>",
            targets: [11],
            className: "text-center",
            orderable: false,
            width: "1%"
        }]
    });

    var detailRows = [];

    $('#Projects tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = Projects.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('details');
        }
        else {
            if (Projects.row('.details').length) {
                $('.details-control', Projects.row('.details').node()).click();
            }
            row.child(format(row.data())).show();
            tr.addClass('details');
        }
    });

    Projects.on('draw', function () {
        $.each(detailRows, function (i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });

    function reloadStyleTable() {
        $('#tabledetail').DataTable({
            oLanguage: {
                "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
            },
            order: [[1, "asc"]],
        });
    }

    function format(data) {
        var detailItems = "";
        var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
        $.ajax({
            url: "/Projects/ActionGetProjectItems",
            data: { "ProjectId": data.ProjectId, "LabelId": vm.Label() },
            type: "GET",
            success: function (result) {
                if (result.status) {
                    $.each(result.model, function (index, value) {

                        var StartDate = moment(value.StartDate).format('DD/MM/YYYY');
                        if (StartDate == 'Invalid date')
                            StartDate = "";

                        var EndDate = moment(value.EndDate).format('DD/MM/YYYY');
                        if (EndDate == 'Invalid date')
                            EndDate = "";

                        detailItems += "<tr><td>" + value.ItemDescription + "</td><td>" + value.LabelDescription + "</td><td>" + value.Status + "</td><td>" + StartDate + "</td><td>" + EndDate + "</td><td>" + "$" + value.EstimateCost.toFixed(2) + "</td>" + "<td>" + "$" + value.estimate_usd.toFixed(2) + "</td>"
                    });
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Articulo</th><th>Etiqueta</th><th>Estatus</th><th>Fecha Inicial</th><th>Fecha Final</th><th>Gasto Estimado MXN</th><th>Gasto Estimado USD</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }
                else
                    SessionFalse("Se terminó su sesión.");
            }
        });
        return tabledetail;
    }

    var model = JSON.parse($("#model").val());
    Projects.clear();
    Projects.rows.add($(model));
    $('#TableDiv').animatePanel();
    Projects.columns.adjust().draw();

    $('#Projects tbody').on('click', '.btn-info', function () {
        var ProjectId = Projects.row($(this).parents('tr')).data().ProjectId;
        vm.getItems(ProjectId);
    });

    $('.return').on('click', function () {
        vm.clearData();
    });

    var IsImage = image => image != "" ? "data:image;base64," + image : "https://www.elflorido.com.mx/images/Logotipo-01.png";

    var categories = JSON.parse($("#categories").val());

    var Item = function (itemid, status, labeldescription, itemdescription, createdby, startdate, enddate, realenddate, days) {
        var self = this;

        self.ItemId = ko.observable(itemid);
        self.Status = ko.observable(status);
        self.LabelDescription = ko.observable(labeldescription);
        self.ItemDescription = ko.observable(itemdescription);
        self.CreatedBy = ko.observable(createdby);
        self.StartDate = ko.observable(startdate);
        self.EndDate = ko.observable(enddate);
        self.RealEndDate = ko.observable(realenddate);
        self.Days = ko.observable(days);

        self.Warning = ko.computed(function () {
            if (self.Status() != "Terminado" && self.Status() != "Cancelado") {
                if (self.Days() === null)
                    return "message-info";
                else if (self.Days() <= 0)
                    return "message-danger";
                else if (self.Days() > 0 && self.Days() <= 7) {
                    return "message-warning";
                }
            }
            else
                return "message-info";
        });
    };

    var CategoryUser = function (empno, name, photo) {
        var self = this;

        self.EmpNo = ko.observable(empno);
        self.Name = ko.observable(name);
        self.Photo = ko.observable(photo);
    };

    var viewModel = function () {
        var self = this;

        self.Categories = ko.observableArray(categories);
        self.CategoryId = ko.observable();
        self.Category = ko.computed(function () {
            return typeof self.CategoryId() != 'undefined' ? self.CategoryId() : 0;
        });

        self.Labels = ko.observableArray();
        self.LabelId = ko.observable();
        self.Label = ko.computed(function () {
            return typeof self.LabelId() != 'undefined' ? self.LabelId() : "";
        });

        self.FirstDate = ko.observable();
        self.StartDate = ko.computed(function () {
            return typeof self.FirstDate() != 'undefined' ? self.FirstDate() : "";
        });

        self.SecondDate = ko.observable();
        self.EndDate = ko.computed(function () {
            return typeof self.SecondDate() != 'undefined' ? self.SecondDate() : "";
        });

        self.Title = ko.observable();
        self.Description = ko.observable();
        self.CreatedBy = ko.observable();
        self.StartDate = ko.observable();
        self.CompromiseDate = ko.observable();
        self.EndDate = ko.observable();
        self.Status = ko.observable();
        self.projectStatus = ko.computed(function () {
            switch (self.Status()) {
                case "INICIADO":
                    return "label label-info";
                    break;
                case "EN PROCESO":
                    return "label label-warning";
                    break;
                case "CANCELADO":
                    return "label label-danger";
                    break;
                case "TERMINADO":
                    return "label label-success";
                    break;
                default:
                    return "label label-info";
                    break;
            }
        });
        self.EstimateCost = ko.observable();
        self.TotalCost = ko.observable();
        self.TodayCost = ko.observable();
        self.CostByWeek = ko.observable();

        self.estimate_usd = ko.observable();
        self.total_cost_usd = ko.observable();
        self.today_cost_usd = ko.observable();
        self.cost_by_week_usd = ko.observable();
        
        self.Items = ko.observableArray();
        self.CategoryUsers = ko.observableArray();

        self.coverageLineRowSize = ko.observable(2);
        self.coverageLineRows = ko.computed(function () {
            var rows = [];
            var i = 0;
            var lines = self.CategoryUsers();
            var size = self.coverageLineRowSize();

            while (i < lines.length) {
                rows.push(lines.slice(i, i += size));
            }

            return rows;
        });

        self.reportVisible = ko.observable(true);
        self.detailVisible = ko.observable(false);

        self.isDisabled = ko.observable();
        self.budgetStatus = ko.computed(function () {
            if (self.TotalCost() > self.EstimateCost())
                return "m-xs text-danger";
            else
                return "m-xs text-success";
        });

        if (!$.isEmptyObject(model))
            self.isDisabled(false);
        else
            self.isDisabled(true);

        self.validateDates = function () {
            if ((self.StartDate() != "" & self.EndDate() == "") || (self.StartDate() == "" & self.EndDate() != ""))
                return false;
            else
                return true;
        }

        self.checkDates = function () {
            if (self.StartDate() != "" & self.EndDate() != "") {
                if (new Date(self.StartDate()) > new Date(self.EndDate())) {
                    toastr.remove();
                    toastr.warning('La fecha inicial no puede ser mayor a la fecha final');
                    return false;
                }
                else {
                    toastr.remove();
                    return true;
                }
            }
            else {
                toastr.remove();
                return true;
            }
        }

        self.categoryChanged = function () {
            if (self.Category() != 0) {
                $.ajax({
                    url: "/Projects/ActionGetLabelsByCategory",
                    type: "POST",
                    data: { "id": self.CategoryId() },
                    success: function (result) {
                        if (result != "SF")
                            self.Labels(result);
                        else
                            SessionFalse("Terminó su sesión.");
                    },
                    error: function () {
                        toastr.error('Error inesperado contactar a sistemas.');
                    }
                });
            }
            else
                self.Labels.removeAll();
        }

        self.clearData = function () {
            self.reportVisible(true);
            self.detailVisible(false);
            $(".partial-report").animatePanel();

            self.Title("");
            self.Description("");
            self.CreatedBy("");
            self.StartDate("");
            self.CompromiseDate("");
            self.EndDate("");
            self.Status("");
            self.EstimateCost("");
            self.TotalCost("");
            self.TodayCost("");
            self.CostByWeek("");
            
            self.estimate_usd("");
            self.total_cost_usd("");
            self.today_cost_usd("");
            self.cost_by_week_usd("");
            
            self.Items.removeAll();
            self.CategoryUsers.removeAll();
        }

        self.getReport = function () {
            if (self.validateDates()) {
                if (self.checkDates()) {
                    StartLoading();
                    $.ajax({
                        url: "/Projects/ActionProjectReport",
                        type: "GET",
                        data: { "CategoryId": self.Category(), "LabelId": self.Label(), "StartDate": self.StartDate(), "EndDate": self.EndDate() },
                        success: function (result) {
                            EndLoading();
                            if (result.status) {
                                Projects.clear();
                                Projects.rows.add($(result.model));
                                Projects.columns.adjust().draw();
                                $('#TableDiv').animatePanel();

                                if (!$.isEmptyObject(result.model))
                                    self.isDisabled(false);
                                else
                                    self.isDisabled(true);
                            }
                            else
                                SessionFalse("Se terminó su sesión.");
                        },
                        error: function () {
                            EndLoading();
                            toastr.remove();
                            toastr.error('Error inesperado contactar a sistemas.');
                            self.isDisabled(true);
                        }
                    });
                }
            }
            else {
                toastr.remove();
                toastr.warning('Capture correctamente el período de fechas');
            }
        }

        self.printReport = function () {
            if (self.validateDates()) {
                if (self.checkDates()) {
                    StartLoading();
                    $.ajax({
                        url: '/Projects/ActionProjectReportPrint',
                        type: "POST",
                        data: { "CategoryId": self.Category(), "LabelId": self.Label(), "StartDate": self.StartDate(), "EndDate": self.EndDate() },
                        success: function (result) {
                            EndLoading();
                            if (result.success == true) {
                                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(result.responseText) + "'></iframe>");
                                $("#modalReference").modal('show');
                            }
                            else
                                SessionFalse("Se terminó su sesión.");
                        },
                        error: function () {
                            EndLoading();
                            toastr.remove();
                            toastr.error('Error inesperado contactar a sistemas.');
                        }
                    });
                }
            }
            else {
                toastr.remove();
                toastr.warning('Capture correctamente el período de fechas');
            }
        }

        self.getItems = function (projectId) {
            $.ajax({
                url: "/Projects/ActionGetProjectDetails",
                type: "GET",
                data: { "ProjectId": projectId },
                success: function (result) {
                    if (result.success) {
                        self.Title(result.data.Title);
                        self.Description(result.data.Description);
                        self.CreatedBy(result.data.CreatedBy);
                        self.StartDate(moment(result.data.StartDate).format('DD/MM/YYYY'));
                        self.CompromiseDate(moment(result.data.CompromiseDate).format('DD/MM/YYYY'));
                        self.EndDate(moment(result.data.EndDate).format('DD/MM/YYYY') === "Invalid date" ? "" : moment(result.data.EndDate).format('DD/MM/YYYY'));
                        self.Status(result.data.Status);
                        self.EstimateCost("$" + result.data.EstimateCost.toLocaleString());
                        self.TotalCost("$" + result.data.TotalCost.toLocaleString());
                        self.TodayCost("$" + result.data.TodayCost.toLocaleString());
                        self.CostByWeek("$" + result.data.CostByWeek.toLocaleString());

                        self.estimate_usd("$" + result.data.estimate_usd.toLocaleString());
                        self.total_cost_usd("$" + result.data.total_cost_usd.toLocaleString());
                        self.today_cost_usd("$" + result.data.today_cost_usd.toLocaleString());
                        self.cost_by_week_usd("$" + result.data.cost_by_week_usd.toLocaleString());
                        
                        result.data.Items.forEach(function (item) {
                            self.Items.push(new Item(item.ItemId, item.Status, item.LabelDescription, item.ItemDescription, item.CreatedBy, moment(item.StartDate).format('DD/MM/YYYY'), moment(item.EndDate).format('DD/MM/YYYY'), moment(item.RealEndDate).format('DD/MM/YYYY') === "Invalid date" ? "" : moment(item.RealEndDate).format('DD/MM/YYYY'), item.Days));
                        });

                        result.data.CategoryUsers.forEach(function (user) {
                            self.CategoryUsers.push(new CategoryUser(user.EmployeeNumber, user.Name, IsImage(user.ImageBase64)));
                        });

                        self.reportVisible(false);
                        self.detailVisible(true);
                        $(".partial-detail").animatePanel();
                    }
                    else
                        SessionFalse("Terminó su sesión.");
                },
                error: function () {
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
        }
    };

    var vm = new viewModel();
    ko.applyBindings(vm);
});