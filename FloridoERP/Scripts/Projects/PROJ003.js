﻿var projectAvailable = true;
var projectUsersCategory = [];
var projectUsersLabel = [];
var projectUSersLabelLeader = [];
var projectLeader = "";
var projectId = 0;
var projectIdLabel = 0;
var projectItemDesc = "";
var projectTitle = "";
var commentFiles = [];
var projectItem = 0;
var varnameLabel = "";
var varprojectIdLabel = 0;
var projectIdCategory = 0;

$(document).ready(function () {
    $("#projectDrop").select2();
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits: 4
});

var IsImage = image => image != "" ? "data:image;base64," + image : "https://www.elflorido.com.mx/images/Logotipo-01.png";
var IsComment = comment => comment != null ? comment : "No hay comentarios en este artículo, se el primero en agregar una historia al artículo.";
var IsPost = post => post != null ? post : "0";
var IsDate = date => date != null ? moment(date).lang('es').format('MMM DD YYYY') : "S/Fecha";
var IsDateFull = date => date != null ? moment(date).lang('es').format('LLLL') : "S/Fecha";
var IsLeaderUser = user => projectLeader == user ? `<i class="fa fa-bell text-warning" data-toggle="tooltip" data-placement="bottom" title="Lider del proyecto"></i>` : ``;
var isPDF = (file, id_history) => file != null ? `<i class="fa fa-file-pdf-o fa-3x pull-left" onclick="OpenModelPdf(${id_history});" style="cursor: pointer;"></i>` : ``;
var isUser = user => user != null ? user : "/";
var getPDF = function (id_history_detail) {
    var attached = attachedByModal.find(x => x.id_history_detail == id_history_detail);
    return attached.document_pdf;
}

var isImage = file => file != null ? `<a href="${file}" title="" data-gallery=""><img src="${file}" onclick="OpenModalImage(this);" width="100" height="70"></a> ` : ``;
var isLabelItem = function (status) {
    if (status == 1)
        return `<span class="label label-info pull-left">INICIADO</span>`;
    else if (status == 2)
        return `<span class="label label-warning pull-left">EN PROCESO</span>`;
    else if (status == 8)
        return `<span class="label label-danger pull-left">CANCELADO</span>`;
    else if (status == 9)
        return `<span class="label label-success pull-left">TERMINADO</span>`;
    else
        return `<span class="label label-danger pull-left">ERROR</span>`;
}

var IsCategoryUser = function (user) {
    var strReturn = "";
    var find = projectUsersCategory.filter(x => x.user_name == user);
    if (find.length > 0) {
        $.each(find, function (index, value) {
            strReturn += `<i class="fa fa-shield text-success" data-toggle="tooltip" data-placement="bottom" title="Miembro de la Categoría '${value.id_label_project_default}'"></i> `;
        });
        return strReturn;
    }
    else
        return ``;
}

var IsLabelUser = function (user) {
    var strReturn = "";
    var find = projectUsersLabel.filter(x => x.user_name == user);
    if (find.length > 0) {
        $.each(find, function (index, value) {
            strReturn += `<i class="fa fa-tag text-dark" data-toggle="tooltip" data-placement="bottom" title="Miembro de la etiqueta '${value.id_label_project_default}'"></i> `;
        });
        return strReturn;
    }
    else
        return ``;
}

var IsLabelLeader = function (user) {
    var strReturn = "";
    var find = projectUSersLabelLeader.filter(x => x.user_name == user);
    if (find.length > 0) {
        $.each(find, function (index, value) {
            strReturn += `<i class="fa fa-bookmark" style="color:#5974b6;" data-toggle="tooltip" data-placement="bottom" title="Lider de la etiqueta '${value.id_label_project_default}'"></i> `;
        });
        return strReturn;
    }
    else
        return ``;
}

var table = $('#TableAttached').DataTable({
    "autoWidth": true,
    "paging": true,
    "ordering": false,
    "info": false,
    "searching": false,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    lengthMenu: [[3], [3]],
    columns: [
        { targets: 0, data: 'file_name' },
        { targets: 1, data: 'file_type' },
        { targets: 2, data: 'file_base64' },
        { targets: 3, data: null }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3],
        width: "1%"
    },
    {
        data: null,
        width: "1%",
        defaultContent: '<button class="btn btn-xs btn-danger" id="remove-button" ><i class="fa fa-times"></i></button> ',
        targets: [3]
    }]
});

//Ocultar el Ids
table.column(2).visible(false);
table.clear().draw();

//Eliminar archivo de la lista
$('#TableAttached tbody').on('click', '#remove-button', function () {
    var index = table.row($(this).parents('tr')).index();
    commentFiles.splice(index, 1);
    table.clear().draw();
    table.rows.add($(commentFiles));
    table.columns.adjust().draw();
});

function ShowMain() {
    $('#itemMain').removeClass("hide");
    $('#itemMain').animatePanel();
    $('#projectHeader').removeClass("hide");
    $('#projectHeader').animatePanel();
    $('#itemMainPagination').removeClass("hide");
    $('#itemMainPagination').animatePanel();
    $('#projectItemDescription').addClass('hide');
    $('#projectItemDescription').animatePanel();
    $('#itemComments').addClass('hide');
    $('#itemComments').animatePanel();
    $('#itemCommentsPagination').addClass('hide');
    $('#itemCommentsPagination').animatePanel();
    $('#itemAddComment').addClass("hide");
    $('#itemAddComment').animatePanel();
    $('#projectLabelDescription').addClass('hide');
    $('#projectLabelDescription ').animatePanel();

    $('#itemCommentsNull').addClass("hide");
    $('#itemCommentsNull').animatePanel();

    projectIdLabel = "";
    projectItemDesc = "";
    projectItem = 0;
    varnameLabel = "";
    projectIdCategory = 0;
    ClearComment();
    projectLeader = "";
    GetProyectBasicInformation(projectId);
}

function ChangeProjectDrop() {
    var projectDrop = $('#projectDrop').val();
    if (projectDrop != "") {
        GetProyectBasicInformation(projectDrop);
    }
    else {
        $('#projectMain').animatePanel();
        $('#projectMain').addClass("hide");

        $('#itemMain').animatePanel();
        $('#itemMain').addClass("hide");

        $('#itemMainPagination').animatePanel();
        $('#itemMainPagination').addClass("hide");

        $('#proyectLabelNull').animatePanel();
        $('#proyectLabelNull').addClass("hide");
    }
}

function GetProyectBasicInformation(projectDrop) {
    $.ajax({
        url: '/Projects/ActionGetItemHistoryBasic/',
        type: "GET",
        data: { "id_project": projectDrop },
        success: function (returndata) {
            if (returndata == "SF") {
                SessionFalse("Su sesión a terminado.")
            }
            if (returndata.info.length > 0) {
                projectUsersLabel = returndata.userLabel;
                projectUSersLabelLeader = returndata.userLabelLeader;
                projectUsersCategory = returndata.userCategory;

                projectIdLabel = returndata.info[0]['id_label'];
                projectIdCategory = returndata.info[0]['id_category'];
                projectLeader = returndata.info[0]['label_emp_no_user_name'];
                projectId = returndata.info[0]['id_proyect'];
                projectTitle = returndata.info[0]['title'];
                $('#proyectTitle').html('<i class="pe-7s-light"></i> ' + returndata.info[0]['title']);
                $('#projectDesc').html('<i class="pe-7s-ribbon"></i> ' + returndata.info[0]['description']);
                $('#proyectStartDate').html(IsDate(returndata.info[0]['start_date']));
                $('#proyectEndDate').html(IsDate(returndata.info[0]['end_date']));
                $('#proyectCompromiseDate').html(IsDate(returndata.info[0]['compromise_date']));
                $('#proyectCategory').html(returndata.info[0]['category']);
                $('#proyectEstimateCost').html(formatter.format(returndata.info[0]['estimate']) + ' MXN');
                $('#proyectEstimateCostUsd').html(formatter.format(returndata.info[0]['estimate_usd']) + ' USD');
                $('#proyectTotalCost').html('<i class="fa fa-money"></i> ' + formatter.format(returndata.info[0]['total_cost']) + ' MXN');
                $('#proyectTotalCostUsd').html('<i class="fa fa-money"></i> ' + formatter.format(returndata.info[0]['total_cost_usd']) + ' USD');
                var status = projectAvailable = returndata.info[0]['project_status'];

                if (status == 0) {
                    $('#projectMainLabel').removeClass("label-warning");
                    $('#projectMainLabel').removeClass("label-danger");
                    $('#projectMainLabel').removeClass("label-success");
                    $('#projectMainLabel').html('Iniciado');
                    $('#projectMainLabel').addClass("label-info");
                    $('#proyectStatus').html('Iniciado');
                } else if (status == 8) {
                    $('#projectMainLabel').removeClass("label-warning");
                    $('#projectMainLabel').removeClass("label-info");
                    $('#projectMainLabel').removeClass("label-success");
                    $('#projectMainLabel').html('Cancelado');
                    $('#projectMainLabel').addClass("label-danger");
                    $('#proyectStatus').html('Cancelado');
                } else if (status == 1) {
                    $('#projectMainLabel').removeClass("label-danger");
                    $('#projectMainLabel').removeClass("label-info");
                    $('#projectMainLabel').removeClass("label-success");
                    $('#projectMainLabel').html('En proceso');
                    $('#projectMainLabel').addClass("label-warning");
                    $('#proyectStatus').html('En proceso');
                }
                else if (status == 9) {
                    $('#projectMainLabel').removeClass("label-info");
                    $('#projectMainLabel').removeClass("label-warning");
                    $('#projectMainLabel').removeClass("label-danger");
                    $('#projectMainLabel').removeClass("label-success");
                    $('#projectMainLabel').html('Terminado');
                    $('#projectMainLabel').addClass("label-success");
                    $('#proyectStatus').html('Terminado');
                } else {
                    $('#projectMainLabel').removeClass("label-warning");
                    $('#projectMainLabel').removeClass("label-info");
                    $('#projectMainLabel').removeClass("label-success");
                    $('#projectMainLabel').html('ERROR');
                    $('#projectMainLabel').addClass("label-danger");
                    $('#proyectStatus').html('ERROR');
                }

                $('#pagination-container-label').pagination({
                    dataSource: returndata.info,
                    pageSize: 10,
                    className: 'paginationjs-theme-blue',
                    callback: function (returndata, pagination) {
                        var html = ForumTemplate(returndata);
                        $('#data-label').html(html);
                    }
                });
                $('#projectMain').removeClass("hide");
                $('#projectMain').animatePanel();
                $('#itemMain').removeClass("hide");
                $('#itemMain').animatePanel();
                $('#itemMainPagination').removeClass("hide");
                $('#itemMainPagination').animatePanel();
                $('#proyectLabelNull').addClass("hide");
                $('#proyectLabelNull').animatePanel();

            } else {
                $('#projectMain').addClass("hide");
                $('#projectMain').animatePanel();
                $('#itemMain').addClass("hide");
                $('#itemMain').animatePanel();
                $('#itemMainPagination').addClass("hide");
                $('#itemMainPagination').animatePanel();
                $('#proyectLabelNull').addClass("hide");
                $('#proyectLabelNull').animatePanel();

                $('#proyectLabelNull').removeClass("hide");
                $('#proyectLabelNull').animatePanel();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function ForumTemplate(data) {
    var totalItemTempleateHtml = [];
    var items = [];
    var template = "";
    $.each(data, function (index, value) {
        template = "";
        items = value.table_item_history;
        if (items.length > 0) {
            if (items[0] != null) {
                //Initialize template
                template = `<div class="hpanel forum-box">
                                                                <div class ="panel-heading">
                                                                   <i class="fa fa-bookmark"></i> ${value.label_name}
                                                                </div>`;
                $.each(items, function (index, value) {
                    template += `<div class ="panel-body">
                                                                     <div class ="row">
                                                                        <div class ="media col-md-1">
                                                                            <div class ="media-image pull-left">
                                                                                <img src="${IsImage(value.ImageBase64)}" class="img-circle m-b" width="100" height="100">
                                                                            </div>
                                                                        </div>
                                                                        <div class ="col-md-9 forum-heading">
                                                                            ${isLabelItem(value.item_status)}
                                                                            <a onclick="GetAllComments(${value.id_item})"><h4>${value.item_description}</h4></a>
                                                                            <div class ="desc">
                                                                                <span class="text-muted font-bold" data-toggle="tooltip" data-placement="bottom" title="${isUser(value.user_full_name)}"><i class="fa fa-user"></i> ${isUser(value.user_name)}</span> - <span class="text-info" data-toggle="tooltip" data-placement="bottom" title="${IsDateFull(value.post_date)}"><i class="fa fa-calendar"></i> ${IsDate(value.post_date)}</span> - <i class="fa fa-commenting"></i> ${IsComment(value.user_comment)}
                                                                            </div>
                                                                        </div>
                                                                        <div class ="col-md-1 forum-info">
                                                                        </div>
                                                                        <div class ="col-md-2 forum-info">
                                                                            <span class ="number"> ${IsPost(value.post_numbers)} </span>
                                                                            <small><i class="fa fa-comments"></i> Comentarios</small>
                                                                        </div>
                                                                        </div>
                                                                   </div>`;
                });
                template += `</div>`;
            } else {
                template = `<div class="hpanel forum-box">
                                                                <div class ="panel-heading">
                                                                   <i class="fa fa-bookmark"></i> ${value.label_name}
                                                                </div>
                                                                <div class ="panel-body">
                                                                    <div class ="row">
                                                                        <div class ="media col-md-1">
                                                                            <div class ="media-image pull-left">
                                                                                <i id="Imagecontainer" class ="pe-7s-display2 fa-3x"></i>
                                                                            </div>
                                                                        </div>
                                                                            <div class ="col-md-9 forum-heading">
                                                                                ${isLabelItem(value.item_status)}
                                                                                <a onclick="GetAllComments(${value.id_item})"><h4>${value.item_description}</h4></a>
                                                                                <div class ="desc"><i class="fa fa-commenting"></i> No existen comentarios en este artículo, se el primero en comentar.</div>
                                                                            </div>
                                                                            <div class ="col-md-1 forum-info">
                                                                            </div>
                                                                            <div class ="col-md-2 forum-info">
                                                                                <span class ="number"> 0 </span>
                                                                                <small><i class="fa fa-comments"></i> Comentarios</small>
                                                                            </div>
                                                                        </div>
                                                                     </div>
                                                                </div>`;
            }
        } else {
            template = `<div class="hpanel forum-box">
                                                            <div class ="panel-heading">
                                                               <i class="fa fa-bookmark"></i> ${value.label_name}
                                                            </div>
                                                            <div class ="panel-body">
                                                                <div class ="row">
                                                                    <div class ="media col-md-1">
                                                                        <div class ="media-image pull-left">
                                                                            <i id="Imagecontainer" class ="pe-7s-display2 fa-3x"></i>
                                                                        </div>
                                                                    </div>
                                                                        <div class ="col-md-9 forum-heading">
                                                                            <h4>Sin Artículo</h4>
                                                                            <div class ="desc"> No existen artículos en esta etiqueta, pide a tu lider de etiqueta que agrege haciendo clic <a href="PROJ002">aquí.</a></div>
                                                                        </div>
                                                                        <div class ="col-md-1 forum-info">
                                                                        </div>
                                                                        <div class ="col-md-2 forum-info">
                                                                            <span class ="number"> ${IsPost(value.post_numbers)} </span>
                                                                            <small><i class="fa fa-comments"></i> Comentarios</small>
                                                                        </div>
                                                                   </div>
                                                                 </div>
                                                             </div>`;
        }
        totalItemTempleateHtml.push(template);
    });
    return totalItemTempleateHtml;
}

function GetAllComments(id_item) {
    $.ajax({
        url: '/Projects/ActionGetAllCommentsByItem/',
        type: "GET",
        data: {
            "id_item": id_item
        },
        success: function (returndata) {
            if (returndata == "SF") {
                SessionFalse("Su sesión a terminado.")
            }
            if (returndata.item.length > 0) {

                var leaderLabelUserName = returndata.labelInfo['description'];
                var leaderLabelFullName = returndata.labelInfo['label_emp_no_user_name'];
                varnameLabel = returndata.labelInfo['label_name'];
                $('#labelDesc').html(returndata.labelInfo['label_name']);
                $('#labelUserLeader').html(`<span class="text-muted font-bold" data-toggle="tooltip" data-placement="bottom" title="${leaderLabelFullName}"> ${isUser(leaderLabelUserName)}</span>`);
                $('#labelEstimateCost').html(formatter.format(returndata.labelInfo['estimate']));
                $('#labelTotalCost').html(formatter.format(returndata.labelInfo['total_cost']));

                projectItemDesc = returndata.item[0]['item_description']
                varprojectIdLabel = returndata.item[0]['id_label']
                projectItem = id_item;
                $('#itemDesc').html('<i class="pe-7s-ribbon"></i> ' + returndata.item[0]['item_description']);
                $('#itemStartDate').html(IsDate(returndata.item[0]['start_date']));
                $('#itemEndDate').html(IsDate(returndata.item[0]['end_date']));
                $('#itemRealStartDate').html(IsDate(returndata.item[0]['real_start_date']));
                $('#itemRealEndDate').html(IsDate(returndata.item[0]['real_end_date']));
                $('#itemEstimateCost').html(formatter.format(returndata.item[0]['estimate']));
                $('#itemTotalCost').html(formatter.format(returndata.item[0]['total_cost']));
                var status = returndata.item[0]['item_status'];
                if (status == 1) {
                    $('#projectItemLabel').removeClass("label-warning");
                    $('#projectItemLabel').removeClass("label-danger");
                    $('#projectItemLabel').removeClass("label-success");
                    $('#projectItemLabel').html('Iniciado');
                    $('#projectItemLabel').addClass("label-info");
                    $('#itemStatus').html('Iniciado');
                }
                else if (status == 2) {
                    $('#projectItemLabel').removeClass("label-danger");
                    $('#projectItemLabel').removeClass("label-info");
                    $('#projectItemLabel').removeClass("label-success");
                    $('#projectItemLabel').html('En proceso');
                    $('#projectItemLabel').addClass("label-warning");
                    $('#itemStatus').html('En proceso');
                }
                else if (status == 8) {
                    $('#projectItemLabel').removeClass("label-warning");
                    $('#projectItemLabel').removeClass("label-info");
                    $('#projectItemLabel').removeClass("label-success");
                    $('#projectItemLabel').html('Cancelado');
                    $('#projectItemLabel').addClass("label-danger");
                    $('#itemStatus').html('Cancelado');
                }
                else if (status == 9) {
                    $('#projectItemLabel').removeClass("label-info");
                    $('#projectItemLabel').removeClass("label-warning");
                    $('#projectItemLabel').removeClass("label-danger");
                    $('#projectItemLabel').removeClass("label-success");
                    $('#projectItemLabel').html('Terminado');
                    $('#projectItemLabel').addClass("label-success");
                    $('#itemStatus').html('Terminado');
                }
                else {
                    $('#projectItemLabel').removeClass("label-warning");
                    $('#projectItemLabel').removeClass("label-info");
                    $('#projectItemLabel').removeClass("label-success");
                    $('#projectItemLabel').html('ERROR');
                    $('#projectItemLabel').addClass("label-danger");
                    $('#itemStatus').html('ERROR');
                }

                $('#pagination-container-label-detail').pagination({
                    dataSource: returndata.item,
                    pageSize: 10,
                    className: 'paginationjs-theme-blue',
                    callback: function (returndata, pagination) {
                        var html = ForumTemplateDetail(returndata);
                        if (html.length <= 0) {
                            $('#itemComments').addClass('hide');
                            $('#itemComments').animatePanel();
                            $('#itemCommentsPagination').addClass('hide');
                            $('#itemCommentsPagination').animatePanel();

                            $('#itemCommentsNull').removeClass("hide");
                            $('#itemCommentsNull').animatePanel();
                        }
                        else {
                            $('#itemComments').removeClass('hide');
                            $('#itemComments').animatePanel();
                            $('#itemCommentsPagination').removeClass('hide');
                            $('#itemCommentsPagination').animatePanel();
                            $('#itemCommentsNull').addClass("hide");
                            $('#itemCommentsNull').animatePanel();
                        }

                        $('#data-label-detail').html(html);
                    }
                });

                $('#proyectLabelNull').addClass("hide");
                $('#proyectLabelNull').animatePanel();

                $('#projectHeader').animatePanel();
                $('#itemMain').addClass("hide");
                $('#itemMain').animatePanel();
                $('#itemMainPagination').addClass("hide");
                $('#itemMainPagination').animatePanel();
                $('#projectItemDescription').removeClass('hide');
                $('#projectItemDescription').animatePanel();
                $('#projectHeader').addClass("hide");
                $('#itemAddComment').removeClass('hide');
                $('#itemAddComment').animatePanel();
                $('#projectLabelDescription').removeClass('hide');
                $('#projectLabelDescription ').animatePanel();

            } else {
                // si no atreaeee nadaaaa
                ////
                /////

                $('#itemCommentsNull').removeClass("hide");
                $('#itemCommentsNull').animatePanel();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function ForumTemplateDetail(data) {
    var totalItemTempleateHtml = [];
    var dataItems = [];
    var template = "";
    $('#itemProyectTitle').html(projectTitle);
    $('#itemProyectTitle2').html(projectTitle);
    $.each(data, function (index, value) {
        template = "";
        dataItems = [];
        dataItems = value.table_attached;
        if (value.user_comment != null) {
            template += `        <div class="panel-body">
                                                                <div class="media">
                                                                    <div class="media-image pull-left">
                                                                        <img src="${IsImage(value.ImageBase64)}" alt="profile-picture">
                                                                        <div class="author-info">
                                                                            <strong data-toggle="tooltip" data-placement="bottom" title="${value.user_full_name}"><i class="fa fa-user"></i> ${value.user_name}</strong><br>
                                                                            <div class="text-info" data-toggle="tooltip" data-placement="bottom" title="${IsDateFull(value.post_date)}"><i class="fa fa-calendar"></i> ${IsDate(value.post_date)}</div>
                                                                            <div class="badges">
                                                                                ${IsLeaderUser(value.user_name)}
                                                                                ${IsCategoryUser(value.user_name)}
                                                                                ${IsLabelLeader(value.user_name)}
                                                                                ${IsLabelUser(value.user_name)}
                                                                            </div>
                                                                         </div>
                                                                     </div>
                                                                     <div class="media-body">
                                                                        ${value.user_comment}
                                                                     </div>`;
            if (dataItems.length > 0) {
                template += `<div class="pull-left" style='margin-top:30px;'><code>Archivos Adjuntos:</code><br /><br />  `;
                $.each(dataItems, function (index, valueAttached) {
                    template += `${isPDF(valueAttached.document_pdf, valueAttached.id_history_detail)}
                                                             ${isImage(valueAttached.document_image)}`;
                });
                template += `</div>`;
            }
            template += `             </div>
                                                              </div>`;

            if (index == data.length - 1) {
                $('#dateLastUpdateOne').html(IsDateFull(data[0].post_date));
                $('#dateLastUpdateTwo').html(IsDateFull(data[0].post_date));
            }
            totalItemTempleateHtml.push(template);
        }

    });

    return totalItemTempleateHtml;
}

function UpdateSourceImage() {
    if (commentFiles.length < 10) {
        //Read File
        var selectedFile = document.getElementById("file_source_image").files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            var idxDot = selectedFile[0].name.lastIndexOf(".") + 1;
            var extFile = selectedFile[0].name.substr(idxDot, selectedFile[0].name.length).toLowerCase();
            //check type file
            if (extFile == "jpg" || extFile == "jpeg" || extFile == "png" || extFile == "gif") {
                //Check file
                if (selectedFile[0].size < 10085760) {
                    //check same file
                    if (!commentFiles.find(f => f.file_name === selectedFile[0].name)) {
                        // Select the very first file from list
                        var fileToLoad = selectedFile[0];
                        // FileReader function for read the file.
                        var fileReader = new FileReader();
                        var base64;
                        // Onload of file read the file content
                        fileReader.onload = function (fileLoadedEvent) {
                            base64 = fileLoadedEvent.target.result;
                            var fileTable =
                            {
                                file_name: selectedFile[0].name,
                                file_type: "Imagen",
                                file_base64: base64
                            };
                            commentFiles.push(fileTable);
                            document.getElementById("file_source_image").value = "";
                            table.clear().draw();
                            table.rows.add($(commentFiles));
                            table.columns.adjust().draw();
                        };
                        fileReader.readAsDataURL(fileToLoad);
                    } else {
                        toastr.error("No se puede subir el mismo archivo dos veces.");
                    }
                } else {
                    toastr.error("El peso del archivo debe ser menor a 10MB. Intente reducir el peso del archivo con un software.");
                }
            } else {
                toastr.error("Seleccione un archivo de fotografia correcto: jpg, jpeg, png, gif.");
            }
        } else {
            toastr.error("Seleccione un archivo de imagen correctamente.");
        }
    } else {
        toastr.error("Solo se permiten 10 archivos por comentario.");
    }
}

function UpdateSourcePdf() {
    if (commentFiles.length < 10) {
        //Read File
        var selectedFile = document.getElementById("file_source_pdf").files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            var idxDot = selectedFile[0].name.lastIndexOf(".") + 1;
            var extFile = selectedFile[0].name.substr(idxDot, selectedFile[0].name.length).toLowerCase();
            //check type file
            if (extFile == "pdf") {
                //Check size
                if (selectedFile[0].size < 5242880/*10085760*/) {
                    if (!commentFiles.find(f => f.file_name === selectedFile[0].name)) {
                        // Select the very first file from list
                        var fileToLoad = selectedFile[0];
                        // FileReader function for read the file.
                        var fileReader = new FileReader();
                        var base64;
                        // Onload of file read the file content
                        fileReader.onload = function (fileLoadedEvent) {
                            base64 = fileLoadedEvent.target.result;
                            var fileTable =
                            {
                                file_name: selectedFile[0].name,
                                file_type: "PDF",
                                file_base64: base64
                            };
                            commentFiles.push(fileTable);
                            document.getElementById("file_source_pdf").value = "";
                            table.clear().draw();
                            table.rows.add($(commentFiles));
                            table.columns.adjust().draw();
                        };
                        fileReader.readAsDataURL(fileToLoad);
                    } else {
                        toastr.error("No se puede subir el mismo archivo dos veces.");
                    }
                } else {
                    toastr.error("El peso del archivo debe ser menor a 5MB.");
                }
            } else {
                toastr.error("Seleccione solo archivos PDF.");
            }
        } else {
            toastr.error("Ingrese un archivo pdf correctamente.");
        }
    } else {
        toastr.error("Solo se permiten 10 archivos por comentario.");
    }
}

$('#commentSend').on('click', function () {
    var comment = $("#commentMain").val();
    if (comment.trim().length <= 8) {
        toastr.error("Ingresa un comentario con más de ocho caracteres.");
        $("#commentMain").focus();
    }
    else {
        swal({
            title: "¿Esta seguro que desea enviar el mensaje?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                $.ajax({
                    type: "POST",
                    url: "/Projects/ActionInsertCommentsWithAttached/",
                    data: { "id_item": projectItem, "comment": comment.trim(), "attached": commentFiles },
                    success: function (response) {
                        EndLoading();
                        if (response == "SF") {
                            setTimeout(function () { SessionFalse("Su sesión a terminado."); }, 600);
                        }
                        if (response.status == true) {
                            ClearComment();
                            toastr.success("Mensaje enviado.");
                            GetAllComments(projectItem);
                            SendNofitication();
                        }
                    },
                    error: function () {
                        EndLoading();
                        toastr.error("ERROR DESCONOCIDO, CONTACTA A SISTEMAS.");
                    }
                });
            }
        });
    }
});

function ClearComment() {
    $("#commentMain").val("");
    commentFiles = [];
    table.clear().draw();
    table.rows.add($(commentFiles));
    table.columns.adjust().draw();
}

function OpenModelPdf(id_history_detail) {
    $.ajax({
        type: "POST",
        url: "/Projects/ActionGetAttachedPDFByHistory/",
        data: { "id_hitosry_detail": id_history_detail },
        success: function (response) {
            if (response == "SF") {
                SessionFalse("Su sesión a terminado.")
            } else {
                $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='${response}'></iframe>`);
                $("#modalReference").modal('show');
            }
        },
        error: function () {
            toastr.error("ERROR DESCONOCIDO, CONTACTA A SISTEMAS.");
        }
    });
}

function OpenModalImage(element) {
    var modal = document.getElementById("myModalImage");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = element.src;

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close-image")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
}

function SendNofitication() {
    $.ajax({
        type: "POST",
        url: "/Projects/ActionNotification/",
        data: { "ProjectTitle": projectTitle, "IdProject": projectId, "nameLabel": varnameLabel, "item": projectItemDesc, "history": true },
        success: function (response) {
            if (response == "SF") {
                SessionFalse("Su sesión a terminado.")
            }
            if (response.status == true) {
                //Se enviaron las notificaciones a los usuarios
                //toastr.success("Mensaje enviado a los usuarios.");
            }
        },
        error: function () {
            toastr.error("ERROR AL NOTIFICAR LOS USUARIOS, CONTACTA A SISTEMAS.");
        }
    });
}