﻿var Projects = JSON.parse($("#model").val());

var FormatDate = date => moment(date).format("DD/MM/YYYY");
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
const isDecimal = decimal => number => {
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
var button = option => {
    if (option.Status == 1) {
        return `<button class="btn btn-warning" style="margin-top:12px;" onclick="EditModalItem('${option.IdItem}')">
            <i data-toggle="tooltip" data-placement="top" data-original-title="editar articulos" class="fa fa-pencil"></i>
        </button>

 <button class="btn btn-primary" style="margin-top:12px;" onclick="StartDateItem('${option.IdItem}')">
            <i data-toggle="tooltip" data-placement="top" data-original-title="procesar articulos" class="fa fa-forward"></i>
        </button>
    <!--<button class="btn btn-success" style="margin-top:12px;" onclick="TerminateItem('${option.IdItem}')">
            <i data-toggle="tooltip" data-placement="top" data-original-title="terminar articulos" class="fa fa-check"></i>
        </button>-->
    <button class="btn btn-danger" style="margin-top:12px;" onclick="DeleteItem('${option.IdItem}')">
            <i data-toggle="tooltip" data-placement="top" data-original-title="borrar articulos" class="fa fa-close"></i>
        </button>
    `
    }
    if (option.Status == 2) {
        return `<button disabled class="btn btn-warning" style="margin-top:12px;">
            <i class="fa fa-pencil"></i>
        </button>
    <button class="btn btn-success" style="margin-top:12px;" onclick="TerminateItem('${option.IdItem}')">
            <i data-toggle="tooltip" data-placement="top" data-original-title="terminar articulos" class="fa fa-check"></i>
        </button>
    `
    }
    else {
        return `<button disabled class="btn btn-warning" style="margin-top:12px;">
            <i class="fa fa-pencil"></i>
        </button>`
    }
}
var IsImage = image => image != "Ti9B" ? "data:image;base64," + image : "https://www.elflorido.com.mx/images/Logotipo-01.png";
var HtmlItem = item => ` <div class ="col-lg-3">
    <div class="card ${Colors(item.Status)} mb-3" style="max-width: 100%;width:100%;height:280px;">
      <div class="card-header"><i class='fa fa-paint-brush'></i> ${item.Description}</div>
      <div class="card-body text-primary">
        <h5 class="card-title"><i class='fa fa-flag-checkered'></i> Estatus: ${StatusText(item.Status)}</h5>
        <p class="card-text"><i class='fa fa-dollar'></i> Presupuesto pesos: ${fomartmoney(item.Estimate)(4)}</p>
        <p class="card-text"><i class='fa fa-dollar'></i> Presupuesto dolar: ${fomartmoney(item.estimate_usd)(4)}</p>
        <!--<p class="card-text"><i class='fa fa-money'></i> Gasto total: ${fomartmoney(item.TotalCost)(4)}</p>-->
        <p class="card-text"><i class='glyphicon glyphicon-calendar'></i> Fecha de inicio: ${FormatDate(item.StartDate)}</p>
        <p class="card-text"><i class='glyphicon glyphicon-calendar'></i> Fecha de terminacion: ${FormatDate(item.EndDate)}</p>
        <!--<p class="card-text"><i class='fa fa-hashtag'></i> Posicion: ${item.ConsecutiveItem}</p>-->
        ${button(item)}
      </div>
          </div>
    </div>`;
var ButtonEmail = label => label.ItemsCount > 0 ? ` <button class="btn btn-info" onclick="EmailLabel('${label.IdLabelProject}','${label.IdProject}')" >
                                <i data-toggle="tooltip" data-placement="top" data-original-title="Manda una alerta al correo de los usuarios de etiqueta" class="fa fa-bell"></i>
                            </button>`: ``;
var HtmlLabel = label => `
<div class ="col-lg-4">
    <div class="card" style="width: 100%;height:300px;max-height:100%;margin-bottom:20px;">
    <div class="row " style="padding: 0.5rem; margin:0">
        <img src="${IsImage(label.ImageBase64)}" class="col-xs-6"  style="display: inline; max-height:5.9rem; max-width:100px; width: auto; height: auto" class="card-img-top" alt="...">
        
        <p class=" col-xs-6" style="display:inline; font-size: 1.3rem; font-weight: bold;max-height:5.9rem; overflow-y: hidden"> <span style="font-size: 0.9rem;display: block" ><i class='fa fa-user'></i> Encargado</span>  ${label.FullName}</p>
    </div>
  
  <div class="card-body" style="padding-top: 0">
    <h4>${label.IdLabelProjectDefault}</h4>
    
    <p class="card-text"><i class='fa fa-cubes'></i> Articulos:${label.ItemsCount}</p>
    <p class="card-text"><i class='fa fa-dollar'></i> Presupuesto pesos:${fomartmoney(label.Estimate)(4)}</p>
    <p class="card-text"><i class='fa fa-money'></i> Gastos pesos:${fomartmoney(label.TotalCost)(4)}</p>

    <p class="card-text"><i class='fa fa-dollar'></i> Presupuesto dolar:${fomartmoney(label.estimate_usd)(4)}</p>
        <p class="card-text"><i class='fa fa-money'></i> Gastos dolar:${fomartmoney(label.total_cost_usd)(4)}</p>
        <button class="btn btn-warning"  onclick="EditLabelModal('${label.IdLabelProject}')">
                                <i data-toggle="tooltip" data-placement="top" data-original-title="Editar Etiquetas y agregar presupuesto" class="fa fa-dollar"></i>
                            </button>
                            <button class="btn btn-success" onclick="OpenFormItem('${label.IdLabelProject}')" >
                                <i data-toggle="tooltip" data-placement="top" data-original-title="Agregar Etiquetas o ver" class="fa fa-plus"></i>
                            </button>
         
${ButtonEmail(label)}
                        </div>
    </div>
                    </div>`;

var ListCard = [];
var ListItems = [];
var TotalItems = 0;
var TotalItemsUsd = 0;
var EstimateProject = 0;
var EstimateProjectUsd = 0;
var LabelFormLabel = null;
var ItemForm = null;

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $("#selectProject").select2({
        //minimumInputLength: 2,
        //formatInputTooShort: function () {
        //    return "Busca un proyecto";
        //},
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#selectLabel").select2({
        //minimumInputLength: 2,
        //formatInputTooShort: function () {
        //    return "Busca un proyecto";
        //},
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#btnEstimate").hide();
    $('#inputStartDate').datepicker({
        autoclose: true,
        startDate: new Date(),
        todayHighlight: true,
    });

    $('#inputEndDate').datepicker({
        autoclose: true,
        startDate: new Date(),
        todayHighlight: true,
    });
});

function SearchProject() {
    var projectId = $("#selectProject").val()
    ListCard = [];
    EstimateProject = 0;
    EstimateProjectUsd = 0;
    $("#spanEstimate").html(EstimateProject)
    $("#spanEstimateUsd").html(EstimateProjectUsd);
    if (Projects.filter(x => x.id_project == projectId && x.cuser == x.currentuser).length > 0)
        $("#btnEstimate").show();
    else
        $("#btnEstimate").hide();
    if (projectId != "0") {
        GetProjectLabel(projectId);
        ReloadProyect(true)
    }
    else {
        selectGeneric("#selectLabel", ListCard, ["IdLabelProjectDefault", "IdLabelProjectDefault"], { id: "0", text: "Selecciona una opcion" });
        $("#formLabel").hide()
        $("#cardItem").hide()
        $("#formProyect").show()
        $("#cardLabel").hide()
    }
}

function CardItemBoostrap(data) {
    return data.map(x => HtmlItem(x));
}

function CardLabelBoostrap(data) {
    return data.map(x => HtmlLabel(x));
}

function Colors(option) {
    var color = ""
    switch (option) {
        case 1:
            color = "border-primary"
            break;
        case 2:
            color = "border-warning"
            break;
        case 9:
            color = "border-success"
            break;
        case 8:
            color = "border-danger"
            break;
        default:
            color = "border-dark"
            break;
    }
    return color
}

function StatusText(option) {
    var status = ""
    switch (option) {
        case 1:
            status = "Iniciado"
            break;
        case 2:
            status = "En proceso"
            break;
        case 9:
            status = "Terminado"
            break;
        case 8:
            status = "Cancelado"
            break;
        default:
            status = "N/A"
            break;
    }
    return status
}

function selectGeneric(id, list, array, firstOption) {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    const selectList = arrayOption => elements => selectOption(elements[arrayOption[0]])(elements[arrayOption[1]]);
    $(id).html([`${selectOption(firstOption.id)(firstOption.text)}`, ...list.map(elements => selectList(array)(elements))])
}

function GetProjectLabel(idproject) {
    axios.get(`/Projects/ActionGetProjectLabel/?Label=${idproject}&statusLabel=${true}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse("Se terminó su sesion.");
            }
            else if (data.data.length > 0) {
                ListCard = data.data;
                if (ListCard.length > 0) {
                    EstimateProject = Projects.find(x => x.id_project == idproject).estimate - ListCard[0].EstimateCard;
                    EstimateProjectUsd = Projects.find(x => x.id_project == idproject).estimate_usd - ListCard[0].EstimateCardUsd;
                }
                else {
                    EstimateProject = Projects.find(x => x.id_project == idproject).estimate
                    EstimateProjectUsd = Projects.find(x => x.id_project == idproject).estimate_usd
                }
                $("#spanEstimate").html(parseFloat(EstimateProject.toFixed(4)))
                $("#spanEstimateUsd").html(parseFloat(EstimateProjectUsd.toFixed(4)))
                selectGeneric("#selectLabel", ListCard.filter(x => x.Estimate == 0 && x.estimate_usd === 0), ["IdLabelProjectDefault", "IdLabelProjectDefault"], { id: "0", text: "Selecciona una opcion" });
                GeneralOptionCard(ListCard, '#pagination-container-label', '#data-label', false)
            }
            else {
                GeneralOptionCard(ListCard, '#pagination-container-label', '#data-label', false)
                toastr.warning("No contiene ninguna etiqueta")
                $("#selectProject").val($("#selectProject :first").val()).trigger('change');
            }
        })
        .catch(error => {
            console.log(`%cEl error es ${error}`, "color:red;font-size:24px")
        })
}

function comparar(a, b) { return a.ConsecutiveItem - b.ConsecutiveItem; }

function OrderCard(list, option) {
    return option ? list.sort(comparar) : list
}

function OptionCardGenerate(data, option) {
    return option ? data.map(x => HtmlItem(x)) : data.map(x => HtmlLabel(x));
}

function CantidadTiquet(option) {
    return option ? 8 : 6;
}

function GeneralOptionCard(list, idHeader, idBody, option) {
    $(idHeader).pagination({
        dataSource: OrderCard(list, option),
        pageSize: CantidadTiquet(option),
        showGoInput: true,
        showGoButton: true,
        className: 'paginationjs-theme-blue',
        callback: function (data, pagination) {
            var html = OptionCardGenerate(data, option);
            $(idBody).html(html);
        }
    });
}

function GetProjectItem(ProjectItem) {
    if (ProjectItem > 0) {
        ListItems = [];
        TotalItems = 0;
        TotalItemsUsd = 0;
        LabelFormLabel = ListCard.find(x => x.IdLabelProject == ProjectItem)
        $("#PNombreUser").html(LabelFormLabel.FullName)
        $("#PNumeroUser").html(LabelFormLabel.EmpNo)
        $("#PEtiqueta").html(LabelFormLabel.IdLabelProjectDefault)
        axios.get(`/Projects/ActionGetProjectItem/?IdLabel=${ProjectItem}`)
            .then(data => {
                if (data.data.length > 0) {
                    $('#ImageNoItems').addClass("hide");
                    ListItems = data.data
                    TotalItems = ListItems.filter(x => x.Status !== 8).reduce((a, b) => a + b.Estimate, 0);
                    TotalItems = LabelFormLabel.Estimate - (isNaN(parseFloat(TotalItems)) ? 0 : parseFloat(TotalItems));

                    TotalItemsUsd = ListItems.filter(x => x.Status !== 8).reduce((a, b) => a + b.estimate_usd, 0);
                    TotalItemsUsd = LabelFormLabel.estimate_usd - (isNaN(parseFloat(TotalItemsUsd)) ? 0 : parseFloat(TotalItemsUsd));

                    $("#estimateItem").html(parseFloat(TotalItems.toFixed(4)));
                    $("#estimateItemUsd").html(parseFloat(TotalItems.toFixed(4)));
                    GeneralOptionCard(ListItems, "#pagination-container", '#data-container', true)
                }
                else {
                    $('#ImageNoItems').removeClass("hide");
                    TotalItems = LabelFormLabel.Estimate;
                    TotalItemsUsd = LabelFormLabel.estimate_usd;
                    GeneralOptionCard([], "#pagination-container", '#data-container', true)
                }
            })
            .catch(error => {
                console.log(`%cEl error es ${error}`, "color:red;font-size:24px")
            })
    }
    else
        toastr.error("Hubo un problema")
}

function OpenFormItem(a) {
    GetProjectItem(a)
    ReloadProyect(false)
}

function ReloadProyect(value) {
    if (value) {
        $("#formLabel").hide()
        $("#cardItem").hide()
        $("#formProyect").show()
        $("#cardLabel").show()
    }
    else {
        $("#formLabel").show()
        $("#cardItem").show()
        $("#formProyect").hide()
        $("#cardLabel").hide()
    }
}

function EditLabelModal(IdLabel) {
    OptionModal(false)
    $("#myModal5").modal('show');
    LabelFormLabel = ListCard.find(x => x.IdLabelProject == IdLabel)
    $("#inputEstimate").val(LabelFormLabel.Estimate)
    $("#inputEstimateUsd").val(LabelFormLabel.estimate_usd);
    $("#spanEstimate").html(parseFloat(EstimateProject.toFixed(4)))
    $("#spanEstimateUsd").html(parseFloat(EstimateProjectUsd.toFixed(4)));
    selectGeneric("#selectLabel", ListCard, ["IdLabelProjectDefault", "IdLabelProjectDefault"], { id: "0", text: "Selecciona una opcion" });
    $("#selectLabel").attr('disabled', true);
    $('#selectLabel').val(`${LabelFormLabel.IdLabelProjectDefault}`).trigger('change');
}

function openModalLabel() {
    OptionModal(true)
    $("#myModal5").modal('show');
    selectGeneric("#selectLabel", ListCard.filter(x => x.Estimate == 0), ["IdLabelProjectDefault", "IdLabelProjectDefault"], { id: "0", text: "Selecciona una opcion" });
    $("#selectLabel").attr('disabled', false);
    LabelFormLabel = null;
    ClearLabel();
    $("#spanEstimate").html(parseFloat(EstimateProject.toFixed(4)))
    $("#spanEstimateUsd").html(parseFloat(EstimateProjectUsd.toFixed(4)))
}

function OptionModal(value) {
    if (value) {
        $("#saveLabel").show();
        $("#editLabel").hide();
    }
    else {
        $("#saveLabel").hide();
        $("#editLabel").show();
    }
}

function ClearLabel() {
    $("#inputEstimate").val("");
    $('#selectLabel').val($('#selectLabel :first').val()).trigger('change');
}

function EditAndCreateLabel(id, EstimatePast, Estimate, EstimatePastUsd, EstimateUsd) {
    var LabelPost = ListCard.find(x => x.IdLabelProject == id);
    var LabelValuePast = LabelPost.Estimate;
    var LabelValuePastUsd = LabelPost.estimate_usd;
    LabelPost.Estimate = Estimate;
    LabelPost.estimate_usd = EstimateUsd;
    axios.post("/Projects/ActionUpdateProjectLabel/", { Label: LabelPost, ProjectHeader: Projects.find(x => x.id_project == LabelPost.IdProject) })
        .then(data => {
            if (data.data == 1) {
                EstimateProject = (EstimateProject + EstimatePast) - Estimate;
                EstimateProjectUsd = (EstimateProjectUsd + EstimatePastUsd) - EstimateUsd;
                let e = ListCard.find(x => x.IdLabelProject == LabelFormLabel.IdLabelProject);
                e.Estimate = Estimate;
                e.estimate_usd = EstimateUsd;
                OptionModal(true);
                $("#myModal5").modal('hide');
                GeneralOptionCard(ListCard, '#pagination-container-label', '#data-label', false)
            }
            else if (data.data == 11) {
                EstimateProject = (EstimateProject + EstimatePast) - Estimate;
                EstimateProjectUsd = (EstimateProjectUsd + EstimatePastUsd) - EstimateUsd;
                let e = ListCard.find(x => x.IdLabelProject == LabelFormLabel.IdLabelProject);
                e.Estimate = Estimate;
                e.estimate_usd = EstimateUsd;
                OptionModal(true);
                $("#myModal5").modal('hide');
                Projects.find(x => x.id_project == LabelPost.IdProject).start_date = moment().format("MM/DD/YYYY")
                GeneralOptionCard(ListCard, '#pagination-container-label', '#data-label', false)
            }
            else if (data.data == 2) {
                $("#myModal5").modal('hide');
                LabelPost.Estimate = LabelValuePast;
                LabelPost.estimate_usd = LabelValuePastUsd;
                toastr.error("Hubo un problema al guardar el presupuesto")
            }
            else if (data.data == 3) {
                $("#myModal5").modal('hide');
                LabelPost.Estimate = LabelValuePast;
                LabelPost.estimate_usd = LabelValuePastUsd;
                toastr.warning("el articulo no puede cambiarse el presupuesto")
            }
            else if (data.data == 108) {
                toastr.warning("Hubo un problema al activar el proyecto favor de contactar a sistemas")
            }
            else {
                $("#myModal5").modal('hide');
                LabelPost.Estimate = LabelValuePast;
                LabelPost.estimate_usd = LabelValuePastUsd;
                toastr.error("Error desconosido")
            }
        })
        .catch(error => {
            console.log(`%cEl error es ${error}`, "color:red;font-size:24px")
        });
}

function SaveLabel() {
    EstimateTimeReal()
    EstimateTimeRealUSD();
    let Estimate = isNaN(parseFloat($("#inputEstimate").val())) ? 0 : parseFloat($("#inputEstimate").val());
    let EstimateFinal = EstimateProject - Estimate;

    let EstimateUsd = isNaN(parseFloat($("#inputEstimateUsd").val())) ? 0 : parseFloat($("#inputEstimateUsd").val());
    let EstimateFinalUsd = EstimateProjectUsd - EstimateUsd;

    if (Estimate < 0 || EstimateUsd < 0) {
        toastr.warning("agrega una cantidad superior a 0")
    }
    else if (EstimateFinal >= 0 && EstimateFinalUsd >= 0) {
        var IdLabel = $("#selectLabel").val()
        if (IdLabel != "0") {
            EditAndCreateLabel(LabelFormLabel.IdLabelProject, 0, Estimate, 0, EstimateUsd)
        }
        else
            toastr.warning("Favor de seleecionar una opcion")
    }
    else {
        toastr.warning("El presupuesto colocado supera al actual")
    }
}

function EditLabel() {
    EstimateTimeReal()
    EstimateTimeRealUSD();
    let Estimate = parseFloat($("#inputEstimate").val()) != NaN ? parseFloat($("#inputEstimate").val()) : 0;
    let EstimatePast = ListCard.find(x => x.IdLabelProject == LabelFormLabel.IdLabelProject).Estimate;
    let EstimateFinal = (EstimateProject + EstimatePast) - Estimate;
    let EstimateUsd = parseFloat($("#inputEstimateUsd").val()) != NaN ? parseFloat($("#inputEstimateUsd").val()) : 0;
    let EstimatePastUsd = ListCard.find(x => x.IdLabelProject == LabelFormLabel.IdLabelProject).estimate_usd;
    let EstimateFinalUsd = (EstimateProjectUsd + EstimatePastUsd) - EstimateUsd;

    if (Estimate < 0 || EstimateUsd < 0) {
        toastr.warning("agrega una cantidad no negativa puede ser 0")
    }
    else if (EstimateFinal >= 0 && EstimateFinalUsd >= 0) {
        EditAndCreateLabel(LabelFormLabel.IdLabelProject, EstimatePast, Estimate, EstimatePastUsd, EstimateUsd)
    }
    else {
        toastr.warning("El presupuesto colocado supera al actual")
    }
}

function EstimateTimeReal() {
    let input = $("#inputEstimate");
    let num = parseFloat(input.val());
    num = isNaN(num) || num < 0 || num > 9999999999 ? 0 : num;
    input.val(num);

    let EstimatePast = 0;
    if (LabelFormLabel != null) {
        EstimatePast = ListCard.find(x => x.IdLabelProject == LabelFormLabel.IdLabelProject).Estimate;
    }
    const EstimateFinal = (EstimateProject + EstimatePast) - (num > 99999999 ? 0 : num);
    $("#spanEstimate").html(parseFloat(EstimateFinal.toFixed(4)))
}

function EstimateTimeRealUSD() {
    let input = $("#inputEstimateUsd");
    let num = parseFloat(input.val());
    num = isNaN(num) || num < 0 || num > 9999999999 ? 0 : num;
    input.val(num);
    let EstimatePast = 0;
    if (LabelFormLabel != null) {
        EstimatePast = ListCard.find(x => x.IdLabelProject == LabelFormLabel.IdLabelProject).estimate_usd;
    }
    const EstimateFinal = (EstimateProjectUsd + EstimatePast) - (num > 99999999 ? 0 : num);
    $("#spanEstimateUsd").html(parseFloat(EstimateFinal.toFixed(4)))
}

function RealTimeItem() {
    var inputItemEstimate = isNaN(parseFloat($("#inputItemEstimate").val())) ? 0 : parseFloat($("#inputItemEstimate").val());
    if (inputItemEstimate < 0 || isNaN(parseFloat($("#inputItemEstimate").val())) || inputItemEstimate > 99999999 || !isDecimal(4)(inputItemEstimate))
        $("#inputItemEstimate").val(0)
    else
        $("#inputItemEstimate").val($("#inputItemEstimate").val())
    var EstimateItem = 0
    if (ItemForm != null) {
        var Items = ListItems.find(x => x.IdItem == ItemForm.IdItem);
        EstimateItem = Items.Estimate;
    }
    $("#estimateItem").html(parseFloat(((TotalItems + EstimateItem) - (inputItemEstimate > 99999999 ? 0 : inputItemEstimate)).toFixed(4)));
}

function RealTimeItemUSD() {
    var inputItemEstimate = isNaN(parseFloat($("#inputItemEstimateUsd").val())) ? 0 : parseFloat($("#inputItemEstimateUsd").val());
    if (inputItemEstimate < 0 || isNaN(parseFloat($("#inputItemEstimateUsd").val())) || inputItemEstimate > 99999999 || !isDecimal(4)(inputItemEstimate))
        $("#inputItemEstimateUsd").val(0)
    else
        $("#inputItemEstimateUsd").val($("#inputItemEstimateUsd").val())
    var EstimateItem = 0
    if (ItemForm != null) {
        var Items = ListItems.find(x => x.IdItem == ItemForm.IdItem);
        EstimateItem = Items.estimate_usd;
    }
    $("#estimateItemUsd").html(parseFloat(((TotalItemsUsd + EstimateItem) - (inputItemEstimate > 99999999 ? 0 : inputItemEstimate)).toFixed(4)));
}

function ValidateError(input, error) {
    setTimeout(function () {
        position = $(input).offset()
        $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
        $(error).show();
        $(input).addClass("error");
        if ($(input).val().trim() != null && $(input).val().trim() != "") {
            $(error).html("Poner una fecha valida")
        }
        else
            $(error).html("El Campo no puede ser nulo")
        $(error).fadeOut(5000, function () {
            $(input).removeClass("error")
        })
    }, 100);
    $(".datepicker").hide();
}

function ValidationProjecItem(inputItemDescription, inputItemEstimate, inputItemEstimateUsd, inputStartDate, inputEndDate, inputPositionItem, Estimate, EstimateUsd) {
    var ItemValue = false;
    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    if (inputItemEstimate >= 0 && inputItemEstimate < 99999999 && !isNaN(parseFloat(inputItemEstimate)) && ((TotalItems + Estimate) - parseFloat(inputItemEstimate)) >= 0 &&
        inputItemEstimateUsd >= 0 && inputItemEstimateUsd < 99999999 && !isNaN(parseFloat(inputItemEstimateUsd)) && ((TotalItemsUsd + EstimateUsd) - parseFloat(inputItemEstimateUsd)) >= 0) {
        if (inputStartDate.trim() != "" && inputStartDate.match(RegExPattern)) {
            if (inputEndDate.trim() != "" && inputEndDate.match(RegExPattern)) {
                if (moment(inputStartDate.trim()) <= moment(inputEndDate.trim())) {
                    ItemValue = true;
                }
                else {
                    toastr.warning("La segunda fecha es inferior a la primera");
                }
            } else {
                if (!inputEndDate.match(RegExPattern))
                    toastr.warning("Pon una fecha valida")
                else
                    toastr.error('necesita tener una fecha');
                ValidateError("#inputEndDate", "#errorEndDate")
            }
        }
        else {
            if (!inputStartDate.match(RegExPattern))
                toastr.warning("Pon una fecha valida")
            else
                toastr.error('necesita tener una fecha');
            ValidateError("#inputStartDate", "#errorStratDate")
        }
    }
    else {
        toastr.warning("Favor de un estimado valido");
    }
    return ItemValue;
}

function SaveProjectItem() {
    RealTimeItem();
    RealTimeItemUSD();
    var inputItemDescription = $("#inputItemDescription").val();
    var inputItemEstimate = $("#inputItemEstimate").val();
    var inputItemEstimateUsd = $("#inputItemEstimateUsd").val();
    var inputStartDate = $("#inputStartDate").val();
    var inputEndDate = $("#inputEndDate").val();
    var inputPositionItem = 1/*$("#inputPositionItem").val();*/

    if (inputItemDescription.trim() == "") {
        toastr.warning("Favor de no poner espacios vacios")
    }
    else if (inputItemDescription.trim() != "" && ListItems.filter(x => x.Description.trim().toLowerCase() == inputItemDescription.trim().toLowerCase()).length == 0) {
        if (ValidationProjecItem(inputItemDescription, inputItemEstimate, inputItemEstimateUsd, inputStartDate, inputEndDate, inputPositionItem, 0, 0)) {
            if (true/*¿En serio Daev? [[Aplaudiendo lentamente]] */ /*ListItems.filter(x => x.ConsecutiveItem == inputPositionItem).length == 0 && inputPositionItem > 0 && inputPositionItem < 999999*/) {
                const ProjectItemModel = { RealEndDate: null, RealStartDate: null, Description: inputItemDescription.trim(), Estimate: parseFloat(inputItemEstimate), StartDate: inputStartDate, EndDate: inputEndDate, ConsecutiveItem: inputPositionItem, TotalCost: 0, Status: 1, IdLabelProject: LabelFormLabel.IdLabelProject, estimate_usd: parseFloat(inputItemEstimateUsd) }
                axios.post("/Projects/ActionPostProjectItem", ProjectItemModel).then(data => {
                    if (data.data == "SF") {
                        SessionFalse("Se terminó su sesion.");
                    }
                    else if (data.data.IdItem != null && data.data.IdItem > 0) {
                        $('#ImageNoItems').addClass("hide");
                        TotalItems = (TotalItems) - inputItemEstimate;
                        TotalItemsUsd = (TotalItemsUsd) - inputItemEstimateUsd;
                        ListItems = [...ListItems, data.data]
                        GeneralOptionCard(ListItems, "#pagination-container", '#data-container', true)
                        ClearItems();
                        EnableStatus();
                    }
                    else {
                        toast.warning("Problema al guardar")
                    }
                }).catch(error => {
                    console.log(`%cEl error es ${error}`, "color:red;font-size:24px"); toastr.error("Contacta con sistemas")
                }).then(() => { $("#myModal6").modal("hide") })
            }
            else {
                toastr.warning("Favor de colocar un orden valido");
            }
        }
    }
    else {
        toastr.warning("Coloca otro nombre ese no esta disponible");
    }
}

function ClearItems() {
    $("#inputItemDescription").val("");
    $("#inputItemEstimate").val("");
    $("#inputItemEstimateUsd").val("");
    $("#inputStartDate").val("");
    $("#inputEndDate").val("");
    //$("#inputPositionItem").val("");
    $("#inputItemDescription").attr('disabled', false);
}

function OpenModalItems() {
    $("#myModal6").modal('show');
    $("#btnSaveItem").show();
    $("#btnEditItem").hide();
    $("#estimateItem").html(parseFloat(TotalItems.toFixed(4)));
    $("#estimateItemUsd").html(parseFloat(TotalItemsUsd.toFixed(4)));
    ItemForm = null;
    ClearItems();
}

function EditModalItem(id) {
    $("#myModal6").modal('show');
    var ItemsModal = ListItems.find(x => x.IdItem == id)
    ItemForm = ItemsModal
    $("#inputItemDescription").val(ItemsModal.Description).attr('disabled', true);;
    $("#inputItemEstimate").val(ItemsModal.Estimate);
    $("#inputItemEstimateUsd").val(ItemsModal.estimate_usd);
    $("#inputStartDate").val(moment(ItemsModal.StartDate).format("MM/DD/YYYY"));
    $("#inputEndDate").val(moment(ItemsModal.EndDate).format("MM/DD/YYYY"));
    //$("#inputPositionItem").val(ItemsModal.ConsecutiveItem);
    $("#btnSaveItem").hide();
    $("#btnEditItem").show();
    $("#estimateItem").html(parseFloat(TotalItems.toFixed(4)));
    $("#estimateItemUsd").html(parseFloat(TotalItemsUsd.toFixed(4)));
}

function EditProjectItem() {
    RealTimeItem();
    var inputItemDescription = $("#inputItemDescription").val();
    var inputItemEstimate = $("#inputItemEstimate").val();
    var inputItemEstimateUsd = $("#inputItemEstimateUsd").val();
    var inputStartDate = $("#inputStartDate").val();
    var inputEndDate = $("#inputEndDate").val();
    var inputPositionItem = 1//$("#inputPositionItem").val();
    if (ValidationProjecItem(inputItemDescription, inputItemEstimate, inputItemEstimateUsd, inputStartDate, inputEndDate, inputPositionItem, ItemForm.Estimate, ItemForm.estimate_usd)) {
        if (true/*((ListItems.filter(x => x.ConsecutiveItem == inputPositionItem).length == 1 && ItemForm.ConsecutiveItem == inputPositionItem) || (ListItems.filter(x => x.ConsecutiveItem == inputPositionItem).length == 0)) && inputPositionItem > 0 && inputPositionItem < 999999*/) {
            const ProjectItemModel = { RealEndDate: null, RealStartDate: null, IdItem: ItemForm.IdItem, Description: inputItemDescription.trim(), Estimate: parseFloat(inputItemEstimate), StartDate: inputStartDate, EndDate: inputEndDate, ConsecutiveItem: parseInt(inputPositionItem), TotalCost: 0, Status: 1, IdLabelProject: LabelFormLabel.IdLabelProject, estimate_usd: parseFloat(inputItemEstimateUsd) }
            axios.post("/Projects/ActionUpdateProjectItem", ProjectItemModel).then(data => {
                if (data.data == "SF") {
                    SessionFalse("Se terminó su sesion.");
                }
                else if (data.data == 1) {
                    TotalItems = (TotalItems + ItemForm.Estimate) - inputItemEstimate;
                    TotalItemsUsd = (TotalItemsUsd + ItemForm.estimate_usd) - inputItemEstimateUsd;
                    ItemsFormModel = ListItems.find(x => x.IdItem == ItemForm.IdItem);
                    ItemsFormModel.EndDate = ProjectItemModel.EndDate;
                    ItemsFormModel.StartDate = ProjectItemModel.StartDate;
                    ItemsFormModel.ConsecutiveItem = ProjectItemModel.ConsecutiveItem;
                    ItemsFormModel.Estimate = ProjectItemModel.Estimate;
                    ItemsFormModel.estimate_usd = ProjectItemModel.estimate_usd;
                    GeneralOptionCard(ListItems, "#pagination-container", '#data-container', true)
                    ClearItems();
                }
                else if (data.data == 2) {
                    toastr.warning("Ese valor se encuentra en un estado diferente de Iniciado favor de recargar la pagina")
                }
                else {
                    toast.warning("Problema al editar")
                }
            }).catch(error => {
                console.log(`%cEl error es ${error}`, "color:red;font-size:24px"); toastr.error("Contacta con sistemas")
            }).then(() => { $("#myModal6").modal("hide") })
        }
        else {
            toastr.warning("Favor de colocar un orden valido");
        }
    }
}

function ButtonValidationGeneric(status, id) {
    ProjectItemModel = ListItems.find(x => x.IdItem == id)
    ProjectItemModel.Status = status
    ProjectItemModel.StartDate = moment(ProjectItemModel.StartDate).format("MM/DD/YYYY")
    ProjectItemModel.EndDate = moment(ProjectItemModel.EndDate).format("MM/DD/YYYY")
    if (status == 9) {
        ProjectItemModel.RealStartDate = moment().format("MM/DD/YYYY")
        ProjectItemModel.RealEndDate = moment().format("MM/DD/YYYY")
    }
    axios.post("/Projects/ActionUpdateProjectItem", ProjectItemModel).then(data => {
        if (data.data == "SF") {
            SessionFalse("Se terminó su sesion.");
        }
        else if (data.data == 1) {
            if (status == 8) {
                TotalItems += parseFloat(ProjectItemModel.Estimate)
            }
            ListItems = ListItems.filter(x => x.Status != 8)
            if (ListItems.length == 0)
                $('#ImageNoItems').removeClass("hide");
            GeneralOptionCard(ListItems, "#pagination-container", '#data-container', true)
            ClearItems();
        }
        else if (data.data == 2) {
            toastr.warning("Ese valor se encuentra en un estado diferente de Iniciado favor de recargar la pagina")
        }
        else {
            toast.warning("Problema al cambiar el estado del articulo")
        }
    }).catch(error => {
        console.log(`%cEl error es ${error}`, "color:red;font-size:24px"); toastr.error("Contacta con sistemas")
    }).then(() => { $("#myModal6").modal("hide") })
}

function DeleteItem(id) {
    swal({
        title: "¿Deseas borrar el articulo ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm)
            ButtonValidationGeneric(8, id);
    });
}

function TerminateItem(id) {
    swal({
        title: "¿Deseas terminar el proceso del articulo?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm)
            ButtonValidationGeneric(9, id);
    });
}

function StartDateItem(id) {
    swal({
        title: "¿Deseas procesar el articulo?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm)
            ButtonValidationGeneric(2, id);
    });
}

function LabelRealadCard() {
    $('#ImageNoItems').addClass("hide");
    EnableStatus();
    GeneralOptionCard(ListCard, '#pagination-container-label', '#data-label', false)
}

function EnableStatus() {
    LabelFormLabel.ItemsCount = ListItems.length;
    var projectF = Projects.find(x => x.id_project == LabelFormLabel.IdProject);
    if (ListCard.filter(x => x.ItemsCount > 0).length > 0 && projectF.status == 0) {
        projectF.status = 1;
        axios.post("/projects/ActionUpdateStatusProjectHeader", projectF)
            .then(data => {
                if (data.data == "SF") {
                    SessionFalse("Se terminó su sesion.");
                }
                if (data.data == 1) {
                    Projects.find(x => x.id_project == LabelFormLabel.IdProject).start_date = moment().format("MM/DD/YYYY")
                }
                else {
                    toast.warning("No se activo el label")
                }
            })
            .catch(error => { console.log(`%cEl error es ${error}`, "color:red;font-size:24px"); toastr.error("Contacta con sistemas") });
    }
}

function EmailLabel(idProjecLabel, idProject) {
    if (idProjecLabel != "" && idProject != "") {
        axios.post(`/Projects/ActionEmail/`, { IdProject: idProject, IdLabel: idProjecLabel, item: "", history: false, allUsers: false }).then(data => {
            if (data.data == "SF") {
                SessionFalse("Se terminó su sesion.");
            }
            if (data.data) {
                toastr.success("Fue mandado el correo")
            }
        }).catch(error => {
            console.log(`%cEl error es ${error}`, "color:red;font-size:24px");
            toastr.error("Contacta con sistemas")
        });
    }
    else {
        toastr.warning("hubo un problema al mandar correo")
    }
}