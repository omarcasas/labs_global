let projects = [];
const select_project = $("#select_project");
let selected_project_data;
const modal = $('#modal');
 

const modal_label_id=$('#mo_id');
const modal_emp=$('#mo_emp');
const modal_status=$('#mo_status');
const modal_len=$('#mo_len');
const modal_estimate=$('#mo_estimate');
const modal_estimate_usd=$('#mo_estimate_usd');
const modal_fac_count=$('#mo_fac_count');
const modal_fac_val=$('#mo_fac_val');
const modal_fac_val_usd=$('#mo_fac_val_usd');

const modal_xml_label=$('#mo_xml_label');
const modal_xml_label2=$('#mo_xml_label2');
const modal_btn_save = $('#mo_btn_save');
let select_label_data;
let xmls =[];

function update(t,data) {
    t.clear();
    t.rows.add($(data));
    t.columns.adjust().draw();
}
function updateTable(project_id) {
    //StartLoading();
    let url = `/Projects/ActionReadyToPayLabels/?project_id=${project_id}`;
    axios.get(url)
        .then(res => {
            console.log(res);
            if (!res.data.Ok){
                if(res.data.Type === "sf"){ SessionFalse(res.data.Msg);}
                else {toastr.error("Error al cargar etiquetas");}
                return;
            } 
            var data = res.data.Data;
            update(table,data);
            const tr = document.getElementById("pr_th_btn");
            tr.classList.remove("sorting_asc");
            tr.classList.add("sorting_disabled");
            EndLoading();
        }).catch(e => toastr.error("Error al cargar etiquetas.") );
}
function open_modal(data){
    xmls = [];
    modal_xml_label.html("Seleccione un archivo XML");
    modal_xml_label2.html("Ha seleccionado 0 artículos para ligar a factura");
    console.log(data);
    modal_label_id.html("<i class='fa fa-paint-brush'></i> Codigo etiqueta: " + data.IdLabelProjectDefault);
    modal_emp.html("<i class='fa fa-user'></i> Encargado: " + data.EmpName);
    modal_len.html("<i class='fa fa-sort-numeric-asc'></i> Numero articulos: " + data.Items.length);
    modal_estimate.html("<i class='fa fa-line-chart'></i>  Estimado Pesos: $" + data.Estimate);
    modal_estimate_usd.html("<i class='fa fa-line-chart'></i>  Estimado Dolar: $" + data.estimate_usd);
    modal_fac_count.html("<i class='fa fa-sticky-note'></i> Facturas registradas: "+ data.ActualInvoiceCount);
    modal_fac_val.html("<i class='fa fa-usd'></i> Total Facturas pesos: $"+ data.ActualTotalCost);
    modal_fac_val_usd.html("<i class='fa fa-usd'></i> Total Facturas dolar: $"+ data.ActualTotalCostUsd);
    select_label_data=data;
    update(mo_table,data.Items);
    let ck_count=0;
    $('.is_ck').on('change',function () {
        updateTotal();
    });
    modal.modal('show');
}
function dispFile(contents) {
    document.getElementById('contents').innerHTML = contents
}
function clickElem(elem) {
    var eventMouse = document.createEvent("MouseEvents");
    eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
    elem.dispatchEvent(eventMouse)
}
function openFile(func) {
    let readFile = function (e) {
        var file = e.target.files;
        xmls = file;
        if(xmls.length>0){
            modal_xml_label.html(`Ha seleccionado ${xmls.length} archivo(s) para subir <br/> <br/>`); 
        }else{
            modal_xml_label.html(`Seleccione un archivo XML `);
        }
        const conv = (file,i)=> {
            let fileReader = new FileReader();
            return new Promise((resolve,reject)=>{
                fileReader.onerror = () => { reader.abort(); reject(new Error("Error parsing file"));}
                fileReader.onload = function (fileLoadedEvent) {
                    resolve(fileLoadedEvent.target.result);
                };
                fileReader.readAsText(file);
            });


        };
        let proms = [];
        for(let i = 0; i<xmls.length; i++){
            proms.push(conv(xmls[i],i));
        }
        Promise.all(proms).
        then((xmlList)=> {
            const parser = new DOMParser();
            const total = xmlList.reduce((sum,xml)=>{
                const doc = parser.parseFromString(xml,"text/xml");
                const header = doc.getElementsByTagName("cfdi:Comprobante")[0].attributes;
                const t = header["Total"].value;
                const m = header["Moneda"].value;
                const type = header["TipoDeComprobante"].value;
                if (m === 'USD') {
                    if (type === "E") {
                        return { p: sum.p, d: sum.d - parseFloat(t) };
                    } else {
                        return { p: sum.p, d: sum.d + parseFloat(t) };
                    }
                } else {
                    if (type === "E") {
                        return { p: sum.p - parseFloat(t), d: sum.d };
                    } else {
                        return { p: sum.p + parseFloat(t), d: sum.d };
                    }
                    
                }
            },{p:0,d:0});
            modal_xml_label.html( modal_xml_label.html( ) + `
                Total XML:<br/>
                Pesos: ${total.p.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')}<br/>
                Dolar: ${total.d.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')}<br/>`);
        });
        
        
    };
    fileInput = document.createElement("input");
    fileInput.type = 'file';
    fileInput.style.display = 'none';
    fileInput.accept = '.xml';
    fileInput.onchange = readFile;
    fileInput.multiple = true;
    fileInput.func = func;
    document.body.appendChild(fileInput);
    clickElem(fileInput);
}
function updateTotal(){
    
    let total=0;
    let total_usd=0;
    let e=false;
    let ck_count=0;
    
    mo_table.rows().every(function (index, element) {
        const row = $(this.node());
        const check = row.find('.is_ck').eq(0)[0];
        if(check.checked) {
            ck_count+=1;
            let  txt = row.find('.item_total_class').eq(0)[0];
            let txt2 = row.find('.item_total_usd_class').eq(0)[0];
            let t1=  parseFloat(txt.value || 0);
            let t2= parseFloat(txt2.value || 0);
            if( (!t1 || isNaN(t1)) && (!t2 || isNaN(t2))) {
                err = true;
                return;
            }
            total+=t1;
            total_usd+=t2;
        }
    });
    if(!e){
        modal_xml_label2.html ( 
            `Ha seleccionado ${ck_count} artículos para ligar a factura<br/><br/>
                Total capturado: <br/>
                Pesos: ${total.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')} <br/>
                Dolares ${total_usd.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')} <br/>
            `)
    }
}
select_project.select2({
    placeholder: "Proyectos",
    minimumInputLength: 0,
    ajax:{
        url: "/Projects/ActionActiveProjects",
        dataType: 'json',
        results: function(data) {
            console.log(data);
            if(!data.Ok) {
                if(data.Type === "sf"){ SessionFalse(data.Msg);}
                else {toastr.error("Error al cargar proyectos");}
                return ;
            }
            projects = data.Data;
            return {results: projects.map((e,i) => ({ text:e.title, id:i }))}
        },
    },
    cache:true,

});
select_project.on("change", e => { 
    selected_project_data=projects[e.val].id_project;  
    updateTable(selected_project_data) ;
});

const table = $('#table').DataTable({
    destroy:true,
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:[],
    columns: [
        {
            class: "",
            data: null,
            defaultContent: "<button class='btn btn-xs btn-outline btn-warning'><i class='fa fa-pencil'></i> Artículos</button>",
            ordering: false,
            orderable: false

        },
        { data: 'IdLabelProjectDefault' },
        { data: 'EmpName' },
        { data: 'Items.length' },
        { data: x => "$"+x.Estimate },
        { data: x => "$"+x.estimate_usd },
        { data: 'ActualInvoiceCount' },
        { data: x => "$"+x.ActualTotalCost },
        { data: x => "$"+x.ActualTotalCostUsd },
    ]
});

const mo_table = $('#mo_table').DataTable({
    destroy:true,
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:[],
    columns: [
        { data: 'IdItem' },
        { data: 'Description' },
        { data: x => x.Status === 1 ? "Iniciado" : x.Status === 2 ? "En Proceso" : x.Status === 8 ? "Cancelado" : x.Status === 9 ? "Terminado" : "", },
        
        { data: 'Estimate' },
        { data: 'estimate_usd' },
        { data: 'TotalCost' },
        { data: 'total_cost_usd' },
        { data: 'ActualInvoiceCount' },

        
        { data: (m) => {
                return `
                            <input class="item_total_class" type="number" value=${m.ntotal_item } onkeyup=updateTotal()>
                            
                        
               `;
            } },
        { data: (m) => {
                return `
                            <input class="item_total_usd_class" type="number" value=${m.ntotal_item_usd } onkeyup=updateTotal()>
               `;
            } },
        {
            data: (m) => {
                return `
                        <label class="sck_container">
                            <input class="is_ck" type="checkbox" ${m.add_to_facture ? 'checked': ''}>
                            <span class="checkmark"></span>
                        </label>
               `;
            }
        },
    ]
});
$('#table tbody').on('click','button',function(){
    const closestRow = $(this).closest('tr');
    const data = table.row(closestRow).data();
    open_modal(data);
});

modal_btn_save.on('click',() => {
    let to_send = [];
    let err="";
    mo_table.rows().every(function (index, element) {
        const row = $(this.node());
        const check = row.find('.is_ck').eq(0)[0];
        if(check.checked){
            let d= this.data();
            
            let  txt = row.find('.item_total_class').eq(0)[0];
            let txt2 = row.find('.item_total_usd_class').eq(0)[0];
            d.NTotalCost = parseFloat(txt.value || 0);
            d.ntotal_cost_usd = parseFloat(txt2.value || 0);
            if( (!d.NTotalCost || isNaN(d.NTotalCost)) && (!d.ntotal_cost_usd || isNaN(d.ntotal_cost_usd)) ) {
                err = `Por favor ingrese un total valido para (${d.Description})`;
                return;
            }
           
                to_send.push(d);
        }
        
    });
    if(err) {
        toastr.warning(err);
        return;
    }
    if(to_send.length===0 || xmls.length===0){
        toastr.warning("Debe agregar al menos 1 factura y un artículo");
        return;
    }
    let xmls64 = [];
    const conv = (file,i)=> {
        let fileReader = new FileReader();
        return new Promise((resolve,reject)=>{
            fileReader.onerror = () => { reader.abort(); reject(new Error("Error parsing file"));}
            fileReader.onload = function (fileLoadedEvent) {
                const base64 = window.btoa(fileLoadedEvent.target.result);
                resolve({xml_name:file.name,xml:base64});
            };
            fileReader.readAsText(file);
        });
        
       
    };
    let proms = [];
    for(let i = 0; i<xmls.length; i++){
        proms.push(conv(xmls[i],i));
    } 
    Promise.all(proms).
    then((xmls64)=>{
        const data = {label:select_label_data,items:to_send,xmls:xmls64};
        axios.post('/Projects/ActionUploadXmls/',data)
            .then((res)=>{
                if(!res.data.Ok) {
                    if(res.data.Type === "sf"){ SessionFalse(res.data.Msg)}
                    else {toastr.error(res.data.Msg)}
                    return;
                }
                toastr.success("Las facturas se han registrado correctamente");
                
                updateTable(selected_project_data);
                modal.modal('hide');
            }).catch(e => toastr.error("Error al subir archivos") );
    }).catch(e => toastr.error("No se pudieron procesar los XML verifique que sean correctos") );
    
    
});
