﻿var label = []
var editProyect = false;
var descError = ""
var editLabelFlag = false;
var index = 0;
var idLabel = 0;

$(document).ready(function () {
    designChecks();
    $("#selectProject").select2();
    $("#category").select2();
    $("#usersCategory").select2({
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });
    $("#usersLabel").select2();
    $("#userLeaderLabel").select2();
});

$("#compromiseDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
    startDate: new Date(),
}).on("changeDate", function (e) {
});

function UpdateStatusHeader(status) {
    var idProject = $("#selectProject").val();
    var title = "";
    var desc = "";
    if (status == 8) { title = "¿Está seguro que desea Cancelar el proyecto?"; desc = "cancelado" } else {
        title = "¿Está seguro que desea Terminar el proyecto?"; desc = "terminado"
    }
    swal({
        title: title,
        text: "El no. de proyecto : " + idProject + " será " + desc,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    },
        function (isConfirm) {
            if (isConfirm) { ApprovedUpdate(idProject, status); }
        });
}

function ApprovedUpdate(id, status) {
    $.ajax({
        type: "POST",
        url: "/Projects/ActionUpdateStatusHeader",
        data: { "id": id, "status": status },
        success: function (response) {
            console.log(response)
            if (response == "SF") {
                EndLoading();
                SessionFalse("Terminó tu sesión")
            } else if (response.data == "OK") {
                if (status == 8) {
                    toastr.success("Proyecto Cancelado Correctamente");
                } else {
                    toastr.success("Proyecto Terminado Correctamente");
                }
                clearForm();
                $("#btnSend").show();
                $("button[name=btnCancel]").hide();
                $("#selectProject option:selected").remove();
                $("#selectProject").select2("val", null);
            } else {
                toastr.error(response.data);
            }
        },
        error: function () {
            EndLoading();
            toastr.error("Error desconocido , contacta a sistemas")
        }
    });
}

function newProject() {
    editProyect = false;
    editLabelFlag = false;
    clearForm();
    $("#btnSend").show();
    $("#selectProject").select2("val", null);
    $("button[name=btnCancel]").hide();
    $("#main").show();
    $("#main").animatePanel();
}

function setForm() {
    var idProject = $("#selectProject").val();
    if (idProject != "") {
        $.ajax({
            type: "POST",
            url: "/Projects/ActionGetDetailsByHeader",
            data: { "id": idProject },
            success: function (response) {
                console.log("sdfasdfasdfasd");
                console.log(response)
                if (response == "SF") {
                    EndLoading();
                    SessionFalse("Terminó tu sesión")
                } else {
                    $("#main").animatePanel();
                    $("#main").show();
                    editProyect = true;
                    $("#btnSend").hide();
                    $("button[name=btnCancel]").show();
                    $("#titleHeader").val(response.data.title).prop("disabled", false);
                    $("#descriptionHeader").val(response.data.description).prop("disabled", false);
                    var date = moment(response.data.compromise_date).format("DD/MM/YYYY");
                    $("#compromiseDate").val(date).prop("disabled", false);
                    $("#estimate").val(response.data.estimate).prop("disabled", false);
                    $("#estimate_usd").val(response.data.estimate_usd).prop("disabled", false);
                    $("#category").select2("val", response.data.id_category).prop("disabled", true);
                    $("#usersCategory").select2("val", response.data.users_category).prop("disabled", false);
                    label = response.data.label;
                    updateTable();
                    changeEditLabel(true)
                }
            },
            error: function () {
                EndLoading();
                toastr.error("Error desconocido , contacta a sistemas")
            }
        });
    } else {
        clearForm();
        $("#btnSend").show();
        $("button[name=btnCancel]").hide();
    }
}

var tableLabels = $('#tableLabels').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'IdLabelProject' },
            { data: null },
            { data: 'Description' },
            { data: 'userLeaderName' },
            { data: 'usersNameLabels' },
            { data: null }
        ],
    columnDefs:
        [{
            data: null,
            render: function (data, type, full, meta) {
                return `<button name="edit" class='btn btn-xs btn-outline btn-warning btn-edit' type='button' onclick = "editLabel('${data.IdLabelProject}' ,'${data.Description}')"><i class='fa fa-pencil'></i> Editar</button>`;
            },
            targets: [1],
            className: "text-center",
            orderable: false,
        },
        {
            data: null,
            targets: [5],
            render: function (data, type, full, meta) {
                return `<button class='btn btn-xs btn-outline btn-danger btn-remove' type='button' onclick = "deleteLabel('${data.Description}' , '${data.IdLabelProject}')"><i class='fa fa-trash'></i> Eliminar</button>`;
            },
            className: "text-center",
            orderable: false,
        }, {
            targets: [5, 1, 2, 3, 4],
            width: "1%"
        }]
});

tableLabels.column(0).visible(false);

function designChecks() {
    $('#labels').find('input[name="Level1"]').iCheck({
        radioClass: 'iradio_square-green'
    });
}

function deleteLabel(desc, idLabel) {
    if (editProyect) {
        if (label.length != 1) {
            $.ajax({
                type: "POST",
                url: "/Projects/ActionDeleteLabelById",
                data: { "idProj": $("#selectProject").val(), "desc": desc, "idLabel": idLabel },
                success: function (response) {
                    EndLoading();
                    if (response == "SF") {
                        SessionFalse("Terminó tu sesión")
                    } else if (response.data == "OK") {
                        toastr.success("Eliminado con éxito");
                        label = label.filter(x => x.IdLabelProject != idLabel)
                        updateTable();
                        //$("button[name='edit']").prop("disabled", true);
                    } else {
                        toastr.error(response.data);
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error("Error desconocido , contacta a sistemas")
                }
            });
        } else {
            toastr.error("El proyecto no se puede quedar sin etiquetas")
        }
    } else {
        label = label.filter(x => x.Description != desc);
        updateTable();
    }
}

function editLabel(id, desc) {
    if (editProyect) {
        changeEditLabel(true)
    } else {
        $("#descriptionLabel").prop("disabled", false);
        $("#userLeaderLabel").prop("disabled", false);
    }
    idLabel = id;
    index = label.findIndex(x => x.Description == desc);
    var row = label.filter(x => x.Description == desc)
    showModal("label", 1);
    var usersSelected = []
    for (var i = 0; i < row[0].usersNameLabels.length; i++) {
        var each = row[0].usersLabel[i] + "//" + row[0].usersNameLabels[i]
        usersSelected.push(each);
    };
    $("#userLeaderLabel").select2('val', row[0].EmpNo + "//" + row[0].userLeaderName);
    $("#usersLabel").select2('val', usersSelected);
    $("#descriptionLabel").val(row[0].Description);
}

function updateLabel() {
    if (editLabelFlag) {
        var arrayEmpNo = [];
        var users = $("#usersLabel").val()
        if (users != null)
            for (var i = 0; i < users.length; i++) {
                var u = users[i].split("//")[0];
                arrayEmpNo.push(u);
            }
        $.ajax({
            type: "POST",
            url: "/Projects/ActionEditLabelUsers",
            data: { "users": arrayEmpNo, "idLabel": idLabel },
            success: function (response) {
                console.log(response)
                EndLoading();
                if (response.data == "SF") {
                    SessionFalse("Terminó tu sesión")
                } else if (response.data) {
                    toastr.success("Editado con éxito");
                    closeModal("label");
                    var namesLabels = []
                    var numbersLabels = []
                    if ($("#usersLabel").val() != null) {
                        for (var i = 0; i < ($("#usersLabel").val()).length; i++) {
                            var name = $("#usersLabel").val()[i].split("//")[1]
                            var numbers = $("#usersLabel").val()[i].split("//")[0]
                            namesLabels.push(name);
                            numbersLabels.push(numbers)
                        }
                    }
                    label[index].usersLabel = numbersLabels
                    label[index].usersNameLabels = namesLabels
                    updateTable();

                } else {
                    toastr.error("Error desconocido , contacta a sistemas")
                }
            },
            error: function () {
                EndLoading();
                toastr.error("Error desconocido , contacta a sistemas")
            }
        });
    } else {
        if ($("#userLeaderLabel").val() == null) { toastr.error("Seleccione al encargado de la etiqueta"); return }
        var desc = $("#descriptionLabel").val();
        var userLeader = ($("#userLeaderLabel").val()).split("//")[0];
        var userLeaderName = ($("#userLeaderLabel").val()).split("//")[1];
        var namesLabels = []
        var numbersLabels = []
        if ($("#usersLabel").val() != null) {
            for (var i = 1; i <= ($("#usersLabel").val()).length; i++) {
                var name = $("#usersLabel").val()[i - 1].split("//")[1]
                var numbers = $("#usersLabel").val()[i - 1].split("//")[0]
                namesLabels.push(name);
                numbersLabels.push(numbers)
            }
        }
        var listLabel = label.filter(x => x.Description == desc)
        var listPosition = label[index];
        if ((listLabel.length == 1 && listPosition.Description == desc) || listLabel.length == 0) {
            label[index].EmpNo = userLeader;
            label[index].Description = desc;
            label[index].userLeader = userLeader;
            label[index].userLeaderName = userLeaderName;
            label[index].usersNameLabels = namesLabels;
            label[index].usersLabel = numbersLabels;
            updateTable();
            $("#descriptionLabel").val("");
            $("#usersLabel").select2("val", null);
            $("#userLeaderLabel").select2("val", null);
            closeModal("label");

        } else {
            toastr.error("Ya existe una etiqueta con la misma descripción");
        }
    }
}

function updateTable() {
    tableLabels.clear();
    tableLabels.rows.add(label);
    tableLabels.columns.adjust();
    tableLabels.draw();
}

function showModal(modal, edit) {
    validatorCategory.resetForm();
    if (modal == "category") {
        $('#modalCategory').modal('show');
    } else if (edit == 1) {
        $('#modalLabel').modal('show');
        $("#addLabel").hide()
        $("#updateLabel").show();
        $("#descriptionLabel").val("");
        $("#usersLabel").select2("val", null);
        $("#userLeaderLabel").select2("val", null);
    } else if (modal == "label") {
        $("#descriptionLabel").prop("disabled", false);
        $("#userLeaderLabel").prop("disabled", false);
        $('#modalLabel').modal('show');
        $("#addLabel").show()
        $("#updateLabel").hide();
        $("#descriptionLabel").val("");
        $("#usersLabel").select2("val", null);
        $("#userLeaderLabel").select2("val", null);
    }
}

function closeModal(modal) {
    $('#modalCategory').modal('hide');
    $('#modalLabel').modal('hide');
}

function checkEmpNoLabel() {
    for (var i = 0; i < label.length; i++) {
        if (label[i].EmpNo == "") {
            descError = label[i].Description;
            return false;
        }
    }
    return true;
}

function send(update) {
    var idProject = $("#selectProject").val();
    $("#titleHeader").val($("#titleHeader").val().trim());
    $("#descriptionHeader").val($("#descriptionHeader").val().trim());
    $("#compromiseDate").val($("#compromiseDate").val().trim());
    $("#estimate").val($("#estimate").val().trim());
    $("#estimate_usd").val($("#estimate_usd").val().trim());
    if (!$("#estimate").val().trim()) {
        $("#estimate").val(0);
    }
    if (!$("#estimate_usd").val().trim()) {
        $("#estimate_usd").val(0);
    }
    $("#descriptionCategory").val($("#descriptionCategory").val().trim());

    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    if (validatorHeader.element("#titleHeader") && validatorHeader.element("#descriptionHeader") && validatorHeader.element("#estimate") && validatorHeader.element("#estimate_usd")) {
        StartLoading();
        if ($("#category").val() != "") {
            if ($("#compromiseDate").val() != "" && $("#compromiseDate").val().match(RegExPattern)) {
                if (checkEmpNoLabel()) {
                    if (label.length > 0) {
                        if ($("#estimate").val() > 0 || $("#estimate_usd").val() > 0) {
                            var model = {
                                id_project: idProject,
                                title: $("#titleHeader").val(),
                                description: $("#descriptionHeader").val(),
                                compromise_date: $("#compromiseDate").val(),
                                compromise_date_str: $("#compromiseDate").val(),
                                estimate: $("#estimate").val(),
                                estimate_usd: $("#estimate_usd").val(),
                                id_category: $("#category").val(),
                                users_category: $("#usersCategory").val(),
                                label: label
                            };
                            console.log(model);
                            let url = "/Projects/ActionAddHeader";
                            if (update) url = "/Projects/ActionUpdateHeader";
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: { "model": model },
                                success: function (response) {
                                    EndLoading();
                                    if (response === "SF") {
                                        SessionFalse("Terminó tu sesión")
                                    } else if (response.model.Item1) {
                                        if (update) {
                                            toastr.success("Actualizado con éxito");
                                            setForm()
                                        } else {
                                            toastr.success("Agregado con éxito");
                                            sendEmails(response.model.Item2)
                                            $("#selectProject").append(new Option($("#titleHeader").val(), response.model.Item2));
                                            clearForm();
                                        }
                                    } else {
                                        toastr.error(response.model.Item2)
                                    }
                                },
                                error: function () {
                                    EndLoading();
                                    toastr.error("Error desconocido , contacta a sistemas")
                                }
                            });
                        } else {
                            EndLoading();
                            toastr.error("Ingrese una canitdad mayor a 0");
                            if ($("#estimate").val() > 0) {
                                $("#estimate_usd").focus();
                            } else {
                                $("#estimate").focus();
                            }

                        }
                    } else {
                        EndLoading();
                        toastr.error("El proyecto debe tener al menos una etiqueta")
                    }
                } else {
                    EndLoading()
                    toastr.error("Seleccione empleado encargado por la etiqueta " + descError + ", o eliminela")
                }
            } else {
                EndLoading()
                toastr.error("Agregue Fecha de Compromiso");
            }
        } else {
            EndLoading();
            toastr.error("Debe seleccionar la categoria del proyecto");
            $("#category").focus();
        }
    }
}

function sendEmails(idHeader) {
    $.ajax({
        type: "POST",
        url: "/Projects/ActionEmail",
        data: { "IdProject": idHeader, "IdLabel": 0, "item": 0, "history": false, "allUsers": true },
        success: function (response) {
            EndLoading();
            if (response == "SF") {
                SessionFalse("Terminó tu sesión")
            } else if (response) {
                toastr.success("Correos enviados correctamente");

            } else {
                toastr.error("Error desconocido , contacta a sistemas")
            }
        },
        error: function () {
            EndLoading();
            toastr.error("Error desconocido , contacta a sistemas")
        }
    });
}

function addLabel() {
    $("#descriptionLabel").val($("#descriptionLabel").val().trim());
    if (label.filter(x => x.Description.trim().toLowerCase() == $("#descriptionLabel").val().trim().toLowerCase()).length == 0) {
        if (validatorLabel.element("#descriptionLabel")) {
            if ($("#userLeaderLabel").val() != "") {
                var descLabel = $("#descriptionLabel").val();
                var userLeader = ($("#userLeaderLabel").val()).split("//")[0];
                var usersLabel = $("#usersLabel").val();
                var userNameLeader = ($("#userLeaderLabel").val()).split("//")[1];
                var namesLabels = []
                var numbersLabels = []
                if (usersLabel != null) {
                    for (var i = 1; i <= ($("#usersLabel").val()).length; i++) {
                        var name = $("#usersLabel").val()[i - 1].split("//")[1]
                        var numbers = $("#usersLabel").val()[i - 1].split("//")[0]
                        namesLabels.push(name);
                        numbersLabels.push(numbers)

                    }
                }
                if (label.length == 0 || label.map(x => x.Description == descLabel)[0] == false) {
                    if (editProyect) {
                        var modelLabel = { Description: descLabel, EmpNo: userLeader, userLeaderName: userNameLeader, usersLabel: numbersLabels, usersNameLabels: namesLabels }
                        $.ajax({
                            type: "POST",
                            url: "/Projects/ActionAddLabelById",
                            data: { "idHeader": $("#selectProject").val(), "model": modelLabel, "idCategory": $("#category").val() },
                            success: function (response) {
                                EndLoading();
                                if (response == "SF") {
                                    SessionFalse("Terminó tu sesión")
                                } else if (response.data) {
                                    toastr.success("Agregado con éxito");
                                    label.push({ Description: descLabel, EmpNo: userLeader, userLeaderName: userNameLeader, usersLabel: numbersLabels, usersNameLabels: namesLabels, IdLabelProject: 0 });
                                    updateTable();
                                    closeModal('label');
                                } else {
                                    toastr.error("Error desconocido , contacta a sistemas")
                                }
                            },
                            error: function () {
                                EndLoading();
                                toastr.error("Error desconocido , contacta a sistemas")
                            }
                        });
                    } else {
                        label.push({ Description: descLabel, EmpNo: userLeader, userLeaderName: userNameLeader, usersLabel: numbersLabels, usersNameLabels: namesLabels, IdLabelProject: 0 });
                        updateTable();
                        toastr.success("Etiqueta Agregada Correctamente");
                        $("#modalLabel").modal('hide');
                        $("#descriptionLabel").val("");
                        $("#usersLabel").select2("val", null);
                        $("#userLeaderLabel").select2("val", null);
                    }
                } else {
                    toastr.error("Ya existe una etiqueta con la misma descripción");
                }
            }
            else {
                toastr.error("Debe seleccionar un usuario encargado por la etiqueta");
            }
        } else {

        }
    } else {
        toastr.error("Ya existe una etiqueta con la misma descripción");
    }
}

function addCategory() {
    $("#descriptionCategory").val($("#descriptionCategory").val().trim());
    if (validatorCategory.element("#descriptionCategory")) {
        $.ajax({
            type: "POST",
            url: "/Projects/ActionAddCategory",
            data: { "description": $("#descriptionCategory").val() },
            success: function (response) {
                console.log(response);
                if (response == "SF") {
                    EndLoading();
                    SessionFalse("Terminó tu sesión")
                } else if (response != null) {
                    toastr.success("Agregado con éxito");
                    $("#descriptionCategory").val("");
                    var o = new Option(response.description, response.id_category);
                    $(o).html(response.description);
                    $("#category").append(o);
                    closeModal("category");
                } else {
                    toastr.error("Error desconocido , contacta a sistemas")
                }
            },
            error: function () {
                EndLoading();
                toastr.error("Error desconocido , contacta a sistemas")
            }
        });
    }
}

function getLabelsCategory() {
    var id = $("#category").val()
    if (label.length == 0) {
        if (id != "") {
            label = []
            updateTable();
            $("#labels").empty()
            $.ajax({
                type: "POST",
                url: "/Projects/ActionGetLabelsByCategory",
                data: { "id": id },
                success: function (response) {
                    console.log(response)
                    if (response == "SF") {
                        EndLoading();
                        SessionFalse("Terminó tu sesión")
                    } else if (response != null) {
                        $.each(response, function (index, value) {
                            label.push({ Description: value.description, EmpNo: "", userLeaderName: "", usersLabel: [], usersNameLabels: [], IdLabelProject: 0 });
                            updateTable();
                            designChecks();
                        })
                        closeModal("category");
                    } else {
                        toastr.error("Error desconocido , contacta a sistemas")
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error("Error desconocido , contacta a sistemas")
                }
            });
        }
    }
}

function clearForm() {
    $("#titleHeader").val("").prop("disabled", false);
    $("#descriptionHeader").val("").prop("disabled", false);
    $("#compromiseDate").val("").prop("disabled", false);
    $("#estimate").val("").prop("disabled", false);
    $("#estimate_usd").val("").prop("disabled", false);
    $("#category").select2("val", null).prop("disabled", false);
    $("#usersCategory").select2("val", null).prop("disabled", false);
    label = []
    updateTable();
}

var validatorCategory = $("#formCategory").validate({
    ignore: [],
    rules: {
        descriptionCategory: {
            required: true, minlength: 4, maxlength: 100
        }
    },
    messages: {
        descriptionCategory: {
            required: "El Campo no puede estar vacio", minlength: 'Favor de ingresar mas de 4 caracteres', maxlength: 'Máximo 25 caracteres'
        }
    }
});

var validatorHeader = $("#formHeader").validate({
    ignore: [],
    rules: {
        titleHeader: {
            required: true, minlength: 4, maxlength: 100
        },
        descriptionHeader: {
            required: true, minlength: 4, maxlength: 100
        },
        estimate: {
            required: true, minlength: 1, maxlength: 25
        },
        estimate_usd: {
            required: true, minlength: 1, maxlength: 25
        }
    },
    messages: {
        titleHeader: {
            required: "El Campo no puede estar vacio", minlength: 'Favor de ingresar mas de 4 caracteres', maxlength: 'Máximo 25 caracteres'
        },
        descriptionHeader: {
            required: "El Campo no puede estar vacio", minlength: 'Favor de ingresar mas de 4 caracteres', maxlength: 'Máximo 25 caracteres'
        },
        estimate: {
            required: "El Campo no puede estar vacio", minlength: 'Favor de ingresar mas de 1 caracter', maxlength: 'Máximo 10 caracteres'
        },
        estimate_usd: {
            required: "El Campo no puede estar vacio", minlength: 'Favor de ingresar mas de 1 caracter', maxlength: 'Máximo 10 caracteres'
        }
    }
});

var validatorLabel = $("#formLabel").validate({
    ignore: [],
    rules: {
        descriptionLabel: {
            required: true, minlength: 4, maxlength: 50
        }
    },
    messages: {
        descriptionLabel: {
            required: "El Campo no puede estar vacio", minlength: 'Favor de ingresar mas de 4 caracteres', maxlength: 'Máximo 25 caracteres'
        }
    }
});

function RealTimeItem(input) {
    let num = parseFloat(input.value);
    num = isNaN(num) || num < 0 || num > 9999999999 ? 0 : num;
    input.value = num;
}

function changeEditLabel(edit) {
    if (edit) {
        editLabelFlag = true;
        $("#descriptionLabel").prop("disabled", true);
        $("#userLeaderLabel").prop("disabled", true);
    } else {
        editLabelFlag = false;
        $("#descriptionLabel").prop("disabled", false);
        $("#userLeaderLabel").prop("disabled", false);
    }
}