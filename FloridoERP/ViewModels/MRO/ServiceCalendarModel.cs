﻿using App.Entities;
using App.Entities.ViewModels.MaSupplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.MRO
{
    public class ServiceCalendarModel
    {
        private SERVICES_CALENDAR_DETAIL _servicesCalendarDetail;

        public ServiceCalendarModel()
        {
            _servicesCalendarDetail = new SERVICES_CALENDAR_DETAIL();
            MaCode = new List<MA_CODE>();
        }

        //header
        public int service_id { get; set; }
        public DateTime service_date { get; set; }
        public int service_status { get; set; }
        public string site_code { get; set; }
        public string service_department { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public string program_id { get; set; }
        //detail
        public string equipment_id { get; set; }
        public string supplier_id { get; set; }
        public string service_cost { get; set; }
        public string quotation_document { get; set; }
        public List<MA_CODE> MaCode { get; set; }
        public string currency { get; set; }
        public virtual List<SITES> Site { get; set; }
        public IEnumerable<SelectListItem> Sites
        {
            get
            {
                return FillSelectListSites();
            }
        }
        public List<SelectListItem> FillSelectListSites()
        {
            List<SelectListItem> sitesL = new List<SelectListItem>();
            sitesL.Add(new SelectListItem { Text = "Selecciona una Sucursal", Value = "" });
            foreach (var n in Site)
            {
                sitesL.Add(new SelectListItem { Text = n.site_name, Value = n.site_code });
            }
            return sitesL;
        }

        public virtual List<MA_CODE> Code { get; set; }
        public IEnumerable<SelectListItem> Codes
        {
            get
            {
                return FillSelectListCodes();
            }
        }
        public List<SelectListItem> FillSelectListCodes()
        {
            List<SelectListItem> codesL = new List<SelectListItem>();
            codesL.Add(new SelectListItem { Text = "Selecciona un Departamento", Value = "" });
            foreach (var n in Code)
            {
                codesL.Add(new SelectListItem { Text = n.description, Value = n.vkey });
            }
            return codesL;
        }

        public virtual List<MaSupplierDropDownModel> Supplier { get; set; }
        public IEnumerable<SelectListItem> Suppliers
        {
            get
            {
                return FillSelectListSuppliers();
            }
        }
        public List<SelectListItem> FillSelectListSuppliers()
        {
            List<SelectListItem> suppliersL = new List<SelectListItem>();
            suppliersL.Add(new SelectListItem { Text = "Selecciona un Proveedor", Value = "" });
            foreach (var n in Supplier)
            {
                suppliersL.Add(new SelectListItem { Text = n.business_name, Value = n.supplier_id.ToString() });
            }
            return suppliersL;
        }

        public SERVICES_CALENDAR_DETAIL ServiceCalendarDetail
        {
            get
            {
                return new SERVICES_CALENDAR_DETAIL
                {
                    equipment_id = Int32.Parse(equipment_id),
                    supplier_id = Int32.Parse(supplier_id),
                    service_cost = Decimal.Parse(service_cost),
                    service_department = service_department,
                    quotation_document = quotation_document
                };
            }
        }
    }
}