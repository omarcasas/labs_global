﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.Projects;
using App.BLL.Projects;
using App.Entities;

namespace FloridoERP.ViewModels.Project
{
    public class PROJ001Model
    {
        private ProjectHeaderBusiness _projectHeaderBusiness = new ProjectHeaderBusiness();
        private ProjectCategoryBusiness _projectCategoryBusiness = new ProjectCategoryBusiness();
        public List<ProjectCategoryModel> Categories { get; set; }
        public List<ProjectDescriptionModel> Labels {get;set;}
        public List<USER_MASTER> Users { get; set; }
        public List<ProjectHeaderModel> Headers { get; set; }
        public IEnumerable<SelectListItem> SiteList
        {
            get
            {
                return FillSelectListCategories();
            }
        }
        public List<SelectListItem> FillSelectListCategories()
        {
            var categories = _projectCategoryBusiness.GetGetAllCategory();
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in categories)
            {
                list.Add(new SelectListItem { Text = n.description, Value = n.id_category.ToString() });
            }
            return list;
        }
        
    }
}