﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Lookups
{
    public class Lookups
    {       
        public List<SelectListItem> Currencies()
        {
            List<SelectListItem> List = new List<SelectListItem>();
            List.Add(new SelectListItem { Text = "Pesos", Value = "MXN" });
            List.Add(new SelectListItem { Text = "Dolares", Value = "USD" });
            return List;
        }

        public List<SelectListItem> VoucherType()
        {
            List<SelectListItem> List = new List<SelectListItem>();
            List.Add(new SelectListItem { Text = "--Selecciona Tipo Vale--", Value = "" });
            List.Add(new SelectListItem { Text = "Electrónico", Value = "E" });
            List.Add(new SelectListItem { Text = "Vale Físico", Value = "V" });
            return List;
        }
    }
}