﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Supplier
{
    public class SupplierContactsModel
    {
        private MA_SUPPLIER_CONTACT _supplierContact;

        public SupplierContactsModel()
        {
            _supplierContact = new MA_SUPPLIER_CONTACT();
        }
        public int supplier_contact_id { get; set; }
        public string name { get; set; }
        public int supplier_id { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public bool flagActive { get; set; }
        public string email { get; set; }
        public string department { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string program { get; set; }

        public MA_SUPPLIER_CONTACT SupplierContact
        {
            get
            {
                return new MA_SUPPLIER_CONTACT
                {
                    supplier_id = supplier_id,
                    name = name,
                    last_name = last_name,
                    phone = phone,
                    email = email,
                    departament = department,
                    flagActive = flagActive,
                    cdate = cdate,
                    cuser = cuser,
                    program_id = program
                };
            }
        }
    }
}