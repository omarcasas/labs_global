﻿using App.Entities;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.FixedAsset
{
    public class FixedAssetRegistrationViewModel
    {
        public string site_code { get; set; }
        public string fixed_asset_code { get; set; }
        public int requisition_code { get; set; }
        public string currency { get; set; }
        public int supplier_id { get; set; }

        public List<SiteModel> Sites { get; set; }
        public List<FIXED_ASSET> FixedAsset { get; set; }
        public virtual List<MA_SUPPLIER> Supplier { get; set; }
        public virtual List<MA_CODE> Currency { get; set; }
        public List<MA_CODE> ListProcess { get; set; }


        public IEnumerable<SelectListItem> CurrencySelectList
        {
            get
            {
                return FillSelectListOferts();
            }
        }
        public List<SelectListItem> FillSelectListOferts()
        {
            List<SelectListItem> ofertass = new List<SelectListItem>();
            ofertass.Add(new SelectListItem { Text = "Seleccione una moneda", Value = "" });
            foreach (var n in Currency)
            {
                ofertass.Add(new SelectListItem { Text = n.description, Value = n.vkey });
            }

            return ofertass;
        }

        public IEnumerable<SelectListItem> SitesSelectList
        {
            get
            {
                List<SelectListItem> selectListSupplier = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Seleccione una opción", Value = "" }
                };

                foreach (var item in Sites)
                {
                    selectListSupplier.Add(new SelectListItem { Text = item.SiteName, Value = item.SiteCode.ToString() });
                }

                return selectListSupplier;
            }
        }

        public IEnumerable<SelectListItem> FixedAssetSelectList
        {
            get
            {
                List<SelectListItem> selectListSupplier = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Seleccione una opción", Value = "" }
                };

                foreach (var item in FixedAsset)
                {
                    selectListSupplier.Add(new SelectListItem { Text = item.fixed_asset_name, Value = item.fixed_asset_code.ToString() });
                }

                return selectListSupplier;
            }
        }

        public IEnumerable<SelectListItem> SupplierSelectList
        {
            get
            {
                return FillSelectListItemSupplier();
            }
        }
        public List<SelectListItem> FillSelectListItemSupplier()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un proveedor", Value = "0" });
            foreach (var n in Supplier)
            {
                items.Add(new SelectListItem { Text = n.business_name, Value = n.supplier_id.ToString() });
            }

            return items;
        }

        public IEnumerable<SelectListItem> ListSelectProcess
        {
            get
            {
                return FillSelectListProcess();
            }
        }
        public List<SelectListItem> FillSelectListProcess()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in ListProcess)
            {
                items.Add(new SelectListItem { Text = n.description.Trim(), Value = n.vkey.Trim() });
            }

            return items;
        }

    }
}