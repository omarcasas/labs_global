﻿using App.DAL.FixedAsset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.FixedAsset
{
    public class FixedAssetRequisitionViewModel
    {

        public List<FixedAssetRequisitionModel> ListPendinRequisition { get; set; }

    }
}