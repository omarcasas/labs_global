﻿using App.BLL.CommonBusiness;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.FixedAsset
{
    public class FixedAssetViewModel
    {

        public List<FIXED_ASSET> ListFixedAsset { get; set; }
        public List<MA_CODE> ListCategory { get; set; }
        public List<MA_CODE> ListDepreciation { get; set; }
        public List<SITES> ListSites { get; set; }


        public string category { get; set; }
        public string name { get; set; }
        public bool status_flag { get; set; }
        public string site_code { get; set; }

        public IEnumerable<SelectListItem> ListSelectSite
        {
            get
            {
                return FillSelectListSite();
            }
        }
        public List<SelectListItem> FillSelectListSite()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una sucursal", Value = "0" });
            foreach (var n in ListSites)
            {
                items.Add(new SelectListItem { Text = n.site_name.Trim(), Value = n.site_code.Trim() });
            }

            return items;
        }


        public IEnumerable<SelectListItem> ListSelectCategory
        {
            get
            {
                return FillSelectListCategoty();
            }
        }
        public List<SelectListItem> FillSelectListCategoty()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in ListCategory)
            {
                items.Add(new SelectListItem { Text = n.description.Trim(), Value = n.vkey.Trim() });
            }

            return items;
        }

        public IEnumerable<SelectListItem> ListSelectDepreciation
        {
            get
            {
                return FillSelectListDepreciation();
            }
        }
        public List<SelectListItem> FillSelectListDepreciation()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in ListDepreciation)
            {
                items.Add(new SelectListItem { Text = n.description.Trim(), Value = n.vkey.Trim() });
            }

            return items;
        }

    }
}