﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Purchases
{
    public class AddPromotionAdvancedViewModel
    {
        public DateTime promotion_start_date { get; set; }
        public DateTime promotion_end_date { get; set; }
        public decimal promoion_price { get; set; }
        //
        public int promotion_quantity { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string program { get; set; }

        public PROMOTION_ADVANCED_HEADER promotionAdvH
        {
            get
            {
                return new PROMOTION_ADVANCED_HEADER
                {
                    promotion_start_date = promotion_start_date,
                    promotion_end_date = promotion_end_date,
                    cuser = cuser,
                    cdate = cdate,
                    program_id = ""
                };
            }
        }
    }
}