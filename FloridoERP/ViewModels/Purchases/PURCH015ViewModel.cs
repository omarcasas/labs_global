﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH015ViewModel
    {
        public virtual List<SITES> Site { get; set; }
        public IEnumerable<SelectListItem> SiteList
        {
            get
            {
                return FillSelectListSite();
            }
        }
        public List<SelectListItem> FillSelectListSite()
        {
            List<SelectListItem> sites = new List<SelectListItem>();
            sites.Add(new SelectListItem { Text = "Todas las Tiendas", Value = "All" });
            foreach (var n in Site)
            {
                sites.Add(new SelectListItem { Text = n.site_name, Value = n.site_code });
            }
            return sites;
        }
    }
}