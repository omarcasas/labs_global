﻿using App.Entities;
using App.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using App.BLL.CommonBusiness;
using System.Web.Mvc;
using App.DAL.SAT;
using App.Entities.ViewModels.Item;
using App.BLL.Purchases;
using App.DAL.Item;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH004ViewModel
    {
        public List<ITEM> Items { get; set; }
        private List<MA_CODE> Um { get; set; }
        private List<MA_CODE> buyerDivision { get; set; }
        private List<MA_CODE> ItemType { get; set; }
        private List<MA_TAX> Iva { get; set; }
        private List<MA_TAX> Ieps { get; set; }
        private List<SATDivision> satDivision { get; set; }
        private List<SATum> satUm { get; set; }
        public List<MA_CLASS> MaClass { get; set; }
        public List<ItemViewModel> itemlist { get; set; }
        private List<ItemOldErpModel> DistriType { get; set; }
        private List<ItemOldErpModel> ItemGrup { get; set; }
        private List<ItemOldErpModel> ItemUm { get; set; }
        public ITEM_SPECS itemSpecs { get { return new ITEM_SPECS { part_number = part_number, packing_of_size = packing_of_size, pallet_of_size = pallet_of_size, weight_of_packing = weight_of_packing, dun14 = dun14, gr_storage_location = gr_storage_location}; } }
        public MA_CLASS MA_CLASS { get { return new MA_CLASS { class_name = MaClassclass_name, description = MaClassDescription, level_category = MaClassLevelCategory, parent_class = MaClassParentClass, cdate = DateTime.Now, cuser = cuser, program_id = "PURCH004.cshtml" }; } }
        public COMBO_HEADER comboHeader { get { return new COMBO_HEADER { part_number_combo = part_number, combo_status = combo_status, cdate = cdate, cuser = cuser, program_id = "PURCH004.cshtml" }; } }
        public PURCH004ViewModel()
        {
            var itemMaster = new ItemMasterBusiness();

            var Ivaitems = new SysMaTaxBusiness();
            Iva = Ivaitems.GetAllIva();

            var Iepsitems = new SysMaTaxBusiness();
            Ieps = Iepsitems.GetAllIeps();

            var satDivisionitems = new productCatalogueSatRepository();
            satDivision = satDivisionitems.GetAllSATDivision();

            var getDistributionTypeOldERP = new ItemOldErpRepository();
            DistriType = getDistributionTypeOldERP.getDistributionType();

            var getItemGrup = new ItemOldErpRepository();
            ItemGrup = getDistributionTypeOldERP.getItemGrup();

            var GetItemUm = new ItemOldErpRepository();
            ItemUm = getDistributionTypeOldERP.getItemUm();


            
            Items = itemMaster.GetAllItemsAll();

            itemlist = new List<ItemViewModel>();
            foreach (var itm in Items)
            {                
                var itemN = new ItemViewModel()
                {
                    active_flag = itm.active_flag,
                    extra_items_flag = itm.extra_items_flag == null ? false: itm.extra_items_flag.Value,
                    buyer_division = itm.buyer_division,                    
                    part_iva = Iva.Find(i => i.tax_code == (string.IsNullOrEmpty(itm.part_iva_purchase) ? "N/A" : itm.part_iva_purchase)).name,                    
                    part_iva_sale = Iva.Find(i => i.tax_code == (string.IsNullOrEmpty(itm.part_iva_sale) ? "N/A" : itm.part_iva_sale)).name,                    
                    part_ieps = Ieps.Find(i => i.tax_code == (string.IsNullOrEmpty(itm.part_ieps) ? "N/A" : itm.part_ieps)).name,
                    part_description = itm.part_description,
                    part_number = itm.part_number,
                    part_number_sat = itm.part_number_sat,
                    part_type = itm.part_type,
                    unit = itm.unit_size,
                    unit_sat = itm.unit_sat,
                    weight_flag = itm.weight_flag,
                    combo_flag=itm.combo_flag
                };
                itemlist.Add(itemN);
            }

            var UMitems = new SysMaCodeBusiness();
            Um = UMitems.GetAllUM();


            var DivisionBuyer = new SysMaCodeBusiness();
            buyerDivision = DivisionBuyer.GetAllBuyerDivision();

            var typeItems = new SysMaCodeBusiness();
            ItemType = typeItems.GetAllItemType();

            var MaClassItems = new SysMaClassBusiness();
            MaClass = MaClassItems.GetMaClassDivision(0, 1);

        }
        [Display(Name = "Codigo de Producto")]
        public string part_number { get; set; }

        [Display(Name = "Nombre")]
        public string part_description { get; set; }
        [Display(Name = "Días de Caducidad")]
        public int expiration_days { get; set; }

        [Display(Name = "Unidad")]
        public string unit { get; set; }
        [Display(Name = "Tipo")]
        public string part_type { get; set; }
        [Display(Name = "División de Comprador")]
        public string buyer_division { get; set; }
        [Display(Name = "Activo")]
        public bool active_flag { get; set; }
        [Display(Name = "Peso")]
        public bool weight_flag { get; set; }
        [Display(Name = "IVA Compra")]
        public string part_iva { get; set; }
        [Display(Name = "IVA Venta")]
        public string part_iva_sale { get; set; }
        [Display(Name = "IEPS")]
        public string part_ieps { get; set; }

        [Display(Name = "SAT NO.")]
        public string part_number_sat { get; set; }
        [Display(Name = "SAT Unidad")]
        public string unit_sat { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string MaClassclass_name { get; set; }
        public string MaClassDescription { get; set; }
        public int MaClassLevelCategory { get; set; }
        public int MaClassParentClass { get; set; }
        public int level_header { get; set; }
        public int level1_code { get; set; }
        public int level2_code { get; set; }
        public int level3_code { get; set; }
        public int divisionSAT { get; set; }
        public decimal packing_of_size { get; set; }
        public decimal pallet_of_size { get; set; }
        public decimal weight_of_packing { get; set; }
        public string dun14 { get; set; }
        public string gr_storage_location { get; set; }
        public string combo_status { get; set; }
        public bool combo_flag { get; set; }
        public bool extra_items_flag { get; set; }

        //ERP/WMS

        public Int64 WMSSupplier { get; set; }
        public string WMSDistributionType { get; set; }
        public int WMSGroup { get; set; }
        public int WMSFamili { get; set; }
        public string WMSCurrency { get; set; }
        public decimal WMSUnitCost { get; set; }
        public decimal WMSUnitPrice { get; set; }
        public decimal WMSMarg { get; set; }
        public decimal WMSIvaPrice { get; set; }
        public string WMSUm { get; set; }
        public string WMSwweight { get; set; }
        public int WMSBuyerId { get; set; }
        public string WMSmrp_type { get; set; }


        public ITEM Item
        {
            get
            {
                return new ITEM
                {
                    part_number = part_number,
                    part_description = part_description,
                    unit_size = unit,
                    level_header= level_header,
                    level1_code= level1_code,
                    level2_code = level2_code,
                    level3_code = level3_code,
                    part_type = part_type,
                    buyer_division = buyer_division,
                    active_flag = active_flag,
                    extra_items_flag = extra_items_flag,
                    weight_flag = weight_flag,
                    part_iva_purchase = part_iva,
                    part_iva_sale = part_iva_sale,
                    part_ieps = part_ieps,
                    part_number_sat = part_number_sat,
                    unit_sat = unit_sat,
                    combo_flag= combo_flag,
                    expiration_days = expiration_days,
                    cdate = cdate,
                    cuser = cuser,
                    udate = udate,
                    uuser = uuser,
                    program_id = "PURCH004.cshtml"


                    //ERP/WMS

                };
            }
        }
        public ITEM_TEMP_ERP ItemWMS
        {
            get
            {
                return new ITEM_TEMP_ERP
                {
                    product_id = part_number,
                    product_name = part_description,
                    product_description = part_description,
                    product_nick = part_description,
                    um = unit,
                    iva = part_iva,
                    ieps = part_ieps,
                    distribution_type = WMSDistributionType,
                    buyer_division = buyer_division,
                    product_type = part_type,
                    ERP_part_cost = WMSUnitCost,
                    ERP_part_cost_cur = WMSCurrency,
                    ERP_part_price = WMSUnitPrice,
                    ERP_part_price3 = WMSIvaPrice,
                    ERP_part_price_cur = WMSCurrency,
                    ERP_supplier_id = WMSSupplier,
                    ERP_level1_code = WMSFamili,
                    ERP_part_um = WMSUm,
                    ERP_grupo_id = WMSGroup,
                    ERP_part_margen = WMSMarg,
                    ERP_part_margen3 = WMSMarg,
                    ERP_part_pesado = WMSwweight,
                    ERP_buyer_id = WMSBuyerId,
                    mrp_type = WMSmrp_type,
                    
                    cdate = cdate,
                    cuser = cuser,
                    udate = udate,
                    uuser = uuser,
                    sat_code = part_number_sat
                };
            }
        }
        public IEnumerable<SelectListItem> UMList
        {
            get
            {
                return FillSelectListItemUm();
            }
        }
        public List<SelectListItem> FillSelectListItemUm()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Um)
            {
                items.Add(new SelectListItem { Text = n.vkey, Value = n.vkey });
            }

            return items;
        }
        public IEnumerable<SelectListItem> IVAList
        {
            get
            {
                return FillSelectListItemIva();
            }
        }
        public List<SelectListItem> FillSelectListItemIva()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Iva)
            {
                items.Add(new SelectListItem { Text = n.name, Value = n.tax_code });
            }

            return items;
        }
        public IEnumerable<SelectListItem> IEPSList
        {
            get
            {
                return FillSelectListItemIeps();
            }
        }
        public List<SelectListItem> FillSelectListItemIeps()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Ieps)
            {
                items.Add(new SelectListItem { Text = n.name, Value = n.tax_code });
            }

            return items;
        }
        public List<SelectListItem> FillSelectListItemMaClass(int level)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var MaClassItems = new SysMaClassBusiness();
            MaClass = MaClassItems.GetMaClassDivisionForLevel(level);
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in MaClass)
            {
                items.Add(new SelectListItem { Text = n.class_name, Value = n.class_id.ToString() });
            }

            return items;
        }
        public IEnumerable<SelectListItem> SATDivision
        {
            get
            {
                return FillSelectListItemSatDivision();
            }
        }
        public List<SelectListItem> FillSelectListItemSatDivision()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in satDivision)
            {
                items.Add(new SelectListItem { Text = "("+ n.cve_division +") "+ n.division, Value = n.cve_division });
            }

            return items;
        }
        public IEnumerable<SelectListItem> BuyerDivision
        {
            get
            {
                return FillSelectListbuyerDivision();
            }
        }
        public List<SelectListItem> FillSelectListbuyerDivision()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in buyerDivision)
            {
                items.Add(new SelectListItem { Text = n.description.Trim(), Value = n.vkey.Trim() });
            }

            return items;
        }
        public IEnumerable<SelectListItem> itemType
        {
            get
            {
                return FillSelectListItemType();
            }
        }
        public List<SelectListItem> FillSelectListItemType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in ItemType)
            {
                items.Add(new SelectListItem { Text = n.description.Trim(), Value = n.vkey.Trim() });
            }

            return items;
        }
        public IEnumerable<SelectListItem> ERPDistrType
        {
            get
            {
                return FillSelectListItemDistriType();
            }
        }
        public List<SelectListItem> FillSelectListItemDistriType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            if(DistriType!=null)
            foreach (var n in DistriType)
            {
                items.Add(new SelectListItem { Text = n.vkey.Trim(), Value = n.vkey.Trim() });
            }

            return items;
        }
        public IEnumerable<SelectListItem> ERPItemGrup
        {
            get
            {
                return FillSelectListItemERPItemGrup();
            }
        }
        public List<SelectListItem> FillSelectListItemERPItemGrup()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            if(ItemGrup!=null)
            foreach (var n in ItemGrup)
            {
                items.Add(new SelectListItem { Text = n.grupo_desc.Trim(), Value = n.grupo_id.ToString() });
            }

            return items;
        }
        public IEnumerable<SelectListItem> ERPItemUm
        {
            get
            {
                return FillSelectListItemERPItemUm();
            }
        }
        public List<SelectListItem> FillSelectListItemERPItemUm()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            if(ItemUm!=null)
            foreach (var n in ItemUm)
            {
                items.Add(new SelectListItem { Text = n.um_desc, Value = n.um_desc});
            }

            return items;
        }

        
        public PURCH004ViewModel(string id)
        {


            ITEM item = new ItemMasterBusiness().GetItemByPartNumber(id);
            this.part_number = item.part_number;
            this.part_description = item.part_description;
            this.unit = item.unit_size;
            this.part_type = item.part_type;
            this.buyer_division = item.buyer_division;
            this.active_flag = item.active_flag.Value;
            this.extra_items_flag = item.extra_items_flag.Value;
            this.weight_flag = item.weight_flag.Value;
            this.part_iva = item.part_iva_purchase;
            this.part_iva_sale = item.part_iva_sale;
            this.part_ieps = item.part_ieps;
            this.part_number_sat = item.part_number_sat;
            this.unit_sat = item.unit_sat;
            this.expiration_days = item.expiration_days ?? 0;
            this.cdate = item.cdate;
            this.cuser = item.cuser;
            this.udate = item.udate;
            this.uuser = item.uuser;
        }
    }
}