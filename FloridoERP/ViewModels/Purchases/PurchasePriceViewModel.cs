﻿namespace FloridoERP.ViewModels.Purchases
{
    public class PurchasePriceViewModel
    {
        public string base_price { get; set; }
        public string base_price_id { get; set; }
        public string business_name { get; set; }
        public string currency { get; set; }
        public string part_name { get; set; }
        public string profit_margin { get; set; }
        public string sales_price { get; set; }
        public string valid_from { get; set; }
        public string valid_to { get; set; }
        public string supplier_part_number { get; set; }
    }
}