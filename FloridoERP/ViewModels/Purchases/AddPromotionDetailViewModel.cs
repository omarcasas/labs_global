﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Purchases
{
    public class AddPromotionDetailViewModel
    {
        private PROMOTION_DETAIL _promotionDetail;
        private PROMOTION_SITES _promotionSite;

        public AddPromotionDetailViewModel()
        {
            _promotionDetail = new PROMOTION_DETAIL();
            _promotionSite = new PROMOTION_SITES();
        }

        public string promotion_code { get; set; }
        public string part_number { get; set; }
        public decimal promotion_price { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string program_id { get; set; }
        public int promotion_detail_id { get; set; }
        public bool presentation_flag { get; set; }
        public string site_code { get; set; }

        public PROMOTION_DETAIL PromotionDetail
        {
            get
            {
                return new PROMOTION_DETAIL
                {
                    promotion_code = promotion_code,
                    part_number = part_number,
                    promotion_price = promotion_price,
                    cuser = cuser,
                    cdate = cdate,
                    program_id = ""
                };
            }
        }

        public PROMOTION_SITES PromotionSites
        {
            get
            {
                return new PROMOTION_SITES
                {
                    promotion_code = promotion_code,
                    site_code = site_code,
                    cuser = cuser,
                    cdate = cdate,
                    program_id = ""
                };
            }
        }
    }
}