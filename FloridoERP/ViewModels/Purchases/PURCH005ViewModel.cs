﻿using App.BLL;
using App.BLL.CommonBusiness;
using App.BLL.Purchases;
using App.BLL.Site;
using App.BLL.StorageLocation;
using App.Entities;
using App.Entities.ViewModels.Site;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH005ViewModel
    {
        public PURCH005ViewModel()
        {
            _business = new ItemMasterBusiness();
            var Currencyitems = new SysMaCodeBusiness();
            Currency = Currencyitems.GetAllCurrency();


            var locations = new StorageLocationBusiness();
            Locationsr = locations.GetAllLocationsInStatusActive();

            listAllItems5 = _business.GetAllItems();
            itemlist = new List<ItemViewModel>();
            foreach (var itm in listAllItems5)
            {
                var itemN = new ItemViewModel()
                {


                    part_number = itm.part_number,
                    part_description = itm.part_description,
                    unit_size = itm.unit_size,
                    part_ieps=itm.part_ieps,
                    part_type = itm.part_type,
                    buyer_division = itm.buyer_division,
                    part_iva = itm.part_iva_purchase,
                    part_number_sat = itm.part_number_sat,
                    unit_sat = itm.unit_sat,
                    weight_flag = itm.weight_flag,
                    active_flag = itm.active_flag
                };
                itemlist.Add(itemN);
            }
        }
        public List<ItemViewModel> itemlist { get; set; }



        private SiteBusiness _siteBusiness = new SiteBusiness();
        private ItemMasterBusiness _business;

        public List<ITEM> listAllItems5 { get; set; }
        public List<ITEM> listAllItems { get { return _business.GetAllItems(); } }
        public List<MA_SUPPLIER> SupplierList { get; set; }
        public List<ITEM_SUPPLIER> ItemSupplier { get; set; }
        public int supplier_part_number { get; set; }
        public string part_number { get; set; }
        public int supplier_id { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }
        public string business_name { get; set; }
        public string rfc { get; set; }
        public string city { get; set; }
        public string phone_sales { get; set; }
        public string email_sales { get; set; }
        public string currency { get; set; }
        private List<MA_CODE> Currency { get; set; }
        private List<StorageLocationModel> Locationsr { get; set; }
        public virtual List<SiteModel> Sites { get; set; }

        public ITEM_SUPPLIER ITEM_SUPPLIER { get { return new ITEM_SUPPLIER {part_number = part_number, supplier_id = supplier_id, cuser = cuser, cdate = cdate, udate = udate, uuser = uuser, program_id = "PURCH005.cshtml" }; } }

        public IEnumerable<SelectListItem> CurrencyList
        {
            get
            {
                return FillSelectListItemUm();
            }
        }
        public List<SelectListItem> FillSelectListItemUm()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Currency)
            {
                items.Add(new SelectListItem { Text = n.vkey, Value = n.vkey });
            }

            return items;
        }
        public IEnumerable<SelectListItem> Locations
        {
            get
            {
                return FillSelectListLocations();
            }
        }
        public List<SelectListItem> FillSelectListLocations()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Locationsr)
            {
                items.Add(new SelectListItem { Text = n.storage_location, Value = n.storage_location });
            }

            return items;
        }
        public IEnumerable<SelectListItem> SiteList
        {
            get
            {
                return FillSelectListSite();
            }
        }
        public List<SelectListItem> FillSelectListSite()
        {

            List<SiteModel> site = _siteBusiness.GetAllSitesName();
            List<SelectListItem> sites = new List<SelectListItem>();
            sites.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in site)
            {
                sites.Add(new SelectListItem { Text = n.SiteName, Value = n.SiteCode });
            }
            return sites;
        }


    }
}