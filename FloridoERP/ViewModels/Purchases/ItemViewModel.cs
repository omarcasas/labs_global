﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Purchases
{
    public class ItemViewModel
    {
        public string part_number { get; set; }
        public string ean_upc { get; set; }
        public string part_name { get; set; }
        public string part_description { get; set; }
        public string part_nick { get; set; }
        public string unit { get; set; }
        public string part_type { get; set; }
        public int expiration_days { get; set; }
        public string buyer_division { get; set; }
        public Nullable<bool> active_flag { get; set; }
        public Nullable<bool> weight_flag { get; set; }
        public string part_iva { get; set; }
        public string part_iva_sale { get; set; }
        public string part_ieps { get; set; }
        public string part_number_sat { get; set; }
        public string unit_sat { get; set; }
        public string unit_size { get; set; }
        public bool extra_items_flag { get; set; }
        public Nullable<bool> combo_flag { get; set; }
    }
}