﻿using App.Entities;
using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH005ViewModel2
    {
        public PURCH005ViewModel2()
        {
            table_supplier = new List<SupplierNotActiveModel>();
            table_price = new List<PurchasePriceViewModel>();
        }
        public string part_description { get; set; }
        public string part_name { get; set; }
        public string part_number { get; set; }
        public string part_nick { get; set; }
        public string buyer_division { get; set; }
        public string part_type { get; set; }
        public string active_flag { get; set; }
        public int expiration_days { get; set; }
        public decimal exchange_price { get; set; }
        public bool isForced90 { get; set; }
        public Nullable<bool> combo_flag { get; set; }
        public Nullable<bool> extra_items_flag { get; set; }
        public string weight_flag { get; set; }
        public string part_iva { get; set; }
        public string part_iva_sale { get; set; }
        public string part_ieps { get; set; }
        public string ean_upc { get; set; }
        public string part_number_sat { get; set; }
        public string unit_sat { get; set; }
        public string unit { get; set; }
        public string cdate { get; set; }
        public string udate { get; set; }
        public string cuser { get; set; }
        public int level_header { get; set; }
        public int level1_code { get; set; }
        public int level2_code { get; set; }
        public int level3_code { get; set; }
        public List<SupplierNotActiveModel> table_supplier { get; set; }
        public List<SelectListItem> table_Drop { get; set; }
        public List<PurchasePriceViewModel> table_price { get; set; }
    }                   
}