﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.InternalRequirement;
using App.Entities.ViewModels.Site;
using App.BLL.Site;
namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH020ViewModel
    {
        private SiteBusiness _siteBusiness = new SiteBusiness();
        public virtual List<InternalRequirementModel> InternalRequierement { get; set; }
        public virtual List<SiteModel> Sites { get; set; }
        public IEnumerable<SelectListItem> SiteList
        {
            get
            {
                return FillSelectListSite();
            }
        }
        public List<SelectListItem> FillSelectListSite()
        {

            List<SiteModel> site = _siteBusiness.GetAllSitesName();
            List<SelectListItem> sites = new List<SelectListItem>();
            sites.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in site)
            {
             sites.Add(new SelectListItem { Text = n.SiteName, Value = n.SiteCode });
            }
            return sites;
        }
    }
}