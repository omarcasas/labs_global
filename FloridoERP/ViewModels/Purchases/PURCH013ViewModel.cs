﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH013ViewModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }

        //public virtual List<ItemDropDownModel> Items { get; set; }
        //public IEnumerable<SelectListItem> ItemsList
        //{
        //    get
        //    {
        //        return FillSelectListItems();
        //    }
        //}
        //public List<SelectListItem> FillSelectListItems()
        //{
        //    List<SelectListItem> products = new List<SelectListItem>();
        //    products.Add(new SelectListItem { Text = "Buscar un Producto.", Value = "" });
        //    foreach (var n in Items)
        //    {
        //        products.Add(new SelectListItem { Text = n.part_description, Value = n.part_number });
        //    }
        //    return products;
        //}
        //public virtual List<ItemDropDownModel> ItemsPresentation { get; set; }
        //public IEnumerable<SelectListItem> ItemsPresentationList
        //{
        //    get
        //    {
        //        return FillSelectListItemsPresentation();
        //    }
        //}
        //public List<SelectListItem> FillSelectListItemsPresentation()
        //{
        //    List<SelectListItem> products = new List<SelectListItem>();
        //    products.Add(new SelectListItem { Text = "Buscar un Producto.", Value = "" });
        //    foreach (var n in Items)
        //    {
        //        products.Add(new SelectListItem { Text = n.part_description, Value = n.part_number });
        //    }
        //    return products;
        //}

        public virtual List<MA_CODE> Ofertas { get; set; }
        public IEnumerable<SelectListItem> OfertList
        {
            get
            {
                return FillSelectListOferts();
            }
        }
        public List<SelectListItem> FillSelectListOferts()
        {
            List<SelectListItem> ofertass = new List<SelectListItem>();
            ofertass.Add(new SelectListItem { Text = "Seleccione un Tipo de Oferta", Value = "" });
            foreach (var n in Ofertas)
            {
                ofertass.Add(new SelectListItem { Text = n.description, Value = n.vkey });
            }

            return ofertass;
        }

        public virtual List<SITES> Site { get; set; }
        public IEnumerable<SelectListItem> SiteList
        {
            get
            {
                return FillSelectListSite();
            }
        }
        public List<SelectListItem> FillSelectListSite()
        {
            List<SelectListItem> sites = new List<SelectListItem>();
            sites.Add(new SelectListItem { Text = "Todas las Tiendas", Value = "All" });
            foreach (var n in Site)
            {
                sites.Add(new SelectListItem { Text = n.site_name, Value = n.site_code });
            }
            return sites;
        }

        public ItemDropDownModel ITEM
        {
            get
            {
                return new ItemDropDownModel
                {
                    part_number = part_number,
                    part_description = part_description
                };
            }
        }
    }
}