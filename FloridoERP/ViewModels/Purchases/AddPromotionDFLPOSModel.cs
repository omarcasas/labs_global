﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Purchases
{
    public class AddPromotionDFLPOSModel
    {
      
        public string promotion_name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public bool status { get; set; }
        public int promotions_by_ticket { get; set; }
        public string ticket_header { get; set; }
        public string ticket_footer { get; set; }
        public decimal sale_min { get; set; }
        public int promotion_value { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public decimal item_amount { get; set; }
        public int item_quantity { get; set; }






        public DFLPOS_SALES_PROMOTIONS promotionH
        {
            get
            {
                return new DFLPOS_SALES_PROMOTIONS
                {
                    promotion_name = promotion_name,
                    start_date = start_date,
                    end_date = end_date,
                    status = status,
                    promotions_by_ticket= promotions_by_ticket,
                    ticket_header= ticket_header,
                    ticket_footer= ticket_footer,
                    sale_min= sale_min,
                    promotion_value= promotion_value,
                    cuser = cuser,
                    cdate = cdate,
                    program_id = ""
                };
            }
        }
    }
}