﻿using App.BLL;
using App.BLL.CommonBusiness;
using App.BLL.Purchases;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH008ViewModel
    {
       

        public PURCH008ViewModel()
        {

            var SupplierListObj = new SupplierBusiness();
            SupplierList = SupplierListObj.GetAllSuppliers();

            var BuyersListObj = new UserMasterBusiness();
            BuyersList = BuyersListObj.GetAllUsers();

            var DiscountListObj = new SysMaCodeBusiness();
            DiscountList = DiscountListObj.GetAllNegotiationDiscount();

            var PriceDiscListObj = new SysMaCodeBusiness();
            PriceNegotiList = PriceDiscListObj.GetAllNegotiationPrice();

            var PointListObj = new SysMaCodeBusiness();
            PointDiscList = PointListObj.GetAllNegotiationPoint();

            var PenaltiesListObj = new SysMaCodeBusiness();
            PenaltiesList = PenaltiesListObj.GetAllNegotiationPenalties();

        }
        public List<MA_SUPPLIER> SupplierList { get; set; }
        public List<USER_MASTER> BuyersList { get; set; } 
        public List<MA_CODE> DiscountList { get; set; }
        public List<MA_CODE> PriceNegotiList { get; set; }
        public List<MA_CODE> PointDiscList { get; set; }
        public List<MA_CODE> PenaltiesList { get; set; }
        public int nm_id { get; set; }
        public int supplier_id { get; set; }
        public string user_id { get; set; }
        public int total_negotiations { get; set; }
        public int total_active_negotiations { get; set; }
        public int total_inactive_negotiations { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program { get; set; }
        public int nd_id { get; set; }
        public string status { get; set; }
        public System.DateTime valid_until { get; set; }
        public string negotiation_type { get; set; }
        public string payment_conditions { get; set; }
        public string delivery_points { get; set; }
        public string supplier_references { get; set; }
        public string general_notes { get; set; }
        public string negotiatied_discounts { get; set; }
        public string conditions_prices { get; set; }
        public string penalties { get; set; }
        public string quality_level { get; set; }
        public string packaging_type { get; set; }
        public string filebase64 { get; set; }
        public string file_name_ext { get; set; }
        public Nullable<int> renovated_from { get; set; }

        //Syupplier
        public string rfc { get; set; }
        public string commercial_name { get; set; }
        public string business_name { get; set; }
        public string supplier_address { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string supplier_state { get; set; }
        public string country { get; set; }
        public string supplier_type { get; set; }
        public string accounting_account { get; set; }
        public string code { get; set; }
        public string vkey { get; set; }
        public Nullable<int> vkey_seq { get; set; }
        public string description { get; set; }
        public string used { get; set; }

        public MA_SUPPLIER MA_SUPPLIER { get { return new MA_SUPPLIER { supplier_id = supplier_id, rfc = rfc, commercial_name = commercial_name, business_name = business_name, supplier_address = supplier_address, city = city, zip_code = zip_code, supplier_state = supplier_state, country = country, supplier_type = supplier_type, accounting_account = accounting_account, cuser = cuser, cdate = cdate, uuser = uuser, udate = udate, program = program }; } }

        public NEGOTIATION_MASTER NegotiationMaster { get { return new NEGOTIATION_MASTER {supplier_id = supplier_id, user_id = user_id, total_negotiations = total_negotiations, total_active_negotiations = total_active_negotiations, total_inactive_negotiations = total_inactive_negotiations, cdate = cdate, cuser = cuser, udate = udate, uuser = uuser, program= program }; } }

        public NEGOTIATION_DETAIL NegotiationDetail { get { return new NEGOTIATION_DETAIL { nd_id = nd_id, nm_id = nm_id, status = status, valid_until = valid_until, negotiation_type = negotiation_type, payment_conditions = payment_conditions, delivery_points = delivery_points, supplier_references = supplier_references, general_notes = general_notes, negotiatied_discounts = negotiatied_discounts, conditions_prices = conditions_prices, penalties = penalties, quality_level = quality_level, packaging_type = packaging_type, filebase64 = filebase64,file_name_ext= file_name_ext, renovated_from = renovated_from,cdate = cdate, cuser = cuser, udate = udate, uuser = uuser, program = program }; } }

        public MA_CODE NegotiationMaCodeAddCatalogue { get { return new MA_CODE { code = code, vkey = vkey, vkey_seq = vkey_seq, description = description, used = used, cdate = cdate, cuser = cuser, udate = udate, uuser = uuser, program_id = program }; } }

        public IEnumerable<SelectListItem> SupplierListItems
        {
            get
            {
                return FillSelectListItemSupplier();
            }
        }
        public List<SelectListItem> FillSelectListItemSupplier()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in SupplierList)
            {
                items.Add(new SelectListItem { Text = n.business_name, Value = n.supplier_id.ToString() });
            }

            return items;
        }
        public IEnumerable<SelectListItem> BuyersListItems
        {
            get
            {
                return FillSelectListBuyers();
            }
        }
        public List<SelectListItem> FillSelectListBuyers()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in BuyersList)
            {
                items.Add(new SelectListItem { Text = n.first_name +" "+ n.last_name, Value = n.emp_no.ToString() });
            }

            return items;
        }
        //--------------------------Lista Descuentos
        public IEnumerable<SelectListItem> DiscountListItems
        {
            get
            {
                return FillSelectListDiscount();
            }
        }
        public List<SelectListItem> FillSelectListDiscount()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in DiscountList)
            {
                items.Add(new SelectListItem { Text = n.vkey +" ("+ n.description+")", Value = n.vkey.ToString() });
            }

            return items;
        }
        //--------------------------Lista Descuentos
        //--------------------------Lista Condiciones de precio
        public IEnumerable<SelectListItem> PriceDiscListItems
        {
            get
            {
                return FillSelectListPriceDisc();
            }
        }
        public List<SelectListItem> FillSelectListPriceDisc()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in PriceNegotiList)
            {
                items.Add(new SelectListItem { Text = n.vkey + " (" + n.description + ")", Value = n.vkey.ToString() });
            }

            return items;
        }
        //--------------------------Lista Condiciones de precio
        //--------------------------Lista Negociacion Puntos de entrega
        public IEnumerable<SelectListItem> PointListItems
        {
            get
            {
                return FillSelectListPoint();
            }
        }
        public List<SelectListItem> FillSelectListPoint()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in PointDiscList)
            {
                items.Add(new SelectListItem { Text = n.vkey + " (" + n.description + ")", Value = n.vkey.ToString() });
            }

            return items;
        }
        //--------------------------Lista Negociacion Puntos de entrega
        //--------------------------Lista Negociacion sanciones
        public IEnumerable<SelectListItem> PenaltiesListItems
        {
            get
            {
                return FillSelectListPenalties();
            }
        }
        public List<SelectListItem> FillSelectListPenalties()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in PenaltiesList)
            {
                items.Add(new SelectListItem { Text = n.vkey + " (" + n.description + ")", Value = n.vkey.ToString() });
            }

            return items;
        }
        //--------------------------Lista Negociacion sanciones
    }

}