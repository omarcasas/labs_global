﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Purchases
{
    public class AddPromotionDetailDFLPOSModel
    {

        private DFLPOS_SALES_PROMOTIONS_DETAIL _promotionDetail;
        private DFLPOS_SALES_PROMOTIONS_SITES _promotionSite;

        public AddPromotionDetailDFLPOSModel()
        {
            _promotionDetail = new DFLPOS_SALES_PROMOTIONS_DETAIL();
            _promotionSite = new DFLPOS_SALES_PROMOTIONS_SITES();
        }

        public long promotion_id { get; set; }
        public string part_number { get; set; }
        public decimal item_amount { get; set; }
        public int item_quantity { get; set; }

        public DFLPOS_SALES_PROMOTIONS_DETAIL PromotionDetail
        {
            get
            {
                return new DFLPOS_SALES_PROMOTIONS_DETAIL
                {
                    promotion_id = promotion_id,
                    part_number = part_number,
                    item_amount = item_amount,
                    item_quantity = item_quantity
                };
            }
        }

        public DFLPOS_SALES_PROMOTIONS_SITES PromotionSites
        {
            get
            {
                return new DFLPOS_SALES_PROMOTIONS_SITES
                {
                    promotion_id = promotion_id,
                    site_code = "",
                    cuser = "",
                    cdate = null
                };
            }
        }

    }
}