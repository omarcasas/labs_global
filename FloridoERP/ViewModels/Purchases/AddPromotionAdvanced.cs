﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Purchases
{
    public class AddPromotionAdvanced
    {
        public int pos { get; set; }
        public string part_number { get; set; }
        public decimal price { get; set; }
        public int quantity { get; set; }
        public int quantity_total { get; set; }
        public int common_id { get; set; }
    }
}