﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH010ViewModel
    {
        public List<SupplierList> Supplier;
        public List<ProductList> Product;
        public List<CategoryList> Category;
        public List<CategoryList> Category2;
        public PURCH010ViewModel()
        {
            Supplier = new List<SupplierList>();
            Product = new List<ProductList>();
            Category = new List<CategoryList>();
            Category2 = new List<CategoryList>();
        }
        public void AddSupplierViewModel(List<MA_SUPPLIER> Suppliers)
        {
            Supplier = Suppliers.Select(x => new SupplierList { commercial_name = x.commercial_name, supplier_id = x.supplier_id }).ToList();
        }
        public void AddProductViewModel(List<ITEM> Products)
        {
            Product = Products.Select(x => new ProductList { part_description = x.part_description, part_number = x.part_number }).ToList();
        }
        public void AddCategoryViewModel(List<MA_CLASS> Categotys)
        {
            Category = Categotys.Where(x => x.level_category == 1).Select(x => new CategoryList { class_name = x.class_name, class_id= x.class_id}).ToList();
            Category2 = Categotys.Where(x => x.level_category == 2 ).Select(x => new CategoryList { class_name = x.class_name, class_id = x.class_id }).ToList();
        }
        public List<SelectListItem> FillSelectListSupplier()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Supplier)
            {
                items.Add(new SelectListItem { Text = n.commercial_name, Value = n.supplier_id.ToString() });
            }

            return items;
        }
        public List<SelectListItem> FillSelectListProduct()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Product)
            {
                items.Add(new SelectListItem { Text = n.part_description, Value = n.part_number });
            }

            return items;
        }
        public List<SelectListItem> FillSelectListCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Category)
            {
                items.Add(new SelectListItem { Text = n.class_name, Value = n.class_id.ToString() });
            }

            return items;
        }
        public List<SelectListItem> FillSelectListCategory2()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Category2)
            {
                items.Add(new SelectListItem { Text = n.class_name, Value = n.class_id.ToString() });
            }

            return items;
        }
    }
    public class SupplierList
    {
        public int supplier_id { get; set; }
        public string commercial_name { get; set; }
    }
    public class ProductList
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
    }
    public class CategoryList
    {
        public int class_id { get; set; }
        public string class_name { get; set; }
    }
}