﻿using App.BLL;
using App.BLL.Item;
using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERP.ViewModels.Purchases
{
    public class PURCH016ViewModel
    {
        public PURCH016ViewModel()
        {
            var combosBus = new ItemCombosBusiness();
            CombosList = combosBus.getCombosByStatus("1");

            var itemsBusi = new ItemMasterBusiness();
            //itemList = itemsBusi.GetAllItemsActives();
            itemList = itemsBusi.GetAllItemsActivesN();

        }
        public string part_number { get; set; }
        public string part_number_combo { get; set; }
        public string part_description_combo { get; set; }
        public string combo_status { get; set; }
        public decimal quantity { get; set; }
        public decimal iva { get; set; }
        public string ivaDescription { get; set; }
        public bool active_status { get; set; }
        public decimal total_price_sale { get; set; }
        public List<ItemComboModel> CombosList { get; set; }
        public List<ItemMasterModel> itemList { get; set; }

        public IEnumerable<SelectListItem> ItemsCombosList
        {
            get
            {
                return FillSelectListItemscombo();
            }
        }
        public List<SelectListItem> FillSelectListItemscombo()
        {
            List<SelectListItem> Combos = new List<SelectListItem>();
            Combos.Add(new SelectListItem { Text = "Seleccione una opción.", Value = "" });
            foreach (var n in CombosList)
            {
                Combos.Add(new SelectListItem { Text = n.part_description_combo, Value = n.part_number_combo });
            }
            return Combos;
        }
        public IEnumerable<SelectListItem> ItemsLists
        {
            get
            {
                return FillSelectListItems();
            }
        }
        public List<SelectListItem> FillSelectListItems()
        {
            List<SelectListItem> Products = new List<SelectListItem>();
            Products.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in itemList)
            {
                Products.Add(new SelectListItem { Text = n.part_description, Value = n.part_number });
            }
            return Products;
        }
    }
}