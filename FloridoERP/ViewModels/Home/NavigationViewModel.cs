﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FloridoERP.ViewModels.Home
{
    public class NavigationViewModel
    {
        public DataSet menu_user_dataset { get; set; }
        public string user_name { get; set; }
        public byte[] photo { get; set; }
    }
}