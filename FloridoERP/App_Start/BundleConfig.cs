﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace FloridoERP
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;

            // Homer style
            bundles.Add(new StyleBundle("~/bundles/homer/css")
                .Include("~/Content/style.css", new CssRewriteUrlTransform())
                .Include("~/styles/simple_check.css"));

            // Animate.css
            bundles.Add(new StyleBundle("~/bundles/animate/css").Include(
                      "~/Vendor/animate.css/animate.min.css",
                      "~/vendor/toastr/build/toastr.min.css",
                       "~/fonts/pe-icon-7-stroke/css/helper.css",
                       "~/styles/static_custom.css",
                      "~/Vendor/sweetalert/lib/sweet-alert.css"));

            // fullcalendar style
            bundles.Add(new StyleBundle("~/bundles/fullcalendar/css").Include("~/Vendor/fullcalendar/dist/fullcalendar.min.css", new CssRewriteUrlTransform()));

            // fullcalendar style
            bundles.Add(new StyleBundle("~/bundles/fullcalendar-media/css").Include("~/vendor/fullcalendar/dist/fullcalendar.print.css", new CssRewriteUrlTransform()));

            // metisMenu style
            bundles.Add(new StyleBundle("~/bundles/metisMenu/css").Include("~/Vendor/metisMenu/dist/metisMenu.css", new CssRewriteUrlTransform()));

            // style
            bundles.Add(new StyleBundle("~/bundles/style/css").Include("~/styles/style.css", new CssRewriteUrlTransform()));

            // bootstrap-tour style
            bundles.Add(new StyleBundle("~/bundles/bootstrap-tour/css").Include("~/Vendor/bootstrap-tour/build/css/bootstrap-tour.min.css", new CssRewriteUrlTransform()));

            // Datepicker style
            bundles.Add(new StyleBundle("~/bundles/datepicker/css").Include("~/Vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css", new CssRewriteUrlTransform()));

            // Pe-icon-7-stroke
            bundles.Add(new StyleBundle("~/bundles/peicon7stroke/css").Include("~/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css", new CssRewriteUrlTransform()));

            // Font Awesome icons style
            bundles.Add(new StyleBundle("~/bundles/font-awesome/css").Include("~/Vendor/fontawesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            // Bootstrap style
            bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include("~/Vendor/bootstrap/dist/css/bootstrap.min.css", new CssRewriteUrlTransform()));

            // Datatables style
            bundles.Add(new StyleBundle("~/bundles/datatables/css").Include("~/Vendor/datatables.net-bs/css/dataTables.bootstrap.min.css"));

            // table editable style
            bundles.Add(new StyleBundle("~/bundles/edit/css").Include("~/vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css"));

            // touchspin style
            bundles.Add(new StyleBundle("~/bundles/touchspin/css").Include("~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css"));

            // pagination style
            bundles.Add(new StyleBundle("~/bundles/pagination/css").Include("~/styles/pagination.css"));

            // cards style
            bundles.Add(new StyleBundle("~/bundles/cards/css").Include("~/styles/Cards.css"));

            // Select 2 style
            var bundle = new Bundle("~/bundles/select2/css")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/select2-3.5.2/select2.css", new CssRewriteUrlTransform())
                .Include("~/Vendor/select2-bootstrap/select2-bootstrap.css", new CssRewriteUrlTransform());
            bundles.Add(bundle);

            // simple_toggle
            bundles.Add(new StyleBundle("~/bundles/simple_toggle/css").Include("~/styles/simple_toggle.css"));

            // Homer script
            bundles.Add(new ScriptBundle("~/bundles/homer/js").Include(
                      "~/Vendor/metisMenu/dist/metisMenu.min.js",
                      "~/Vendor/iCheck/icheck.min.js",
                      "~/Vendor/peity/jquery.peity.min.js",
                      "~/Vendor/sparkline/index.js",
                      "~/Scripts/homer.js",
                      "~/Scripts/charts.js",
                      "~/vendor/toastr/build/toastr.min.js",
                      "~/Vendor/sweetalert/lib/sweet-alert.js"));

            // Bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include("~/Vendor/bootstrap/dist/js/bootstrap.min.js"));

            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery/js").Include("~/Vendor/jquery/dist/jquery.min.js"));

            // jQuery UI
            bundles.Add(new ScriptBundle("~/bundles/jqueryui/js").Include("~/Vendor/jquery-ui/jquery-ui.min.js"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/validation/js").Include("~/vendor/jquery-validation/jquery.validate.min.js"));

            // Datatables
            bundles.Add(new ScriptBundle("~/bundles/datatables/js").Include("~/Scripts/Datatable.js"));

            // Datatables bootstrap
            bundles.Add(new ScriptBundle("~/bundles/datatablesBootstrap/js").Include("~/Vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"));

            // Datatables plugins
            bundles.Add(new ScriptBundle("~/bundles/datatablesPlugins/js").Include(
                      "~/Vendor/pdfmake/build/pdfmake.min.js",
                      "~/Vendor/pdfmake/build/vfs_fonts.js",
                      "~/Vendor/datatables.net-buttons/js/buttons.html5.min.js",
                      "~/Vendor/datatables.net-buttons/js/buttons.print.min.js",
                      "~/Vendor/datatables.net-buttons/js/dataTables.buttons.min.js",
                      "~/Vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"));

            // Datatable Sum
            bundles.Add(new ScriptBundle("~/bundles/sum/js").Include("~/Vendor/datatables_plugins/api/sum().js"));

            //table editable js
            bundles.Add(new ScriptBundle("~/bundles/edit/js").Include(
                "~/vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.js",
                "~/vendor/moment/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker/js").Include(
                "~/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js",
                "~/vendor/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.es.min.js"));

            // axios js
            bundles.Add(new ScriptBundle("~/bundles/axios/js").Include("~/Scripts/axios.min.js"));

            // fullcalendar
            bundles.Add(new ScriptBundle("~/bundles/fullcalendar/js").Include(
                "~/Vendor/fullcalendar/dist/fullcalendar.min.js",
                "~/Vendor/fullcalendar/dist/locale/es-us.js"));

            // bootstrap-tour
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-tour/js").Include("~/Vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"));

            // Scripts
            bundle = new Bundle("~/bundles/select2/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/select2-3.5.2/select2.min.js")
                .Include("~/Vendor/select2-3.5.2/select2_locale_es.js");
            bundles.Add(bundle);

            // Knockout scripts
            bundles.Add(new ScriptBundle("~/bundles/MRO002/js").Include(
                "~/Scripts/knockout-min.js",
                "~/Scripts/MRO/MRO002.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/knockout-min.js"));

            //request
            bundles.Add(new ScriptBundle("~/bundles/request").Include("~/Scripts/request/*.js"));

            // Datepicker scripts
            bundle = new Bundle("~/bundles/datepicker/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js")
                .Include("~/Vendor/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.es.min.js")
                .Include("~/Vendor/moment/moment.js")
                .Include("~/Vendor/clockpicker/dist/bootstrap-clockpicker.min.js");
            bundles.Add(bundle);

            // slimscroll
            bundles.Add(new ScriptBundle("~/bundles/slimscroll/js").Include("~/Vendor/slimScroll/jquery.slimscroll.js"));

            // touchspin
            bundles.Add(new ScriptBundle("~/bundles/touchspin/js").Include("~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"));

            // pagination
            bundles.Add(new ScriptBundle("~/bundles/pagination/js").Include("~/Scripts/pagination.js"));

            // PURCH018 scripts
            bundles.Add(new ScriptBundle("~/bundles/PURCH018/js").Include(
                "~/Scripts/Purchases/PURCH018/PURCH018_Category.js",
                "~/Scripts/Purchases/PURCH018/PURCH018_ITEM_CREDITORS.js",
                "~/Scripts/Purchases/PURCH018/PURCH018_Creditors.js",
                "~/Scripts/Purchases/PURCH018/PURCH018.js"));

            // scripts
            bundles.Add(new ScriptBundle("~/bundles/Layout/js").Include("~/Scripts/Shared/Layout.js"));
            bundles.Add(new ScriptBundle("~/bundles/Register/js").Include("~/Scripts/Home/Register.js"));
            bundles.Add(new ScriptBundle("~/bundles/RecoveryPassword/js").Include("~/Scripts/Home/RecoveryPassword.js"));
            bundles.Add(new ScriptBundle("~/bundles/Unlocking/js").Include("~/Scripts/Home/Unlocking.js"));
            bundles.Add(new ScriptBundle("~/bundles/Login/js").Include("~/Scripts/Home/Login.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS001/js").Include("~/Scripts/Configuration/ADMS001.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS002/js").Include("~/Scripts/Configuration/ADMS002.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS003/js").Include("~/Scripts/Configuration/ADMS003.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS004/js").Include("~/Scripts/Configuration/ADMS004.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS006/js").Include("~/Scripts/Configuration/ADMS006.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH021/js").Include("~/Scripts/expenses/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/INVE001/js").Include("~/Scripts/Inventory/INVE001.js"));
            bundles.Add(new ScriptBundle("~/bundles/INVE002/js").Include("~/Scripts/Inventory/INVE002.js"));
            bundles.Add(new ScriptBundle("~/bundles/INV001/js").Include("~/Scripts/Invoices/INV001.js"));
            bundles.Add(new ScriptBundle("~/bundles/MRO001/js").Include("~/Scripts/MRO/MRO001.js"));
            bundles.Add(new ScriptBundle("~/bundles/MRO003/js").Include("~/Scripts/MRO/MRO003.js"));
            bundles.Add(new ScriptBundle("~/bundles/PROJ001/js").Include("~/Scripts/Projects/PROJ001.js"));
            bundles.Add(new ScriptBundle("~/bundles/PROJ002/js").Include("~/Scripts/Projects/PROJ002.js"));
            bundles.Add(new ScriptBundle("~/bundles/PROJ003/js").Include("~/Scripts/Projects/PROJ003.js"));
            bundles.Add(new ScriptBundle("~/bundles/PROJ004/js").Include("~/Scripts/Projects/PROJ004/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/PROJ005/js").Include("~/Scripts/Projects/PROJ005.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH002/js").Include("~/Scripts/Purchases/PURCH002.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH003/js").Include("~/Scripts/Purchases/PURCH003.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH004/js").Include("~/Scripts/Purchases/PURCH004.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH005/js").Include("~/Scripts/Purchases/PURCH005.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH008/js").Include("~/Scripts/Purchases/PURCH008.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH009/js").Include("~/Scripts/Purchases/PURCH009.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH011/js").Include("~/Scripts/Purchases/PURCH011.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH012/js").Include("~/Scripts/Purchases/PURCH012.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH013AND023/js").Include("~/Scripts/Purchases/PURCH013AND023.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH015/js").Include("~/Scripts/Purchases/PURCH015.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH016/js").Include("~/Scripts/Purchases/PURCH016.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH017/js").Include("~/Scripts/Purchases/PURCH017.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH019/js").Include("~/Scripts/Purchases/PURCH019.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH020/js").Include("~/Scripts/Purchases/PURCH020.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH022/js").Include("~/Scripts/Purchases/PURCH022.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH023/js").Include("~/Scripts/Purchases/PURCH023.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH025/js").Include("~/Scripts/Purchases/PURCH025.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH026/js").Include("~/Scripts/Purchases/PURCH026.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH027/js").Include("~/Scripts/Purchases/PURCH027.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH028/js").Include("~/Scripts/Purchases/PURCH028.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH029/js").Include("~/Scripts/Purchases/PURCH029.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH030/js").Include("~/Scripts/Purchases/PURCH030.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH032/js").Include("~/Scripts/Purchases/PURCH032.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH033/js").Include("~/Scripts/Purchases/PURCH033.js"));
        }
    }

    public class BundlesFormats
    {
        public const string Print = @"<link href=""{0}"" rel=""stylesheet"" type=""text/css"" media=""print"" />";
    }

    public class AsIsBundleOrderer : IBundleOrderer
    {
        public virtual IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}