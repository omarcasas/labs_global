﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.ServicesCalendar;
using System.Collections.Generic;

namespace FloridoERP.Report
{
    public partial class ServiceCalendarToSupplierReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableServices = null;

        public ServiceCalendarToSupplierReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(string Site, string User, string Fecha, List<ServicesCalendarDetailModel> servicesDetails)
        {
            try
            {
                DataSetTableServices = new DataSet();

                DataTable DataTableServices = new DataTable();

                DataSetTableServices.Tables.Add(DataTableServices);

                xrLabelDate.Text = Fecha;
                xrLabelSite.Text = Site;
                xrLabelUser.Text = User;

                DataTableServices.Columns.Add(new DataColumn("xrTableCellSerialNumber"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellDescription"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellCost"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellDepto"));

                foreach (var item in servicesDetails)
                {
                    DataRow DataRowOrdered = DataSetTableServices.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellSerialNumber"] = item.equipment_serial;
                    DataRowOrdered["xrTableCellDescription"] = item.equipment_description;
                    DataRowOrdered["xrTableCellCost"] = "$ " + item.service_cost;
                    DataRowOrdered["xrTableCellDepto"] = item.service_department;
                    DataSetTableServices.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellSerialNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSerialNumber"));
                xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDescription"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCost"));
                xrTableCellDepto.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDepto"));

                return DataSetTableServices;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
