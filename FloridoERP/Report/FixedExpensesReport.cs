﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.BLL.Purchases;
using System.Data;
using App.Entities.ViewModels.Expenses;
using System.Collections.Generic;

namespace FloridoERP.Report
{
    public partial class FixedExpensesReport : DevExpress.XtraReports.UI.XtraReport
    {
        private SupplierBusiness _supplierBusiness;
        public DataSet DataSetTableExpense = null;
        public decimal ivaValueMxn, iepsValueMxn, ImportValueMxn, subTotalMxn, isrRMxn, ivaRMxn, discountMxn, ivaValueUsd, iepsValueUsd, ImportValueUsd, subTotalUsd, isrRUsd, ivaRUsd, discountUsd = 0;
        public FixedExpensesReport()
        {
            InitializeComponent();
            _supplierBusiness = new SupplierBusiness();
        }
        public DataSet printTable(ItemXMLDetail header, List<ItemXMLDetail> detail/*, SITES site_info*/)
        {
            try
            {
                var supplierInfo = _supplierBusiness.GetSupplierInfoById(Convert.ToInt32(header.supplier_id));

                //Gastos
                empLabel.Text = header.cuser.ToUpper();
                xrlblFolioInternoShow.Text = header.id.ToString();
                xrlblFolioGeneral.Text = header.invoice.ToUpper();
                xrlblDateInShow.Text = header.cdate.HasValue ? header.cdate.Value.ToString("dd/MM/yyyy") : "---";
                xrlblCurrencyShow.Text = header.currency;
                xrlblTypeShow.Text = header.expense_type;
                xrlblExpenseExactlyShow.Text = header.expense_type_exactly;
                //Tienda
                //xraddress.Text = site_info.address + " C.P. " + site_info.postal_code;
                //xrState.Text = site_info.city + ", " + site_info.state;
                //siteLabel.Text = "*" + site_info.site_name;
                //Proveedor
                supplierRFCShow.Text = supplierInfo.Rfc.ToUpper();
                supplierNameShow.Text = supplierInfo.BusinessName.ToUpper() ?? "";
                xrlblCurrencyShow.Text = header.currency;
                xrlblInvoiceDateShow.Text = header.invoice_date.HasValue ? header.invoice_date.Value.ToString("dd/MM/yyyy") : "---";
                supAddrsLabel.Text = supplierInfo.SupplierAddress + ", CP:" + supplierInfo.ZipCode;
                //Contacto
                if (supplierInfo.supplier_contact_id == 0)
                {
                    supTelLabel.Text = "Sin Contacto";
                    supEmailLabel.Text = "Sin Contacto";
                    supDeparLabel.Text = "Sin Contacto";
                }
                else
                {
                    supTelLabel.Text = supplierInfo.phone.ToUpper();
                    supEmailLabel.Text = supplierInfo.email.ToUpper();
                    supDeparLabel.Text = supplierInfo.departament.ToUpper();
                }

                DataSetTableExpense = new DataSet();
                DataTable table = new DataTable();
                DataSetTableExpense.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrCellPartNumber"));
                table.Columns.Add(new DataColumn("xrCellDescription"));
                table.Columns.Add(new DataColumn("xrCellQuantity"));
                table.Columns.Add(new DataColumn("xrCellCostUnit"));
                table.Columns.Add(new DataColumn("xrCellSubtotal"));
                table.Columns.Add(new DataColumn("xrCellIVA"));
                table.Columns.Add(new DataColumn("xrCellIEPS"));
                table.Columns.Add(new DataColumn("xrCellDiscount"));
                table.Columns.Add(new DataColumn("xrCellISRR"));
                table.Columns.Add(new DataColumn("xrCellIVAR"));
                table.Columns.Add(new DataColumn("xrCellImport"));
                foreach (var item in detail)
                {
                    DataRow DataRowOrdered = DataSetTableExpense.Tables[0].NewRow();
                    DataRowOrdered["xrCellPartNumber"] = item.item_no;
                    DataRowOrdered["xrCellDescription"] = item.item_description;
                    DataRowOrdered["xrCellQuantity"] = item.quantity;
                    DataRowOrdered["xrCellCostUnit"] = item.unit_cost.ToString("C4");
                    DataRowOrdered["xrCellDiscount"] = item.discount.ToString("C4");
                    DataRowOrdered["xrCellSubtotal"] = item.item_amount.ToString("C4");
                    DataRowOrdered["xrCellIVA"] = item.iva.ToString("C4");
                    DataRowOrdered["xrCellIEPS"] = item.ieps.ToString("C4");
                    DataRowOrdered["xrCellISRR"] = item.isr_retained.ToString("C4");
                    DataRowOrdered["xrCellIVAR"] = item.iva_retained.ToString("C4");
                    DataRowOrdered["xrCellImport"] = item.total_amount.ToString("C4");
                    DataSetTableExpense.Tables[0].Rows.Add(DataRowOrdered);
                    if (header.currency == "MXN")
                    {
                        ivaValueMxn += item.iva;
                        iepsValueMxn += item.ieps;
                        subTotalMxn += item.item_amount;
                        discountMxn += item.discount;
                        isrRMxn += item.isr_retained;
                        ivaRMxn += item.iva_retained;
                        ImportValueMxn += item.total_amount;
                    }
                    else
                    {
                        ivaValueUsd += item.iva;
                        iepsValueUsd += item.ieps;
                        subTotalUsd += item.item_amount;
                        discountUsd += item.discount;
                        isrRUsd += item.isr_retained;
                        ivaRUsd += item.iva_retained;
                        ImportValueUsd += item.total_amount;
                    }

                }
                xrlblMxnSubtotal.Text = subTotalMxn.ToString("C4");
                xrlblMxnIVA.Text = ivaValueMxn.ToString("C4");
                xrlblMxnIEPS.Text = iepsValueMxn.ToString("C4");
                xrlblMxnDiscount.Text = discountMxn.ToString("C4");
                xrlblMxnISRR.Text = isrRMxn.ToString("C4");
                xrlblMxnIVAR.Text = ivaRMxn.ToString("C4");
                xrlblMxnImport.Text = ImportValueMxn.ToString("C4");

                xrlblUsdSubtotal.Text = subTotalUsd.ToString("C4");
                xrlblUsdIVA.Text = ivaValueUsd.ToString("C4");
                xrlblUsdIEPS.Text = iepsValueUsd.ToString("C4");
                xrlblUsdDiscount.Text = discountUsd.ToString("C4");
                xrlblUsdISRR.Text = isrRUsd.ToString("C4");
                xrlblUsdIVAR.Text = ivaRUsd.ToString("C4");
                xrlblUsdImport.Text = ImportValueUsd.ToString("C4");

                xrCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellPartNumber"));
                xrCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDescription"));
                xrCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellQuantity"));
                xrCellCostUnit.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellCostUnit"));
                xrCellSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellSubtotal"));
                xrCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVA"));
                xrCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIEPS"));
                xrCellDiscount.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDiscount"));
                xrCellISRR.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellISRR"));
                xrCellIVAR.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVAR"));
                xrCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellImport"));
                return DataSetTableExpense;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

    }
}
