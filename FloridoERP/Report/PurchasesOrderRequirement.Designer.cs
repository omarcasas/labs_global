﻿namespace FloridoERP.Report
{
    partial class PurchasesOrderRequirement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchasesOrderRequirement));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellPartNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellBaseCost = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellSubtotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCreditorNombre = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCreditorName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSubTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelstock = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelplanograma = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabeldescrip = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCodigo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrExecuteDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDistribuidora = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCity = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCorreo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrAddress = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLState = new DevExpress.XtraReports.UI.XRLabel();
            this.xrState = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLStore = new DevExpress.XtraReports.UI.XRLabel();
            this.xrEmail = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLAddress = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCity = new DevExpress.XtraReports.UI.XRLabel();
            this.xrStore = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrRequirement = new DevExpress.XtraReports.UI.XRLabel();
            this.xrExecute = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLExecuteDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCreditorPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCreditorEmail = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCreditorPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCreditorEmail = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCreditorBusinessName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLContacts = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLCreditorBusinessName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLRequirement = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLExecute = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCellImport = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.xrIVASupplier = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 32.39356F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(794F, 32.29167F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellPartNumber,
            this.xrTableCellDescription,
            this.xrTableCellQuantity,
            this.xrTableCellBaseCost,
            this.xrTableCellImport,
            this.xrTableCellIVA,
            this.xrTableCellSubtotal});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCellPartNumber
            // 
            this.xrTableCellPartNumber.Dpi = 100F;
            this.xrTableCellPartNumber.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellPartNumber.Name = "xrTableCellPartNumber";
            this.xrTableCellPartNumber.StylePriority.UseFont = false;
            this.xrTableCellPartNumber.StylePriority.UseTextAlignment = false;
            this.xrTableCellPartNumber.Text = "xrTableCellPartNumber";
            this.xrTableCellPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellPartNumber.Weight = 0.97497728455972188D;
            // 
            // xrTableCellDescription
            // 
            this.xrTableCellDescription.Dpi = 100F;
            this.xrTableCellDescription.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellDescription.Name = "xrTableCellDescription";
            this.xrTableCellDescription.StylePriority.UseFont = false;
            this.xrTableCellDescription.StylePriority.UseTextAlignment = false;
            this.xrTableCellDescription.Text = "xrTableCellDescription";
            this.xrTableCellDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellDescription.Weight = 2.3265170622354172D;
            // 
            // xrTableCellQuantity
            // 
            this.xrTableCellQuantity.Dpi = 100F;
            this.xrTableCellQuantity.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellQuantity.Name = "xrTableCellQuantity";
            this.xrTableCellQuantity.StylePriority.UseFont = false;
            this.xrTableCellQuantity.StylePriority.UseTextAlignment = false;
            this.xrTableCellQuantity.Text = "xrTableCellQuantity";
            this.xrTableCellQuantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellQuantity.Weight = 0.52348114246688959D;
            // 
            // xrTableCellBaseCost
            // 
            this.xrTableCellBaseCost.Dpi = 100F;
            this.xrTableCellBaseCost.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellBaseCost.Name = "xrTableCellBaseCost";
            this.xrTableCellBaseCost.StylePriority.UseFont = false;
            this.xrTableCellBaseCost.StylePriority.UseTextAlignment = false;
            this.xrTableCellBaseCost.Text = "xrTableCellBaseCost";
            this.xrTableCellBaseCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellBaseCost.Weight = 0.58497653279526851D;
            // 
            // xrTableCellIVA
            // 
            this.xrTableCellIVA.Dpi = 100F;
            this.xrTableCellIVA.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellIVA.Name = "xrTableCellIVA";
            this.xrTableCellIVA.StylePriority.UseFont = false;
            this.xrTableCellIVA.StylePriority.UseTextAlignment = false;
            this.xrTableCellIVA.Text = "xrTableCellIVA";
            this.xrTableCellIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIVA.Weight = 0.6947176134767139D;
            // 
            // xrTableCellSubtotal
            // 
            this.xrTableCellSubtotal.Dpi = 100F;
            this.xrTableCellSubtotal.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellSubtotal.Name = "xrTableCellSubtotal";
            this.xrTableCellSubtotal.StylePriority.UseFont = false;
            this.xrTableCellSubtotal.StylePriority.UseTextAlignment = false;
            this.xrTableCellSubtotal.Text = "xrTableCellSubtotal";
            this.xrTableCellSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellSubtotal.Weight = 0.67807003103236752D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 28F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 29F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrIVASupplier,
            this.xrLblSupplier,
            this.xrLabel1,
            this.xrLabelIVA,
            this.xrFolio,
            this.xrLabelFolio,
            this.xrCreditorNombre,
            this.xrLCreditorName,
            this.xrLabelSubTotal,
            this.xrLabelstock,
            this.xrLabelplanograma,
            this.xrLabeldescrip,
            this.xrLabelCodigo,
            this.xrExecuteDate,
            this.xrDistribuidora,
            this.xrCP,
            this.xrCity,
            this.xrLCP,
            this.xrLCorreo,
            this.xrAddress,
            this.xrLState,
            this.xrState,
            this.xrLStore,
            this.xrEmail,
            this.xrLPhone,
            this.xrLAddress,
            this.xrLCity,
            this.xrStore,
            this.xrPhone,
            this.xrPictureBox1,
            this.xrRequirement,
            this.xrExecute,
            this.xrLExecuteDate,
            this.xrLTitle,
            this.xrLCreditorPhone,
            this.xrLCreditorEmail,
            this.xrCreditorPhone,
            this.xrCreditorEmail,
            this.xrCreditorBusinessName,
            this.xrLContacts,
            this.xrLCreditorBusinessName,
            this.xrLRequirement,
            this.xrLine1,
            this.xrLExecute});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 236.6324F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabelIVA
            // 
            this.xrLabelIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelIVA.Dpi = 100F;
            this.xrLabelIVA.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelIVA.LocationFloat = new DevExpress.Utils.PointFloat(623.9593F, 214.8549F);
            this.xrLabelIVA.Multiline = true;
            this.xrLabelIVA.Name = "xrLabelIVA";
            this.xrLabelIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelIVA.SizeF = new System.Drawing.SizeF(86.05145F, 21.63786F);
            this.xrLabelIVA.StylePriority.UseBorders = false;
            this.xrLabelIVA.StylePriority.UseFont = false;
            this.xrLabelIVA.StylePriority.UseTextAlignment = false;
            this.xrLabelIVA.Text = "IVA";
            this.xrLabelIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrFolio
            // 
            this.xrFolio.Dpi = 100F;
            this.xrFolio.Font = new System.Drawing.Font("Arial", 7F);
            this.xrFolio.LocationFloat = new DevExpress.Utils.PointFloat(78.05079F, 76.19786F);
            this.xrFolio.Name = "xrFolio";
            this.xrFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFolio.SizeF = new System.Drawing.SizeF(72.9166F, 13F);
            this.xrFolio.StylePriority.UseFont = false;
            this.xrFolio.StylePriority.UseTextAlignment = false;
            this.xrFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelFolio
            // 
            this.xrLabelFolio.Dpi = 100F;
            this.xrLabelFolio.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelFolio.LocationFloat = new DevExpress.Utils.PointFloat(10.0001F, 76.19786F);
            this.xrLabelFolio.Name = "xrLabelFolio";
            this.xrLabelFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFolio.SizeF = new System.Drawing.SizeF(65.79997F, 13F);
            this.xrLabelFolio.StylePriority.UseFont = false;
            this.xrLabelFolio.Text = "Folio:";
            // 
            // xrCreditorNombre
            // 
            this.xrCreditorNombre.Dpi = 100F;
            this.xrCreditorNombre.Font = new System.Drawing.Font("Arial", 7F);
            this.xrCreditorNombre.LocationFloat = new DevExpress.Utils.PointFloat(96.92352F, 149.8401F);
            this.xrCreditorNombre.Name = "xrCreditorNombre";
            this.xrCreditorNombre.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCreditorNombre.SizeF = new System.Drawing.SizeF(376.8581F, 24.71875F);
            this.xrCreditorNombre.StylePriority.UseFont = false;
            // 
            // xrLCreditorName
            // 
            this.xrLCreditorName.Dpi = 100F;
            this.xrLCreditorName.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCreditorName.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 149.8401F);
            this.xrLCreditorName.Name = "xrLCreditorName";
            this.xrLCreditorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCreditorName.SizeF = new System.Drawing.SizeF(57.72836F, 12.77994F);
            this.xrLCreditorName.StylePriority.UseFont = false;
            this.xrLCreditorName.Text = "Nombre(s):";
            // 
            // xrLabelSubTotal
            // 
            this.xrLabelSubTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelSubTotal.Dpi = 100F;
            this.xrLabelSubTotal.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelSubTotal.LocationFloat = new DevExpress.Utils.PointFloat(710.0107F, 214.9946F);
            this.xrLabelSubTotal.Multiline = true;
            this.xrLabelSubTotal.Name = "xrLabelSubTotal";
            this.xrLabelSubTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSubTotal.SizeF = new System.Drawing.SizeF(83.98926F, 21.63786F);
            this.xrLabelSubTotal.StylePriority.UseBorders = false;
            this.xrLabelSubTotal.StylePriority.UseFont = false;
            this.xrLabelSubTotal.StylePriority.UseTextAlignment = false;
            this.xrLabelSubTotal.Text = "SubTotal";
            this.xrLabelSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelstock
            // 
            this.xrLabelstock.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelstock.Dpi = 100F;
            this.xrLabelstock.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelstock.LocationFloat = new DevExpress.Utils.PointFloat(473.7816F, 214.855F);
            this.xrLabelstock.Multiline = true;
            this.xrLabelstock.Name = "xrLabelstock";
            this.xrLabelstock.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelstock.SizeF = new System.Drawing.SizeF(72.4584F, 21.63786F);
            this.xrLabelstock.StylePriority.UseBorders = false;
            this.xrLabelstock.StylePriority.UseFont = false;
            this.xrLabelstock.StylePriority.UseTextAlignment = false;
            this.xrLabelstock.Text = "Precio";
            this.xrLabelstock.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelplanograma
            // 
            this.xrLabelplanograma.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelplanograma.Dpi = 100F;
            this.xrLabelplanograma.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelplanograma.LocationFloat = new DevExpress.Utils.PointFloat(408.9406F, 214.855F);
            this.xrLabelplanograma.Name = "xrLabelplanograma";
            this.xrLabelplanograma.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelplanograma.SizeF = new System.Drawing.SizeF(64.84106F, 21.63782F);
            this.xrLabelplanograma.StylePriority.UseBorders = false;
            this.xrLabelplanograma.StylePriority.UseFont = false;
            this.xrLabelplanograma.StylePriority.UseTextAlignment = false;
            this.xrLabelplanograma.Text = "Cantidad";
            this.xrLabelplanograma.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabeldescrip
            // 
            this.xrLabeldescrip.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabeldescrip.Dpi = 100F;
            this.xrLabeldescrip.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabeldescrip.LocationFloat = new DevExpress.Utils.PointFloat(120.7655F, 214.855F);
            this.xrLabeldescrip.Name = "xrLabeldescrip";
            this.xrLabeldescrip.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabeldescrip.SizeF = new System.Drawing.SizeF(288.1751F, 21.63785F);
            this.xrLabeldescrip.StylePriority.UseBorders = false;
            this.xrLabeldescrip.StylePriority.UseFont = false;
            this.xrLabeldescrip.StylePriority.UseTextAlignment = false;
            this.xrLabeldescrip.Text = "Descripción";
            this.xrLabeldescrip.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelCodigo
            // 
            this.xrLabelCodigo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelCodigo.Dpi = 100F;
            this.xrLabelCodigo.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelCodigo.LocationFloat = new DevExpress.Utils.PointFloat(4.01078E-05F, 214.855F);
            this.xrLabelCodigo.Name = "xrLabelCodigo";
            this.xrLabelCodigo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCodigo.SizeF = new System.Drawing.SizeF(120.7655F, 21.63786F);
            this.xrLabelCodigo.StylePriority.UseBorders = false;
            this.xrLabelCodigo.StylePriority.UseFont = false;
            this.xrLabelCodigo.StylePriority.UseTextAlignment = false;
            this.xrLabelCodigo.Text = "Código";
            this.xrLabelCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrExecuteDate
            // 
            this.xrExecuteDate.Dpi = 100F;
            this.xrExecuteDate.Font = new System.Drawing.Font("Arial", 7F);
            this.xrExecuteDate.LocationFloat = new DevExpress.Utils.PointFloat(150.9674F, 49.19781F);
            this.xrExecuteDate.Name = "xrExecuteDate";
            this.xrExecuteDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrExecuteDate.SizeF = new System.Drawing.SizeF(142.7909F, 13F);
            this.xrExecuteDate.StylePriority.UseFont = false;
            // 
            // xrDistribuidora
            // 
            this.xrDistribuidora.Dpi = 100F;
            this.xrDistribuidora.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDistribuidora.LocationFloat = new DevExpress.Utils.PointFloat(360.4094F, 0.1978397F);
            this.xrDistribuidora.Name = "xrDistribuidora";
            this.xrDistribuidora.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDistribuidora.SizeF = new System.Drawing.SizeF(290.6326F, 12F);
            this.xrDistribuidora.StylePriority.UseFont = false;
            this.xrDistribuidora.StylePriority.UseTextAlignment = false;
            this.xrDistribuidora.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrDistribuidora.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrCP
            // 
            this.xrCP.Dpi = 100F;
            this.xrCP.Font = new System.Drawing.Font("Arial", 6F);
            this.xrCP.LocationFloat = new DevExpress.Utils.PointFloat(683.8843F, 76.19785F);
            this.xrCP.Name = "xrCP";
            this.xrCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCP.SizeF = new System.Drawing.SizeF(63.11603F, 24.34006F);
            this.xrCP.StylePriority.UseFont = false;
            this.xrCP.StylePriority.UseTextAlignment = false;
            this.xrCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrCity
            // 
            this.xrCity.Dpi = 100F;
            this.xrCity.Font = new System.Drawing.Font("Arial", 6F);
            this.xrCity.LocationFloat = new DevExpress.Utils.PointFloat(372.8525F, 49.19781F);
            this.xrCity.Name = "xrCity";
            this.xrCity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCity.SizeF = new System.Drawing.SizeF(79.53903F, 13F);
            this.xrCity.StylePriority.UseFont = false;
            this.xrCity.StylePriority.UseTextAlignment = false;
            this.xrCity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLCP
            // 
            this.xrLCP.Dpi = 100F;
            this.xrLCP.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCP.LocationFloat = new DevExpress.Utils.PointFloat(651.0423F, 76.19785F);
            this.xrLCP.Name = "xrLCP";
            this.xrLCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCP.SizeF = new System.Drawing.SizeF(32.84198F, 13F);
            this.xrLCP.StylePriority.UseFont = false;
            this.xrLCP.StylePriority.UseTextAlignment = false;
            this.xrLCP.Text = "C. P.:";
            this.xrLCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLCorreo
            // 
            this.xrLCorreo.Dpi = 100F;
            this.xrLCorreo.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCorreo.LocationFloat = new DevExpress.Utils.PointFloat(469.0477F, 76.19785F);
            this.xrLCorreo.Name = "xrLCorreo";
            this.xrLCorreo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCorreo.SizeF = new System.Drawing.SizeF(37.66742F, 13.00001F);
            this.xrLCorreo.StylePriority.UseFont = false;
            this.xrLCorreo.StylePriority.UseTextAlignment = false;
            this.xrLCorreo.Text = "Correo: ";
            this.xrLCorreo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrAddress
            // 
            this.xrAddress.Dpi = 100F;
            this.xrAddress.Font = new System.Drawing.Font("Arial", 6F);
            this.xrAddress.LocationFloat = new DevExpress.Utils.PointFloat(383.2626F, 25.19784F);
            this.xrAddress.Name = "xrAddress";
            this.xrAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrAddress.SizeF = new System.Drawing.SizeF(267.7794F, 24F);
            this.xrAddress.StylePriority.UseFont = false;
            // 
            // xrLState
            // 
            this.xrLState.Dpi = 100F;
            this.xrLState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLState.LocationFloat = new DevExpress.Utils.PointFloat(469.0477F, 49.19781F);
            this.xrLState.Name = "xrLState";
            this.xrLState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLState.SizeF = new System.Drawing.SizeF(37.66742F, 13F);
            this.xrLState.StylePriority.UseFont = false;
            this.xrLState.StylePriority.UseTextAlignment = false;
            this.xrLState.Text = "Estado: ";
            this.xrLState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrState
            // 
            this.xrState.Dpi = 100F;
            this.xrState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrState.LocationFloat = new DevExpress.Utils.PointFloat(506.7151F, 49.19783F);
            this.xrState.Name = "xrState";
            this.xrState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrState.SizeF = new System.Drawing.SizeF(144.3267F, 13F);
            this.xrState.StylePriority.UseFont = false;
            this.xrState.StylePriority.UseTextAlignment = false;
            this.xrState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLStore
            // 
            this.xrLStore.Dpi = 100F;
            this.xrLStore.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLStore.LocationFloat = new DevExpress.Utils.PointFloat(331.2464F, 12.19784F);
            this.xrLStore.Name = "xrLStore";
            this.xrLStore.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLStore.SizeF = new System.Drawing.SizeF(52.01611F, 13F);
            this.xrLStore.StylePriority.UseFont = false;
            this.xrLStore.StylePriority.UseTextAlignment = false;
            this.xrLStore.Text = "Sucursal: ";
            this.xrLStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrEmail
            // 
            this.xrEmail.Dpi = 100F;
            this.xrEmail.Font = new System.Drawing.Font("Arial", 6F);
            this.xrEmail.LocationFloat = new DevExpress.Utils.PointFloat(506.7151F, 76.19785F);
            this.xrEmail.Name = "xrEmail";
            this.xrEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrEmail.SizeF = new System.Drawing.SizeF(144.3267F, 24.34006F);
            this.xrEmail.StylePriority.UseFont = false;
            this.xrEmail.StylePriority.UseTextAlignment = false;
            this.xrEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLPhone
            // 
            this.xrLPhone.Dpi = 100F;
            this.xrLPhone.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLPhone.LocationFloat = new DevExpress.Utils.PointFloat(331.2426F, 77.19781F);
            this.xrLPhone.Name = "xrLPhone";
            this.xrLPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLPhone.SizeF = new System.Drawing.SizeF(45.94989F, 13.00002F);
            this.xrLPhone.StylePriority.UseFont = false;
            this.xrLPhone.StylePriority.UseTextAlignment = false;
            this.xrLPhone.Text = "Teléfono: ";
            this.xrLPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLAddress
            // 
            this.xrLAddress.Dpi = 100F;
            this.xrLAddress.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLAddress.LocationFloat = new DevExpress.Utils.PointFloat(331.2426F, 25.19784F);
            this.xrLAddress.Name = "xrLAddress";
            this.xrLAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLAddress.SizeF = new System.Drawing.SizeF(52.02F, 13F);
            this.xrLAddress.StylePriority.UseFont = false;
            this.xrLAddress.StylePriority.UseTextAlignment = false;
            this.xrLAddress.Text = "Dirección: ";
            this.xrLAddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLCity
            // 
            this.xrLCity.Dpi = 100F;
            this.xrLCity.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCity.LocationFloat = new DevExpress.Utils.PointFloat(331.2426F, 48.86449F);
            this.xrLCity.Name = "xrLCity";
            this.xrLCity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCity.SizeF = new System.Drawing.SizeF(41.61F, 13F);
            this.xrLCity.StylePriority.UseFont = false;
            this.xrLCity.StylePriority.UseTextAlignment = false;
            this.xrLCity.Text = "Ciudad: ";
            this.xrLCity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrStore
            // 
            this.xrStore.Dpi = 100F;
            this.xrStore.Font = new System.Drawing.Font("Arial", 6F);
            this.xrStore.LocationFloat = new DevExpress.Utils.PointFloat(383.2626F, 12.19783F);
            this.xrStore.Name = "xrStore";
            this.xrStore.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrStore.SizeF = new System.Drawing.SizeF(267.7794F, 13F);
            this.xrStore.StylePriority.UseFont = false;
            this.xrStore.StylePriority.UseTextAlignment = false;
            this.xrStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPhone
            // 
            this.xrPhone.Dpi = 100F;
            this.xrPhone.Font = new System.Drawing.Font("Arial", 6F);
            this.xrPhone.LocationFloat = new DevExpress.Utils.PointFloat(377.1924F, 77.19784F);
            this.xrPhone.Name = "xrPhone";
            this.xrPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPhone.SizeF = new System.Drawing.SizeF(79.53897F, 13F);
            this.xrPhone.StylePriority.UseFont = false;
            this.xrPhone.StylePriority.UseTextAlignment = false;
            this.xrPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(651.0421F, 0.1978397F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(95.95795F, 61.66666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrRequirement
            // 
            this.xrRequirement.Dpi = 100F;
            this.xrRequirement.Font = new System.Drawing.Font("Arial", 7F);
            this.xrRequirement.LocationFloat = new DevExpress.Utils.PointFloat(75.80002F, 62.19781F);
            this.xrRequirement.Name = "xrRequirement";
            this.xrRequirement.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRequirement.SizeF = new System.Drawing.SizeF(76.50684F, 13F);
            this.xrRequirement.StylePriority.UseFont = false;
            this.xrRequirement.StylePriority.UseTextAlignment = false;
            this.xrRequirement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrExecute
            // 
            this.xrExecute.Dpi = 100F;
            this.xrExecute.Font = new System.Drawing.Font("Arial", 7F);
            this.xrExecute.LocationFloat = new DevExpress.Utils.PointFloat(73.72639F, 25.19784F);
            this.xrExecute.Name = "xrExecute";
            this.xrExecute.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrExecute.SizeF = new System.Drawing.SizeF(237.9178F, 13F);
            this.xrExecute.StylePriority.UseFont = false;
            // 
            // xrLExecuteDate
            // 
            this.xrLExecuteDate.Dpi = 100F;
            this.xrLExecuteDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLExecuteDate.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 49.19781F);
            this.xrLExecuteDate.Name = "xrLExecuteDate";
            this.xrLExecuteDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLExecuteDate.SizeF = new System.Drawing.SizeF(119.7364F, 13F);
            this.xrLExecuteDate.StylePriority.UseFont = false;
            this.xrLExecuteDate.Text = "Fecha de Realización:";
            // 
            // xrLTitle
            // 
            this.xrLTitle.Dpi = 100F;
            this.xrLTitle.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLTitle.LocationFloat = new DevExpress.Utils.PointFloat(73.72639F, 0F);
            this.xrLTitle.Name = "xrLTitle";
            this.xrLTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLTitle.SizeF = new System.Drawing.SizeF(237.9178F, 20F);
            this.xrLTitle.StylePriority.UseFont = false;
            this.xrLTitle.StylePriority.UseTextAlignment = false;
            this.xrLTitle.Text = "Orden de Compra a Acreedor";
            this.xrLTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLCreditorPhone
            // 
            this.xrLCreditorPhone.Dpi = 100F;
            this.xrLCreditorPhone.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCreditorPhone.LocationFloat = new DevExpress.Utils.PointFloat(499.97F, 119.9357F);
            this.xrLCreditorPhone.Name = "xrLCreditorPhone";
            this.xrLCreditorPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCreditorPhone.SizeF = new System.Drawing.SizeF(57.72838F, 13F);
            this.xrLCreditorPhone.StylePriority.UseFont = false;
            this.xrLCreditorPhone.Text = "Teléfono(s):";
            // 
            // xrLCreditorEmail
            // 
            this.xrLCreditorEmail.Dpi = 100F;
            this.xrLCreditorEmail.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCreditorEmail.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 186.3905F);
            this.xrLCreditorEmail.Name = "xrLCreditorEmail";
            this.xrLCreditorEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCreditorEmail.SizeF = new System.Drawing.SizeF(57.72838F, 13F);
            this.xrLCreditorEmail.StylePriority.UseFont = false;
            this.xrLCreditorEmail.Text = "Correo(s):";
            // 
            // xrCreditorPhone
            // 
            this.xrCreditorPhone.Dpi = 100F;
            this.xrCreditorPhone.Font = new System.Drawing.Font("Arial", 7F);
            this.xrCreditorPhone.LocationFloat = new DevExpress.Utils.PointFloat(496.0857F, 136.8401F);
            this.xrCreditorPhone.Name = "xrCreditorPhone";
            this.xrCreditorPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCreditorPhone.SizeF = new System.Drawing.SizeF(240.9143F, 41.62312F);
            this.xrCreditorPhone.StylePriority.UseFont = false;
            // 
            // xrCreditorEmail
            // 
            this.xrCreditorEmail.Dpi = 100F;
            this.xrCreditorEmail.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCreditorEmail.LocationFloat = new DevExpress.Utils.PointFloat(96.92352F, 186.3904F);
            this.xrCreditorEmail.Name = "xrCreditorEmail";
            this.xrCreditorEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCreditorEmail.SizeF = new System.Drawing.SizeF(640.0764F, 28.46454F);
            this.xrCreditorEmail.StylePriority.UseFont = false;
            // 
            // xrCreditorBusinessName
            // 
            this.xrCreditorBusinessName.Dpi = 100F;
            this.xrCreditorBusinessName.Font = new System.Drawing.Font("Arial", 7F);
            this.xrCreditorBusinessName.LocationFloat = new DevExpress.Utils.PointFloat(150.9674F, 121.3953F);
            this.xrCreditorBusinessName.Name = "xrCreditorBusinessName";
            this.xrCreditorBusinessName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCreditorBusinessName.SizeF = new System.Drawing.SizeF(322.8142F, 12.99999F);
            this.xrCreditorBusinessName.StylePriority.UseFont = false;
            // 
            // xrLContacts
            // 
            this.xrLContacts.Dpi = 100F;
            this.xrLContacts.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLContacts.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 136.8401F);
            this.xrLContacts.Name = "xrLContacts";
            this.xrLContacts.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLContacts.SizeF = new System.Drawing.SizeF(65.79998F, 13F);
            this.xrLContacts.StylePriority.UseFont = false;
            this.xrLContacts.Text = "Contacto(s):";
            // 
            // xrLCreditorBusinessName
            // 
            this.xrLCreditorBusinessName.Dpi = 100F;
            this.xrLCreditorBusinessName.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLCreditorBusinessName.LocationFloat = new DevExpress.Utils.PointFloat(96.92352F, 121.3953F);
            this.xrLCreditorBusinessName.Name = "xrLCreditorBusinessName";
            this.xrLCreditorBusinessName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLCreditorBusinessName.SizeF = new System.Drawing.SizeF(54.04387F, 12.99999F);
            this.xrLCreditorBusinessName.StylePriority.UseFont = false;
            this.xrLCreditorBusinessName.Text = "Acreedor:";
            // 
            // xrLRequirement
            // 
            this.xrLRequirement.Dpi = 100F;
            this.xrLRequirement.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLRequirement.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 62.19781F);
            this.xrLRequirement.Name = "xrLRequirement";
            this.xrLRequirement.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLRequirement.SizeF = new System.Drawing.SizeF(65.8F, 13F);
            this.xrLRequirement.StylePriority.UseFont = false;
            this.xrLRequirement.Text = "O. Compra:";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(2.103693E-05F, 100.5379F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(747F, 10F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLExecute
            // 
            this.xrLExecute.Dpi = 100F;
            this.xrLExecute.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLExecute.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 25.19784F);
            this.xrLExecute.Name = "xrLExecute";
            this.xrLExecute.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLExecute.SizeF = new System.Drawing.SizeF(55.21029F, 13F);
            this.xrLExecute.StylePriority.UseFont = false;
            this.xrLExecute.Text = "Realizó:";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 23.95833F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrImport,
            this.xrIVA,
            this.xrTotal,
            this.xrLTotal});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 21.63786F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrIVA
            // 
            this.xrIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrIVA.Dpi = 100F;
            this.xrIVA.Font = new System.Drawing.Font("Arial", 9F);
            this.xrIVA.LocationFloat = new DevExpress.Utils.PointFloat(623.9594F, 0F);
            this.xrIVA.Multiline = true;
            this.xrIVA.Name = "xrIVA";
            this.xrIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrIVA.SizeF = new System.Drawing.SizeF(86.05145F, 21.63786F);
            this.xrIVA.StylePriority.UseBorders = false;
            this.xrIVA.StylePriority.UseFont = false;
            this.xrIVA.StylePriority.UseTextAlignment = false;
            this.xrIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTotal
            // 
            this.xrTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTotal.Dpi = 100F;
            this.xrTotal.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTotal.LocationFloat = new DevExpress.Utils.PointFloat(710.0108F, 0F);
            this.xrTotal.Multiline = true;
            this.xrTotal.Name = "xrTotal";
            this.xrTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTotal.SizeF = new System.Drawing.SizeF(83.9892F, 21.63786F);
            this.xrTotal.StylePriority.UseBorders = false;
            this.xrTotal.StylePriority.UseFont = false;
            this.xrTotal.StylePriority.UseTextAlignment = false;
            this.xrTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLTotal
            // 
            this.xrLTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLTotal.Dpi = 100F;
            this.xrLTotal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.xrLTotal.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLTotal.Multiline = true;
            this.xrLTotal.Name = "xrLTotal";
            this.xrLTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLTotal.SizeF = new System.Drawing.SizeF(120.7656F, 21.63786F);
            this.xrLTotal.StylePriority.UseBorders = false;
            this.xrLTotal.StylePriority.UseFont = false;
            this.xrLTotal.StylePriority.UseTextAlignment = false;
            this.xrLTotal.Text = "Total";
            this.xrLTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCellImport
            // 
            this.xrTableCellImport.Dpi = 100F;
            this.xrTableCellImport.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellImport.Name = "xrTableCellImport";
            this.xrTableCellImport.StylePriority.UseFont = false;
            this.xrTableCellImport.StylePriority.UseTextAlignment = false;
            this.xrTableCellImport.Text = "xrTableCellImport";
            this.xrTableCellImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellImport.Weight = 0.627450362748873D;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(546.24F, 214.9946F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(77.71936F, 21.63786F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Importe";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrImport
            // 
            this.xrImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrImport.Dpi = 100F;
            this.xrImport.Font = new System.Drawing.Font("Arial", 9F);
            this.xrImport.LocationFloat = new DevExpress.Utils.PointFloat(546.24F, 0F);
            this.xrImport.Multiline = true;
            this.xrImport.Name = "xrImport";
            this.xrImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrImport.SizeF = new System.Drawing.SizeF(77.71936F, 21.63786F);
            this.xrImport.StylePriority.UseBorders = false;
            this.xrImport.StylePriority.UseFont = false;
            this.xrImport.StylePriority.UseTextAlignment = false;
            this.xrImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(120.7656F, 0F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(425.4744F, 21.63786F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLblSupplier
            // 
            this.xrLblSupplier.Dpi = 100F;
            this.xrLblSupplier.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblSupplier.LocationFloat = new DevExpress.Utils.PointFloat(164.1F, 76.19785F);
            this.xrLblSupplier.Name = "xrLblSupplier";
            this.xrLblSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblSupplier.SizeF = new System.Drawing.SizeF(65.8F, 13F);
            this.xrLblSupplier.StylePriority.UseFont = false;
            this.xrLblSupplier.Text = "IVA: ";
            this.xrLblSupplier.Visible = false;
            // 
            // xrIVASupplier
            // 
            this.xrIVASupplier.Dpi = 100F;
            this.xrIVASupplier.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrIVASupplier.LocationFloat = new DevExpress.Utils.PointFloat(245.8441F, 76.19785F);
            this.xrIVASupplier.Name = "xrIVASupplier";
            this.xrIVASupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrIVASupplier.SizeF = new System.Drawing.SizeF(65.8F, 13F);
            this.xrIVASupplier.StylePriority.UseFont = false;
            this.xrIVASupplier.Visible = false;
            // 
            // PurchasesOrderRequirement
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.GroupFooter1});
            this.Margins = new System.Drawing.Printing.Margins(25, 31, 28, 29);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLCreditorBusinessName;
        private DevExpress.XtraReports.UI.XRLabel xrLRequirement;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLExecute;
        private DevExpress.XtraReports.UI.XRLabel xrDistribuidora;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLCreditorPhone;
        private DevExpress.XtraReports.UI.XRLabel xrLCreditorEmail;
        private DevExpress.XtraReports.UI.XRLabel xrCreditorPhone;
        private DevExpress.XtraReports.UI.XRLabel xrCreditorEmail;
        private DevExpress.XtraReports.UI.XRLabel xrCreditorBusinessName;
        private DevExpress.XtraReports.UI.XRLabel xrLState;
        private DevExpress.XtraReports.UI.XRLabel xrLCity;
        private DevExpress.XtraReports.UI.XRLabel xrLCP;
        private DevExpress.XtraReports.UI.XRLabel xrLAddress;
        private DevExpress.XtraReports.UI.XRLabel xrLStore;
        private DevExpress.XtraReports.UI.XRLabel xrAddress;
        private DevExpress.XtraReports.UI.XRLabel xrLExecuteDate;
        private DevExpress.XtraReports.UI.XRLabel xrLTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLCorreo;
        private DevExpress.XtraReports.UI.XRLabel xrLPhone;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrExecute;
        private DevExpress.XtraReports.UI.XRLabel xrRequirement;
        private DevExpress.XtraReports.UI.XRLabel xrStore;
        private DevExpress.XtraReports.UI.XRLabel xrCP;
        private DevExpress.XtraReports.UI.XRLabel xrEmail;
        private DevExpress.XtraReports.UI.XRLabel xrState;
        private DevExpress.XtraReports.UI.XRLabel xrPhone;
        private DevExpress.XtraReports.UI.XRLabel xrCity;
        private DevExpress.XtraReports.UI.XRLabel xrExecuteDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSubTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabelstock;
        private DevExpress.XtraReports.UI.XRLabel xrLabelplanograma;
        private DevExpress.XtraReports.UI.XRLabel xrLabeldescrip;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCodigo;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellPartNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellQuantity;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellBaseCost;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellSubtotal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDescription;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLTotal;
        private DevExpress.XtraReports.UI.XRLabel xrCreditorNombre;
        private DevExpress.XtraReports.UI.XRLabel xrLCreditorName;
        private DevExpress.XtraReports.UI.XRLabel xrLContacts;
        private DevExpress.XtraReports.UI.XRLabel xrFolio;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFolio;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIVA;
        private DevExpress.XtraReports.UI.XRLabel xrLabelIVA;
        private DevExpress.XtraReports.UI.XRLabel xrIVA;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellImport;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrImport;
        private DevExpress.XtraReports.UI.XRLabel xrIVASupplier;
        private DevExpress.XtraReports.UI.XRLabel xrLblSupplier;
    }
}
