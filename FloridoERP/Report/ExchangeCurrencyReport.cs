﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.DAL.ExchangeCurrency;

namespace FloridoERP.Report
{
    public partial class ExchangeCurrencyReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static DataSet DataSetTableExchangeCurrency = null;
        public ExchangeCurrencyReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<ExchangeCurrencyViewModel> Currency, string siteName) 
        {
            DataSetTableExchangeCurrency = new DataSet();

            DataTable DataTableCurrency = new DataTable();
            DataSetTableExchangeCurrency.Tables.Add(DataTableCurrency);

            DataTableCurrency.Columns.Add(new DataColumn("xrTableCell1"));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCell2"));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCell3"));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCell4"));
            xrLabel6.Text = "Elaborado: " + DateTime.Now.ToShortDateString();
            xrLabel7.Text = "Tienda: " + siteName;

            foreach (var item in Currency)
            {
                DataRow DataRowOrdered = DataSetTableExchangeCurrency.Tables[0].NewRow();
                DataRowOrdered["xrTableCell1"] = "$ "+ item.Price;
                DataRowOrdered["xrTableCell2"] = item.Currency;
                DataRowOrdered["xrTableCell3"] = item.CurrencyDate;
                DataRowOrdered["xrTableCell4"] = item.User;
                DataSetTableExchangeCurrency.Tables[0].Rows.Add(DataRowOrdered);
            }

            xrTableCell1.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCell1"));
            xrTableCell2.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCell2"));
            xrTableCell3.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCell3"));
            xrTableCell4.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCell4"));


            return DataSetTableExchangeCurrency;
        }
        internal object printTable(object p, List<ExchangeCurrencyViewModel> list)
        {
            throw new NotImplementedException();
        }
    }
}
