﻿namespace FloridoERP.Report
{
    partial class InvoicePDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.QRCodeGenerator qrCodeGenerator1 = new DevExpress.XtraPrinting.BarCode.QRCodeGenerator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrDescripcion = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableDescripcion = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellCodigo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellNoIdentificacion = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellCantidad = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellClaveUnidad = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellUnidad = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellValorUnitario = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellImporte = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDescuento = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellNoPedimento = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellNoCuentaPredial = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrUsoCFDI = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUsoCFDI = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRegimenFiscal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelRegimenFiscal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelNoCuentaPredial = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelNoPedimento = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDescuento = new DevExpress.XtraReports.UI.XRLabel();
            this.xrConceptos = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFolioFiscal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCPFechaHora = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSerieCSD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFolioFiscal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCPFechaHora = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSerieCSD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrEfectoComp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEfectoComp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUnidad = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelValorUnitario = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelReceptorNombre = new DevExpress.XtraReports.UI.XRLabel();
            this.xrReceptor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImporte = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelClaveUnidad = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCantidad = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelIdentificacion = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCodigo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEmisor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelRFCReceptor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelRFCEmisor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrEmisor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRFCReceptor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRFCEmisor = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabelImpuestoRet02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipoRet02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelBaseRet02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipoFactorRet02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTasaCuotaRet02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImportRet02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImpuestoRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipoRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelBaseRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipoFactorRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTasaCuotaRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImportRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBarCodeQR = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFechaTimbrado = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCertificadoSAT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelRFCProvCert = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCadenaSAT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCadenaSAT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSelloSAT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSelloSAT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSelloCFDI = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSelloCFDI = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTasaOCuota = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImpRetIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImpTras = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImpRetISR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrImpTras = new DevExpress.XtraReports.UI.XRLabel();
            this.xrImpRet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrMoneda = new DevExpress.XtraReports.UI.XRLabel();
            this.xrMetodoPago = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFormaPago = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelMoneda = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelMetodoPago = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFormaPago = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTipoCambio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipoCambio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTasaCuota = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipoFactor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelBase = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTipo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelImpuesto = new DevExpress.XtraReports.UI.XRLabel();
            this.xrImporte = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTasaCuota = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTipoFactor = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBase = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTipo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrImpuesto = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFolioFactura = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDescripcion,
            this.xrTable2,
            this.xrTable1});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 69.1F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDescripcion
            // 
            this.xrDescripcion.BackColor = System.Drawing.Color.LightGray;
            this.xrDescripcion.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrDescripcion.Dpi = 100F;
            this.xrDescripcion.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrDescripcion.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.5F);
            this.xrDescripcion.Name = "xrDescripcion";
            this.xrDescripcion.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDescripcion.SizeF = new System.Drawing.SizeF(95.77802F, 45.59628F);
            this.xrDescripcion.StylePriority.UseBackColor = false;
            this.xrDescripcion.StylePriority.UseBorders = false;
            this.xrDescripcion.StylePriority.UseFont = false;
            this.xrDescripcion.StylePriority.UseTextAlignment = false;
            this.xrDescripcion.Text = "Descripción";
            this.xrDescripcion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(95.78F, 23.5F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(285.49F, 45.6F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableDescripcion});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableDescripcion
            // 
            this.xrTableDescripcion.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableDescripcion.Dpi = 100F;
            this.xrTableDescripcion.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableDescripcion.Name = "xrTableDescripcion";
            this.xrTableDescripcion.StylePriority.UseBorders = false;
            this.xrTableDescripcion.StylePriority.UseFont = false;
            this.xrTableDescripcion.StylePriority.UseTextAlignment = false;
            this.xrTableDescripcion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableDescripcion.Weight = 6.41018997877871D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(794F, 22.98196F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellCodigo,
            this.xrTableCellNoIdentificacion,
            this.xrTableCellCantidad,
            this.xrTableCellClaveUnidad,
            this.xrTableCellUnidad,
            this.xrTableCellValorUnitario,
            this.xrTableCellImporte,
            this.xrTableCellDescuento,
            this.xrTableCellNoPedimento,
            this.xrTableCellNoCuentaPredial});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.66729862276656871D;
            // 
            // xrTableCellCodigo
            // 
            this.xrTableCellCodigo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellCodigo.Dpi = 100F;
            this.xrTableCellCodigo.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellCodigo.Name = "xrTableCellCodigo";
            this.xrTableCellCodigo.StylePriority.UseBorders = false;
            this.xrTableCellCodigo.StylePriority.UseFont = false;
            this.xrTableCellCodigo.StylePriority.UseTextAlignment = false;
            this.xrTableCellCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellCodigo.Weight = 0.77324347088153389D;
            // 
            // xrTableCellNoIdentificacion
            // 
            this.xrTableCellNoIdentificacion.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellNoIdentificacion.Dpi = 100F;
            this.xrTableCellNoIdentificacion.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellNoIdentificacion.Name = "xrTableCellNoIdentificacion";
            this.xrTableCellNoIdentificacion.StylePriority.UseBorders = false;
            this.xrTableCellNoIdentificacion.StylePriority.UseFont = false;
            this.xrTableCellNoIdentificacion.StylePriority.UseTextAlignment = false;
            this.xrTableCellNoIdentificacion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellNoIdentificacion.Weight = 0.75391097196248213D;
            // 
            // xrTableCellCantidad
            // 
            this.xrTableCellCantidad.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellCantidad.Dpi = 100F;
            this.xrTableCellCantidad.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellCantidad.Name = "xrTableCellCantidad";
            this.xrTableCellCantidad.StylePriority.UseBorders = false;
            this.xrTableCellCantidad.StylePriority.UseFont = false;
            this.xrTableCellCantidad.StylePriority.UseTextAlignment = false;
            this.xrTableCellCantidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellCantidad.Weight = 0.5234807729011306D;
            // 
            // xrTableCellClaveUnidad
            // 
            this.xrTableCellClaveUnidad.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellClaveUnidad.Dpi = 100F;
            this.xrTableCellClaveUnidad.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellClaveUnidad.Name = "xrTableCellClaveUnidad";
            this.xrTableCellClaveUnidad.StylePriority.UseBorders = false;
            this.xrTableCellClaveUnidad.StylePriority.UseFont = false;
            this.xrTableCellClaveUnidad.StylePriority.UseTextAlignment = false;
            this.xrTableCellClaveUnidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellClaveUnidad.Weight = 0.49247089726432836D;
            // 
            // xrTableCellUnidad
            // 
            this.xrTableCellUnidad.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellUnidad.Dpi = 100F;
            this.xrTableCellUnidad.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellUnidad.Name = "xrTableCellUnidad";
            this.xrTableCellUnidad.StylePriority.UseBorders = false;
            this.xrTableCellUnidad.StylePriority.UseFont = false;
            this.xrTableCellUnidad.StylePriority.UseTextAlignment = false;
            this.xrTableCellUnidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellUnidad.Weight = 0.53494435765217418D;
            // 
            // xrTableCellValorUnitario
            // 
            this.xrTableCellValorUnitario.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellValorUnitario.Dpi = 100F;
            this.xrTableCellValorUnitario.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellValorUnitario.Name = "xrTableCellValorUnitario";
            this.xrTableCellValorUnitario.StylePriority.UseBorders = false;
            this.xrTableCellValorUnitario.StylePriority.UseFont = false;
            this.xrTableCellValorUnitario.StylePriority.UseTextAlignment = false;
            this.xrTableCellValorUnitario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellValorUnitario.Weight = 0.5349361040168934D;
            // 
            // xrTableCellImporte
            // 
            this.xrTableCellImporte.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellImporte.Dpi = 100F;
            this.xrTableCellImporte.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellImporte.Name = "xrTableCellImporte";
            this.xrTableCellImporte.StylePriority.UseBorders = false;
            this.xrTableCellImporte.StylePriority.UseFont = false;
            this.xrTableCellImporte.StylePriority.UseTextAlignment = false;
            this.xrTableCellImporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellImporte.Weight = 0.53743794093290242D;
            // 
            // xrTableCellDescuento
            // 
            this.xrTableCellDescuento.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellDescuento.Dpi = 100F;
            this.xrTableCellDescuento.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellDescuento.Name = "xrTableCellDescuento";
            this.xrTableCellDescuento.StylePriority.UseBorders = false;
            this.xrTableCellDescuento.StylePriority.UseFont = false;
            this.xrTableCellDescuento.StylePriority.UseTextAlignment = false;
            this.xrTableCellDescuento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellDescuento.Weight = 0.53743794093290242D;
            // 
            // xrTableCellNoPedimento
            // 
            this.xrTableCellNoPedimento.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellNoPedimento.Dpi = 100F;
            this.xrTableCellNoPedimento.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellNoPedimento.Name = "xrTableCellNoPedimento";
            this.xrTableCellNoPedimento.StylePriority.UseBorders = false;
            this.xrTableCellNoPedimento.StylePriority.UseFont = false;
            this.xrTableCellNoPedimento.StylePriority.UseTextAlignment = false;
            this.xrTableCellNoPedimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellNoPedimento.Weight = 0.78722794323103418D;
            // 
            // xrTableCellNoCuentaPredial
            // 
            this.xrTableCellNoCuentaPredial.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellNoCuentaPredial.Dpi = 100F;
            this.xrTableCellNoCuentaPredial.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellNoCuentaPredial.Name = "xrTableCellNoCuentaPredial";
            this.xrTableCellNoCuentaPredial.StylePriority.UseBorders = false;
            this.xrTableCellNoCuentaPredial.StylePriority.UseFont = false;
            this.xrTableCellNoCuentaPredial.StylePriority.UseTextAlignment = false;
            this.xrTableCellNoCuentaPredial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellNoCuentaPredial.Weight = 0.93509957900332874D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 28F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 29F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelFolioFactura,
            this.xrLabel1,
            this.xrUsoCFDI,
            this.xrLabelUsoCFDI,
            this.xrRegimenFiscal,
            this.xrLabelRegimenFiscal,
            this.xrLabelNoCuentaPredial,
            this.xrLabelNoPedimento,
            this.xrLabelDescuento,
            this.xrConceptos,
            this.xrFolioFiscal,
            this.xrCPFechaHora,
            this.xrSerieCSD,
            this.xrLabelFolioFiscal,
            this.xrLabelCPFechaHora,
            this.xrLabelSerieCSD,
            this.xrEfectoComp,
            this.xrLabelEfectoComp,
            this.xrLabelUnidad,
            this.xrLabelValorUnitario,
            this.xrLabelReceptorNombre,
            this.xrReceptor,
            this.xrLabelImporte,
            this.xrLabelClaveUnidad,
            this.xrLabelCantidad,
            this.xrLabelIdentificacion,
            this.xrLabelCodigo,
            this.xrLabelEmisor,
            this.xrLabelRFCReceptor,
            this.xrLabelRFCEmisor,
            this.xrEmisor,
            this.xrRFCReceptor,
            this.xrRFCEmisor});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 201.8456F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrUsoCFDI
            // 
            this.xrUsoCFDI.Dpi = 100F;
            this.xrUsoCFDI.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrUsoCFDI.LocationFloat = new DevExpress.Utils.PointFloat(0F, 79F);
            this.xrUsoCFDI.Name = "xrUsoCFDI";
            this.xrUsoCFDI.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrUsoCFDI.SizeF = new System.Drawing.SizeF(112.44F, 14F);
            this.xrUsoCFDI.StylePriority.UseFont = false;
            this.xrUsoCFDI.Text = "Uso CFDI:";
            // 
            // xrLabelUsoCFDI
            // 
            this.xrLabelUsoCFDI.Dpi = 100F;
            this.xrLabelUsoCFDI.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelUsoCFDI.LocationFloat = new DevExpress.Utils.PointFloat(120.1405F, 79F);
            this.xrLabelUsoCFDI.Name = "xrLabelUsoCFDI";
            this.xrLabelUsoCFDI.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelUsoCFDI.SizeF = new System.Drawing.SizeF(246.5368F, 14F);
            this.xrLabelUsoCFDI.StylePriority.UseFont = false;
            this.xrLabelUsoCFDI.StylePriority.UseTextAlignment = false;
            this.xrLabelUsoCFDI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrRegimenFiscal
            // 
            this.xrRegimenFiscal.Dpi = 100F;
            this.xrRegimenFiscal.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrRegimenFiscal.LocationFloat = new DevExpress.Utils.PointFloat(378.7318F, 92.00001F);
            this.xrRegimenFiscal.Name = "xrRegimenFiscal";
            this.xrRegimenFiscal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRegimenFiscal.SizeF = new System.Drawing.SizeF(122.19F, 14F);
            this.xrRegimenFiscal.StylePriority.UseFont = false;
            this.xrRegimenFiscal.Text = "Régimen fiscal:";
            // 
            // xrLabelRegimenFiscal
            // 
            this.xrLabelRegimenFiscal.Dpi = 100F;
            this.xrLabelRegimenFiscal.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelRegimenFiscal.LocationFloat = new DevExpress.Utils.PointFloat(548.34F, 92F);
            this.xrLabelRegimenFiscal.Name = "xrLabelRegimenFiscal";
            this.xrLabelRegimenFiscal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelRegimenFiscal.SizeF = new System.Drawing.SizeF(245.6599F, 14F);
            this.xrLabelRegimenFiscal.StylePriority.UseFont = false;
            this.xrLabelRegimenFiscal.StylePriority.UseTextAlignment = false;
            this.xrLabelRegimenFiscal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelNoCuentaPredial
            // 
            this.xrLabelNoCuentaPredial.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelNoCuentaPredial.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabelNoCuentaPredial.Dpi = 100F;
            this.xrLabelNoCuentaPredial.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelNoCuentaPredial.LocationFloat = new DevExpress.Utils.PointFloat(678.1736F, 169.5073F);
            this.xrLabelNoCuentaPredial.Multiline = true;
            this.xrLabelNoCuentaPredial.Name = "xrLabelNoCuentaPredial";
            this.xrLabelNoCuentaPredial.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelNoCuentaPredial.SizeF = new System.Drawing.SizeF(115.8264F, 32.19002F);
            this.xrLabelNoCuentaPredial.StylePriority.UseBackColor = false;
            this.xrLabelNoCuentaPredial.StylePriority.UseBorders = false;
            this.xrLabelNoCuentaPredial.StylePriority.UseFont = false;
            this.xrLabelNoCuentaPredial.StylePriority.UseTextAlignment = false;
            this.xrLabelNoCuentaPredial.Text = "No. de cuenta preedial";
            this.xrLabelNoCuentaPredial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelNoPedimento
            // 
            this.xrLabelNoPedimento.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelNoPedimento.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabelNoPedimento.Dpi = 100F;
            this.xrLabelNoPedimento.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelNoPedimento.LocationFloat = new DevExpress.Utils.PointFloat(580.6636F, 169.5073F);
            this.xrLabelNoPedimento.Multiline = true;
            this.xrLabelNoPedimento.Name = "xrLabelNoPedimento";
            this.xrLabelNoPedimento.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelNoPedimento.SizeF = new System.Drawing.SizeF(97.51007F, 32.19F);
            this.xrLabelNoPedimento.StylePriority.UseBackColor = false;
            this.xrLabelNoPedimento.StylePriority.UseBorders = false;
            this.xrLabelNoPedimento.StylePriority.UseFont = false;
            this.xrLabelNoPedimento.StylePriority.UseTextAlignment = false;
            this.xrLabelNoPedimento.Text = "No. de pedimento";
            this.xrLabelNoPedimento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelDescuento
            // 
            this.xrLabelDescuento.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelDescuento.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabelDescuento.Dpi = 100F;
            this.xrLabelDescuento.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelDescuento.LocationFloat = new DevExpress.Utils.PointFloat(514.4034F, 169.5073F);
            this.xrLabelDescuento.Multiline = true;
            this.xrLabelDescuento.Name = "xrLabelDescuento";
            this.xrLabelDescuento.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDescuento.SizeF = new System.Drawing.SizeF(66.26F, 32.19F);
            this.xrLabelDescuento.StylePriority.UseBackColor = false;
            this.xrLabelDescuento.StylePriority.UseBorders = false;
            this.xrLabelDescuento.StylePriority.UseFont = false;
            this.xrLabelDescuento.StylePriority.UseTextAlignment = false;
            this.xrLabelDescuento.Text = "Descuento";
            this.xrLabelDescuento.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrConceptos
            // 
            this.xrConceptos.Dpi = 100F;
            this.xrConceptos.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.xrConceptos.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 125.8333F);
            this.xrConceptos.Name = "xrConceptos";
            this.xrConceptos.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrConceptos.SizeF = new System.Drawing.SizeF(84.31969F, 19.25002F);
            this.xrConceptos.StylePriority.UseFont = false;
            this.xrConceptos.Text = "Conceptos";
            // 
            // xrFolioFiscal
            // 
            this.xrFolioFiscal.Dpi = 100F;
            this.xrFolioFiscal.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrFolioFiscal.LocationFloat = new DevExpress.Utils.PointFloat(378.7318F, 0F);
            this.xrFolioFiscal.Name = "xrFolioFiscal";
            this.xrFolioFiscal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFolioFiscal.SizeF = new System.Drawing.SizeF(84.32F, 14F);
            this.xrFolioFiscal.StylePriority.UseFont = false;
            this.xrFolioFiscal.Text = "Folio fiscal:";
            // 
            // xrCPFechaHora
            // 
            this.xrCPFechaHora.Dpi = 100F;
            this.xrCPFechaHora.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrCPFechaHora.LocationFloat = new DevExpress.Utils.PointFloat(378.7318F, 39.99999F);
            this.xrCPFechaHora.Name = "xrCPFechaHora";
            this.xrCPFechaHora.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCPFechaHora.SizeF = new System.Drawing.SizeF(168.81F, 28F);
            this.xrCPFechaHora.StylePriority.UseFont = false;
            this.xrCPFechaHora.Text = "Código postal, fecha y hora de emisión:";
            // 
            // xrSerieCSD
            // 
            this.xrSerieCSD.Dpi = 100F;
            this.xrSerieCSD.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrSerieCSD.LocationFloat = new DevExpress.Utils.PointFloat(378.7318F, 20F);
            this.xrSerieCSD.Name = "xrSerieCSD";
            this.xrSerieCSD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSerieCSD.SizeF = new System.Drawing.SizeF(121.7682F, 14F);
            this.xrSerieCSD.StylePriority.UseFont = false;
            this.xrSerieCSD.Text = "No. de serie del CSD:";
            // 
            // xrLabelFolioFiscal
            // 
            this.xrLabelFolioFiscal.Dpi = 100F;
            this.xrLabelFolioFiscal.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelFolioFiscal.LocationFloat = new DevExpress.Utils.PointFloat(550.3638F, 0F);
            this.xrLabelFolioFiscal.Name = "xrLabelFolioFiscal";
            this.xrLabelFolioFiscal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFolioFiscal.SizeF = new System.Drawing.SizeF(243.6362F, 14F);
            this.xrLabelFolioFiscal.StylePriority.UseFont = false;
            // 
            // xrLabelCPFechaHora
            // 
            this.xrLabelCPFechaHora.Dpi = 100F;
            this.xrLabelCPFechaHora.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelCPFechaHora.LocationFloat = new DevExpress.Utils.PointFloat(550.3638F, 38.99998F);
            this.xrLabelCPFechaHora.Name = "xrLabelCPFechaHora";
            this.xrLabelCPFechaHora.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCPFechaHora.SizeF = new System.Drawing.SizeF(243.6362F, 14F);
            this.xrLabelCPFechaHora.StylePriority.UseFont = false;
            this.xrLabelCPFechaHora.StylePriority.UseTextAlignment = false;
            this.xrLabelCPFechaHora.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelSerieCSD
            // 
            this.xrLabelSerieCSD.Dpi = 100F;
            this.xrLabelSerieCSD.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelSerieCSD.LocationFloat = new DevExpress.Utils.PointFloat(550.3638F, 18.99999F);
            this.xrLabelSerieCSD.Name = "xrLabelSerieCSD";
            this.xrLabelSerieCSD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSerieCSD.SizeF = new System.Drawing.SizeF(243.6362F, 14F);
            this.xrLabelSerieCSD.StylePriority.UseFont = false;
            // 
            // xrEfectoComp
            // 
            this.xrEfectoComp.Dpi = 100F;
            this.xrEfectoComp.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrEfectoComp.LocationFloat = new DevExpress.Utils.PointFloat(378.7318F, 73F);
            this.xrEfectoComp.Name = "xrEfectoComp";
            this.xrEfectoComp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrEfectoComp.SizeF = new System.Drawing.SizeF(137.8149F, 14F);
            this.xrEfectoComp.StylePriority.UseFont = false;
            this.xrEfectoComp.Text = "Efecto de comprobante:";
            // 
            // xrLabelEfectoComp
            // 
            this.xrLabelEfectoComp.Dpi = 100F;
            this.xrLabelEfectoComp.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelEfectoComp.LocationFloat = new DevExpress.Utils.PointFloat(548.34F, 73F);
            this.xrLabelEfectoComp.Name = "xrLabelEfectoComp";
            this.xrLabelEfectoComp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelEfectoComp.SizeF = new System.Drawing.SizeF(245.6599F, 14F);
            this.xrLabelEfectoComp.StylePriority.UseFont = false;
            this.xrLabelEfectoComp.StylePriority.UseTextAlignment = false;
            this.xrLabelEfectoComp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelUnidad
            // 
            this.xrLabelUnidad.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelUnidad.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelUnidad.Dpi = 100F;
            this.xrLabelUnidad.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelUnidad.LocationFloat = new DevExpress.Utils.PointFloat(315.0089F, 169.6514F);
            this.xrLabelUnidad.Multiline = true;
            this.xrLabelUnidad.Name = "xrLabelUnidad";
            this.xrLabelUnidad.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelUnidad.SizeF = new System.Drawing.SizeF(66.25476F, 32.19421F);
            this.xrLabelUnidad.StylePriority.UseBackColor = false;
            this.xrLabelUnidad.StylePriority.UseBorders = false;
            this.xrLabelUnidad.StylePriority.UseFont = false;
            this.xrLabelUnidad.StylePriority.UseTextAlignment = false;
            this.xrLabelUnidad.Text = "Unidad";
            this.xrLabelUnidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelValorUnitario
            // 
            this.xrLabelValorUnitario.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelValorUnitario.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabelValorUnitario.Dpi = 100F;
            this.xrLabelValorUnitario.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelValorUnitario.LocationFloat = new DevExpress.Utils.PointFloat(381.2636F, 169.6514F);
            this.xrLabelValorUnitario.Multiline = true;
            this.xrLabelValorUnitario.Name = "xrLabelValorUnitario";
            this.xrLabelValorUnitario.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelValorUnitario.SizeF = new System.Drawing.SizeF(66.26F, 32.19F);
            this.xrLabelValorUnitario.StylePriority.UseBackColor = false;
            this.xrLabelValorUnitario.StylePriority.UseBorders = false;
            this.xrLabelValorUnitario.StylePriority.UseFont = false;
            this.xrLabelValorUnitario.StylePriority.UseTextAlignment = false;
            this.xrLabelValorUnitario.Text = "Valor unitario";
            this.xrLabelValorUnitario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelReceptorNombre
            // 
            this.xrLabelReceptorNombre.Dpi = 100F;
            this.xrLabelReceptorNombre.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelReceptorNombre.LocationFloat = new DevExpress.Utils.PointFloat(120.1405F, 60.00001F);
            this.xrLabelReceptorNombre.Name = "xrLabelReceptorNombre";
            this.xrLabelReceptorNombre.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelReceptorNombre.SizeF = new System.Drawing.SizeF(246.5368F, 14F);
            this.xrLabelReceptorNombre.StylePriority.UseFont = false;
            this.xrLabelReceptorNombre.StylePriority.UseTextAlignment = false;
            this.xrLabelReceptorNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrReceptor
            // 
            this.xrReceptor.Dpi = 100F;
            this.xrReceptor.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrReceptor.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60F);
            this.xrReceptor.Name = "xrReceptor";
            this.xrReceptor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrReceptor.SizeF = new System.Drawing.SizeF(112.44F, 14F);
            this.xrReceptor.StylePriority.UseFont = false;
            this.xrReceptor.Text = "Nombre receptor:";
            // 
            // xrLabelImporte
            // 
            this.xrLabelImporte.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelImporte.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabelImporte.Dpi = 100F;
            this.xrLabelImporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelImporte.LocationFloat = new DevExpress.Utils.PointFloat(447.95F, 169.6514F);
            this.xrLabelImporte.Multiline = true;
            this.xrLabelImporte.Name = "xrLabelImporte";
            this.xrLabelImporte.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImporte.SizeF = new System.Drawing.SizeF(66.26F, 32.19F);
            this.xrLabelImporte.StylePriority.UseBackColor = false;
            this.xrLabelImporte.StylePriority.UseBorders = false;
            this.xrLabelImporte.StylePriority.UseFont = false;
            this.xrLabelImporte.StylePriority.UseTextAlignment = false;
            this.xrLabelImporte.Text = "Importe";
            this.xrLabelImporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelClaveUnidad
            // 
            this.xrLabelClaveUnidad.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelClaveUnidad.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelClaveUnidad.Dpi = 100F;
            this.xrLabelClaveUnidad.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelClaveUnidad.LocationFloat = new DevExpress.Utils.PointFloat(254.0025F, 169.6472F);
            this.xrLabelClaveUnidad.Multiline = true;
            this.xrLabelClaveUnidad.Name = "xrLabelClaveUnidad";
            this.xrLabelClaveUnidad.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelClaveUnidad.SizeF = new System.Drawing.SizeF(61.00006F, 32.0546F);
            this.xrLabelClaveUnidad.StylePriority.UseBackColor = false;
            this.xrLabelClaveUnidad.StylePriority.UseBorders = false;
            this.xrLabelClaveUnidad.StylePriority.UseFont = false;
            this.xrLabelClaveUnidad.StylePriority.UseTextAlignment = false;
            this.xrLabelClaveUnidad.Text = "Clave de unidad";
            this.xrLabelClaveUnidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCantidad
            // 
            this.xrLabelCantidad.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelCantidad.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelCantidad.Dpi = 100F;
            this.xrLabelCantidad.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelCantidad.LocationFloat = new DevExpress.Utils.PointFloat(189.1614F, 169.6472F);
            this.xrLabelCantidad.Name = "xrLabelCantidad";
            this.xrLabelCantidad.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCantidad.SizeF = new System.Drawing.SizeF(64.84106F, 32.05452F);
            this.xrLabelCantidad.StylePriority.UseBackColor = false;
            this.xrLabelCantidad.StylePriority.UseBorderColor = false;
            this.xrLabelCantidad.StylePriority.UseBorders = false;
            this.xrLabelCantidad.StylePriority.UseFont = false;
            this.xrLabelCantidad.StylePriority.UseTextAlignment = false;
            this.xrLabelCantidad.Text = "Cantidad";
            this.xrLabelCantidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelIdentificacion
            // 
            this.xrLabelIdentificacion.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelIdentificacion.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelIdentificacion.Dpi = 100F;
            this.xrLabelIdentificacion.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelIdentificacion.LocationFloat = new DevExpress.Utils.PointFloat(95.77802F, 169.6471F);
            this.xrLabelIdentificacion.Name = "xrLabelIdentificacion";
            this.xrLabelIdentificacion.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelIdentificacion.SizeF = new System.Drawing.SizeF(93.3834F, 32.05461F);
            this.xrLabelIdentificacion.StylePriority.UseBackColor = false;
            this.xrLabelIdentificacion.StylePriority.UseBorders = false;
            this.xrLabelIdentificacion.StylePriority.UseFont = false;
            this.xrLabelIdentificacion.StylePriority.UseTextAlignment = false;
            this.xrLabelIdentificacion.Text = "No. identificación";
            this.xrLabelIdentificacion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCodigo
            // 
            this.xrLabelCodigo.BackColor = System.Drawing.Color.LightGray;
            this.xrLabelCodigo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelCodigo.Dpi = 100F;
            this.xrLabelCodigo.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelCodigo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 169.6471F);
            this.xrLabelCodigo.Name = "xrLabelCodigo";
            this.xrLabelCodigo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCodigo.SizeF = new System.Drawing.SizeF(95.77802F, 32.05461F);
            this.xrLabelCodigo.StylePriority.UseBackColor = false;
            this.xrLabelCodigo.StylePriority.UseBorders = false;
            this.xrLabelCodigo.StylePriority.UseFont = false;
            this.xrLabelCodigo.StylePriority.UseTextAlignment = false;
            this.xrLabelCodigo.Text = "Clave del producto y/o servicio";
            this.xrLabelCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelEmisor
            // 
            this.xrLabelEmisor.Dpi = 100F;
            this.xrLabelEmisor.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelEmisor.LocationFloat = new DevExpress.Utils.PointFloat(120.1406F, 20F);
            this.xrLabelEmisor.Name = "xrLabelEmisor";
            this.xrLabelEmisor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelEmisor.SizeF = new System.Drawing.SizeF(246.5366F, 14F);
            this.xrLabelEmisor.StylePriority.UseFont = false;
            // 
            // xrLabelRFCReceptor
            // 
            this.xrLabelRFCReceptor.Dpi = 100F;
            this.xrLabelRFCReceptor.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelRFCReceptor.LocationFloat = new DevExpress.Utils.PointFloat(120.1406F, 39.99999F);
            this.xrLabelRFCReceptor.Name = "xrLabelRFCReceptor";
            this.xrLabelRFCReceptor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelRFCReceptor.SizeF = new System.Drawing.SizeF(246.5366F, 14F);
            this.xrLabelRFCReceptor.StylePriority.UseFont = false;
            this.xrLabelRFCReceptor.StylePriority.UseTextAlignment = false;
            this.xrLabelRFCReceptor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelRFCEmisor
            // 
            this.xrLabelRFCEmisor.Dpi = 100F;
            this.xrLabelRFCEmisor.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelRFCEmisor.LocationFloat = new DevExpress.Utils.PointFloat(120.1406F, 0F);
            this.xrLabelRFCEmisor.Name = "xrLabelRFCEmisor";
            this.xrLabelRFCEmisor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelRFCEmisor.SizeF = new System.Drawing.SizeF(246.5366F, 14F);
            this.xrLabelRFCEmisor.StylePriority.UseFont = false;
            // 
            // xrEmisor
            // 
            this.xrEmisor.Dpi = 100F;
            this.xrEmisor.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrEmisor.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20F);
            this.xrEmisor.Name = "xrEmisor";
            this.xrEmisor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrEmisor.SizeF = new System.Drawing.SizeF(112.44F, 14F);
            this.xrEmisor.StylePriority.UseFont = false;
            this.xrEmisor.Text = "Nombre emisor:";
            // 
            // xrRFCReceptor
            // 
            this.xrRFCReceptor.Dpi = 100F;
            this.xrRFCReceptor.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrRFCReceptor.LocationFloat = new DevExpress.Utils.PointFloat(0F, 40F);
            this.xrRFCReceptor.Name = "xrRFCReceptor";
            this.xrRFCReceptor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRFCReceptor.SizeF = new System.Drawing.SizeF(112.44F, 14F);
            this.xrRFCReceptor.StylePriority.UseFont = false;
            this.xrRFCReceptor.Text = "RFC receptor:";
            // 
            // xrRFCEmisor
            // 
            this.xrRFCEmisor.Dpi = 100F;
            this.xrRFCEmisor.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrRFCEmisor.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrRFCEmisor.Name = "xrRFCEmisor";
            this.xrRFCEmisor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRFCEmisor.SizeF = new System.Drawing.SizeF(112.44F, 14F);
            this.xrRFCEmisor.StylePriority.UseFont = false;
            this.xrRFCEmisor.Text = "RFC emisor:";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrPageInfo2});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 23.95833F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(694F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo2.Format = "Este documento es una representación impresa de un CDFI";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(68.24036F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(594.6925F, 15F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelImpuestoRet02,
            this.xrLabelTipoRet02,
            this.xrLabelBaseRet02,
            this.xrLabelTipoFactorRet02,
            this.xrLabelTasaCuotaRet02,
            this.xrLabelImportRet02,
            this.xrLabelImpuestoRet,
            this.xrLabelTipoRet,
            this.xrLabelBaseRet,
            this.xrLabelTipoFactorRet,
            this.xrLabelTasaCuotaRet,
            this.xrLabelImportRet,
            this.xrBarCodeQR,
            this.xrLabel5,
            this.xrLabelFechaTimbrado,
            this.xrLabelCertificadoSAT,
            this.xrLabelRFCProvCert,
            this.xrLabel3,
            this.xrLabel4,
            this.xrCadenaSAT,
            this.xrLabelCadenaSAT,
            this.xrSelloSAT,
            this.xrLabelSelloSAT,
            this.xrLabelSelloCFDI,
            this.xrSelloCFDI,
            this.xrLabelTasaOCuota,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabelImpRetIVA,
            this.xrLabelTotal,
            this.xrTotal,
            this.xrLabelImpTras,
            this.xrLabelImpRetISR,
            this.xrLabelSubtotal,
            this.xrImpTras,
            this.xrImpRet,
            this.xrSubtotal,
            this.xrMoneda,
            this.xrMetodoPago,
            this.xrFormaPago,
            this.xrLabelMoneda,
            this.xrLabelMetodoPago,
            this.xrLabelFormaPago,
            this.xrTipoCambio,
            this.xrLabelTipoCambio,
            this.xrLabelImport,
            this.xrLabelTasaCuota,
            this.xrLabelTipoFactor,
            this.xrLabelBase,
            this.xrLabelTipo,
            this.xrLabelImpuesto,
            this.xrImporte,
            this.xrTasaCuota,
            this.xrTipoFactor,
            this.xrBase,
            this.xrTipo,
            this.xrImpuesto});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 403.125F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabelImpuestoRet02
            // 
            this.xrLabelImpuestoRet02.Dpi = 100F;
            this.xrLabelImpuestoRet02.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelImpuestoRet02.LocationFloat = new DevExpress.Utils.PointFloat(398.5236F, 58.92952F);
            this.xrLabelImpuestoRet02.Name = "xrLabelImpuestoRet02";
            this.xrLabelImpuestoRet02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImpuestoRet02.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelImpuestoRet02.StylePriority.UseFont = false;
            this.xrLabelImpuestoRet02.StylePriority.UseTextAlignment = false;
            this.xrLabelImpuestoRet02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTipoRet02
            // 
            this.xrLabelTipoRet02.Dpi = 100F;
            this.xrLabelTipoRet02.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTipoRet02.LocationFloat = new DevExpress.Utils.PointFloat(465.0935F, 58.92952F);
            this.xrLabelTipoRet02.Name = "xrLabelTipoRet02";
            this.xrLabelTipoRet02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipoRet02.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelTipoRet02.StylePriority.UseFont = false;
            this.xrLabelTipoRet02.StylePriority.UseTextAlignment = false;
            this.xrLabelTipoRet02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelBaseRet02
            // 
            this.xrLabelBaseRet02.Dpi = 100F;
            this.xrLabelBaseRet02.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelBaseRet02.LocationFloat = new DevExpress.Utils.PointFloat(529.5338F, 58.92952F);
            this.xrLabelBaseRet02.Name = "xrLabelBaseRet02";
            this.xrLabelBaseRet02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelBaseRet02.SizeF = new System.Drawing.SizeF(57F, 14F);
            this.xrLabelBaseRet02.StylePriority.UseFont = false;
            this.xrLabelBaseRet02.StylePriority.UseTextAlignment = false;
            this.xrLabelBaseRet02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTipoFactorRet02
            // 
            this.xrLabelTipoFactorRet02.Dpi = 100F;
            this.xrLabelTipoFactorRet02.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTipoFactorRet02.LocationFloat = new DevExpress.Utils.PointFloat(599.3167F, 58.92952F);
            this.xrLabelTipoFactorRet02.Name = "xrLabelTipoFactorRet02";
            this.xrLabelTipoFactorRet02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipoFactorRet02.SizeF = new System.Drawing.SizeF(34.32F, 14F);
            this.xrLabelTipoFactorRet02.StylePriority.UseFont = false;
            this.xrLabelTipoFactorRet02.StylePriority.UseTextAlignment = false;
            this.xrLabelTipoFactorRet02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTasaCuotaRet02
            // 
            this.xrLabelTasaCuotaRet02.Dpi = 100F;
            this.xrLabelTasaCuotaRet02.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTasaCuotaRet02.LocationFloat = new DevExpress.Utils.PointFloat(661.1003F, 58.92951F);
            this.xrLabelTasaCuotaRet02.Name = "xrLabelTasaCuotaRet02";
            this.xrLabelTasaCuotaRet02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTasaCuotaRet02.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelTasaCuotaRet02.StylePriority.UseFont = false;
            this.xrLabelTasaCuotaRet02.StylePriority.UseTextAlignment = false;
            this.xrLabelTasaCuotaRet02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelImportRet02
            // 
            this.xrLabelImportRet02.Dpi = 100F;
            this.xrLabelImportRet02.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelImportRet02.LocationFloat = new DevExpress.Utils.PointFloat(727F, 58.92951F);
            this.xrLabelImportRet02.Name = "xrLabelImportRet02";
            this.xrLabelImportRet02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImportRet02.SizeF = new System.Drawing.SizeF(57F, 14F);
            this.xrLabelImportRet02.StylePriority.UseFont = false;
            this.xrLabelImportRet02.StylePriority.UseTextAlignment = false;
            this.xrLabelImportRet02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabelImpuestoRet
            // 
            this.xrLabelImpuestoRet.Dpi = 100F;
            this.xrLabelImpuestoRet.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelImpuestoRet.LocationFloat = new DevExpress.Utils.PointFloat(398.5236F, 44.92952F);
            this.xrLabelImpuestoRet.Name = "xrLabelImpuestoRet";
            this.xrLabelImpuestoRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImpuestoRet.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelImpuestoRet.StylePriority.UseFont = false;
            this.xrLabelImpuestoRet.StylePriority.UseTextAlignment = false;
            this.xrLabelImpuestoRet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTipoRet
            // 
            this.xrLabelTipoRet.Dpi = 100F;
            this.xrLabelTipoRet.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTipoRet.LocationFloat = new DevExpress.Utils.PointFloat(465.0935F, 44.92952F);
            this.xrLabelTipoRet.Name = "xrLabelTipoRet";
            this.xrLabelTipoRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipoRet.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelTipoRet.StylePriority.UseFont = false;
            this.xrLabelTipoRet.StylePriority.UseTextAlignment = false;
            this.xrLabelTipoRet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelBaseRet
            // 
            this.xrLabelBaseRet.Dpi = 100F;
            this.xrLabelBaseRet.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelBaseRet.LocationFloat = new DevExpress.Utils.PointFloat(529.5338F, 44.92952F);
            this.xrLabelBaseRet.Name = "xrLabelBaseRet";
            this.xrLabelBaseRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelBaseRet.SizeF = new System.Drawing.SizeF(57F, 14F);
            this.xrLabelBaseRet.StylePriority.UseFont = false;
            this.xrLabelBaseRet.StylePriority.UseTextAlignment = false;
            this.xrLabelBaseRet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTipoFactorRet
            // 
            this.xrLabelTipoFactorRet.Dpi = 100F;
            this.xrLabelTipoFactorRet.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTipoFactorRet.LocationFloat = new DevExpress.Utils.PointFloat(599.3167F, 44.92952F);
            this.xrLabelTipoFactorRet.Name = "xrLabelTipoFactorRet";
            this.xrLabelTipoFactorRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipoFactorRet.SizeF = new System.Drawing.SizeF(34.32F, 14F);
            this.xrLabelTipoFactorRet.StylePriority.UseFont = false;
            this.xrLabelTipoFactorRet.StylePriority.UseTextAlignment = false;
            this.xrLabelTipoFactorRet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTasaCuotaRet
            // 
            this.xrLabelTasaCuotaRet.Dpi = 100F;
            this.xrLabelTasaCuotaRet.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTasaCuotaRet.LocationFloat = new DevExpress.Utils.PointFloat(661.1003F, 44.9295F);
            this.xrLabelTasaCuotaRet.Name = "xrLabelTasaCuotaRet";
            this.xrLabelTasaCuotaRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTasaCuotaRet.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelTasaCuotaRet.StylePriority.UseFont = false;
            this.xrLabelTasaCuotaRet.StylePriority.UseTextAlignment = false;
            this.xrLabelTasaCuotaRet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelImportRet
            // 
            this.xrLabelImportRet.Dpi = 100F;
            this.xrLabelImportRet.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelImportRet.LocationFloat = new DevExpress.Utils.PointFloat(727F, 44.9295F);
            this.xrLabelImportRet.Name = "xrLabelImportRet";
            this.xrLabelImportRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImportRet.SizeF = new System.Drawing.SizeF(57F, 14F);
            this.xrLabelImportRet.StylePriority.UseFont = false;
            this.xrLabelImportRet.StylePriority.UseTextAlignment = false;
            this.xrLabelImportRet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrBarCodeQR
            // 
            this.xrBarCodeQR.Alignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrBarCodeQR.AutoModule = true;
            this.xrBarCodeQR.BorderWidth = 0F;
            this.xrBarCodeQR.Dpi = 100F;
            this.xrBarCodeQR.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 299.1378F);
            this.xrBarCodeQR.Name = "xrBarCodeQR";
            this.xrBarCodeQR.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrBarCodeQR.ShowText = false;
            this.xrBarCodeQR.SizeF = new System.Drawing.SizeF(144.2883F, 89.5834F);
            this.xrBarCodeQR.StylePriority.UseBorderWidth = false;
            this.xrBarCodeQR.StylePriority.UsePadding = false;
            this.xrBarCodeQR.StylePriority.UseTextAlignment = false;
            qrCodeGenerator1.CompactionMode = DevExpress.XtraPrinting.BarCode.QRCodeCompactionMode.Byte;
            qrCodeGenerator1.ErrorCorrectionLevel = DevExpress.XtraPrinting.BarCode.QRCodeErrorCorrectionLevel.H;
            this.xrBarCodeQR.Symbology = qrCodeGenerator1;
            this.xrBarCodeQR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(514.0936F, 338.1778F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(148.8394F, 13F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Fecha y hora de certificacion:";
            // 
            // xrLabelFechaTimbrado
            // 
            this.xrLabelFechaTimbrado.Dpi = 100F;
            this.xrLabelFechaTimbrado.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelFechaTimbrado.LocationFloat = new DevExpress.Utils.PointFloat(677.8636F, 338.1779F);
            this.xrLabelFechaTimbrado.Name = "xrLabelFechaTimbrado";
            this.xrLabelFechaTimbrado.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFechaTimbrado.SizeF = new System.Drawing.SizeF(101.9236F, 13F);
            this.xrLabelFechaTimbrado.StylePriority.UseFont = false;
            // 
            // xrLabelCertificadoSAT
            // 
            this.xrLabelCertificadoSAT.Dpi = 100F;
            this.xrLabelCertificadoSAT.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelCertificadoSAT.LocationFloat = new DevExpress.Utils.PointFloat(337.3042F, 356.1778F);
            this.xrLabelCertificadoSAT.Name = "xrLabelCertificadoSAT";
            this.xrLabelCertificadoSAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCertificadoSAT.SizeF = new System.Drawing.SizeF(145.5392F, 13F);
            this.xrLabelCertificadoSAT.StylePriority.UseFont = false;
            // 
            // xrLabelRFCProvCert
            // 
            this.xrLabelRFCProvCert.Dpi = 100F;
            this.xrLabelRFCProvCert.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelRFCProvCert.LocationFloat = new DevExpress.Utils.PointFloat(337.3042F, 338.1778F);
            this.xrLabelRFCProvCert.Name = "xrLabelRFCProvCert";
            this.xrLabelRFCProvCert.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelRFCProvCert.SizeF = new System.Drawing.SizeF(79.87289F, 13F);
            this.xrLabelRFCProvCert.StylePriority.UseFont = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(144.2884F, 356.1778F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(170.7143F, 13F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "No. de serie del certificado SAT";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(144.2884F, 338.1778F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(170.7143F, 13F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "RFC del proveedor de certificacion:";
            // 
            // xrCadenaSAT
            // 
            this.xrCadenaSAT.Dpi = 100F;
            this.xrCadenaSAT.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCadenaSAT.LocationFloat = new DevExpress.Utils.PointFloat(144.2884F, 299.1378F);
            this.xrCadenaSAT.Name = "xrCadenaSAT";
            this.xrCadenaSAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCadenaSAT.SizeF = new System.Drawing.SizeF(318.7634F, 13F);
            this.xrCadenaSAT.StylePriority.UseFont = false;
            this.xrCadenaSAT.Text = "Cadena Original del complemento de certificacion digital del SAT";
            // 
            // xrLabelCadenaSAT
            // 
            this.xrLabelCadenaSAT.Dpi = 100F;
            this.xrLabelCadenaSAT.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelCadenaSAT.LocationFloat = new DevExpress.Utils.PointFloat(144.2884F, 312.1378F);
            this.xrLabelCadenaSAT.Name = "xrLabelCadenaSAT";
            this.xrLabelCadenaSAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCadenaSAT.SizeF = new System.Drawing.SizeF(649.7117F, 26.04001F);
            this.xrLabelCadenaSAT.StylePriority.UseFont = false;
            this.xrLabelCadenaSAT.StylePriority.UseTextAlignment = false;
            this.xrLabelCadenaSAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrSelloSAT
            // 
            this.xrSelloSAT.Dpi = 100F;
            this.xrSelloSAT.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSelloSAT.LocationFloat = new DevExpress.Utils.PointFloat(0F, 245.5128F);
            this.xrSelloSAT.Name = "xrSelloSAT";
            this.xrSelloSAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSelloSAT.SizeF = new System.Drawing.SizeF(112.4447F, 13F);
            this.xrSelloSAT.StylePriority.UseFont = false;
            this.xrSelloSAT.Text = "Sello digital del SAT:";
            // 
            // xrLabelSelloSAT
            // 
            this.xrLabelSelloSAT.Dpi = 100F;
            this.xrLabelSelloSAT.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelSelloSAT.LocationFloat = new DevExpress.Utils.PointFloat(0F, 258.5128F);
            this.xrLabelSelloSAT.Name = "xrLabelSelloSAT";
            this.xrLabelSelloSAT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSelloSAT.SizeF = new System.Drawing.SizeF(794F, 26.04173F);
            this.xrLabelSelloSAT.StylePriority.UseFont = false;
            this.xrLabelSelloSAT.StylePriority.UseTextAlignment = false;
            this.xrLabelSelloSAT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelSelloCFDI
            // 
            this.xrLabelSelloCFDI.Dpi = 100F;
            this.xrLabelSelloCFDI.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelSelloCFDI.LocationFloat = new DevExpress.Utils.PointFloat(0F, 202.2628F);
            this.xrLabelSelloCFDI.Name = "xrLabelSelloCFDI";
            this.xrLabelSelloCFDI.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSelloCFDI.SizeF = new System.Drawing.SizeF(794F, 26.04173F);
            this.xrLabelSelloCFDI.StylePriority.UseFont = false;
            this.xrLabelSelloCFDI.StylePriority.UseTextAlignment = false;
            this.xrLabelSelloCFDI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrSelloCFDI
            // 
            this.xrSelloCFDI.Dpi = 100F;
            this.xrSelloCFDI.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSelloCFDI.LocationFloat = new DevExpress.Utils.PointFloat(0F, 189.2628F);
            this.xrSelloCFDI.Name = "xrSelloCFDI";
            this.xrSelloCFDI.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSelloCFDI.SizeF = new System.Drawing.SizeF(112.4447F, 13F);
            this.xrSelloCFDI.StylePriority.UseFont = false;
            this.xrSelloCFDI.Text = "Sello digital del CFDI:";
            // 
            // xrLabelTasaOCuota
            // 
            this.xrLabelTasaOCuota.Dpi = 100F;
            this.xrLabelTasaOCuota.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelTasaOCuota.LocationFloat = new DevExpress.Utils.PointFloat(588.0735F, 107.4583F);
            this.xrLabelTasaOCuota.Name = "xrLabelTasaOCuota";
            this.xrLabelTasaOCuota.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTasaOCuota.SizeF = new System.Drawing.SizeF(62.43665F, 14F);
            this.xrLabelTasaOCuota.StylePriority.UseFont = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(550.3636F, 107.4583F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(28.17F, 14F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "IVA";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(550.3597F, 147.4583F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(28.17F, 14F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "IVA";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(550.3597F, 127.4583F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(28.17F, 14F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "ISR";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelImpRetIVA
            // 
            this.xrLabelImpRetIVA.Dpi = 100F;
            this.xrLabelImpRetIVA.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelImpRetIVA.LocationFloat = new DevExpress.Utils.PointFloat(661.2198F, 147.4583F);
            this.xrLabelImpRetIVA.Name = "xrLabelImpRetIVA";
            this.xrLabelImpRetIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImpRetIVA.SizeF = new System.Drawing.SizeF(118.69F, 14F);
            this.xrLabelImpRetIVA.StylePriority.UseFont = false;
            this.xrLabelImpRetIVA.StylePriority.UseTextAlignment = false;
            this.xrLabelImpRetIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabelTotal
            // 
            this.xrLabelTotal.Dpi = 100F;
            this.xrLabelTotal.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelTotal.LocationFloat = new DevExpress.Utils.PointFloat(661.2198F, 167.4583F);
            this.xrLabelTotal.Name = "xrLabelTotal";
            this.xrLabelTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTotal.SizeF = new System.Drawing.SizeF(118.69F, 14F);
            this.xrLabelTotal.StylePriority.UseFont = false;
            this.xrLabelTotal.StylePriority.UseTextAlignment = false;
            this.xrLabelTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTotal
            // 
            this.xrTotal.Dpi = 100F;
            this.xrTotal.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrTotal.LocationFloat = new DevExpress.Utils.PointFloat(398.6398F, 167.4583F);
            this.xrTotal.Name = "xrTotal";
            this.xrTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTotal.SizeF = new System.Drawing.SizeF(84.32F, 14F);
            this.xrTotal.StylePriority.UseFont = false;
            this.xrTotal.Text = "Total";
            // 
            // xrLabelImpTras
            // 
            this.xrLabelImpTras.Dpi = 100F;
            this.xrLabelImpTras.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelImpTras.LocationFloat = new DevExpress.Utils.PointFloat(661.2198F, 107.4583F);
            this.xrLabelImpTras.Name = "xrLabelImpTras";
            this.xrLabelImpTras.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImpTras.SizeF = new System.Drawing.SizeF(118.69F, 14F);
            this.xrLabelImpTras.StylePriority.UseFont = false;
            this.xrLabelImpTras.StylePriority.UseTextAlignment = false;
            this.xrLabelImpTras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabelImpRetISR
            // 
            this.xrLabelImpRetISR.Dpi = 100F;
            this.xrLabelImpRetISR.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelImpRetISR.LocationFloat = new DevExpress.Utils.PointFloat(661.2198F, 127.4583F);
            this.xrLabelImpRetISR.Name = "xrLabelImpRetISR";
            this.xrLabelImpRetISR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImpRetISR.SizeF = new System.Drawing.SizeF(118.69F, 14F);
            this.xrLabelImpRetISR.StylePriority.UseFont = false;
            this.xrLabelImpRetISR.StylePriority.UseTextAlignment = false;
            this.xrLabelImpRetISR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabelSubtotal
            // 
            this.xrLabelSubtotal.Dpi = 100F;
            this.xrLabelSubtotal.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(661.0997F, 87.45833F);
            this.xrLabelSubtotal.Name = "xrLabelSubtotal";
            this.xrLabelSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSubtotal.SizeF = new System.Drawing.SizeF(118.69F, 14F);
            this.xrLabelSubtotal.StylePriority.UseFont = false;
            this.xrLabelSubtotal.StylePriority.UseTextAlignment = false;
            this.xrLabelSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrImpTras
            // 
            this.xrImpTras.Dpi = 100F;
            this.xrImpTras.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrImpTras.LocationFloat = new DevExpress.Utils.PointFloat(398.6398F, 107.4583F);
            this.xrImpTras.Name = "xrImpTras";
            this.xrImpTras.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrImpTras.SizeF = new System.Drawing.SizeF(130.7649F, 14F);
            this.xrImpTras.StylePriority.UseFont = false;
            this.xrImpTras.Text = "Impuestos Trasladados";
            // 
            // xrImpRet
            // 
            this.xrImpRet.Dpi = 100F;
            this.xrImpRet.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrImpRet.LocationFloat = new DevExpress.Utils.PointFloat(398.6398F, 127.4583F);
            this.xrImpRet.Name = "xrImpRet";
            this.xrImpRet.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrImpRet.SizeF = new System.Drawing.SizeF(130.765F, 14F);
            this.xrImpRet.StylePriority.UseFont = false;
            this.xrImpRet.Text = "Impuestos retenidos";
            // 
            // xrSubtotal
            // 
            this.xrSubtotal.Dpi = 100F;
            this.xrSubtotal.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(398.5198F, 87.45833F);
            this.xrSubtotal.Name = "xrSubtotal";
            this.xrSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSubtotal.SizeF = new System.Drawing.SizeF(84.32F, 14F);
            this.xrSubtotal.StylePriority.UseFont = false;
            this.xrSubtotal.Text = "Subtotal";
            // 
            // xrMoneda
            // 
            this.xrMoneda.Dpi = 100F;
            this.xrMoneda.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrMoneda.LocationFloat = new DevExpress.Utils.PointFloat(0F, 87.45833F);
            this.xrMoneda.Name = "xrMoneda";
            this.xrMoneda.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrMoneda.SizeF = new System.Drawing.SizeF(84.32F, 14F);
            this.xrMoneda.StylePriority.UseFont = false;
            this.xrMoneda.Text = "Moneda:";
            // 
            // xrMetodoPago
            // 
            this.xrMetodoPago.Dpi = 100F;
            this.xrMetodoPago.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrMetodoPago.LocationFloat = new DevExpress.Utils.PointFloat(0F, 127.4583F);
            this.xrMetodoPago.Name = "xrMetodoPago";
            this.xrMetodoPago.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrMetodoPago.SizeF = new System.Drawing.SizeF(99.31005F, 14F);
            this.xrMetodoPago.StylePriority.UseFont = false;
            this.xrMetodoPago.Text = "Metodo de pago:";
            // 
            // xrFormaPago
            // 
            this.xrFormaPago.Dpi = 100F;
            this.xrFormaPago.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrFormaPago.LocationFloat = new DevExpress.Utils.PointFloat(0F, 107.4583F);
            this.xrFormaPago.Name = "xrFormaPago";
            this.xrFormaPago.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFormaPago.SizeF = new System.Drawing.SizeF(99.31005F, 14F);
            this.xrFormaPago.StylePriority.UseFont = false;
            this.xrFormaPago.Text = "Forma de pago:";
            // 
            // xrLabelMoneda
            // 
            this.xrLabelMoneda.Dpi = 100F;
            this.xrLabelMoneda.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelMoneda.LocationFloat = new DevExpress.Utils.PointFloat(120.1403F, 87.45833F);
            this.xrLabelMoneda.Name = "xrLabelMoneda";
            this.xrLabelMoneda.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelMoneda.SizeF = new System.Drawing.SizeF(261.1295F, 14F);
            this.xrLabelMoneda.StylePriority.UseFont = false;
            // 
            // xrLabelMetodoPago
            // 
            this.xrLabelMetodoPago.Dpi = 100F;
            this.xrLabelMetodoPago.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelMetodoPago.LocationFloat = new DevExpress.Utils.PointFloat(120.1403F, 127.4583F);
            this.xrLabelMetodoPago.Name = "xrLabelMetodoPago";
            this.xrLabelMetodoPago.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelMetodoPago.SizeF = new System.Drawing.SizeF(261.1296F, 14F);
            this.xrLabelMetodoPago.StylePriority.UseFont = false;
            this.xrLabelMetodoPago.StylePriority.UseTextAlignment = false;
            this.xrLabelMetodoPago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelFormaPago
            // 
            this.xrLabelFormaPago.Dpi = 100F;
            this.xrLabelFormaPago.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelFormaPago.LocationFloat = new DevExpress.Utils.PointFloat(120.1402F, 107.4583F);
            this.xrLabelFormaPago.Name = "xrLabelFormaPago";
            this.xrLabelFormaPago.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFormaPago.SizeF = new System.Drawing.SizeF(261.1296F, 14F);
            this.xrLabelFormaPago.StylePriority.UseFont = false;
            // 
            // xrTipoCambio
            // 
            this.xrTipoCambio.Dpi = 100F;
            this.xrTipoCambio.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrTipoCambio.LocationFloat = new DevExpress.Utils.PointFloat(0F, 147.4583F);
            this.xrTipoCambio.Name = "xrTipoCambio";
            this.xrTipoCambio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTipoCambio.SizeF = new System.Drawing.SizeF(99.31005F, 14F);
            this.xrTipoCambio.StylePriority.UseFont = false;
            this.xrTipoCambio.Text = "Tipo de cambio:";
            // 
            // xrLabelTipoCambio
            // 
            this.xrLabelTipoCambio.Dpi = 100F;
            this.xrLabelTipoCambio.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelTipoCambio.LocationFloat = new DevExpress.Utils.PointFloat(120.1403F, 147.4583F);
            this.xrLabelTipoCambio.Name = "xrLabelTipoCambio";
            this.xrLabelTipoCambio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipoCambio.SizeF = new System.Drawing.SizeF(261.1295F, 14F);
            this.xrLabelTipoCambio.StylePriority.UseFont = false;
            this.xrLabelTipoCambio.StylePriority.UseTextAlignment = false;
            this.xrLabelTipoCambio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelImport
            // 
            this.xrLabelImport.Dpi = 100F;
            this.xrLabelImport.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelImport.LocationFloat = new DevExpress.Utils.PointFloat(727F, 30.92953F);
            this.xrLabelImport.Name = "xrLabelImport";
            this.xrLabelImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImport.SizeF = new System.Drawing.SizeF(57F, 14F);
            this.xrLabelImport.StylePriority.UseFont = false;
            this.xrLabelImport.StylePriority.UseTextAlignment = false;
            this.xrLabelImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabelTasaCuota
            // 
            this.xrLabelTasaCuota.Dpi = 100F;
            this.xrLabelTasaCuota.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTasaCuota.LocationFloat = new DevExpress.Utils.PointFloat(661.1003F, 30.92953F);
            this.xrLabelTasaCuota.Name = "xrLabelTasaCuota";
            this.xrLabelTasaCuota.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTasaCuota.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelTasaCuota.StylePriority.UseFont = false;
            this.xrLabelTasaCuota.StylePriority.UseTextAlignment = false;
            this.xrLabelTasaCuota.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTipoFactor
            // 
            this.xrLabelTipoFactor.Dpi = 100F;
            this.xrLabelTipoFactor.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTipoFactor.LocationFloat = new DevExpress.Utils.PointFloat(599.3167F, 30.92953F);
            this.xrLabelTipoFactor.Name = "xrLabelTipoFactor";
            this.xrLabelTipoFactor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipoFactor.SizeF = new System.Drawing.SizeF(34.32F, 14F);
            this.xrLabelTipoFactor.StylePriority.UseFont = false;
            this.xrLabelTipoFactor.StylePriority.UseTextAlignment = false;
            this.xrLabelTipoFactor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelBase
            // 
            this.xrLabelBase.Dpi = 100F;
            this.xrLabelBase.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelBase.LocationFloat = new DevExpress.Utils.PointFloat(529.5338F, 30.92953F);
            this.xrLabelBase.Name = "xrLabelBase";
            this.xrLabelBase.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelBase.SizeF = new System.Drawing.SizeF(57F, 14F);
            this.xrLabelBase.StylePriority.UseFont = false;
            this.xrLabelBase.StylePriority.UseTextAlignment = false;
            this.xrLabelBase.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelTipo
            // 
            this.xrLabelTipo.Dpi = 100F;
            this.xrLabelTipo.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelTipo.LocationFloat = new DevExpress.Utils.PointFloat(465.0935F, 30.92953F);
            this.xrLabelTipo.Name = "xrLabelTipo";
            this.xrLabelTipo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTipo.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelTipo.StylePriority.UseFont = false;
            this.xrLabelTipo.StylePriority.UseTextAlignment = false;
            this.xrLabelTipo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelImpuesto
            // 
            this.xrLabelImpuesto.Dpi = 100F;
            this.xrLabelImpuesto.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabelImpuesto.LocationFloat = new DevExpress.Utils.PointFloat(398.5236F, 30.92953F);
            this.xrLabelImpuesto.Name = "xrLabelImpuesto";
            this.xrLabelImpuesto.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelImpuesto.SizeF = new System.Drawing.SizeF(49F, 14F);
            this.xrLabelImpuesto.StylePriority.UseFont = false;
            this.xrLabelImpuesto.StylePriority.UseTextAlignment = false;
            this.xrLabelImpuesto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrImporte
            // 
            this.xrImporte.Dpi = 100F;
            this.xrImporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrImporte.LocationFloat = new DevExpress.Utils.PointFloat(723.6318F, 7.000001F);
            this.xrImporte.Name = "xrImporte";
            this.xrImporte.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrImporte.SizeF = new System.Drawing.SizeF(40.56995F, 13.00001F);
            this.xrImporte.StylePriority.UseFont = false;
            this.xrImporte.StylePriority.UseTextAlignment = false;
            this.xrImporte.Text = "Importe";
            this.xrImporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTasaCuota
            // 
            this.xrTasaCuota.Dpi = 100F;
            this.xrTasaCuota.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTasaCuota.LocationFloat = new DevExpress.Utils.PointFloat(661.1003F, 0F);
            this.xrTasaCuota.Name = "xrTasaCuota";
            this.xrTasaCuota.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTasaCuota.SizeF = new System.Drawing.SizeF(38.48669F, 26.54168F);
            this.xrTasaCuota.StylePriority.UseFont = false;
            this.xrTasaCuota.StylePriority.UseTextAlignment = false;
            this.xrTasaCuota.Text = "Tasa o Cuota";
            this.xrTasaCuota.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTipoFactor
            // 
            this.xrTipoFactor.Dpi = 100F;
            this.xrTipoFactor.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTipoFactor.LocationFloat = new DevExpress.Utils.PointFloat(599.3167F, 0F);
            this.xrTipoFactor.Name = "xrTipoFactor";
            this.xrTipoFactor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTipoFactor.SizeF = new System.Drawing.SizeF(34.32007F, 26.54168F);
            this.xrTipoFactor.StylePriority.UseFont = false;
            this.xrTipoFactor.StylePriority.UseTextAlignment = false;
            this.xrTipoFactor.Text = "Tipo Factor";
            this.xrTipoFactor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrBase
            // 
            this.xrBase.Dpi = 100F;
            this.xrBase.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrBase.LocationFloat = new DevExpress.Utils.PointFloat(539.9504F, 7.000001F);
            this.xrBase.Name = "xrBase";
            this.xrBase.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrBase.SizeF = new System.Drawing.SizeF(28.16684F, 13F);
            this.xrBase.StylePriority.UseFont = false;
            this.xrBase.Text = "Base";
            // 
            // xrTipo
            // 
            this.xrTipo.Dpi = 100F;
            this.xrTipo.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTipo.LocationFloat = new DevExpress.Utils.PointFloat(475.5102F, 7.000001F);
            this.xrTipo.Name = "xrTipo";
            this.xrTipo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTipo.SizeF = new System.Drawing.SizeF(28.16684F, 13F);
            this.xrTipo.StylePriority.UseFont = false;
            this.xrTipo.Text = "Tipo";
            // 
            // xrImpuesto
            // 
            this.xrImpuesto.Dpi = 100F;
            this.xrImpuesto.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrImpuesto.LocationFloat = new DevExpress.Utils.PointFloat(398.5236F, 7.000001F);
            this.xrImpuesto.Name = "xrImpuesto";
            this.xrImpuesto.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrImpuesto.SizeF = new System.Drawing.SizeF(49F, 13F);
            this.xrImpuesto.StylePriority.UseFont = false;
            this.xrImpuesto.Text = "Impuesto";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 97F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(112.44F, 14F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Folio:";
            // 
            // xrLabelFolioFactura
            // 
            this.xrLabelFolioFactura.Dpi = 100F;
            this.xrLabelFolioFactura.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabelFolioFactura.LocationFloat = new DevExpress.Utils.PointFloat(120.1402F, 96.99999F);
            this.xrLabelFolioFactura.Name = "xrLabelFolioFactura";
            this.xrLabelFolioFactura.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFolioFactura.SizeF = new System.Drawing.SizeF(246.5368F, 14F);
            this.xrLabelFolioFactura.StylePriority.UseFont = false;
            this.xrLabelFolioFactura.StylePriority.UseTextAlignment = false;
            this.xrLabelFolioFactura.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // InvoicePDF
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.GroupFooter1});
            this.Margins = new System.Drawing.Printing.Margins(25, 31, 28, 29);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrRFCReceptor;
        private DevExpress.XtraReports.UI.XRLabel xrRFCEmisor;
        private DevExpress.XtraReports.UI.XRLabel xrEmisor;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelRFCEmisor;
        private DevExpress.XtraReports.UI.XRLabel xrLabelRFCReceptor;
        private DevExpress.XtraReports.UI.XRLabel xrLabelEmisor;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImporte;
        private DevExpress.XtraReports.UI.XRLabel xrLabelClaveUnidad;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCantidad;
        private DevExpress.XtraReports.UI.XRLabel xrLabelIdentificacion;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCodigo;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCodigo;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCantidad;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellClaveUnidad;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellImporte;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNoIdentificacion;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelReceptorNombre;
        private DevExpress.XtraReports.UI.XRLabel xrReceptor;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellValorUnitario;
        private DevExpress.XtraReports.UI.XRLabel xrLabelValorUnitario;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellUnidad;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUnidad;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRLabel xrFolioFiscal;
        private DevExpress.XtraReports.UI.XRLabel xrCPFechaHora;
        private DevExpress.XtraReports.UI.XRLabel xrSerieCSD;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFolioFiscal;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCPFechaHora;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSerieCSD;
        private DevExpress.XtraReports.UI.XRLabel xrEfectoComp;
        private DevExpress.XtraReports.UI.XRLabel xrLabelEfectoComp;
        private DevExpress.XtraReports.UI.XRLabel xrConceptos;
        private DevExpress.XtraReports.UI.XRLabel xrLabelNoPedimento;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDescuento;
        private DevExpress.XtraReports.UI.XRLabel xrLabelNoCuentaPredial;
        private DevExpress.XtraReports.UI.XRLabel xrImpuesto;
        private DevExpress.XtraReports.UI.XRLabel xrLabelRegimenFiscal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDescuento;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNoPedimento;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNoCuentaPredial;
        private DevExpress.XtraReports.UI.XRLabel xrTipoFactor;
        private DevExpress.XtraReports.UI.XRLabel xrBase;
        private DevExpress.XtraReports.UI.XRLabel xrTipo;
        private DevExpress.XtraReports.UI.XRLabel xrImporte;
        private DevExpress.XtraReports.UI.XRLabel xrTasaCuota;
        private DevExpress.XtraReports.UI.XRLabel xrRegimenFiscal;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImpuesto;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipo;
        private DevExpress.XtraReports.UI.XRLabel xrLabelBase;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImport;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTasaCuota;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipoFactor;
        private DevExpress.XtraReports.UI.XRLabel xrMoneda;
        private DevExpress.XtraReports.UI.XRLabel xrMetodoPago;
        private DevExpress.XtraReports.UI.XRLabel xrFormaPago;
        private DevExpress.XtraReports.UI.XRLabel xrLabelMoneda;
        private DevExpress.XtraReports.UI.XRLabel xrLabelMetodoPago;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFormaPago;
        private DevExpress.XtraReports.UI.XRLabel xrTipoCambio;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipoCambio;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTotal;
        private DevExpress.XtraReports.UI.XRLabel xrTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImpTras;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImpRetISR;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrImpTras;
        private DevExpress.XtraReports.UI.XRLabel xrImpRet;
        private DevExpress.XtraReports.UI.XRLabel xrSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImpRetIVA;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTasaOCuota;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrSelloCFDI;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSelloCFDI;
        private DevExpress.XtraReports.UI.XRLabel xrSelloSAT;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSelloSAT;
        private DevExpress.XtraReports.UI.XRLabel xrCadenaSAT;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCadenaSAT;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCertificadoSAT;
        private DevExpress.XtraReports.UI.XRLabel xrLabelRFCProvCert;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFechaTimbrado;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCodeQR;
        private DevExpress.XtraReports.UI.XRLabel xrUsoCFDI;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUsoCFDI;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImpuestoRet02;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipoRet02;
        private DevExpress.XtraReports.UI.XRLabel xrLabelBaseRet02;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipoFactorRet02;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTasaCuotaRet02;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImportRet02;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImpuestoRet;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipoRet;
        private DevExpress.XtraReports.UI.XRLabel xrLabelBaseRet;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTipoFactorRet;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTasaCuotaRet;
        private DevExpress.XtraReports.UI.XRLabel xrLabelImportRet;
        private DevExpress.XtraReports.UI.XRLabel xrDescripcion;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableDescripcion;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFolioFactura;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
