﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Expenses;
using System.Collections.Generic;
using DevExpress.Charts.Model;
using DevExpress.XtraCharts;
using System.Linq;

namespace FloridoERP.Report
{
    public partial class ExpensesReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableServices = null;
        public DataSet ds = null;
        public decimal total = 0;
        public ExpensesReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(string Site, string User, string Suppplier, string Category, DateTime? BeginDate, DateTime? EndDate, List<ExpensesReportModel> ExpensesReport, ModelChartExpenses modelChart)
        {
            try
            {
                DataSetTableServices = new DataSet();
                DataTable DataTableServices = new DataTable();
                DataSetTableServices.Tables.Add(DataTableServices);

                if (Suppplier != "")
                    xrSupplier.Text = Suppplier;
                else
                    xrSupplier.Text = "TODOS LOS PROVEEDORES.";
                if (Category != "")
                    xrCategory.Text = Category;
                else
                    xrCategory.Text = "TODAS LAS CATEGORIAS.";

                xrType.Text = "TODOS LOS GASTOS.";
                xrDateTimeNow.Text = BeginDate.HasValue ? BeginDate.Value.ToShortDateString() : "---";
                xrDateTimeEnd.Text = EndDate.HasValue ? EndDate.Value.ToShortDateString() : "---";
                xrStore.Text = Site;
                xrUser.Text = User;

                DataTableServices.Columns.Add(new DataColumn("xrTableCellFolio"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellSite"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellCategory"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellType"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellProvider"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellDateRequirement"));
                DataTableServices.Columns.Add(new DataColumn("xrTableCellTotal"));

                foreach (var item in ExpensesReport)
                {
                    DataRow DataRowOrdered = DataSetTableServices.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellFolio"] = item.ReferenceFolio;
                    DataRowOrdered["xrTableCellSite"] = item.SiteName;
                    DataRowOrdered["xrTableCellCategory"] = item.Category;
                    DataRowOrdered["xrTableCellType"] = item.ReferenceType;
                    DataRowOrdered["xrTableCellProvider"] = item.Supplier;
                    DataRowOrdered["xrTableCellDateRequirement"] = item.InvoiceDate.ToShortDateString();
                    DataRowOrdered["xrTableCellTotal"] = "$ " + item.TotalAmount;
                    DataSetTableServices.Tables[0].Rows.Add(DataRowOrdered);
                    total += item.TotalAmount;
                }
                xrTableTotal.Text = "$ " + total.ToString();
                xrTableCellFolio.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellFolio"));
                xrTableCellSite.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSite"));
                xrTableCellCategory.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCategory"));
                xrTableCellType.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellType"));
                xrTableCellProvider.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellProvider"));
                xrTableCellDateRequirement.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDateRequirement"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));
                ChartOnePrint(modelChart.CreditorsExpensive);
                ChartTwoPrint(modelChart.CreditorsExpensiveQuantity);
                ChartThreePrint(modelChart.CreditorsExpensiveReferenceType);
                return DataSetTableServices;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }

        public void ChartOnePrint(List<ExpensesChart> chart)
        {
            DashCaus.Series.Clear();
            DashCaus.Titles.Clear();

            //TITULO de la grafica---------------------------
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            chartTitle1.Text = "Total de Compras por Proveedor";
            chartTitle1.Font = new Font("Times New Roman", 14, FontStyle.Bold);
            chartTitle1.Alignment = StringAlignment.Center;
            chartTitle1.Dock = ChartTitleDockStyle.Top;
            DashCaus.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] { chartTitle1 });

            if (chart == null || chart.Count() < 1)
            {
                DevExpress.XtraCharts.ChartTitle ErrorTitu = new DevExpress.XtraCharts.ChartTitle();
                ErrorTitu.Text = "No existen datos para mostrar en este rango fechas determinado.";
                ErrorTitu.Font = new Font("Verdana", 10, FontStyle.Regular);
                ErrorTitu.Alignment = StringAlignment.Center;
                ErrorTitu.Dock = ChartTitleDockStyle.Bottom;
                DashCaus.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] { ErrorTitu });
            }
            else
            {
                Series series1 = new Series("Pie Series 1", ViewType.Pie);
                DashCaus.Series.Add(series1);

                foreach (var c in chart)
                {
                    series1.Points.Add(new SeriesPoint(c.ColumnString, c.Value));
                    ((PiePointOptions)series1.Label.PointOptions).PointView = PointView.ArgumentAndValues;
                    ((PiePointOptions)series1.LegendPointOptions).PointView = PointView.Values;
                    ((PiePointOptions)series1.LegendPointOptions).PercentOptions.ValueAsPercent = false;
                    series1.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.General;
                    series1.LegendPointOptions.Pattern = " {A}: {V} unidades.";
                }
            }
        }

        public void ChartTwoPrint(List<ExpensesChart> chart)
        {
            xrChart2.Series.Clear();
            xrChart2.Titles.Clear();

            //TITULO de la grafica---------------------------
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            chartTitle1.Text = "Total de Gasto por Proveedor";
            chartTitle1.Font = new Font("Times New Roman", 14, FontStyle.Bold);
            chartTitle1.Alignment = StringAlignment.Center;
            chartTitle1.Dock = ChartTitleDockStyle.Top;
            xrChart2.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] { chartTitle1 });

            if (chart == null || chart.Count() < 1)
            {
                DevExpress.XtraCharts.ChartTitle ErrorTitu = new DevExpress.XtraCharts.ChartTitle();
                ErrorTitu.Text = "No existen datos para mostrar en este rango fechas determinado.";
                ErrorTitu.Font = new Font("Arial", 14, FontStyle.Regular);
                ErrorTitu.Alignment = StringAlignment.Center;
                ErrorTitu.Dock = ChartTitleDockStyle.Bottom;
                xrChart2.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] { ErrorTitu });
            }
            else
            {
                Series series1 = new Series("Pie Series 1", ViewType.Pie);
                xrChart2.Series.Add(series1);

                foreach (var c in chart)
                {
                    series1.Points.Add(new SeriesPoint(c.ColumnString, c.Value));
                    ((PiePointOptions)series1.Label.PointOptions).PointView = PointView.ArgumentAndValues;
                    ((PiePointOptions)series1.LegendPointOptions).PointView = PointView.Values;
                    ((PiePointOptions)series1.LegendPointOptions).PercentOptions.ValueAsPercent = false;
                    series1.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.General;
                    series1.LegendPointOptions.Pattern = " {A}: ${V} MXN";
                }
            }
        }


        public void ChartThreePrint(List<ExpensesChart> chart)
        {
            xrChart3.Series.Clear();
            xrChart3.Titles.Clear();

            //TITULO de la grafica---------------------------
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            chartTitle1.Text = "Total de Tipos de Gasto";
            chartTitle1.Font = new Font("Times New Roman", 14, FontStyle.Bold);
            chartTitle1.Alignment = StringAlignment.Center;
            chartTitle1.Dock = ChartTitleDockStyle.Top;
            xrChart3.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] { chartTitle1 });

            if (chart == null || chart.Count() < 1)
            {
                DevExpress.XtraCharts.ChartTitle ErrorTitu = new DevExpress.XtraCharts.ChartTitle();
                ErrorTitu.Text = "No existen datos para mostrar en este rango fechas determinado.";
                ErrorTitu.Font = new Font("Arial", 10, FontStyle.Regular);
                ErrorTitu.Alignment = StringAlignment.Center;
                ErrorTitu.Dock = ChartTitleDockStyle.Bottom;
                xrChart3.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] { ErrorTitu });
            }
            else
            {
                Series series1 = new Series("Pie Series 1", ViewType.Pie3D);
                xrChart3.Series.Add(series1);

                foreach (var c in chart)
                {
                    series1.Points.Add(new SeriesPoint(c.ColumnString, c.Value));
                    ((PiePointOptions)series1.Label.PointOptions).PointView = PointView.ArgumentAndValues;
                    ((PiePointOptions)series1.LegendPointOptions).PointView = PointView.Values;
                    ((PiePointOptions)series1.LegendPointOptions).PercentOptions.ValueAsPercent = false;
                    series1.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.General;

                    // Adjust the view-type-specific options of the series. 
                    ((Pie3DSeriesView)series1.View).Depth = 30;
                    ((Pie3DSeriesView)series1.View).ExplodedPoints.Add(series1.Points[0]);
                    ((Pie3DSeriesView)series1.View).ExplodedDistancePercentage = 30;

                    // Access the diagram's options. 
                    ((SimpleDiagram3D)xrChart3.Diagram).RotationType = RotationType.UseAngles;
                    ((SimpleDiagram3D)xrChart3.Diagram).RotationAngleX = -35;
                    series1.LegendPointOptions.Pattern = " {A}: ${V} MXN";
                }
            }
        }
    }
}
