﻿using App.Entities.ViewModels.Projects;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class ProjectReport : DevExpress.XtraReports.UI.XtraReport
    {
        private DataSet DataSetTableOrdered = null;

        public ProjectReport()
        {
            InitializeComponent();
        }

        public DataSet PrintTable(List<ProjectModel> ProjectModel)
        {
            try
            {
                labelDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("Folio"));
                DataTableOrdered.Columns.Add(new DataColumn("Title"));
                DataTableOrdered.Columns.Add(new DataColumn("Category"));
                DataTableOrdered.Columns.Add(new DataColumn("Status"));
                DataTableOrdered.Columns.Add(new DataColumn("StartDate"));
                DataTableOrdered.Columns.Add(new DataColumn("EndDate"));
                DataTableOrdered.Columns.Add(new DataColumn("CompromiseDate"));
                DataTableOrdered.Columns.Add(new DataColumn("EstimateCost"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalCost"));

                foreach (var item in ProjectModel)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();

                    DataRowOrdered["Folio"] = item.ProjectId;
                    DataRowOrdered["Title"] = item.Title;
                    DataRowOrdered["Category"] = item.Category;
                    DataRowOrdered["Status"] = item.Status;
                    DataRowOrdered["StartDate"] = item.StartDate.HasValue ? item.StartDate.Value.Date.ToString("dd/MM/yyyy") : "";
                    DataRowOrdered["EndDate"] = item.EndDate.HasValue ? item.EndDate.Value.Date.ToString("dd/MM/yyyy") : "";
                    DataRowOrdered["CompromiseDate"] = item.CompromiseDate.ToString("dd/MM/yyyy");
                    DataRowOrdered["EstimateCost"] = "$" + item.EstimateCost;
                    DataRowOrdered["TotalCost"] = "$" + item.TotalCost;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                Folio.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Folio"));
                Title.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Title"));
                Category.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Category"));
                Status.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Status"));
                StartDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.StartDate"));
                EndDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.EndDate"));
                CompromiseDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CompromiseDate"));
                EstimateCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.EstimateCost"));
                TotalCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalCost"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }
    }
}
