﻿namespace FloridoERP.Report
{
    partial class FixedExpensesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FixedExpensesReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTablePurchase = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrCellPartNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellCostUnit = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellSubtotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellDiscount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellISRR = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIVAR = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellImport = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrlblUsdImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdISRR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIVAR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdDiscount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnDiscount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIVAR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnISRR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrlblExpenseExactly = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblExpenseExactlyShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblInvoiceDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblInvoiceDateShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCurrencyShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCurrency = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblFolioGeneral = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.supplierRFC = new DevExpress.XtraReports.UI.XRLabel();
            this.supplierRFCShow = new DevExpress.XtraReports.UI.XRLabel();
            this.supplierName = new DevExpress.XtraReports.UI.XRLabel();
            this.supplierNameShow = new DevExpress.XtraReports.UI.XRLabel();
            this.supAddrsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.supTelLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supEmailLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supDeparLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateInShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateIn = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.empLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRFC = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xraddress = new DevExpress.XtraReports.UI.XRLabel();
            this.xrState = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTypeShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIEPs = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCostUnit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblQuantity = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblPartNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrlblFolioInterno = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblFolioInternoShow = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 25F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 23.95833F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupFooter1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTablePurchase});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 50.09804F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTablePurchase
            // 
            this.xrTablePurchase.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTablePurchase.Dpi = 100F;
            this.xrTablePurchase.LocationFloat = new DevExpress.Utils.PointFloat(2.499978F, 0F);
            this.xrTablePurchase.Name = "xrTablePurchase";
            this.xrTablePurchase.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTablePurchase.SizeF = new System.Drawing.SizeF(1050.5F, 50.09804F);
            this.xrTablePurchase.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrCellPartNumber,
            this.xrCellDescription,
            this.xrCellQuantity,
            this.xrCellCostUnit,
            this.xrCellSubtotal,
            this.xrCellIVA,
            this.xrCellIEPS,
            this.xrCellDiscount,
            this.xrCellISRR,
            this.xrCellIVAR,
            this.xrCellImport});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrCellPartNumber
            // 
            this.xrCellPartNumber.Dpi = 100F;
            this.xrCellPartNumber.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellPartNumber.Name = "xrCellPartNumber";
            this.xrCellPartNumber.StylePriority.UseFont = false;
            this.xrCellPartNumber.StylePriority.UseTextAlignment = false;
            this.xrCellPartNumber.Text = "xrCellPartNumber";
            this.xrCellPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellPartNumber.Weight = 0.48728678369052936D;
            // 
            // xrCellDescription
            // 
            this.xrCellDescription.Dpi = 100F;
            this.xrCellDescription.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellDescription.Name = "xrCellDescription";
            this.xrCellDescription.StylePriority.UseFont = false;
            this.xrCellDescription.StylePriority.UseTextAlignment = false;
            this.xrCellDescription.Text = "xrCellDescription";
            this.xrCellDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellDescription.Weight = 1.4649191891692501D;
            // 
            // xrCellQuantity
            // 
            this.xrCellQuantity.Dpi = 100F;
            this.xrCellQuantity.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellQuantity.Name = "xrCellQuantity";
            this.xrCellQuantity.StylePriority.UseFont = false;
            this.xrCellQuantity.StylePriority.UseTextAlignment = false;
            this.xrCellQuantity.Text = "xrCellQuantity";
            this.xrCellQuantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellQuantity.Weight = 0.52687484823176889D;
            // 
            // xrCellCostUnit
            // 
            this.xrCellCostUnit.Dpi = 100F;
            this.xrCellCostUnit.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellCostUnit.Name = "xrCellCostUnit";
            this.xrCellCostUnit.StylePriority.UseFont = false;
            this.xrCellCostUnit.StylePriority.UseTextAlignment = false;
            this.xrCellCostUnit.Text = "xrCellCostUnit";
            this.xrCellCostUnit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellCostUnit.Weight = 0.62835506570742439D;
            // 
            // xrCellSubtotal
            // 
            this.xrCellSubtotal.Dpi = 100F;
            this.xrCellSubtotal.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellSubtotal.Name = "xrCellSubtotal";
            this.xrCellSubtotal.StylePriority.UseFont = false;
            this.xrCellSubtotal.StylePriority.UseTextAlignment = false;
            this.xrCellSubtotal.Text = "xrCellSubtotal";
            this.xrCellSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellSubtotal.Weight = 0.72138113931683545D;
            // 
            // xrCellIVA
            // 
            this.xrCellIVA.Dpi = 100F;
            this.xrCellIVA.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIVA.Name = "xrCellIVA";
            this.xrCellIVA.StylePriority.UseFont = false;
            this.xrCellIVA.StylePriority.UseTextAlignment = false;
            this.xrCellIVA.Text = "xrCellIVA";
            this.xrCellIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIVA.Weight = 0.73829267724448311D;
            // 
            // xrCellIEPS
            // 
            this.xrCellIEPS.Dpi = 100F;
            this.xrCellIEPS.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIEPS.Name = "xrCellIEPS";
            this.xrCellIEPS.StylePriority.UseFont = false;
            this.xrCellIEPS.StylePriority.UseTextAlignment = false;
            this.xrCellIEPS.Text = "xrCellIEPS";
            this.xrCellIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIEPS.Weight = 0.83977361793690009D;
            // 
            // xrCellDiscount
            // 
            this.xrCellDiscount.Dpi = 100F;
            this.xrCellDiscount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellDiscount.Name = "xrCellDiscount";
            this.xrCellDiscount.StylePriority.UseFont = false;
            this.xrCellDiscount.StylePriority.UseTextAlignment = false;
            this.xrCellDiscount.Text = "xrCellDiscount";
            this.xrCellDiscount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellDiscount.Weight = 0.72340751126015013D;
            // 
            // xrCellISRR
            // 
            this.xrCellISRR.Dpi = 100F;
            this.xrCellISRR.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellISRR.Name = "xrCellISRR";
            this.xrCellISRR.StylePriority.UseFont = false;
            this.xrCellISRR.StylePriority.UseTextAlignment = false;
            this.xrCellISRR.Text = "xrCellISRR";
            this.xrCellISRR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellISRR.Weight = 0.56069888180409955D;
            // 
            // xrCellIVAR
            // 
            this.xrCellIVAR.Dpi = 100F;
            this.xrCellIVAR.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIVAR.Name = "xrCellIVAR";
            this.xrCellIVAR.StylePriority.UseFont = false;
            this.xrCellIVAR.StylePriority.UseTextAlignment = false;
            this.xrCellIVAR.Text = "xrCellIVAR";
            this.xrCellIVAR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIVAR.Weight = 0.60475266510873382D;
            // 
            // xrCellImport
            // 
            this.xrCellImport.Dpi = 100F;
            this.xrCellImport.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellImport.Name = "xrCellImport";
            this.xrCellImport.StylePriority.UseFont = false;
            this.xrCellImport.StylePriority.UseTextAlignment = false;
            this.xrCellImport.Text = "xrCellImport";
            this.xrCellImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellImport.Weight = 0.744224995625924D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblUsdImport,
            this.xrLabel18,
            this.xrlblUsdISRR,
            this.xrLabel25,
            this.xrlblUsdIVAR,
            this.xrlblUsdDiscount,
            this.xrlblUsdIEPS,
            this.xrlblUsdIVA,
            this.xrlblUsdSubtotal,
            this.xrlblMxnSubtotal,
            this.xrlblMxnIVA,
            this.xrlblMxnIEPS,
            this.xrlblMxnDiscount,
            this.xrlblMxnImport,
            this.xrlblMxnIVAR,
            this.xrlblMxnISRR,
            this.xrLabel20,
            this.xrLabel19});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 46.00002F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrlblUsdImport
            // 
            this.xrlblUsdImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdImport.Dpi = 100F;
            this.xrlblUsdImport.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdImport.LocationFloat = new DevExpress.Utils.PointFloat(955.7596F, 23.00002F);
            this.xrlblUsdImport.Name = "xrlblUsdImport";
            this.xrlblUsdImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdImport.SizeF = new System.Drawing.SizeF(97.24042F, 23F);
            this.xrlblUsdImport.StylePriority.UseBorders = false;
            this.xrlblUsdImport.StylePriority.UseFont = false;
            this.xrlblUsdImport.StylePriority.UseTextAlignment = false;
            this.xrlblUsdImport.Text = "$ 0";
            this.xrlblUsdImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(62.52201F, 23.00002F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(345.9949F, 23F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "USD";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdISRR
            // 
            this.xrlblUsdISRR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdISRR.Dpi = 100F;
            this.xrlblUsdISRR.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdISRR.LocationFloat = new DevExpress.Utils.PointFloat(803.4821F, 23.00002F);
            this.xrlblUsdISRR.Name = "xrlblUsdISRR";
            this.xrlblUsdISRR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdISRR.SizeF = new System.Drawing.SizeF(73.26068F, 23F);
            this.xrlblUsdISRR.StylePriority.UseBorders = false;
            this.xrlblUsdISRR.StylePriority.UseFont = false;
            this.xrlblUsdISRR.StylePriority.UseTextAlignment = false;
            this.xrlblUsdISRR.Text = "$ 0";
            this.xrlblUsdISRR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(2.499978F, 23.00002F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(60.02223F, 23F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "TOTAL";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIVAR
            // 
            this.xrlblUsdIVAR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIVAR.Dpi = 100F;
            this.xrlblUsdIVAR.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIVAR.LocationFloat = new DevExpress.Utils.PointFloat(876.7427F, 23.00002F);
            this.xrlblUsdIVAR.Name = "xrlblUsdIVAR";
            this.xrlblUsdIVAR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIVAR.SizeF = new System.Drawing.SizeF(79.01678F, 23F);
            this.xrlblUsdIVAR.StylePriority.UseBorders = false;
            this.xrlblUsdIVAR.StylePriority.UseFont = false;
            this.xrlblUsdIVAR.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIVAR.Text = "$ 0";
            this.xrlblUsdIVAR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdDiscount
            // 
            this.xrlblUsdDiscount.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdDiscount.Dpi = 100F;
            this.xrlblUsdDiscount.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdDiscount.LocationFloat = new DevExpress.Utils.PointFloat(708.9619F, 23.00002F);
            this.xrlblUsdDiscount.Name = "xrlblUsdDiscount";
            this.xrlblUsdDiscount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdDiscount.SizeF = new System.Drawing.SizeF(94.5202F, 23F);
            this.xrlblUsdDiscount.StylePriority.UseBorders = false;
            this.xrlblUsdDiscount.StylePriority.UseFont = false;
            this.xrlblUsdDiscount.StylePriority.UseTextAlignment = false;
            this.xrlblUsdDiscount.Text = "$ 0";
            this.xrlblUsdDiscount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIEPS
            // 
            this.xrlblUsdIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIEPS.Dpi = 100F;
            this.xrlblUsdIEPS.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIEPS.LocationFloat = new DevExpress.Utils.PointFloat(599.2373F, 23.00002F);
            this.xrlblUsdIEPS.Name = "xrlblUsdIEPS";
            this.xrlblUsdIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIEPS.SizeF = new System.Drawing.SizeF(108.9881F, 23F);
            this.xrlblUsdIEPS.StylePriority.UseBorders = false;
            this.xrlblUsdIEPS.StylePriority.UseFont = false;
            this.xrlblUsdIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIEPS.Text = "$ 0";
            this.xrlblUsdIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIVA
            // 
            this.xrlblUsdIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIVA.Dpi = 100F;
            this.xrlblUsdIVA.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIVA.LocationFloat = new DevExpress.Utils.PointFloat(502.7722F, 23.00002F);
            this.xrlblUsdIVA.Name = "xrlblUsdIVA";
            this.xrlblUsdIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIVA.SizeF = new System.Drawing.SizeF(96.46509F, 23F);
            this.xrlblUsdIVA.StylePriority.UseBorders = false;
            this.xrlblUsdIVA.StylePriority.UseFont = false;
            this.xrlblUsdIVA.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIVA.Text = "$ 0";
            this.xrlblUsdIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdSubtotal
            // 
            this.xrlblUsdSubtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdSubtotal.Dpi = 100F;
            this.xrlblUsdSubtotal.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(408.5168F, 23.00002F);
            this.xrlblUsdSubtotal.Name = "xrlblUsdSubtotal";
            this.xrlblUsdSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdSubtotal.SizeF = new System.Drawing.SizeF(94.25528F, 23F);
            this.xrlblUsdSubtotal.StylePriority.UseBorders = false;
            this.xrlblUsdSubtotal.StylePriority.UseFont = false;
            this.xrlblUsdSubtotal.StylePriority.UseTextAlignment = false;
            this.xrlblUsdSubtotal.Text = "$ 0";
            this.xrlblUsdSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnSubtotal
            // 
            this.xrlblMxnSubtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnSubtotal.Dpi = 100F;
            this.xrlblMxnSubtotal.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(408.5168F, 0F);
            this.xrlblMxnSubtotal.Name = "xrlblMxnSubtotal";
            this.xrlblMxnSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnSubtotal.SizeF = new System.Drawing.SizeF(94.25516F, 23F);
            this.xrlblMxnSubtotal.StylePriority.UseBorders = false;
            this.xrlblMxnSubtotal.StylePriority.UseFont = false;
            this.xrlblMxnSubtotal.StylePriority.UseTextAlignment = false;
            this.xrlblMxnSubtotal.Text = "$ 0";
            this.xrlblMxnSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIVA
            // 
            this.xrlblMxnIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIVA.Dpi = 100F;
            this.xrlblMxnIVA.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIVA.LocationFloat = new DevExpress.Utils.PointFloat(502.7719F, 0F);
            this.xrlblMxnIVA.Name = "xrlblMxnIVA";
            this.xrlblMxnIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIVA.SizeF = new System.Drawing.SizeF(96.4653F, 23F);
            this.xrlblMxnIVA.StylePriority.UseBorders = false;
            this.xrlblMxnIVA.StylePriority.UseFont = false;
            this.xrlblMxnIVA.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIVA.Text = "$ 0";
            this.xrlblMxnIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIEPS
            // 
            this.xrlblMxnIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIEPS.Dpi = 100F;
            this.xrlblMxnIEPS.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIEPS.LocationFloat = new DevExpress.Utils.PointFloat(599.2373F, 0F);
            this.xrlblMxnIEPS.Name = "xrlblMxnIEPS";
            this.xrlblMxnIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIEPS.SizeF = new System.Drawing.SizeF(108.9882F, 23F);
            this.xrlblMxnIEPS.StylePriority.UseBorders = false;
            this.xrlblMxnIEPS.StylePriority.UseFont = false;
            this.xrlblMxnIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIEPS.Text = "$ 0";
            this.xrlblMxnIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnDiscount
            // 
            this.xrlblMxnDiscount.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnDiscount.Dpi = 100F;
            this.xrlblMxnDiscount.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnDiscount.LocationFloat = new DevExpress.Utils.PointFloat(708.2255F, 0F);
            this.xrlblMxnDiscount.Name = "xrlblMxnDiscount";
            this.xrlblMxnDiscount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnDiscount.SizeF = new System.Drawing.SizeF(95.25659F, 23F);
            this.xrlblMxnDiscount.StylePriority.UseBorders = false;
            this.xrlblMxnDiscount.StylePriority.UseFont = false;
            this.xrlblMxnDiscount.StylePriority.UseTextAlignment = false;
            this.xrlblMxnDiscount.Text = "$ 0";
            this.xrlblMxnDiscount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnImport
            // 
            this.xrlblMxnImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnImport.Dpi = 100F;
            this.xrlblMxnImport.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnImport.LocationFloat = new DevExpress.Utils.PointFloat(955.7596F, 0F);
            this.xrlblMxnImport.Name = "xrlblMxnImport";
            this.xrlblMxnImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnImport.SizeF = new System.Drawing.SizeF(97.24042F, 23F);
            this.xrlblMxnImport.StylePriority.UseBorders = false;
            this.xrlblMxnImport.StylePriority.UseFont = false;
            this.xrlblMxnImport.StylePriority.UseTextAlignment = false;
            this.xrlblMxnImport.Text = "$ 0";
            this.xrlblMxnImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIVAR
            // 
            this.xrlblMxnIVAR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIVAR.Dpi = 100F;
            this.xrlblMxnIVAR.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIVAR.LocationFloat = new DevExpress.Utils.PointFloat(876.7427F, 0F);
            this.xrlblMxnIVAR.Name = "xrlblMxnIVAR";
            this.xrlblMxnIVAR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIVAR.SizeF = new System.Drawing.SizeF(79.01678F, 23F);
            this.xrlblMxnIVAR.StylePriority.UseBorders = false;
            this.xrlblMxnIVAR.StylePriority.UseFont = false;
            this.xrlblMxnIVAR.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIVAR.Text = "$ 0";
            this.xrlblMxnIVAR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnISRR
            // 
            this.xrlblMxnISRR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnISRR.Dpi = 100F;
            this.xrlblMxnISRR.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnISRR.LocationFloat = new DevExpress.Utils.PointFloat(803.4821F, 0F);
            this.xrlblMxnISRR.Name = "xrlblMxnISRR";
            this.xrlblMxnISRR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnISRR.SizeF = new System.Drawing.SizeF(73.26068F, 23F);
            this.xrlblMxnISRR.StylePriority.UseBorders = false;
            this.xrlblMxnISRR.StylePriority.UseFont = false;
            this.xrlblMxnISRR.StylePriority.UseTextAlignment = false;
            this.xrlblMxnISRR.Text = "$ 0";
            this.xrlblMxnISRR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(62.52221F, 0F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(345.9947F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "MXN";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(2.499978F, 0F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(60.02223F, 23F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "TOTAL";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblFolioInterno,
            this.xrlblFolioInternoShow,
            this.xrlblExpenseExactly,
            this.xrlblExpenseExactlyShow,
            this.xrlblInvoiceDate,
            this.xrlblInvoiceDateShow,
            this.xrlblCurrencyShow,
            this.xrlblCurrency,
            this.xrlblFolioGeneral,
            this.xrLabel1,
            this.supplierRFC,
            this.supplierRFCShow,
            this.supplierName,
            this.supplierNameShow,
            this.supAddrsLabel,
            this.xrLabel14,
            this.xrLabel15,
            this.xrLabel16,
            this.supTelLabel,
            this.supEmailLabel,
            this.supDeparLabel,
            this.xrlblDateInShow,
            this.xrlblDateIn,
            this.xrPictureBox1,
            this.empLabel,
            this.xrLabel3,
            this.siteLabel,
            this.xrRFC,
            this.xrLabel5,
            this.xrLabel2,
            this.xraddress,
            this.xrState,
            this.xrlblType,
            this.xrlblTypeShow,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel4,
            this.xrlblSubtotal,
            this.xrlblIEPs,
            this.xrlblIVA,
            this.xrlblImport,
            this.xrlblCostUnit,
            this.xrlblQuantity,
            this.xrlblDescription,
            this.xrlblPartNumber});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 225.8361F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrlblExpenseExactly
            // 
            this.xrlblExpenseExactly.Dpi = 100F;
            this.xrlblExpenseExactly.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblExpenseExactly.LocationFloat = new DevExpress.Utils.PointFloat(26.38096F, 118.8774F);
            this.xrlblExpenseExactly.Name = "xrlblExpenseExactly";
            this.xrlblExpenseExactly.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblExpenseExactly.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrlblExpenseExactly.StylePriority.UseFont = false;
            this.xrlblExpenseExactly.Text = "Gasto:";
            // 
            // xrlblExpenseExactlyShow
            // 
            this.xrlblExpenseExactlyShow.Dpi = 100F;
            this.xrlblExpenseExactlyShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblExpenseExactlyShow.LocationFloat = new DevExpress.Utils.PointFloat(107.6529F, 118.8774F);
            this.xrlblExpenseExactlyShow.Name = "xrlblExpenseExactlyShow";
            this.xrlblExpenseExactlyShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblExpenseExactlyShow.SizeF = new System.Drawing.SizeF(351.5141F, 15F);
            this.xrlblExpenseExactlyShow.StylePriority.UseFont = false;
            // 
            // xrlblInvoiceDate
            // 
            this.xrlblInvoiceDate.Dpi = 100F;
            this.xrlblInvoiceDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblInvoiceDate.LocationFloat = new DevExpress.Utils.PointFloat(261.6056F, 167.7576F);
            this.xrlblInvoiceDate.Name = "xrlblInvoiceDate";
            this.xrlblInvoiceDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblInvoiceDate.SizeF = new System.Drawing.SizeF(73.35068F, 15F);
            this.xrlblInvoiceDate.StylePriority.UseFont = false;
            this.xrlblInvoiceDate.Text = "Fecha Factura:";
            // 
            // xrlblInvoiceDateShow
            // 
            this.xrlblInvoiceDateShow.Dpi = 100F;
            this.xrlblInvoiceDateShow.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblInvoiceDateShow.LocationFloat = new DevExpress.Utils.PointFloat(340.1647F, 167.7576F);
            this.xrlblInvoiceDateShow.Name = "xrlblInvoiceDateShow";
            this.xrlblInvoiceDateShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblInvoiceDateShow.SizeF = new System.Drawing.SizeF(119.0016F, 15F);
            this.xrlblInvoiceDateShow.StylePriority.UseFont = false;
            // 
            // xrlblCurrencyShow
            // 
            this.xrlblCurrencyShow.Dpi = 100F;
            this.xrlblCurrencyShow.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCurrencyShow.LocationFloat = new DevExpress.Utils.PointFloat(107.6531F, 167.7576F);
            this.xrlblCurrencyShow.Name = "xrlblCurrencyShow";
            this.xrlblCurrencyShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCurrencyShow.SizeF = new System.Drawing.SizeF(110.243F, 15F);
            this.xrlblCurrencyShow.StylePriority.UseFont = false;
            // 
            // xrlblCurrency
            // 
            this.xrlblCurrency.Dpi = 100F;
            this.xrlblCurrency.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCurrency.LocationFloat = new DevExpress.Utils.PointFloat(24.37485F, 167.7576F);
            this.xrlblCurrency.Name = "xrlblCurrency";
            this.xrlblCurrency.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCurrency.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrlblCurrency.StylePriority.UseFont = false;
            this.xrlblCurrency.Text = "Moneda:";
            // 
            // xrlblFolioGeneral
            // 
            this.xrlblFolioGeneral.Dpi = 100F;
            this.xrlblFolioGeneral.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblFolioGeneral.LocationFloat = new DevExpress.Utils.PointFloat(107.6519F, 152.7576F);
            this.xrlblFolioGeneral.Name = "xrlblFolioGeneral";
            this.xrlblFolioGeneral.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblFolioGeneral.SizeF = new System.Drawing.SizeF(351.5143F, 15F);
            this.xrlblFolioGeneral.StylePriority.UseFont = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(26.38117F, 152.7576F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(78.12499F, 15F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Factura:";
            // 
            // supplierRFC
            // 
            this.supplierRFC.Dpi = 100F;
            this.supplierRFC.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierRFC.LocationFloat = new DevExpress.Utils.PointFloat(638.9638F, 103.8774F);
            this.supplierRFC.Name = "supplierRFC";
            this.supplierRFC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supplierRFC.SizeF = new System.Drawing.SizeF(49.10321F, 15F);
            this.supplierRFC.StylePriority.UseFont = false;
            this.supplierRFC.Text = "RFC:";
            // 
            // supplierRFCShow
            // 
            this.supplierRFCShow.Dpi = 100F;
            this.supplierRFCShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierRFCShow.LocationFloat = new DevExpress.Utils.PointFloat(701.3188F, 103.8775F);
            this.supplierRFCShow.Name = "supplierRFCShow";
            this.supplierRFCShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supplierRFCShow.SizeF = new System.Drawing.SizeF(310.7156F, 15F);
            this.supplierRFCShow.StylePriority.UseFont = false;
            // 
            // supplierName
            // 
            this.supplierName.Dpi = 100F;
            this.supplierName.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierName.LocationFloat = new DevExpress.Utils.PointFloat(26.38117F, 182.7575F);
            this.supplierName.Name = "supplierName";
            this.supplierName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supplierName.SizeF = new System.Drawing.SizeF(69.2616F, 15F);
            this.supplierName.StylePriority.UseFont = false;
            this.supplierName.Text = "Proveedor:";
            // 
            // supplierNameShow
            // 
            this.supplierNameShow.Dpi = 100F;
            this.supplierNameShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierNameShow.LocationFloat = new DevExpress.Utils.PointFloat(107.6533F, 182.7575F);
            this.supplierNameShow.Name = "supplierNameShow";
            this.supplierNameShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supplierNameShow.SizeF = new System.Drawing.SizeF(351.513F, 14.99998F);
            this.supplierNameShow.StylePriority.UseFont = false;
            // 
            // supAddrsLabel
            // 
            this.supAddrsLabel.Dpi = 100F;
            this.supAddrsLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supAddrsLabel.LocationFloat = new DevExpress.Utils.PointFloat(638.9638F, 118.8775F);
            this.supAddrsLabel.Name = "supAddrsLabel";
            this.supAddrsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supAddrsLabel.SizeF = new System.Drawing.SizeF(373.0705F, 15F);
            this.supAddrsLabel.StylePriority.UseFont = false;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(638.9638F, 133.8774F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(33.33336F, 15F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Tel.:";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 100F;
            this.xrLabel15.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(638.9638F, 148.8775F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(33.33336F, 15F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Email:";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(638.9638F, 163.8774F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(69.26146F, 15F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Departamento:";
            // 
            // supTelLabel
            // 
            this.supTelLabel.Dpi = 100F;
            this.supTelLabel.Font = new System.Drawing.Font("Arial", 7F);
            this.supTelLabel.LocationFloat = new DevExpress.Utils.PointFloat(681.1603F, 133.8774F);
            this.supTelLabel.Name = "supTelLabel";
            this.supTelLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supTelLabel.SizeF = new System.Drawing.SizeF(330.8734F, 15.00003F);
            this.supTelLabel.StylePriority.UseFont = false;
            // 
            // supEmailLabel
            // 
            this.supEmailLabel.Dpi = 100F;
            this.supEmailLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supEmailLabel.LocationFloat = new DevExpress.Utils.PointFloat(681.1603F, 148.8775F);
            this.supEmailLabel.Name = "supEmailLabel";
            this.supEmailLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supEmailLabel.SizeF = new System.Drawing.SizeF(330.8734F, 15.00001F);
            this.supEmailLabel.StylePriority.UseFont = false;
            // 
            // supDeparLabel
            // 
            this.supDeparLabel.Dpi = 100F;
            this.supDeparLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supDeparLabel.LocationFloat = new DevExpress.Utils.PointFloat(724.3799F, 163.8777F);
            this.supDeparLabel.Name = "supDeparLabel";
            this.supDeparLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supDeparLabel.SizeF = new System.Drawing.SizeF(287.6541F, 15F);
            this.supDeparLabel.StylePriority.UseFont = false;
            // 
            // xrlblDateInShow
            // 
            this.xrlblDateInShow.Dpi = 100F;
            this.xrlblDateInShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateInShow.LocationFloat = new DevExpress.Utils.PointFloat(107.6533F, 137.7575F);
            this.xrlblDateInShow.Name = "xrlblDateInShow";
            this.xrlblDateInShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateInShow.SizeF = new System.Drawing.SizeF(351.5141F, 15F);
            this.xrlblDateInShow.StylePriority.UseFont = false;
            // 
            // xrlblDateIn
            // 
            this.xrlblDateIn.Dpi = 100F;
            this.xrlblDateIn.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateIn.LocationFloat = new DevExpress.Utils.PointFloat(24.37485F, 137.7575F);
            this.xrlblDateIn.Name = "xrlblDateIn";
            this.xrlblDateIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateIn.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrlblDateIn.StylePriority.UseFont = false;
            this.xrlblDateIn.Text = "Fecha Registro:";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(895.5704F, 19.99728F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(116.4632F, 75F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // empLabel
            // 
            this.empLabel.Dpi = 100F;
            this.empLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.empLabel.LocationFloat = new DevExpress.Utils.PointFloat(107.6529F, 69.99727F);
            this.empLabel.Name = "empLabel";
            this.empLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.empLabel.SizeF = new System.Drawing.SizeF(351.5143F, 15.00002F);
            this.empLabel.StylePriority.UseFont = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(652.8669F, 19.99728F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(225.3727F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(652.8669F, 34.99726F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(225.3727F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrRFC
            // 
            this.xrRFC.Dpi = 100F;
            this.xrRFC.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRFC.LocationFloat = new DevExpress.Utils.PointFloat(771.0146F, 49.99725F);
            this.xrRFC.Name = "xrRFC";
            this.xrRFC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRFC.SizeF = new System.Drawing.SizeF(107.2252F, 15F);
            this.xrRFC.StylePriority.UseFont = false;
            this.xrRFC.StylePriority.UseTextAlignment = false;
            this.xrRFC.Text = "DFL-950802-5N4";
            this.xrRFC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.BorderWidth = 4F;
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(125.7391F, 24.99732F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(209.2172F, 30F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseBorderWidth = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Reporte de Gastos";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(26.38096F, 69.99731F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Realizó:";
            // 
            // xraddress
            // 
            this.xraddress.Dpi = 100F;
            this.xraddress.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xraddress.LocationFloat = new DevExpress.Utils.PointFloat(652.8669F, 64.99728F);
            this.xraddress.Name = "xraddress";
            this.xraddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xraddress.SizeF = new System.Drawing.SizeF(225.3724F, 15F);
            this.xraddress.StylePriority.UseFont = false;
            this.xraddress.StylePriority.UseTextAlignment = false;
            this.xraddress.Text = "- Dirección";
            this.xraddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrState
            // 
            this.xrState.Dpi = 100F;
            this.xrState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrState.LocationFloat = new DevExpress.Utils.PointFloat(652.8669F, 79.9973F);
            this.xrState.Name = "xrState";
            this.xrState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrState.SizeF = new System.Drawing.SizeF(225.3727F, 15F);
            this.xrState.StylePriority.UseFont = false;
            this.xrState.StylePriority.UseTextAlignment = false;
            this.xrState.Text = "- Estado";
            this.xrState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlblType
            // 
            this.xrlblType.Dpi = 100F;
            this.xrlblType.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblType.LocationFloat = new DevExpress.Utils.PointFloat(26.38117F, 103.8774F);
            this.xrlblType.Name = "xrlblType";
            this.xrlblType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblType.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrlblType.StylePriority.UseFont = false;
            this.xrlblType.Text = "Tipo de Gasto:";
            // 
            // xrlblTypeShow
            // 
            this.xrlblTypeShow.Dpi = 100F;
            this.xrlblTypeShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTypeShow.LocationFloat = new DevExpress.Utils.PointFloat(107.6531F, 103.8774F);
            this.xrlblTypeShow.Name = "xrlblTypeShow";
            this.xrlblTypeShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTypeShow.SizeF = new System.Drawing.SizeF(351.5143F, 15F);
            this.xrlblTypeShow.StylePriority.UseFont = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(708.2254F, 202.836F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(95.25659F, 23F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Descuento";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(803.4819F, 202.836F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(73.26068F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "ISR Ret.";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(876.7427F, 202.836F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(79.01691F, 23F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "IVA Ret.";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblSubtotal
            // 
            this.xrlblSubtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblSubtotal.Dpi = 100F;
            this.xrlblSubtotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(955.7596F, 202.836F);
            this.xrlblSubtotal.Name = "xrlblSubtotal";
            this.xrlblSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSubtotal.SizeF = new System.Drawing.SizeF(94.74036F, 23F);
            this.xrlblSubtotal.StylePriority.UseBorders = false;
            this.xrlblSubtotal.StylePriority.UseFont = false;
            this.xrlblSubtotal.StylePriority.UseTextAlignment = false;
            this.xrlblSubtotal.Text = "Importe";
            this.xrlblSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIEPs
            // 
            this.xrlblIEPs.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIEPs.Dpi = 100F;
            this.xrlblIEPs.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIEPs.LocationFloat = new DevExpress.Utils.PointFloat(599.2372F, 202.836F);
            this.xrlblIEPs.Name = "xrlblIEPs";
            this.xrlblIEPs.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIEPs.SizeF = new System.Drawing.SizeF(108.9882F, 23F);
            this.xrlblIEPs.StylePriority.UseBorders = false;
            this.xrlblIEPs.StylePriority.UseFont = false;
            this.xrlblIEPs.StylePriority.UseTextAlignment = false;
            this.xrlblIEPs.Text = "IEPS";
            this.xrlblIEPs.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIVA
            // 
            this.xrlblIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIVA.Dpi = 100F;
            this.xrlblIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIVA.LocationFloat = new DevExpress.Utils.PointFloat(502.772F, 202.836F);
            this.xrlblIVA.Name = "xrlblIVA";
            this.xrlblIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIVA.SizeF = new System.Drawing.SizeF(96.46506F, 23F);
            this.xrlblIVA.StylePriority.UseBorders = false;
            this.xrlblIVA.StylePriority.UseFont = false;
            this.xrlblIVA.StylePriority.UseTextAlignment = false;
            this.xrlblIVA.Text = "IVA";
            this.xrlblIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblImport
            // 
            this.xrlblImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblImport.Dpi = 100F;
            this.xrlblImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblImport.LocationFloat = new DevExpress.Utils.PointFloat(408.5167F, 202.836F);
            this.xrlblImport.Name = "xrlblImport";
            this.xrlblImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblImport.SizeF = new System.Drawing.SizeF(94.25537F, 23F);
            this.xrlblImport.StylePriority.UseBorders = false;
            this.xrlblImport.StylePriority.UseFont = false;
            this.xrlblImport.StylePriority.UseTextAlignment = false;
            this.xrlblImport.Text = "Subtotal";
            this.xrlblImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblCostUnit
            // 
            this.xrlblCostUnit.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblCostUnit.Dpi = 100F;
            this.xrlblCostUnit.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCostUnit.LocationFloat = new DevExpress.Utils.PointFloat(326.416F, 202.836F);
            this.xrlblCostUnit.Name = "xrlblCostUnit";
            this.xrlblCostUnit.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCostUnit.SizeF = new System.Drawing.SizeF(82.10071F, 23F);
            this.xrlblCostUnit.StylePriority.UseBorders = false;
            this.xrlblCostUnit.StylePriority.UseFont = false;
            this.xrlblCostUnit.StylePriority.UseTextAlignment = false;
            this.xrlblCostUnit.Text = "Costo Unitario";
            this.xrlblCostUnit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblQuantity
            // 
            this.xrlblQuantity.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblQuantity.Dpi = 100F;
            this.xrlblQuantity.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblQuantity.LocationFloat = new DevExpress.Utils.PointFloat(257.5746F, 202.836F);
            this.xrlblQuantity.Name = "xrlblQuantity";
            this.xrlblQuantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblQuantity.SizeF = new System.Drawing.SizeF(68.84135F, 23F);
            this.xrlblQuantity.StylePriority.UseBorders = false;
            this.xrlblQuantity.StylePriority.UseFont = false;
            this.xrlblQuantity.StylePriority.UseTextAlignment = false;
            this.xrlblQuantity.Text = "Cantidad";
            this.xrlblQuantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDescription
            // 
            this.xrlblDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDescription.Dpi = 100F;
            this.xrlblDescription.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDescription.LocationFloat = new DevExpress.Utils.PointFloat(66.16885F, 202.836F);
            this.xrlblDescription.Name = "xrlblDescription";
            this.xrlblDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDescription.SizeF = new System.Drawing.SizeF(191.4059F, 23F);
            this.xrlblDescription.StylePriority.UseBorders = false;
            this.xrlblDescription.StylePriority.UseFont = false;
            this.xrlblDescription.StylePriority.UseTextAlignment = false;
            this.xrlblDescription.Text = "Descripción";
            this.xrlblDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblPartNumber
            // 
            this.xrlblPartNumber.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblPartNumber.Dpi = 100F;
            this.xrlblPartNumber.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblPartNumber.LocationFloat = new DevExpress.Utils.PointFloat(2.500026F, 202.836F);
            this.xrlblPartNumber.Name = "xrlblPartNumber";
            this.xrlblPartNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblPartNumber.SizeF = new System.Drawing.SizeF(63.66874F, 23F);
            this.xrlblPartNumber.StylePriority.UseBorders = false;
            this.xrlblPartNumber.StylePriority.UseFont = false;
            this.xrlblPartNumber.StylePriority.UseTextAlignment = false;
            this.xrlblPartNumber.Text = "Códgo";
            this.xrlblPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 28.54169F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrlblFolioInterno
            // 
            this.xrlblFolioInterno.Dpi = 100F;
            this.xrlblFolioInterno.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblFolioInterno.LocationFloat = new DevExpress.Utils.PointFloat(26.3799F, 84.99727F);
            this.xrlblFolioInterno.Name = "xrlblFolioInterno";
            this.xrlblFolioInterno.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblFolioInterno.SizeF = new System.Drawing.SizeF(78.12499F, 15F);
            this.xrlblFolioInterno.StylePriority.UseFont = false;
            this.xrlblFolioInterno.Text = "Folio:";
            // 
            // xrlblFolioInternoShow
            // 
            this.xrlblFolioInternoShow.Dpi = 100F;
            this.xrlblFolioInternoShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblFolioInternoShow.LocationFloat = new DevExpress.Utils.PointFloat(107.6533F, 84.99731F);
            this.xrlblFolioInternoShow.Name = "xrlblFolioInternoShow";
            this.xrlblFolioInternoShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblFolioInternoShow.SizeF = new System.Drawing.SizeF(351.5141F, 15F);
            this.xrlblFolioInternoShow.StylePriority.UseFont = false;
            // 
            // FixedExpensesReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.PageHeader,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(23, 24, 25, 24);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTablePurchase;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrCellPartNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrCellDescription;
        private DevExpress.XtraReports.UI.XRTableCell xrCellQuantity;
        private DevExpress.XtraReports.UI.XRTableCell xrCellCostUnit;
        private DevExpress.XtraReports.UI.XRTableCell xrCellSubtotal;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIVA;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrCellDiscount;
        private DevExpress.XtraReports.UI.XRTableCell xrCellISRR;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIVAR;
        private DevExpress.XtraReports.UI.XRTableCell xrCellImport;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdImport;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdISRR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIVAR;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdDiscount;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnDiscount;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIVAR;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnISRR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrlblExpenseExactly;
        private DevExpress.XtraReports.UI.XRLabel xrlblExpenseExactlyShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblInvoiceDate;
        private DevExpress.XtraReports.UI.XRLabel xrlblInvoiceDateShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblCurrencyShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblCurrency;
        private DevExpress.XtraReports.UI.XRLabel xrlblFolioGeneral;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel supplierRFC;
        private DevExpress.XtraReports.UI.XRLabel supplierRFCShow;
        private DevExpress.XtraReports.UI.XRLabel supplierName;
        private DevExpress.XtraReports.UI.XRLabel supplierNameShow;
        private DevExpress.XtraReports.UI.XRLabel supAddrsLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel supTelLabel;
        private DevExpress.XtraReports.UI.XRLabel supEmailLabel;
        private DevExpress.XtraReports.UI.XRLabel supDeparLabel;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateInShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateIn;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel empLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrRFC;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xraddress;
        private DevExpress.XtraReports.UI.XRLabel xrState;
        private DevExpress.XtraReports.UI.XRLabel xrlblType;
        private DevExpress.XtraReports.UI.XRLabel xrlblTypeShow;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrlblSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblIEPs;
        private DevExpress.XtraReports.UI.XRLabel xrlblIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblCostUnit;
        private DevExpress.XtraReports.UI.XRLabel xrlblQuantity;
        private DevExpress.XtraReports.UI.XRLabel xrlblDescription;
        private DevExpress.XtraReports.UI.XRLabel xrlblPartNumber;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrlblFolioInterno;
        private DevExpress.XtraReports.UI.XRLabel xrlblFolioInternoShow;
    }
}
