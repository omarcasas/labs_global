﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.ServicesCalendar;
using System.Collections.Generic;

namespace FloridoERP.Report
{
    public partial class MROReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        decimal Total = 0;
        public MROReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(ServicesCalendarModel Service, List<ServicesCalendarDetailModel> items)
        {
            try
            {

                xrLabelSite.Text = Service.site_name;
                xrLabelDocument.Text = Service.service_id.ToString();
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellMachinery"));

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSupplier"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCost"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCurrency"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDepartment"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellMachinery"] = item.equipment_description;
                    DataRowOrdered["xrTableCellSupplier"] = item.supplier_name ;
                    DataRowOrdered["xrTableCellCost"] = item.service_cost;
                    DataRowOrdered["xrTableCellCurrency"] = item.currency;
                    DataRowOrdered["xrTableCellDepartment"] = item.service_department;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    Total += item.service_cost;
                }

                xrTotal.Text = Total.ToString();
                xrTableCellMachinery.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellMachinery"));

                xrTableCellSupplier.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellSupplier"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCost"));
                xrTableCellCurrency.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCurrency"));
                xrTableCellDepartment.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellDepartment"));
              
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
        //public DataSet printTable(ServicesCalendarModel Service, List<ServicesCalendarDetailModel> items)
        //{
        //    try
        //    {
        //        DataSetTableOrdered = new DataSet();

        //        DataTable DataTableOrdered = new DataTable();
        //        DataSetTableOrdered.Tables.Add(DataTableOrdered);
        //        xrLabelSite.Text = Service.site_name;
        //        xrLabelDocument.Text= Service.service_id.ToString();
        //        DataTableOrdered.Columns.Add(new DataColumn("xrTableCellMachinery"));
        //        DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSupplier"));
        //        DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCost"));
        //        DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCurrency"));
        //        DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDepartment"));
        //        decimal total = 0;
        //        foreach (var item in items)
        //        {
        //            DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
        //            DataRowOrdered["xrTableCellMachinery"] = item.equipment_description;
        //            DataRowOrdered["xrTableCellSupplier"] = item.supplier_name;
        //            DataRowOrdered["xrTableCellCost"] = item.service_cost.ToString("C4");
        //            DataRowOrdered["xrTableCellCurrency"] = item.currency;
        //            DataRowOrdered["xrTableCellDepartment"] = item.service_department;
        //            total += item.service_cost;
        //            DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
        //        }
        //        xrTotal.Text = total.ToString();
        //        xrTableCellMachinery.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellMachinery"));
        //        xrTableCellSupplier.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellSupplier"));
        //        xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCost"));
        //        xrTableCellCurrency.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCurrency"));
        //        xrTableCellDepartment.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellDepartment"));

        //        return DataSetTableOrdered;
        //    }
        //    catch (Exception ex)
        //    {
        //        string d = ex.Message;
        //        throw;
        //    }
        //}
        //internal object printTable(object p, ServicesCalendarModel Service, List<ServicesCalendarDetailModel> items)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
