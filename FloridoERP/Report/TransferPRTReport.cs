﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.PRT;
using System.Data;
using System.Collections.Generic;

namespace FloridoERP.Report
{
    public partial class TransferPRTReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public TransferPRTReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<TransferItems> transferItems , string do_no , string purchase_no , string date , string site_name , string status)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelDate.Text = date;
                xrLabelTransfer.Text = do_no;
                xrLabelSite.Text = site_name;
                xrLabelPurchase.Text = purchase_no;
                xrLabelStatus.Text = status;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmount"));

                foreach (var item in transferItems)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.part_number;
                    DataRowOrdered["xrTableCellPartDescription"] = item.part_description;
                    DataRowOrdered["xrTableCellQuantity"] = item.quantity.ToString("N2");
                    DataRowOrdered["xrTableCellAmount"] = item.amount.ToString("C2");
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmount"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
