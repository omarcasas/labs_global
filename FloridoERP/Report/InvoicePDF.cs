﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using App.DAL;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.InternalRequirement;
using App.Entities.ViewModels.Site;
using App.Entities.ViewModels.Creditors;
using System.Collections.Generic;
using App.Entities;
using System.Text;
using System.Xml.Linq;
using System.Linq;
using DevExpress.XtraPrinting.BarCode;
using DevExpress.XtraEditors;
using DevExpress.CodeParser;

namespace FloridoERP.Report
{
    public partial class InvoicePDF : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;

        public InvoicePDF()
        {
            InitializeComponent();
        }

        public DataSet printTable(INVOICE_XML model)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                var xml = Encoding.UTF8.GetString(Convert.FromBase64String(model.xml));
                var doc = XDocument.Parse(xml);
                var root = doc.Root;

                var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                var Uuid = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("UUID");

                var CertificadoSAT = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("NoCertificadoSAT");

                var FechaTimbrado = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("FechaTimbrado");

                var RfcProvCertif = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("RfcProvCertif");

                var SelloCFDI = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("SelloCFD");

                var SelloSAT = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("SelloSAT");

                var Version = root?
                   .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                   .Attribute("Version");

                var Folio = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("Folio");
                if (Folio != null)
                    xrLabelFolioFactura.Text = Folio.Value;

                StringBuilder CadenaSAT = new StringBuilder();
                CadenaSAT.Append("||");
                if (Version != null)
                    CadenaSAT.Append(Version.Value);

                CadenaSAT.Append("|");
                if (Uuid != null)
                    CadenaSAT.Append(Uuid.Value);

                CadenaSAT.Append("|");
                if (FechaTimbrado != null)
                    CadenaSAT.Append(FechaTimbrado.Value);

                CadenaSAT.Append("|");
                if (RfcProvCertif != null)
                    CadenaSAT.Append(RfcProvCertif.Value);

                CadenaSAT.Append("|");
                if (SelloCFDI != null)
                    CadenaSAT.Append(SelloCFDI.Value);

                CadenaSAT.Append("|");
                if (CertificadoSAT != null)
                    CadenaSAT.Append(CertificadoSAT.Value);

                CadenaSAT.Append("||");

                var NoCertificado = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("NoCertificado");
                if (NoCertificado != null)
                    xrLabelSerieCSD.Text = NoCertificado.Value;

                var CP = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("LugarExpedicion");
                var Fecha = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("Fecha");
                if (CP != null && Fecha != null)
                    xrLabelCPFechaHora.Text = CP.Value + " " + Fecha.Value.Replace("T", " ");

                var Receptor = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Receptor").Attribute("Nombre");
                if (Receptor != null)
                    xrLabelReceptorNombre.Text = Receptor.Value;

                var RFCReceptor = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Receptor").Attribute("Rfc");
                if (RFCReceptor != null)
                    xrLabelRFCReceptor.Text = RFCReceptor.Value;

                var UsoCFDI = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Receptor").Attribute("UsoCFDI");
                if (UsoCFDI != null)
                {
                    switch (UsoCFDI.Value)
                    {
                        case "G01":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Adquisicion de Mercancias";
                            break;
                        case "G02":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Devoluciones, Descuentos o Bonificaciones";
                            break;
                        case "G03":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Gastos en General";
                            break;
                        case "I01":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Construcciones";
                            break;
                        case "I02":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Mobiliario y Equipo de Oficina por Inversiones";
                            break;
                        case "I03":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Equipo de Transporte";
                            break;
                        case "I04":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Equipo de Computo y Accesorios";
                            break;
                        case "I05":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Dados, Troqueles, Moldes, Matrices y Herramiental";
                            break;
                        case "I06":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Comunicaciones Telefonicas";
                            break;
                        case "I07":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Comunicaciones Satelitales";
                            break;
                        case "I08":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Otra Maquinaria y Equipo";
                            break;
                        case "D01":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Honorarios Medicos, Dentales y Gastos Hospitalarios";
                            break;
                        case "D02":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Gastos Medicos por Incapacidad o Discapacidad";
                            break;
                        case "D03":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Gastos Funerales";
                            break;
                        case "D04":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Donativos";
                            break;
                        case "D05":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Intereses Reales Efectivamente Pagados por Creditos Hipotecarios (Casa Habitacion)";
                            break;
                        case "D06":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Aportaciones Voluntarias al SAR";
                            break;
                        case "D07":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Primas por Seguros de Gastos Medicos";
                            break;
                        case "D08":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Gastos de Transportacion Escolar Obligatoria";
                            break;
                        case "D09":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Depositos en Cuentas para el Ahorro, Primas que tengan como Base Planes de Pensiones";
                            break;
                        case "D10":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Pagos por Servicios Educativos (Colegiaturas)";
                            break;
                        case "P01":
                            xrLabelUsoCFDI.Text = UsoCFDI.Value + " Por Definir";
                            break;
                        default:
                            xrLabelUsoCFDI.Text = "";
                            break;
                    };
                }

                var Emisor = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Emisor").Attribute("Nombre");
                if (Emisor != null)
                    xrLabelEmisor.Text = Emisor.Value;

                var RFCEmisor = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Emisor").Attribute("Rfc");
                if (RFCEmisor != null)
                    xrLabelRFCEmisor.Text = RFCEmisor.Value;

                var RegimenFiscal = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Emisor").Attribute("RegimenFiscal");
                if (RegimenFiscal != null)
                {
                    switch (RegimenFiscal.Value)
                    {
                        case "605":
                            xrLabelRegimenFiscal.Text = "Sueldos y Salarios e Ingresos Asimilados a Salarios";
                            break;
                        case "606":
                            xrLabelRegimenFiscal.Text = "Arrendamiento";
                            break;
                        case "608":
                            xrLabelRegimenFiscal.Text = "Demas Ingresos";
                            break;
                        case "611":
                            xrLabelRegimenFiscal.Text = "Ingresos por Dividendos (socios y accionistas)";
                            break;
                        case "612":
                            xrLabelRegimenFiscal.Text = "Personas Fisicas con Actividades Empresariales y Profesionales";
                            break;
                        case "614":
                            xrLabelRegimenFiscal.Text = "Ingresos por intereses";
                            break;
                        case "615":
                            xrLabelRegimenFiscal.Text = "Regimen de los ingresos por obtencion de premios";
                            break;
                        case "616":
                            xrLabelRegimenFiscal.Text = "Sin obligaciones fiscales";
                            break;
                        case "621":
                            xrLabelRegimenFiscal.Text = "Incorporacion Fiscal";
                            break;
                        case "622":
                            xrLabelRegimenFiscal.Text = "Actividades Agricolas, Ganaderas, Silvicolas y Pesqueras";
                            break;
                        case "629":
                            xrLabelRegimenFiscal.Text = "De los Regimenes Fiscales Preferentes y de las Empresas Multinacionales";
                            break;
                        case "630":
                            xrLabelRegimenFiscal.Text = "Enajenacion de acciones en bolsa de valores";
                            break;
                        case "601":
                            xrLabelRegimenFiscal.Text = "General de Ley Personas Morales";
                            break;
                        case "603":
                            xrLabelRegimenFiscal.Text = "Personas Morales con Fines no Lucrativos";
                            break;
                        case "607":
                            xrLabelRegimenFiscal.Text = "Regimen de Enajenacion o Adquisicion de Bienes";
                            break;
                        case "609":
                            xrLabelRegimenFiscal.Text = "Consolidacion";
                            break;
                        case "620":
                            xrLabelRegimenFiscal.Text = "Sociedades Corporativas de Produccion que optan por Diferir sus ingresos";
                            break;
                        case "623":
                            xrLabelRegimenFiscal.Text = "Opcional para Grupos de Sociedades";
                            break;
                        case "624":
                            xrLabelRegimenFiscal.Text = "Coordinados";
                            break;
                        case "628":
                            xrLabelRegimenFiscal.Text = "Hidrocarburos";
                            break;
                        default:
                            xrLabelRegimenFiscal.Text = "";
                            break;
                    };
                }

                var Moneda = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("Moneda");
                if (Moneda != null)
                {
                    if (Moneda.Value == "USD")
                    {
                        xrLabelMoneda.Text = "Dolar americano";

                        var TipoCambio = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("TipoCambio");
                        if (TipoCambio != null)
                            xrLabelTipoCambio.Text = TipoCambio.Value ?? "";
                    }
                    else if (Moneda.Value == "MXN")
                        xrLabelMoneda.Text = "Peso Mexicano";
                    else
                        xrLabelMoneda.Text = "Pesos";
                }

                var FormaPago = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("FormaPago");
                if (FormaPago != null)
                    FormaDePago(FormaPago.Value);

                var MetodoPago = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("MetodoPago");
                if (MetodoPago != null)
                    MetodoDePago(MetodoPago.Value);

                var TipoComprobante = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("TipoDeComprobante");
                if (TipoComprobante != null)
                {
                    if (TipoComprobante.Value == "I")
                        xrLabelEfectoComp.Text = "Ingresos";
                    else if (TipoComprobante.Value == "E")
                        xrLabelEfectoComp.Text = "Egresos";
                    else if (TipoComprobante.Value == "P")
                        xrLabelEfectoComp.Text = "Pagos";
                    else
                        xrLabelEfectoComp.Text = "";
                }

                var tasacuota = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Traslado");
                if (tasacuota != null)
                {
                    var TasaOCuota = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Traslado").Attribute("TasaOCuota");
                    if (TasaOCuota != null)
                        xrLabelTasaOCuota.Text = string.Format("{0:0.0000}", Convert.ToDecimal(TasaOCuota.Value) * 100).ToString() + "%";
                }

                var SubTotal = doc.Descendants().SingleOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("SubTotal");
                if (SubTotal != null)
                    xrLabelSubtotal.Text = "$ " + SubTotal.Value;

                var totalimptras = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos");
                if (totalimptras != null)
                {
                    var TotalImpuestosTrasladados = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Attribute("TotalImpuestosTrasladados");
                    if (TotalImpuestosTrasladados != null)
                        xrLabelImpTras.Text = "$ " + TotalImpuestosTrasladados.Value;
                }

                var Total = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Comprobante").Attribute("Total");
                if (Total != null)
                    xrLabelTotal.Text = "$ " + Total.Value;

                var imp = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos");
                if (imp != null)
                {
                    var impisr = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Descendants().FirstOrDefault(p => p.Name.LocalName == "Retencion");
                    if (impisr != null)
                    {
                        var ImpuestoISR = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Descendants().FirstOrDefault(p => p.Name.LocalName == "Retencion").Attribute("Impuesto");
                        if (ImpuestoISR != null)
                        {
                            if (ImpuestoISR.Value == "001")
                            {
                                var ImpRetISR = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Descendants().FirstOrDefault(p => p.Name.LocalName == "Retencion").Attribute("Importe");
                                if (ImpRetISR != null)
                                    xrLabelImpRetISR.Text = "$ " + ImpRetISR.Value;
                            }
                        }
                    }
                }

                var iva = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos");
                if (iva != null)
                {
                    var impiva = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Descendants().LastOrDefault(p => p.Name.LocalName == "Retencion");
                    if (impiva != null)
                    {
                        var ImpuestoIVA = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Descendants().LastOrDefault(p => p.Name.LocalName == "Retencion").Attribute("Impuesto");
                        if (ImpuestoIVA != null)
                        {
                            if (ImpuestoIVA.Value == "002")
                            {
                                var ImpRetIVA = doc.Descendants().LastOrDefault(p => p.Name.LocalName == "Impuestos").Descendants().LastOrDefault(p => p.Name.LocalName == "Retencion").Attribute("Importe");
                                if (ImpRetIVA != null)
                                    xrLabelImpRetIVA.Text = "$ " + ImpRetIVA.Value;
                            }
                        }
                    }
                }

                var traslado = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Traslados");
                if (traslado != null)
                {
                    var Traslado = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Traslados").Descendants().FirstOrDefault(p => p.Name.LocalName == "Traslado");
                    if (Traslado != null)
                    {
                        if (Traslado.Attribute("Impuesto").Value == "001")
                            xrLabelImpuesto.Text = "ISR";
                        else if (Traslado.Attribute("Impuesto").Value == "002")
                            xrLabelImpuesto.Text = "IVA";
                        else if (Traslado.Attribute("Impuesto").Value == "003")
                            xrLabelImpuesto.Text = "IEPS";

                        xrLabelTipo.Text = "Traslado";
                        xrLabelBase.Text = Traslado.Attribute("Base").Value ?? "";
                        xrLabelTipoFactor.Text = Traslado.Attribute("TipoFactor").Value ?? "";
                        xrLabelTasaCuota.Text = string.Format("{0:0.0000}", Convert.ToDecimal(Traslado.Attribute("TasaOCuota").Value) * 100).ToString() + "%";
                        xrLabelImport.Text = Traslado.Attribute("Importe").Value ?? "";
                    }
                }

                var ret = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Retenciones"); // .Descendants().FirstOrDefault(p => p.Name.LocalName == "Retencion");
                if (ret != null)
                {
                    var Retencion = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Retenciones").Descendants().FirstOrDefault(p => p.Name.LocalName == "Retencion");
                    if (Retencion != null)
                    {
                        if (Retencion.Attribute("Impuesto").Value == "001")
                            xrLabelImpuestoRet.Text = "ISR";
                        else if (Retencion.Attribute("Impuesto").Value == "002")
                            xrLabelImpuestoRet.Text = "IVA";
                        else if (Retencion.Attribute("Impuesto").Value == "003")
                            xrLabelImpuestoRet.Text = "IEPS";

                        xrLabelTipoRet.Text = "Retencion";
                        xrLabelBaseRet.Text = Retencion.Attribute("Base").Value;
                        xrLabelTipoFactorRet.Text = Retencion.Attribute("TipoFactor").Value;
                        xrLabelTasaCuotaRet.Text = string.Format("{0:0.0000}", Convert.ToDecimal(Retencion.Attribute("TasaOCuota").Value) * 100).ToString() + "%";
                        xrLabelImportRet.Text = Retencion.Attribute("Importe").Value;
                    }
                }

                var ret02 = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Retenciones"); // .Descendants().LastOrDefault(p => p.Name.LocalName == "Retencion");
                if (ret02 != null)
                {
                    var Retencion02 = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Retenciones").Descendants().LastOrDefault(p => p.Name.LocalName == "Retencion");
                    if (Retencion02 != null)
                    {
                        if (Retencion02.Attribute("Impuesto").Value == "001")
                            xrLabelImpuestoRet02.Text = "ISR";
                        else if (Retencion02.Attribute("Impuesto").Value == "002")
                            xrLabelImpuestoRet02.Text = "IVA";
                        else if (Retencion02.Attribute("Impuesto").Value == "003")
                            xrLabelImpuestoRet02.Text = "IEPS";

                        xrLabelTipoRet02.Text = "Retencion";
                        xrLabelBaseRet02.Text = Retencion02.Attribute("Base").Value;
                        xrLabelTipoFactorRet02.Text = Retencion02.Attribute("TipoFactor").Value;
                        xrLabelTasaCuotaRet02.Text = string.Format("{0:0.0000}", Convert.ToDecimal(Retencion02.Attribute("TasaOCuota").Value) * 100).ToString() + "%";
                        xrLabelImportRet02.Text = Retencion02.Attribute("Importe").Value;
                    }
                }

                if (Uuid != null)
                {
                    xrLabelFolioFiscal.Text = Uuid.Value;
                    xrBarCodeQR.Text = Uuid.Value;
                }

                if (CertificadoSAT != null)
                    xrLabelCertificadoSAT.Text = CertificadoSAT.Value;

                if (FechaTimbrado != null)
                    xrLabelFechaTimbrado.Text = FechaTimbrado.Value.Replace("T", " ");

                if (RfcProvCertif != null)
                    xrLabelRFCProvCert.Text = RfcProvCertif.Value;

                if (SelloCFDI != null)
                    xrLabelSelloCFDI.Text = SelloCFDI.Value;

                if (SelloSAT != null)
                    xrLabelSelloSAT.Text = SelloSAT.Value;

                if (CadenaSAT != null)
                    xrLabelCadenaSAT.Text = CadenaSAT.ToString();

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCodigo"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellNoIdentificacion"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCantidad"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellClaveUnidad"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellUnidad"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellValorUnitario"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellImporte"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDescuento"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellNoPedimento"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellNoCuentaPredial"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableDescripcion"));

                if (doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Conceptos").Elements() != null && doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Pagos") != null)
                {
                    foreach (XElement xe in doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Conceptos").Elements())
                    {
                        DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                        DataRowOrdered["xrTableDescripcion"] = xe.Attribute("Descripcion").Value ?? "";
                        DataRowOrdered["xrTableCellCodigo"] = xe.Attribute("ClaveProdServ").Value ?? "";

                        var noident = xe.Attribute("NoIdentificacion");
                        if (noident != null)
                            DataRowOrdered["xrTableCellNoIdentificacion"] = noident.Value ?? "";

                        var unidad = xe.Attribute("Unidad");
                        if (unidad != null)
                            DataRowOrdered["xrTableCellUnidad"] = unidad.Value ?? "";

                        DataRowOrdered["xrTableCellCantidad"] = xe.Attribute("Cantidad").Value ?? "";
                        DataRowOrdered["xrTableCellClaveUnidad"] = xe.Attribute("ClaveUnidad").Value ?? "";

                        //DataRowOrdered["xrTableCellValorUnitario"] = xe.Attribute("ValorUnitario").Value ?? "";
                        //DataRowOrdered["xrTableCellDescuento"] = "";
                        //DataRowOrdered["xrTableCellNoPedimento"] = "";
                        //DataRowOrdered["xrTableCellNoCuentaPredial"] = "";

                        var pago = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Pagos").Descendants().FirstOrDefault(p => p.Name.LocalName == "Pago").Attribute("Monto");
                        if (pago != null)
                        {
                            DataRowOrdered["xrTableCellImporte"] = pago.Value ?? "";
                            xrLabelTotal.Text = "$ " + pago.Value;
                        }

                        var formapago = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Pagos").Descendants().FirstOrDefault(p => p.Name.LocalName == "Pago").Attribute("FormaDePagoP");
                        if (formapago != null)
                            FormaDePago(formapago.Value);

                        var metodopago = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Pagos").Descendants().FirstOrDefault(p => p.Name.LocalName == "Pago").Descendants().FirstOrDefault(p => p.Name.LocalName == "DoctoRelacionado").Attribute("MetodoDePagoDR");
                        if (metodopago != null)
                            MetodoDePago(metodopago.Value);

                        DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    }
                }
                else if (doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Conceptos").Elements() != null)
                {
                    foreach (XElement xe in doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Conceptos").Elements())
                    {

                        DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                        DataRowOrdered["xrTableDescripcion"] = xe.Attribute("Descripcion").Value ?? "";
                        DataRowOrdered["xrTableCellCodigo"] = xe.Attribute("ClaveProdServ").Value ?? "";

                        var noident = xe.Attribute("NoIdentificacion");
                        if (noident != null)
                            DataRowOrdered["xrTableCellNoIdentificacion"] = noident.Value ?? "";

                        var unidad = xe.Attribute("Unidad");
                        if (unidad != null)
                            DataRowOrdered["xrTableCellUnidad"] = unidad.Value ?? "";

                        DataRowOrdered["xrTableCellCantidad"] = xe.Attribute("Cantidad").Value ?? "";
                        DataRowOrdered["xrTableCellClaveUnidad"] = xe.Attribute("ClaveUnidad").Value ?? "";
                        DataRowOrdered["xrTableCellValorUnitario"] = xe.Attribute("ValorUnitario").Value ?? "";
                        DataRowOrdered["xrTableCellImporte"] = xe.Attribute("Importe").Value ?? "";
                        //DataRowOrdered["xrTableCellDescuento"] = "";
                        //DataRowOrdered["xrTableCellNoPedimento"] = "";
                        //DataRowOrdered["xrTableCellNoCuentaPredial"] = "";
                        DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    }
                }

                xrTableCellCodigo.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCodigo"));
                xrTableCellNoIdentificacion.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellNoIdentificacion"));
                xrTableCellCantidad.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCantidad"));
                xrTableCellClaveUnidad.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellClaveUnidad"));
                xrTableCellUnidad.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellUnidad"));
                xrTableCellValorUnitario.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellValorUnitario"));
                xrTableCellImporte.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellImporte"));
                xrTableCellDescuento.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDescuento"));
                xrTableCellNoPedimento.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellNoPedimento"));
                xrTableCellNoCuentaPredial.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellNoCuentaPredial"));
                xrTableDescripcion.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableDescripcion"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

        private void MetodoDePago(string MetodoPago)
        {
            if (MetodoPago == "PUE")
                xrLabelMetodoPago.Text = MetodoPago + " Pago en una sola exhibición";
            else if (MetodoPago == "PPD")
                xrLabelMetodoPago.Text = MetodoPago + " Pago en parcialidades o diferido";
            else
                xrLabelMetodoPago.Text = MetodoPago;
        }

        private void FormaDePago(string FormaPago)
        {
            switch (FormaPago)
            {
                case "01":
                    xrLabelFormaPago.Text = FormaPago + " Efectivo";
                    break;
                case "02":
                    xrLabelFormaPago.Text = FormaPago + " Cheque Nominativo";
                    break;
                case "03":
                    xrLabelFormaPago.Text = FormaPago + " Transferencia electrónica de fondos";
                    break;
                case "04":
                    xrLabelFormaPago.Text = FormaPago + " Tarjeta de crédito";
                    break;
                case "05":
                    xrLabelFormaPago.Text = FormaPago + " Monedero electrónico";
                    break;
                case "06":
                    xrLabelFormaPago.Text = FormaPago + " Dinero electrónico";
                    break;
                case "08":
                    xrLabelFormaPago.Text = FormaPago + " Vales de despensa";
                    break;
                case "12":
                    xrLabelFormaPago.Text = FormaPago + " Dación en pago";
                    break;
                case "13":
                    xrLabelFormaPago.Text = FormaPago + " Pago por subrogación";
                    break;
                case "14":
                    xrLabelFormaPago.Text = FormaPago + " Pago por consignación";
                    break;
                case "15":
                    xrLabelFormaPago.Text = FormaPago + " Condonación";
                    break;
                case "17":
                    xrLabelFormaPago.Text = FormaPago + " Compensación";
                    break;
                case "23":
                    xrLabelFormaPago.Text = FormaPago + " Novación";
                    break;
                case "24":
                    xrLabelFormaPago.Text = FormaPago + " Confisión";
                    break;
                case "25":
                    xrLabelFormaPago.Text = FormaPago + " Remisión de deuda";
                    break;
                case "26":
                    xrLabelFormaPago.Text = FormaPago + " Prescripción o caducidad";
                    break;
                case "27":
                    xrLabelFormaPago.Text = FormaPago + " A satisfacción del acreedor";
                    break;
                case "28":
                    xrLabelFormaPago.Text = FormaPago + " Tarjeta de débito";
                    break;
                case "29":
                    xrLabelFormaPago.Text = FormaPago + " Tarjeta de servicios";
                    break;
                case "30":
                    xrLabelFormaPago.Text = FormaPago + " Aplicación de anticipos";
                    break;
                case "31":
                    xrLabelFormaPago.Text = FormaPago + " Intermediario pagos";
                    break;
                case "99":
                    xrLabelFormaPago.Text = FormaPago + " Por definir";
                    break;
                default:
                    xrLabelFormaPago.Text = FormaPago;
                    break;
            };
        }
    }
}