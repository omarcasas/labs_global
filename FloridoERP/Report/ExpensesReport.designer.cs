﻿namespace FloridoERP.Report
{
    partial class ExpensesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExpensesReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellFolio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellSite = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellCategory = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellProvider = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDateRequirement = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableFolio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStore = new DevExpress.XtraReports.UI.XRLabel();
            this.xrStore = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateTimeStart = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTimeNow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabelTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSite = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelProvider = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDateRequirement = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCategory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUser = new DevExpress.XtraReports.UI.XRLabel();
            this.DashCaus = new DevExpress.XtraReports.UI.XRChart();
            this.xrlblDateTimeEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTimeEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCategory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCategory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart2 = new DevExpress.XtraReports.UI.XRChart();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrChart3 = new DevExpress.XtraReports.UI.XRChart();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DashCaus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(748.9999F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellFolio,
            this.xrTableCellSite,
            this.xrTableCellCategory,
            this.xrTableCellType,
            this.xrTableCellProvider,
            this.xrTableCellDateRequirement,
            this.xrTableCellTotal});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCellFolio
            // 
            this.xrTableCellFolio.Dpi = 100F;
            this.xrTableCellFolio.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellFolio.Name = "xrTableCellFolio";
            this.xrTableCellFolio.StylePriority.UseFont = false;
            this.xrTableCellFolio.StylePriority.UseTextAlignment = false;
            this.xrTableCellFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellFolio.Weight = 0.7818583184270449D;
            // 
            // xrTableCellSite
            // 
            this.xrTableCellSite.Dpi = 100F;
            this.xrTableCellSite.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellSite.Name = "xrTableCellSite";
            this.xrTableCellSite.StylePriority.UseFont = false;
            this.xrTableCellSite.StylePriority.UseTextAlignment = false;
            this.xrTableCellSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellSite.Weight = 1.3854169270257926D;
            // 
            // xrTableCellCategory
            // 
            this.xrTableCellCategory.Dpi = 100F;
            this.xrTableCellCategory.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellCategory.Name = "xrTableCellCategory";
            this.xrTableCellCategory.StylePriority.UseFont = false;
            this.xrTableCellCategory.StylePriority.UseTextAlignment = false;
            this.xrTableCellCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellCategory.Weight = 0.88304852506852072D;
            // 
            // xrTableCellType
            // 
            this.xrTableCellType.Dpi = 100F;
            this.xrTableCellType.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellType.Name = "xrTableCellType";
            this.xrTableCellType.StylePriority.UseFont = false;
            this.xrTableCellType.StylePriority.UseTextAlignment = false;
            this.xrTableCellType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellType.Weight = 1.0438285353096819D;
            // 
            // xrTableCellProvider
            // 
            this.xrTableCellProvider.Dpi = 100F;
            this.xrTableCellProvider.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellProvider.Name = "xrTableCellProvider";
            this.xrTableCellProvider.StylePriority.UseFont = false;
            this.xrTableCellProvider.StylePriority.UseTextAlignment = false;
            this.xrTableCellProvider.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellProvider.Weight = 1.0620971585772496D;
            // 
            // xrTableCellDateRequirement
            // 
            this.xrTableCellDateRequirement.Dpi = 100F;
            this.xrTableCellDateRequirement.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellDateRequirement.Name = "xrTableCellDateRequirement";
            this.xrTableCellDateRequirement.StylePriority.UseFont = false;
            this.xrTableCellDateRequirement.StylePriority.UseTextAlignment = false;
            this.xrTableCellDateRequirement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellDateRequirement.Weight = 1.3545818854726703D;
            // 
            // xrTableCellTotal
            // 
            this.xrTableCellTotal.Dpi = 100F;
            this.xrTableCellTotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellTotal.Name = "xrTableCellTotal";
            this.xrTableCellTotal.StylePriority.UseFont = false;
            this.xrTableCellTotal.StylePriority.UseTextAlignment = false;
            this.xrTableCellTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellTotal.Weight = 0.97916706279477506D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 31F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 27.12501F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 26.13635F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(649F, 10.00001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrTableTotal
            // 
            this.xrTableTotal.Dpi = 100F;
            this.xrTableTotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableTotal.Name = "xrTableTotal";
            this.xrTableTotal.StylePriority.UseFont = false;
            this.xrTableTotal.StylePriority.UseTextAlignment = false;
            this.xrTableTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableTotal.Weight = 0.97916706279477506D;
            // 
            // xrTableFolio
            // 
            this.xrTableFolio.Dpi = 100F;
            this.xrTableFolio.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableFolio.Name = "xrTableFolio";
            this.xrTableFolio.StylePriority.UseFont = false;
            this.xrTableFolio.StylePriority.UseTextAlignment = false;
            this.xrTableFolio.Text = "Total";
            this.xrTableFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableFolio.Weight = 1.3445801019213564D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableFolio,
            this.xrTableTotal});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(516.6251F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(232.3747F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 27.16667F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(91.18595F, 73.16666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrTitle
            // 
            this.xrTitle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTitle.Dpi = 100F;
            this.xrTitle.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTitle.LocationFloat = new DevExpress.Utils.PointFloat(333.2678F, 0F);
            this.xrTitle.Name = "xrTitle";
            this.xrTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTitle.SizeF = new System.Drawing.SizeF(195.8333F, 27.16667F);
            this.xrTitle.StylePriority.UseBorderDashStyle = false;
            this.xrTitle.StylePriority.UseFont = false;
            this.xrTitle.Text = "Reporte de Gastos.";
            // 
            // xrlblStore
            // 
            this.xrlblStore.Dpi = 100F;
            this.xrlblStore.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStore.LocationFloat = new DevExpress.Utils.PointFloat(102.4999F, 27.16667F);
            this.xrlblStore.Name = "xrlblStore";
            this.xrlblStore.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStore.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblStore.StylePriority.UseFont = false;
            this.xrlblStore.StylePriority.UseTextAlignment = false;
            this.xrlblStore.Text = "Tienda:";
            this.xrlblStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrStore
            // 
            this.xrStore.Dpi = 100F;
            this.xrStore.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrStore.LocationFloat = new DevExpress.Utils.PointFloat(201.4166F, 27.16667F);
            this.xrStore.Name = "xrStore";
            this.xrStore.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrStore.SizeF = new System.Drawing.SizeF(546.441F, 23F);
            this.xrStore.StylePriority.UseFont = false;
            this.xrStore.StylePriority.UseTextAlignment = false;
            this.xrStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblDateTimeStart
            // 
            this.xrlblDateTimeStart.Dpi = 100F;
            this.xrlblDateTimeStart.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateTimeStart.LocationFloat = new DevExpress.Utils.PointFloat(102.4999F, 73.16666F);
            this.xrlblDateTimeStart.Name = "xrlblDateTimeStart";
            this.xrlblDateTimeStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateTimeStart.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblDateTimeStart.StylePriority.UseFont = false;
            this.xrlblDateTimeStart.StylePriority.UseTextAlignment = false;
            this.xrlblDateTimeStart.Text = "Fecha Inicio:";
            this.xrlblDateTimeStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDateTimeNow
            // 
            this.xrDateTimeNow.Dpi = 100F;
            this.xrDateTimeNow.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrDateTimeNow.LocationFloat = new DevExpress.Utils.PointFloat(201.4166F, 73.16666F);
            this.xrDateTimeNow.Name = "xrDateTimeNow";
            this.xrDateTimeNow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTimeNow.SizeF = new System.Drawing.SizeF(218.3094F, 23F);
            this.xrDateTimeNow.StylePriority.UseFont = false;
            this.xrDateTimeNow.StylePriority.UseTextAlignment = false;
            this.xrDateTimeNow.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPanel2
            // 
            this.xrPanel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel2.Dpi = 100F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 996.9684F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(749.5F, 31.25F);
            this.xrPanel2.StylePriority.UseBorders = false;
            // 
            // xrLabelTotal
            // 
            this.xrLabelTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelTotal.Dpi = 100F;
            this.xrLabelTotal.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelTotal.LocationFloat = new DevExpress.Utils.PointFloat(651.0832F, 996.9684F);
            this.xrLabelTotal.Name = "xrLabelTotal";
            this.xrLabelTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTotal.SizeF = new System.Drawing.SizeF(98.41681F, 31.25F);
            this.xrLabelTotal.StylePriority.UseBorders = false;
            this.xrLabelTotal.StylePriority.UseFont = false;
            this.xrLabelTotal.StylePriority.UseTextAlignment = false;
            this.xrLabelTotal.Text = "Total";
            this.xrLabelTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelFolio
            // 
            this.xrLabelFolio.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelFolio.Dpi = 100F;
            this.xrLabelFolio.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelFolio.LocationFloat = new DevExpress.Utils.PointFloat(0F, 996.9684F);
            this.xrLabelFolio.Name = "xrLabelFolio";
            this.xrLabelFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFolio.SizeF = new System.Drawing.SizeF(78.18584F, 31.25F);
            this.xrLabelFolio.StylePriority.UseBorders = false;
            this.xrLabelFolio.StylePriority.UseFont = false;
            this.xrLabelFolio.StylePriority.UseTextAlignment = false;
            this.xrLabelFolio.Text = "Folio";
            this.xrLabelFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelSite
            // 
            this.xrLabelSite.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelSite.Dpi = 100F;
            this.xrLabelSite.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelSite.LocationFloat = new DevExpress.Utils.PointFloat(78.18584F, 996.9684F);
            this.xrLabelSite.Multiline = true;
            this.xrLabelSite.Name = "xrLabelSite";
            this.xrLabelSite.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSite.SizeF = new System.Drawing.SizeF(138.5417F, 31.25F);
            this.xrLabelSite.StylePriority.UseBorders = false;
            this.xrLabelSite.StylePriority.UseFont = false;
            this.xrLabelSite.StylePriority.UseTextAlignment = false;
            this.xrLabelSite.Text = "Tienda";
            this.xrLabelSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelType
            // 
            this.xrLabelType.Dpi = 100F;
            this.xrLabelType.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelType.LocationFloat = new DevExpress.Utils.PointFloat(305.0324F, 996.9684F);
            this.xrLabelType.Name = "xrLabelType";
            this.xrLabelType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelType.SizeF = new System.Drawing.SizeF(104.3829F, 31.25F);
            this.xrLabelType.StylePriority.UseFont = false;
            this.xrLabelType.StylePriority.UseTextAlignment = false;
            this.xrLabelType.Text = "Tipo de Gasto";
            this.xrLabelType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelProvider
            // 
            this.xrLabelProvider.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelProvider.Dpi = 100F;
            this.xrLabelProvider.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelProvider.LocationFloat = new DevExpress.Utils.PointFloat(409.4153F, 996.9684F);
            this.xrLabelProvider.Name = "xrLabelProvider";
            this.xrLabelProvider.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelProvider.SizeF = new System.Drawing.SizeF(106.2097F, 31.25F);
            this.xrLabelProvider.StylePriority.UseBorders = false;
            this.xrLabelProvider.StylePriority.UseFont = false;
            this.xrLabelProvider.StylePriority.UseTextAlignment = false;
            this.xrLabelProvider.Text = "Proveedor";
            this.xrLabelProvider.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelDateRequirement
            // 
            this.xrLabelDateRequirement.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelDateRequirement.Dpi = 100F;
            this.xrLabelDateRequirement.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelDateRequirement.LocationFloat = new DevExpress.Utils.PointFloat(515.625F, 996.9684F);
            this.xrLabelDateRequirement.Name = "xrLabelDateRequirement";
            this.xrLabelDateRequirement.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDateRequirement.SizeF = new System.Drawing.SizeF(135.4581F, 31.25F);
            this.xrLabelDateRequirement.StylePriority.UseBorders = false;
            this.xrLabelDateRequirement.StylePriority.UseFont = false;
            this.xrLabelDateRequirement.StylePriority.UseTextAlignment = false;
            this.xrLabelDateRequirement.Text = "Fecha de Requisión";
            this.xrLabelDateRequirement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCategory
            // 
            this.xrLabelCategory.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelCategory.Dpi = 100F;
            this.xrLabelCategory.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelCategory.LocationFloat = new DevExpress.Utils.PointFloat(216.7275F, 996.9684F);
            this.xrLabelCategory.Multiline = true;
            this.xrLabelCategory.Name = "xrLabelCategory";
            this.xrLabelCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCategory.SizeF = new System.Drawing.SizeF(88.30487F, 31.25F);
            this.xrLabelCategory.StylePriority.UseBorders = false;
            this.xrLabelCategory.StylePriority.UseFont = false;
            this.xrLabelCategory.StylePriority.UseTextAlignment = false;
            this.xrLabelCategory.Text = "Categoría";
            this.xrLabelCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrUser
            // 
            this.xrUser.Dpi = 100F;
            this.xrUser.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrUser.LocationFloat = new DevExpress.Utils.PointFloat(201.4166F, 50.16666F);
            this.xrUser.Name = "xrUser";
            this.xrUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrUser.SizeF = new System.Drawing.SizeF(218.3094F, 23F);
            this.xrUser.StylePriority.UseFont = false;
            this.xrUser.StylePriority.UseTextAlignment = false;
            this.xrUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblUser
            // 
            this.xrlblUser.Dpi = 100F;
            this.xrlblUser.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUser.LocationFloat = new DevExpress.Utils.PointFloat(102.4999F, 50.16667F);
            this.xrlblUser.Name = "xrlblUser";
            this.xrlblUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUser.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblUser.StylePriority.UseFont = false;
            this.xrlblUser.StylePriority.UseTextAlignment = false;
            this.xrlblUser.Text = "Solicitante:";
            this.xrlblUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DashCaus
            // 
            this.DashCaus.BorderColor = System.Drawing.Color.Black;
            this.DashCaus.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DashCaus.Dpi = 100F;
            this.DashCaus.Legend.Name = "Default Legend";
            this.DashCaus.LocationFloat = new DevExpress.Utils.PointFloat(10.71812F, 136.875F);
            this.DashCaus.Name = "DashCaus";
            this.DashCaus.PaletteName = "Oriel";
            this.DashCaus.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.DashCaus.SizeF = new System.Drawing.SizeF(728.282F, 272.9167F);
            // 
            // xrlblDateTimeEnd
            // 
            this.xrlblDateTimeEnd.Dpi = 100F;
            this.xrlblDateTimeEnd.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateTimeEnd.LocationFloat = new DevExpress.Utils.PointFloat(419.7261F, 73.16666F);
            this.xrlblDateTimeEnd.Name = "xrlblDateTimeEnd";
            this.xrlblDateTimeEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateTimeEnd.SizeF = new System.Drawing.SizeF(94.75671F, 23F);
            this.xrlblDateTimeEnd.StylePriority.UseFont = false;
            this.xrlblDateTimeEnd.StylePriority.UseTextAlignment = false;
            this.xrlblDateTimeEnd.Text = "Fecha Final:";
            this.xrlblDateTimeEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDateTimeEnd
            // 
            this.xrDateTimeEnd.Dpi = 100F;
            this.xrDateTimeEnd.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrDateTimeEnd.LocationFloat = new DevExpress.Utils.PointFloat(520.2435F, 73.16666F);
            this.xrDateTimeEnd.Name = "xrDateTimeEnd";
            this.xrDateTimeEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTimeEnd.SizeF = new System.Drawing.SizeF(218.7567F, 23F);
            this.xrDateTimeEnd.StylePriority.UseFont = false;
            this.xrDateTimeEnd.StylePriority.UseTextAlignment = false;
            this.xrDateTimeEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblSupplier
            // 
            this.xrlblSupplier.Dpi = 100F;
            this.xrlblSupplier.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSupplier.LocationFloat = new DevExpress.Utils.PointFloat(419.7261F, 50.16667F);
            this.xrlblSupplier.Name = "xrlblSupplier";
            this.xrlblSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSupplier.SizeF = new System.Drawing.SizeF(92.56622F, 23F);
            this.xrlblSupplier.StylePriority.UseFont = false;
            this.xrlblSupplier.StylePriority.UseTextAlignment = false;
            this.xrlblSupplier.Text = "Proveedor:";
            this.xrlblSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrSupplier
            // 
            this.xrSupplier.Dpi = 100F;
            this.xrSupplier.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrSupplier.LocationFloat = new DevExpress.Utils.PointFloat(512.2923F, 50.16666F);
            this.xrSupplier.Name = "xrSupplier";
            this.xrSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSupplier.SizeF = new System.Drawing.SizeF(235.5654F, 23F);
            this.xrSupplier.StylePriority.UseFont = false;
            this.xrSupplier.StylePriority.UseTextAlignment = false;
            this.xrSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblCategory
            // 
            this.xrlblCategory.Dpi = 100F;
            this.xrlblCategory.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCategory.LocationFloat = new DevExpress.Utils.PointFloat(102.4999F, 96.16667F);
            this.xrlblCategory.Name = "xrlblCategory";
            this.xrlblCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCategory.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblCategory.StylePriority.UseFont = false;
            this.xrlblCategory.StylePriority.UseTextAlignment = false;
            this.xrlblCategory.Text = "Categoría:";
            this.xrlblCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrCategory
            // 
            this.xrCategory.Dpi = 100F;
            this.xrCategory.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrCategory.LocationFloat = new DevExpress.Utils.PointFloat(201.4166F, 96.16667F);
            this.xrCategory.Name = "xrCategory";
            this.xrCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCategory.SizeF = new System.Drawing.SizeF(218.3094F, 23F);
            this.xrCategory.StylePriority.UseFont = false;
            this.xrCategory.StylePriority.UseTextAlignment = false;
            this.xrCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblType
            // 
            this.xrlblType.Dpi = 100F;
            this.xrlblType.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblType.LocationFloat = new DevExpress.Utils.PointFloat(419.7261F, 96.16667F);
            this.xrlblType.Name = "xrlblType";
            this.xrlblType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblType.SizeF = new System.Drawing.SizeF(94.75671F, 23F);
            this.xrlblType.StylePriority.UseFont = false;
            this.xrlblType.StylePriority.UseTextAlignment = false;
            this.xrlblType.Text = "Tipo Gasto:";
            this.xrlblType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrType
            // 
            this.xrType.Dpi = 100F;
            this.xrType.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrType.LocationFloat = new DevExpress.Utils.PointFloat(514.4828F, 96.16667F);
            this.xrType.Name = "xrType";
            this.xrType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrType.SizeF = new System.Drawing.SizeF(218.7567F, 23F);
            this.xrType.StylePriority.UseFont = false;
            this.xrType.StylePriority.UseTextAlignment = false;
            this.xrType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrChart2
            // 
            this.xrChart2.BorderColor = System.Drawing.Color.Black;
            this.xrChart2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart2.Dpi = 100F;
            this.xrChart2.Legend.Name = "Default Legend";
            this.xrChart2.LocationFloat = new DevExpress.Utils.PointFloat(10.71812F, 409.7917F);
            this.xrChart2.Name = "xrChart2";
            this.xrChart2.PaletteName = "Concourse";
            this.xrChart2.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart2.SizeF = new System.Drawing.SizeF(728.2822F, 272.9167F);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart3,
            this.xrChart2,
            this.xrType,
            this.xrlblType,
            this.xrCategory,
            this.xrlblCategory,
            this.xrSupplier,
            this.xrlblSupplier,
            this.xrDateTimeEnd,
            this.xrlblDateTimeEnd,
            this.DashCaus,
            this.xrlblUser,
            this.xrUser,
            this.xrLabelCategory,
            this.xrLabelDateRequirement,
            this.xrLabelProvider,
            this.xrLabelType,
            this.xrLabelSite,
            this.xrLabelFolio,
            this.xrLabelTotal,
            this.xrPanel2,
            this.xrDateTimeNow,
            this.xrlblDateTimeStart,
            this.xrStore,
            this.xrlblStore,
            this.xrTitle,
            this.xrPictureBox1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 1028.219F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrChart3
            // 
            this.xrChart3.BorderColor = System.Drawing.Color.Black;
            this.xrChart3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart3.Dpi = 100F;
            this.xrChart3.Legend.Name = "Default Legend";
            this.xrChart3.LocationFloat = new DevExpress.Utils.PointFloat(10.71804F, 699.3751F);
            this.xrChart3.Name = "xrChart3";
            this.xrChart3.PaletteName = "Mixed";
            this.xrChart3.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart3.SizeF = new System.Drawing.SizeF(728.2821F, 272.9167F);
            // 
            // ExpensesReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.GroupFooter1});
            this.Margins = new System.Drawing.Printing.Margins(54, 47, 31, 27);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DashCaus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellFolio;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellSite;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCategory;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellType;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellProvider;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDateRequirement;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotal;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableFolio;
        private DevExpress.XtraReports.UI.XRTableCell xrTableTotal;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrTitle;
        private DevExpress.XtraReports.UI.XRLabel xrlblStore;
        private DevExpress.XtraReports.UI.XRLabel xrStore;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateTimeStart;
        private DevExpress.XtraReports.UI.XRLabel xrDateTimeNow;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFolio;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSite;
        private DevExpress.XtraReports.UI.XRLabel xrLabelType;
        private DevExpress.XtraReports.UI.XRLabel xrLabelProvider;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDateRequirement;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCategory;
        private DevExpress.XtraReports.UI.XRLabel xrUser;
        private DevExpress.XtraReports.UI.XRLabel xrlblUser;
        private DevExpress.XtraReports.UI.XRChart DashCaus;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateTimeEnd;
        private DevExpress.XtraReports.UI.XRLabel xrDateTimeEnd;
        private DevExpress.XtraReports.UI.XRLabel xrlblSupplier;
        private DevExpress.XtraReports.UI.XRLabel xrSupplier;
        private DevExpress.XtraReports.UI.XRLabel xrlblCategory;
        private DevExpress.XtraReports.UI.XRLabel xrCategory;
        private DevExpress.XtraReports.UI.XRLabel xrlblType;
        private DevExpress.XtraReports.UI.XRLabel xrType;
        private DevExpress.XtraReports.UI.XRChart xrChart2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRChart xrChart3;
    }
}
