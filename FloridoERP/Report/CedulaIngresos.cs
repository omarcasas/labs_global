﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;
using System.ComponentModel.Design;
using DevExpress.XtraRichEdit.API.Native;
using App.Entities.ViewModels.Invoices;

namespace FloridoERPT.Report
{
    public partial class CedulaIngresos : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public CedulaIngresos()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<CedulaModel> lista, int _rfc)
        {
            try
            {
                DataSetTableMovements = new DataSet();

                DataTable tableMovements = new DataTable();
                DataSetTableMovements.Tables.Add(tableMovements);
                
                
              
                
                tableMovements.Columns.Add(new DataColumn("uuid"));
                tableMovements.Columns.Add(new DataColumn("Seriefolio"));
                tableMovements.Columns.Add(new DataColumn("serie"));
                tableMovements.Columns.Add(new DataColumn("_folio"));
                tableMovements.Columns.Add(new DataColumn("sucursal"));
                tableMovements.Columns.Add(new DataColumn("cliente"));
                tableMovements.Columns.Add(new DataColumn("rfc"));
                tableMovements.Columns.Add(new DataColumn("fechaemision"));
                tableMovements.Columns.Add(new DataColumn("impuestoRetenido"));
                tableMovements.Columns.Add(new DataColumn("foliorel"));
                tableMovements.Columns.Add(new DataColumn("foliofiscal"));
                tableMovements.Columns.Add(new DataColumn("tipotasa16"));
                tableMovements.Columns.Add(new DataColumn("tipotasa8"));
                tableMovements.Columns.Add(new DataColumn("tipotasa0"));

               

                tableMovements.Columns.Add(new DataColumn("montoOpeAnterior"));
                tableMovements.Columns.Add(new DataColumn("montoBT0Anterior"));
                tableMovements.Columns.Add(new DataColumn("montoBT16Anterior"));
                tableMovements.Columns.Add(new DataColumn("ivaT16Anterior"));
                tableMovements.Columns.Add(new DataColumn("montoBT8Anterior"));
                tableMovements.Columns.Add(new DataColumn("ivaT8Anterior")); ;
                tableMovements.Columns.Add(new DataColumn("iepsTAnterior"));
                tableMovements.Columns.Add(new DataColumn("totalImpAnterior"));
                tableMovements.Columns.Add(new DataColumn("valorTotalFactAnterior"));
                tableMovements.Columns.Add(new DataColumn("tipo"));
                ////////////
                

                tableMovements.Columns.Add(new DataColumn("montoOpe"));
                tableMovements.Columns.Add(new DataColumn("montoBT0"));
                tableMovements.Columns.Add(new DataColumn("montoBT16"));
                tableMovements.Columns.Add(new DataColumn("ivaT16"));
                tableMovements.Columns.Add(new DataColumn("montoBT8"));
                tableMovements.Columns.Add(new DataColumn("ivaT8")); ;
                tableMovements.Columns.Add(new DataColumn("iepsT"));
                tableMovements.Columns.Add(new DataColumn("totalImp"));
                tableMovements.Columns.Add(new DataColumn("valorTotalFact"));
                tableMovements.Columns.Add(new DataColumn("metodopago"));
                tableMovements.Columns.Add(new DataColumn("parcialidad"));
                tableMovements.Columns.Add(new DataColumn("folioFiscComp"));
                tableMovements.Columns.Add(new DataColumn("fechaEmisComp"));
                tableMovements.Columns.Add(new DataColumn("servicioprestado"));

                //

                tableMovements.Columns.Add(new DataColumn("importeSaldoAnt"));
                tableMovements.Columns.Add(new DataColumn("ImportePagado"));
                tableMovements.Columns.Add(new DataColumn("ImporteSaldoInsoluto"));
                tableMovements.Columns.Add(new DataColumn("montoOpeCOb"));
                tableMovements.Columns.Add(new DataColumn("montoBT0C"));                
                tableMovements.Columns.Add(new DataColumn("montoBT16C"));
                tableMovements.Columns.Add(new DataColumn("ivaT16C"));
                tableMovements.Columns.Add(new DataColumn("montoBT8C"));
                tableMovements.Columns.Add(new DataColumn("IvaT8C"));
                tableMovements.Columns.Add(new DataColumn("iepstrascobra"));
                tableMovements.Columns.Add(new DataColumn("importeTotalCob"));
                tableMovements.Columns.Add(new DataColumn("importeComplemento"));
                tableMovements.Columns.Add(new DataColumn("usocfdi"));
                tableMovements.Columns.Add(new DataColumn("formapago"));


                
               

                tableMovements.Columns.Add(new DataColumn("montoOpeMonExt"));
                tableMovements.Columns.Add(new DataColumn("ivaMonExt"));
                tableMovements.Columns.Add(new DataColumn("importeTotalMonExt"));
                tableMovements.Columns.Add(new DataColumn("fechaPub"));
                tableMovements.Columns.Add(new DataColumn("fechaCobro"));
                tableMovements.Columns.Add(new DataColumn("nombreInstFina"));
                tableMovements.Columns.Add(new DataColumn("cuentaReceptor"));

                tableMovements.Columns.Add(new DataColumn("refBanca"));
                tableMovements.Columns.Add(new DataColumn("totalRFC"));

                tableMovements.Columns.Add(new DataColumn("moneda"));
                tableMovements.Columns.Add(new DataColumn("tc"));  

                decimal _montoOperacion = 0.0M,
                   _montoBaseTasa = 0.0M,
                   _montoBaseTasa16 = 0.0M,
                   _ivaTras16 = 0.0M,
                   _montoBaseTasa8 = 0.0M,
                   _ivaTras8 = 0.0M,
                   _tasa = 0.0M,
                   _iepsTras = 0.0M,
                   _totalImpuestos = 0.0M,
                   _valorTotalFact = 0.0M,
                   _importeSaldoAnterior = 0.0M,
                   _importePagado = 0.0M,
                   _importeSaldoInsoluto = 0.0M,
                   _montoOperacionCobrado = 0.0M,
                   _montoBaseTasa0Cobrado = 0.0M,
                   // _montoBaseT0AdicImpuestos = 0.0M,
                   _montoBaset16Cobrado = 0.0M,
                   _ivaT16Cobrado = 0.0M,
                    _montoBaset8Cobrado = 0.0M,
                   _ivaT8Cobrado = 0.0M,
                   _iepsTrascobrado = 0.0M,
                   _importetotalcobrado = 0.0M,
                   _importeCOmplemento = 0.0M,
                   _totalRFC = 0.0M,
                   _montoOperacionAnt = 0.0M,
                   _montoBaseTasaAnt = 0.0M,
                   _montoBaseTasa16Ant = 0.0M,
                   _ivaTras16Ant = 0.0M,
                   _montoBaseTasa8Ant = 0.0M,
                   _ivaTras8Ant = 0.0M,                 
                   _iepsTrasAnt = 0.0M,
                   _totalImpuestosAnt = 0.0M,
                   _valorTotalFactAnt = 0.0M;






                if (_rfc == 1)
                {
                    for (int i = 0; i < lista.Count - 1; i++)
                    {

                        if (i + 1 <= (lista.Count - 1))
                        {



                            DataRow DataRowOrdered4 = DataSetTableMovements.Tables[0].NewRow();
                            DataRowOrdered4["uuid"] = lista[i].Uuid.ToUpper() ?? "";
                            DataRowOrdered4["folio"] = lista[i].Folio ?? "";
                            DataRowOrdered4["foliorel"] = lista[i].FolioRel ?? "";
                            DataRowOrdered4["serie"] = lista[i].Serie ?? "";
                            DataRowOrdered4["_folio"] = lista[i].SerieFolio ?? "";
                            DataRowOrdered4["sucursal"] = lista[i].Sucursal ?? "";
                            DataRowOrdered4["cliente"] = lista[i].Cliente ?? "";
                            DataRowOrdered4["rfc"] = lista[i].Rfc ?? "";
                            DataRowOrdered4["fechaemision"] = lista[i].FechaEmi ?? "";
                            DataRowOrdered4["foliofiscal"] = lista[i].FolioFiscal ?? "";
                            DataRowOrdered4["servicioprestado"] = lista[i].ServicioPrestado ?? "";
                            DataRowOrdered4["moneda"] = lista[i].Moneda ?? "";
                            DataRowOrdered4["tc"] = lista[i].TipoCambio ?? "";
                            ////
                            DataRowOrdered4["montoOpe"] = lista[i].MontoOperacion;
                            DataRowOrdered4["montoBT0"] = lista[i].MontoBaseT0;
                            DataRowOrdered4["montoBT16"] = lista[i].MontoBaseT16;
                            DataRowOrdered4["ivaT16"] = lista[i].IvaTrasladado16;
                            DataRowOrdered4["montoBT8"] = lista[i].MontoBaseT8;
                            DataRowOrdered4["ivaT8"] = lista[i].IvaTrasladado8;
                           // DataRowOrdered4["tasa"] = lista[i].Tasa;
                            DataRowOrdered4["iepsT"] = lista[i].IepsTrasladado;
                            DataRowOrdered4["totalImp"] = lista[i].TotalImpuestos;
                            DataRowOrdered4["valorTotalFact"] = lista[i].ValorTotalFactura;
                            ///
                            DataRowOrdered4["clave"] = lista[i].Clave ?? "";
                            DataRowOrdered4["metodopago"] = lista[i].MetodoPago ?? "" ?? "";
                            DataRowOrdered4["parcialidad"] = lista[i].NumeroParcialidad ?? "";
                            ///
                            DataRowOrdered4["folioFiscComp"] = lista[i].FolioFiscaC ?? "";
                            DataRowOrdered4["fechaEmisComp"] = lista[i].FechaEmisionC ?? "";
                            DataRowOrdered4["importeSaldoAnt"] = lista[i].ImporteSaldoAnterior;
                            DataRowOrdered4["ImportePagado"] = lista[i].ImportePagado;
                            DataRowOrdered4["ImporteSaldoInsoluto"] = lista[i].ImporteSaldoInsoluto;
                            //
                            DataRowOrdered4["usocfdi"] = lista[i].UsoCfdi ?? "";
                            DataRowOrdered4["formapago"] = lista[i].FormaPago ?? "";
                            DataRowOrdered4["percAbon"] = lista[i].AbonadoPorc ?? "";
                            ///
                            DataRowOrdered4["montoOpeCOb"] = lista[i].MontoOperacionCobrado ?? "";
                            DataRowOrdered4["montoBT0C"] = lista[i].MontoBaseT0C;
                            // DataRowOrdered4["montoBTOadicImp"] = lista[i].MontoBaseT0AdicionadoImp;
                            DataRowOrdered4["montoBT16C"] = lista[i].MontoBaseT16C;
                            DataRowOrdered4["ivaT16C"] = lista[i].IvaTrasladadoCobrado16;
                            DataRowOrdered4["montoBT8C"] = lista[i].MontoBaseT8C;
                            DataRowOrdered4["IvaT8C"] = lista[i].IvaTrasladadoCobrado8;
                            ///
                            DataRowOrdered4["iepstrascobra"] = lista[i].IepsladadoCobrado;
                            DataRowOrdered4["importeTotalCob"] = lista[i].ImporteTotalCobrado;
                            DataRowOrdered4["importeComplemento"] = lista[i].ImporteComplemento;
                            DataRowOrdered4["fechaCobro"] = lista[i].FechaCobro ?? "";
                            DataRowOrdered4["refBanca"] = lista[i].ReferenciaBancaria ?? "";
                            DataRowOrdered4["nombreInstFina"] = lista[i].NombreInstitucionFinanciera ?? "";
                            DataRowOrdered4["totalRFC"] = lista[i].TotalPorRFC;



                            _montoOperacion += lista[i].MontoOperacion;
                            _montoBaseTasa += lista[i].MontoBaseT0;
                            _montoBaseTasa16 += lista[i].MontoBaseT16;
                            _ivaTras16 += lista[i].IvaTrasladado16;
                            _montoBaseTasa8 += lista[i].MontoBaseT8;
                            _ivaTras8 += lista[i].IvaTrasladado8;
                            _tasa += lista[i].Tasa;
                            _iepsTras += lista[i].IepsTrasladado;
                            _totalImpuestos += lista[i].TotalImpuestos;
                            _valorTotalFact += lista[i].ValorTotalFactura;
                            _importeSaldoAnterior += lista[i].ImporteSaldoAnterior;
                            _importePagado += lista[i].ImportePagado;
                            _importeSaldoInsoluto += lista[i].ImporteSaldoInsoluto;
                            _montoOperacionCobrado += decimal.Parse(lista[i].MontoOperacionCobrado == "credito" ? "0.0" : lista[i].MontoOperacionCobrado);
                            _montoBaseTasa0Cobrado += lista[i].MontoBaseT0C;
                            // _montoBaseT0AdicImpuestos += lista[i].MontoBaseT0AdicionadoImp;
                            _montoBaset16Cobrado += lista[i].MontoBaseT16C;
                            _ivaT16Cobrado += lista[i].IvaTrasladadoCobrado16;
                            _montoBaset8Cobrado += lista[i].MontoBaseT8C;
                            _ivaT8Cobrado += lista[i].IvaTrasladadoCobrado8;
                            _iepsTrascobrado += lista[i].IepsladadoCobrado;
                            _importetotalcobrado += lista[i].ImporteTotalCobrado;
                            _importeCOmplemento += lista[i].ImporteComplemento;
                            _totalRFC += lista[i].TotalPorRFC;

                            DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered4);
                            if (lista[i].Rfc != lista[i + 1].Rfc)
                            {

                                ////renglon con totales al final

                                DataRow DataRowOrderedTotales = DataSetTableMovements.Tables[0].NewRow();
                                DataRowOrderedTotales["uuid"] = "*************";
                                DataRowOrderedTotales["folio"] = "*************";
                                DataRowOrderedTotales["foliorel"] = "*************";
                                DataRowOrderedTotales["serie"] = "*************";
                                DataRowOrderedTotales["_folio"] = "*************";
                                DataRowOrderedTotales["sucursal"] = "*************";
                                DataRowOrderedTotales["cliente"] = "*************";
                                DataRowOrderedTotales["rfc"] = "*************";
                                DataRowOrderedTotales["fechaemision"] = "*************";
                                DataRowOrderedTotales["foliofiscal"] = "*************";
                                DataRowOrderedTotales["servicioprestado"] = "*************";
                                DataRowOrderedTotales["moneda"] = "*************";
                                DataRowOrderedTotales["tc"] = "*************";
                                ////
                                DataRowOrderedTotales["montoOpe"] = _montoOperacion;
                                DataRowOrderedTotales["montoBT0"] = _montoBaseTasa;
                                DataRowOrderedTotales["montoBT16"] = _montoBaseTasa16;
                                DataRowOrderedTotales["ivaT16"] = _ivaTras16;
                                DataRowOrderedTotales["montoBT8"] = _montoBaseTasa8;
                                DataRowOrderedTotales["ivaT8"] = _ivaTras8;
                               // DataRowOrderedTotales["tasa"] = _tasa;
                                DataRowOrderedTotales["iepsT"] = _iepsTras;
                                DataRowOrderedTotales["totalImp"] = _totalImpuestos;
                                DataRowOrderedTotales["valorTotalFact"] = _valorTotalFact;
                                ///
                                DataRowOrderedTotales["clave"] = "";
                                DataRowOrderedTotales["metodopago"] = "*************";
                                DataRowOrderedTotales["parcialidad"] = "*************";
                                ///
                                DataRowOrderedTotales["folioFiscComp"] = "*************";
                                DataRowOrderedTotales["fechaEmisComp"] = "*************";
                                DataRowOrderedTotales["importeSaldoAnt"] = _importeSaldoAnterior;
                                DataRowOrderedTotales["ImportePagado"] = _importePagado;
                                DataRowOrderedTotales["ImporteSaldoInsoluto"] = _importeSaldoInsoluto;
                                //
                                DataRowOrderedTotales["usocfdi"] = "*************";
                                DataRowOrderedTotales["formapago"] = "*************";
                                DataRowOrderedTotales["percAbon"] = "*************";
                                //
                                DataRowOrderedTotales["montoOpeCOb"] = _montoOperacionCobrado;
                                DataRowOrderedTotales["montoBT0C"] = _montoBaseTasa0Cobrado;
                                // DataRowOrderedTotales["montoBTOadicImp"] = _montoBaseT0AdicImpuestos;
                                DataRowOrderedTotales["montoBT16C"] = _montoBaset16Cobrado;
                                DataRowOrderedTotales["ivaT16C"] = _ivaT16Cobrado;
                                DataRowOrderedTotales["montoBT8C"] = _montoBaset8Cobrado;
                                DataRowOrderedTotales["IvaT8C"] = _ivaT8Cobrado;
                                ///
                                DataRowOrderedTotales["iepstrascobra"] = _iepsTrascobrado;
                                DataRowOrderedTotales["importeTotalCob"] = _importetotalcobrado;
                                DataRowOrderedTotales["importeComplemento"] = _importeCOmplemento;
                                DataRowOrderedTotales["fechaCobro"] = "*************";
                                DataRowOrderedTotales["refBanca"] = "*************";
                                DataRowOrderedTotales["nombreInstFina"] = "*************";
                                DataRowOrderedTotales["totalRFC"] = "*************";

                                DataSetTableMovements.Tables[0].Rows.Add(DataRowOrderedTotales);
                                _montoOperacion = 0.0M;
                                _montoBaseTasa = 0.0M;
                                _montoBaseTasa16 = 0.0M;
                                _ivaTras16 = 0.0M;
                                _montoBaseTasa8 = 0.0M;
                                _ivaTras8 = 0.0M;
                                _tasa = 0.0M;
                                _iepsTras = 0.0M;
                                _totalImpuestos = 0.0M;
                                _valorTotalFact = 0.0M;
                                _importeSaldoAnterior = 0.0M;
                                _importePagado = 0.0M;
                                _importeSaldoInsoluto = 0.0M;
                                _montoOperacionCobrado = 0.0M;
                                _montoBaseTasa0Cobrado = 0.0M;
                                // _montoBaseT0AdicImpuestos = 0.0M;
                                _montoBaset16Cobrado = 0.0M;
                                _ivaT16Cobrado = 0.0M;
                                _montoBaset8Cobrado = 0.0M;
                                _ivaT8Cobrado = 0.0M;
                                _iepsTrascobrado = 0.0M;
                                _importetotalcobrado = 0.0M;
                                _importeCOmplemento = 0.0M;
                                _totalRFC = 0.0M;



                            }
                        }


                    }
                    DataRow DataRowOrderedT = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrderedT["uuid"] = lista[lista.Count - 1].Uuid.ToUpper() ?? "";
                    DataRowOrderedT["folio"] = lista[lista.Count - 1].Folio ?? "";
                    DataRowOrderedT["foliorel"] = lista[lista.Count - 1].FolioRel ?? "";
                    DataRowOrderedT["serie"] = lista[lista.Count - 1].Serie ?? "";
                    DataRowOrderedT["_folio"] = lista[lista.Count - 1].SerieFolio ?? "";
                    DataRowOrderedT["sucursal"] = lista[lista.Count - 1].Sucursal ?? "";
                    DataRowOrderedT["cliente"] = lista[lista.Count - 1].Cliente ?? "";
                    DataRowOrderedT["rfc"] = lista[lista.Count - 1].Rfc ?? "";
                    DataRowOrderedT["fechaemision"] = lista[lista.Count - 1].FechaEmi ?? "";
                    DataRowOrderedT["foliofiscal"] = lista[lista.Count - 1].FolioFiscal ?? "";
                    DataRowOrderedT["servicioprestado"] = lista[lista.Count - 1].ServicioPrestado ?? "";
                    DataRowOrderedT["moneda"] = lista[lista.Count - 1].Moneda ?? "";
                    DataRowOrderedT["tc"] = lista[lista.Count - 1].TipoCambio ?? "";
                    ////
                    DataRowOrderedT["montoOpe"] = lista[lista.Count - 1].MontoOperacion;
                    DataRowOrderedT["montoBT0"] = lista[lista.Count - 1].MontoBaseT0;
                    DataRowOrderedT["montoBT16"] = lista[lista.Count - 1].MontoBaseT16;
                    DataRowOrderedT["ivaT16"] = lista[lista.Count - 1].IvaTrasladado16;
                    DataRowOrderedT["montoBT8"] = lista[lista.Count - 1].MontoBaseT8;
                    DataRowOrderedT["ivaT8"] = lista[lista.Count - 1].IvaTrasladado8;
                    DataRowOrderedT["tasa"] = lista[lista.Count - 1].Tasa;
                    DataRowOrderedT["iepsT"] = lista[lista.Count - 1].IepsTrasladado;
                    DataRowOrderedT["totalImp"] = lista[lista.Count - 1].TotalImpuestos;
                    DataRowOrderedT["valorTotalFact"] = lista[lista.Count - 1].ValorTotalFactura;
                    ///
                    DataRowOrderedT["clave"] = lista[lista.Count - 1].Clave ?? "";
                    DataRowOrderedT["metodopago"] = lista[lista.Count - 1].MetodoPago ?? "" ?? "";
                    DataRowOrderedT["parcialidad"] = lista[lista.Count - 1].NumeroParcialidad ?? "";
                    ///
                    DataRowOrderedT["folioFiscComp"] = lista[lista.Count - 1].FolioFiscaC ?? "";
                    DataRowOrderedT["fechaEmisComp"] = lista[lista.Count - 1].FechaEmisionC ?? "";
                    DataRowOrderedT["importeSaldoAnt"] = lista[lista.Count - 1].ImporteSaldoAnterior;
                    DataRowOrderedT["ImportePagado"] = lista[lista.Count - 1].ImportePagado;
                    DataRowOrderedT["ImporteSaldoInsoluto"] = lista[lista.Count - 1].ImporteSaldoInsoluto;
                    //
                    DataRowOrderedT["usocfdi"] = lista[lista.Count - 1].UsoCfdi ?? "";
                    DataRowOrderedT["formapago"] = lista[lista.Count - 1].FormaPago ?? "";
                    DataRowOrderedT["percAbon"] = lista[lista.Count - 1].AbonadoPorc ?? "";
                    ///
                    DataRowOrderedT["montoOpeCOb"] = lista[lista.Count - 1].MontoOperacionCobrado ?? "";
                    DataRowOrderedT["montoBT0C"] = lista[lista.Count - 1].MontoBaseT0C;
                    // DataRowOrderedT["montoBTOadicImp"] = lista[lista.Count-1].MontoBaseT0AdicionadoImp;
                    DataRowOrderedT["montoBT16C"] = lista[lista.Count - 1].MontoBaseT16C;
                    DataRowOrderedT["ivaT16C"] = lista[lista.Count - 1].IvaTrasladadoCobrado16;
                    DataRowOrderedT["montoBT8C"] = lista[lista.Count - 1].MontoBaseT8C;
                    DataRowOrderedT["IvaT8C"] = lista[lista.Count - 1].IvaTrasladadoCobrado8;
                    ///
                    DataRowOrderedT["iepstrascobra"] = lista[lista.Count - 1].IepsladadoCobrado;
                    DataRowOrderedT["importeTotalCob"] = lista[lista.Count - 1].ImporteTotalCobrado;
                    DataRowOrderedT["importeComplemento"] = lista[lista.Count - 1].ImporteComplemento;
                    DataRowOrderedT["fechaCobro"] = lista[lista.Count - 1].FechaCobro ?? "";
                    DataRowOrderedT["refBanca"] = lista[lista.Count - 1].ReferenciaBancaria ?? "";
                    DataRowOrderedT["nombreInstFina"] = lista[lista.Count - 1].NombreInstitucionFinanciera ?? "";
                    DataRowOrderedT["totalRFC"] = lista[lista.Count - 1].TotalPorRFC;



                    _montoOperacion += lista[lista.Count - 1].MontoOperacion;
                    _montoBaseTasa += lista[lista.Count - 1].MontoBaseT0;
                    _montoBaseTasa16 += lista[lista.Count - 1].MontoBaseT16;
                    _ivaTras16 += lista[lista.Count - 1].IvaTrasladado16;
                    _montoBaseTasa8 += lista[lista.Count - 1].MontoBaseT8;
                    _ivaTras8 += lista[lista.Count - 1].IvaTrasladado8;
                    _tasa += lista[lista.Count - 1].Tasa;
                    _iepsTras += lista[lista.Count - 1].IepsTrasladado;
                    _totalImpuestos += lista[lista.Count - 1].TotalImpuestos;
                    _valorTotalFact += lista[lista.Count - 1].ValorTotalFactura;
                    _importeSaldoAnterior += lista[lista.Count - 1].ImporteSaldoAnterior;
                    _importePagado += lista[lista.Count - 1].ImportePagado;
                    _importeSaldoInsoluto += lista[lista.Count - 1].ImporteSaldoInsoluto;
                    _montoOperacionCobrado += decimal.Parse(lista[lista.Count - 1].MontoOperacionCobrado == "credito" ? "0.0" : lista[lista.Count - 1].MontoOperacionCobrado);
                    _montoBaseTasa0Cobrado += lista[lista.Count - 1].MontoBaseT0C;
                    //_montoBaseT0AdicImpuestos += lista[lista.Count-1].MontoBaseT0AdicionadoImp;
                    _montoBaset16Cobrado += lista[lista.Count - 1].MontoBaseT16C;
                    _ivaT16Cobrado += lista[lista.Count - 1].IvaTrasladadoCobrado16;
                    _montoBaset8Cobrado += lista[lista.Count - 1].MontoBaseT8C;
                    _ivaT8Cobrado += lista[lista.Count - 1].IvaTrasladadoCobrado8;
                    _iepsTrascobrado += lista[lista.Count - 1].IepsladadoCobrado;
                    _importetotalcobrado += lista[lista.Count - 1].ImporteTotalCobrado;
                    _importeCOmplemento += lista[lista.Count - 1].ImporteComplemento;
                    _totalRFC += lista[lista.Count - 1].TotalPorRFC;

                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrderedT);


                    ////renglon con totales al final
                    DataRow DataRowOrderedTotalesF = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrderedTotalesF["uuid"] = "*************";
                    DataRowOrderedTotalesF["folio"] = "*************";
                    DataRowOrderedTotalesF["foliorel"] = "*************";
                    DataRowOrderedTotalesF["serie"] = "*************";
                    DataRowOrderedTotalesF["_folio"] = "*************";
                    DataRowOrderedTotalesF["sucursal"] = "*************";
                    DataRowOrderedTotalesF["cliente"] = "*************";
                    DataRowOrderedTotalesF["rfc"] = "*************";
                    DataRowOrderedTotalesF["fechaemision"] = "*************";
                    DataRowOrderedTotalesF["foliofiscal"] = "*************";
                    DataRowOrderedTotalesF["servicioprestado"] = "*************";
                    DataRowOrderedTotalesF["moneda"] = "*************";
                    DataRowOrderedTotalesF["tc"] = "*************";
                    ////
                    DataRowOrderedTotalesF["montoOpe"] = _montoOperacion;
                    DataRowOrderedTotalesF["montoBT0"] = _montoBaseTasa;
                    DataRowOrderedTotalesF["montoBT16"] = _montoBaseTasa16;
                    DataRowOrderedTotalesF["ivaT16"] = _ivaTras16;
                    DataRowOrderedTotalesF["montoBT8"] = _montoBaseTasa8;
                    DataRowOrderedTotalesF["ivaT8"] = _ivaTras8;
                    DataRowOrderedTotalesF["tasa"] = _tasa;
                    DataRowOrderedTotalesF["iepsT"] = _iepsTras;
                    DataRowOrderedTotalesF["totalImp"] = _totalImpuestos;
                    DataRowOrderedTotalesF["valorTotalFact"] = _valorTotalFact;
                    ///
                    DataRowOrderedTotalesF["clave"] = "";
                    DataRowOrderedTotalesF["metodopago"] = "*************";
                    DataRowOrderedTotalesF["parcialidad"] = "*************";
                    ///
                    DataRowOrderedTotalesF["folioFiscComp"] = "*************";
                    DataRowOrderedTotalesF["fechaEmisComp"] = "*************";
                    DataRowOrderedTotalesF["importeSaldoAnt"] = _importeSaldoAnterior;
                    DataRowOrderedTotalesF["ImportePagado"] = _importePagado;
                    DataRowOrderedTotalesF["ImporteSaldoInsoluto"] = _importeSaldoInsoluto;
                    //
                    DataRowOrderedTotalesF["usocfdi"] = "*************";
                    DataRowOrderedTotalesF["formapago"] = "*************";
                    DataRowOrderedTotalesF["percAbon"] = "*************";
                    //
                    DataRowOrderedTotalesF["montoOpeCOb"] = _montoOperacionCobrado;
                    DataRowOrderedTotalesF["montoBT0C"] = _montoBaseTasa0Cobrado;
                    //DataRowOrderedTotalesF["montoBTOadicImp"] = _montoBaseT0AdicImpuestos;
                    DataRowOrderedTotalesF["montoBT16C"] = _montoBaset16Cobrado;
                    DataRowOrderedTotalesF["ivaT16C"] = _ivaT16Cobrado;
                    DataRowOrderedTotalesF["montoBT8C"] = _montoBaset8Cobrado;
                    DataRowOrderedTotalesF["IvaT8C"] = _ivaT8Cobrado;
                    ///
                    DataRowOrderedTotalesF["iepstrascobra"] = _iepsTrascobrado;
                    DataRowOrderedTotalesF["importeTotalCob"] = _importetotalcobrado;
                    DataRowOrderedTotalesF["importeComplemento"] = _importeCOmplemento;
                    DataRowOrderedTotalesF["fechaCobro"] = "*************";
                    DataRowOrderedTotalesF["refBanca"] = "*************";
                    DataRowOrderedTotalesF["nombreInstFina"] = "*************";
                    DataRowOrderedTotalesF["totalRFC"] = "*************";

                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrderedTotalesF);

                }

                else
                {
                    foreach (var item in lista)
                    {



                        DataRow DataRowOrdered4 = DataSetTableMovements.Tables[0].NewRow();
                        DataRowOrdered4["uuid"] = item.Uuid.ToUpper() ?? "";
                        DataRowOrdered4["Seriefolio"] = item.SerieFolio ?? "";
                        DataRowOrdered4["serie"] = item.Serie ?? "";
                        DataRowOrdered4["_folio"] = item.Folio ?? "";
                        DataRowOrdered4["sucursal"] = item.Sucursal ?? "";
                        DataRowOrdered4["cliente"] = item.Cliente ?? "";
                        DataRowOrdered4["rfc"] = item.Rfc ?? "";
                        DataRowOrdered4["fechaemision"] = item.FechaEmi ?? "";
                        DataRowOrdered4["servicioprestado"] = item.ServicioPrestado ?? "";
                        DataRowOrdered4["impuestoRetenido"] = item.ImpuestoRetenido.ToString() ?? "";
                        DataRowOrdered4["foliorel"] = item.FolioRel ?? "";
                        DataRowOrdered4["foliofiscal"] = item.FolioFiscal ?? "";
                        DataRowOrdered4["tipotasa16"] = item.tipo16 ?? "";
                        DataRowOrdered4["tipotasa8"] = item.tipo8 ?? "";
                        DataRowOrdered4["tipotasa0"] = item.tipo0 ?? "";
                        ////
                        DataRowOrdered4["montoOpeAnterior"] = item.MontoOperacionMesAnt;
                        DataRowOrdered4["montoBT0Anterior"] = item.MontoBaseT0MesAnt;
                        DataRowOrdered4["montoBT16Anterior"] = item.MontoBaseT16MesAnt;
                        DataRowOrdered4["ivaT16Anterior"] = item.IvaTrasladado16MesAnt;
                        DataRowOrdered4["montoBT8Anterior"] = item.MontoBaseT8MesAnt;
                        DataRowOrdered4["ivaT8Anterior"] = item.IvaTrasladado8MesAnt;                       
                        DataRowOrdered4["iepsTAnterior"] = item.IepsTrasladadoMesAnt;
                        DataRowOrdered4["totalImpAnterior"] = item.TotalImpuestosMesAnt;
                        DataRowOrdered4["valorTotalFactAnterior"] = item.ValorTotalFacturaMesAnt;
                        DataRowOrdered4["tipo"] = item.Tipo;
                        ///                        
                        DataRowOrdered4["montoOpe"] = item.MontoOperacion;
                        DataRowOrdered4["montoBT0"] = item.MontoBaseT0;
                        DataRowOrdered4["montoBT16"] = item.MontoBaseT16;
                        DataRowOrdered4["ivaT16"] = item.IvaTrasladado16;
                        DataRowOrdered4["montoBT8"] = item.MontoBaseT8;
                        DataRowOrdered4["ivaT8"] = item.IvaTrasladado8;
                     
                        DataRowOrdered4["iepsT"] = item.IepsTrasladado;
                        DataRowOrdered4["totalImp"] = item.TotalImpuestos;
                        DataRowOrdered4["valorTotalFact"] = item.ValorTotalFactura;
                        DataRowOrdered4["metodopago"] = item.MetodoPago ?? "" ?? "";
                        DataRowOrdered4["parcialidad"] = item.NumeroParcialidad ?? "";
                        DataRowOrdered4["folioFiscComp"] = item.FolioFiscaC ?? "";
                        DataRowOrdered4["fechaEmisComp"] = item.FechaEmisionC ?? "";
                        ///

                        DataRowOrdered4["importeSaldoAnt"] = item.ImporteSaldoAnterior;
                        DataRowOrdered4["ImportePagado"] = item.ImportePagado;
                        DataRowOrdered4["ImporteSaldoInsoluto"] = item.ImporteSaldoInsoluto;                       
                        DataRowOrdered4["montoOpeCOb"] = item.MontoOperacionCobrado ?? "";
                        DataRowOrdered4["montoBT0C"] = item.MontoBaseT0C;
                        DataRowOrdered4["montoBT16C"] = item.MontoBaseT16C;
                        DataRowOrdered4["ivaT16C"] = item.IvaTrasladadoCobrado16;
                        DataRowOrdered4["montoBT8C"] = item.MontoBaseT8C;
                        DataRowOrdered4["IvaT8C"] = item.IvaTrasladadoCobrado8;
                        DataRowOrdered4["iepstrascobra"] = item.IepsladadoCobrado;
                        DataRowOrdered4["importeTotalCob"] = item.ImporteTotalCobrado;
                        DataRowOrdered4["importeComplemento"] = item.ImporteComplemento;
                        DataRowOrdered4["usocfdi"] = item.UsoCfdi ?? "";
                        DataRowOrdered4["formapago"] = item.FormaPago ?? "";
                        //
                        DataRowOrdered4["montoOpeMonExt"] = item.MontoOperacionMonedaExt;
                        DataRowOrdered4["ivaMonExt"] = item.ivaMonedaExt;
                        DataRowOrdered4["importeTotalMonExt"] = item.ImporteMonedaExtra ;
                        DataRowOrdered4["moneda"] = item.Moneda ?? "";
                        DataRowOrdered4["tc"] = item.TipoCambio ?? "";
                        DataRowOrdered4["fechaPub"] = item.FechaPublicacion ?? "";
                        DataRowOrdered4["fechaCobro"] = item.FechaCobro ?? "";
                        DataRowOrdered4["nombreInstFina"] = item.NombreInstitucionFinanciera ?? "";
                        DataRowOrdered4["cuentaReceptor"] = "";
                        DataRowOrdered4["refBanca"] = item.ReferenciaBancaria ?? "";
                        DataRowOrdered4["totalRFC"] = item.TotalPorRFC;

                        // DataRowOrdered4["clave"] = item.Clave ?? "";



                        // DataRowOrdered4["percAbon"] = item.AbonadoPorc ?? "";
                        ///

                        // DataRowOrdered4["montoBTOadicImp"] = item.MontoBaseT0AdicionadoImp;

                        ///


                        _montoOperacionAnt   += item.MontoOperacionMesAnt;
                        _montoBaseTasaAnt += item.MontoBaseT0MesAnt;
                        _montoBaseTasa16Ant += item.MontoBaseT16MesAnt;
                        _ivaTras16Ant  += item.IvaTrasladado16MesAnt;
                        _montoBaseTasa8Ant += item.MontoBaseT8MesAnt;
                        _ivaTras8Ant += item.IvaTrasladado8MesAnt;
                        _iepsTrasAnt += item.IepsTrasladadoMesAnt;
                        _totalImpuestosAnt += item.TotalImpuestosMesAnt;
                        _valorTotalFactAnt += item.ValorTotalFacturaMesAnt;




                        _montoOperacion += item.MontoOperacion;
                        _montoBaseTasa += item.MontoBaseT0;
                        _montoBaseTasa16 += item.MontoBaseT16;
                        _ivaTras16 += item.IvaTrasladado16;
                        _montoBaseTasa8 += item.MontoBaseT8;
                        _ivaTras8 += item.IvaTrasladado8;
                        _tasa += item.Tasa;
                        _iepsTras += item.IepsTrasladado;
                        _totalImpuestos += item.TotalImpuestos;
                        _valorTotalFact += item.ValorTotalFactura;
                        _importeSaldoAnterior += item.ImporteSaldoAnterior;
                        _importePagado += item.ImportePagado;
                        _importeSaldoInsoluto += item.ImporteSaldoInsoluto;
                        _montoOperacionCobrado += decimal.Parse(item.MontoOperacionCobrado == "credito" ? "0.0" : item.MontoOperacionCobrado);
                        _montoBaseTasa0Cobrado += item.MontoBaseT0C;
                        //_montoBaseT0AdicImpuestos += item.MontoBaseT0AdicionadoImp;
                        _montoBaset16Cobrado += item.MontoBaseT16C;
                        _ivaT16Cobrado += item.IvaTrasladadoCobrado16;
                        _montoBaset8Cobrado += item.MontoBaseT8C;
                        _ivaT8Cobrado += item.IvaTrasladadoCobrado8;
                        _iepsTrascobrado += item.IepsladadoCobrado;
                        _importetotalcobrado += item.ImporteTotalCobrado;
                        _importeCOmplemento += item.ImporteComplemento;
                        _totalRFC += item.TotalPorRFC;

                        DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered4);






                    }
                    ////renglon con totales al final
                    DataRow DataRowOrderedTotalesF = DataSetTableMovements.Tables[0].NewRow();
                    
                    DataRowOrderedTotalesF["uuid"] = "*************";
                    DataRowOrderedTotalesF["Seriefolio"] = "*************";
                    DataRowOrderedTotalesF["serie"] = "*************";
                    DataRowOrderedTotalesF["_folio"] = "*************";
                    DataRowOrderedTotalesF["sucursal"] = "*************";
                    DataRowOrderedTotalesF["cliente"] = "*************";
                    DataRowOrderedTotalesF["rfc"] = "*************";
                    DataRowOrderedTotalesF["fechaemision"] = "*************";
                    DataRowOrderedTotalesF["servicioprestado"] = "*************";
                    DataRowOrderedTotalesF["impuestoRetenido"] = "*************";
                    DataRowOrderedTotalesF["foliorel"] = "*************";
                    DataRowOrderedTotalesF["foliofiscal"] = "*************";
                    DataRowOrderedTotalesF["tipotasa16"] = "*************";
                    DataRowOrderedTotalesF["tipotasa8"] = "*************";
                    DataRowOrderedTotalesF["tipotasa0"] = "*************";
                    ////
                    DataRowOrderedTotalesF["montoOpeAnterior"] = _montoOperacionAnt;
                    DataRowOrderedTotalesF["montoBT0Anterior"] = _montoBaseTasaAnt;
                    DataRowOrderedTotalesF["montoBT16Anterior"] = _montoBaseTasa16Ant;
                    DataRowOrderedTotalesF["ivaT16Anterior"] = _ivaTras16Ant;
                    DataRowOrderedTotalesF["montoBT8Anterior"] = _montoBaseTasa8Ant;
                    DataRowOrderedTotalesF["ivaT8Anterior"] = _ivaTras8Ant;
                    DataRowOrderedTotalesF["iepsTAnterior"] = _iepsTrasAnt;
                    DataRowOrderedTotalesF["totalImpAnterior"] = _totalImpuestosAnt;
                    DataRowOrderedTotalesF["valorTotalFactAnterior"] = _valorTotalFactAnt;
                    DataRowOrderedTotalesF["tipo"] = "*************";
                    ///
                    ////
                    
                    DataRowOrderedTotalesF["montoOpe"] = _montoOperacion;
                    DataRowOrderedTotalesF["montoBT0"] = _montoBaseTasa;
                    DataRowOrderedTotalesF["montoBT16"] = _montoBaseTasa16;
                    DataRowOrderedTotalesF["ivaT16"] = _ivaTras16;
                    DataRowOrderedTotalesF["montoBT8"] = _montoBaseTasa8;
                    DataRowOrderedTotalesF["ivaT8"] = _ivaTras8;
                    //DataRowOrderedTotalesF["tasa"] = _tasa;
                    DataRowOrderedTotalesF["iepsT"] = _iepsTras;
                    DataRowOrderedTotalesF["totalImp"] = _totalImpuestos;
                    DataRowOrderedTotalesF["valorTotalFact"] = _valorTotalFact;
                    DataRowOrderedTotalesF["metodopago"] = "*************";
                    DataRowOrderedTotalesF["parcialidad"] = "*************";
                    DataRowOrderedTotalesF["folioFiscComp"] = "*************";
                    DataRowOrderedTotalesF["fechaEmisComp"] = "*************";
                    ///
                    ///
                    DataRowOrderedTotalesF["importeSaldoAnt"] = _importeSaldoAnterior;
                    DataRowOrderedTotalesF["ImportePagado"] = _importePagado;
                    DataRowOrderedTotalesF["ImporteSaldoInsoluto"] = _importeSaldoInsoluto;
                    DataRowOrderedTotalesF["montoOpeCOb"] = _montoOperacionCobrado;
                    DataRowOrderedTotalesF["montoBT0C"] = _montoBaseTasa0Cobrado;
                    DataRowOrderedTotalesF["montoBT16C"] = _montoBaset16Cobrado;
                    DataRowOrderedTotalesF["ivaT16C"] = _ivaT16Cobrado;
                    DataRowOrderedTotalesF["montoBT8C"] = _montoBaset8Cobrado;
                    DataRowOrderedTotalesF["IvaT8C"] = _ivaT8Cobrado;                    
                    DataRowOrderedTotalesF["iepstrascobra"] = _iepsTrascobrado;
                    DataRowOrderedTotalesF["importeTotalCob"] = _importetotalcobrado;
                    DataRowOrderedTotalesF["importeComplemento"] = _importeCOmplemento;

                 
                    DataRowOrderedTotalesF["usocfdi"] = "*************";
                    DataRowOrderedTotalesF["formapago"] = "*************";
                    DataRowOrderedTotalesF["montoOpeMonExt"] = "*************";
                    DataRowOrderedTotalesF["ivaMonExt"] = "*************";
                    DataRowOrderedTotalesF["importeTotalMonExt"] = "*************";
                    DataRowOrderedTotalesF["moneda"] = "*************";
                    DataRowOrderedTotalesF["tc"] = "*************";
                    DataRowOrderedTotalesF["fechaPub"] = "*************";
                    DataRowOrderedTotalesF["fechaCobro"] = "*************";
                    DataRowOrderedTotalesF["nombreInstFina"] = "*************";
                    DataRowOrderedTotalesF["cuentaReceptor"] = "*************";
                    DataRowOrderedTotalesF["refBanca"] = "*************";
                    DataRowOrderedTotalesF["totalRFC"] = "*************";
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrderedTotalesF);

                }

                CellUuid.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.uuid"));
                CellserieFolio.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Seriefolio"));
                Cellserie.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.serie"));
                CellFolio.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1._folio"));
                CellSucursal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.sucursal"));
                CellCliente.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cliente"));
                CellRFC.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.rfc"));
                CellFechaEmision.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.fechaemision"));
                CellServicioPres.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.servicioprestado"));
                CellImpuestoRetenido.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.impuestoRetenido"));
                CellFolioRelacionado.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.foliorel"));
                CellFolioFiscalRelacionado.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.foliofiscal"));
                CellTipoTasa16.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.tipotasa16"));               
                CellTipoTasa8.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.tipotasa8"));
                CellTipoTasa0.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.tipotasa0"));
                ////
                CellMontoOperAterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoOpeAnterior"));
                CellMontoBase0Anterio.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT0Anterior"));
                CellMontoBase16Anterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT16Anterior"));
                CellIVA16Anterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ivaT16Anterior"));
                CellMonto8Anterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT8Anterior"));
                CellIVA8Anterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ivaT8Anterior"));
                CellIepsAnterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.iepsTAnterior"));
                CellTotalImpuestosAnterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.totalImpAnterior"));
                CellValorTotalAnterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.valorTotalFactAnterior"));
                CellTipo.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.tipo"));
                //
                
               
                CellMontoOperacion.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoOpe"));               
                CellMontoT0.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT0"));
                CellMontoT16.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT16"));
                CellIVA16.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ivaT16"));
                CellMontoT8.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT8"));
                CellIVA8.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ivaT8"));               
                CellIEPStras.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.iepsT"));
                CellTotalImpuestos.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.totalImp"));
                CellValorTotalFac.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.valorTotalFact"));
                CellMetodoPago.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.metodopago"));
                CellNumParc.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.parcialidad"));
                CellFoloFiscalComp.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.folioFiscComp"));
                CellFechaEmisionComp.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.fechaEmisComp"));

                //

                CellImporteAnterior.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.importeSaldoAnt"));
                CellImportePagado.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ImportePagado"));
                CellImporteSaldoInsouto.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ImporteSaldoInsoluto"));
                
                CellMontoOpeCob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoOpeCOb"));
                CellMontoBaseT0Cob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT0C"));
                CellMontoB16Cob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT16C"));
                CellIVA16Cob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ivaT16C"));
                CellMontoT8Cob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoBT8C"));
                CellIVA8Cob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.IvaT8C"));
                CellIEPSCob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.iepstrascobra"));
                CellImporteCob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.importeComplemento"));
                //
               
                CellUsoCFDI.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.usocfdi"));
                CellFormaPago.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.formapago"));
                CellMontoOperMonExt.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.montoOpeMonExt"));
                CellIVAMonExt.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ivaMonExt"));
                CellImporteMonExt.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.importeTotalMonExt"));
                CellMoneda.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.moneda"));
                CellTipoCamb.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.tc"));
                CellFechaPub.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.fechaPub"));
                CellFechaCob.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.fechaCobro"));
                CellNombreInst.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.nombreInstFina"));
                CellRefBanc.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.refBanca"));
                CellNumCuenta.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cuentaReceptor"));

                CellTotalRFC.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.totalRFC"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }

    }
}
