﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using App.DAL.Purchases;

namespace FloridoERP.Report
{
    public partial class ChangeCost : DevExpress.XtraReports.UI.XtraReport
    {
        private static DataSet DataSetTableExchangeCurrency = null;
        public ChangeCost()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<PurchaseBasePriceViewModel> purchase, DateTime DateInit,DateTime DateFin)
        {
            DataSetTableExchangeCurrency = new DataSet();

            DataTable DataTableCurrency = new DataTable();
            DataSetTableExchangeCurrency.Tables.Add(DataTableCurrency);

            xrLabelDateFin.Text = DateFin.ToString("MM/dd/yyyy");
            xrLabelDate.Text = DateInit.ToString("MM/dd/yyyy");

            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellCode"      ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellName"      ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellBissness"  ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellCost"      ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellCurrency"  ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellDepartment"));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellFamily"    ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellCategory"  ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellDateInit"  ));
            DataTableCurrency.Columns.Add(new DataColumn("xrTableCellDateFin"   ));

            foreach (var item in purchase)
            {
                DataRow DataRowOrdered = DataSetTableExchangeCurrency.Tables[0].NewRow();
                DataRowOrdered["xrTableCellCode"] = item.part_number;
                DataRowOrdered["xrTableCellName"] = item.part_name;
                DataRowOrdered["xrTableCellBissness"] = item.business_name;
                DataRowOrdered["xrTableCellCost"] = "$ " +item.base_price;
                DataRowOrdered["xrTableCellCurrency"] = item.currency;
                DataRowOrdered["xrTableCellDepartment"] = item.NameLevel1;
                DataRowOrdered["xrTableCellFamily"] = item.NameLevel2;
                DataRowOrdered["xrTableCellCategory"] = item.NameLevel3;
                DataRowOrdered["xrTableCellDateInit"] = item.valid_from;
                DataRowOrdered["xrTableCellDateFin"] = item.valid_to;
                DataSetTableExchangeCurrency.Tables[0].Rows.Add(DataRowOrdered);
            }
            xrTableCellCode.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCode"));
            xrTableCellName.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellName"));
            xrTableCellBissness.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellBissness"));
            xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCost"));
            xrTableCellCurrency.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCurrency"));
            xrTableCellDepartment.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellDepartment"));
            xrTableCellFamily.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellFamily"));
            xrTableCellCategory.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellCategory"));
            xrTableCellDateInit.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellDateInit"));
            xrTableCellDateFin.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellDateFin"));

            return DataSetTableExchangeCurrency;
        }
        internal object printTable(object p, List<PurchaseBasePriceViewModel> list)
        {
            throw new NotImplementedException();
        }
    }
}
