﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using App.DAL;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.InternalRequirement;
using App.Entities.ViewModels.Site;
using App.Entities.ViewModels.Creditors;
using System.Collections.Generic;

namespace FloridoERP.Report
{
    public partial class PurchasesOrderRequirement : DevExpress.XtraReports.UI.XtraReport
    {
        private UserMasterRepository _userMasterRepository;
        public DataSet DataSetTableOrdered = null;
        public decimal total = 0;
        public decimal iva = 0;
        public decimal import = 0;
        public string creName = "";
        public string crePhone = "";
        public string credEmail = "";
        public bool Error = true;
        public PurchasesOrderRequirement()
        {
            InitializeComponent();
            _userMasterRepository = new UserMasterRepository();
        }

        public DataSet printTable(int folio, int purchase_no, string uuser, List<AllItemSitesSupplier> items, CreditorPOModel creditor, decimal taxSupplier)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                //Purchase creditor
                //xrExecute.Text = header.cuser;
                //xrExecuteDate.Text = header.cdate.HasValue ? header.cdate.Value.ToString() : string.Empty;

                xrExecute.Text = _userMasterRepository.getFirstLastName(uuser);
                xrExecuteDate.Text = DateTime.Now.ToShortDateString();
                xrFolio.Text = folio.ToString();

                xrRequirement.Text = purchase_no.ToString();

                if (taxSupplier != 0)
                {
                    xrIVASupplier.Text = "No Aplica";
                } else
                {
                    xrIVASupplier.Text = taxSupplier.ToString() + " %";
                }

                xrCreditorBusinessName.Text = creditor.CreditorNameBusiness ?? "";
                xrCreditorNombre.Text = creditor.CreditorName ?? "";
                xrCreditorPhone.Text = creditor.CreditorPhone ?? "";
                xrCreditorEmail.Text = creditor.CreditorEmail ?? "";

                //Site information
                xrStore.Text = items[0].site_name;
                xrAddress.Text = items[0].address;
                xrCity.Text = items[0].city;
                xrState.Text = items[0].state;
                xrPhone.Text = items[0].phone;
                xrEmail.Text = items[0].email_manager;
                xrCP.Text = items[0].potal_code;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellBaseCost"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellImport"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIVA"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSubtotal"));
                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.part_number;
                    DataRowOrdered["xrTableCellDescription"] = item.description;
                    DataRowOrdered["xrTableCellQuantity"] = item.quantity;
                    DataRowOrdered["xrTableCellBaseCost"] = Math.Round(item.price, 2);
                    DataRowOrdered["xrTableCellImport"] = Math.Round(item.import, 2);
                    DataRowOrdered["xrTableCellIVA"] = Math.Round(item.iva, 2);
                    DataRowOrdered["xrTableCellSubtotal"] = Math.Round(item.total, 2);
                    total += item.total;
                    iva += item.iva;
                    import = item.import;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }
                xrTotal.Text = total.ToString();
                xrIVA.Text = iva.ToString();
                xrImport.Text = import.ToString();
                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellBaseCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellBaseCost"));
                xrTableCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellImport"));
                xrTableCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIVA"));
                xrTableCellSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSubtotal"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

    }
}
