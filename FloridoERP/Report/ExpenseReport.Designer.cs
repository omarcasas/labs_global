﻿namespace FloridoERP.Report
{
    partial class ExpenseReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExpenseReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStore = new DevExpress.XtraReports.UI.XRLabel();
            this.xrStore = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateTimeStart = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTimeNow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUser = new DevExpress.XtraReports.UI.XRLabel();
            this.DashCaus = new DevExpress.XtraReports.UI.XRChart();
            this.xrlblDateTimeEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTimeEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCategory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCategory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart2 = new DevExpress.XtraReports.UI.XRChart();
            this.xrChart3 = new DevExpress.XtraReports.UI.XRChart();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellFolio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellSite = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellProvider = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDateRequirement = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSite = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelProvider = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDateRequirement = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableFolio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableIEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable3Detalle = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3PartNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3Description = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3Quantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3Unitcost = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3IEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3IVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3Subtotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrlblIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblPartNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblQuantity = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUnitCost = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSubTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDetail = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5Quantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5UnitCost = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5Discount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5Subtotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCellCurrency = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.DashCaus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 29F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 1F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.xrTitle,
            this.xrlblStore,
            this.xrStore,
            this.xrlblDateTimeStart,
            this.xrDateTimeNow,
            this.xrUser,
            this.xrlblUser,
            this.DashCaus,
            this.xrlblDateTimeEnd,
            this.xrDateTimeEnd,
            this.xrlblSupplier,
            this.xrSupplier,
            this.xrlblCategory,
            this.xrCategory,
            this.xrlblType,
            this.xrType,
            this.xrChart2,
            this.xrChart3});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 1035.833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(1.071167F, 27.16667F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(91.18595F, 73.16666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrTitle
            // 
            this.xrTitle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTitle.Dpi = 100F;
            this.xrTitle.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTitle.LocationFloat = new DevExpress.Utils.PointFloat(334.339F, 0F);
            this.xrTitle.Name = "xrTitle";
            this.xrTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTitle.SizeF = new System.Drawing.SizeF(195.8333F, 27.16667F);
            this.xrTitle.StylePriority.UseBorderDashStyle = false;
            this.xrTitle.StylePriority.UseFont = false;
            this.xrTitle.Text = "Reporte de Gastos.";
            // 
            // xrlblStore
            // 
            this.xrlblStore.Dpi = 100F;
            this.xrlblStore.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStore.LocationFloat = new DevExpress.Utils.PointFloat(103.5711F, 27.16667F);
            this.xrlblStore.Name = "xrlblStore";
            this.xrlblStore.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStore.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblStore.StylePriority.UseFont = false;
            this.xrlblStore.StylePriority.UseTextAlignment = false;
            this.xrlblStore.Text = "Tienda:";
            this.xrlblStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrStore
            // 
            this.xrStore.Dpi = 100F;
            this.xrStore.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrStore.LocationFloat = new DevExpress.Utils.PointFloat(202.4878F, 27.16667F);
            this.xrStore.Name = "xrStore";
            this.xrStore.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrStore.SizeF = new System.Drawing.SizeF(546.441F, 23F);
            this.xrStore.StylePriority.UseFont = false;
            this.xrStore.StylePriority.UseTextAlignment = false;
            this.xrStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblDateTimeStart
            // 
            this.xrlblDateTimeStart.Dpi = 100F;
            this.xrlblDateTimeStart.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateTimeStart.LocationFloat = new DevExpress.Utils.PointFloat(103.5711F, 73.16666F);
            this.xrlblDateTimeStart.Name = "xrlblDateTimeStart";
            this.xrlblDateTimeStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateTimeStart.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblDateTimeStart.StylePriority.UseFont = false;
            this.xrlblDateTimeStart.StylePriority.UseTextAlignment = false;
            this.xrlblDateTimeStart.Text = "Fecha Inicio:";
            this.xrlblDateTimeStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDateTimeNow
            // 
            this.xrDateTimeNow.Dpi = 100F;
            this.xrDateTimeNow.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrDateTimeNow.LocationFloat = new DevExpress.Utils.PointFloat(202.4878F, 73.16666F);
            this.xrDateTimeNow.Name = "xrDateTimeNow";
            this.xrDateTimeNow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTimeNow.SizeF = new System.Drawing.SizeF(218.3094F, 23F);
            this.xrDateTimeNow.StylePriority.UseFont = false;
            this.xrDateTimeNow.StylePriority.UseTextAlignment = false;
            this.xrDateTimeNow.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrUser
            // 
            this.xrUser.Dpi = 100F;
            this.xrUser.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrUser.LocationFloat = new DevExpress.Utils.PointFloat(202.4878F, 50.16666F);
            this.xrUser.Name = "xrUser";
            this.xrUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrUser.SizeF = new System.Drawing.SizeF(218.3094F, 23F);
            this.xrUser.StylePriority.UseFont = false;
            this.xrUser.StylePriority.UseTextAlignment = false;
            this.xrUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblUser
            // 
            this.xrlblUser.Dpi = 100F;
            this.xrlblUser.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUser.LocationFloat = new DevExpress.Utils.PointFloat(103.5711F, 50.16667F);
            this.xrlblUser.Name = "xrlblUser";
            this.xrlblUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUser.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblUser.StylePriority.UseFont = false;
            this.xrlblUser.StylePriority.UseTextAlignment = false;
            this.xrlblUser.Text = "Solicitante:";
            this.xrlblUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DashCaus
            // 
            this.DashCaus.BorderColor = System.Drawing.Color.Black;
            this.DashCaus.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DashCaus.Dpi = 100F;
            this.DashCaus.Legend.Name = "Default Legend";
            this.DashCaus.LocationFloat = new DevExpress.Utils.PointFloat(11.78929F, 136.875F);
            this.DashCaus.Name = "DashCaus";
            this.DashCaus.PaletteName = "Oriel";
            this.DashCaus.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.DashCaus.SizeF = new System.Drawing.SizeF(728.282F, 272.9167F);
            // 
            // xrlblDateTimeEnd
            // 
            this.xrlblDateTimeEnd.Dpi = 100F;
            this.xrlblDateTimeEnd.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateTimeEnd.LocationFloat = new DevExpress.Utils.PointFloat(420.7973F, 73.16666F);
            this.xrlblDateTimeEnd.Name = "xrlblDateTimeEnd";
            this.xrlblDateTimeEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateTimeEnd.SizeF = new System.Drawing.SizeF(94.75671F, 23F);
            this.xrlblDateTimeEnd.StylePriority.UseFont = false;
            this.xrlblDateTimeEnd.StylePriority.UseTextAlignment = false;
            this.xrlblDateTimeEnd.Text = "Fecha Final:";
            this.xrlblDateTimeEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrDateTimeEnd
            // 
            this.xrDateTimeEnd.Dpi = 100F;
            this.xrDateTimeEnd.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrDateTimeEnd.LocationFloat = new DevExpress.Utils.PointFloat(521.3147F, 73.16666F);
            this.xrDateTimeEnd.Name = "xrDateTimeEnd";
            this.xrDateTimeEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTimeEnd.SizeF = new System.Drawing.SizeF(218.7567F, 23F);
            this.xrDateTimeEnd.StylePriority.UseFont = false;
            this.xrDateTimeEnd.StylePriority.UseTextAlignment = false;
            this.xrDateTimeEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblSupplier
            // 
            this.xrlblSupplier.Dpi = 100F;
            this.xrlblSupplier.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSupplier.LocationFloat = new DevExpress.Utils.PointFloat(420.7973F, 50.16667F);
            this.xrlblSupplier.Name = "xrlblSupplier";
            this.xrlblSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSupplier.SizeF = new System.Drawing.SizeF(92.56622F, 23F);
            this.xrlblSupplier.StylePriority.UseFont = false;
            this.xrlblSupplier.StylePriority.UseTextAlignment = false;
            this.xrlblSupplier.Text = "Proveedor:";
            this.xrlblSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrSupplier
            // 
            this.xrSupplier.Dpi = 100F;
            this.xrSupplier.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrSupplier.LocationFloat = new DevExpress.Utils.PointFloat(513.3635F, 50.16666F);
            this.xrSupplier.Name = "xrSupplier";
            this.xrSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSupplier.SizeF = new System.Drawing.SizeF(235.5654F, 23F);
            this.xrSupplier.StylePriority.UseFont = false;
            this.xrSupplier.StylePriority.UseTextAlignment = false;
            this.xrSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblCategory
            // 
            this.xrlblCategory.Dpi = 100F;
            this.xrlblCategory.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCategory.LocationFloat = new DevExpress.Utils.PointFloat(103.5711F, 96.16667F);
            this.xrlblCategory.Name = "xrlblCategory";
            this.xrlblCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCategory.SizeF = new System.Drawing.SizeF(98.91669F, 23F);
            this.xrlblCategory.StylePriority.UseFont = false;
            this.xrlblCategory.StylePriority.UseTextAlignment = false;
            this.xrlblCategory.Text = "Categoría:";
            this.xrlblCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrlblCategory.Visible = false;
            // 
            // xrCategory
            // 
            this.xrCategory.Dpi = 100F;
            this.xrCategory.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrCategory.LocationFloat = new DevExpress.Utils.PointFloat(202.4878F, 96.16667F);
            this.xrCategory.Name = "xrCategory";
            this.xrCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrCategory.SizeF = new System.Drawing.SizeF(218.3094F, 23F);
            this.xrCategory.StylePriority.UseFont = false;
            this.xrCategory.StylePriority.UseTextAlignment = false;
            this.xrCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrCategory.Visible = false;
            // 
            // xrlblType
            // 
            this.xrlblType.Dpi = 100F;
            this.xrlblType.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblType.LocationFloat = new DevExpress.Utils.PointFloat(420.7973F, 96.16667F);
            this.xrlblType.Name = "xrlblType";
            this.xrlblType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblType.SizeF = new System.Drawing.SizeF(94.75671F, 23F);
            this.xrlblType.StylePriority.UseFont = false;
            this.xrlblType.StylePriority.UseTextAlignment = false;
            this.xrlblType.Text = "Tipo Gasto:";
            this.xrlblType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrlblType.Visible = false;
            // 
            // xrType
            // 
            this.xrType.Dpi = 100F;
            this.xrType.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrType.LocationFloat = new DevExpress.Utils.PointFloat(515.554F, 96.16667F);
            this.xrType.Name = "xrType";
            this.xrType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrType.SizeF = new System.Drawing.SizeF(218.7567F, 23F);
            this.xrType.StylePriority.UseFont = false;
            this.xrType.StylePriority.UseTextAlignment = false;
            this.xrType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrType.Visible = false;
            // 
            // xrChart2
            // 
            this.xrChart2.BorderColor = System.Drawing.Color.Black;
            this.xrChart2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart2.Dpi = 100F;
            this.xrChart2.Legend.Name = "Default Legend";
            this.xrChart2.LocationFloat = new DevExpress.Utils.PointFloat(11.71776F, 441.0417F);
            this.xrChart2.Name = "xrChart2";
            this.xrChart2.PaletteName = "Concourse";
            this.xrChart2.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart2.SizeF = new System.Drawing.SizeF(728.2822F, 272.9167F);
            // 
            // xrChart3
            // 
            this.xrChart3.BorderColor = System.Drawing.Color.Black;
            this.xrChart3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart3.Dpi = 100F;
            this.xrChart3.Legend.Name = "Default Legend";
            this.xrChart3.LocationFloat = new DevExpress.Utils.PointFloat(11.78934F, 732.0835F);
            this.xrChart3.Name = "xrChart3";
            this.xrChart3.PaletteName = "Mixed";
            this.xrChart3.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart3.SizeF = new System.Drawing.SizeF(728.2821F, 272.9167F);
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader1,
            this.GroupFooter1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(748.933F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellFolio,
            this.xrTableCellSite,
            this.xrTableCellType,
            this.xrTableCellProvider,
            this.xrTableCellDateRequirement,
            this.xrTableCellCurrency,
            this.xrTableCellIEPS,
            this.xrTableCellIVA,
            this.xrTableCellTotal});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCellFolio
            // 
            this.xrTableCellFolio.Dpi = 100F;
            this.xrTableCellFolio.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellFolio.Name = "xrTableCellFolio";
            this.xrTableCellFolio.StylePriority.UseFont = false;
            this.xrTableCellFolio.StylePriority.UseTextAlignment = false;
            this.xrTableCellFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellFolio.Weight = 0.43402523114650488D;
            // 
            // xrTableCellSite
            // 
            this.xrTableCellSite.Dpi = 100F;
            this.xrTableCellSite.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellSite.Name = "xrTableCellSite";
            this.xrTableCellSite.StylePriority.UseFont = false;
            this.xrTableCellSite.StylePriority.UseTextAlignment = false;
            this.xrTableCellSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellSite.Weight = 1.1845707598006108D;
            // 
            // xrTableCellType
            // 
            this.xrTableCellType.Dpi = 100F;
            this.xrTableCellType.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellType.Name = "xrTableCellType";
            this.xrTableCellType.StylePriority.UseFont = false;
            this.xrTableCellType.StylePriority.UseTextAlignment = false;
            this.xrTableCellType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellType.Weight = 0.80024282574301941D;
            // 
            // xrTableCellProvider
            // 
            this.xrTableCellProvider.Dpi = 100F;
            this.xrTableCellProvider.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellProvider.Name = "xrTableCellProvider";
            this.xrTableCellProvider.StylePriority.UseFont = false;
            this.xrTableCellProvider.StylePriority.UseTextAlignment = false;
            this.xrTableCellProvider.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellProvider.Weight = 0.853715721014132D;
            // 
            // xrTableCellDateRequirement
            // 
            this.xrTableCellDateRequirement.Dpi = 100F;
            this.xrTableCellDateRequirement.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellDateRequirement.Name = "xrTableCellDateRequirement";
            this.xrTableCellDateRequirement.StylePriority.UseFont = false;
            this.xrTableCellDateRequirement.StylePriority.UseTextAlignment = false;
            this.xrTableCellDateRequirement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellDateRequirement.Weight = 0.75282339796304942D;
            // 
            // xrTableCellIEPS
            // 
            this.xrTableCellIEPS.Dpi = 100F;
            this.xrTableCellIEPS.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellIEPS.Name = "xrTableCellIEPS";
            this.xrTableCellIEPS.StylePriority.UseFont = false;
            this.xrTableCellIEPS.StylePriority.UseTextAlignment = false;
            this.xrTableCellIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIEPS.Weight = 0.62435453173140931D;
            // 
            // xrTableCellIVA
            // 
            this.xrTableCellIVA.Dpi = 100F;
            this.xrTableCellIVA.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellIVA.Name = "xrTableCellIVA";
            this.xrTableCellIVA.StylePriority.UseFont = false;
            this.xrTableCellIVA.StylePriority.UseTextAlignment = false;
            this.xrTableCellIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIVA.Weight = 0.62669187113934144D;
            // 
            // xrTableCellTotal
            // 
            this.xrTableCellTotal.Dpi = 100F;
            this.xrTableCellTotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellTotal.Name = "xrTableCellTotal";
            this.xrTableCellTotal.StylePriority.UseFont = false;
            this.xrTableCellTotal.StylePriority.UseTextAlignment = false;
            this.xrTableCellTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellTotal.Weight = 0.79468385639025285D;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabelIVA,
            this.xrLabelIEPS,
            this.xrLabelTotal,
            this.xrLabelFolio,
            this.xrLabelSite,
            this.xrLabelType,
            this.xrLabelProvider,
            this.xrLabelDateRequirement});
            this.ReportHeader1.Dpi = 100F;
            this.ReportHeader1.HeightF = 52.08334F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrLabelIVA
            // 
            this.xrLabelIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelIVA.Dpi = 100F;
            this.xrLabelIVA.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelIVA.LocationFloat = new DevExpress.Utils.PointFloat(587.5828F, 0F);
            this.xrLabelIVA.Name = "xrLabelIVA";
            this.xrLabelIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelIVA.SizeF = new System.Drawing.SizeF(71.1402F, 31.25F);
            this.xrLabelIVA.StylePriority.UseBorders = false;
            this.xrLabelIVA.StylePriority.UseFont = false;
            this.xrLabelIVA.StylePriority.UseTextAlignment = false;
            this.xrLabelIVA.Text = "IVA";
            this.xrLabelIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelIEPS
            // 
            this.xrLabelIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelIEPS.Dpi = 100F;
            this.xrLabelIEPS.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelIEPS.LocationFloat = new DevExpress.Utils.PointFloat(516.7081F, 0F);
            this.xrLabelIEPS.Name = "xrLabelIEPS";
            this.xrLabelIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelIEPS.SizeF = new System.Drawing.SizeF(70.87469F, 31.25F);
            this.xrLabelIEPS.StylePriority.UseBorders = false;
            this.xrLabelIEPS.StylePriority.UseFont = false;
            this.xrLabelIEPS.StylePriority.UseTextAlignment = false;
            this.xrLabelIEPS.Text = "IEPS";
            this.xrLabelIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelTotal
            // 
            this.xrLabelTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelTotal.Dpi = 100F;
            this.xrLabelTotal.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelTotal.LocationFloat = new DevExpress.Utils.PointFloat(658.723F, 0F);
            this.xrLabelTotal.Name = "xrLabelTotal";
            this.xrLabelTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTotal.SizeF = new System.Drawing.SizeF(90.20569F, 31.25F);
            this.xrLabelTotal.StylePriority.UseBorders = false;
            this.xrLabelTotal.StylePriority.UseFont = false;
            this.xrLabelTotal.StylePriority.UseTextAlignment = false;
            this.xrLabelTotal.Text = "Total";
            this.xrLabelTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelFolio
            // 
            this.xrLabelFolio.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelFolio.Dpi = 100F;
            this.xrLabelFolio.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelFolio.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabelFolio.Name = "xrLabelFolio";
            this.xrLabelFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFolio.SizeF = new System.Drawing.SizeF(49.26917F, 31.25F);
            this.xrLabelFolio.StylePriority.UseBorders = false;
            this.xrLabelFolio.StylePriority.UseFont = false;
            this.xrLabelFolio.StylePriority.UseTextAlignment = false;
            this.xrLabelFolio.Text = "Folio";
            this.xrLabelFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelSite
            // 
            this.xrLabelSite.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelSite.Dpi = 100F;
            this.xrLabelSite.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelSite.LocationFloat = new DevExpress.Utils.PointFloat(49.2691F, 0F);
            this.xrLabelSite.Multiline = true;
            this.xrLabelSite.Name = "xrLabelSite";
            this.xrLabelSite.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelSite.SizeF = new System.Drawing.SizeF(134.4687F, 31.25F);
            this.xrLabelSite.StylePriority.UseBorders = false;
            this.xrLabelSite.StylePriority.UseFont = false;
            this.xrLabelSite.StylePriority.UseTextAlignment = false;
            this.xrLabelSite.Text = "Tienda";
            this.xrLabelSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelType
            // 
            this.xrLabelType.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelType.Dpi = 100F;
            this.xrLabelType.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelType.LocationFloat = new DevExpress.Utils.PointFloat(183.7378F, 0F);
            this.xrLabelType.Name = "xrLabelType";
            this.xrLabelType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelType.SizeF = new System.Drawing.SizeF(90.84119F, 31.25F);
            this.xrLabelType.StylePriority.UseBorders = false;
            this.xrLabelType.StylePriority.UseFont = false;
            this.xrLabelType.StylePriority.UseTextAlignment = false;
            this.xrLabelType.Text = "Tipo de Gasto";
            this.xrLabelType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelProvider
            // 
            this.xrLabelProvider.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelProvider.Dpi = 100F;
            this.xrLabelProvider.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelProvider.LocationFloat = new DevExpress.Utils.PointFloat(274.579F, 0F);
            this.xrLabelProvider.Name = "xrLabelProvider";
            this.xrLabelProvider.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelProvider.SizeF = new System.Drawing.SizeF(96.91101F, 31.25F);
            this.xrLabelProvider.StylePriority.UseBorders = false;
            this.xrLabelProvider.StylePriority.UseFont = false;
            this.xrLabelProvider.StylePriority.UseTextAlignment = false;
            this.xrLabelProvider.Text = "Proveedor";
            this.xrLabelProvider.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelDateRequirement
            // 
            this.xrLabelDateRequirement.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelDateRequirement.Dpi = 100F;
            this.xrLabelDateRequirement.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDateRequirement.LocationFloat = new DevExpress.Utils.PointFloat(371.49F, 0F);
            this.xrLabelDateRequirement.Name = "xrLabelDateRequirement";
            this.xrLabelDateRequirement.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDateRequirement.SizeF = new System.Drawing.SizeF(85.45813F, 31.25F);
            this.xrLabelDateRequirement.StylePriority.UseBorders = false;
            this.xrLabelDateRequirement.StylePriority.UseFont = false;
            this.xrLabelDateRequirement.StylePriority.UseTextAlignment = false;
            this.xrLabelDateRequirement.Text = "Fecha de Requisión";
            this.xrLabelDateRequirement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 34.375F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(431.25F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(317.6787F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableFolio,
            this.xrTableIEPS,
            this.xrTableIVA,
            this.xrTableTotal});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableFolio
            // 
            this.xrTableFolio.Dpi = 100F;
            this.xrTableFolio.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableFolio.Name = "xrTableFolio";
            this.xrTableFolio.StylePriority.UseFont = false;
            this.xrTableFolio.StylePriority.UseTextAlignment = false;
            this.xrTableFolio.Text = "Total";
            this.xrTableFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableFolio.Weight = 0.8545814063450472D;
            // 
            // xrTableIEPS
            // 
            this.xrTableIEPS.Dpi = 100F;
            this.xrTableIEPS.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableIEPS.Name = "xrTableIEPS";
            this.xrTableIEPS.StylePriority.UseFont = false;
            this.xrTableIEPS.StylePriority.UseTextAlignment = false;
            this.xrTableIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableIEPS.Weight = 0.70874702661939271D;
            // 
            // xrTableIVA
            // 
            this.xrTableIVA.Dpi = 100F;
            this.xrTableIVA.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableIVA.Name = "xrTableIVA";
            this.xrTableIVA.StylePriority.UseFont = false;
            this.xrTableIVA.StylePriority.UseTextAlignment = false;
            this.xrTableIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableIVA.Weight = 0.71140266664106977D;
            // 
            // xrTableTotal
            // 
            this.xrTableTotal.Dpi = 100F;
            this.xrTableTotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableTotal.Name = "xrTableTotal";
            this.xrTableTotal.StylePriority.UseFont = false;
            this.xrTableTotal.StylePriority.UseTextAlignment = false;
            this.xrTableTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableTotal.Weight = 0.90205645625406783D;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2,
            this.GroupFooter2});
            this.DetailReport1.Dpi = 100F;
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail2.Dpi = 100F;
            this.Detail2.HeightF = 26.04167F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Dpi = 100F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(750F, 25F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable3Detalle,
            this.xrTable3PartNumber,
            this.xrTable3Description,
            this.xrTable3Quantity,
            this.xrTable3Unitcost,
            this.xrTable3IEPS,
            this.xrTable3IVA,
            this.xrTable3Subtotal});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTable3Detalle
            // 
            this.xrTable3Detalle.Dpi = 100F;
            this.xrTable3Detalle.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3Detalle.Name = "xrTable3Detalle";
            this.xrTable3Detalle.StylePriority.UseFont = false;
            this.xrTable3Detalle.StylePriority.UseTextAlignment = false;
            this.xrTable3Detalle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3Detalle.Weight = 0.49269169191242956D;
            // 
            // xrTable3PartNumber
            // 
            this.xrTable3PartNumber.Dpi = 100F;
            this.xrTable3PartNumber.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3PartNumber.Name = "xrTable3PartNumber";
            this.xrTable3PartNumber.StylePriority.UseFont = false;
            this.xrTable3PartNumber.StylePriority.UseTextAlignment = false;
            this.xrTable3PartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3PartNumber.Weight = 1.2417864163492816D;
            // 
            // xrTable3Description
            // 
            this.xrTable3Description.Dpi = 100F;
            this.xrTable3Description.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3Description.Name = "xrTable3Description";
            this.xrTable3Description.StylePriority.UseFont = false;
            this.xrTable3Description.StylePriority.UseTextAlignment = false;
            this.xrTable3Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3Description.Weight = 1.84895896572334D;
            // 
            // xrTable3Quantity
            // 
            this.xrTable3Quantity.Dpi = 100F;
            this.xrTable3Quantity.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3Quantity.Name = "xrTable3Quantity";
            this.xrTable3Quantity.StylePriority.UseFont = false;
            this.xrTable3Quantity.StylePriority.UseTextAlignment = false;
            this.xrTable3Quantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3Quantity.Weight = 0.84334621480060989D;
            // 
            // xrTable3Unitcost
            // 
            this.xrTable3Unitcost.Dpi = 100F;
            this.xrTable3Unitcost.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3Unitcost.Name = "xrTable3Unitcost";
            this.xrTable3Unitcost.StylePriority.UseFont = false;
            this.xrTable3Unitcost.StylePriority.UseTextAlignment = false;
            this.xrTable3Unitcost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3Unitcost.Weight = 0.87493987729597011D;
            // 
            // xrTable3IEPS
            // 
            this.xrTable3IEPS.Dpi = 100F;
            this.xrTable3IEPS.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3IEPS.Name = "xrTable3IEPS";
            this.xrTable3IEPS.StylePriority.UseFont = false;
            this.xrTable3IEPS.StylePriority.UseTextAlignment = false;
            this.xrTable3IEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3IEPS.Weight = 0.77202087548271792D;
            // 
            // xrTable3IVA
            // 
            this.xrTable3IVA.Dpi = 100F;
            this.xrTable3IVA.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3IVA.Name = "xrTable3IVA";
            this.xrTable3IVA.StylePriority.UseFont = false;
            this.xrTable3IVA.StylePriority.UseTextAlignment = false;
            this.xrTable3IVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3IVA.Weight = 0.61765197813486639D;
            // 
            // xrTable3Subtotal
            // 
            this.xrTable3Subtotal.Dpi = 100F;
            this.xrTable3Subtotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable3Subtotal.Name = "xrTable3Subtotal";
            this.xrTable3Subtotal.StylePriority.UseFont = false;
            this.xrTable3Subtotal.StylePriority.UseTextAlignment = false;
            this.xrTable3Subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable3Subtotal.Weight = 0.80860402225271866D;
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblIEPS,
            this.xrlblPartNumber,
            this.xrlblDescription,
            this.xrlblQuantity,
            this.xrlblUnitCost,
            this.xrlblIVA,
            this.xrlblSubTotal,
            this.xrlblDetail});
            this.ReportHeader2.Dpi = 100F;
            this.ReportHeader2.HeightF = 42.29168F;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrlblIEPS
            // 
            this.xrlblIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIEPS.Dpi = 100F;
            this.xrlblIEPS.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblIEPS.LocationFloat = new DevExpress.Utils.PointFloat(530.1724F, 9.999974F);
            this.xrlblIEPS.Name = "xrlblIEPS";
            this.xrlblIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIEPS.SizeF = new System.Drawing.SizeF(77.20197F, 31.25F);
            this.xrlblIEPS.StylePriority.UseBorders = false;
            this.xrlblIEPS.StylePriority.UseFont = false;
            this.xrlblIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblIEPS.Text = "IEPS";
            this.xrlblIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblPartNumber
            // 
            this.xrlblPartNumber.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblPartNumber.Dpi = 100F;
            this.xrlblPartNumber.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblPartNumber.LocationFloat = new DevExpress.Utils.PointFloat(49.26917F, 9.999974F);
            this.xrlblPartNumber.Name = "xrlblPartNumber";
            this.xrlblPartNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblPartNumber.SizeF = new System.Drawing.SizeF(124.1786F, 31.25F);
            this.xrlblPartNumber.StylePriority.UseBorders = false;
            this.xrlblPartNumber.StylePriority.UseFont = false;
            this.xrlblPartNumber.StylePriority.UseTextAlignment = false;
            this.xrlblPartNumber.Text = "Código";
            this.xrlblPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDescription
            // 
            this.xrlblDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDescription.Dpi = 100F;
            this.xrlblDescription.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblDescription.LocationFloat = new DevExpress.Utils.PointFloat(173.4478F, 9.999974F);
            this.xrlblDescription.Name = "xrlblDescription";
            this.xrlblDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDescription.SizeF = new System.Drawing.SizeF(184.8959F, 31.25F);
            this.xrlblDescription.StylePriority.UseBorders = false;
            this.xrlblDescription.StylePriority.UseFont = false;
            this.xrlblDescription.StylePriority.UseTextAlignment = false;
            this.xrlblDescription.Text = "Descripción";
            this.xrlblDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblQuantity
            // 
            this.xrlblQuantity.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblQuantity.Dpi = 100F;
            this.xrlblQuantity.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblQuantity.LocationFloat = new DevExpress.Utils.PointFloat(358.3437F, 10.00001F);
            this.xrlblQuantity.Name = "xrlblQuantity";
            this.xrlblQuantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblQuantity.SizeF = new System.Drawing.SizeF(84.33469F, 31.25F);
            this.xrlblQuantity.StylePriority.UseBorders = false;
            this.xrlblQuantity.StylePriority.UseFont = false;
            this.xrlblQuantity.StylePriority.UseTextAlignment = false;
            this.xrlblQuantity.Text = "Cantidad";
            this.xrlblQuantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUnitCost
            // 
            this.xrlblUnitCost.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUnitCost.Dpi = 100F;
            this.xrlblUnitCost.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblUnitCost.LocationFloat = new DevExpress.Utils.PointFloat(442.6783F, 9.999974F);
            this.xrlblUnitCost.Name = "xrlblUnitCost";
            this.xrlblUnitCost.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUnitCost.SizeF = new System.Drawing.SizeF(87.49405F, 31.25F);
            this.xrlblUnitCost.StylePriority.UseBorders = false;
            this.xrlblUnitCost.StylePriority.UseFont = false;
            this.xrlblUnitCost.StylePriority.UseTextAlignment = false;
            this.xrlblUnitCost.Text = "Costo Unitario";
            this.xrlblUnitCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIVA
            // 
            this.xrlblIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIVA.Dpi = 100F;
            this.xrlblIVA.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblIVA.LocationFloat = new DevExpress.Utils.PointFloat(607.3743F, 9.999974F);
            this.xrlblIVA.Name = "xrlblIVA";
            this.xrlblIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIVA.SizeF = new System.Drawing.SizeF(61.7652F, 31.25F);
            this.xrlblIVA.StylePriority.UseBorders = false;
            this.xrlblIVA.StylePriority.UseFont = false;
            this.xrlblIVA.StylePriority.UseTextAlignment = false;
            this.xrlblIVA.Text = "IVA";
            this.xrlblIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblSubTotal
            // 
            this.xrlblSubTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblSubTotal.Dpi = 100F;
            this.xrlblSubTotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblSubTotal.LocationFloat = new DevExpress.Utils.PointFloat(669.1396F, 9.999974F);
            this.xrlblSubTotal.Name = "xrlblSubTotal";
            this.xrlblSubTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSubTotal.SizeF = new System.Drawing.SizeF(80.86041F, 31.25F);
            this.xrlblSubTotal.StylePriority.UseBorders = false;
            this.xrlblSubTotal.StylePriority.UseFont = false;
            this.xrlblSubTotal.StylePriority.UseTextAlignment = false;
            this.xrlblSubTotal.Text = "SubTotal";
            this.xrlblSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDetail
            // 
            this.xrlblDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDetail.Dpi = 100F;
            this.xrlblDetail.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrlblDetail.LocationFloat = new DevExpress.Utils.PointFloat(0.2500534F, 9.999974F);
            this.xrlblDetail.Name = "xrlblDetail";
            this.xrlblDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDetail.SizeF = new System.Drawing.SizeF(49.01912F, 31.25F);
            this.xrlblDetail.StylePriority.UseBorders = false;
            this.xrlblDetail.StylePriority.UseFont = false;
            this.xrlblDetail.StylePriority.UseTextAlignment = false;
            this.xrlblDetail.Text = "Detalle";
            this.xrlblDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.GroupFooter2.Dpi = 100F;
            this.GroupFooter2.HeightF = 25F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Dpi = 100F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(217.4778F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable5.SizeF = new System.Drawing.SizeF(532.5222F, 25F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.Visible = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTable5Quantity,
            this.xrTable5UnitCost,
            this.xrTable5Discount,
            this.xrTable5Subtotal});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 100F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Total";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 1.919375306838599D;
            // 
            // xrTable5Quantity
            // 
            this.xrTable5Quantity.Dpi = 100F;
            this.xrTable5Quantity.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5Quantity.Name = "xrTable5Quantity";
            this.xrTable5Quantity.StylePriority.UseFont = false;
            this.xrTable5Quantity.StylePriority.UseTextAlignment = false;
            this.xrTable5Quantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable5Quantity.Weight = 0.83763438245002675D;
            // 
            // xrTable5UnitCost
            // 
            this.xrTable5UnitCost.Dpi = 100F;
            this.xrTable5UnitCost.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5UnitCost.Name = "xrTable5UnitCost";
            this.xrTable5UnitCost.StylePriority.UseFont = false;
            this.xrTable5UnitCost.StylePriority.UseTextAlignment = false;
            this.xrTable5UnitCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable5UnitCost.Weight = 0.92667888638897367D;
            // 
            // xrTable5Discount
            // 
            this.xrTable5Discount.Dpi = 100F;
            this.xrTable5Discount.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5Discount.Name = "xrTable5Discount";
            this.xrTable5Discount.StylePriority.UseFont = false;
            this.xrTable5Discount.StylePriority.UseTextAlignment = false;
            this.xrTable5Discount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable5Discount.Weight = 0.83293223190318655D;
            // 
            // xrTable5Subtotal
            // 
            this.xrTable5Subtotal.Dpi = 100F;
            this.xrTable5Subtotal.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable5Subtotal.Name = "xrTable5Subtotal";
            this.xrTable5Subtotal.StylePriority.UseFont = false;
            this.xrTable5Subtotal.StylePriority.UseTextAlignment = false;
            this.xrTable5Subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable5Subtotal.Weight = 0.8086020090590782D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 27.08333F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(648.4284F, 9.999974F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(456.9481F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(59.76004F, 31.25F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Tipo de Moneda";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCellCurrency
            // 
            this.xrTableCellCurrency.Dpi = 100F;
            this.xrTableCellCurrency.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCellCurrency.Name = "xrTableCellCurrency";
            this.xrTableCellCurrency.StylePriority.UseFont = false;
            this.xrTableCellCurrency.StylePriority.UseTextAlignment = false;
            this.xrTableCellCurrency.Text = "Currency";
            this.xrTableCellCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellCurrency.Weight = 0.52644208823677663D;
            // 
            // ExpenseReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport,
            this.DetailReport1,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 29, 1);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.DashCaus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrTitle;
        private DevExpress.XtraReports.UI.XRLabel xrlblStore;
        private DevExpress.XtraReports.UI.XRLabel xrStore;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateTimeStart;
        private DevExpress.XtraReports.UI.XRLabel xrDateTimeNow;
        private DevExpress.XtraReports.UI.XRLabel xrUser;
        private DevExpress.XtraReports.UI.XRLabel xrlblUser;
        private DevExpress.XtraReports.UI.XRChart DashCaus;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateTimeEnd;
        private DevExpress.XtraReports.UI.XRLabel xrDateTimeEnd;
        private DevExpress.XtraReports.UI.XRLabel xrlblSupplier;
        private DevExpress.XtraReports.UI.XRLabel xrSupplier;
        private DevExpress.XtraReports.UI.XRLabel xrlblType;
        private DevExpress.XtraReports.UI.XRLabel xrType;
        private DevExpress.XtraReports.UI.XRChart xrChart2;
        private DevExpress.XtraReports.UI.XRChart xrChart3;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFolio;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSite;
        private DevExpress.XtraReports.UI.XRLabel xrLabelType;
        private DevExpress.XtraReports.UI.XRLabel xrLabelProvider;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDateRequirement;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellFolio;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellSite;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellType;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellProvider;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDateRequirement;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotal;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrlblPartNumber;
        private DevExpress.XtraReports.UI.XRLabel xrlblDescription;
        private DevExpress.XtraReports.UI.XRLabel xrlblQuantity;
        private DevExpress.XtraReports.UI.XRLabel xrlblUnitCost;
        private DevExpress.XtraReports.UI.XRLabel xrlblIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblSubTotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblDetail;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3PartNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3Description;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3Quantity;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3Unitcost;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3IEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3Subtotal;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableFolio;
        private DevExpress.XtraReports.UI.XRTableCell xrTableTotal;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable5Quantity;
        private DevExpress.XtraReports.UI.XRTableCell xrTable5UnitCost;
        private DevExpress.XtraReports.UI.XRTableCell xrTable5Discount;
        private DevExpress.XtraReports.UI.XRTableCell xrTable5Subtotal;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3Detalle;
        private DevExpress.XtraReports.UI.XRLabel xrlblCategory;
        private DevExpress.XtraReports.UI.XRLabel xrCategory;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIVA;
        private DevExpress.XtraReports.UI.XRLabel xrLabelIVA;
        private DevExpress.XtraReports.UI.XRLabel xrLabelIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrTable3IVA;
        private DevExpress.XtraReports.UI.XRTableCell xrTableIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrTableIVA;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCurrency;
    }
}
